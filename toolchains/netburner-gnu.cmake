
# Target is an embedded system
SET(CMAKE_SYSTEM_NAME Generic)
SET(CMAKE_SYSTEM_VERSION 1)
SET(CMAKE_SYSTEM_PROCESSOR m68k)

# chech NetBurner environment
IF(NOT("$ENV{NBROOT}" STREQUAL ""))
  # NBROOT is set to an invalid value by default.
  # In this case use the NBROOTMINGW variable
  IF(("$ENV{NBROOT}" STREQUAL "/nburn") AND NOT("$ENV{NBROOTMINGW}" STREQUAL ""))
    FILE(TO_CMAKE_PATH "$ENV{NBROOTMINGW}" NBROOT)
  ELSE(("$ENV{NBROOT}" STREQUAL "/nburn") AND NOT("$ENV{NBROOTMINGW}" STREQUAL ""))
    FILE(TO_CMAKE_PATH "$ENV{NBROOT}" NBROOT)
  ENDIF(("$ENV{NBROOT}" STREQUAL "/nburn") AND NOT("$ENV{NBROOTMINGW}" STREQUAL ""))
ELSE(NOT("$ENV{NBROOT}" STREQUAL ""))
  MESSAGE(FATAL_ERROR "Set your environment! (execute c:\\nburn\\SetEnv.bat for example)")
ENDIF(NOT("$ENV{NBROOT}" STREQUAL ""))

IF(NOT("$ENV{DEFPLATFORM}" STREQUAL ""))
  SET(DEFPLATFORM "$ENV{DEFPLATFORM}")
ELSE(NOT("$ENV{DEFPLATFORM}" STREQUAL ""))
  SET(DEFPLATFORM "SB70LC")
ENDIF(NOT("$ENV{DEFPLATFORM}" STREQUAL ""))

IF(NOT("$ENV{CPUFLAG}" STREQUAL ""))
  SET(CPUFLAG "$ENV{CPUFLAG}")
ELSE(NOT("$ENV{CPUFLAG}" STREQUAL ""))
  SET(CPUFLAG "m5206e")
ENDIF(NOT("$ENV{CPUFLAG}" STREQUAL ""))


IF("${DEFPLATFORM}" STREQUAL "MOD5270B")
  # revision B uses same
  SET(DEFPLATFORM_COMMON "MOD5270")
ELSE("${DEFPLATFORM}" STREQUAL "MOD5270B")
  SET(DEFPLATFORM_COMMON "${DEFPLATFORM}")
ENDIF("${DEFPLATFORM}" STREQUAL "MOD5270B")

SET(CPU_TYPE "MCF5270")

SET(ALT_IO_MODE "nb20" CACHE STRING "use the nb20 alt-io")

SET(ADVANCED_OPTIONS ON CACHE BOOL "activate advaned options")
SET(WITH_WS4D_TOOLS OFF CACHE BOOL "dpws tools disabled")
SET(WITH_HOSTING_SERVICE OFF CACHE BOOL "hosting service")
SET(WITH_OTHER_ARCH ON CACHE BOOL "other arch then linux/windows")

# SB70LC code from make file
#NBINCLUDE="$(NBROOTMINGW)/include" -I"$(NBROOTMINGW)/SB70LC/include"
#LDFLAGS = -m5206e -Wl -Wl,-n -T$(PLATFORMROOT)/lib/SB70LC.ld -Wl,-R$(NBROOTMINGW)/SB70LC/lib/sys.ld
#
#NBLIBS = $(NBROOTMINGW)/lib/SB70LC.a $(NBROOTMINGW)/lib/NetBurner.a $(NBROOTMINGW)/lib/FatFile.a $(LDEXTRA)
#DBNBLIBS = $(NBROOTMINGW)/lib/DBSB70LC.a $(NBROOTMINGW)/lib/DBNetBurner.a $(NBROOTMINGW)/lib/FatFile.a $(LDEXTRA)
#
#PLATFORMDEF=SB70LC
#CPU_TYPE=MCF5270
#ifndef COMPCODEFLAGS
#COMPCODEFLAGS = 0xffc08000 0xFFC80000
#endif

# linking
#m68k-elf-g++ -mcpu=5208 -Wl -Wl,-n -TC:\nburn\SB70LC\lib\SB70LC.ld -Wl,-RC:\nburn\SB70LC\lib\sys.ld  -Wl,-Map=linkstatus.map -o linkstatus.elf  ./main.o -Wl,--start-group,C:\nburn\lib\SB70LC.a C:\nburn\lib\NetBurner.a C:\nburn\lib\FatFile.a  C:\nburn\lib\rtl8711Library.a   -Wl,--end-group

# create netburner code
#m68k-elf-objcopy --strip-all --output-target=srec linkstatus.elf C:\nburn\bin\linkstatus.s19
#compcode C:\nburn\bin\linkstatus.s19 C:\nburn\bin\linkstatus_APP.s19 -R 0xffc08000 0xFFC80000 -PSB70LC

SET(CMAKE_EXE_LINKER_FLAGS "-${CPUFLAG} -Wl,-n -T${NBROOT}/${DEFPLATFORM}/lib/${DEFPLATFORM}.ld -Wl,-R${NBROOT}/${DEFPLATFORM}/lib/sys.ld" CACHE STRING "linker flags")

# it is necessary to add the NetBurner libs at the end of the link command.
# setting is to the linker flags does not work
SET(ADDITIONAL_IO_LIBS "-Wl,--start-group,${NBROOT}/lib/${DEFPLATFORM_COMMON}.a,${NBROOT}/lib/NetBurner.a,${NBROOT}/lib/FatFile.a,${NBROOT}/lib/rtl8711Library.a,--end-group" CACHE STRING "additional io libs")

# original makefile content
#CFLAGS     = -$(CPUFLAG) -gdwarf-2 -Wall          -falign-functions=4 -O2 -D$(PLATFORMDEF) -D$(CPU_TYPE) -DNBMINGW -I$(NBINCLUDE) $(EXTRACFLAGS)
#DBCFLAGS   = -$(CPUFLAG) -gdwarf-2 -Wall -D_DEBUG -falign-functions=4 -D$(PLATFORMDEF) -D$(CPU_TYPE) -DNBMINGW -I$(NBINCLUDE) $(EXTRADBCFLAGS)

#CXXFLAGS   = -$(CPUFLAG) -gdwarf-2 -Wall -D$(PLATFORMDEF) -D$(CPU_TYPE) -DNBMINGW -fno-rtti -fno-exceptions -falign-functions=4 -O2 -I$(NBINCLUDE) $(EXTRACXXFLAGS)
#DBCXXFLAGS = -$(CPUFLAG) -gdwarf-2 -Wall -fno-rtti -fno-exceptions -D_DEBUG -falign-functions=4 -D$(PLATFORMDEF) -D$(CPU_TYPE) -DNBMINGW -I$(NBINCLUDE) $(EXTRADBCXXFLAGS)

#ASFLAGS    = -$(CPUFLAG) $(EXTRAASFLAGS)
#DBASFLAGS  = -$(CPUFLAG) $(EXTRADBASFLAGS)


SET(CMAKE_C_FLAGS "-${CPUFLAG} -falign-functions=4 -D${DEFPLATFORM} -D${CPU_TYPE} -DNBMINGW" CACHE STRING "c flags")
SET(CMAKE_C_FLAGS_DEBUG "-gdwarf-2 -D_DEBUG" CACHE STRING "c debug flags")
SET(CMAKE_C_FLAGS_RELEASE "-O2" CACHE STRING "c release flags")
SET(CMAKE_C_FLAGS_RELWITHDEBINFO "-gdwarf-2 -O2" CACHE STRING "c release with debug info flags")
SET(CMAKE_C_FLAGS_MINSIZEREL "-Os" CACHE STRING "c min size release flags")

SET(CMAKE_CXX_FLAGS "-${CPUFLAG} -fno-rtti -fno-exceptions -falign-functions=4 -D${DEFPLATFORM} -D${CPU_TYPE} -DNBMINGW" CACHE STRING "c++ flags")
SET(CMAKE_CXX_FLAGS_DEBUG "-gdwarf-2 -D_DEBUG" CACHE STRING "c++ debug flags")
SET(CMAKE_CXX_FLAGS_RELEASE "-O2" CACHE STRING "c++ release flags")
SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-gdwarf-2 -O2" CACHE STRING "c++ release with debug info flags")
SET(CMAKE_CXX_FLAGS_MINSIZEREL "-Os" CACHE STRING "c++ min size release flags")

# add include directories to the build for the SB70LC (three default paths)
INCLUDE_DIRECTORIES(BEFORE "${NBROOT}/include" "${NBROOT}/${DEFPLATFORM}/include" "${NBROOT}/gcc-m68k/m68k-elf/include")

# Specify directories in which the linker will look for libraries.
LINK_DIRECTORIES("${NBROOT}/lib" "${NBROOT}/${DEFPLATFORM}/lib")

SET(CMAKE_C_COMPILER   "${NBROOT}/gcc-m68k/bin/m68k-elf-gcc.exe")
SET(CMAKE_CXX_COMPILER "${NBROOT}/gcc-m68k/bin/m68k-elf-g++.exe")

# we have to use the c++ linker because the initialization for c++ is required
SET(CMAKE_C_LINK_EXECUTABLE "${CMAKE_CXX_COMPILER} <FLAGS> <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> <OBJECTS>  -o <TARGET> <LINK_LIBRARIES>" CACHE STRING "c link executable")


