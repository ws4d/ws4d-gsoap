# Copyright (C) 2007  University of Rostock
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.

# this one is important
SET(CMAKE_SYSTEM_NAME Linux)

# specify the cross compiler
SET(CROSS_COMPILER_PATH /usr/bin)
SET(C_CROSS_COMPILER arm-linux-gnueabi-)

FIND_PROGRAM(CCACHE ccache)
IF(CCACHE)
    SET(CMAKE_C_COMPILER "${CCACHE}" "${CROSS_COMPILER_PATH}/${C_CROSS_COMPILER}gcc")
    SET(CMAKE_CXX_COMPILER "${CCACHE}" "${CROSS_COMPILER_PATH}/${C_CROSS_COMPILER}c++")
ELSE(CCACHE)
    SET(CMAKE_C_COMPILER "${CROSS_COMPILER_PATH}/${C_CROSS_COMPILER}gcc" "")
    SET(CMAKE_CXX_COMPILER "${CROSS_COMPILER_PATH}/${C_CROSS_COMPILER}c++" "")
ENDIF(CCACHE)

SET(CMAKE_RANLIB "${CROSS_COMPILER_PATH}/${C_CROSS_COMPILER}ranlib")
SET(CMAKE_AR "${CROSS_COMPILER_PATH}/${C_CROSS_COMPILER}ar")

SET(CMAKE_COMPILER_IS_GNUCC 1)
SET(CMAKE_COMPILER_IS_GNUCXX 1)

SET(CMAKE_SKIP_RPATH ON)

SET(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS)

INCLUDE_DIRECTORIES(BEFORE SYSTEM /usr/arm-linux-gnueabi/include)
LINK_DIRECTORIES(/usr/arm-linux-gnueabi/lib)

# where is the target environment 
SET(CMAKE_FIND_ROOT_PATH /usr/arm-linux-gnueabi/ ${GSOAP_PATH} ${DPWS_PATH} ${DPWS_HOSTEXEC_PATH})

#search for libraries and header files only in the target environment
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
