# Copyright (C) 2007  University of Rostock
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.

IF(NOT CMAKE_CROSSCOMPILING)

IF (WITH_DOCU)

CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/dpws_doxyfile.in
	${CMAKE_CURRENT_BINARY_DIR}/dpws_doxyfile
	@ONLY)

FILE(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/html)

SET(dpws_doc_DEPS
	${CMAKE_CURRENT_SOURCE_DIR}/intro.h
	${CMAKE_CURRENT_SOURCE_DIR}/dpws_info.h
	${CMAKE_CURRENT_SOURCE_DIR}/tutorial.h
	${CMAKE_CURRENT_SOURCE_DIR}/features.h
	${CMAKE_CURRENT_SOURCE_DIR}/implementation_details.h
	${CMAKE_CURRENT_SOURCE_DIR}/ref.h
	${CMAKE_CURRENT_SOURCE_DIR}/ext_sec_profile.h
	${DPWS_INCLUDE_DIR}/stddpws.h
	${DPWS_INCLUDE_DIR}/ws4d_uri.h
	${DPWS_INCLUDE_DIR}/ws4d_epr.h
	${DPWS_INCLUDE_DIR}/ws4d_device_description.h
	${DPWS_INCLUDE_DIR}/ws4d_localizedstring.h
	${DPWS_INCLUDE_DIR}/ws4d_abstract_eprlist.h
	${DPWS_INCLUDE_DIR}/ws-addressing.h
	${DPWS_INCLUDE_DIR}/ws4d_discovery_ctx.h
	${DPWS_INCLUDE_DIR}/ws-discovery.h
	${DPWS_INCLUDE_DIR}/ws-eventing.h
	${DPWS_INCLUDE_DIR}/ws4d_eprllist.h
	${DPWS_INCLUDE_DIR}/ws-metadataexchange.h
	${DPWS_INCLUDE_DIR}/ws4d_servicecache.h
	${DPWS_INCLUDE_DIR}/ws4d_alloclist.h
	${DPWS_INCLUDE_DIR}/ws4d_list.h
	${DPWS_INCLUDE_DIR}/ws4d_misc.h
	${DPWS_INCLUDE_DIR}/ws4d_mutex.h
	${DPWS_INCLUDE_DIR}/ws4d_qname.h
        ${DPWS_INCLUDE_DIR}/ws4d_qnamelist.h
        ${DPWS_INCLUDE_DIR}/ws4d_stringlist.h
        ${DPWS_INCLUDE_DIR}/ws4d_time.h
	${DPWS_INCLUDE_DIR}/ws4d_uuid.h
	${DPWS_INCLUDE_DIR}/soap_misc.h
	${DPWS_INCLUDE_DIR}/ws4d_target.h
	${DPWS_INCLUDE_DIR}/ws4d_service.h
	${DPWS_INCLUDE_DIR}/ws4d_hostingservice.h
	${DPWS_INCLUDE_DIR}/ws4d_targetservice.h
	${DPWS_INCLUDE_DIR}/ws4d_targetcache.h
	${DPWS_INCLUDE_DIR}/ws4d_epr_kv.h
	${DPWS_INCLUDE_DIR}/ws4d_subscription.h
	${DPWS_INCLUDE_DIR}/ws4d_subsmanager.h
	${DPWS_INCLUDE_DIR}/ws4d_subsmanproxy.h)

SET(dpws_doc_FILES
		${CMAKE_CURRENT_BINARY_DIR}/dpws.tags
		${CMAKE_CURRENT_BINARY_DIR}/html/index.html
		${CMAKE_CURRENT_BINARY_DIR}/latex/refman.tex)

ADD_CUSTOM_COMMAND(OUTPUT ${dpws_doc_FILES}
	COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/dpws_doxyfile
	DEPENDS ${dpws_doc_DEPS})

ADD_CUSTOM_TARGET(doc
	DEPENDS ${dpws_doc_FILES})

ADD_DEPENDENCIES(doc gen_headers gen_sources)

INSTALL(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/html DESTINATION share/doc/ws4d-gSOAP)

IF(WITH_DOCU_PDF)

SET(dpws_refman
    ${CMAKE_CURRENT_BINARY_DIR}/latex/refman.pdf)

ADD_CUSTOM_COMMAND(OUTPUT ${dpws_refman}
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/doxygen.sty ${CMAKE_CURRENT_BINARY_DIR}/latex/
    COMMAND make -C latex -s refman.pdf > /dev/null
    DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/latex/refman.tex)

ADD_CUSTOM_TARGET(gen_pdfs DEPENDS ${dpws_refman})

ADD_DEPENDENCIES(gen_pdfs gen_headers)

#INSTALL(FILES ${dpws_refman} DESTINATION share/doc/ws4d-gSOAP)

ENDIF(WITH_DOCU_PDF)
ENDIF (WITH_DOCU)
ENDIF(NOT CMAKE_CROSSCOMPILING)
