/*! \page features Features of WS4D-gSOAP
 *
 * Be aware that projects linking WS4D-gSOAP should use the same compile time
 * options as the WS4D-gSOAP build they are linking to. Compile time modes
 * change the application binary interface (ABI) !!!.
 *
 * \section buildTypes Build types
 *
 * WS4D-gSOAP offers several build modes that activate specific compiler,
 * debugging and optimization features:
 *
 * <ul>
 * <li>Debug: This mode activates the compiler debug code generation and also
 * activates debugging features is WS4D-gSOAP. Clients and Devices using this
 * mode print assertions and warnings to stdout and generated indented XML
 * messages for easier debugging. This mode produces big executables!!!</li>
 * <li>Release (default): This mode is the default mode and no debugging
 * features are active. This mode should be used for production systems</li>
 * <li>RelWithDebInfo: This mode is similar to the Release mode with the
 * exception that the compiler debug code generation is activated. This mode
 * should be used for debugging in production systems.</li>
 * <li>MinSizeRel: This mode activates compiler switches and WS4D-gSOAP features
 * to optimize clients and devices for minimal binary file size. This mode should be
 * used on resource constrained devices.</li>
 * </ul>
 *
 * This option can be configured at compile time with the cmake option
 * CMAKE_BUILD_TYPE.
 *
 * \section versionSupport DPWS version support
 *
 * WS4D-gSOAP currently supports two DPWS versions that can be switched at
 * compile:
 *
 * <ul>
 * <li>2006_02 (default): This is the DPWS version 1.0
 * from http://schemas.xmlsoap.org/ws/2006/02/devprof that is also implemented
 * in MS Windows starting from Vista.</li>
 * <li>2009_01: This is the official OASIS DPWS version 1.1 from
 * http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01 created by the OASIS WS-DD
 * technical committee.</li>
 * </ul>
 *
 * This option can be configured at compile time with the cmake option
 * DPWS_VERSION.
 *
 * \section eptype Endpoint types
 *
 * WS4D-gSOAP supports implementing the following endpoint types. When
 * applications using WS4D-gSOAP use the cmake build system they can use the
 * FindDPWS.cmake helper to link in WS4D-gSOAP. This helper defines four
 * components that represent the endpoint types WS4D-gSOAP does support.
 * Endpoints always have to link against the libraries defined in
 * DPWS_LIBRARIES. Depending on which endpoint type (component) an application
 * implements it has to link against further libraries.
 *
 * <ul>
 * <li>client: DPWS client role
 *   <ul>
 *   <li>DPWS-C_LIBRARIES: not threadsafe </li>
 *   <li>DPWS-CMT_LIBRARIES: threadsafe </li>
 *   </ul>
 * </li>
 * <li>device: DPWS device role
 *   <ul>
 *   <li>DPWS-D_LIBRARIES: not threadsafe </li>
 *   <li>DPWS-DMT_LIBRARIES: threadsafe </li>
 *   </ul>
 * </li>
 * <li>peer: DPWS device role combined with client role
 *   <ul>
 *   <li>DPWS-P_LIBRARIES: not threadsafe </li>
 *   <li>DPWS-PMT_LIBRARIES: threadsafe </li>
 *   </ul>
 * </li>
 * <li>hosted: DPWS device role but only the hosted service part, discovery and
 *     description part is implemented by the hosting service tool.
 *   <ul>
 *   <li>DPWS-H_LIBRARIES: not threadsafe </li>
 *   <li>DPWS-HMT_LIBRARIES: threadsafe </li>
 *   </ul>
 * </li>
 * </ul>
 *
 * \section codegen Code generation
 *
 * \image html Designflow.png
 * \image latex Designflow.pdf "Code Generation Workflow" width=10cm
 *
 * WS4D-gSOAP uses a similar work-flow as gSOAP. To create
 * a DPWS device a developer has to specify a WSDL description of the services
 * on a device and the device’s metadata. The WSDL files are used for code
 * generation with the gSOAP code generator (soapcpp2). It converts XML schema
 * to a C data binding as well as stub and skeleton code for a specific gSOAP
 * service description. The XML schema to C data binding creates a mapping
 * from every type of the used XML schema definitions to a C type structure
 * and generates functions for the marshalling and demarshalling. The skeleton
 * and stub code generator finally maps WSDL operations to C functions. The
 * device metadata is used to generate code for service setup and assignment
 * of model metadata and device characteristics. With the resulting code a
 * developer can concentrate just on the implementation of the functionality of
 *the services hosted by a device.
 *
 * \section crosscompile Cross compilation
 *
 * WS4D-gSOAP supports cross compiling with the help of cmake and the toolchain
 * file mechanism (link). WS4D-gSOAP already supports several embedded systems
 * (see link trac-seite) with toolchain files located in the toolchain directory. With
 * this mechanism further cross compilers can be added.
 *
 * See the CROSSCOMPILE file in the source distribution for more information
 * about cross compiling.
 *
 * \section altio Embedded system support
 *
 * WS4D-gSOAP and gSOAP support several operating systems with POSIX like APIs
 * and BSD Socket like network API. Besides the network API gSOAP and WS4D-gSOAP
 * has only few dependency on system APIs. But there are systems that do not
 * offer POSIX and BSD Socket like APIs. For this case WS4D-gSOAP has a mode to
 * implement wrapper code to integrate other network and system APIs. This can
 * can be activated with the ALT_IO_MODE cmake variable.
 *
 * Currently WS4D-gSOAP supports the following  ALT_IO_MODEs:
 * <ul>
 * <li>none (default): no alternative mode, uses the POSIX and BSD Socket like
 * API.</li>
 * <li>lwip: uses the lwIP TCP/IP network stack for networking. </li>
 * <li>nb20: support for the system and network API of the netburner sdk. </li>
 * </ul>
 *
 * See src/alt-io/SYSTEM_SUPPORT for more information how to add new systems
 * APIs.
 *
 * \section ipv6 IPv6 support
 *
 * This mode is still experimental and not finished !!!
 *
 * WS4D-gSOAP supports IPv4 by default. IPv6 support can by activated by
 * enabling the cmake option WITH_IPV6. If this mode is activated devices and
 * clients support IPv4 and IPv6 communication.
 *
 * \section hostingService Hosting service
 *
 * \image html HostingService.png
 * \image latex HostingService.pdf "Hosting Service" width=10cm
 *
 * The hosting service is an alternative mode to build DPWS devices where the
 * building blocks of an DPWS device can be implemented and run separately.
 * DPWS devices consist of an hosting service that announces the availability
 * of hosted services and offers clients to retrieve device and service metadata
 * describing the device. This part of DPWS devices can be split into a daemon
 * process so that hosted services can be implemented and run independently from
 * a hosting services. Hosted services and the hosting service communicate with
 * a metadata repository. Whereas a hosted service can register its metadata at
 * the repository the hosting service observes the repository for new services
 * and announces them as part of a DPWS device in the network.
 *
 * The metadata repository is a file based repository. The hosting service
 * expects description files of the same format as used with the code generator.
 * I evaluates the hosted element and announces all services described with a
 * hosted element. The metadata repository is located inside the ws4d-gsoap
 * installation directory tree:
 * \verbatim
[WS4D-gSOAP Installation Directory]
 |-- etc
 |   |-- WS4D-gSOAP             (WS4D-gSOAP runtime configuration
 |   |   |                       directory)
 |   |   |-- announce.d         (Metadata repository directory of
 |   |   |                       hosting service. Active services are
 |   |   |                       described here)
 |   |   |-- conf.d             (Configuration of hosting service)
 |   |   |   |-- hosting_ip     (Interface of hosting service.
 |   |   |   |                   Automatically generated)
 |   |   |   |-- hosting_uuid   (UUID of hosting service)
 |   |   |   |-- hosting.xml    (Device description for hosted
 |   |   |   |                   service: ThisDevice and ThisModel)
 |   |   |   `-- start-stop-service  (Configuration for startup
 |   |   |                            scripts)
 |   |   |-- debug_service.sh        (Script for starting a hosted
 |   |   |                            service inside a debugger.  )
 |   |   |-- hostingservice_debug.sh (Script for starting the hosting
 |   |   |                            service inside a debugger.)
 |   |   |-- hostingservice_start.sh (Script for starting the hosting
 |   |   |                            service.)
 |   |   |-- hostingservice_stop.sh  (Script for stopping the hosting
 |   |   |                            service.)
 |   |   |-- init.d             (Directory contains scripts used by
 |   |   |                       the startup scripts to start and stop
 |   |   |                       services.)
 |   |   |-- meta.d             (Directory contains metadata files of
 |   |   |                       available services, that are copied
 |   |   |                       to the metadata repository be the
 |   |   |                       startup scripts.)
 |   |   |-- start_service.sh   (Script for starting a hosted
 |   |   |                       service.)
 |   |   `-- stop_service.sh    (Script for starting a hosted
 |   |                           service.)
 |   `-- [...]
 `-- [...]
\endverbatim
 *
 * An example for a hosted service project is in the documentation directory of
 * the WS4D-gSOAP installation under share/doc/ws4d-gSOAP/hosted-example.
 *
 * \section lcm Life cycle manager
 *
 * This optional feature is based on the hosting service feature, that separates hosted
 * services from the hosting service of an device and offers deployment of
 * services during the runtime of a device.
 *
 * \image html LifeCycleManager.png
 * \image latex LifeCycleManager.pdf "Life cycle manager" width=10cm
 *
 * All services under the control of the life cycle manager will
 * be called managed services. The life cycle manager is able to add, start,
 * stop, update and remove managed services during run time. The manager offers
 * a web service with the methods following methods:
 * <ul>
 * <li>install: TODO</li>
 * <li>uninstall: TODO</li>
 * <li>start: TODO</li>
 * <li>stop: TODO</li>
 * <li>update: TODO</li>
 * <li>resolve: checks if the dependencies of a service can be resolved</li>
 * </ul>
 *
 * The life cycle manager is platform independent and can be used on several
 * systems for example Linux, Windows and embedded systems like the Fox-Board.
 *
 * To install a new managed service, a service bundle is required. A service
 * bundle is a standard ZIP-archive and consists of the following parts:
 * <ul>
 * <li>service: the executable file including the functionality (more than one
 * file is possible to support multiple platforms)</li>
 * <li>WSDL: interface description of the service</li>
 * <li>XML: metadata description, fundamental for the hosting service to
 * announce the hosted service</li>
 * <li>scripts (resolve-, start-, stop- and isAlife script): to interact with
 * the hosted services, especial for starting and stopping</li>
 * <li>further files and directories can be added as needed</li>
 * </ul>
 *
 * The life-cycle-manager decompresses the archive and uses the scripts to
 * manage the service.
 *
 * TODO: describe LUA scripts
 *
 * An example for a managed service is part of the hosted service project in the
 * documentation directory of the WS4D-gSOAP installation under
 * share/doc/ws4d-gSOAP/hosted-example. The managed service part can be
 * activated with the cmake option LCM_PACKAGE. If this option is activated
 * the project will generate a managed service bundle.
 *
 * TODO: describe client.
 *
 * \section dpwssec DPWS security support
 *
 * WS4D-gSOAP partially supports the security feature of DPWS. At the moment
 * only the secure channel feature is supported. The secure channel consists of
 * Web Service messaging on top of TLS/SSL.
 *
 * The secure channel feature is based on gSOAPs OpenSSL-Support. Thus projects
 * that want to use this feature depend on OpenSSL. The secure channel support
 * can be activated for the hosting service and the hosted services separately.
 * The WS4D-gSOAP generates setup functions for the hosting service and the
 * hosted services specified in the metadata.xml file. Additionally there are
 * the same functions with a _sec postfix that have in integer parameter called
 * secured. If this parameter has the value 1 then the corresponding service
 * will be initialized as secured service and has an https://... address.
 *
 * TODO: add an example
 *
 * \section wsse WS-Security support
 *
 * This feature enables the support to use gSOAPs WS-Security plugin with
 * WS4D-gSOAP. It must be enabled at compile time of WS4D-gSOAP with the cmake
 * option WITH_WS_SECURITY.
 *
 * An example for a device using WS-Security can be found in the
 * documentation directory of the WS4D-gSOAP installation under
 * share/doc/ws4d-gSOAP/sec_tutorial.
 *
*/
