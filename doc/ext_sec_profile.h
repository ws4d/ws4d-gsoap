/*! \page ext_sec_profile Step by Step Airconditioner Tutorial with WS4D Security Profile
 *
 * This tutorial gives an introduction to the development of devices and
 * control points (clients) using the extended security profile for mobile
 * devices (or <i>WS4D Security Profile</i>).
 * First, a short introduction to the \ref SecArch is given. After that
 * the usage is described using the \ref SecSamples. If you haven't worked
 * with WS4D-gSOAP before, we strongly advise to start with the
 * \ref tutorial_page since this tutorial only covers security-related issues.
 *
 * <b>WARNING!</b> This implementation can be considered <b>pre-Alpha</b>!
 * Refer to \ref cpt_ToDo to figure out what this means.
 *
 * If you'd like to know what all this is good for, you may want to sneak in
 * \ref cpt_SampleRun to see how applications are enriched when implementing
 * the WS4D Security Profile.
 *
 * Once you worked through this tutorial, you may want to have a look at the
 * source code in qt4-sample. This way you can learn how to use WS4D-gSOAP as
 * well as the WS4D Security Shell in Qt4 (or just C++ based) applications. It
 * also covers how to implement and plug in your own handlers e.g. for
 * displaying and entering PINs or make authorization decisions.
 *
 * \section SecArch WS4D Security Profile and its implementation's architecture
 *
 * This is only a short introduction. For a thorough description of the profile
 * and the implementation's architecture, see \ref ref1 "[1]" and \ref ref2 "[2]".
 *
 * The key aspects of the WS4D Security Profile are
 *
 * <ul>
 * <li>direct authentication between devices, no need for a third party (e.g.
 *     a Certificate Authority)</li>
 * <li>configurable for different security needs regarding confidentiality and
 *     integrity for every Hosted Services and - if necessary - for each
 *     service's methods</li>
 * <li>thus, support of mechanisms to provide integrity and confidentiality
 *     different from TLS/SSL
 * </ul>
 *
 * Furthermore, the WS4D Security Profile's implementation offers
 * <ul><li>An authorization subsystem on service- / application level</li></ul>
 *
 * \subsection cpt_Overview Overview
 *
 * The complete architecture is depicted in the following image.
 *
 * \image html secArchBWen.png
 * \image latex secArchBWen.pdf "The Security Architecture" width=15.5cm
 *
 * In the following, the architecture's implementation is referred to as the
 * <i>Security Shell</i>. The single components are now discussed in the
 * context of their usage.
 *
 * \subsection cpt_SecurityEngine Security Engine
 *
 * The central component is the Security Engine. To provide a high level of
 * flexibility and maintainability, the Security Engine basically provides
 * a plugin socket for all the other components and thus interconnects them.
 * The other components don't communicate directly with each other
 * but always through the Security Engine's interfaces. This way, it is easy
 * to exchange single components and to adjust them to a use case's needs.
 *
 * \subsection cpt_Authentication Authentication
 *
 * Before Hosted Services or their methods may be accessed by clients, a client
 * needs to be authenticated. To this purpose, the Security Shell adds an
 * additional Hosted Service to the Device, the so called <i>Authentication Service</i>.
 * The Authentication Service provides an interface to the <i>Authentication Engine</i>.
 * Together, these two components build the authentication subsystem. By accessing
 * the authentication subsystem, a client can initiate exchange and validation
 * and therefore authentication of X.509-certificates. The Authentication
 * Engine itself relies on a certain mechanism for authentication e.g. sharing
 * a short one-time-password. Authenticated certificates are stored in the
 * <i>Certificate Database</i>.
 *
 * Regarding the messaging phases of DPWS, one can consider authentication as an
 * additional phase:
 *
 * \image html DPWS-PhasesBWen.png
 * \image latex DPWS-PhasesBWen.pdf "Communication Phases of DPWS" width=10.0cm
 *
 * Authentication takes place after Discovery and Description. However, this way
 * it can not be assured that the previous Discovery and Description actions
 * led to trustworthy information. Thus, Discovery and Description have to be
 * verified after Authentication (e.g. by repeating the phases or by
 * retroactively verifying signatures of exchanged messages basing on the
 * exchanged certificates)
 *
 * \subsection cpt_Invocation Invocation of Services / Methods
 *
 * After certificates of the communicating parties have been authenticated, a
 * client may invoke a service's method or register to event sources. With an
 * incoming request, the <i>Security Handler</i> first checks, whether the
 * requests meets the destination's (service, method or event source) security
 * needs. These security needs are defined by a service's Policies at design
 * time and stored in the <i>Security Configuration Database</i>. After that,
 * the Security Handler verifies embedded signatures, decrypts encrypted
 * message parts or any other security-related task. The Security Handler's
 * goal is to convert secured to unsecured contexts and vice versa. This means,
 * that no security-related logic needs to be integrated into the Hosted
 * Service's implementation. For outgoing messages (responses, events), the
 * Security Handler signs or encrypts messages, depending on the specified
 * constraints.
 *
 * \subsection cpt_Authorization Authorization
 *
 * Since Authorization happens on service- / application level, it is not part
 * of the WS4D Security Profile. However, the described architecture contains
 * a proposal for an authorization subsystem.
 *
 * At design time, an application developer can set <i>static</i> authorization
 * tokens, that are saved in the <i>Authorization Database</i>. These tokens
 * basically contain a proof (e.g. possession of a certain certificate,
 * possession of a certificate signed by a certain CA, knowledge of a shared
 * secret, etc...), a resource to access (service, method, event source) and
 * a decision (access granted / denied). A special authorization token is a
 * <i>dynamic authorization token</i>. It allows a device's user at <i>run
 * time</i> to decide whether an incoming request gets authorized or not.
 *
 * \section cpt_Stack Build WS4D-gSOAP with WS4D Security Shell
 *
 * Before developing and building applications using the WS4D Security Shell,
 * WS4D-gSOAP has to be recompiled. For the stack as well as the applications
 * to build, you need openssl <b>as well as its header files</b> (try
 * libssl-dev) installed.
 * To activate the Security Shell, re-run cmake with the additional parameters
 *
 * \verbatim
 -DWITH_WS4D_SECURITY_SHELL -DWITH_WS_SECURITY \endverbatim
 *
 * Then, again run
 *
 * \verbatim
 $ make
 $ make install \endverbatim
 *
 * \section SecSamples Secure Airconditioner Samples
 *
 * Now that you have a basic idea of how the Security Shell works, let's have
 * a look on how to use it. The following samples are based on the simple
 * air conditioner device as well as the simple GetStatusClient and the
 * simple SetTargetTemperatureClient. They are described in \ref tutorial_page.
 *
 * \subsection SecDevice Secure Airconditioner Device
 *
 * To understand how to set up and configure the WS4D Security Shell, you can
 * examine the delivered samples in
 * share/doc/ws4d-gSOAP/ext_sec_profile-samples/secure_airconditioner which base
 * on the simple-sample-application series. We start with the simple
 * airconditioner device which will be extended to the secured airconditioner
 * step by step.
 *
 * First, we need the following additional include files:
 * \code
#include "smdevp.h"
#include "wsseapi.h"
#include "authentication_service.h"
#include "security_engine.h" \endcode
 *
 * While the former two are needed for the WSSE-Plugin of gSOAP, the latter two
 * are related to the Security Shell. Next, we need an additional soap handle
 * for the Authentication Service. Since a soap handle can only carry a single
 * security configuration (e.g. sign messages / verify signatures) we also need
 * an additional handle for the Hosting Service (signatures don't work yet for
 * Discovery and Description, see \ref cpt_ToDo):
 * \code
struct soap hosting_service;
struct soap authentication_service; \endcode
 *
 * Before getting into action, we add a bunch of forward declarations using the
 * following macro for convenience. Besides, we need an instance of a Security
 * Engine:
 * \code
EXTERN_INIT_TABLE("ws4d")
struct security_engine_s sec_engine; \endcode
 *
 * There we go. First, initialize the addtional soap handles together with the
 * original one:
 *
 * \code
  soap_init (&service);
  soap_init (&hosting_service);
  soap_init (&authentication_service);
#ifdef DEBUG
  soap_set_omode (&service, SOAP_XML_INDENT);
  soap_set_omode (&hosting_service, SOAP_XML_INDENT);
  soap_set_omode (&authentication_service, SOAP_XML_INDENT);
#endif
  soap_set_namespaces (&service, acs1_namespaces);
  soap_set_namespaces (&hosting_service, acs1_namespaces);
  wsa_register_handle(&hosting_service); \endcode
 *
 * There is no need to assign namespaces to the authentication service. This
 * is done later automatically by the framework. Now, you initialize and
 * configure the Security Engine. The first steps are related to the
 * gSOAP-WSSE-Plugin and give paths to Certificate files as well as the
 * password to access the private key:
 * \code
security_engine_init(&sec_engine);
sec_engine.cafile = SECSHELLPATH "/certs/demoCA/cacert.pem";
sec_engine.certfile = SECSHELLPATH "/certs/myDeviceCert.pem";
sec_engine.keyfile = SECSHELLPATH "/certs/myDeviceKey.pem";
sec_engine.keypassword = "password";
sec_engine.cert_list_location = "device.cdb"; \endcode
 * The last parameter gives the location of the Certificate Database. Don't
 * wonder about that unknown constant SECSHELLPATH - we'll come to that later.
 *
 * Now, we plug in the authentication- and authorization components into the
 * Security Engine. To this purpose, we use the forward declaration generated
 * by the macro earlier.
 *
 * \code
ws4d_init_authentication_engine(&sec_engine);
ws4d_init_certificate_db(&sec_engine);
ws4d_init_authorization_engine(&sec_engine);
ws4d_init_authorization_db(&sec_engine); \endcode
 *
 * Every soap handle has its own Security Engine as well as it own Security
 * Handler. However, the same instance of a Security Engine can be used for
 * several soap handles. This means, that all handles using the same Security
 * Engine have the same security configuration. The Security Handler is a
 * gSOAP-plugin. This way, it can check authentication, authorization, etc...
 * immediately after a message is received.
 * \code
soap_register_plugin_arg(&service, sh_plugin, &sec_engine);
soap_register_plugin_arg(&authentication_service, sh_plugin, &sec_engine); \endcode
 *
 * Since signing messages and verifying signatures is currently done by the
 * gSOAP-WSSE-PlugIn, we need to initialize it here.
 * \code
soap_register_plugin_arg(&service, soap_wsse,
					   sec_engine.cdb_wsse_token_handler);
service.cafile = sec_engine.cafile; \endcode
 *
 * The additional parameter of soap_register_plugin_arg() provides a way to
 * look up previously authenticated certificates in the Certificate Database
 * basing on a reference which was exchanged between device and client upon
 * authentication. This way, the certificate doesn't need to be embedded into
 * every subsequent request but can be replaced by the (much smaller)
 * reference. Finally, we switch on a couple of features such as signing
 * messages and to allow only authenticated and authorized requests:
 * \code
soap_wsse_verify_init(&service);
soap_wsse_verify_auto(&service, SOAP_SMD_NONE, NULL, 0);
sh_insist_on_authentication(&service, 1);
sec_engine.authe_activate_authorization(&service); \endcode
 *
 * Since we use a dedicated soap handle for the Hosting Service, the
 * corresponding call must be adjusted. At this time, we can also set up the
 * Authentication Service.
 * \code
if (acs_setup_HostingService (&device, &hosting_service, uuid, 100))
  {
    fprintf (stderr,
             "Can't init service. Something wrong with interface uuid\n");
    USAGE dpws_done (&device);
    exit (1);
  }
if (setup_AuthenticationService(&device, &authentication_service, AUTHENTICATIONSERVICE_WSDL, 100))
  {
    fprintf(stderr,
            "\nAuthentication Service: Error in setting up Service!\n");
    dpws_done(&device);
    return 1;
  }\endcode
 *
 * Basically the last thing to do is to allow dynamic authorization for the
 * Hosted Service's two methods.
 * \code
sec_engine.authe_set_dynamic_authorization(
			  &sec_engine, (void*) "http://www.ws4d.org/axis2/tutorial/AirConditioner/GetStatusIn",
			  PERMITTED, METHOD);
sec_engine.authe_set_dynamic_authorization(
			  &sec_engine, (void*) "http://www.ws4d.org/axis2/tutorial/AirConditioner/SetTargetTemperatureIn",
			  PERMITTED, METHOD);\endcode
 *
 * By using granularity METHOD, setting and getting the temperature can be
 * authorized independently from each other. By using granularity SERVICE, every
 * method of a Hosted Service has the same authorization configuration (e.g.
 * once the user allowed a client to <b>get</b> the temperature, the client is
 * also allowed to <b>set</b> it).
 *
 * Last thing to do is adjusting the message reception loop:
 * \code
  for (;;)
    {
      struct soap *handle = NULL, *soap_set[] = SOAP_HANDLE_SET (&service, &hosting_service, &authentication_service);
      int (*serve_requests[]) (struct soap * soap) =
        SOAP_SERVE_SET (acs1_serve_request, auths_serve_request);
//                          [...]
      handle = dpws_maccept (&device, 100000, 3, soap_set);

      if (handle)
        {
//                          [...]
          if (dpws_mserve (handle, 2, serve_requests))
//                          [...]
          soap_end (handle);
        }
    }\endcode
 *
 * Technically, we are done now - at least we were, if the implementation was
 * feature-complete. Unfortunately, it isn't yet, so we have to take a little
 * more effort. Jump to one of the service's methods' implementation (e.g.
 * __acs1__GetStatus()). The Security Handler should keep security-related code
 * out of the service's implementation. Regarding authentication and
 * authorization it does its task pretty well. However, the gSOAP-WSSE-Plugin is
 * supposed to be replaced in future so it is not integrated into the Security
 * Handler yet. This means, we have to take care for checking if an incoming
 * message is actually signed ourselves and we have to sign the response and
 * embed the certificate reference.
 *
 * First we need a reference to the service's Security Handler as this way,
 * we can access the service's Security Engine which contains the file paths we
 * need to know.
 * \code
struct sh_plugin_data* data = (struct sh_plugin_data*)
                              soap_lookup_plugin(soap, SH_PLUGIN_ID);\endcode
 *
 * Now, let's check whether the message's <body> was included in the signature
 * (if the signature was invalid, gSOAP would have rejected the message
 * already).
 * \code
if (soap_wsse_verify_body(soap))
  {
    fprintf(stderr, "Signature does not include the body\n");
    soap_wsse_delete_Security(soap);
    return soap_sender_fault(soap, "Service operation not signed", NULL);
#ifdef DEBUG
  }
else
  {
    printf("Signature includes body - all fine.\n");
#endif // DEBUG
  }\endcode
 *
 * Now, we obtain our private key so we can later sign the outgoing message
 * with it.
 * \code
if (rsa_private_key == NULL)
{
  if ((fd = fopen(data->sec_engine->keyfile, "r")) == 0)
	{
	  fprintf(stderr, "ERROR: pem file not found\n");
	  return 0;
	}
  rsa_private_key = PEM_read_PrivateKey(fd, NULL, NULL, (void *)data->sec_engine->keypassword);
  fclose(fd);
  if (!rsa_private_key)
	{
	  fprintf(stderr, "ERROR: reading private key failed\n");
	  return 0;
	}
}\endcode
 *
 * Client and Device use the same certificate reference. You can think of it as
 * some sort of communication session key. Thus, we obtain this reference from
 * the incoming request's header:
 * \code
client_cert_ref= (char *)soap_wsse_get_KeyInfo_SecurityTokenReferenceURI(soap);\endcode
 *
 * Eventually all the loose ends lead together: Embed the Certificate Reference
 * into the response and sign the message with the private key we obtained
 * earlier.
 * \code
if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509(soap, client_cert_ref))
  {
    printf("ERROR: add reference failed\n");
    return SOAP_ERR;
  }

if (soap_wsse_sign_body(soap, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
  {
    printf("sign body failed\n");
    return SOAP_ERR;
  }\endcode
 *
 * Now, that's <b>really</b> been it now (except for that you have to repeat
 * the last part for every method).
 *
 * \subsection SecClient Secure Airconditioner Clients
 *
 * We will discuss client development by examining the simple GetStatus_client
 * in the same directory. Regarding the Security Shell, the only difference
 * between GetStatus_client and SetTargetTemperature_client is, that they use
 * different certificates so the two clients have different identities.
 *
 * Implementing a client is pretty much the same as implementing a device.
 * However, there is no need to include the authorization subsystem as the
 * client has no use for it. Besides, a client doesn't need to offer an
 * Authentication Service but needs the facility to use it. Thus we will only
 * discuss the differences.
 *
 * These differences start with which files to include and the necessary
 * soap-handles:
 * \code
#include "smdevp.h"
#include "wsseapi.h"
#include "security_engine.h"
#include "authentication_service_usage.h"
//                 [...]
struct soap client;
struct soap auth_client; \endcode
 * authentication_service_usage.h provides few api-calls to trigger
 * authentication. Like in the Device, we need different soap-handles for
 * invoking Hosted Services and invoking the Authentication Service due to
 * different Security Configurations.
 *
 * Setting up and configuring the Security Engine as well as the Security
 * Handler is the same as in the Device and thus not discussed here. The next
 * interesting part is to initiate authentication. The API provides you with
 * the facility to do the following:
 * \code
if (err = auths_authenticate_device(&auth_client, &device, &dpws, NULL, &sec_engine))
  {
    printf("simple_client: Error in Authenticating Device\n");
    return 1;
  }
#ifdef DEBUG
else
  {
    printf("simple_client: Device successfully authenticated\n");
#endif //DEBUG
  }\endcode
 *
 * This single call searches for an Authentication Service at the depicted
 * device and once it found one, it starts authentication. As soon as this
 * call returns without an error message, client and device are successfully
 * authenticated.
 *
 * Basically, that's it already. Well unless signing messaged would be
 * integrated in the Security Handler already. Here the only difference
 * compared to how it works in a Device is, that the reference is looked up
 * using the device's UUID which was saved in the Certificate Database during
 * authentication:
 *
 * \code
if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509(&client,
    sec_engine.cdb_lookup_cert_reference(&sec_engine, device.Address)))
  {
    fprintf(stderr, "ERROR: add reference failed\n");
    exit(1);
  }\endcode
 *
 *
 *
 * \subsection cpt_BuildSamples Build the sample applications.
 *
 * The source code is complete now, so let's have a look at the build system.
 * When you examine CMakeLists.txt in the sample directory you get a good idea
 * of the basic tricks.
 *
 * First thing to mention is the additional INCLUDE_DIRECTORY
 * \verbatim
${GSOAP_INCLUDE_DIR}/plugin \endverbatim
 * It contains the header files smdevp.h and wsseapi.h. Next thing to mention
 * is a couple of libraries you will need:
 * \verbatim
# set libraries
SET(DPWS_AUTHS_SERVICE ${DPWS_LIB_DIR}/libauthsS.a)
SET(DPWS_AUTHC_SERVICE ${DPWS_LIB_DIR}/libauthsC.a)
SET(WSSEAPI_LIB  ${DPWS_LIB_DIR}/libwsseapi.a) \endverbatim
 * The former two are libraries to offer and access an Authentication Service
 * respectively. (S - Service, C - Client). The latter one is a patched version
 * of gSOAP's wsseapi.c - the implementation of the WSSE-Plugin - as well as
 * few other source files you always need when building applications using the
 * gSOAP WSSE Plugin. We packed them into one library to link against for
 * convenience.
 *
 * Now let's have a look at
 * \verbatim
# path to Security Shell files
SET(SECSHELLPATH ${DPWS_DIR}/share/doc/ws4d-gsoap/ext_sec_profile-samples/SecurityShell CACHE PATH "Security Shell path") \endverbatim
 *
 * As mentioned earlier, the Security Engine is merely a plugin socket which
 * interconnects all the other components. Besides the Authentication-Service
 * libraries, the security engine is the only component which is actually an
 * integral part of WS4D-gSOAP. All the other components are included when
 * compiling the application. This means that it is very easy to exchange
 * single components (e.g. you're not happy with saving authenticated
 * certificates in a plain text file - then you can plug in your own
 * implementation of a Certificate Database). We provide you with a full
 * implementation of the WS4D Security Shell in SECSHELLPATH/src. For your
 * convenience, we also ship a bunch of certificates (one for a device and one
 * for each client) as well as a shiny Certificate Authority (CA) so you can
 * create new certificates. <b>BUT:</b> The certificates, private keys and the
 * CA are <b>for testing purposes only!</b> Be aware that, when going to the
 * wild with your applications, it's absolutely vital to <b>keep your secret
 * keys secret</b> (the shipped keys are obviously not).
 *
 * Well then, let's see how to build the applications. The Device needs all
 * the sources of the security shell:
 * \verbatim
SET(simple_arcondtitioner_SRCS
	simple_airconditioner.c
	${GSOAP_STDSOAP2_SOURCE}
	${gen_DIR}/acs1ServerLib.c
	${gen_DIR}/acs_metadata.c
	${gen_DIR}/acs_wsdl.c
	${SECSHELLPATH}/src/authentication_engine.c
	${SECSHELLPATH}/src/cert_db.c
	${SECSHELLPATH}/src/authorization_engine.c
	${SECSHELLPATH}/src/authorization_db.c
	${SECSHELLPATH}/src/security_handler.c
	${SECSHELLPATH}/src/crypto_engine.c) \endverbatim
 *
 * Besides, it needs a couple of additional compile flags and the
 * libraries we discussed earlier:
\verbatim
EXTEND_TGT_COMPILE_FLAGS(acs_simple_device FLAGS "-DDPWS_DEVICE -DWITH_OPENSSL -DWITH_DOM")

# link the device specific dpws libraries into the airconditioner executable
TARGET_LINK_LIBRARIES(acs_simple_device ${DPWS_LIBRARIES} ${DPWS-D_LIBRARIES}
                                        ${WSSEAPI_LIB} ${DPWS_AUTHS_SERVICE}
                                        -lssl -lcrypto) \endverbatim
 *
 * Building the client applications is pretty much the same. There's no need
 * to include the authorization-related files. Also, a client doesn't need to
 * offer an Authentication Service but the facility to access it. So we don't
 * link against DPWS_AUTH<b>S</b>_SERVICE but DPWS_AUTH<b>C</b>_SERVICE:
 *
 * \verbatim
SET(GetStatus_client_SRCS
	GetStatus_client.c
	${GSOAP_STDSOAP2_SOURCE}
	${gen_DIR}/acs1ClientLib.c
	${SECSHELLPATH}/src/authentication_engine.c
	${SECSHELLPATH}/src/cert_db.c
	${SECSHELLPATH}/src/security_handler.c
	${SECSHELLPATH}/src/crypto_engine.c)

ADD_EXECUTABLE(acs_GetStatus_client ${GetStatus_client_SRCS})

EXTEND_TGT_COMPILE_FLAGS(acs_GetStatus_client FLAGS
                         "-DDPWS_CLIENT -DWITH_OPENSSL -DWITH_DOM")

TARGET_LINK_LIBRARIES(acs_GetStatus_client ${DPWS_LIBRARIES} ${DPWS-C_LIBRARIES}
                                           ${WSSEAPI_LIB} -lssl -lcrypto
                                           ${DPWS_AUTHC_SERVICE})\endverbatim
 *
 * Well, this is all you need to know. Run (c)cmake and make and there you go.
 *
 * \section cpt_SampleRun Running the sample applications
 *
 * To figure out what all this was good for, let's run the applications. First,
 * let's start the device:
 *
 * \verbatim
$ ./bin/acs_simple_device -i 192.168.0.104 -u urn:uuid:c5bec2e8-e60d-4807-9873-c2eaaa6e725a

Airconditioner: Set interface to "192.168.0.104"

Airconditioner: Set uuid to "urn:uuid:c5bec2e8-e60d-4807-9873-c2eaaa6e725a"
libauthsS: registered wsse-plugin
Adding new dynamic auth-entry permission for http://www.ws4d.org/axis2/tutorial/AirConditioner/GetStatusIn
authorization_db: saved new authorization:
        cred-type: 0
        ressource: http://www.ws4d.org/axis2/tutorial/AirConditioner/GetStatusIn
        validity: 0 / Thu Jan  1 01:00:00 1970

Adding new dynamic auth-entry permission for http://www.ws4d.org/axis2/tutorial/AirConditioner/SetTargetTemperatureIn
authorization_db: saved new authorization:
        cred-type: 0
        ressource: http://www.ws4d.org/axis2/tutorial/AirConditioner/SetTargetTemperatureIn
        validity: 0 / Thu Jan  1 01:00:00 1970


Airconditioner: ready to serve... (Ctrl-C for shut down)

Airconditioner: waiting for request \endverbatim
 * If you activated the DEBUG flag, you can see status reports on the
 * activation of dynamic authorization for the methods GetStatusIn and
 * SetTargetTemperatureIn. Now, let's start the GetStatus_client:
 * \verbatim
$ ./bin/acs_GetStatus_client -i 192.168.0.104 -d urn:uuid:c5bec2e8-e60d-4807-9873-c2eaaa6e725a

simple_client: Set interface to "192.168.0.104"

simple_client: Set device address to "urn:uuid:c5bec2e8-e60d-4807-9873-c2eaaa6e725a"

simple_client: device urn:uuid:c5bec2e8-e60d-4807-9873-c2eaaa6e725a found at addr http://192.168.0.104:45912/c5bec2e8-e60d-4807-9873-c2eaaa6e725a

libauthsC: device offers AuthenticationService at http://192.168.0.104:60369/AuthenticationService
libauthsC: registered wsse-plugin

libauthsC: got response from http://192.168.0.104:60369/AuthenticationService
libauthsC: Signature includes body!
Okay, need pin!
 > \endverbatim
 * The first half of the authentication process is already done now - when you
 * switch back to the device, you will see that it generated a short one time
 * pin:
 * \verbatim
Airconditioner: processing request from 192.168.0.104:56797
libauthsS: Signature includes body!
New Certificate - will save it

Generated PIN (>>>PIN<<<):
        >>>L(m//R<<< \endverbatim
 * As soon as you enter this PIN in the client application, authentication
 * completes:
 * \verbatim
Okay, need pin!
 > L(m//R
L(m//R

libauthsC: got response from http://192.168.0.104:60369/AuthenticationService
libauthsC: Signature includes body!
libauthsC: received Reference on my certificate:
        <<http://ws4d.org/certificate-reference/tmscFi\rsXICcXo_CCM]obcIhvINPKAAws[uYwdQTlKnArJDtV`aoziTvjZCtZCiJ^[bZDjeguPhdYcUfzuZy\mlKL>>
saved certificate reference:
        <urn:uuid:c5bec2e8-e60d-4807-9873-c2eaaa6e725a>:<http://ws4d.org/certificate-reference/tmscFi\rsXICcXo_CCM]obcIhvINPKAAws[uYwdQTlKnArJDtV`aoziTvjZCtZCiJ^[bZDjeguPhdYcUfzuZy\mlKL>
simple_client: Device successfully authenticated

simple_client: device offers AirConditionerService at http://192.168.0.104:55855/AirConditioner \endverbatim
 * Still, we did not get a response from the device. Let's switch back to the
 * device and see what happened:
 * \verbatim
A Client identified as
-------------------------------------------
DE
Bavaria
Munich
ws4d.org
org.ws4d.client.unger.sebastian
sebastian.unger@uni-rostock.de
-------------------------------------------
wants to access ressource "http://www.ws4d.org/axis2/tutorial/AirConditioner/GetStatusIn"
Do you want to grant access?
        0 - No! Deny access!
        1 - Yes, but only once!
        2 - Yes, but only for the next 30 seconds!
        3 - Yes, permanently
 > \endverbatim
 * Now, as soon as you make your decision, things proceed. Besides - by various
 * status messages - you might have realized, that both applications
 * continuously checked whether the received messages were signed.
 *
 * \section cpt_ToDo To Do and open issues
 *
 * To show that this implementation is really in pre-alpha status, here are
 * (only few) of the most serious issues (in a truely unordered list):
 * <ul>
 * <li>When the stack is built with the Security Shell, it's no longer possible
 *     to build applications not using it (but we're working on it!)</li>
 * <li>WS-DD Compact Signatures not supported yet (but a basic reference
 *     mechanism is already implemented)</li>
 * <li>Various memory leaks</li>
 * <li>If secured eventing is desired, it works only for a single eventing
 *     device</li>
 * <li>Configuring different security needs not supported yet</li>
 * <li>Encryption on message level (XML-Encryption) not yet supported</li>
 * <li>Signing messages doesn't work yet for Discovery and Description</li>
 * <li>... many many more issues</li>
 * </ul>
 *
 *
 * \anchor ref1 [1] Sebastian Unger, Elmar Zeeb, Holger Grandy, Frank Golatowski, Dirk
 * Timmermann: "Extending the Devices Profile for Web Services for Secure
 * Mobile Device Communication". 4th International Workshop on Trustworthy
 * Internet of People, Things & Services, Tokyo/Japan, November 2010
 *
 * \anchor ref2 [2] Sebastian Unger: "Sichere Service Schnittstellen für vernetzte
 * Automotive Applikationen". Diplomarbeit an der Universität Rostock, Mai 2010
 */
