/*! \mainpage Introduction

This is the documentation of the WS4D-gSoap DPWS toolkit.

WS4D-gSOAP is an extension of the well known gSOAP Web services
toolkit, a toolkit for building SOAP-based Web services with C/C++
developed by Robert A. van Engelen. It is designed to develop
small footprint and high throughput Web services.

WS4D-gSOAP uses a similar workflow as gSOAP. To create a DPWS device a
developer has to specify a WSDL description of the services on a
device and the device's metadata. The WSDL files are used for code
generation in gSOAP's typical way. The device metadata is used to
generate code for service setup and assignment of model metadata and
device characteristics. With the resulting code a developer can
concentrate just on the implementation of the functionality of the
services hosted on a device.

The toolkit uses gSOAP's plug-in mechanism to implement WS-Addressing,
WS-Discovery, WS-MetadataExchange / WS-Transfer and WS-Eventing on top
of gSOAP. WS4D-gSOAP supports three different roles for an endpoint
implementation that can be switched at compile time by including a
specific header file: device (dpws_device.h), client (dpws_client.h)
and peer (dpws_peer.h). With the device role an endpoint implements
the device side of the specification. The client role is used to
create code for a Web service client, respectively. The peer role has
to be used when both client and device are about to be integrated in
one application.

WS4D-gSOAP offers multi-platform support such as the Linux i386,
Windows- native, Windows-cygwin and embedded Linux (FOX Board [Acm07]
and Nokia Maemo [Mae07]) platforms. To develop devices a typical GNU
software development toolchain can be used. Developers preferring
integrated development environments can use Visual Studio 8.0 on
Windows or Eclipse on other platforms.

This document doesn't include instructions to install WS4D-gSOAP. See the
INSTALL file in the source distribution for more information.

This document has the following structure. The first chapter \ref dpws_page
introduces the standard that WS4D-gSOAP implements. The next chapter consists of a
\subpage tutorial_page that is a good starting point for learning how to
implement dpws devices and clients with WS4D-gSOAP. Chapter \subpage features
describes the features of WS4D-gSOAP. The following chapters contain the reference
manual for WS4D-gSOAP and address specific aspects of the WS4D-gSOAP api and how
to use them in an applications.

If you are interested in using the prototype implementation of the WS4D
Security Profile, you may want to read \subpage ext_sec_profile.
*/
