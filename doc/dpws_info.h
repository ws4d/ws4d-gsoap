/*! \page dpws_page Devices Profile for Web Services
The Devices Profile for Web Services (DPWS) standard was developed to
enable secure Web service capabilities on resource-constraint
devices. It features secure exchange of messages with Web services,
dynamic discovery and description of Web services, and subscribing to,
and receiving events from a Web service.

\image html DPWSProtocolStack.png
\image latex DPWSProtocolStack.pdf "The Devices Profile for Web Services protocol stack" width=10cm

As shown in the picture, DPWS is based on well known protocols and
several Web service specifications. It employs similar messaging
mechanisms as the Web Services Architecture (WSA) with restrictions to
complexity and message size. On top of the low level
communication foundations like IP- Uni- and Multicasting, TCP and HTTP
it uses SOAP-over-UDP, SOAP, and XML Schema for the actual message
exchange. WS-Policy and WS-Addressing are on top of the
messaging layer. WS-Policy is used to exchange and negotiate
policies and parameters required for service usage. WS- Addressing
separates the SOAP messaging layer from its close binding to HTTP as
defined in the SOAP specification. It introduces the concepts of
message information headers and endpoint references making service
users, providers and transmitted messages uniquely identifiable and
addressable..

In general, DPWS is very similar to UPnP while the main difference is
the direct alignment of DPWS to the latest Web Service specifications. In
UPnP the referenced specifications are not updated.

DPWS defines a client role that uses the features that are described in the
following paragraphs and a device role that implements these features.

\section addressing Addressing

Similar to UPnP addressing, the foundation of DPWS is IPv4/IPv6 addressing.
DPWS assumes that a device has obtained a valid IP address. DPWS refers to DHCP
and IPv6 auto configuration as mechanisms to obtain valid IP addresses.

\section discovery Discovery

The discovery mechanisms enables devices to announce their availability in the
local network with IP multicast messages. Clients can listen for this
messages or send messages to search for devices in the network. All discovery
messages can contain device type and device scope information. A device type is a
unique identifier that classifies a device defined at design time. For example
printer device or scanner device are device types. The meaning of device types
should be specified at design time in separate specifications comparable to a
UPnP DCP. A device can support (implement) several device types. In
contrast to device types, a device scope is a classification that can be
configured at runtime. Room 1227 would be an example for a scope to identify all
devices that are located in room 1227.

An important feature that was introduced with version 1.1 of DPWS and
WS-Discovery is the discovery proxy. This is an alternative mode to the
regular mode for discovery based on multicast messages. When clients detect a
discovery proxy in the network they switch to managed mode an send discovery
requests to the discovery proxy. The discovery proxy reduces multicast messages
in the network and can integrate other discovery and directory services into
DPWS and WS-Discovery.

\section description Description

The description mechanism of DPWS enables the dynamic description of device
metadata such as hosted services, device information, model information or
service description. This metadata is associated with a metadata version number
that is distributed in discovery messages: Thereby clients can track changes of
the device description. The interfaces to retrieve the device description are
based on WS-Metadataexchange. As specified in WS-Metadataexchange, metadata is
partitioned into sections. DPWS defines which endpoints of an device should
provide at least which metadata sections. Custom metadata sections can be
specified to extend the device description data for custom applications.

\section control Control and Eventing

To control devices DPWS offers services with operations and events. Operations are
the same as SOAP Web Services operations, whereby the services hosted on a
DPWS device are regular SOAP Web Services. Events are controlled with
WS-Eventing and represented as inverse SOAP Web Service operations. This means
that client and service exchange roles and the message exchange is triggered by
the device. To subscribe for an event, a client can send a subscribe message to
the service endpoint. The subscribe message contains the requested delivery mode
and event filter. With the delivery mode mechanism a client can negotiate a
suitable delivery mechanism. DPWS defines the delivery push mode that sends
the events to an endpoint specified in the delivery mode. Further application
specific delivery modes can be defined. A dedicated event filter specifies which
events the device should send. DPWS defines the action filter. SOAP and
WS-Addressing define actions (identifiers) for operations. Thus event filters can
be applied and mapped directly to the corresponding inverse operations. Like the
flexibility to define own delivery modes, also own event filters can be used to
meet applications needs. To transport binary data and other data formats than
XML, DPWS includes the MTOM attachment mechanism to attach arbitrary data to
SOAP messages.

\section presentation Presentation

The presentation is an alternative way to retrieve information about a
device and control a device. In DPWS the presentation is announced as part
of the device description. It is announced as an HTTP IRI and can be used with a
regular web browser.

\section security Security

The DPWS specification defines the concept of security profiles. One such
security profile is defined in the specification. Devices can support this
security profile or others not specified in DPWS. As security requirements
heavily depend on application scenarios, the security profile targets
easy and lightweight implementation than wide coverage of possible security
requirements. Thus the security profile defined in DPWS consists of two main
features: secured discovery and secure channel. The discovery multicast messages
are secured with WS-Discovery compact signatures. These signatures are similar
to XML signatures as defined WS-Security with less overhead. The secure channel
provides encryption on transport layer based on SSL/TLS. Via the secure channel
DPWS provides authentication of clients and services, message integrity and
confidentiality. A device indicates the support for DPWS security with an HTTPS
scheme IRI for its transport address.

\section extensibility Extensibility and Composability

A big advantage of DPWS is the extensible and composable nature of
its specification and Web services in general. Web services mostly define
interfaces and technology independent mechanisms. So an implementer is free to
implement these interfaces in a way to meet the requirements of a specific
application scenario but can still provide interoperability with other endpoints.
It is up to the implementer to implement DPWS in a specific way. At several
positions in the specification as for example in the discovery part DPWS
provides several options for implementers. In the case of discovery DPWS
defines a generic discovery mechanism to discover devices in a subnet that offers
high dynamics but may produce high network traffic. However if DPWS clients
can discover device by other means like static configuration or a more static
discovery schema they are still within the scope of the DPWS specification.

How certain features of DPWS are implemented causes different properties of
the resulting system concerning performance, scalability, extendibility,
interoperability, etc. Hence an implementer should further profile the features
of DPWS to meet application scenario specific requirements. This could be
done in DCP specification where the semantic of DPWS device types are
described. Such specifications are application scenario specific and can combine
further Web service specifications with DPWS.

\section dpwsVersion Versions

The DPWS version 1.0 was initially published in May 2004 at
http://schemas.xmlsoap.org/ and was submitted for standardization to OASIS
in July 2008. Within WS-DD TC DPWS 1.1 was approved as OASIS Standard
together with WS-Discovery 1.1 and SOAP-over-UDP 1.1 on June 30, 2009. At the
moment the WS-DD TC is working on DPWS 1.2 and also the 1.2 versions of
WS-Discovery and SOAP-over-UDP.

DPWS versions are not downwards compatible. Each DPWS version has a
distinct XML namespace. Devices or clients supporting several versions of
DPWS must implement several namespaces (and thus versions) of DPWS.

The main changes between DPWS version 1.0 and 1.1 is the update of the
referenced WS-Addressing specification to version 1.0, definition of the
discovery proxy in WS-Discovery and clean ups in the security feature of
DPWS.

As DPWS version 1.2 is still work in progress not all changes are known
yet. The biggest change known at the moment is the update of the referenced
specifications WS-Eventing, WS-Transfer and WS-Metadataexchange that are
standardized in the WS-RA Working Group at the W3C.

*/
