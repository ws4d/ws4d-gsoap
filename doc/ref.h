/**
 * gSOAP DPWS Plugin API
 *
 * @addtogroup DPWSAPI DPWS-API
 *
 *
 * The gSOAP DPWS Plugin implements the devices and the client side of the
 * Devices Profile for Web Services. The compile time switches DPWS_DEVICE and
 * DPWS_CLIENT can be used to choose between the device and the client
 * implementation. The DPWS_PEER switch is used for endpoints acting as device
 * as well as client.
 *
 * @{
 */

 /**
 * @addtogroup APIDevice Device side API
 * @ingroup DPWSAPI
 *
 *
 * This is the device side implementation of the gSOAP DPWS Plugin activated
 * with the DPWS_DEVICE compile time switch.
 *
 * @{
 */

/** @} */

/**
 * @addtogroup DPWSAPIClient Client side API
 * @ingroup DPWSAPI
 *
 *
 * This is the client side implementation of the gSOAP DPWS Plugin activated
 * with the DPWS_CLIENT compile time switch
 *
 * @{
 */

/** @} */

/**
 * @addtogroup APIPeer Peer side API
 * @ingroup DPWSAPI
 *
 *
 * The peer implementation is for endpoints that require both client and device
 * side functionality. This implementation can be activated with the DPWS_PEER
 * switch at compile time.
 *
 * @{
 */

/** @} */

 /** @} */

 /**
 * WS4D utilities
 *
 * @addtogroup WS4D_UTILS WS4D Utilities
 *
 * @{
 */

/** @} */

/**
 * DPWS specific Web Service Modules
 *
 * @addtogroup DPWS_WS_MODULES gSOAP Web Service Modules and Utilities
 *
 * @{
 */

/** @} */

