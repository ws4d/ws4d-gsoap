/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef WS4D_MISC_H_
#define WS4D_MISC_H_

#ifdef __cplusplus
extern "C"
{
#if 0
}
#endif
#endif

#ifdef _WIN32
#ifndef WIN32
#define WIN32
#endif
#include <windows.h>
#include <process.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef WITH_NOIO
#include "alt_io.h"
#endif

/**
 * gSoap gSOAP runtime extensions API
 *
 * @addtogroup SOAP_MISCAPI Misc
 * @ingroup WS4D_UTILS
 *
 * @{
 */

#include "ws4d_error.h"

#ifdef WIN32
#define INLINE
#define SOCKLEN_T int
#define SNPRINTF _snprintf
#pragma warning(disable : 4996) /* Remove snprintf Secure Warnings */
#define STRCASECMP _stricmp
#define STRTOK strtok_s
#define SLEEP(seconds) Sleep(seconds*1000)
#else
#ifndef WITH_NOIO
#include <sys/socket.h>
#endif
#define INLINE inline
#define SOCKLEN_T socklen_t
#define SNPRINTF snprintf
#define STRCASECMP strcasecmp
#define STRTOK strtok_r
#define SLEEP(seconds) sleep(seconds)
#endif

#ifndef INET_ADDRSTRLEN
#define INET_ADDRSTRLEN 16
#endif

#ifndef INET6_ADDRSTRLEN
#define INET6_ADDRSTRLEN 46
#endif

#include "ws4d_config.h"

#define DPWS_SYNC 0

#define WS4D_ABORT() abort()

#ifdef ABORT_ON_ASSERT

#define WS4D_HANDLE_ASSERT(value) WS4D_ABORT()
#else
#define WS4D_HANDLE_ASSERT(value) return value
#endif

#ifdef ABORT_ON_FAIL
#define WS4D_HANDLE_FAIL(value) WS4D_ABORT()
#else
#define WS4D_HANDLE_FAIL(value) return value
#endif

enum ws4d_trace_events
{
  ws4d_trace_assert = 1,
  ws4d_trace_fail = 2,
  ws4d_trace_warn = 3
};

void ws4d_custom_tracefunc (enum ws4d_trace_events type, const char *file,
                            int line, const char *expression);

#if defined(DEBUG) || defined(WS4D_TRACE_ALWAYS)

#ifdef WS4D_CUSTOM_TRACE

#define WS4D_CUSTOM_ASSERT_TRACE_FUNC(file, line, expression) \
  ws4d_custom_tracefunc(ws4d_trace_assert, file, line, expression);

#define WS4D_CUSTOM_FAIL_TRACE_FUNC(file, line, expression) \
  ws4d_custom_tracefunc(ws4d_trace_fail, file, line, expression);

#define WS4D_CUSTOM_WARN_TRACE_FUNC(file, line, expression) \
  ws4d_custom_tracefunc(ws4d_trace_warn, file, line, expression);

#else

#define WS4D_CUSTOM_ASSERT_TRACE_FUNC(file_p, line_p, expression_p) \
  fprintf(stderr,"(%s:%d) assertion failed: (%s)\n", file_p, line_p, expression_p)

#define WS4D_CUSTOM_FAIL_TRACE_FUNC(file_p, line_p, expression_p) \
  fprintf(stderr,"(%s:%d) expression failed: (%s)\n", file_p, line_p, expression_p)

#define WS4D_CUSTOM_WARN_TRACE_FUNC(file_p, line_p, expression_p) \
  fprintf(stderr,"(%s:%d) WARNING expression failed: (%s)\n", file_p, line_p, expression_p)

#endif

#define ws4d_assert(expr_p, val_p) \
  if (expr_p) {} else { \
    WS4D_CUSTOM_ASSERT_TRACE_FUNC(__FILE__, __LINE__, #expr_p); \
    WS4D_HANDLE_ASSERT(val_p); \
  }

#define ws4d_fail(expr_p, err_p) \
  if (expr_p) { \
    WS4D_CUSTOM_FAIL_TRACE_FUNC(__FILE__, __LINE__, #expr_p); \
    WS4D_HANDLE_FAIL(err_p); \
  }

#define ws4d_warn(expr_p) \
  if (expr_p) { \
    WS4D_CUSTOM_WARN_TRACE_FUNC(__FILE__, __LINE__, #expr_p); \
  }

#else

#define ws4d_assert(expr_p, val_p)

#define ws4d_fail(expr_p, err_p) \
  if (expr_p) { \
    WS4D_HANDLE_FAIL(err_p); \
  }

#define ws4d_warn(expr_p)

#endif

#define WS4D_UNUSED_PARAM(param) (param = param)

#define ws4d_int_setbit(intp, n) ((*intp) |=  (n))
#define ws4d_int_clrbit(intp, n) ((*intp) &= ~(n))
#define ws4d_int_chkbit(intp, n) ((*intp) &   (n))


#include "ws4d_mutex.h"

#include "ws4d_list.h"

#include "ws4d_mm.h"

#include "ws4d_alloclist.h"

#include "ws4d_stringlist.h"

#include "ws4d_time.h"

#include "ws4d_uuid.h"

#include "ws4d_uri.h"

#include "ws4d_qnamelist.h"

#include "ws4d_localizedstring.h"

#include "ws4d_device_description.h"

#include "ws4d_base64.h"

#ifdef HAVE_INET_PTON_H
#define ws4d_inet_pton inet_pton
#else
int ws4d_inet_pton (int af, const char *src, void *dst);
#endif

#ifdef HAVE_INET_NTOP_H
#define ws4d_inet_ntop inet_ntop
#else
const char *ws4d_inet_ntop (int af, const void *src, char *dst, size_t size);
#endif

#if defined(HAVE_GETIFADDRS_H) && defined(HAVE_FREEIFADDRS_H)
#include <ifaddrs.h>

#define ws4d_getifaddrs getifaddrs
#define ws4d_freeifaddrs freeifaddrs
#else
struct ifaddrs
   {
     struct ifaddrs *ifa_next;     /* Pointer to the next structure.  */

     char *ifa_name;               /* Name of this network interface.  */
     unsigned int ifa_flags;       /* Flags as from SIOCGIFFLAGS ioctl.  */

     struct sockaddr *ifa_addr;    /* Network address of this interface.  */
     struct sockaddr *ifa_netmask; /* Netmask of this interface.  */
     union
     {
       /* At most one of the following two is valid.  If the IFF_BROADCAST
          bit is set in `ifa_flags', then `ifa_broadaddr' is valid.  If the
          IFF_POINTOPOINT bit is set, then `ifa_dstaddr' is valid.
          It is never the case that both these bits are set at once.  */
       struct sockaddr *ifu_broadaddr; /* Broadcast address of this interface. */
       struct sockaddr *ifu_dstaddr; /* Point-to-point destination address.  */
     } ifa_ifu;

# ifndef ifa_broadaddr
#  define ifa_broadaddr ifa_ifu.ifu_broadaddr
# endif
# ifndef ifa_dstaddr
#  define ifa_dstaddr   ifa_ifu.ifu_dstaddr
# endif

     void *ifa_data;               /* Address-specific data (may be unused).  */
   };

int ws4d_getifaddrs (struct ifaddrs **ifap);
void ws4d_freeifaddrs (struct ifaddrs *ifa);
#endif

/** @} */

#ifdef __cplusplus
#if 0
{
#endif
}
#endif

#endif /*WS4D_MISC_H_ */
