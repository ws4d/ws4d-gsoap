/* lib hosting service - hosting service as library
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 01.06.2011
 *      Author: elmex
 */

#ifndef HOSTING_SERVICE_H_
#define HOSTING_SERVICE_H_

#include "stddpws.h"

struct hosting_service
{
  struct dpws_s dpws;
  struct soap soap;
};

typedef int (*hosting_service_soapinit_cb) (struct soap *,
                                            const struct Namespace *);

int hs_init (struct hosting_service *service, const char *uuid,
             const char *serviceID, const char *interf, int port, int backlog,
             int https, const struct Namespace *ns);
int hs_init2 (struct hosting_service *service,
              hosting_service_soapinit_cb soap_init_cb, const char *uuid,
              const char *serviceID, const char *interf, int port,
              int backlog, int https, const struct Namespace *ns);
int hs_done (struct hosting_service *service);

int hs_addtypes (struct hosting_service *service, const char *types);
int hs_removetypes (struct hosting_service *service, const char *types);

int hs_addscopes (struct hosting_service *service, const char *scopes);
int hs_removescopes (struct hosting_service *service, const char *scopes);

struct ws4d_thisModel *hs_metadata_change_thismodel (struct hosting_service
                                                     *service);
struct ws4d_thisDevice *hs_metadata_change_thisdevice (struct hosting_service
                                                       *service);

int hs_metadata_update (struct hosting_service *service);

int hs_activate (struct hosting_service *service);
int hs_deactivate (struct hosting_service *service);

int hs_addservice (struct hosting_service *service, const char *serviceID,
                   char *addrs, const char *types);
int hs_removeservice (struct hosting_service *service, const char *serviceID);

int hs_dowork (struct hosting_service *service, ws4d_time timeout);

#endif /* HOSTING_SERVICE_H_ */
