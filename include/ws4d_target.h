/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef WS4D_TARGET_H_
#define WS4D_TARGET_H_

#include "ws4d_epr.h"
#include "ws4d_qnamelist.h"

/**
 * Target Endpoint
 *
 * @addtogroup APITargetEP Target Endpoint
 * @ingroup WS4D_UTILS
 *
 * @{
 */


/**
 * Function to set network address of a target endpoint
 *
 * A target endpoint has a logical address (Addrs) and a network address
 * (XAddrs). The logical address is network independent and does not change
 * when a target joins a network.
 *
 * @code
 * struct ws4d_epr target;
 *
 * ws4d_epr_init(&target);
 *
 * ws4d_targetep_set_XAddrs(&target, "http://127.0.0.1/testaddrs");
 *
 * ws4d_epr_done(&target);
 * @endcode
 *
 * @param epr target epr
 * @param XAddrs space separated list of network adresses
 */
int ws4d_targetep_set_XAddrs (struct ws4d_epr *epr, const char *XAddrs);


/**
 * Function to get network address of a target endpoint as string list
 *
 * @param epr target epr
 * @returns list of network adresses as string list
 */
struct ws4d_stringlist *ws4d_targetep_get_XAddrsList (struct ws4d_epr *epr);


/**
 * Function to get network address of a target endpoint
 *
 * A target endpoint has a logical address (Addrs) and a network address
 * (XAddrs). The logical address is network independent and does not change
 * when a target joins a network.
 *
 * @code
 * struct ws4d_epr target;
 *
 * ws4d_epr_init(&target);
 *
 * ws4d_targetep_set_XAddrs(&target, "http://127.0.0.1/testaddrs");
 * printf("Network address: %s\n", ws4d_targetep_get_XAddrs(&target));
 *
 * ws4d_epr_done(&target);
 * @endcode
 *
 * @param epr target epr
 * @returns list of network adresses as space separated string list
 */
#define ws4d_targetep_get_XAddrs(__epr) \
    ws4d_stringlist_tostring(ws4d_targetep_get_XAddrsList(__epr))


/**
 * Function to set probe message id related to this target endpoint.
 *
 * @note for internal use only!
 */
int ws4d_targetep_set_ProbeMsgId (struct ws4d_epr *epr,
                                  const char *ProbeMsgId);


/**
 * Function to get the probe message id related to this target endpoint.
 *
 * This can be used to relate a target endpoint to a probe request.
 *
 * @param epr target endpoint
 * @returns pointer to message id if available or NULL otherwise
 */
const char *ws4d_targetep_get_ProbeMsgId (struct ws4d_epr *epr);


/**
 * Function to set resolve message id related to this target endpoint.
 *
 * @note for internal use only!
 */
int ws4d_targetep_set_ResolveMsgId (struct ws4d_epr *epr,
                                    const char *ResolveMsgId);


/**
 * Function to get the resolve message id related to this target endpoint.
 *
 * This can be used to relate a target endpoint to a resolve request.
 *
 * @param epr target endpoint
 * @returns pointer to message id if available or NULL otherwise
 */
const char *ws4d_targetep_get_ResolveMsgId (struct ws4d_epr *epr);


/**
 * Function to get a pointer to the type list to apply changes
 *
 * This function should be used when the list will be changed
 *
 * @param epr target endpoint
 * @returns pointer to type list or NULL on failure
 */
struct ws4d_qnamelist *ws4d_targetep_change_TypeList (struct ws4d_epr *epr);


/**
 * Function to get a pointer to the type list for read only access
 *
 * @param epr target endpoint
 * @returns pointer to type list or NULL on failure
 */
struct ws4d_qnamelist *ws4d_targetep_get_TypeList (struct ws4d_epr *epr);


/**
 * Function to add a type to the type list
 *
 * @param __epr target endpoint
 * @param __type type as qname
 * @returns WS4D_OK or error code on failure
 */
#define ws4d_targetep_add_Type(__epr, __type) \
    ws4d_qnamelist_add(ws4d_targetep_change_TypeList(__epr), __type)


/**
 * Function to add a type to the type list
 *
 * @param __epr target endpoint
 * @param __typestr type as string
 * @returns WS4D_OK or error code on failure
 */
#define ws4d_targetep_add_Typestr(__epr, __typestr) \
    ws4d_qnamelist_addstring(ws4d_targetep_change_TypeList(__epr), __typestr)


/**
 * Function to remove a type from the type list
 *
 * @param __epr target endpoint
 * @param __type type as qname
 * @returns WS4D_OK or error code on failure
 */
#define ws4d_targetep_del_Type(__epr, __type) \
    ws4d_qnamelist_remove(ws4d_targetep_change_TypeList(__epr), __type)


/**
 * Function to clear and set contents of type list
 *
 * @param __epr target endpoint
 * @param __typestr type as string
 * @returns WS4D_OK or error code on failure
 */
#define ws4d_targetep_set_Types(__epr, __typestr) \
    ws4d_qnamelist_setstring(ws4d_targetep_change_TypeList(__epr), __typestr)


/**
 * Function to get contents of type list as space separated list
 *
 * @param __epr target endpoint
 * @returns pointer to string or NULL on failure
 */
#define ws4d_targetep_get_Types(__epr) \
    ws4d_qnamelist_tostring(ws4d_targetep_get_TypeList(__epr))


/**
 * Function to check if the types list matches a given list
 *
 * @param epr target endpoint
 * @param Types types to match
 * @return 1 if target type list is equal or contains Types, 0 otherwise
 */
int ws4d_targetep_matches_Types (struct ws4d_epr *epr,
                                 struct ws4d_qnamelist* Types);


/**
 * Function to get a pointer to the scope list to apply changes
 *
 * This function should be used when the list will be changed
 *
 * @param epr target endpoint
 * @returns pointer to scope list or NULL on failure
 */
struct ws4d_stringlist *ws4d_targetep_change_ScopeList (struct ws4d_epr *epr);


/**
 * Function to get a pointer to the scope list for read only access
 *
 * @param epr target endpoint
 * @returns pointer to scope list or NULL on failure
 */
struct ws4d_stringlist *ws4d_targetep_get_ScopeList (struct ws4d_epr *epr);


/**
 * Function to add a scope to the scope list
 *
 * @param __epr target endpoint
 * @param __scope scope as string
 * @returns WS4D_OK or error code on failure
 */
#define ws4d_targetep_add_Scope(__epr, __scope) \
    ws4d_stringlist_add(ws4d_targetep_change_ScopeList(__epr), __scope)


/**
 * Function to remove a scope from the scope list
 *
 * @param __epr target endpoint
 * @param __scope scope as string
 * @returns WS4D_OK or error code on failure
 */
#define ws4d_targetep_del_Scope(__epr, __scope) \
    ws4d_stringlist_delete(ws4d_targetep_change_ScopeList(__epr), __scope)


/**
 * Function to clear and set contents of scope list
 *
 * @param __epr target endpoint
 * @param __scope scope list as string
 * @returns WS4D_OK or error code on failure
 */
#define ws4d_targetep_set_Scopes(__epr, __scope) \
    ws4d_stringlist_set(ws4d_targetep_change_ScopeList(__epr), __scope)


/**
 * Function to get contents of scope list as space separated list
 *
 * @param __epr target endpoint
 * @returns pointer to string or NULL on failure
 */
#define ws4d_targetep_get_Scopes(__epr) \
    ws4d_stringlist_tostring(ws4d_targetep_get_ScopeList(__epr))


/**
 * Function to check if the types list matches a given list
 *
 * @param epr target endpoint
 * @param Scopes scopes to match
 * @return 1 if target type list is equal or contains Scopes, 0 otherwise
 */
int ws4d_targetep_matches_Scopes (struct ws4d_epr *epr,
                                  struct ws4d_stringlist *Scopes,
                                  ws4d_stringlist_matchingfunction scope_match_func);


/**
 * Function to set MetadataVersion of target endpoint
 *
 * @note for internal use only!
 */
int ws4d_targetep_set_MetadataVersion (struct ws4d_epr *epr,
                                       int MetadataVersion);


/**
 * Function to get MetadataVersion fo a target endpoint
 *
 * @parma epr target endpoint
 * @returns MetadataVersion as integer
 */
int ws4d_targetep_get_MetadataVersion (struct ws4d_epr *epr);


/**
 * Function to mark that a target endpoint has changed metadata
 *
 * @param epr target endpoint
 * @return WS4D_OK on success, error code otherwise
 */
int ws4d_targetep_changeMetadata (struct ws4d_epr *epr);


/**
 * Function to activate a target endpoint. Only activated target endpoints are
 * adevertised.
 */
int
ws4d_targetep_activate (struct ws4d_epr *epr, int MessageNumber,
                        int InstanceId, int MetadataVersion);


/**
 * Function to deactivate a target endpoint. Only activated target endpoints are
 * adevertised.
 */
int ws4d_targetep_deactivate (struct ws4d_epr *elem);


/**
 * Function to check if a target endpoint is active
 */
int ws4d_targetep_isactive (struct ws4d_epr *elem);


/**
 * Function to set the target service that is related to a target endpoint
 *
 * @note for internal use only!
 */
int ws4d_targetep_set_ts (struct ws4d_epr *epr, void *targetservice);


/**
 * Function to get the target service that is related to a target endpoint
 */
void *ws4d_targetep_get_ts (struct ws4d_epr *epr);


/** @} */

#endif /*WS4D_TARGET_H_ */
