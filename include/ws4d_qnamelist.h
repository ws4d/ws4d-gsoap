/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef WS4D_QNAMELIST_H_
#define WS4D_QNAMELIST_H_

#include "ws4d_qname.h"

/**
 * List of Qualified Names
 *
 * @addtogroup APIQNameList List of Qualified Names
 * @ingroup WS4D_LISTS
 *
 * @{
 */

typedef struct ws4d_qnamelist
{
  int status;
  struct ws4d_list_node qnames;
  const char *string;
  struct ws4d_abs_allocator alist;
#ifdef WITH_MUTEXES
    WS4D_MUTEX (lock);
#endif
} ws4d_qnamelist;


/**
 * Function to initialize a QName list head
 *
 * @code
 * struct ws4d_qnamelist list;
 *
 * ws4d_qnamelist_init(&list);
 * @endcode
 *
 * @param head list head to initialize
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_qnamelist_init (struct ws4d_qnamelist *head);


/**
 * Function to initialize a QName list head using an external allocator for
 * internal memory management
 *
 * @code
 * ws4d_alloc_list alloc;
 * struct ws4d_qnamelist list;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * ws4d_qnamelist_init2(&list, &alloc);
 * @endcode
 *
 * @param head list head to initialize
 * @param allocator external allocator for memory management
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_qnamelist_init2 (struct ws4d_qnamelist *head, const char *string);


/**
 * Function to destroy a QName list
 *
 * @code
 * struct ws4d_qnamelist list;
 *
 * ws4d_qnamelist_init(&list);
 * ws4d_qnamelist_done(&list);
 * @endcode
 *
 * @param head list head to destroy
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_qnamelist_done (struct ws4d_qnamelist *head);


/**
 * Function to add a QName to a QName list
 *
 * @code
 * struct ws4d_qname qname;
 * struct ws4d_qnamelist list;
 *
 * ws4d_qname_init(&qname);
 * ws4d_qname_setNS(&qname, "http://www.ws4d.org/testns");
 * ws4d_qname_setName(&qname, "test");
 *
 * ws4d_qnamelist_init(&list);
 * ws4d_qnameList_add(&list, &qname);
 * ws4d_qnamelist_done(&list);
 *
 * ws4d_qname_done(&qname);
 * @endcode
 *
 * @param head list head
 * @param qname QName to add
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_qnamelist_add (struct ws4d_qnamelist *head,
                        const struct ws4d_qname *qname);


/**
 * Function to add a QName string to a QName list
 *
 * @code
 * char *test_string = "\"http://www.ws4d.org/testns1\":test1 \"http://www.ws4d.org/testns2\":test2";
 * struct ws4d_qnamelist list;
 *
 * ws4d_qnamelist_init(&list);
 * ws4d_qnamelist_add(&list, test_string);
 * ws4d_qnamelist_done(&list);
 * @endcode
 *
 * @param head list head
 * @param qnames space seperated list of QNames to add
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_qnamelist_addstring (struct ws4d_qnamelist *head,
                              const char *qnames);


/**
 * Function to add all items of a QName list to a QName list
 *
 * @code
 * char *test_string = "\"http://www.ws4d.org/testns1\":test1 \"http://www.ws4d.org/testns2\":test2";
 * struct ws4d_qnamelist list1, list2;
 *
 * ws4d_qnamelist_init2(&list1, test_string);
 *
 * ws4d_qnamelist_init(&list2);
 * ws4d_qnamelist_addlist(&list2, &list1);
 * ws4d_qnamelist_done(&list2);
 *
 * ws4d_qnamelist_done(&list1);
 * @endcode
 *
 * @param head list head
 * @param list QName list to add
 * @return WS4D_OK on success, an error code otherwise
 */
int
ws4d_qnamelist_addlist (struct ws4d_qnamelist *head,
                        struct ws4d_qnamelist *list);


/**
 * Function to clear and set contents of QName list
 *
 * This function is a combination of ws4d_qnamelist_clear() and
 * ws4d_qnamelist_addstring().
 *
 * @param head qname list
 * @param string space seperated list of QNames to set as content of the list
 * @return WS4D_OK on success, an error code otherwise
 */
int
ws4d_qnamelist_setstring (struct ws4d_qnamelist *head, const char *string);


/**
 * Function to remove a QName of a QName list
 *
 * @code
 * char *test_string = "\"http://www.ws4d.org/testns1\":test1 \"http://www.ws4d.org/testns2\":test2";
 * struct ws4d_qname qname;
 * struct ws4d_qnamelist list;
 *
 * ws4d_qname_init(&qname);
 * ws4d_qname_setNS(&qname, "http://www.ws4d.org/testns1");
 * ws4d_qname_setName(&qname, "test1");
 *
 * ws4d_qnamelist_init2(&list, test_string);
 * ws4d_qnamelist_remove(&list, &qname);
 * ws4d_qnamelist_done(&list);
 *
 * ws4d_qname_done(&qname);
 * @endcode
 *
 * @param head list head
 * @param list QName to remove
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_qnamelist_remove (struct ws4d_qnamelist *head,
                           struct ws4d_qname *qname);


/**
 * Function to remove all elements of a QName list
 *
 * @code
 * char *test_string = "\"http://www.ws4d.org/testns1\":test1 \"http://www.ws4d.org/testns2\":test2";
 * struct ws4d_qnamelist list;
 *
 * ws4d_qnamelist_init2(&list, test_string);
 * ws4d_qnamelist_clear(&list);
 * ws4d_qnamelist_done(&list);
 * @endcode
 *
 * @param head list head
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_qnamelist_clear (struct ws4d_qnamelist *head);


/**
 * Function to check if a QName list is empty
 *
 * @code
 * char *test_string = "\"http://www.ws4d.org/testns1\":test1 \"http://www.ws4d.org/testns2\":test2";
 * struct ws4d_qnamelist list;
 *
 * ws4d_qnamelist_init2(&list, test_string);
 * if (ws4d_qnamelist_isempty(&list))
 * {
 *   //
 * }
 * ws4d_qnamelist_done(&list);
 * @endcode
 *
 * @param head list head
 * @return 1 if list is empty, 0 otherwise
 */
#define ws4d_qnamelist_isempty(__head) \
    ws4d_list_empty (&(__head)->qnames)


/**
 * Function to get the length of a QName list
 *
 * @code
 * int len;
 * char *test_string = "\"http://www.ws4d.org/testns1\":test1 \"http://www.ws4d.org/testns2\":test2";
 * struct ws4d_qnamelist list;
 *
 * ws4d_qnamelist_init2(&list, test_string);
 * len = ws4d_stringlist_len(&list);
 * ws4d_stringlist_done(&list);
 * @endcode
 *
 * @param head list head
 * @return number of elements in qname list
 */
int ws4d_qnamelist_len (struct ws4d_qnamelist *head);


/**
 * Function to copy all elements of QName list source to QName list destination
 *
 * @code
 * char *test_string = *test_string = "\"http://www.ws4d.org/testns1\":test1 \"http://www.ws4d.org/testns2\":test2";
 * struct ws4d_qnamelist list1, list2;
 *
 * ws4d_qnamelist_init2(&list1, test_string);
 *
 * ws4d_qnamelist_init(&list2);
 * ws4d_qnamelist_copy(&list1, &list2);
 * ws4d_qnamelist_done(&list2);
 *
 * ws4d_qnamelist_done(&list1);
 * @endcode
 *
 * @param src source list head
 * @param dst destination list head
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_qnamelist_copy (struct ws4d_qnamelist *src,
                         struct ws4d_qnamelist *dst);


/**
 * Function to check if list1 is contained in list2. It returns -1 if list2
 * does not contain list1, 0 if lists are equal or 1 if list2 contains list 1.
 *
 * @code
 * char *test_string = *test_string = "\"http://www.ws4d.org/testns1\":test1 \"http://www.ws4d.org/testns2\":test2";
 * struct ws4d_qnamelist list1, list2;
 *
 * ws4d_qnamelist_init2(&list1, test_string);
 * ws4d_qnamelist_init2(&list2, test_string);
 *
 * ret = ws4d_qnamelist_compare(&list1, &list2);
 * //check ret
 *
 * ws4d_qnamelist_done(&list2);
 * ws4d_qnamelist_done(&list1);
 * @endcode
 *
 * @param head1 list head
 * @param head2 list head
 * @return -1 if list2 does not contain list1, 0 if lists are equal or 1 if
 * list2 contains list 1
 */
int
ws4d_qnamelist_compare (struct ws4d_qnamelist *head1,
                        struct ws4d_qnamelist *head2);


/**
 * Function to find if a qname that matches an item in a QName list
 *
 * @param head QName list
 * @param qname QName structure to match
 * @return matching QName structure
 */
struct ws4d_qname *ws4d_qnamelist_finditem (struct ws4d_qnamelist *head,
                                            struct ws4d_qname *qname);


/**
 * Function to create string representation of qname list
 *
 * @code
 * struct ws4d_qname qname;
 * struct ws4d_qnamelist list;
 * char *string;
 *
 * ws4d_qname_init(&qname);
 * ws4d_qname_setNS(&qname, "http://www.ws4d.org/testns1");
 * ws4d_qname_setName(&qname, "test1");
 *
 * ws4d_qnamelist_init(&list);
 * ws4d_qnamelist_add(&list, &qname);
 * string = ws4d_qnamelist_tostring(&list);
 * ws4d_qnamelist_done(&list);
 *
 * ws4d_qname_done(&qname);
 * @endcode
 *
 * @param head list head
 * @return string representation of list, or NULL on failure
 */
const char *ws4d_qnamelist_tostring (struct ws4d_qnamelist *head);


/**
 * Function to lock a qname list
 *
 * @see ws4d_qnamelist_unlock() for an example
 *
 * @param list head
 */
#ifdef WITH_MUTEXES
void ws4d_qnamelist_lock (struct ws4d_qnamelist *head);
#else
#define ws4d_qnamelist_lock(head)
#endif


/**
 * Function to lock a qname list
 *
 * @code
 * struct ws4d_qnamelist list;
 *
 * ws4d_qnamelist_init(&list);
 *
 * ws4d_qnamelist_lock(&list);
 * //critical path
 * ws4d_qnamelist_unlock(&list);
 *
 * ws4d_qnamelist_done(&list);
 * @endcode
 *
 * @param list head
 */
#ifdef WITH_MUTEXES
void ws4d_qnamelist_unlock (struct ws4d_qnamelist *head);
#else
#define ws4d_qnamelist_unlock(head)
#endif


/**
 * Macro to iterate qname list
 *
 * @code
 * char *test_string = *test_string = "\"http://www.ws4d.org/testns1\":test1 \"http://www.ws4d.org/testns2\":test2";
 * struct ws4d_qnamelist list1, list2;
 * struct ws4d_qname *qname, *next;
 *
 * ws4d_qnamelist_init2(&list, test_string);
 *
 * ws4d_qnamelist_foreach(qname, next, &list)
 * {
 *   // do something with qname
 * }
 *
 * ws4d_qnamelist_done(&list);
 * @endcode
 */
#define ws4d_qnamelist_foreach(pos, iter, head) \
    ws4d_list_foreach (pos, iter, &(head)->qnames, struct ws4d_qname, list)

/** @} */

#endif /*WS4D_QNAMELIST_H_ */
