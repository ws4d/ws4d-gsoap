/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef WS4D_STRINGLIST_H_
#define WS4D_STRINGLIST_H_

#include "ws4d_misc.h"
#include "ws4d_uri.h"

/**
 * List of Strings
 *
 * @addtogroup APIStringList List of Strings
 * @ingroup WS4D_LISTS
 *
 * @{
 */

struct ws4d_stringlist
{
  int status;
  struct ws4d_list_node strings;
  const char *string;
  struct ws4d_abs_allocator alist;
#ifdef WITH_MUTEXES
    WS4D_MUTEX (lock);
#endif
};


/**
 * Function to initialize a string list head
 *
 * @code
 * struct ws4d_stringlist list;
 *
 * ws4d_stringlist_init(&list);
 * @endcode
 *
 * @param head list head to initialize
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_stringlist_init (struct ws4d_stringlist *head);


/**
 * Function to initialize a string list head using an external allocator for
 * internal memory management
 *
 * @code
 * ws4d_alloc_list alloc;
 * struct ws4d_stringlist list;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * ws4d_stringlist_init2(&list, &alloc);
 * @endcode
 *
 * @param head list head to initialize
 * @param allocator external allocator for memory management
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_stringlist_init2 (struct ws4d_stringlist *head,
                           struct ws4d_abs_allocator *allocator);


/**
 * Function to initialize a string list head and add a string to the list
 *
 * @code
 * char *test_string = "item1 item2 item3";
 * struct ws4d_stringlist list;
 *
 * ws4d_stringlist_init3(&list, test_string);
 * @endcode
 *
 * @param head list head to initialize
 * @param items space separated string list of items to add
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_stringlist_init3 (struct ws4d_stringlist *head, const char *items);


/**
 * Function to check if a stringlist structure is initialized
 *
 * @param head string list
 * @return 1 if list is initialized, 0 otherwise
 */
int ws4d_stringlist_check (struct ws4d_stringlist *head);


/**
 * Function to destroy a string list
 *
 * @code
 * struct ws4d_stringlist list;
 *
 * ws4d_stringlist_init(&list);
 * ws4d_stringlist_done(&list);
 * @endcode
 *
 * @param head list head to destroy
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_stringlist_done (struct ws4d_stringlist *head);


/**
 * Function to add a string to a string list
 *
 * @code
 * char *test_string = "item1 item2 item3";
 * struct ws4d_stringlist list;
 *
 * ws4d_stringlist_init(&list);
 * ws4d_stringlist_add(&list, test_string);
 * ws4d_stringlist_done(&list);
 * @endcode
 *
 * @param head list head
 * @param strings space seperated list of strings to add
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_stringlist_add (struct ws4d_stringlist *head, const char *strings);


/**
 * Function to add a uri to a string list
 *
 * @code
 * struct ws4d_uri uri;
 * struct ws4d_stringlist list;
 *
 * ws4d_uri_init(&uri, "http://www.ws4d.org/testuri", WITH_ALL);
 *
 * ws4d_stringlist_init(&list);
 * ws4d_stringlist_add(&list, &uri);
 * ws4d_stringlist_done(&list);
 *
 * ws4d_uri_done(&uri);
 * @endcode
 *
 * @param head list head
 * @param uri uri to add
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_stringlist_adduri (struct ws4d_stringlist *head,
                            struct ws4d_uri *uri);


/**
 * Function to add all items of an string list to a string list
 *
 * @code
 * char *test_string = "string1 string2";
 * struct ws4d_stringlist list1, list2;
 *
 * ws4d_stringlist_init3(&list1, test_string);
 *
 * ws4d_stringlist_init(&list2);
 * ws4d_stringlist_addlist(&list2, &list1);
 * ws4d_stringlist_done(&list2);
 *
 * ws4d_stringlis_done(&list1);
 * @endcode
 *
 * @param head list head
 * @param list string list to add
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_stringlist_addlist (struct ws4d_stringlist *head,
                             struct ws4d_stringlist *list);


/**
 * Function to clear and set contents of string list
 *
 * This function is a combination of ws4d_stringlist_clear() and
 * ws4d_stringlist_add().
 *
 * @param head qname list
 * @param string space seperated list of strings
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_stringlist_set (struct ws4d_stringlist *head,
                         const char *strings);


/**
 * Function to remove a string of a string list
 *
 * @code
 * char *test_string = "test1 test2";
 * struct ws4d_stringlist list;
 *
 * ws4d_stringlist_init3(&list, test_string);
 * ws4d_stringlist_remove(&list, "test1");
 * ws4d_stringlist_done(&list);
 * @endcode
 *
 * @param head list head
 * @param list string to remove
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_stringlist_delete (struct ws4d_stringlist *head, const char *sitem);


/**
 * Function to remove all elements of a string list
 *
 * @code
 * char *test_string = "test1 test2";
 * struct ws4d_stringlist list;
 *
 * ws4d_stringlist_init2(&list, test_string);
 * ws4d_stringlist_clear(&list);
 * ws4d_stringlist_done(&list);
 * @endcode
 *
 * @param head list head
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_stringlist_clear (struct ws4d_stringlist *head);


/**
 * Function to check if a string list is empty
 *
 * @code
 * char *test_string = "test1 test2";
 * struct ws4d_stringlist list;
 *
 * ws4d_stringlist_init3(&list, test_string);
 * if (ws4d_stringlist_isempty(&list))
 * {
 *   //
 * }
 * ws4d_stringlist_done(&list);
 * @endcode
 *
 * @param head list head
 * @return 1 if list is empty, 0 otherwise
 */
#define ws4d_stringlist_isempty(__head) \
    ws4d_list_empty (&(__head)->strings)


/**
 * Function to get the length of a string list
 *
 * @code
 * int len;
 * char *test_string = "test1 test2";
 * struct ws4d_stringlist list;
 *
 * ws4d_stringlist_init3(&list, test_string);
 * len = ws4d_stringlist_len(&list);
 * ws4d_stringlist_done(&list);
 * @endcode
 *
 * @param head list head
 * @return number of elements in string list
 */
int ws4d_stringlist_len (struct ws4d_stringlist *head);


/**
 * Function to copy all elements of string list source to string list destination
 *
 * @code
 * char *test_string = "test1 test2";
 * struct ws4d_stringlist list1, list2;
 *
 * ws4d_qnamelist_init3(&list1, test_string);
 *
 * ws4d_qnamelist_init(&list2);
 * ws4d_qnamelist_copy(&list1, &list2);
 * ws4d_stringlist_done(&list2);
 *
 * ws4d_stringlist_done(&list1);
 * @endcode
 *
 * @param src source list head
 * @param dst destination list head
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_stringlist_copy (struct ws4d_stringlist *src,
                          struct ws4d_stringlist *dst);


typedef int (*ws4d_stringlist_matchingfunction) (const char *s1, const char *s2,
                                                 void *matchingdata);


/**
 * Function to check if list1 is contained in list2. It returns -1 if list2
 * does not contain list1, 0 if lists are equal or 1 if list2 contains list 1.
 * This function uses a custom matching function.
 *
 * @code
 * char *test_string = "test1 test2";
 * struct ws4d_stringlist list1, list2;
 * int ret;
 *
 * ws4d_stringlist_init3(&list1, test_string);
 * ws4d_stringlist_init3(&list2, test_string);
 *
 * ret = ws4d_stringlist_compare(&list1, &list2);
 * //check ret
 *
 * ws4d_stringlist_done(&list2);
 * ws4d_stringlist_done(&list1);
 * @endcode
 *
 * @param head1 list head
 * @param head2 list head
 * @return -1 if list2 does not contain list1, 0 if lists are equal or 1 if
 * list2 contains list 1
 */
int ws4d_stringlist_compare2 (struct ws4d_stringlist *head1,
                              struct ws4d_stringlist *head2,
                              ws4d_stringlist_matchingfunction mf,
                              void *mfdata);


/**
 * Function to check if list1 is contained in list2. It returns -1 if list2
 * does not contain list1, 0 if lists are equal or 1 if list2 contains list 1.
 *
 * @code
 * char *test_string = "test1 test2";
 * struct ws4d_stringlist list1, list2;
 * int ret;
 *
 * ws4d_stringlist_init3(&list1, test_string);
 * ws4d_stringlist_init3(&list2, test_string);
 *
 * ret = ws4d_stringlist_compare(&list1, &list2);
 * //check ret
 *
 * ws4d_stringlist_done(&list2);
 * ws4d_stringlist_done(&list1);
 * @endcode
 *
 * @param head1 list head
 * @param head2 list head
 * @return -1 if list2 does not contain list1, 0 if lists are equal or 1 if
 * list2 contains list 1
 */
#define ws4d_stringlist_compare(__head1, __head2) \
    ws4d_stringlist_compare2(__head1, __head2, NULL, NULL)


#ifndef DOXYGEN_SHOULD_SKIP_THIS
struct ws4d_stringlist_item *
ws4d_stringlist_finditem2 (struct ws4d_stringlist *head, const char *item,
                           ws4d_stringlist_matchingfunction mf, void *mfdata);

#define ws4d_stringlist_finditem(__head, __item) \
    ws4d_stringlist_finditem2(__head, __item, NULL, NULL)
#endif


/**
 * Function to check if a string list contains a specific string
 *
 * @param __head string list
 * @param __string string to check
 * @return 1 if list contains string, 0 otherwise
 */
#define ws4d_stringlist_contains(__head, __string) \
    (ws4d_stringlist_finditem(__head, __string) != NULL)


/**
 * Function to create string representation of string list
 *
 * @code
 * struct ws4d_stringlist list;
 * char *string;
 *
 * ws4d_stringlist_init(&list);
 * ws4d_stringlist_add(&list, "string1");
 * ws4d_stringlist_add(&list, "string2");
 * string = ws4d_stringlist_tostring(&list);
 * ws4d_stringlist_done(&list);
 * @endcode
 *
 * @param head list head
 * @return string representation of list, or NULL on failure
 */
const char *ws4d_stringlist_tostring (struct ws4d_stringlist *head);


/**
 * Function to lock a string list
 *
 * @see ws4d_stringlist_unlock() for an example
 *
 * @param head list head
 */
#ifdef WITH_MUTEXES
void ws4d_stringlist_lock (struct ws4d_stringlist *head);
#else
#define ws4d_stringlist_lock(head)
#endif


/**
 * Function to lock a string list
 *
 * @code
 * struct ws4d_stringlist list;
 *
 * ws4d_stringlist_init(&list);
 *
 * ws4d_stringlist_lock(&list);
 * //critical path
 * ws4d_stringlist_unlock(&list);
 *
 * ws4d_stringlist_done(&list);
 * @endcode
 *
 * @param head list head
 */
#ifdef WITH_MUTEXES
void ws4d_stringlist_unlock (struct ws4d_stringlist *head);
#else
#define ws4d_stringlist_unlock(head)
#endif

#ifndef DOXYGEN_SHOULD_SKIP_THIS
const char *ws4d_stringlist_iterate (const struct ws4d_stringlist *head,
                                     void **iterator);
#endif


/**
 * Macro to iterate string list
 *
 * @code
 * char *string, *test_string = "test1 test2";
 * struct ws4d_stringlist list;
 * void *it = NULL;
 *
 * ws4d_stringlist_init3(&list, test_string);
 *
 * ws4d_stringlist_foreach(string, &it, &list)
 * {
 *   // do something with string
 * }
 *
 * ws4d_stringlist_done(&list);
 * @endcode
 */
#define ws4d_stringlist_foreach(elem_s, iterator, head) \
  for(iterator=NULL, elem_s = ws4d_stringlist_iterate(head, &(iterator)); elem_s != NULL; elem_s = ws4d_stringlist_iterate(head, &(iterator)))


/**
 * Function to check if a string contains a space separated string list
 *
 * @param string string to check
 * @return 1 if string is a space separated string list, 0 otherwise
 */
int ws4d_isstringlist (const char *string);

/** @} */

#endif /*WS4D_STRINGLIST_H_ */
