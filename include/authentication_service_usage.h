/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * authentication_service_usage.h
 *
 *  Created on: 23.03.2010
 *      Author: cht
 */

#ifndef AUTHENTICATION_SERVICE_USAGE_H_
#define AUTHENTICATION_SERVICE_USAGE_H_

#include "dpws_client.h"
#include "security_engine.h"
#include "ws4d_eprllist.h"


#ifdef __cplusplus
extern "C"
{
#endif

  struct ws4d_epr *auths_find_authentication_service (struct dpws_s *dpws,
                                                      struct ws4d_epr
                                                      *device);

  int auths_authenticate_device (struct soap *soap, struct ws4d_epr *device,
                                 struct dpws_s *dpws,
                                 struct ws4d_epr *service,
                                 struct security_engine_s *s);

#ifdef __cplusplus
}
#endif
#endif                          /* AUTHENTICATION_SERVICE_USAGE_H_ */
