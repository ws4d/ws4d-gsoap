/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * security_engine.h
 *
 *  Created on: 09.03.2010
 *      Author: cht
 */

#ifndef SECURITY_ENGINE_H_
#define SECURITY_ENGINE_H_

#include "stdsoap2.h"

#include "security_shell_types.h"
#include <unistd.h>
#include <stdlib.h>
#include "ws4d_misc.h"
#include "ws4d_list.h"
#include "security_handler.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define sDEBUG(s) fprintf(stderr, s);

// FIXME: This is no longer necessary when wsseapi is replaced
  const char *soap_wsse_get_KeyInfo_SecurityTokenReferenceURI (struct soap *);


  struct security_engine_s
  {
  /*************************************************/
    /* Global attributes                             */
  /*************************************************/
    /* certificate and key files */
    const char *cafile;
    const char *certfile;
    const char *keyfile;
    const char *keypassword;

  /*************************************************/
    /* Authentication Engine                         */
  /*************************************************/

    /* process_request_one()
     * Input: Certificate, Generated Challenge
     * Output: Error
     */
    int (*ae_process_request_one) (struct security_engine_s *, ws4dX509,
                                   ws4dChallenge);

    /* process_request_two()
     * Input: Certificate, Generated Challenge
     * Output: Error
     */
    int (*ae_process_request_two) (struct security_engine_s *, ws4dX509,
                                   ws4dResponse, ws4dChallenge, ws4dResponse,
                                   ws4dCertRef);

    char *(*ae_request_pin) (struct security_engine_s *, ws4dX509);
      ws4dDerivedKey (*ae_derive_key_from_pin) (struct security_engine_s *,
                                                ws4dPin);
      ws4dChallenge (*ae_generate_challenge) (struct security_engine_s *,
                                              ws4dChallenge);
      ws4dResponse (*ae_compute_response) (struct security_engine_s *,
                                           ws4dChallenge, ws4dDerivedKey);
    int (*ae_verify_response) (struct security_engine_s *, ws4dChallenge,
                               ws4dResponse, ws4dDerivedKey);
    int (*ae_check_client_authentication) (struct security_engine_s *,
                                           ws4dX509);
    int (*ae_check_device_authentication) (struct security_engine_s *, char *,
                                           ws4dX509);
      ws4dPin (*ae_retrieve_pin) (struct security_engine_s *, ws4dPin);
    void (*ae_propagate_pin) (struct security_engine_s *, ws4dPin);
  /*************************************************/
    /* Certificate Database                          */
  /*************************************************/
    struct ws4d_list_node cert_list_head;
    const char *cert_list_location;
      ws4dCertRef (*cdb_save_certificate) (struct security_engine_s *,
                                           ws4dX509, ws4dPin);
      ws4dPin (*cdb_lookup_certificate_pin) (struct security_engine_s *,
                                             ws4dX509);
    struct _cert_list_elem *(*cdb_lookup_certificate) (struct
                                                       security_engine_s *,
                                                       ws4dX509);
    void (*cdb_update_challenge) (struct security_engine_s *, ws4dX509,
                                  ws4dChallenge);
    void (*cdb_update_pin) (CertStorage *, ws4dPin);
    int (*cdb_cert_compare) (ws4dX509, ws4dX509);
    char *(*cdb_compute_reference) (CertStorage *);
    void (*cdb_save_cert_reference) (struct security_engine_s *, char *,
                                     ws4dCertRef, ws4dX509);
      ws4dCertRef (*cdb_lookup_cert_reference) (struct security_engine_s *,
                                                char *);
    int (*cdb_update_persistent_db) (struct security_engine_s *);
    int (*cdb_read_persistent_db) (struct security_engine_s *);
      ws4dX509 (*cdb_lookup_cert_by_ref) (struct security_engine_s *,
                                          ws4dCertRef);
    const void *(*cdb_wsse_token_handler) (struct soap *, int, int *);
    const char *(*cdb_get_credential_identifier) (struct security_engine_s *,
                                                  void *, credential_type_t);

  /*************************************************/
    /* Authorization Engine                          */
  /*************************************************/
    int (*authe_set_dynamic_authorization) (struct security_engine_s *,
                                            void *, permission_t,
                                            ressource_type_t);
      permission_t (*authe_authorize_request) (struct security_engine_s *,
                                               struct soap *, unsigned int,
                                               ressource_type_t);
    int (*authe_set_authorization_mode) (struct soap *, int);
    int (*authe_activate_authorization) (struct soap *);
    int (*authe_deactivate_authorization) (struct soap *);
    void (*authe_request_dynamic_authorization_oob)
      (struct soap *, struct security_engine_s *, void *,
       struct authorization_s *);
    void *(*authe_get_credential) (struct soap *, credential_type_t);
  /*************************************************/
    /* Authorization Database                        */
  /*************************************************/
    struct ws4d_list_node auth_list_head;
    int (*authdb_save_authorization) (struct security_engine_s *,
                                      struct authorization_s *);
    int (*authdb_delete_authorization) (struct security_engine_s *,
                                        struct authorization_s *);
    struct authorization_s *(*authdb_lookup_authorization)
      (struct security_engine_s * s, void *credential,
       void *ressource, credential_type_t);
    int (*authdb_update_authorization) (struct security_engine_s *,
                                        struct authorization_s *);
        /*************************************************/
    /* Crypto Engine                                 */
        /*************************************************/
    /* digest-related */
    int (*initialize_message_digest) (struct security_engine_s *);
    int (*update_message_digest) (struct security_engine_s *,
                                  unsigned char *);
    int (*finalize_message_digest) (struct security_engine_s *);
  };

/* application developers may use this macro to generate external
 * function declarations for initializing components
 */
#define EXTERN_INIT_TABLE(prefix) 																			\
    extern struct security_engine_s* 																		\
        prefix_init_authentication_engine(struct security_engine_s *s); \
    extern struct security_engine_s* 																		\
        prefix_del_authentication_engine(struct security_engine_s *s); 	\
    extern struct security_engine_s* 																		\
        prefix_init_certificate_db(struct security_engine_s *s); 				\
    extern struct security_engine_s* 																		\
        prefix_del_certificate_db(struct security_engine_s *s); 				\
    extern struct security_engine_s* 																		\
        prefix_init_authorization_engine(struct security_engine_s *s); 	\
    extern struct security_engine_s* 																		\
        prefix_del_authorization_engine(struct security_engine_s *s); 	\
		extern struct security_engine_s* 																		\
				prefix_init_authorization_db(struct security_engine_s *s); 			\
		extern struct security_engine_s* 																		\
				prefix_del_authorization_db(struct security_engine_s *s);  			\
		extern struct security_engine_s* 																		\
				prefix_init_crypto_engine(struct security_engine_s *s); 				\
		extern struct security_engine_s* 																		\
				prefix_del_crypto_engine(struct security_engine_s *s);


  struct security_engine_s *security_engine_init (struct security_engine_s
                                                  *s);

#ifdef __cplusplus
}
#endif

#endif                          /* SECURITY_ENGINE_H_ */
