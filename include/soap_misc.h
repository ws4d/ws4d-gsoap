/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef _MISC_H_
#define _MISC_H_

#ifdef __cplusplus
extern "C"
{
#if 0
}
#endif
#endif

#ifdef WITH_NOIO
#include "alt_io.h"
#endif

#include "stdsoap2.h"
#include "ws4d_misc.h"

#ifdef DEBUG

#define soap_assert(handle_p, expr_p, val_p) \
  if (expr_p) {} else { \
    if (handle_p) \
    { \
      ((struct soap*) handle_p)->error = SOAP_ERR; \
    } \
    WS4D_CUSTOM_ASSERT_TRACE_FUNC(__FILE__, __LINE__, #expr_p); \
    WS4D_HANDLE_ASSERT(val_p); \
  }

#define soap_fail(handle_p, expr_p, err_p) \
  if (expr_p) { \
    if (handle_p) \
    { \
      ((struct soap*) handle_p)->error = SOAP_ERR; \
    } \
    WS4D_CUSTOM_FAIL_TRACE_FUNC(__FILE__, __LINE__, #expr_p); \
    WS4D_HANDLE_FAIL(err_p); \
  }

#else

#define soap_assert(handle_p, expr_p, val_p) \
  if (expr_p) {} else { \
    if (handle_p) \
    { \
      ((struct soap*) handle_p)->error = SOAP_ERR; \
    } \
    WS4D_HANDLE_ASSERT(val_p); \
  }

#define soap_fail(handle_p, expr_p, err_p) \
  if (expr_p) { \
    if (handle_p) \
    { \
      ((struct soap*) handle_p)->error = SOAP_ERR; \
    } \
    WS4D_HANDLE_FAIL(err_p); \
  }

#endif


/**
 * Macro to convert ws4d localized string to soap localized string
 */
#define ws4d_locstring_tosoap(__src, __src_size, __dst, __dst_size, __alist) \
    ws4d_locstring_copy(__src, __src_size, (struct ws4d_locstring *) __dst, __dst_size, alist);


/**
 * Macro to convert soap localized string to ws4d localized string
 */
#define soap_locstring_tows4d(__src, __src_size, __dst, __dst_size, __alist) \
    ws4d_locstring_copy((struct ws4d_locstring *) __dst, __dst_size, __src, __src_size, __alist)


/**
 * Macro to free soap localized string allocated with soap_locstring_tows4d()
 */
#define soap_locstring_free(__str, __str_size) \
    ws4d_locstring_free((struct ws4d_locstring *) __str, __str_size);


/**
 * Macro to get a specific string of a localized string
 */
#define soap_locstring_get(__str, __str_size, __lang) \
    ws4d_locstring_get((struct ws4d_locstring *) __str, __str_size, __lang)


/**
 * @addtogroup DynamicNS Functions name space table management
 * @ingroup DPWS_WS_MODULES
 *
 * @{
 */

struct Namespace *soap_extend_namespaces (struct Namespace *orig_namespaces,
                                          struct Namespace *extns,
                                          struct ws4d_abs_allocator *alist);

int
soap_free_namespaces (struct Namespace *namespaces,
                      struct ws4d_abs_allocator *alist);

struct Namespace *soap_qnamelist_namespaces (struct ws4d_qnamelist * head,
                                             struct ws4d_abs_allocator *alist,
                                             struct Namespace
                                             *qname_namespaces);

struct Namespace *
soap_qnamelist_extend_namespaces (struct ws4d_qnamelist *qnamelist,
                                  const struct Namespace *namespaces,
                                  struct ws4d_abs_allocator *alist);

char *soap_gen_prefix (int *num, struct ws4d_abs_allocator *alist,
                       struct Namespace *prefix_namespaces);

/** @} */

/**
 * @addtogroup DpwsMAccept Functions for serving on multiple soap handles
 * @ingroup DPWS_WS_MODULES
 *
 * @{
 */


void *soap_getpeer (struct soap *soap);

size_t soap_getpeerlen (struct soap *soap);


/**
 * Macro to create a set of soap handels as parameter for soap_maccept
 *
 * @code
 * struct soap service1, service2;
 * struct soap *soap_set[] = SOAP_HANDLE_SET (&service1, &service2);
 * @endcode
 */
#ifdef _MSC_VER
#if _MSC_VER >= 1400
#define SOAP_HANDLE_SET(...) { __VA_ARGS__, NULL, NULL, NULL }
#endif
#else
#define SOAP_HANDLE_SET(...) { __VA_ARGS__, NULL, NULL, NULL }
#endif


/**
 * Function to listen for new messages on multiple soap handles. This function
 * is typically used in the loop where new messages are received and processed.
 *
 * @code
 * struct soap service1, service2;
 * [...]
 * for (;;)
 *  {
 *     struct soap *handle = NULL;
 *     struct soap *soap_set[] = SOAP_HANDLE_SET (&service1, &service2);
 *     int soap_count = 2;
 *
 *     handle = dpws_maccept (&device, 100000, soap_count, soap_set);
 *     if (handle)
 *      {
 *         if (soap_serve (handle))
 *          {
 *             soap_print_fault (handle, stderr);
 *          }
 *         soap_end (handle);
 *      }
 *  }
 * @endcode
 *
 * @param timeout time how long dpws_maccept should block
 * @param count number of soap handles in soap set
 * @param soap_handles soap handles set to listen on
 */
struct soap *soap_maccept (ws4d_time timeout, int count,
                           struct soap **soap_handles);

/**
 * Macro to create a set of soap serve functions as parameter for soap_mserve
 *
 * @code
 * int sis_serve_request (struct soap *soap);
 * int ats_serve_request (struct soap *soap);
 * struct soap *soap_set[] = SOAP_SERVE_SET (sis_serve_request, ats_serve_request);
 * @endcode
 */
#ifdef _MSC_VER
#if _MSC_VER >= 1400
#define SOAP_SERVE_SET(...) { __VA_ARGS__, NULL, NULL, NULL }
#endif
#else
#define SOAP_SERVE_SET(...) { __VA_ARGS__, NULL, NULL, NULL }
#endif

/**
 * Function to serve multiple serve request funtions generated by soapcpp2 at
 * a soap handle. This function is typically used in the loop where new
 * messages are received and processed.
 *
 * @code
 * int sis_serve_request (struct soap *soap);
 * int ats_serve_request (struct soap *soap);
 *
 * struct soap service1, service2;
 * [...]
 * for (;;)
 *  {
 *     struct soap *handle = NULL;
 *     struct soap *soap_handle_set[] = SOAP_HANDLE_SET (&service1, &service2);
 *     int soap_handle_count = 2;
 *     struct soap *soap_serve_set[] = SOAP_SERVE_SET (sis_serve_request, ats_serve_request);
 *     int soap_serve_count = 2;
 *
 *     handle = soap_maccept (&device, 100000, soap_count, soap_handle_set);
 *     if (handle)
 *      {
 *         if (soap_mserve (handle, soap_serve_count, serve_serve_set))
 *          {
 *             soap_print_fault (handle, stderr);
 *          }
 *         soap_end (handle);
 *      }
 *  }
 * @endcode
 *
 * @param soap handle to serve new request
 * @param count number of serve request functions in serve functions set
 * @param serve_requests set of serve request functions
 */
int
soap_mserve (struct soap *soap, int count,
             int (*serve_requests[])(struct soap * soap));

/** @} */

/**
 * @addtogroup DpwsMSocket Functions to bind several sockets to one soap handle
 * @ingroup DPWS_WS_MODULES
 *
 * @{
 */

/**
 * Function to check if a soap handle has multiple sockets
 *
 * @param soap soap handle to check
 * @return returns 1 if a socket has multiple sockets or 0 otherwise
 */
int soap_ismsocket (struct soap *soap);

/**
 * Function to bind a soap handel to a specific host and port that can be called
 * more often than once
 *
 * @param soap handle to bind
 * @param host hostname or interface address to bind to
 * @param port port numnber to bind to
 * @param backlog
 * @return a socket descriptor on success or SOAP_INVALID_SOCKET on failure
 */
SOAP_SOCKET
soap_mbind (struct soap *soap, const char *host, int port, int backlog);

/**
 * Function to switch between sockets a soap handel was bound to with soap_mbind()
 *
 * @param soap soap handel to switch active socket
 * @param sock socket descriptor to switch to
 *
 * @return SOAP_OK on success or an SOAP_* error code on failure
 */
int soap_mswitch (struct soap *soap, SOAP_SOCKET sock);

/**
 * Function to iterate over socket descriptors a soap handel was bound to
 *
 * @param soap soap handle to iterate socket descriptors
 * @param iterator pointer to pointer to an socket descriptor as internal iterator
 * @return socket descriptor on success or SOAP_INVALID_SOCKET on failure
 */
SOAP_SOCKET soap_msocket_iterate (struct soap *soap, void **iterator);

/**
 * Function to get the number of sockets a soap handle was bound to
 *
 * @param soap soap handel to get number
 * @return number of sockets
 */
int soap_msocket_getsocketcount (struct soap *soap);

/** @} */


int soap_out_transform_qnames (struct soap *soap, char **outstring,
                               char *const *a);

int soap_in_expand_qnames (struct soap *soap, char **instring, char **a);

/**
 * function to expand locally known namespace prefixes in a string.
 * Used after gSOAP expansion for QNames to remove gSOAP dependencies.
 */
const char *soap_expand_QNames (struct soap *soap, const char *s);

int soap_header_new (struct soap *soap, size_t size);

void soap_copy_local_namespaces (struct soap *src, struct soap *dst);

/**
 * gSOAP based allocator implementation
 *
 * @addtogroup APIgSOAPAllocatorImplementation gSOAP based allocator implementation
 * @ingroup APIAbsAllocatorImplementations
 *
 * @{
 */

/**
 * function to create a gSOAP based abstract allocator
 *
 * Don't call this function itself! But use it with ws4d_allocator_init() !
 * @code
 * ws4d_abs_allocator alloc;
 * struct soap soap;
 *
 * soap_init(&soap);
 * ws4d_eprlist_init(&alloc, soap_allocator_finit, &soap);
 * ws4d_eprlist_done(&alloc);
 * soap_done(&soap);
 * @endcode
 *
 * @param allocator allocator to initialize
 * @param arg argument for allocator, in this case an initialized gSOAP handle
 */
int soap_allocator_finit (struct ws4d_abs_allocator *allocator, void *arg);


/**
 * Function to get a gSOAP based allocator of a soap handle
 *
 * @code
 * ws4d_abs_allocator *alloc;
 * struct soap soap;
 *
 * soap_init(&soap);
 * alloc = soap_get_allocator(&soap);
 *
 * soap_done(&soap);
 * @endcode
 *
 * @param soap soap handle
 *
 * @
 */
struct ws4d_abs_allocator *soap_get_allocator(struct soap *soap);

/**
 * Macro to initialize an allocation list at run time
 *
 * @code
 * struct ws4d_abs_allocator alloclist;
 * struct soap_s soap;
 *
 * soap_init(&soap)
 * SOAP_ALLOCLIST_INIT(&alloclist, &soap);
 * @endcode
 *
 * @param head of allocation list
 * @param soaphandle soap handle to use for allocation
 */
#define SOAP_ALLOCLIST_INIT(head, soaphandle) \
  ws4d_allocator_init (head, soap_allocator_finit, soaphandle)

/** @} */

#define SOAP_PLUGIN_DUPLICATE 700

int soap_register_plugin_singleton (struct soap *soap,
                                    int (*fcreate) (struct soap *,
                                                    struct soap_plugin *,
                                                    void *), const char *id,
                                    void **plugin);

/** @} */

#ifdef __cplusplus
#if 0
{
#endif
}
#endif
#endif /* _MISC_H_ */
