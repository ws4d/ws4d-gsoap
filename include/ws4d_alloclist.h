/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef WS4D_ALLOCLIST_H_
#define WS4D_ALLOCLIST_H_

#include "ws4d_misc.h"

/**
 * list based allocator implementation
 *
 * @addtogroup APIws4dAllocatorImplementation list based allocator implementation
 * @ingroup APIAbsAllocatorImplementations
 *
 * @{
 */

typedef struct ws4d_alloc_list_s
{
  struct ws4d_list_node alist;
#ifdef WITH_MUTEXES
    WS4D_MUTEX (lock);
#endif
} ws4d_alloc_list;

int ws4d_allocator_finit (struct ws4d_abs_allocator *allocator, void *arg);

/**
 * Macro to initialize an allocation list at run time
 *
 * @code
 * struct ws4d_abs_allocator alloclist;
 *
 * WS4D_ALLOCLIST_INIT(&alloclist);
 * @endcode
 *
 * @param head of allocation list
 */
#define WS4D_ALLOCLIST_INIT(head) \
  ws4d_allocator_init (head, ws4d_allocator_finit, NULL)

/**
 * Function allocates memory and registers allocation in an allocation list.
 *
 * If allocation list is ommited this function behaves like ws4d_malloc()
 *
 * @code
 * ws4d_alloc_list alloclist;
 * void *buf = NULL;
 *
 * WS4D_ALLOCLIST_INIT(&alloclist);
 * buf = ws4d_malloc_alist(100, &alloclist);
 * @endcode
 *
 * @param size length of the buffer
 * @param alist allocation list to insert allocation
 *
 * @return pointer to newly-allocated buffer or NULL if no memory
 */
#define ws4d_malloc_alist(size, alist) \
  ws4d_allocator_alloc((alist), size)

/**
 * Function frees a allocation that is listed in an allocation list.
 *
 * @code
 * ws4d_alloc_list alloclist;
 * void *buf = NULL;
 *
 * WS4D_ALLOCLIST_INIT(&alloclist);
 * buf = ws4d_malloc_alist(100, &alloclist);
 * ws4d_free_alist(buf);
 * @endcode
 *
 * @param p pointer to allocation buffer to free and remove from
 * allocation list
 */
void ws4d_free_alist (void *p);

/**
 * Function frees all memory that is listed in an allocation list.
 *
 * @code
 * ws4d_alloc_list alloclist;
 * void *buf1 = NULL, *buf2 = NULL;
 *
 * WS4D_ALLOCLIST_INIT(&alloclist);
 * buf1 = ws4d_malloc_alist(100, &alloclist);
 * buf2 = ws4d_malloc_alist(300, &alloclist);
 * ws4d_alloclist_done(&alloclist);
 * @endcode
 *
 * @param alist allocation list to free
 */
#define ws4d_alloclist_done(alist) \
  ws4d_allocator_done((alist))

/**
 * TODO: add documentation
 */
int ws4d_strndup2 (const char *src, size_t size, const char **dst,
                   struct ws4d_abs_allocator *alist);

/**
 * TODO: add documentation
 */
int ws4d_strdup2 (const char *src, const char **dst,
                  struct ws4d_abs_allocator *alist);

/** @} */

#endif /*WS4D_ALLOCLIST_H_ */
