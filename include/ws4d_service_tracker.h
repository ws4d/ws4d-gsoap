/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 * service_tracker.h
 *
 *  Created on: 24.03.2010
 *      Author: elmex
 */

#ifndef WS4D_SERVICE_TRACKER_H_
#define WS4D_SERICE_TRACKER_H_

#include "ws4d_misc.h"
#include "ws4d_tracker_worker.h"

/**
 * @addtogroup APIServiceTracker Service Tracker API
 * @ingroup APITracker
 *
 * TODO: write documentation
 * @{
 */

struct ws4d_stracker_hooks
{
  ws4d_dt_newepr_cb NewService;
  ws4d_dt_invalidateepr_cb InvalidateService;
};

int ws4d_stracker_init (struct ws4d_stracker *stracker,
                        struct ws4d_dtracker *dtracker,
                        ws4d_qnamelist * types,
                        struct ws4d_stracker_hooks *hooks,
                        void *hooks_context);

int ws4d_stracker_done (struct ws4d_stracker *dtracker);

int ws4d_stracker_addservice (struct ws4d_stracker *stracker,
                              struct ws4d_epr *service);

struct ws4d_abs_eprlist *ws4d_stracker_getservices (struct ws4d_stracker
                                                    *stracker);

/** @} */

#endif /* WS4D_SERVICE_TRACKER_H_ */
