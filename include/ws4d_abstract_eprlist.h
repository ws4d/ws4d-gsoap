/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef _ABSTRACT_EPRLIST_H_
#define _ABSTRACT_EPRLIST_H_

#ifdef __cplusplus
extern "C"
{
#if 0
}
#endif
#endif

#include "ws4d_misc.h"
#include "ws4d_epr.h"

typedef int (*ws4d_eprt_matchingfunction) (struct ws4d_epr * epr,
                                           void *matchingdata,
                                           int iterations);

struct ws4d_abs_eprlist;

struct ws4d_abs_eprlist_cb
{
  void (*fdone) (struct ws4d_abs_eprlist * list);

  struct ws4d_epr *(*falloc) (struct ws4d_abs_eprlist * list);

  int (*ffree) (struct ws4d_abs_eprlist * list, struct ws4d_epr * epr);

  int (*fadd) (struct ws4d_abs_eprlist * list, struct ws4d_epr * epr);

  int (*fadd2) (struct ws4d_abs_eprlist * list, struct ws4d_epr * epr);

  int (*fremove) (struct ws4d_abs_eprlist * list, struct ws4d_epr * epr);

  int (*fclear) (struct ws4d_abs_eprlist * list);

  int (*fisempty) (struct ws4d_abs_eprlist * list);

  struct ws4d_epr *(*fget_first) (struct ws4d_abs_eprlist * list);

  struct ws4d_epr *(*fget_next) (struct ws4d_abs_eprlist * list,
                                 struct ws4d_epr * epr);

  struct ws4d_epr *(*fget_byMatch) (struct ws4d_abs_eprlist * list,
                                    ws4d_eprt_matchingfunction mf,
                                    void *mdata, int iteration);

  void (*flock) (struct ws4d_abs_eprlist * list);

  void (*funlock) (struct ws4d_abs_eprlist * list);
};

struct ws4d_abs_eprlist
{
  const char *id;
  void *data;
  struct ws4d_abs_eprlist_cb *callbacks;
};

/**
 * Abstract List of Endpoint References
 *
 * @addtogroup APIEndpointLists Abstract List of Endpoint References
 * @ingroup WS4D_LISTS
 *
 * @{
 */

/**
 * Filter functions for EPR lists
 *
 * This functions can be used as matching filter for ws4d_eprlist_filter(),
 * ws4d_eprlist_filterlen(), ws4d_eprlist_get_byMatch() functions
 *
 * @addtogroup APIEprFilter Filter functions for EPR lists
 * @ingroup APIEndpointLists
 *
 * @{
 */

/**
 * Function to match by Address string - uses strcmp
 */
int ws4d_epr_AddrsS_mf (struct ws4d_epr *epr, void *matchingdata, int iterations);

/**
 * Function to match by Address string list - uses ws4d_stringlist_compare()
 */
int ws4d_epr_AddrsL_mf (struct ws4d_epr *epr, void *matchingdata, int iterations);

/** @} */

/**
 * Function to initialize an Enpoint Reference List head
 *
 * @code
 * ws4d_abs_eprlist head;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 * @endcode
 *
 * @param head head of eprlist
 * @param finit function to create eprlist
 * @param arg argumets to finit function
 *
 * @return WS4D_OK on success, error code otherwise
 */
int ws4d_eprlist_init (struct ws4d_abs_eprlist *head,
                       int (*finit) (struct ws4d_abs_eprlist *
                                     head, void *arg), void *arg);


/**
 * Function to destroy abstract eprlist and free allocated memory
 *
 * @code
 * ws4d_abs_eprlist head;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 * ws4d_eprlist_done(&head);
 * @endcode
 *
 * @param head head of eprlist
 */
void ws4d_eprlist_done (struct ws4d_abs_eprlist *head);


/**
 * function to allocate a new element within an eprlist
 *
 * @code
 * ws4d_abs_eprlist head;
 * ws4d_epr *epr;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 * epr = ws4d_eprlist_alloc(&head);
 * ws4d_eprlist_done(&head);
 * @endcode
 *
 * @param head head of eprlist
 *
 * @return pointer to allocated epr structure or NULL on failure
 */
struct ws4d_epr *ws4d_eprlist_alloc (struct ws4d_abs_eprlist *head);


/**
 * function to free memory of an eprlist element
 *
 * @code
 * ws4d_abs_eprlist head;
 * ws4d_epr *epr;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 *
 * epr = ws4d_eprlist_alloc(&head);
 *
 * ws4d_eprlist_free(&head, epr);
 *
 * ws4d_eprlist_done(&head);
 * @endcode
 *
 * @param head head of eprlist
 * @param epr epr element to free
 *
 * @return WS4D_OK on success or error code otherwise
 */
int ws4d_eprlist_free (struct ws4d_abs_eprlist *head, struct ws4d_epr *epr);


/**
 * Function to add an epr to an eprlist
 *
 * @code
 * ws4d_abs_eprlist head;
 * struct ws4d_epr *epr;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 *
 * epr = ws4d_eprlist_alloc(&head);
 *
 * ws4d_eprlist_add(&head, epr);
 *
 * ws4d_eprlist_done(&head);
 * @endcode
 *
 * @param head head of eprlist
 * @param epr Endpoint Reference to add
 *
 * @return WS4D_OK on success WS4D_ERR otherwise
 */
int ws4d_eprlist_add (struct ws4d_abs_eprlist *head, struct ws4d_epr *epr);


/**
 * Function to add a copy of an epr to an eprlist
 *
 * @code
 * ws4d_abs_eprlist head;
 * struct ws4d_epr epr;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 *
 * ws4d_epr_init(&epr);
 *
 * ws4d_eprlist_add(&head, &epr);
 *
 * ws4d_eprlist_done(&head);
 * @endcode
 *
 * @param head head of eprlist
 * @param epr Endpoint Reference to add a copy of
 *
 * @return WS4D_OK on success WS4D_ERR otherwise
 */
int ws4d_eprlist_add2 (struct ws4d_abs_eprlist *head, struct ws4d_epr *epr);


/**
 * Function to remove an Endpoint Reference from an Endpoint Reference List
 *
 * @code
 * ws4d_abs_eprlist head;
 * struct ws4d_epr *epr;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 * epr = ws4d_eprlist_alloc(&head);
 * ws4d_eprlist_add(&head, epr);
 *
 * ws4d_eprlist_remove(&head, epr);
 *
 * ws4d_eprlist_done(&head);
 * @endcode
 *
 * @param head head of eprlist
 * @param epr Endpoint Reference to remove
 * @return WS4D_OK on success WS4D_ERR otherwise
 */
int ws4d_eprlist_remove (struct ws4d_abs_eprlist *head, struct ws4d_epr *epr);


/**
 * Function to check if an eprlist is empty
 *
 * @code
 * ws4d_abs_eprlist head;
 * struct ws4d_epr *epr;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 *
 * if (ws4d_eprlist_isempty(&head))
 * {
 *   //eprlist is empty
 * }
 *
 * ws4d_eprlist_done(&head);
 * @endcode
 *
 * @param head head of eprlist
 */
int ws4d_eprlist_isempty (struct ws4d_abs_eprlist *head);


/**
 * Function to clear an eprlist
 *
 * @code
 * ws4d_abs_eprlist head;
 * struct ws4d_epr *epr;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 *
 * epr = ws4d_eprlist_alloc(&head);
 * ws4d_eprlist_add(&head, epr);
 *
 * if (!ws4d_eprlist_isempty(&head))
 * {
 *   //eprlist is not empty
 * }
 *
 * ws4d_eprlist_clear(&head, epr);
 *
 * if (ws4d_eprlist_isempty(&head))
 * {
 *   //eprlist is empty
 * }
 *
 * ws4d_eprlist_done(&head);
 * @endcode
 *
 * @param head head of eprlist
 *
 * @return WS4D_OK on success, error code otherwise
 */
int ws4d_eprlist_clear (struct ws4d_abs_eprlist *head);


/**
 * Function to filter a epr list and save the result in another epr list
 *
 * @code
 * @endcode
 *
 * @param dst list to put eprs
 * @param src list to filter
 * @param mf matching function for filter
 * @param mdata data passed to matching function
 * @param copies number of copied eprs
 * @return WS4D_OK on success, error code otherwise
 */
int ws4d_eprlist_filter (struct ws4d_abs_eprlist *dst,
                         struct ws4d_abs_eprlist *src,
                         ws4d_eprt_matchingfunction mf, void *mdata,
                         int *copies);


/**
 * Function to filter a epr list and save the result in another epr list. The
 * difference to ws4d_eprlist_filter() is that this function does not clear the
 * destination list. So this can be used to update the eprs of to lists.
 *
 * @code
 * @endcode
 *
 * @param dst list to put eprs
 * @param src list to filter
 * @param mf matching function for filter
 * @param mdata data passed to matching function
 * @param copies number of copied eprs
 * @return WS4D_OK on success, error code otherwise
 */
int
ws4d_eprlist_update (struct ws4d_abs_eprlist *dst,
                     struct ws4d_abs_eprlist *src,
                     ws4d_eprt_matchingfunction mf, void *mdata, int *copies);


/**
 * Function to get the number of elements that match a specific filter
 *
 * @code
 * @endcode
 *
 * @param head list to filter
 * @param mf matching function for filter
 * @param mdata data passed to matching function
 * @param copies number of copied eprs
 * @return WS4D_OK on success, error code otherwise
 */
int ws4d_eprlist_filterlen (struct ws4d_abs_eprlist *head,
                            ws4d_eprt_matchingfunction mf, void *mdata);


/**
 * Function to copy an epr list
 *
 * @code
 * @endcode
 *
 * @param dst copy destination list
 * @param src copy source list
 * @return WS4D_OK on success, error code otherwise
 */
#define ws4d_eprlist_copy(__dest, __src) \
    ws4d_eprlist_filter(__dest, __src, NULL, NULL, NULL)


/**
 * Function to search for a specific epr in an epr list
 *
 * @see APIEprFilter for available matching functions
 *
 * @param head epr list to search
 * @param mf matching function
 * @param mdata data passed to matching function
 * @return first matching epr, or NULL if no matching epr was found
 */
#define ws4d_eprlist_get_byMatch(__head, __mf, __mdata) \
    ws4d_eprlist_get_byMatch2(__head, __mf, __mdata, 0)


/**
 * Function to search for a specific epr in an epr list
 *
 * @see APIEprFilter for available matching functions
 *
 * @param head epr list to search
 * @param mf matching function
 * @param mdata data passed to matching function
 * @param iterations number of iterations to traverse list
 * @return first matching epr, or NULL if no matching epr was found
 */
struct ws4d_epr *ws4d_eprlist_get_byMatch2 (struct ws4d_abs_eprlist *head,
                                            ws4d_eprt_matchingfunction mf,
                                            void *mdata, int iterations);


/**
 * Function to get first element of an eprlist
 *
 * @code
 * ws4d_abs_eprlist head;
 * struct ws4d_epr *epr, *first;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 *
 * epr = ws4d_eprlist_alloc(&head);
 * ws4d_eprlist_add(&head, epr);
 *
 * first = ws4d_eprlist_get_first(&head);
 *
 * ws4d_eprlist_done(&head);
 * @endcode
 *
 * @param head head of eprlist
 *
 * @return pointer to first element of eprlist, NULL on failure
 */
struct ws4d_epr *ws4d_eprlist_get_first (struct ws4d_abs_eprlist *head);


/**
 * Function to get the next element of an eprlist after a specific epr
 *
 * @code
 * ws4d_abs_eprlist head;
 * struct ws4d_epr *epr1, *epr2, *first, *next;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 *
 * epr1 = ws4d_eprlist_alloc(&head);
 * ws4d_eprlist_add(&head, epr1);
 *
 * epr2 = ws4d_eprlist_alloc(&head);
 * ws4d_eprlist_add(&head, epr2);
 *
 * first = ws4d_eprlist_get_first(&head);
 * next = ws4d_eprlist_get_next(&head, first);
 *
 * ws4d_eprlist_done(&head);
 * @endcode
 *
 * @param head head of eprlist
 * @param epr element of eprlist
 *
 * @return pointer to epr after the on specified or NULL on failure
 */
struct ws4d_epr *ws4d_eprlist_get_next (struct ws4d_abs_eprlist *head,
                                        struct ws4d_epr *epr);


/**
 * Function to search for an epr by address
 *
 * @code
 * ws4d_abs_eprlist head;
 * struct ws4d_epr *epr, *found;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 *
 * epr = ws4d_eprlist_alloc(&head);
 * ws4d_epr_set_Addrs(epr, "urn:uuid:[uuid]");
 * ws4d_eprlist_add(&head, epr);
 *
 * found = ws4d_eprlist_get_byAddr(&head, "urn:uuid:[uuid]");
 *
 * ws4d_eprlist_done(&head);
 * @endcode
 *
 * @param head head of eprlist
 * @param Addr address of epr
 *
 * @return pointer to epr with specified address or NULL on otherwise
 */
#define ws4d_eprlist_get_byAddr(__head, __Addr) \
    ws4d_eprlist_get_byMatch(__head, ws4d_epr_AddrsS_mf, (void *) __Addr)

/**
 * Macro to iterate over all eprs in an eprlist
 *
 * @code
 * ws4d_abs_eprlist head;
 * struct ws4d_epr *epr1, *epr2, *epr, *iter;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 *
 * epr1 = ws4d_eprlist_alloc(&head);
 * ws4d_eprlist_add(&head, epr1);
 *
 * epr2 = ws4d_eprlist_alloc(&head);
 * ws4d_eprlist_add(&head, epr2);
 *
 * ws4d_eprlist_foreach(epr, iter, &head)
 * {
 *    //do something with epr
 * }
 *
 * ws4d_eprlist_done(&head);
 * @endcode
 */
#define ws4d_eprlist_foreach(epr, iter, list) \
for ( epr = ws4d_eprlist_get_first((list)), iter = ws4d_eprlist_get_next((list), epr); \
    epr; \
    epr = iter, iter = ws4d_eprlist_get_next((list), iter))

#ifdef WITH_MUTEXES
void ws4d_eprlist_lock (struct ws4d_abs_eprlist *head);

void ws4d_eprlist_unlock (struct ws4d_abs_eprlist *head);
#else

/**
 * Macro to lock an eprlist for mutual exclusion
 *
 * @code
 * ws4d_abs_eprlist head;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 *
 * ws4d_eprlist_lock(&head);
 *
 * ws4d_eprlist_done(&head);
 * @endcode
 */
#define ws4d_eprlist_lock(head)

/**
 * Macro to unlock an eprlist for mutual exclusion
 *
 * @code
 * ws4d_abs_eprlist head;
 *
 * ws4d_eprlist_init(&head, ws4d_eprllist_init, NULL);
 *
 * ws4d_eprlist_lock(&head);
 * ws4d_eprlist_unlock(&head);
 *
 * ws4d_eprlist_done(&head);
 * @endcode
 */
#define ws4d_eprlist_unlock(head)
#endif

/** @} */

/**
 * Endpoint Reference List Implementations
 *
 * @addtogroup APIEndpointListImplementation Implementations
 * @ingroup APIEndpointLists
 *
 * @{
 */

/** @} */

#ifdef __cplusplus
#if 0
{
#endif
}
#endif

#endif /*_ABSTRACT_EPRLIST_H_*/
