/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef WS4D_SERVICE_H_
#define WS4D_SERVICE_H_

#include "ws4d_mutex.h"
#include "ws4d_epr.h"
#include "ws4d_qname.h"
#include "ws4d_abstract_eprlist.h"

#ifdef __cplusplus
extern "C"
{
#if 0
}
#endif
#endif

/**
 * Service Endpoint Reference
 *
 * @addtogroup APIServiceEP Service Endpoint
 * @ingroup WS4D_UTILS
 *
 *
 * @{
 */


/**
 * Adds a type to a service
 *
 * @code
 * struct ws4d_epr epr;
 * struct ws4d_qname *qname = {{NULL, NULL}, NULL, "exampletype1", "http://www.ws4d.org/ns/examplens1"};
 *
 * ws4d_epr_init(&epr);
 *
 * ws4d_serviceep_addtype(&epr, type);
 *
 * ws4d_epr_reset(&epr);
 * @endcode
 *
 * @param service service to add a type
 * @param type service type to add as struct ws4d_qname
 *
 * @return WS4D_OK on success, an error code otherwise
 */
#define ws4d_serviceep_addtype(__service, __type) \
    ws4d_qnamelist_add(ws4d_serviceep_gettypelist((__service)), (__type))


/**
 * Adds types passed in string representation to a service
 *
 * @code
 * struct ws4d_epr epr;
 * char *qname_list = "\"http://www.ws4d.org/ns/examplens1\":exampletype1 \"http://www.ws4d.org/ns/examplens2\":exampletype2";
 *
 * ws4d_epr_init(&epr);
 *
 * ws4d_serviceep_addtypestr(&epr, qname_list);
 *
 * ws4d_epr_reset(&epr);
 * @endcode
 *
 * @param service service to add types
 * @param types to add as white space separated list
 *
 * @return WS4D_OK on success, an error code otherwise
 */
#define ws4d_serviceep_addtypestr(__service, __types) \
    ws4d_qnamelist_addstring(ws4d_serviceep_gettypelist((__service)), (__types))


/**
 * Returns list of types of a service as string
 *
 * @code
 * struct ws4d_epr epr;
 * char *qname_list = "\"http://www.ws4d.org/ns/examplens1\":exampletype1 \"http://www.ws4d.org/ns/examplens2\":exampletype2";
 * register struct ws4d_qname *cur, *iter;
 *
 * ws4d_epr_init(&epr);
 *
 * ws4d_serviceep_addtypestr(&epr, qname_list);
 * printf("Type: %s\n", ws4d_serviceep_gettypestr(&epr));
 *
 * ws4d_epr_reset(&epr);
 * @endcode
 *
 * @param service service to get list of types
 *
 * @return qname list on success, NULL otherwise
 */
#define ws4d_serviceep_gettypestr(__service) \
    ws4d_qnamelist_tostring (ws4d_serviceep_gettypelist((__service)))


/**
 * Returns list of types of a service as qname list
 *
 * @code
 * struct ws4d_epr epr;
 * char *qname_list = "\"http://www.ws4d.org/ns/examplens1\":exampletype1 \"http://www.ws4d.org/ns/examplens2\":exampletype2";
 * register struct ws4d_qname *cur, *iter;
 *
 * ws4d_epr_init(&epr);
 *
 * ws4d_serviceep_addtypestr(&epr, qname_list);
 * ws4d_qname_foreach(cur, iter, ws4d_serviceep_gettypelist(epr))
 * {
 *   printf("Type: (%s):%s\n", ws4d_qname_getNS(cur), ws4d_qname_getName(cur));
 * }
 *
 * ws4d_epr_reset(&epr);
 * @endcode
 *
 * @param service service to get list of types
 *
 * @return qname list on success, NULL otherwise
 */
struct ws4d_qnamelist *ws4d_serviceep_gettypelist (struct ws4d_epr *service);


/**
 * Set the ID of a service
 *
 * @code
 * struct ws4d_epr epr;
 *
 * ws4d_epr_init(&epr);
 *
 * ws4d_serviceep_setid(&epr, "http://www.ws4d.org/srvs/ExampleService1");
 *
 * ws4d_epr_reset(&epr);
 * @endcode
 *
 * @param service service to get id
 * @param id id to set
 *
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_serviceep_setid (struct ws4d_epr *service, const char *id);


/**
 * Get the ID of a service
 *
 * @code
 * struct ws4d_epr epr;
 *
 * ws4d_epr_init(&epr);
 *
 * ws4d_serviceep_setid(&epr, "http://www.ws4d.org/srvs/ExampleService1");
 *
 * printf("ServiceID: %s\n", ws4d_serviceep_getid(epr));
 *
 * ws4d_epr_reset(&epr);
 * @endcode
 *
 * @param service service to get id
 *
 * @return ID of service on success, or NULL otherwise
 */
const char *ws4d_serviceep_getid (struct ws4d_epr *service);


/**
 * Set the device epr related to a service
 *
 * @code
 * struct ws4d_epr epr, device;
 *
 * ws4d_epr_init(&epr);
 * ws4d_epr_init(&device);
 *
 * ws4d_serviceep_setdevice(&epr, &device);
 *
 * ws4d_epr_done(&epr);
 * ws4d_epr_done(&device);
 * @endcode
 *
 * @param service service to set related device
 * @param device related device to set
 *
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_serviceep_setdevice (struct ws4d_epr *epr, struct ws4d_epr *device);


/**
 * Get the device epr related to a service
 *
 * @code
 * struct ws4d_epr epr, device, *device_epr;
 *
 * ws4d_epr_init(&epr);
 * ws4d_epr_init(&device);
 *
 * ws4d_serviceep_setdevice(&epr, &device);
 * device_epr = ws4d_serviceep_getdevice(&epr);
 *
 * ws4d_epr_done(&epr);
 * ws4d_epr_done(&device);
 * @endcode
 *
 * @param service service epr to get related device
 *
 * @return pointer to ws4d_epr structure on success, or NULL otherwise
 */
struct ws4d_epr *ws4d_serviceep_getdevice (struct ws4d_epr *epr);


/**
 * Match types with types of an service
 *
 * @code
 * struct ws4d_epr epr;
 * char *qname_list = "\"http://www.ws4d.org/ns/examplens1\":exampletype1 \"http://www.ws4d.org/ns/examplens2\":exampletype2";
 *
 * ws4d_epr_init(&epr);
 *
 * ws4d_serviceep_addtypestr(&epr, qname_list);
 *
 * if (ws4d_serviceep_matchesTypes(&epr, "\"http://www.ws4d.org/ns/examplens1\":exampletype1")
 * {
 *   printf("Types are matching\n");
 * }
 * else
 * {
 *   printf("Types are NOT matching\n");
 * }
 *
 * ws4d_epr_reset(&epr);
 * @endcode
 *
 * @param service service to match types
 * @param types types to match
 *
 * @return returns 1 on match, 0 otherwise
 */
#define ws4d_serviceep_matchesTypes(__service, __types) \
    (!ws4d_qnamelist_compare(__types, ws4d_serviceep_gettypelist(__service)))


/**
 * Function to match by service id
 *
 * @ingroup APIEprFilter
 */
int ws4d_serviceep_id_mf(struct ws4d_epr *epr, void *matchingdata, int iterations);


/**
 * Function to search for service in epr list by service ID
 */
#define ws4d_eprlist_getByServiceId(__head, __serviceid) \
  ws4d_eprlist_get_byMatch2(__head, ws4d_serviceep_id_mf, (void *) __serviceid, 1)


/**
 * Set WSDL of an service
 *
 * @param epr service to set wsdl
 * @param wsdl wsdl
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_serviceep_set_wsdl (struct ws4d_epr *epr, const char *wsdl);

/**
 * Get WSDL of an service
 *
 * @param epr service to get WSDL
 * @return pointer to service WSDL on success or NULL otherwise
 */
const char *ws4d_serviceep_get_wsdl (struct ws4d_epr *epr);

/**
 * Set extension elements of an service
 *
 * This function can be used to add extensions to the element corresponding to
 * the Relationship meta data of this service.
 *
 * @param epr service to set extension elements
 * @param ext ext
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_serviceep_set_ext (struct ws4d_epr *epr, const char *ext);

/**
 * Get extension elements of an service
 *
 * @param epr service to get the extension elements
 * @return pointer to service extension elements on success or NULL otherwise
 */
const char *ws4d_serviceep_get_ext (struct ws4d_epr *service);

/**
 * Set web service toolkit specific information for transport
 *
 * This function is internally used for the ws toolkit abstraction to relate a
 * service with a ws toolkit specific service structure. When you use the gsoap
 * toolkit this is used to relate a gsoap handle to a service.
 *
 * @param epr service to set ws toolkit specific data
 * @param transport_data pointer to ws toolkit specific data structure
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_serviceep_set_transportdata (struct ws4d_epr *epr,
                                   void *transport_data);

/**
 * Get web service toolkit specific information for transport
 *
 * This function is internally used for the ws toolkit abstraction to relate a
 * service with a ws toolkit specific service structure. When you use the gsoap
 * toolkit this is used to relate a gsoap handle to a service.
 *
 * @param epr service to get ws toolkit specific data
 * @return pointer to ws toolkit specific data structure on success, NULL otherwise
 */
void *ws4d_serviceep_get_transportdata (struct ws4d_epr *epr);

/**
 * Activate a service
 *
 * Services have the state active or inactive. Only active services are published
 * within the Relationship metadata. If you create new services they are
 * inactive by default until you activate them.
 *
 * @param epr service to activate
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_serviceep_activate (struct ws4d_epr *epr);

/**
 * Deactivate a service
 *
 * Services have the state active or inactive. Only active services are published
 * within the Relationship metadata. If you create new services they are
 * inactive by default until you activate them.
 *
 * @param epr service to deactivate
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_serviceep_deactivate (struct ws4d_epr *epr);

/**
 * Test if an service is active
 *
 * @param epr service to test
 * @return 1 if service is active, !1 otherwise.
 */
int ws4d_serviceep_isactive (struct ws4d_epr *epr);

/** @} */

#ifdef __cplusplus
#if 0
{
#endif
}
#endif

#endif /*WS4D_SERVICE_H_ */
