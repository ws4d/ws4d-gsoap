/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "ws4d_target.h"

#ifndef WS4D_TARGETSERVICE_H_
#define WS4D_TARGETSERVICE_H_

/**
 * Target Service
 *
 * @addtogroup APITargetService Target Service
 * @ingroup APITargetEP
 *
 * @{
 */

struct ws4d_appsequence
{
  unsigned int InstanceId;
  const char *SequenceId;
  unsigned int MessageNumber;
};

struct ws4d_targetservice
{
  struct ws4d_abs_eprlist targets;
  struct ws4d_appsequence as;
  struct ws4d_list_node MatchBy;
  struct ws4d_abs_allocator alist;
};


/**
 * Function to initialize a target service structure
 */
int ws4d_targetservice_init (struct ws4d_targetservice *ts,
                             const char *defMatchBy,
                             ws4d_stringlist_matchingfunction defscope_match_func);


/**
 * Function to destroy a target service structure
 */
int ws4d_targetservice_done (struct ws4d_targetservice *ts);


/**
 * Function to add a scope matching function to the target service
 */
int
ws4d_targetservice_addMatchBy (struct ws4d_targetservice *ts,
                               const char *MatchBy,
                               ws4d_stringlist_matchingfunction scope_match_func);


/**
 * Function to remove a scope matching function from the target service
 */
int
ws4d_targetservice_delMatchBy (struct ws4d_targetservice *ts,
                               const char *MatchBy);


/**
 * Function to initialize a target advertised by a target service
 */
struct ws4d_epr *ws4d_targetservice_inittarget (struct ws4d_targetservice *ts,
                                                const char *Addrs,
                                                const char *XAddrs);

/**
 * Function to remove a target from the target service
 */
int ws4d_targetservice_deltarget (struct ws4d_targetservice *ts,
                                  struct ws4d_epr *target);


/**
 * Function to get list of targets registered at a target service
 */
#define ws4d_targetservice_get_targetList(__targetservice) \
    &(__targetservice)->targets


/**
 * Function to find a specific target by address at a target service
 */
#define ws4d_targetservice_gettarget_byAddr(__targetservice, __addr) \
    ws4d_eprlist_get_byAddr(ws4d_targetservice_get_targetList(__targetservice), __addr)


/**
 * Function to get target endpoints that match specific scopes and types
 */
int ws4d_targetservice_getmatches (struct ws4d_targetservice *ts,
                                   const char *Scopes,
                                   const char *MatchBy,
                                   const char *Types,
                                   struct ws4d_abs_eprlist *matches);

/**
 * Function to increase the message number used in message headers
 */
int ws4d_targetservice_inc_MessageNumber (struct ws4d_targetservice *ts);

/**
 * Function to get the app sequence used in message headers
 */
struct ws4d_appsequence
  *ws4d_targetservice_get_appsequence (struct ws4d_targetservice *ts);

/** @} */

#endif /*WS4D_TARGETSERVICE_H_ */
