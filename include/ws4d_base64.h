/*
 * base64.h
 *
 *  Created on: 23.07.2010
 *      Author: su009
 *
 *  offers routines for de-/encoding base64
 */

#ifndef BASE64_H_
#define BASE64_H_

#include <stdlib.h>
void decodeblock (unsigned char in[4], unsigned char out[3]);
void encodeblock (unsigned char in[3], unsigned char out[4], int len);

/*
** ws4d_bin2b64
**
** base64 encode a stream adding padding and line breaks as per spec.
*/
unsigned char *ws4d_bin2b64 (unsigned char *binary);

/*
** ws4d_b642bin
**
** decode a base64 encoded stream discarding padding, line breaks and noise
*/
unsigned char *ws4d_b642bin (unsigned char *base64);


#endif /* BASE64_H_ */
