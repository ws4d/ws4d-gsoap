/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef WSMETADATAEXCHANGE_H_
#define WSMETADATAEXCHANGE_H_

#ifdef __cplusplus
extern "C"
{
#if 0
}
#endif
#endif


#include "soap_misc.h"

/**
 * gSoap WS-Transer/Matadataexchange Plugin API
 *
 * @addtogroup MEXAPI Metadata-Exchange-API
 * @ingroup DPWS_WS_MODULES
 *
 * @{
 */


typedef void (*fserializeType) (struct soap *, const void *);
typedef int (*foutType) (struct soap *, const char *, int, const void *,
                         const char *);
typedef void *(*finType) (struct soap *, const char *, void *, const char *);

#define MEX_CAST_FSERIALIZE(func) ((fserializeType) func)
#define MEX_CAST_FOUT(func) ((foutType) func)
#define MEX_CAST_FIN(func) ((finType) func)

/**
 * function prototypes for metadata section serializers
 */
struct mex_section_type_serializers
{
  fserializeType serialize;
  foutType out;
  finType in;
};

/**
 * serializers for literal metadata sections
 */
extern const struct mex_section_type_serializers mex_literal_serializers;


/**
 * function to initialize the metadata exchange (mex) plugin at a soap handle
 *
 * @param soap handle to initialize
 *
 * @result SOAP_OK on success or error on failure
 */
int mex_init (struct soap *soap);


/**
 * function to register a metadata section type
 *
 * @param soap handle with mex plugin
 * @param dialect dialect of section type
 * @param tag QName of section content
 * @param serializers pointer to structure of serializer functions
 *
 * @result SOAP_OK on success or error on failure
 */
int mex_register_metadata_sectiontype (struct soap *soap, const char *dialect,
                                       const char *tag,
                                       const struct
                                       mex_section_type_serializers
                                       *serializers);


/**
 * function to remove a metadata section type
 *
 * @param soap handle with mex plugin
 * @param dialect dialect of section type
 *
 * @result SOAP_OK on success or error on failure
 */
int mex_remove_metadata_section_type (struct soap *soap, const char *dialect);


/**
 * function to register a metadata section
 *
 * @param soap handle with mex plugin
 * @param endpoint endpoint where section is served
 * @param dialect dialect of previously registered section type
 * @param identifier identifier of section (optional, can be NULL)
 * @param content content of section
 *
 * @result SOAP_OK on success or error on failure
 */
int mex_register_metadata_section (struct soap *soap, const char *endpoint,
                                   const char *dialect,
                                   const char *identifier, void *content);


/**
 * function to get metadata section based on endpoint, dialect and identifier
 *
 * @param soap handle with mex plugin
 * @param endpoint endpoint where section is served
 * @param dialect dialect of section
 * @param identifier identifier of section (optional, can be NULL)
 *
 * @result point to section content on success or NULL on failure
 */
void *mex_get_metadata_section (struct soap *soap, const char *endpoint,
                                const char *dialect, const char *identifier);


/**
 * function to remove a metadata section based on endpoint, dialect and identifier
 *
 * @param soap handle with mex plugin
 * @param endpoint endpoint where section is served
 * @param dialect dialect of section
 * @param identifier identifier of section (optional, can be NULL)
 *
 * @result SOAP_OK on success or error on failure
 */
int
mex_remove_metadata_section (struct soap *soap, const char *endpoint,
                             const char *dialect, const char *identifier);


/**
 * function to process a wst:TransferGet request an generate response
 *
 * @param soap handle with mex plugin
 * @param endpoint endpoint where request was directed to
 * @param GetResponseMsg pointer to response message structure to fill
 *
 * @result SOAP_OK on success or error on failure
 */
int
mex_process_TransferGet (struct soap *soap, const char *endpoint,
                         struct _wsm__Metadata *GetResponseMsg);


/**
 * function to process a mex:GetMetadata request an generate response
 *
 * @param soap handle with mex plugin
 * @param endpoint endpoint where request was directed to
 * @param dialect dialect as specified in request
 * @param identifier identifier as specified in request
 * @param GetResponseMsg pointer to response message structure to fill
 *
 * @result SOAP_OK on success or error on failure
 */
int
mex_process_GetMetadata (struct soap *soap, const char *endpoint,
                         const char *dialect, const char *identifier,
                         struct _wsm__Metadata *GetResponseMsg);

#ifdef DEVPROF_2009_01
/**
 * function to call a mex:GetMetadata method
 *
 * @param soap handle with mex plugin
 * @param MsgId message id for request
 * @param Address transport address of endpoint
 * @param To endpoint address
 * @param Dialect dialect to request
 * @param Identifier indentifier to request
 * @param metadata pointer to response message structure to fill
 * @param timeout timout for method call
 *
 * @return SOAP_OK on success or SOAP_ERR otherwise
 */
int mex_GetMetadata (struct soap *soap, char *MsgId, const char *Address,
                     const char *To, const char *Dialect,
                     const char *Identifier, struct _wsm__Metadata *metadata,
                     ws4d_time timeout);
#endif

/**
 * function to call a wst:TransferGet method
 *
 * @param soap handle with mex plugin
 * @param MsgId message id for request
 * @param Address transport address of endpoint
 * @param To endpoint address
 * @param metadata pointer to response message structure to fill
 * @param timeout timout for method call
 *
 * @return SOAP_OK on success or SOAP_ERR otherwise
 */
int mex_TransferGet (struct soap *soap, char *MsgId, const char *Address,
                     const char *To, struct _wsm__Metadata *metadata,
                     ws4d_time timeout);


#ifndef DOXYGEN_SHOULD_SKIP_THIS

/**
 * custom serializers for metadata exchange
 */

void
soap_default__wsm__MetadataSection (struct soap *soap,
                                    struct _wsm__MetadataSection *a);

void
soap_serialize__wsm__MetadataSection (struct soap *soap,
                                      const struct _wsm__MetadataSection *a);

int
soap_out__wsm__MetadataSection (struct soap *soap, const char *tag, int id,
                                const struct _wsm__MetadataSection *a,
                                const char *type);

int
soap_put__wsm__MetadataSection (struct soap *soap,
                                const struct _wsm__MetadataSection *a,
                                const char *tag, const char *type);

struct _wsm__MetadataSection *soap_in__wsm__MetadataSection (struct soap
                                                             *soap,
                                                             const char *tag,
                                                             struct
                                                             _wsm__MetadataSection
                                                             *a,
                                                             const char
                                                             *type);

struct _wsm__MetadataSection *soap_get__wsm__MetadataSection (struct soap
                                                              *soap,
                                                              struct
                                                              _wsm__MetadataSection
                                                              *p,
                                                              const char *tag,
                                                              const char
                                                              *type);

#endif

/** @} */

#ifdef __cplusplus
#if 0
{
#endif
}
#endif

#endif /*WSMETADATAEXCHANGE_H_ */
