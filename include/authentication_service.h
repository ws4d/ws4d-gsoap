/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * authentication_service.h
 *
 *  Created on: 08.03.2010
 *      Author: cht
 */

#ifndef AUTHENTICATION_SERVICE_H_
#define AUTHENTICATION_SERVICE_H_

#ifdef __cplusplus
extern "C"
{
#endif

  int
    setup_AuthenticationService (struct dpws_s *device, struct soap *handle,
                                 const char *wsdl, int backlog);

  int auths_serve_request (struct soap *soap);

#ifdef __cplusplus
}
#endif

#endif                          /* AUTHENTICATION_SERVICE_H_ */
