/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef WS4D_QNAME_H_
#define WS4D_QNAME_H_

/**
 * Qalified Name
 *
 * @addtogroup APIQName Qualified Name
 * @ingroup WS4D_UTILS
 *
 * @{
 */

/**
 * Structure of QName
 */
struct ws4d_qname
{
  struct ws4d_list_node list;
  const char *prefix;
  const char *name;
  const char *ns;
};


/**
 * initialize a qname
 *
 * @code
 * struct ws4d_qname qname;
 *
 * ws4d_qname_init(&qname);
 * @endcode
 *
 * @param qname qname structure to initialize
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_qname_init (struct ws4d_qname *qname);


/**
 * destroy an qname structure
 *
 * @code
 * struct ws4d_qname qname;
 *
 * ws4d_qname_init(&qname);
 * ws4d_qname_done(&qname);
 * @endcode
 *
 * @param qname qname structure to initialize
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_qname_done (struct ws4d_qname *qname);


/**
 * Macro to set the name of a QName
 *
 * @code
 * struct ws4d_qname qname;
 *
 * ws4d_qname_init(&qname);
 *
 * ws4d_qname_setName(&qname, "device");
 *
 * ws4d_qname_done(&qname);
 * @endcode
 *
 * @param qname qname structure to set name
 * @param val name to set
 */
#define ws4d_qname_setName(__qname, __val) (__qname)->name = __val


/**
 * Macro to get the name of a QName
 *
 * @code
 * struct ws4d_qname qname;
 *
 * ws4d_qname_init(&qname);
 *
 * ws4d_qname_setName(&qname, "device");
 * printf("QName name: %s\n", ws4d_qname_getName(&qname));
 *
 * ws4d_qname_done(&qname);
 * @endcode
 *
 * @param qname qname structure to get name
 */
#define ws4d_qname_getName(__qname) (__qname)->name


/**
 * Macro to set the prefix of a QName
 *
 * @code
 * struct ws4d_qname qname;
 *
 * ws4d_qname_init(&qname);
 *
 * ws4d_qname_setPrefix(&qname, "ws4d");
 *
 * ws4d_qname_done(&qname);
 * @endcode
 *
 * @param qname qname structure to set prefix
 * @param val name to set
 */
#define ws4d_qname_setPrefix(__qname, __val) (__qname)->prefix = __val


/**
 * Macro to get the prefix of a QName
 *
 * @code
 * struct ws4d_qname qname;
 *
 * ws4d_qname_init(&qname);
 *
 * ws4d_qname_setPrefix(&qname, "ws4d");
 * printf("QName prefix: %s\n", ws4d_qname_getPrefix(&qname));
 *
 * ws4d_qname_done(&qname);
 * @endcode
 *
 * @param qname qname structure to get prefix
 */
#define ws4d_qname_getPrefix(__qname) (__qname)->prefix


/**
 * Macro to set the namespace of a QName
 *
 * @code
 * struct ws4d_qname qname;
 *
 * ws4d_qname_init(&qname);
 *
 * ws4d_qname_setNS(&qname, "http://www.ws4d.org/testns");
 *
 * ws4d_qname_done(&qname);
 * @endcode
 *
 * @param qname qname structure to set namespace
 * @param val name to set
 */
#define ws4d_qname_setNS(__qname, __val) (__qname)->ns = __val


/**
 * Macro to get the namespace of a QName
 *
 * @code
 * struct ws4d_qname qname;
 *
 * ws4d_qname_init(&qname);
 *
 * ws4d_qname_setNS(&qname, "http://www.ws4d.org/testns");
 * printf("QName namespace: %s\n", ws4d_qname_getNS(&qname));
 *
 * ws4d_qname_done(&qname);
 * @endcode
 *
 * @param qname qname structure to get namespace
 */
#define ws4d_qname_getNS(__qname) (__qname)->ns


/**
 * Function to generate string representation of QName
 *
 * @code
 * struct ws4d_qname qname;
 * char buffer[100] = "", *qnamestr;
 *
 * ws4d_qname_init(&qname);
 *
 * ws4d_qname_setPrefix(&qname, "ws4d");
 * ws4d_qname_setName(&qname, "device");
 *
 * qnamestr = ws4d_qname_tostring2(&qname, buffer, 100);
 * printf("QName: %s\n", qnamestr);
 *
 * ws4d_qname_done(&qname);
 * @endcode
 *
 * @param qname qname structure to generate string
 * @param buffer pointer to buffer to store string
 * @param size size of buffer
 * @returns pointer to qname string or NULL on error
 */
char *ws4d_qname_tostring2 (struct ws4d_qname *qname, char *buffer,
                            size_t size);


/**
 * Function to calculate the string length of a qname returned by ws4d_qname_tostring2()
 *
 * @code
 * struct ws4d_qname qname;
 * char *buffer, *qnamestr;
 * size_t size;
 *
 * ws4d_qname_init(&qname);
 *
 * ws4d_qname_setPrefix(&qname, "ws4d");
 * ws4d_qname_setName(&qname, "device");
 *
 * size = ws4d_qname_strlen(&qname)
 * buffer = malloc(size);
 *
 * qnamestr = ws4d_qname_tostring2(&qname, buffer, size);
 * printf("QName: %s\n", qnamestr);
 *
 * free(buffer);
 * ws4d_qname_done(&qname);
 * @endcode
 *
 * @param qname qname structure to generate string length
 * @returns size of qname string
 */
size_t ws4d_qname_strlen (struct ws4d_qname *qname);


/**
 * Function to match qnames
 *
 * @code
 * struct ws4d_qname qname1, qname2;
 *
 * ws4d_qname_init(&qname1);
 * ws4d_qname_init(&qname2);
 *
 * ws4d_qname_setPrefix(&qname1, "ws4d");
 * ws4d_qname_setName(&qname1, "device");
 *
 * ws4d_qname_setPrefix(&qname2, "ws4d");
 * ws4d_qname_setName(&qname2, "device");
 *
 * if (ws4d_qname_match(qname1, qname2))
 * {
 *   // qnames match
 * }
 *
 * ws4d_qname_done(&qname1);
 * ws4d_qname_done(&qname2);
 * @endcode
 *
 * @param q1 qname structure
 * @param q2 qname structure
 * @returns 1 on match, 0 otherwise
 */
int ws4d_qname_match (struct ws4d_qname *q1, struct ws4d_qname *q2);


/**
 * Function to duplicate a qname
 *
 * @code
 * struct ws4d_qname qname, *copy;
 * ws4d_abs_allocator alist;
 *
 * WS4D_ALLOCLIST_INIT(&alist);
 * ws4d_qname_init(&qname);
 *
 * ws4d_qname_setPrefix(&qname, "ws4d");
 * ws4d_qname_setName(&qname, "device");
 *
 * copy = ws4d_qname_dup(qname, &alist);
 *
 * ws4d_qname_done(&qname);
 * ws4d_qname_done(copy);
 *
 * ws4d_free_alist(copy);
 * ws4d_allocator_done(&alist);
 * @endcode
 *
 * @param src
 * @param alist
 * @returns
 */
struct ws4d_qname *ws4d_qname_dup (const struct ws4d_qname *src,
                                   struct ws4d_abs_allocator *alist);


/**
 * Function to parse a string representation of a Qname
 *
 * @code
 * @endcode
 *
 * @param string
 * @param qname
 * @param alist
 */
int
ws4d_qname_parse (const char *string, struct ws4d_qname *qname,
                  struct ws4d_abs_allocator *alist);


/** @} */

#endif /*WS4D_QNAME_H_ */
