/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef _WS4D_MM_H_
#define _WS4D_MM_H_

#ifdef __cplusplus
extern "C"
{
#if 0
}
#endif
#endif

#include "ws4d_misc.h"

/**
 * @addtogroup DpwsMemoryManagement Memory management
 * @ingroup WS4D_UTILS
 *
 * @{
 */


/**
 * Wraps the platform malloc() call
 *
 * @code
 * void *buf = NULL;
 *
 * buf = WS4D_MALLOC(100);
 * @endcode
 *
 * @param size size of buffer
 *
 * @return newly-allocated buffer or NULL if no memory
 */
#define WS4D_MALLOC(size) malloc(size)


/**
 * Wraps the platform free() call
 *
 * @code
 * void *buf = NULL;
 *
 * buf = WS4D_MALLOC(100);
 * WS4D_FREE(buf);
 * @endcode
 *
 * @param ptr pointer to buffer to free
 */
#define WS4D_FREE(ptr) free(ptr)

/**
 * Function allocates and initializes memory with NULL.
 *
 * @code
 * void *buf = NULL;
 *
 * buf = ws4d_malloc(100);
 * @endcode
 *
 * @param length length of the buffer
 *
 * @return pointer to newly-allocated buffer or NULL if no memory
 */
void *ws4d_malloc (size_t length);

/**
 * Function frees memory allocated with ws4d_malloc.
 *
 * @code
 * void *buf = NULL;
 *
 * buf = ws4d_malloc(100);
 * ws4d_free(buf);
 * @endcode
 *
 * @param p pointer to buffer
 */
void ws4d_free (void *p);

/** @} */

struct ws4d_abs_allocator;

struct ws4d_abs_allocator_cbs
{
  void (*fdone) (struct ws4d_abs_allocator * allocator);

  int (*ffreeall) (struct ws4d_abs_allocator * allocator);

  void *(*falloc) (struct ws4d_abs_allocator * allocator, size_t size);

  int (*ffree) (struct ws4d_abs_allocator * allocator, void *buf);

  void *(*fmemdup) (struct ws4d_abs_allocator * allocator, const void *buf,
                    size_t size);

  void *(*fmemref) (struct ws4d_abs_allocator * allocator, const void *buf,
                    size_t size);

  int (*funref) (struct ws4d_abs_allocator * allocator, void *buf);
};

struct ws4d_allocatorcopy_s;

struct ws4d_abs_allocator
{
  const char *id;
  void *data;
  struct ws4d_allocatorcopy_s *copy;
  struct ws4d_abs_allocator_cbs *callbacks;
};

struct ws4d_allocatorcopy_s
{
  struct ws4d_abs_allocator *copyof;
};

#ifndef ABSTRACT_ALLOCATOR_IMPLEMENT

/**
 * Abstract Allocator
 *
 * @addtogroup APIAbsAllocator Abstract Allocator
 * @ingroup DpwsMemoryManagement
 *
 * @{
 */

/**
 * Function to initialize an abstract allocator
 *
 * @code
 * ws4d_abs_allocator alloc;
 *
 * ws4d_allocator_init(&alloc, ws4d_alist_init, NULL);
 * @endcode
 *
 * @param allocator allocator
 * @param finit function to create allocator
 * @param arg argumets to finit function
 *
 * @return WS4D_OK on success, error code otherwise
 */
int
ws4d_allocator_init (struct ws4d_abs_allocator *allocator,
                     int (*finit) (struct ws4d_abs_allocator *
                                   allocator, void *arg), void *arg);


/*
 * Function to duplicate an allocator
 *
 * @code
 * ws4d_abs_allocator alloc, copy;
 * void *buf;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * ws4d_allocator_copy(&alloc, copy);
 *
 * buf = ws4d_allocator_alloc(&copy, 20);
 * ws4d_allocator_free(&copy, buf);
 *
 * ws4d_allocator_done(&copy);
 * ws4d_allocator_done(&alloc);
 * @endcode
 */
int
ws4d_allocator_copy (struct ws4d_abs_allocator *src,
                     struct ws4d_abs_allocator *dst);


/*
 * Function to check if an allocator is a copy
 *
 * @code
 * ws4d_abs_allocator alloc, copy;
 * void *buf;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * ws4d_allocator_copy(&alloc, copy);
 *
 * if (ws4d_allocator_iscopy(&copy)) {
 *  // copy is an allocator copy
 * }
 *
 * ws4d_allocator_done(&copy);
 * ws4d_allocator_done(&alloc);
 * @endcode
 */
int ws4d_allocator_iscopy (struct ws4d_abs_allocator *allocator);


/**
 * Function to destroy allocator and free allocated memory
 *
 * @code
 * ws4d_abs_allocator alloc;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * ws4d_allocator_done(&alloc);
 * @endcode
 *
 * @param allocator allocator
 */
void ws4d_allocator_done (struct ws4d_abs_allocator *allocator);

/**
 * function to free all memory of an allocator
 *
 * @code
 * ws4d_abs_allocator alloc;
 * void *buf;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * ws4d_allocator_freeall(&alloc);
 *
 * ws4d_allocator_done(&alloc);
 * @endcode
 *
 * @param allocator allocator
 *
 * @return WS4D_OK on success or error code otherwise
 */
int ws4d_allocator_freeall (struct ws4d_abs_allocator *allocator);

/**
 * function to allocate a memory with an allocator
 *
 * @code
 * ws4d_abs_allocator alloc;
 * void *buf;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * buf = ws4d_allocator_alloc(&alloc, 20);
 *
 * ws4d_allocator_done(&alloc);
 * @endcode
 *
 * @param allocator allocator
 * @param length length of buffer to allocate
 *
 * @return pointer to allocated memory or NULL on failure
 */
void *ws4d_allocator_alloc (struct ws4d_abs_allocator *allocator,
                            size_t length);

/**
 * function to free memory of an allocator
 *
 * @code
 * ws4d_abs_allocator alloc;
 * void *buf;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * buf = ws4d_allocator_alloc(&alloc, 20);
 * ws4d_allocator_free(&alloc, buf);
 *
 * ws4d_allocator_done(&alloc);
 * @endcode
 *
 * @param allocator allocator
 * @param buf memory to free
 *
 * @return WS4D_OK on success or error code otherwise
 */
int ws4d_allocator_free (struct ws4d_abs_allocator *allocator, void *buf);

/**
 * function to duplicate memory with an allocator
 *
 * @code
 * ws4d_abs_allocator alloc;
 * void *buf, *buf2;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * buf = ws4d_allocator_alloc(&alloc, 20);
 *
 * buf2 = ws4d_allocator_dup(&alloc, buf, 20);
 *
 * ws4d_allocator_done(&alloc);
 * @endcode
 *
 * @param allocator allocator
 * @param src pointer to memory to duplicate
 * @param length lenght of memory buffer to duplicate
 *
 * @return pointer to allocated memory or NULL on failure
 */
void *ws4d_allocator_memdup (struct ws4d_abs_allocator *allocator,
                             const void *src, size_t length);

/**
 * function to reference memory allocated with an allocator
 *
 * @code
 * ws4d_abs_allocator alloc;
 * void *buf, *buf2;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * buf = ws4d_allocator_alloc(&alloc, 20);
 *
 * buf2 = ws4d_allocator_memref(&alloc, buf);
 *
 * ws4d_allocator_done(&alloc);
 * @endcode
 *
 * @param allocator allocator
 * @param src pointer to memory to reference
 * @param length length of memory
 * @return WS4D_OK on success WS4D_ERR otherwise
 */
void *ws4d_allocator_memref (struct ws4d_abs_allocator *allocator,
                             const void *src, size_t length);

/**
 * function to free a reference allocated with an allocator
 *
 * @code
 * ws4d_abs_allocator alloc;
 * void *buf, *buf2;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * buf = ws4d_allocator_alloc(&alloc, 20);
 *
 * buf2 = ws4d_allocator_memref(&alloc, buf);
 * ws4d_allocator_unref(&alloc, buf);
 *
 * ws4d_allocator_done(&alloc);
 * @endcode
 *
 * @param allocator allocator
 * @param src pointer to memory to reference
 * @return WS4D_OK on success WS4D_ERR otherwise
 */
int ws4d_allocator_unref (struct ws4d_abs_allocator *allocator, void *src);

/**
 * Function duplicates string and registers allocation in allocation list.
 *
 * @code
 * ws4d_alloc_list alloc;
 * char *string1 = "Hello", *string2 = NULL;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * string2 = ws4d_strdup(string1, &alloc);
 * @endcode
 *
 * @param src pointer to string to duplicate
 * @param alist allocation list to insert allocation
 *
 * @return pointer to newly-allocated string or NULL if no memory
 */
char *ws4d_strdup (const char *src, struct ws4d_abs_allocator *alist);

/**
 * Function duplicates string and registers allocation in allocation list.
 *
 * @code
 * ws4d_alloc_list alloc;
 * char *string1 = "Hello", *string2 = NULL;
 *
 * WS4D_ALLOCLIST_INIT(&alloc);
 *
 * string2 = ws4d_strndup(string1, &alloc);
 * @endcode
 *
 * @param src pointer to string to duplicate
 * @param size size of string
 * @param alist allocation list to insert allocation
 *
 * @return pointer to newly-allocated string or NULL if no memory
 */
char *ws4d_strndup (const char *src, size_t size,
                    struct ws4d_abs_allocator *alist);

/** @} */

/**
 * Allocator Implementations
 *
 * @addtogroup APIAbsAllocatorImplementations Implementations
 * @ingroup APIAbsAllocator
 *
 * @{
 */

/** @} */

/** @} */

#endif /*ABSTRACT_ALLOCATOR_IMPLEMENT */

#ifdef __cplusplus
#if 0
{
#endif
}
#endif

#endif /*_ABSTRACT_ALLOCATOR_H_*/
