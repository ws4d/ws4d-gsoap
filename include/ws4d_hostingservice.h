/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef WS4D_HOSTINGSERVICE_H_
#define WS4D_HOSTINGSERVICE_H_

/**
 * Service Hosting
 *
 * @addtogroup APIHosting Service Hosting
 * @ingroup APIServiceEP
 *
 * @{
 */

/** @} */

#include "ws4d_epr.h"
#include "ws4d_abstract_eprlist.h"
#include "ws4d_device_description.h"
#include "ws4d_target.h"

/**
 * Hosted Service RelationShip meta data
 *
 * @addtogroup APIHostedRelApi Hosted Service RelationShip meta data
 * @ingroup APIHosting
 *
 * @{
 */

struct ws4d_host
{
  char *Addr;
  char *ServiceId;
  struct ws4d_qnamelist types;
  char *__any;
};

struct ws4d_relationship
{
  struct ws4d_host host;
  struct ws4d_host *hosted;
  int hosted_count;
};

/** @} */

/**
 * Hosting Service
 *
 * @addtogroup APIHostingServiceMod Hosting Service
 * @ingroup APIHosting
 *
 * @{
 */

#define WS4D_HS_UNCHANGED       0x00000000
#define WS4D_HS_RELCHANGED      0x00000001      /* Relationship changed */
#define WS4D_HS_DEVCHANGED      0x00000002      /* ThisDevice changed   */
#define WS4D_HS_MODCHANGED      0x00000004      /* ThisModel changed    */

/**
 * Initialize hosting service structure
 *
 * @param hs hosting service structure to initialize
 * @param interf interface the hosting service is related to
 *
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_hosting_init (struct ws4d_epr *epr);

#ifdef WITH_MUTEXES
#define ws4d_hosting_lock(__epr) ws4d_epr_lock(__epr)
#define ws4d_hosting_unlock(__epr) ws4d_epr_unlock(__epr)
#else

/**
 * Lock the hosting service structure
 */
#define ws4d_hosting_lock(__epr)

/**
 * Unlock the hosting service structure
 */
#define ws4d_hosting_unlock(__epr)
#endif

/**
 * Change interface a hosting service is related to
 *
 * @param hs hosting service structure
 * @param interf new interface the hosting service is related to
 *
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_hosting_change_interf (struct ws4d_epr *epr,
                                const char *interf);

/**
 * Add a new service to the hosting service
 *
 * @param hs hosting service structure
 * @param serviceid locally unique id for the service
 *
 * @return WS4D_OK on success, an error code otherwise
 */
struct ws4d_epr *ws4d_hosting_add_service (struct ws4d_epr *epr,
                                           const char *serviceid);

/**
 * Bind a hosted service to an uri template
 *
 * This function binds the a service to an uri template. Uri templates are
 * uris with variables that are replaced by ws4d_hosting_bind_service() in this
 * case. Uri templates like http://host:0/ are replaced with uris that contain
 * interface related to the hs like this: http://192.168.1.1:0/. The content
 * of the uri template buffer is replaced wiht the resulting uri.
 *
 * @param hs hosting service structure
 * @param service service to bind
 * @param uri_template uri_template that is used to bind the service and replaced with the resulting uri
 * @param size size of the uri template buffer
 *
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_hosting_bind_service (struct ws4d_epr *epr, struct ws4d_epr *service,
                               char *uri_template, size_t size);

/**
 * Activates a hosted service so that it is part of the relationship metadata
 *
 * @param hs hosting service structure
 * @param service service to activate
 * @param uri uri of the service
 * @param size size of the uri buffer
 *
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_hosting_activate_service (struct ws4d_epr *epr,
                                   struct ws4d_epr *service, char *uri,
                                   size_t size);

/**
 * Deactivates a hosted service
 *
 * @param hs hosting service structure
 * @param service service to activate
 *
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_hosting_deactivate_service (struct ws4d_epr *epr,
                                     struct ws4d_epr *service);

/**
 * Removes a hosted service from the hosting service
 *
 * @param hs hosting servicce structure
 * @param service service to deactivate
 *
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_hosting_remove_service (struct ws4d_epr *epr,
                                 struct ws4d_epr *service);


/**
 * TODO: add docu
 */
struct ws4d_abs_eprlist *ws4d_hosting_get_services (struct ws4d_epr *epr);


/**
 * Searches for a hosted service by service id
 *
 * @param hs hosting service structure
 * @param serviceid id of the service to look for
 *
 * @return pointer to service epr if service was found, NULL otherwise
 */
#define ws4d_hosting_get_service(__epr, __id) \
    ws4d_eprlist_getByServiceId(ws4d_hosting_get_services(__epr), __id)


/**
 * Copy all active services to an epr list
 *
 * @param hs hosting service structure
 * @param list list to copy eprs to
 *
 * @return WS4D_OK on success, an error code otherwise
 */
int ws4d_hosting_get_activeservices (struct ws4d_epr *epr, struct ws4d_abs_eprlist *list);

/**
 * Get number of active services of a hosting service
 *
 * @param hs hosting service structure
 *
 * @return number of active services
 */
int ws4d_hosting_get_active_services_count (struct ws4d_epr *epr);

/**
 * Get the relationship metadata of a hosting service by copy
 *
 * @param hs hosting service structure
 * @param alist allocation list to copy data
 *
 * @return Copy of relationship metadata on success or NULL otherwise
 *
 * TODO: check if we need this function
 */
struct ws4d_relationship
  *ws4d_hosting_get_relationship (struct ws4d_epr *epr,
                                  struct ws4d_abs_allocator *alist);

/**
 * Free relationship metadata returned by ws4d_hosting_get_relationship
 *
 * @param hs hosting service structure
 * @param relationship data to free
 *
 * TODO: check if we need this function
 */
void ws4d_hosting_free_relationship (struct ws4d_epr *epr,
                                     struct ws4d_relationship *relationship);

/**
 * Get ThisDevice meta data of an hosting service
 *
 * @param hs hosting service structure
 *
 * @return pointer to ThisDevice meta data on success, or NULL otherwise
 */
struct ws4d_thisDevice
  *ws4d_hosting_get_thisdevice (struct ws4d_epr *epr);

/**
 * Get ThisDevice meta data of an hosting service for changeing
 *
 * @param hs hosting service structure
 *
 * @return pointer to ThisDevice meta data on success, or NULL otherwise
 */ struct ws4d_thisDevice
 *ws4d_hosting_change_thisdevice (struct ws4d_epr *epr);

 /**
  * Get ThisModel meta data of an hosting service
  *
  * @param hs hosting service structure
  *
  * @return pointer to ThisModel meta data on success, or NULL otherwise
  */
struct ws4d_thisModel
  *ws4d_hosting_get_thismodel (struct ws4d_epr *epr);

/**
 * Get ThisModel meta data of an hosting service for changing
 *
 * @param hs hosting service structure
 *
 * @return pointer to ThisModel meta data on success, or NULL otherwise
 */

struct ws4d_thisModel
  *ws4d_hosting_change_thismodel (struct ws4d_epr *epr);

/** @} */

#endif /*WS4D_HOSTINGSERVICE_H_ */
