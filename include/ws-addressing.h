/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef _WSADDRESSING_H_
#define _WSADDRESSING_H_

#ifdef __cplusplus
extern "C"
{
#if 0
}
#endif
#endif

/**
 * gSoap WS-Addressing Plugin API
 *
 * @addtogroup ADDRESSING_API Addressing-API
 * @ingroup DPWS_WS_MODULES
 *
 * @{
 */

#include "soap_misc.h"

#include "ws4d_epr.h"

/**
 * @addtogroup APIMIHHandling Message information header handling functions
 * @ingroup ADDRESSING_API
 *
 * @{
 */


extern const char *wsa_faultAction;
extern const char *wsa_anonymousURI;

int wsa_register_handle (struct soap *soap);

struct message_id_s
{
  struct ws4d_list_node list;
  int valid;
  char *msg_id;
};

int wsa_message_isdup (struct soap *soap);


/**
 * Set the wsa:MessageId field in the WS-Addressing header
 *
 * @param soap handle to set header field
 * @param Id value the field is set to
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_set_MessageId (struct soap *soap, const char *Id);


/**
 * Get the wsa:MessageId field in the WS-Addressing header
 *
 * @param soap handle to set header field
 *
 * @return value of field on success, NULL on failure
 */
const char *wsa_header_get_MessageId (struct soap *soap);


/**
 * Check if message wsa:MessageId is set in the WS-Addressing
 * header.
 *
 * @param soap handle to check header field
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_check_MessageId (struct soap *soap);


/**
 * Set the wsa:To field in the WS-Addressing header
 *
 * @param soap handle to set header field
 * @param To value the field is set to
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_set_To (struct soap *soap, const char *To);


/**
 * Get the wsa:To field in the WS-Addressing header
 *
 * @param soap handle to set header field
 *
 * @return value of field on success, NULL on failure
 */
const char *wsa_header_get_To (struct soap *soap);


/**
 * Set the wsa:From field in the WS-Addressing header
 *
 * @param soap handle to set header field
 * @param Address value the field is set to
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_set_From (struct soap *soap, const char *Address);


/**
 * Get the wsa:From field in the WS-Addressing header
 *
 * @param soap handle to set header field
 *
 * @return value of field on success, NULL on failure
 */
const char *wsa_header_get_From (struct soap *soap);


/**
 * Set the wsa:Action field in the WS-Addressing header
 *
 * @param soap handle to set header field
 * @param Action value the field is set to
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_set_Action (struct soap *soap, const char *Action);


/**
 * Get the wsa:Action field in the WS-Addressing header
 *
 * @param soap handle to set header field
 *
 * @return value of field on success, NULL on failure
 */
const char *wsa_header_get_Action (struct soap *soap);


/**
 * Set the wsa:RelatesTo field in the WS-Addressing header
 *
 * @param soap handle to set header field
 * @param MessageId value the field is set to
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_set_RelatesTo (struct soap *soap, const char *MessageId);


/**
 * Get the wsa:RelatesTo field in the WS-Addressing header
 *
 * @param soap handle to set header field
 *
 * @return value of field on success, NULL on failure
 */
const char *wsa_header_get_RelatesTo (struct soap *soap);


/**
 * Sets the wsa:ReplyTo field in the WS-Addressing header
 *
 * @param soap handle to set header field
 * @param Address address the field is set to
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_set_ReplyTo (struct soap *soap, const char *Address);


/**
 * Get the wsa:ReplyTo field in the WS-Addressing header
 *
 * @param soap handle to set header field
 *
 * @return value of field on success, NULL on failure
 */
const char *wsa_header_get_ReplyTo (struct soap *soap);


/**
 * Sets the wsa:FaultTo field in the WS-Addressing header
 *
 * @param soap handle to set header field
 * @param Address address the field is set to
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_set_FaultTo (struct soap *soap, const char *Address);


/**
 * Get the wsa:FaultTo field in the WS-Addressing header
 *
 * @param soap handle to set header field
 *
 * @return value of field on success, NULL on failure
 */
const char *wsa_header_get_FaultTo (struct soap *soap);


/**
 * Generates a WS-Addressing header for a one way message
 *
 * @param soap handle to generate header
 * @param MessageId wsa:MessageId field
 * @param To wsa:To field
 * @param Action wsa:Action field
 * @param FaultTo wsa:FaultTo field
 * @param size size of SOAP_ENV__Header structure
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_gen_oneway (struct soap *soap, const char *MessageId,
                           const char *To, const char *Action,
                           const char *FaultTo, size_t size);


/**
 * Generates a WS-Addressing header for a request message
 *
 * @param soap handle to generate header
 * @param MessageId wsa:MessageId field
 * @param To wsa:To field
 * @param Action wsa:Action field
 * @param FaultTo wsa:FaultTo field
 * @param ReplyTo
 * @param size size of SOAP_ENV__Header structure
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_gen_request (struct soap *soap, const char *MessageId,
                            const char *To, const char *Action,
                            const char *FaultTo, const char *ReplyTo,
                            size_t size);


/**
 * Generates a WS-Addressing header for a response message
 *
 * @param soap handle to generate header
 * @param MessageId wsa:MessageId field
 * @param To wsa:To field
 * @param Action wsa:Action field
 * @param RelatesTo wsa:RelatesTo field
 * @param size size of SOAP_ENV__Header structure
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_gen_response (struct soap *soap, const char *MessageId,
                             const char *To, const char *Action,
                             const char *RelatesTo, size_t size);


/**
 * Generates a WS-Addressing header for a fault message
 *
 * @param soap handle to generate header
 * @param MessageId wsa:MessageId field
 * @param To wsa:To field
 * @param Action wsa:Action field
 * @param RelatesTo wsa:RelatesTo field
 * @param size size of SOAP_ENV__Header structure
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_gen_fault (struct soap *soap, const char *MessageId,
                          const char *To, const char *Action,
                          const char *RelatesTo, size_t size);


/**
 * Check WS-Addressing header of request message
 *
 * @param soap handle to check header field
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_check_request (struct soap *soap);


/**
 * Check WS-Addressing header of response message
 *
 * @param soap handle to check header field
 *
 * @return SOAP_OK on success, SOAP_ERR on failure
 */
int wsa_header_check_response (struct soap *soap);

/** @} */

/**
 * @addtogroup APIFaultHandling Fault handling functions
 * @ingroup ADDRESSING_API
 *
 * @{
 */
int wsa_sender_fault_subcode (struct soap *soap, const char *faultsubcode,
                              const char *faultstring,
                              const char *faultdetail);

int wsa_receiver_fault_subcode (struct soap *soap, const char *faultsubcode,
                                const char *faultstring,
                                const char *faultdetail);
/** @} */

/** @} */

#ifdef __cplusplus
#if 0
{
#endif
}
#endif

#endif /* _WSADDRESSING_H_ */
