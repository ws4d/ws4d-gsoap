/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * security_handler.h
 *
 *  Created on: 26.07.2010
 *      Author: su009
 */

#ifndef SECURITY_HANDLER_H_
#define SECURITY_HANDLER_H_

#include "security_engine.h"

/* Security Handler PlugIn */
#define SH_PLUGIN_ID "Security_Handler_PlugIn_0.1"

struct sh_plugin_data
{
  /* The sec_engine to be used */
  struct security_engine_s *sec_engine;
  /* disconnect handle is invoked after a complete soap message is received */
  int (*fdisconnect) (struct soap *);
  int insist_on_authentication;
  int insist_on_authorization;
};

int sh_plugin (struct soap *soap, struct soap_plugin *p, void *arg);
/* 1 - yes, 0 - no */
void sh_insist_on_authentication (struct soap *soap, int mode);




#endif /* SECURITY_HANDLER_H_ */
