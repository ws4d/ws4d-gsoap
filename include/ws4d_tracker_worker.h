/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 * ws4d_tracker_worker.h
 *
 *  Created on: 24.03.2010
 *      Author: elmex
 */

#ifndef WS4D_TRACKER_WORKER_H_
#define WS4D_TRACKER_WORKER_H_

#include "dpws_client.h"
#include "ws4d_misc.h"

/**
 * @addtogroup APITracker Device and Service Tracker API
 * @ingroup DPWSAPIClient
 *
 * TODO: write documentation
 * @{
 */

typedef int (*ws4d_dt_newepr_cb) (void *context, struct ws4d_epr * epr);

typedef int (*ws4d_dt_invalidateepr_cb) (void *context,
                                         struct ws4d_epr * epr);

typedef void (*ws4d_dt_dowork_cb) (void *context);

typedef void (*ws4d_dt_delete_cb) (void *context);

struct ws4d_trackerworker_hooks
{
  ws4d_dt_newepr_cb NewDevice;
  ws4d_dt_invalidateepr_cb InvalidateDevice;
  ws4d_dt_dowork_cb DoWork;
  ws4d_dt_delete_cb Delete;
};

struct ws4d_dtracker
{
  struct ws4d_list_node list;
  const char *id;
  void *data;
  int status;
  struct ws4d_trackerworker_hooks *hooks;
  struct ws4d_abs_allocator alist;
};

struct ws4d_stracker
{
  struct ws4d_list_node list;
  const char *id;
  void *data;
  int status;
  struct ws4d_trackerworker_hooks *hooks;
  struct ws4d_abs_allocator alist;
};

typedef int (*ws4d_dt_init_cb) (struct ws4d_dtracker * dtracker);
typedef int (*ws4d_st_init_cb) (struct ws4d_stracker * stracker);

struct ws4d_tc_int *ws4d_tw_create_tc (void *arg);

int ws4d_tw_init (struct dpws_s *peer);

int ws4d_tw_done (void);

int ws4d_tw_doprobe (ws4d_qnamelist * Types, const char *Scopes,
                     ws4d_time timeout, int backlog);

int ws4d_tw_dodprobe (struct ws4d_epr *device, ws4d_qnamelist * Types,
                      const char *Scopes, ws4d_time timeout);

int ws4d_tw_dofindservices (struct ws4d_epr *device, ws4d_qnamelist * Types,
                            ws4d_time timeout,
                            struct ws4d_abs_eprlist *result);

int ws4d_tw_dowork (void);

int ws4d_tw_add_dtracker (struct ws4d_dtracker *dtracker,
                          ws4d_dt_init_cb init_cb);

int ws4d_tw_del_dtracker (struct ws4d_dtracker *dtracker);

int ws4d_tw_add_stracker (struct ws4d_stracker *stracker,
                          ws4d_st_init_cb init_cb);

int ws4d_tw_del_stracker (struct ws4d_stracker *stracker);

int ws4d_tw_addtotc (struct ws4d_epr *device);

int ws4d_tw_getfromtc (const char *addr, struct ws4d_epr *result);

#define ws4d_tw_set_mode(tracker, n) ((tracker)->status |= (n))
#define ws4d_tw_clr_mode(tracker, n) ((tracker)->status &= ~(n))
#define ws4d_tw_check_mode(tracker, n) ((tracker)->status & (n))

#define WS4D_TRACKER_DOWORK      0x00000001

/** @} */

#endif /* WS4D_TRACKER_WORKER_H_ */
