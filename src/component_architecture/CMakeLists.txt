# Copyright (C) 2007  University of Rostock
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.


# directory to put all generated files in
SET(gen_DIR ${CMAKE_CURRENT_BINARY_DIR}/gen)

# If the directory for the generated sources does not
# already exists, create it.
IF(NOT EXISTS ${gen_DIR})
	FILE(MAKE_DIRECTORY ${gen_DIR})
ENDIF(NOT EXISTS ${gen_DIR})

# set include paths
INCLUDE_DIRECTORIES(${GSOAP_INCLUDE_DIR} ${DPWS_INCLUDES} ${gen_DIR} ${CMAKE_CURRENT_SOURCE_DIR})

###############################################################################
# Code generation - for custom projects this must be modified
###############################################################################

# generate code and C data binding operations of service
GSOAP_GENERATE(usm.gsoap usm ${gen_DIR})

# embed wsdl files into c code
SET(USMSERVICE_WSDL ${CMAKE_CURRENT_SOURCE_DIR}/UtilizableServicesManager.wsdl)
DPWS_EMBEDD_WSDL(${gen_DIR} usm ${USMSERVICE_WSDL})

################################################################################
# create config.h
################################################################################

CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/config.h.in
               ${gen_DIR}/config.h
               @ONLY)


################################################################################
# build USM
################################################################################

IF(Threads_FOUND)
  SET(USM_SRCS
      usm_service.c
      ${gen_DIR}/usmServerLib.c
      ${gen_DIR}/usm_wsdl.c)

  ADD_LIBRARY(ws4d_usmsPmt STATIC ${USM_SRCS})

  EXTEND_TGT_COMPILE_FLAGS(ws4d_usmsPmt
    FLAGS "-DWITH_MUTEXES -DDPWS_DEVICE")

  INSTALL(TARGETS ws4d_usmsPmt ARCHIVE DESTINATION lib/ws4d-gSOAP)
ENDIF(Threads_FOUND)


