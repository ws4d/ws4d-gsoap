/* utilizable services manager service - managing service references on devices
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * author: Elmar Zeeb
 */

#include "usm.nsmap"
#include "dpws_device.h"

int
setup_USMService (struct dpws_s *device, struct soap *handle,
                  const char *wsdl, int backlog);

int
setup_USMService (struct dpws_s *device, struct soap *handle,
                  const char *wsdl, int backlog)
{
  char uri[DPWS_URI_MAX_LEN] = "http://host:0/UtilizableServicesManager";
  struct ws4d_qname service_type;
  struct ws4d_epr *service = NULL;

  soap_set_namespaces (handle, usm_namespaces);

  service =
    dpws_service_init (device,
                       "http://www.ws4d.org/services/UtilizableServicesManager");

  if (dpws_service_bind
      (device, service, handle, uri, DPWS_URI_MAX_LEN, backlog))
    {
      return WS4D_ERR;
    }

  ws4d_qname_init(&service_type);
  ws4d_qname_setNS(&service_type, "http://www.ws4d.org/services/UtilizableServicesManager");
  ws4d_qname_setName(&service_type, "usm");
  if (dpws_service_add_type (service, &service_type))
    {
      return WS4D_ERR;
    }


  if (wsdl && *wsdl)
    {
      if (dpws_service_set_wsdl (service, wsdl))
        {
          return WS4D_ERR;
        }
    }

  return dpws_add_hosted_service (device, service, uri, DPWS_URI_MAX_LEN);
}

int
__usm1__GetServiceReferences (struct soap *soap,
                              struct
                              usm1__UtilizableServicesManagerMessageType
                              *usm1__UtilizableServicesManagerMessage,
                              struct __usm1__GetServiceReferencesResponse
                              *_param_1)
{
  return dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                   "http://www.ws4d.org/services/UtilizableServicesManager/GetServiceReferencesResponse",
                                   wsa_header_get_MessageId (soap),
                                   sizeof (struct SOAP_ENV__Header));
}

int
__usm1__SetServiceReferences (struct soap *soap,
                              struct
                              usm1__UtilizableServicesManagerMessageType
                              *usm1__UtilizableServicesManagerMessage,
                              struct __usm1__SetServiceReferencesResponse
                              *_param_2)
{
  return dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                   "http://www.ws4d.org/services/UtilizableServicesManager/SetServiceReferencesResponse",
                                   wsa_header_get_MessageId (soap),
                                   sizeof (struct SOAP_ENV__Header));
}
