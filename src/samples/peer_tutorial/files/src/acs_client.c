/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "acs1.nsmap"
#include "dpws_peer.h"
#include "acs_client.h"

struct soap client;
struct dpws_s *dpws = NULL;

int
acs_client_init (struct dpws_s *peer, struct ws4d_dtracker *dtracker)
{

  if (dpws == NULL)
    {
      dpws = peer;
    }
  else
    {
      return WS4D_ERR;
    }

  /* initialize soap handle */
  soap_init (&client);
#ifdef DEBUG
  soap_set_omode (&client, SOAP_XML_INDENT);
#endif
  soap_set_namespaces (&client, acs1_namespaces);

  printf ("\nAirconditioner: probing for airconditioners for 5 seconds...\n");
  if (ws4d_dtracker_doProbe (dtracker, 5000))
    {
      printf ("\nAirconditioner: Error probing for airconditioners\n");
    }
  printf ("\ndone\n");

  return WS4D_OK;
}

int
acs_client_done ()
{

  if (dpws != NULL)
    {
      dpws = NULL;
    }
  else
    {
      return WS4D_ERR;
    }

  soap_done (&client);

  return WS4D_OK;
}

int
acs_client_getStatus (struct ws4d_epr *service,
                      struct acs_client_StateType *state)
{
  struct acs1__ACStateType ACState;
  int ret = WS4D_ERR;

  ws4d_assert (service && state, WS4D_EPARAM);

  dpws_header_gen_request (&client, NULL, ws4d_epr_get_Addrs (service),
                           "http://www.ws4d.org/axis2/tutorial/AirConditioner/GetStatusIn",
                           NULL, NULL, sizeof (struct SOAP_ENV__Header));

  /* call GetStatus operation */
  if (soap_call___acs1__GetStatus
      (&client, ws4d_epr_get_Addrs (service), NULL, NULL,
       &ACState) == SOAP_OK)
    {
      state->CurrentTemp = ACState.CurrentTemp;
      state->TargetTemp = ACState.TargetTemp;
      ret = WS4D_OK;
    }
  else
    {
      fprintf (stderr, "\nsimple_client: error calling GetStatus on %s\n",
               ws4d_epr_get_Addrs (service));
    }

  /* clean up */
  soap_end (&client);

  return ret;
}

int
acs_client_setTargetTemperature (struct ws4d_epr *service,
                                 int TargetTemperature,
                                 struct acs_client_StateType *state)
{
  struct acs1__ACStateType ACState;
  int ret = WS4D_ERR;

  ws4d_assert (service && state, WS4D_EPARAM);

  dpws_header_gen_request (&client, NULL, ws4d_epr_get_Addrs (service),
                           "http://www.ws4d.org/axis2/tutorial/AirConditioner/SetTargetTemperatureIn",
                           NULL, NULL, sizeof (struct SOAP_ENV__Header));

  /* call GetStatus operation */
  if (soap_call___acs1__SetTargetTemperature
      (&client, ws4d_epr_get_Addrs (service), NULL, TargetTemperature,
       &ACState) == SOAP_OK)
    {
      state->CurrentTemp = ACState.CurrentTemp;
      state->TargetTemp = ACState.TargetTemp;
      ret = WS4D_OK;
    }
  else
    {
      fprintf (stderr,
               "\nsimple_client: error calling set TargetTemperature on %s\n",
               ws4d_epr_get_Addrs (service));
    }

  /* clean up */
  soap_end (&client);

  return ret;
}
