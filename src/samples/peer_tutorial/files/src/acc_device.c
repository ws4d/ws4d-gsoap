/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "acc1.nsmap"
#include "dpws_peer.h"
#include "acc_metadata.h"
#include "acc_wsdl.h"
#include "config.h"
#include "ws4d_tracker_worker.h"
#include "ws4d_device_tracker.h"
#include "ws4d_service_tracker.h"
#include <signal.h>
#include "acs_client.h"

struct soap service, discovery;
struct dpws_s device;
struct ws4d_dtracker dtracker;
struct ws4d_stracker stracker;
ws4d_qnamelist acdevice_type_list, acservice_type_list;
struct ws4d_abs_allocator alist;

void
service_exit ()
{
  printf ("\nAirconditioner: shutting down...\n");

  ws4d_stracker_done (&stracker);
  ws4d_dtracker_done (&dtracker);
  ws4d_tw_done ();
  ws4d_qnamelist_done (&acdevice_type_list);
  dpws_deactivate_hosting_service (&device);
  soap_done (&service);
  soap_done (&discovery);
  dpws_done (&device);
  ws4d_alloclist_done (&alist);
  exit (0);
}

int acc1_serve_request (struct soap *soap);

int
main (int argc, char **argv)
{
#ifndef WIN32
  struct sigaction sa;
#endif
  char *host = NULL;
  char *uuid = NULL;

  /* parsing command line options */
  while (argc > 1)
    {
      if (argv[1][0] == '-')
        {
          char *option = &argv[1][1];

          switch (option[0])
            {
            case 'i':
              if (strlen (option) > 2)
                {
                  ++option;
                  host = option;
                }
              else
                {
                  --argc;
                  ++argv;
                  host = argv[1];
                }
              printf ("\nAirconditioner: Set host to \"%s\"\n", host);
              break;
            case 'u':
              if (strlen (option) > 2)
                {
                  ++option;
                  uuid = option;
                }
              else
                {
                  --argc;
                  ++argv;
                  uuid = argv[1];
                }
              printf ("\nAirconditioner: Set uuid to \"%s\"\n", uuid);
              break;
            default:
              fprintf (stderr, "\nAirconditioner: Bad option %s\n", argv[1]);
            }
        }
      --argc;
      ++argv;
    }

  if (host == NULL)
    {
      printf ("\nAirconditioner: No host was specified!\n");
      exit (0);
    }

  WS4D_ALLOCLIST_INIT (&alist);
  /* initialize soap handle */
  soap_init (&service);
  soap_set_omode (&service, SOAP_XML_INDENT);
  soap_set_namespaces (&service, acc1_namespaces);

  soap_init (&discovery);
  soap_set_omode (&discovery, SOAP_XML_INDENT);
  soap_set_namespaces (&service, acc1_namespaces);

  /* initialize device and services */
  if (dpws_init2 (&device, host, ws4d_tw_create_tc (NULL))
      || acc_setup_HostingService (&device, &service, uuid, 100)
      || ws4d_tw_init (&device)
      || acc_setup_AirConditionerControl (&device, &service,
                                          AIRCONDITIONERCONTROL_WSDL, 100))
    {
      printf ("\nAirconditioner: Can't init device and services\n");
      dpws_done (&device);
      exit (0);
    }

  /* Set Metadata */
  acc_set_Metadata (&device);
  acc_set_wsdl (&device);

  /* Update Metadata */
  if (dpws_update_Metadata (&device))
    {
      printf ("\nAirconditioner: Can't init metadata\n");
      dpws_done (&device);
      exit (0);
    }

  ws4d_qnamelist_init (&acdevice_type_list);
  ws4d_qnamelist_addstring
    ("\"http://www.ws4d.org/axis2/tutorial/AirConditioner\":AirConditioner",
     &acdevice_type_list, &alist);
  if (ws4d_dtracker_init (&dtracker, &acdevice_type_list, NULL, NULL, NULL))
    {
      printf ("\nAirconditioner: Can't init device tracker\n");
      dpws_done (&device);
      exit (0);
    }

  ws4d_qnamelist_init (&acservice_type_list);
  ws4d_qnamelist_addstring
    ("\"http://www.ws4d.org/axis2/tutorial/AirConditioner\":ACServiceInterface",
     &acservice_type_list, &alist);
  if (ws4d_stracker_init
      (&stracker, &dtracker, &acservice_type_list, NULL, NULL))
    {
      printf ("\nAirconditioner: Can't init service tracker\n");
      dpws_done (&device);
      exit (0);
    }

  if (acs_client_init (&device, &dtracker))
    {
      printf ("\nAirconditioner: Can't init acs_client\n");
      dpws_done (&device);
      exit (0);
    }

  /* install signal handler for SIGINT or Ctrl-C */
#ifdef WIN32
  signal (SIGINT, service_exit);
#else
  memset (&sa, 0, sizeof (sa));
  sa.sa_handler = service_exit;
  sigaction (SIGINT, &sa, NULL);
#endif

  /* Tell hosting service to start advertising its hosted services */
  if (dpws_activate_hosting_service (&device))
    {
      printf ("\nAirconditioner: Can't activate device\n");
      dpws_done (&device);
      exit (0);
    }

  printf ("\nAirconditioner: ready to serve... (Ctrl-C for shut down)\n");

  for (;;)
    {
      struct soap *handle = NULL, *soap_set[] = SOAP_HANDLE_SET (&service);
      int (*serve_requests[]) (struct soap * soap) =
        SOAP_SERVE_SET (acc1_serve_request);

      printf ("\nAirconditioner: waiting for request\n");

      /* waiting for new messages */
      handle = dpws_maccept (&device, 100000, 1, soap_set);

      if (handle)
        {

          printf ("\nAirconditioner: processing request from %s:%d\n",
                  inet_ntoa (handle->peer.sin_addr),
                  ntohs (handle->peer.sin_port));

          /* dispatch messages */
          if (dpws_mserve (handle, 1, serve_requests))
            {
              soap_print_fault (handle, stderr);
            }

          /* clean up soaps internaly allocated memory */
          soap_end (handle);
        }

      ws4d_tw_dowork ();
    }

  return -1;
}

int
__acc1__GetStatus (struct soap *soap,
                   struct _acc1__EmptyMessage *acs1__GetStatus,
                   struct acc1__ACStateTypes *acc1__ACStates)
{

  struct ws4d_abs_eprlist *acservices = ws4d_stracker_getservices (&stracker);
  register struct ws4d_epr *cur, *next;
  int length = 0, i = 0;

  ws4d_eprlist_foreach (cur, next, acservices)
  {
    if (ws4d_epr_isvalid (cur))
      {
        length++;
      }
  }

  acc1__ACStates->__sizeStates = length;
  acc1__ACStates->States =
    soap_malloc (soap, length * sizeof (struct acc1__ACStateType));
  memset (acc1__ACStates->States, 0,
          length * sizeof (struct acc1__ACStateType));
  ws4d_eprlist_foreach (cur, next, acservices)
  {
    if (ws4d_epr_isvalid (cur))
      {
        struct acs_client_StateType state;
        acc1__ACStates->States[i].ServiceId =
          (char *) ws4d_serviceep_getid (cur);
        acc1__ACStates->States[i].DeviceId =
          (char *) ws4d_epr_get_Addrs (ws4d_serviceep_getdevice (cur));
        if (acs_client_getStatus (cur, &state) == WS4D_OK)
          {
            acc1__ACStates->States[i].CurrentTemp = state.CurrentTemp;
            acc1__ACStates->States[i].TargetTemp = state.TargetTemp;
          }
        i++;
      }
  }

  return dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                   "http://www.ws4d.org/axis2/tutorial/AirConditionerControl/GetStatusOut",
                                   wsa_header_get_MessageId (soap),
                                   sizeof (struct SOAP_ENV__Header));
}

int
__acc1__SetTargetTemperature (struct soap *soap, int acc1__TargetTemperature,
                              struct acc1__ACStateTypes *acc1__ACStates)
{
  struct ws4d_abs_eprlist *acservices = ws4d_stracker_getservices (&stracker);
  register struct ws4d_epr *cur, *next;
  int length = 0, i = 0;
  struct acs_client_StateType setstate, getstate;

  ws4d_eprlist_foreach (cur, next, acservices)
  {
    if (ws4d_epr_isvalid (cur))
      {
        length++;
      }
  }

  acc1__ACStates->__sizeStates = length;
  acc1__ACStates->States =
    soap_malloc (soap, length * sizeof (struct acc1__ACStateType));
  memset (acc1__ACStates->States, 0,
          length * sizeof (struct acc1__ACStateType));
  ws4d_eprlist_foreach (cur, next, acservices)
  {
    if (ws4d_epr_isvalid (cur))
      {
        struct acs_client_StateType state;
        acc1__ACStates->States[i].ServiceId =
          (char *) ws4d_serviceep_getid (cur);
        acc1__ACStates->States[i].DeviceId =
          (char *) ws4d_epr_get_Addrs (ws4d_serviceep_getdevice (cur));
        if (acs_client_setTargetTemperature
            (cur, acc1__TargetTemperature, &state) == WS4D_OK)
          {
            acc1__ACStates->States[i].CurrentTemp = state.CurrentTemp;
            acc1__ACStates->States[i].TargetTemp = state.TargetTemp;
          }
        i++;
      }
  }

  return dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                   "http://www.ws4d.org/axis2/tutorial/AirConditionerControl/SetTargetTemperatureOut",
                                   wsa_header_get_MessageId (soap),
                                   sizeof (struct SOAP_ENV__Header));
}
