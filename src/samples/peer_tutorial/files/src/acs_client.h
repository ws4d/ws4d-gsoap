/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * acs_client.h
 *
 *  Created on: 14.02.2011
 *      Author: elmex
 */

#ifndef ACS_CLIENT_H_
#define ACS_CLIENT_H_

#include "ws4d_tracker_worker.h"
#include "ws4d_device_tracker.h"

struct acs_client_StateType
{
  int CurrentTemp;
  int TargetTemp;
};

int acs_client_init (struct dpws_s *peer, struct ws4d_dtracker *dtracker);
int acs_client_done ();

int acs_client_getStatus (struct ws4d_epr *service,
                          struct acs_client_StateType *state);

int acs_client_setTargetTemperature (struct ws4d_epr *service,
                                     int TargetTemperature,
                                     struct acs_client_StateType *state);

#endif /* ACS_CLIENT_H_ */
