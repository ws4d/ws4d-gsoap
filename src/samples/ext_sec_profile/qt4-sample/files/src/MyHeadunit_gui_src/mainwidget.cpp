/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "mainwidget.h"
#include "ui_mainwidget.h"

#include <QMessageBox>
#include <QInputDialog>
#include <QMutex>
#include <QWaitCondition>

/* globals for connecting Qt-related C++ parts and dpws-related
 * C-parts */
MainWidget::MainWidget * global_widget;
/* global variables and mutex needed to synchronize threads when requesting
 * dynamic authorization */
QMutex mutex;
QWaitCondition globals_written;

struct soap *
  ipc_g_soap;
struct security_engine_s *
  ipc_g_sec_engine;
void *
  ipc_g_ressource;
struct authorization_s *
  ipc_g_auth;
permission_t ipc_g_perm;

/* extern variables for delivering events */
extern double
  curX;
extern double
  curY;
extern double
  curZ;
extern float
  tankFill;
extern float
  curSpeed;
extern float
  curSpeedTorque;

/* wrapper function to set a received character */
void
setReceivedCharacter (char chr)
{
  global_widget->emitNewCharSignal (chr);
}

void
propagatePinHandler (security_engine_s * s, unsigned char *pin)
{
  global_widget->emitNewPinSignal (pin);
}

void
getDynamicAuthorization (struct soap *soap, struct security_engine_s *s,
                         void *ressource, struct authorization_s *a)
{
  ipc_g_soap = soap;
  ipc_g_sec_engine = s;
  ipc_g_ressource = ressource;
  ipc_g_auth = a;
  global_widget->emitRequestAuth ();
  mutex.lock ();
  globals_written.wait (&mutex);
  ipc_g_soap = NULL;
  ipc_g_sec_engine = NULL;
  ipc_g_ressource = NULL;
  ipc_g_auth = NULL;
  mutex.unlock ();
}

MainWidget::MainWidget (QWidget * parent):QWidget (parent),
ui (new Ui::MainWidget)
{
  ui->setupUi (this);

  global_widget = this;

  // Inits
  this->volltanken ();
  this->timer = new QTimer (this);
  this->timer->setInterval (500);
  this->status = stehen;
  this->speed = 0;

  /* initialize dpws-related members */
  this->hosting = NULL;
  this->remoteKeyboardService = NULL;
  this->carDataService = NULL;
  this->authentication_service = NULL;
  this->sec_engine = NULL;
  this->device = NULL;
  this->interface = NULL;
  this->uuid = NULL;

  ipc_g_soap = NULL;
  ipc_g_sec_engine = NULL;
  ipc_g_ressource = NULL;
  ipc_g_auth = NULL;

  /* read environment variables and feed them into LineEdits */
  ui->ipEdit->setText (QString ((const char *) getenv ("theIP")).trimmed ());
  ui->urnEdit->
    setText (QString ((const char *) getenv ("theDEVICE")).trimmed ());

  // connect signals and slots

  connect (this->timer, SIGNAL (timeout ()), this, SLOT (timerTick ()));

  connect (ui->TankButton, SIGNAL (clicked ()), this, SLOT (volltanken ()));
  connect (ui->StartButton, SIGNAL (clicked ()), this, SLOT (losfahren ()));
  connect (ui->StopButton, SIGNAL (clicked ()), this, SLOT (abbremsen ()));

  connect (ui->TankWidget, SIGNAL (TankAlarm (int)), this,
           SLOT (sendTankAlarm (int)));

  connect (ui->QuitButton, SIGNAL (clicked ()), this,
           SLOT (closeApplication ()));
  connect (ui->connectButton, SIGNAL (clicked ()), this,
           SLOT (start_dpws_device ()));
  connect (ui->authCheckBox, SIGNAL (stateChanged (int)), this,
           SLOT (adjustAuthorization (int)));

  connect (this, SIGNAL (newCharacter (char)), this,
           SLOT (setReceivedCharacter (char)));
  connect (this, SIGNAL (displayPin (unsigned char *)), this,
           SLOT (showGeneratedPin (unsigned char *)));
  connect (this, SIGNAL (requestAuth ()), this, SLOT (showReqAuthDialog ()));

  ui->authCheckBox->setTristate (false);
}

MainWidget::~MainWidget ()
{
  delete ui;
}

int
MainWidget::start_dpws_device (void)
{
  int err;

  /* Initialize members */
  this->hosting = (struct soap *) malloc (sizeof (struct soap));
  this->remoteKeyboardService = (struct soap *) malloc (sizeof (struct soap));
  this->carDataService = (struct soap *) malloc (sizeof (struct soap));
  this->authentication_service =
    (struct soap *) malloc (sizeof (struct soap));
  this->sec_engine =
    (struct security_engine_s *) malloc (sizeof (struct security_engine_s));
  this->device = (struct dpws_s *) malloc (sizeof (struct dpws_s));
  this->interface = (char *) malloc (ui->ipEdit->text ().length () + 1);
  this->uuid = (char *) malloc (ui->urnEdit->text ().length () + 1);
  memset ((void *) this->interface, 0, ui->ipEdit->text ().length () + 1);
  memset ((void *) this->uuid, 0, ui->urnEdit->text ().length () + 1);
  strcpy (this->interface, ui->ipEdit->text ().toAscii ().data ());
  strcpy (this->uuid, ui->urnEdit->text ().toAscii ().data ());

  if (err = dpws_start_device (this->hosting, this->remoteKeyboardService,
                               this->carDataService,
                               this->authentication_service, this->sec_engine,
                               this->device, (const char *) this->interface,
                               (const char *) this->uuid, propagatePinHandler,
                               getDynamicAuthorization))
    {

      free (this->hosting);
      free (this->remoteKeyboardService);
      free (this->carDataService);
      free (this->authentication_service);
      free (this->sec_engine);
      free (this->device);
      free (this->interface);
      free (this->uuid);
      this->hosting = NULL;
      this->remoteKeyboardService = NULL;
      this->carDataService = NULL;
      this->authentication_service = NULL;
      this->sec_engine = NULL;
      this->device = NULL;
      this->interface = NULL;
      this->uuid = NULL;

      return err;
    }
  this->adjustAuthorization (ui->authCheckBox->checkState ());
  ui->okEdit->setText (QString ("OK"));
  ui->StartButton->setEnabled (true);
  ui->StopButton->setEnabled (true);
  ui->TankButton->setEnabled (true);
}

void
MainWidget::adjustAuthorization (int state)
{
  switch (state)
    {
    case Qt::Unchecked:
      if (this->remoteKeyboardService)
        this->sec_engine->
          authe_deactivate_authorization (this->remoteKeyboardService);
      if (this->carDataService)
        this->sec_engine->
          authe_deactivate_authorization (this->carDataService);
      break;
    case Qt::Checked:
      if (this->remoteKeyboardService)
        this->sec_engine->
          authe_activate_authorization (this->remoteKeyboardService);
      if (this->carDataService)
        this->sec_engine->authe_activate_authorization (this->carDataService);
      break;
    }
}

void
MainWidget::emitNewCharSignal (char chr)
{
  emit newCharacter (chr);
}

void
MainWidget::emitNewPinSignal (unsigned char *pin)
{
  emit displayPin (pin);
}

void
MainWidget::emitRequestAuth ()
{
  emit requestAuth ();
}

void
MainWidget::setReceivedCharacter (char chr)
{
  if (chr != '\b')
    ui->textEdit->insertPlainText (QString (QChar (chr)));
  else
    {                           /* catch backspace */
      QString tmptext;
      tmptext = ui->textEdit->toPlainText ();
      tmptext.chop (1);
      ui->textEdit->clear ();
      ui->textEdit->insertPlainText (tmptext);
    }
}

void
MainWidget::showGeneratedPin (unsigned char *pin)
{
  QMessageBox::information (this, "Generated PIN",
                            QString ("Generated Pin: \"")
                            +=
                            QString::fromAscii ((const char *) pin,
                                                PIN_SIZE) += QString ("\""));
}

void
MainWidget::showReqAuthDialog ()
{

  ipc_g_auth->credential =
    ipc_g_sec_engine->authe_get_credential (ipc_g_soap, CredX509CertRef);
  if (ipc_g_auth->credential)
    {
      ipc_g_auth->credential_type = CredX509CertRef;
    }
  else
    {                           /* nope - let's check for a certificate */
      ipc_g_auth->credential =
        ipc_g_sec_engine->authe_get_credential (ipc_g_soap, CredX509Cert);
      if (ipc_g_auth->credential)
        {
          ipc_g_auth->credential_type = CredX509Cert;
        }
      else
        {
          fprintf (stderr, "Could not find any suitable credential!\n"
                   "Right now, only certificates and certificate references "
                   "are supported\n");
          ipc_g_perm = DENIED;
          return;
        }
    }

  char *identifier = (char *) ipc_g_sec_engine->cdb_get_credential_identifier
    (ipc_g_sec_engine, ipc_g_auth->credential, ipc_g_auth->credential_type);


  int duration;

  GetAuthWidget::GetAuthWidget get_auth (ipc_g_auth, identifier,
                                         (char *) ipc_g_ressource, &duration,
                                         this);

  int result = get_auth.exec ();

  ipc_g_auth->permission = (permission_t) result;
  if (ipc_g_auth->permission > 59)
    {                           /* see GetAuthWidget::madeDecision() for */
      time_t now;               /* my poor excuse... */
      time (&now);
      ipc_g_auth->validity = now + result;
      ipc_g_auth->permission = PERMITTED_TEMPORARY;
    }

  globals_written.wakeAll ();   /* wake up DPWS parts */
}

int
MainWidget::shutdown_dpws_device (void)
{
  int err;
  err = dpws_shutdown_device (this->device, this->authentication_service,
                              this->remoteKeyboardService,
                              this->carDataService, this->hosting);
  if (this->hosting)
    free (this->hosting);
  if (this->remoteKeyboardService)
    free (this->remoteKeyboardService);
  if (this->carDataService)
    free (this->carDataService);
  if (this->authentication_service)
    free (this->authentication_service);
  if (this->sec_engine)
    free (this->sec_engine);
  if (this->device)
    free (this->device);
  if (this->interface)
    free (this->interface);
  if (this->uuid)
    free (this->uuid);
  this->hosting = NULL;
  this->remoteKeyboardService = NULL;
  this->carDataService = NULL;
  this->authentication_service = NULL;
  this->sec_engine = NULL;
  this->device = NULL;
  this->interface = NULL;
  this->uuid = NULL;
  return err;
}

void
MainWidget::closeApplication ()
{
  this->shutdown_dpws_device ();
  qApp->quit ();
}

void
MainWidget::setSpeed (int speed)
{

  ui->TachoWidget->setSpeed ((speed < 181) ? speed : 180);
  curSpeed = (float) this->speed;

  int drehzahl;
  drehzahl = (speed % 20) + (int) (speed / 10 - 3) + 5;
  ui->DrehzahlWidget->setValue (drehzahl);
  curSpeedTorque = (float) drehzahl;
}

void
MainWidget::timerTick (void)
{
  static int count = 0;
  this->adjustSpeed ();
  this->adjustTank ();
  count++;
  if (count == 2)
    {
      deliver_SpeedEvent (curSpeed);
      deliver_SpeedTorqueEvent (curSpeedTorque);
      count = 0;
    }
}

void
MainWidget::sendTankAlarm (int level)
{
  deliver_TankWarnEvent ((float) level);
}

void
MainWidget::losfahren (void)
{
  this->status = anfahren;
  this->timer->start ();
}

void
MainWidget::abbremsen (void)
{
  this->status = bremsen;
}

void
MainWidget::volltanken (void)
{
  this->ui->TankWidget->setValue (this->tank = 1000);
}

void
MainWidget::adjustSpeed (void)
{
  switch (this->status)
    {
    case anfahren:
      this->speed += 3 + rand () % 8;
      this->setSpeed (this->speed);
      if (this->speed >= 170)
        this->status = fahren;
      break;
    case fahren:
      this->speed += rand () % 10 - 5;
      this->setSpeed (this->speed);
      break;
    case bremsen:
      this->speed -= rand () % 25;
      if (this->speed < 0)
        {
          this->speed = 0;
          this->status = stehen;
        }
      this->setSpeed (this->speed);
      break;
    case stehen:
      this->timer->stop ();
      this->speed = 0;
      this->setSpeed (this->speed);
      break;
    }
}

void
MainWidget::adjustTank (void)
{
  double sub = 2 * (this->speed / 36) + (this->speed ? 2 : 0);
  this->tank -= (int) sub;
  ui->TankWidget->setValue (this->tank);
  if (this->tank <= 0)
    this->status = bremsen;
  tankFill = (float) this->tank;
}
