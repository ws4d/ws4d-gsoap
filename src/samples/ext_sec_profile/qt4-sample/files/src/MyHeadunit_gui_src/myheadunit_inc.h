/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * myheadunit_inc.h
 *
 *  Created on: 11.11.2009
 *      Author: cht
 */

#ifndef MYHEADUNIT_INC_H_
#define MYHEADUNIT_INC_H_

struct soap service;
struct dpws_s device;

#define USAGE	printf("My Headunit Device\nusage:\n\t%s ", argv[0]); \
				printf("-i [interface] -u [uuid]\n"); \
				printf("\t interface : required element, IP of interface\n"); \
				printf("\t uuid      : optional element, uuid of device\n"); \
				printf("NOTE: uuid should be of form urn:uuid:[uuid]");

void service_exit ();

enum xsd__boolean receptionWorker (struct mhus1__MHUKeyType *mhus1__sendKey);

#endif /* MYHEADUNIT_INC_H_ */
