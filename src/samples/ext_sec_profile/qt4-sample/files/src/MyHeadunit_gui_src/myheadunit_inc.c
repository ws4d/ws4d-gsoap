/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * myheadunit_inc.c
 *
 *  Created on: 11.11.2009
 *      Author: cht
 */

#include "myheadunit_inc.h"

extern struct soap service;
extern struct dpws_s device;

void
service_exit ()
{
  printf ("Shutting down");
  dpws_deactivate_hosting_service (&device);
  soap_done (&service);
  dpws_done (&device);
  exit (0);
}

enum xsd__boolean
receptionWorker (struct mhus1__MHUKeyType *mhus1__sendKey)
{
  // whatever we have to do with received characters - we do (or trigger) it here
  printf ("Received character %02X and modifier %02X",
          *(mhus1__sendKey->Character->__ptr),
          *(mhus1__sendKey->Modifier->__ptr));
  return xsd__boolean__true_;
}
