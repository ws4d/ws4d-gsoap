/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * dpws_related.c
 *
 *  Created on: 09.04.2010
 *      Author: cht
 */

#include "dpws_related.h"
#include "dpws.nsmap"

#include "smdevp.h"
#include "wsseapi.h"
#include "config.h"

struct security_engine_s *g_sec_engine;

int rks_serve_request (struct soap *soap);
int cds_serve_request (struct soap *soap);

const char *
lookup_service_uri (struct dpws_s *device, const char *service_name)
{
  struct ws4d_epr *pos;

  pos =
    device->hosting.services.callbacks->
    fget_first (&(device->hosting.services));
  while (pos)
    {
      char *pos_service_name;
      pos_service_name = strrchr (pos->Address, '/');
      if (!strcmp (service_name, pos_service_name + 1))
        {
          return (const char *) pos->Address;
        }
      pos =
        device->hosting.services.callbacks->
        fget_next (&(device->hosting.services), pos);
    }
  return (const char *) NULL;
}

int shutdown_dpws_thread = 0;
struct soap *g_remoteKeyboardService, *g_carDataService,
  *g_authentication_service, *g_hosting;
struct dpws_s *g_device;

void *soap_serve_loop ();
pthread_t soap_serve_thread = 0;

int
dpws_start_device (struct soap *hosting, struct soap *remoteKeyboardService,
                   struct soap *carDataService,
                   struct soap *authentication_service,
                   struct security_engine_s *sec_engine,
                   struct dpws_s *device, const char *interface,
                   const char *uuid, ppin_handler propagate_pin,
                   get_dyn_auth_handler auth_handler)
{
  const char *rk_service_uri;
  int err;

  g_hosting = hosting;
  g_remoteKeyboardService = remoteKeyboardService;
  g_carDataService = carDataService;
  g_authentication_service = authentication_service;
  g_device = device;
  g_sec_engine = sec_engine;

  // Initialize the hosting Service
  soap_init (hosting);
  soap_set_omode (hosting, SOAP_XML_INDENT);
  soap_set_namespaces (hosting, dpws_namespaces);
  wsa_register_handle (hosting);

  // Initialize RemoteKeyboardService
  soap_init (remoteKeyboardService);
  soap_set_omode (remoteKeyboardService, SOAP_XML_INDENT);

  // Initialize CarDataService
  soap_init (carDataService);
  soap_set_omode (carDataService, SOAP_XML_INDENT);

  // Initialize authentication service
  soap_init (authentication_service);
  soap_set_omode (authentication_service, SOAP_XML_INDENT);

  /******************************/
  /* set up security sec_engine */
  /******************************/
  security_engine_init (sec_engine);
  /* register ca-, cert- and keyfile */
  sec_engine->cafile = SECSHELLPATH "/certs/demoCA/cacert.pem";
  sec_engine->certfile = SECSHELLPATH "/certs/myDeviceCert.pem";
  sec_engine->keyfile = SECSHELLPATH "/certs/myDeviceKey.pem";
  sec_engine->keypassword = "password";
  /* register cert_db-location */
  sec_engine->cert_list_location = "device.cdb";
  /* forward declarations are created by EXTERN_INIT_TABLE("ws4d") */
  /* register authentication engine */
  ws4d_init_authentication_engine (sec_engine);
  /* register certificate database */
  ws4d_init_certificate_db (sec_engine);
  /* register authorization-engine and -database */
  ws4d_init_authorization_engine (sec_engine);
  ws4d_init_authorization_db (sec_engine);

  /* register graphical callbacks */
  /* display generated PIN */
  if (propagate_pin)
    sec_engine->ae_propagate_pin = propagate_pin;
  /* ask for authorization */
  if (auth_handler)
    sec_engine->authe_request_dynamic_authorization_oob = auth_handler;

  /* register security-handler-plugin to soap handle - besides, this way,
   * a security engine is associated with a soap handle */
  soap_register_plugin_arg (remoteKeyboardService, sh_plugin, sec_engine);
  soap_register_plugin_arg (carDataService, sh_plugin, sec_engine);
  soap_register_plugin_arg (authentication_service, sh_plugin, sec_engine);

  /* SetUp and configure WSSE-Plugin for the service-handle, this must be done
   * AFTER registering the certificate database, since this registers the
   * callback to handle binary security tokens, passed as a parameter in
   * soap_register_plugin_arg()*/

  soap_register_plugin_arg (remoteKeyboardService, soap_wsse,
                            sec_engine->cdb_wsse_token_handler);
  remoteKeyboardService->cafile = sec_engine->cafile;

  soap_register_plugin_arg (carDataService, soap_wsse,
                            sec_engine->cdb_wsse_token_handler);
  carDataService->cafile = sec_engine->cafile;

  /* wsse plugin for authentication service is registered automatically if you didn't do it */

  /* activate automatic verification of signatures for hosted service */
  soap_wsse_verify_init (remoteKeyboardService);
  soap_wsse_verify_auto (remoteKeyboardService, SOAP_SMD_NONE, NULL, 0);

  soap_wsse_verify_init (carDataService);
  soap_wsse_verify_auto (carDataService, SOAP_SMD_NONE, NULL, 0);

  /* now, that we set up an Authentication Service, let's insist on
   * Authenticated clients */
  sh_insist_on_authentication (remoteKeyboardService, 1);
  sh_insist_on_authentication (carDataService, 1);

  /* activate necessity to authorize requests */
  sec_engine->authe_activate_authorization (remoteKeyboardService);


#ifdef DEBUG
  printf ("dpws_init...\n");
#endif
  // Initialize Device
  if (err = dpws_init (device, interface))
    {
      printf ("dpws_init() failed wit code %d\n", err);
      dpws_done (device);
      return err;
    }
#ifdef DEBUG
  printf ("mhu_setup_HostingService...\n");
#endif
  // Initialize Hosting Service
  if (err = mhu_setup_HostingService (device, hosting, uuid, 100))
    {
      printf ("mhu_setup_Hosting_Service() failed with code %d\n", err);
      dpws_done (device);
      return err;
    }
#ifdef DEBUG
  printf ("mhu_setup_RemoteKeyboard...\n");
#endif

  if (err = setup_RemoteKeyboard (device, remoteKeyboardService,
                                  REMOTE_KEYBOARD_SERVICE_WSDL, 100))
    {
      printf ("setup_RemoteKeyboard() failed with code %d\n", err);
      dpws_done (device);
      return err;
    }

  if (err = setup_CarDataService (device, carDataService,
                                  CAR_DATA_SERVICE_WSDL, 100))
    {
      printf ("setup_CarDataService() failed with code %d\n", err);
      dpws_done (device);
      return err;
    }

  if (setup_AuthenticationService
      (device, authentication_service, AUTHENTICATIONSERVICE_WSDL, 100))
    {
      fprintf (stderr,
               "\nAuthentication Service: Error in setting up Service!\n");
      dpws_done (device);
      return 1;
    }

  /* allow dynamic (user-driven) authorization */
  /* for whole Remote Keyboard Service ... */
  rk_service_uri = lookup_service_uri (device, "RemoteKeyboard");
  sec_engine->authe_set_dynamic_authorization (sec_engine,
                                               (void *) rk_service_uri,
                                               PERMITTED, SERVICE);
  /* for each method of Car Data Service independently */
  /* for an ordinary Service invocation, the Action itself is sufficient */
  sec_engine->authe_set_dynamic_authorization (sec_engine,
                                               (void *)
                                               "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/GetTankFillIn",
                                               PERMITTED, METHOD);
  /* however, subscription have all the same method, so we need the filter string as well separated by a ; */
  sec_engine->authe_set_dynamic_authorization (sec_engine,
                                               (void *)
                                               "http://schemas.xmlsoap.org/ws/2004/08/eventing/Subscribe;http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/TankWarnEventOut",
                                               PERMITTED, METHOD);
  sec_engine->authe_set_dynamic_authorization (sec_engine,
                                               (void *)
                                               "http://schemas.xmlsoap.org/ws/2004/08/eventing/Subscribe;http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/SpeedEventOut",
                                               PERMITTED, METHOD);
  sec_engine->authe_set_dynamic_authorization (sec_engine,
                                               (void *)
                                               "http://schemas.xmlsoap.org/ws/2004/08/eventing/Subscribe;http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/SpeedTorqueEventOut",
                                               PERMITTED, METHOD);


  // Now that everything is initialized, we can setup and eventually
  // updateMetadata for description
  mhu_set_Metadata (device);
  mhu_set_wsdl (device);        // can only return SOAP_OK

  if (err = dpws_update_Metadata (device))
    {
      printf ("dpws_update_Metadata() failed with code %d\n", err);
      dpws_done (device);
      return err;
    }

  // Make the hosting service advertise its hosted services
  if (err = dpws_activate_hosting_service (device))
    {
      printf ("dpws_activate_hosting_service() failed with code %d\n", err);
      dpws_done (device);
      return err;
    }

  // Here, we have to configure the device to be an event-source, so it
  // accepts subscriptions
  if (err = dpws_activate_eventsource (device, carDataService))
    {
      printf ("dpws_activate_eventsource() failed with code %d\n", err);
      dpws_done (device);
      return err;
    }

  if (err = event_worker_init (device))
    {
      printf ("event_worker_init() failed with code %d\n", err);
      return err;
    }


#ifdef DEBUG
  printf ("\nMy Headunit: ready to serve... (Ctrl-C for shut down)\n");
#endif

// start Thread
  if (err =
      pthread_create (&soap_serve_thread, NULL, soap_serve_loop, NULL) == 0)
    return SOAP_OK;
  else
    {
      printf ("Could not start soap_serve_loop()\n");
      return err;
    }

  return 0;
}

void *
soap_serve_loop ()
{
  while (!shutdown_dpws_thread)
    {
      struct soap *handle = NULL, *soap_set[] = SOAP_HANDLE_SET (g_hosting,
                                                                 g_remoteKeyboardService,
                                                                 g_carDataService,
                                                                 g_authentication_service);
      int (*serve_requests[]) (struct soap * soap) =
        SOAP_SERVE_SET (rks_serve_request, cds_serve_request,
                        auths_serve_request);

      /* waiting for new messages */
      handle = dpws_maccept (g_device, 100000, 4, soap_set);
      if (handle)
        {
          /* dispatch messages */
          if (dpws_mserve (handle, 3, serve_requests))
            {
              soap_print_fault (handle, stderr);
            }
          /* clean up soaps internally allocated memory */
          soap_end (handle);
        }
      dpws_check_subscriptions (g_device);      // check for expired subscriptions
    }
}

int
shutdown_serve_loop (void)
{
  shutdown_dpws_thread = 1;
  if (soap_serve_thread)
    return pthread_join (soap_serve_thread, NULL);
  else
    return 0;
}

int
dpws_shutdown_device (struct dpws_s *device,
                      struct soap *authentication_service,
                      struct soap *remoteKeyboardService,
                      struct soap *carDataService, struct soap *hosting)
{
  printf ("Shutting down");
  // event_worker_shutdown(); /* no longer needed, because GUI takes care of triggering events */
  // shutdown_serve_loop(); /* sry... this just takes too long */
  if (device)
    dpws_deactivate_hosting_service (device);
  if (authentication_service)
    soap_done (authentication_service);
  if (remoteKeyboardService)
    soap_done (remoteKeyboardService);
  if (carDataService)
    soap_done (carDataService);
  if (hosting)
    soap_done (hosting);
  if (device)
    dpws_done (device);
  return 0;
}
