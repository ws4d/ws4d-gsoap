/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * Declarations of setup-routines for hosted services
 */

#ifndef __MHU_SERVICES_H__
#define __MHU_SERVICES_H__

// first approach: global variables... not even elegant, especially
//   as invoking services will change these values, so there is some
//   change, at all...

#include "security_engine.h"

#ifdef __cplusplus
extern "C"
{
#endif

  void deliver_SpeedEvent (float speed);
  void deliver_SpeedTorqueEvent (float speedTorque);
  void deliver_TankWarnEvent (float tankFill);
// void deliver_TargetEvent (double x, double y, double z);

// setup routines for services

  int
    setup_RemoteKeyboardService (struct dpws_s *device, struct soap *handle,
                                 const char *wsdl, int backlog);

  int
    setup_CarDataService (struct dpws_s *device, struct soap *handle,
                          const char *wsdl, int backlog);

// routines for eventing

#ifdef __cplusplus
}
#endif

#endif                          // __MHU_SERVICES_H__
