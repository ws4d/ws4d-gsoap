/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "tacho.h"
#include "ui_tacho.h"

Tacho::Tacho (QWidget * parent):
QWidget (parent), m_ui (new Ui::Tacho)
{
  m_ui->setupUi (this);
}

Tacho::~Tacho ()
{
  delete m_ui;
}

void
Tacho::changeEvent (QEvent * e)
{
  QWidget::changeEvent (e);
  switch (e->type ())
    {
    case QEvent::LanguageChange:
      m_ui->retranslateUi (this);
      break;
    default:
      break;
    }
}

void
Tacho::setSpeed (int speed)
{
  if ((speed >= 0) && (speed <= 180))
    this->m_ui->tachoDial->setValue (speed);
}
