/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * Eventing Routines
 */

#include "cdsinv.nsmap"
#include "dpws_device.h"
#include <pthread.h>
#include "mhu_services.h"
#include <stdlib.h>

#include "wsseapi.h"
#include "smdevp.h"

extern double curX;
extern double curY;
extern double curZ;
extern float tankFill;
extern float curSpeed;
extern float curSpeedTorque;
struct security_engine_s *g_sec_engine;

int shutdownFlag = 0;

struct dpws_s *_device = NULL;

int
event_worker_init (struct dpws_s *device)
{
  if (_device || !device)
    return SOAP_ERR;
  else
    _device = device;
  return SOAP_OK;
}

void
getCurrentCoords (double *x, double *y, double *z)
{
  *x = curX;
  *y = curY;
  *z = curZ;
}

float
getCurrentSpeed (void)
{
  return curSpeed;
}

float
getCurrentSpeedTorque (void)
{
  return curSpeedTorque;
}

float
getCurrentTank (void)
{
  return tankFill;
}

void
deliver_SpeedEvent (float speed)
{
  struct soap soap;
  struct ws4d_subscription *subs, *next;
  static EVP_PKEY *rsa_private_key = NULL;
  FILE *fd;
  static ws4dCertRef cert_ref = NULL;

  /* TODO: CRITICAL: Find way to look up references (without using "eventing_reference") */
  if (!cert_ref)
    cert_ref =
      g_sec_engine->cdb_lookup_cert_reference (g_sec_engine,
                                               "eventing_reference");

  if (!rsa_private_key)
    {
      /* reading in private key */
      if ((fd = fopen (g_sec_engine->keyfile, "r")) == 0)
        {
          fprintf (stderr, "ERROR: pem file not found\n");
          exit (1);
        }
      rsa_private_key =
        PEM_read_PrivateKey (fd, NULL, NULL,
                             (void *) g_sec_engine->keypassword);
      fclose (fd);
      if (!rsa_private_key)
        {
          fprintf (stderr, "ERROR: reading private key failed\n");
          exit (1);
        }
    }

  soap_init (&soap);
  soap_set_namespaces (&soap, cdsinv_namespaces);
  soap_set_omode (&soap, SOAP_XML_CANONICAL);
#ifdef DEBUG
  soap_set_omode (&soap, SOAP_XML_INDENT);
#endif
  soap_set_omode (&soap, SOAP_XML_CANONICAL);
  soap_register_plugin (&soap, soap_wsse);

  /* one way messages - just sign - no need to check any signatures */

  dpws_for_each_subs (subs, next, _device,
                      "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/SpeedEventOut")
  {
    struct __cdsinv1__SpeedEvent response;
    char *deliverto = dpws_subsm_get_deliveryPush_address (_device, subs);

    if (!deliverto)
      continue;

    dpws_header_gen_oneway (&soap, NULL, deliverto,
                            "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/SpeedEventOut",
                            NULL, sizeof (struct SOAP_ENV__Header));

    if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509 (&soap, cert_ref))
      {
        fprintf (stderr, "ERROR: add reference failed\n");

      }
    /* sign the msg  */
    if (soap_wsse_sign_body
        (&soap, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
      {
        fprintf (stderr, "sign body failed\n");

      }

    printf ("Sending Event to %s\n", deliverto);

    if (soap_send___cdsinv1__SpeedEvent (&soap, deliverto, NULL, speed))
      {
        soap_print_fault (&soap, stderr);
      }
    else
      {
        soap_recv___cdsinv1__SpeedEvent (&soap, &response);
      }

    soap_end (&soap);
  }

  soap_done (&soap);

}

void
deliver_SpeedTorqueEvent (float speedTorque)
{
  struct soap soap;
  struct ws4d_subscription *subs, *next;

  static EVP_PKEY *rsa_private_key = NULL;
  FILE *fd;
  static ws4dCertRef cert_ref = NULL;

  /* TODO: CRITICAL: Find way to look up references (without using "eventing_reference") */
  if (!cert_ref)
    cert_ref =
      g_sec_engine->cdb_lookup_cert_reference (g_sec_engine,
                                               "eventing_reference");


  if (!rsa_private_key)
    {
      /* reading in private key */
      if ((fd = fopen (g_sec_engine->keyfile, "r")) == 0)
        {
          fprintf (stderr, "ERROR: pem file not found\n");
          exit (1);
        }
      rsa_private_key =
        PEM_read_PrivateKey (fd, NULL, NULL,
                             (void *) g_sec_engine->keypassword);
      fclose (fd);
      if (!rsa_private_key)
        {
          fprintf (stderr, "ERROR: reading private key failed\n");
          exit (1);
        }
    }

  soap_init (&soap);
  soap_set_namespaces (&soap, cdsinv_namespaces);
  soap_set_omode (&soap, SOAP_XML_CANONICAL);
  soap_register_plugin (&soap, soap_wsse);

  dpws_for_each_subs (subs, next, _device,
                      "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/SpeedTorqueEventOut")
  {
    struct __cdsinv1__SpeedTorqueEvent response;
    char *deliverto = dpws_subsm_get_deliveryPush_address (_device, subs);

    if (!deliverto)
      continue;

    dpws_header_gen_oneway (&soap, NULL, deliverto,
                            "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/SpeedTorqueEventOut",
                            NULL, sizeof (struct SOAP_ENV__Header));

    if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509 (&soap, cert_ref))
      {
        fprintf (stderr, "ERROR: add reference failed\n");

      }
    /* sign the msg  */
    if (soap_wsse_sign_body
        (&soap, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
      {
        fprintf (stderr, "sign body failed\n");

      }

    printf ("Sending Event to %s\n", deliverto);

    if (soap_send___cdsinv1__SpeedTorqueEvent
        (&soap, deliverto, NULL, speedTorque))
      {
        soap_print_fault (&soap, stderr);
      }
    else
      {
        soap_recv___cdsinv1__SpeedTorqueEvent (&soap, &response);
      }

    soap_end (&soap);
  }

  soap_done (&soap);
}

void
deliver_TankWarnEvent (float tankFill)
{
  struct soap soap;
  struct ws4d_subscription *subs, *next;

  static EVP_PKEY *rsa_private_key = NULL;
  FILE *fd;
  static ws4dCertRef cert_ref = NULL;

  /* TODO: CRITICAL: Find way to look up references (without using "eventing_reference") */
  if (!cert_ref)
    cert_ref =
      g_sec_engine->cdb_lookup_cert_reference (g_sec_engine,
                                               "eventing_reference");


  if (!rsa_private_key)
    {
      /* reading in private key */
      if ((fd = fopen (g_sec_engine->keyfile, "r")) == 0)
        {
          fprintf (stderr, "ERROR: pem file not found\n");
          exit (1);
        }
      rsa_private_key =
        PEM_read_PrivateKey (fd, NULL, NULL,
                             (void *) g_sec_engine->keypassword);
      fclose (fd);
      if (!rsa_private_key)
        {
          fprintf (stderr, "ERROR: reading private key failed\n");
          exit (1);
        }
    }

  soap_init (&soap);
  soap_set_namespaces (&soap, cdsinv_namespaces);
  soap_set_omode (&soap, SOAP_XML_CANONICAL);
  soap_register_plugin (&soap, soap_wsse);


  dpws_for_each_subs (subs, next, _device,
                      "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/TankWarnEventOut")
  {
    struct __cdsinv1__TankWarnEvent response;
    char *deliverto = dpws_subsm_get_deliveryPush_address (_device, subs);

    if (!deliverto)
      continue;

    dpws_header_gen_oneway (&soap, NULL, deliverto,
                            "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/TankWarnEventOut",
                            NULL, sizeof (struct SOAP_ENV__Header));

    if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509 (&soap, cert_ref))
      {
        fprintf (stderr, "ERROR: add reference failed\n");

      }
    /* sign the msg  */
    if (soap_wsse_sign_body
        (&soap, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
      {
        fprintf (stderr, "sign body failed\n");

      }

    printf ("Sending Event to %s\n", deliverto);

    if (soap_send___cdsinv1__TankWarnEvent (&soap, deliverto, NULL, tankFill))
      {
        soap_print_fault (&soap, stderr);
      }
    else
      {
        soap_recv___cdsinv1__TankWarnEvent (&soap, &response);
      }

    soap_end (&soap);
  }

  soap_done (&soap);
}
