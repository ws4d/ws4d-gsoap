/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QtGui/QWidget>
#include <QTimer>
#include <stdlib.h>
#include "dpws_related.h"
#include "getauthwidget.h"
#include "mhu_services.h"

extern "C"
{
  void setReceivedCharacter (char chr);
}

namespace Ui
{
  class MainWidget;
}

enum TStatus
{ anfahren, fahren, bremsen, stehen };

class MainWidget:public QWidget
{
Q_OBJECT public:
  MainWidget (QWidget * parent = 0);
  ~MainWidget ();

  int shutdown_dpws_device (void);
  void emitNewCharSignal (char chr);
  void emitNewPinSignal (unsigned char *pin);
  void emitRequestAuth ();

  public slots:void setSpeed (int speed);
  void timerTick (void);
  void losfahren (void);
  void abbremsen (void);
  void volltanken (void);
  int start_dpws_device (void);
  void closeApplication (void);

  void setReceivedCharacter (char chr);
  void showGeneratedPin (unsigned char *pin);
  void showReqAuthDialog (void);

  void sendTankAlarm (int level);

  void adjustAuthorization (int state);

private:
    Ui::MainWidget * ui;
  enum TStatus status;
  QTimer *timer;
  int speed;
  int tank;
  void adjustSpeed ();
  void adjustTank ();
  /* DPWS related */
  struct soap *hosting;
  struct soap *remoteKeyboardService;
  struct soap *carDataService;
  struct soap *authentication_service;
  struct security_engine_s *sec_engine;
  struct dpws_s *device;
  char *interface, *uuid;

    signals:void newCharacter (char chr);
  void displayPin (unsigned char *pin);
  void requestAuth ();
};

#endif // MAINWIDGET_H
