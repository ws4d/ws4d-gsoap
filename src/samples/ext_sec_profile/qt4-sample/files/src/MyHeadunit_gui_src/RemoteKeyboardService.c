/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * Definition of setup-routine for RemoteKeyboardService, declared in
 * services.h
 * besides, implementation of service's methods
 */

/* Definition of setup-routine */

#include "rks.nsmap"
#include "dpws_device.h"
#include "mhu_metadata.h"
#include "mhu_services.h"
#include "security_engine.h"
#include "smdevp.h"

void setReceivedCharacter (char chr);

int
setup_RemoteKeyboard (struct dpws_s *device, struct soap *handle,
                      const char *wsdl, int backlog)
{
  soap_set_namespaces (handle, rks_namespaces);
  return mhu_setup_RemoteKeyboard (device, handle, wsdl, backlog);
}

enum xsd__boolean
RKSreceptionWorker (struct rks1__RKKeyType *rks1__sendKey)
{
  // whatever we have to do with received characters - we do (or trigger) it here
  if ((rks1__sendKey->Character.__ptr == NULL) ||
      (rks1__sendKey->Modifier.__ptr == NULL))
    return xsd__boolean__false_;

  printf ("%c", *(rks1__sendKey->Character.__ptr));
  fflush (stdout);
  setReceivedCharacter (*(rks1__sendKey->Character.__ptr));
  return xsd__boolean__true_;
}

int
__rks1__sendKey (struct soap *soap,
                 struct rks1__RKKeyType *rks1__sendKey,
                 enum xsd__boolean *rks1__returnValue)
{

  int err;
  ws4dCertRef client_cert_ref = NULL;
  static EVP_PKEY *rsa_private_key = (EVP_PKEY *) NULL;
  FILE *fd;

  struct sh_plugin_data *data =
    (struct sh_plugin_data *) soap_lookup_plugin (soap, SH_PLUGIN_ID);

  if (soap_wsse_verify_body (soap))
    {
      fprintf (stderr, "Signature does not include the body\n");
      soap_wsse_delete_Security (soap);
      return soap_sender_fault (soap, "Service operation not signed", NULL);
#ifdef DEBUG
    }
  else
    {
      printf ("Signature includes body - all fine.\n");
#endif // DEBUG
    }

  if (rsa_private_key == NULL)
    {
      /* reading in private key */
      if ((fd = fopen (data->sec_engine->keyfile, "r")) == 0)
        {
          fprintf (stderr, "ERROR: pem file not found\n");
          return 0;
        }
      rsa_private_key =
        PEM_read_PrivateKey (fd, NULL, NULL,
                             (void *) data->sec_engine->keypassword);
      fclose (fd);
      if (!rsa_private_key)
        {
          fprintf (stderr, "ERROR: reading private key failed\n");
          return 0;
        }
    }

  client_cert_ref =
    (char *) soap_wsse_get_KeyInfo_SecurityTokenReferenceURI (soap);

  *rks1__returnValue = RKSreceptionWorker (rks1__sendKey);

  if (err =
      dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                "http://www.bmw-carit.de/dpws-eval/usecase1/MyHeadUnit/sendKeyOut",
                                wsa_header_get_MessageId (soap),
                                sizeof (struct SOAP_ENV__Header)))
    {
      return err;
    }

  /* DON'T add cert to security header but use reference */
  /* reference the cert for signing */
  if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509
      (soap, client_cert_ref))
    {
      printf ("ERROR: add reference failed\n");
      return SOAP_ERR;
    }

  /* sign the msg */
  if (soap_wsse_sign_body (soap, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
    {
      printf ("sign body failed\n");
      return SOAP_ERR;
    }

  return SOAP_OK;
}
