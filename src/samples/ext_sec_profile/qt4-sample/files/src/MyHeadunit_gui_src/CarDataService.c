/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "cds.nsmap"
#include "dpws_device.h"
#include "mhu_metadata.h"
#include "mhu_services.h"

#include "wsseapi.h"
#include "smdevp.h"

double curX;
double curY;
double curZ;
float tankFill = 1000;
float curSpeed = 0;
float curSpeedTorque = 0;
        // so there is *something* at least...


int
setup_CarDataService (struct dpws_s *device, struct soap *handle,
                      const char *wsdl, int backlog)
{
  soap_set_namespaces (handle, cds_namespaces);
  return mhu_setup_CarDataService (device, handle, wsdl, backlog);
}

struct cds1__CoordsType *
getNaviTargetWorker (struct cds1__CoordsType *s)
{
  s->xPosition = curX;
  s->yPosition = curY;
  s->zPosition = curZ;
  return s;
}

struct cds1__CoordsType *
setNaviTargetWorker (struct cds1__CoordsType *newCoords,
                     struct cds1__CoordsType *r)
{
  curX = newCoords->xPosition;
  curY = newCoords->yPosition;
  curZ = newCoords->zPosition;
  return getNaviTargetWorker (r);
}

float
getTankFillWorker (void)
{

  return tankFill;
}

int
__cds1__GetNaviTarget (struct soap *soap,
                       struct _cds1__EmptyMessage *cds1__GetNaviTarget,
                       struct cds1__CoordsType *cds1__Coords)
{

  cds1__Coords = getNaviTargetWorker (cds1__Coords);
  return dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                   "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/GetNaviTargetOut",
                                   wsa_header_get_MessageId (soap),
                                   sizeof (struct SOAP_ENV__Header));
}

int
__cds1__SetNaviTarget (struct soap *soap,
                       struct cds1__CoordsType *cds1__Coords,
                       struct __cds1__SetNaviTargetResponse *_param_1)
{

  _param_1->cds1__Coords =
    setNaviTargetWorker (cds1__Coords, _param_1->cds1__Coords);
  return dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                   "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/SetNaviTargetOut",
                                   wsa_header_get_MessageId (soap),
                                   sizeof (struct SOAP_ENV__Header));
}

int
__cds1__GetTankFill (struct soap *soap,
                     struct _cds1__EmptyMessage *cds1__GetTankFill,
                     float *cds1__Tank)
{

  int err;

  ws4dCertRef client_cert_ref = NULL;
  ws4dX509 client_cert;
  struct _cert_list_elem *client_cert_elem = NULL;
  struct sh_plugin_data *sh_data;
  struct soap_wsse_data *wsse_data;
  static EVP_PKEY *rsa_private_key = NULL;
  FILE *fd;

  /* check, if incoming message was signed */
  if (soap_wsse_verify_body (soap))
    {
      fprintf (stderr, "GetTankFill(): Signature does not include body\n");
      soap_wsse_delete_Security (soap);
      return soap_sender_fault (soap, "Service operation not signed", NULL);
#ifdef DEBUG
    }
  else
    {
      printf ("GetTankFill(): Signature includes body!\n");
#endif //DEBUG
    }

  client_cert_ref =
    (char *) soap_wsse_get_KeyInfo_SecurityTokenReferenceURI (soap);

  sh_data = (struct sh_plugin_data *) soap_lookup_plugin (soap, SH_PLUGIN_ID);
  if (!sh_data)
    {
      fprintf (stderr,
               "getTankFill(): no sh_plugin on handle - THIS IS FATAL!\n");
      return SOAP_ERR;
    }

  *cds1__Tank = getTankFillWorker ();

  if (err =
      dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/GetTankFillOut",
                                wsa_header_get_MessageId (soap),
                                sizeof (struct SOAP_ENV__Header)))
    {
      fprintf (stderr,
               "dpws_header_gen_response() failed in __cds1__GetTankFill() (%d)\n",
               err);
    }

  if (rsa_private_key == NULL)
    {
      /* reading in private key */
      if ((fd = fopen (sh_data->sec_engine->keyfile, "r")) == 0)
        {
          fprintf (stderr, "GetTankFill(): ERROR: pem file not found\n");
          return 1;
        }
      rsa_private_key =
        PEM_read_PrivateKey (fd, NULL, NULL,
                             (void *) sh_data->sec_engine->keypassword);
      fclose (fd);
      if (!rsa_private_key)
        {
          fprintf (stderr,
                   "GetTankFill(): ERROR: reading private key failed\n");
          return 2;
        }
    }


  if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509
      (soap, client_cert_ref))
    {
      printf ("ERROR: add reference failed\n");
      return SOAP_ERR;
    }

  /* sign the msg */
  if (soap_wsse_sign_body (soap, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
    {
      printf ("sign body failed\n");
      return SOAP_ERR;
    }

  return SOAP_OK;
}
