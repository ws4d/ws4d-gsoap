/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "getauthwidget.h"
#include "ui_getauthwidget.h"


GetAuthWidget::GetAuthWidget (struct authorization_s *auth, char *identifier,
                              char *ressource, int *dur, QWidget * parent):
QDialog (parent),
m_ui (new Ui::GetAuthWidget)
{
  m_ui->setupUi (this);
  this->authorization = auth;
  this->duration = dur;

  m_ui->CredentialEdit->insertPlainText (QString::fromAscii (identifier));

  /* create neat Ressource String */
  int eventing = 0;
  char *cursor = NULL;

  QString *ev_prfix;

  if (cursor = strchr ((char *) ressource, ';'))
    {
      eventing = 1;
      ev_prfix = new QString ("Eventquelle: ");
    }
  else
    ev_prfix = new QString ("");

  cursor = strrchr ((char *) ressource, '/');

  m_ui->ressourceLabel->setText (*ev_prfix +=
                                 QString::fromAscii (cursor + 1));

  connect (m_ui->OKButton, SIGNAL (clicked ()), this, SLOT (decisionMade ()));
}

GetAuthWidget::~GetAuthWidget ()
{
  delete m_ui;
}


void
GetAuthWidget::decisionMade (void)
{
  /* I'm really sorry for this...
   * idea: return -1, 0, 1, or 3 OR (and now, it gets nasty)
   * the SpinBoxValue times 60;
   *
   * I tried to come by all this with passing references (see all the
   * constructor's parameters) bit it didn't work and eventually I was just
   * tired keep on trying :-/
   */
  if (m_ui->denyRadio->isChecked ())
    {
      this->done ((int) DENIED);
      this->authorization = NULL;
      return;
    }
  else if (m_ui->permitOnceRadio->isChecked ())
    {
      this->done ((int) PERMITTED_ONCE);
      this->authorization = NULL;
      return;
    }
  else if (m_ui->permitTemporaryRadio->isChecked ())
    {
      this->done (m_ui->minutesBox->value () * 60);
      this->authorization = NULL;
      return;
    }
  else if (m_ui->permitRadio->isChecked ())
    {
      this->done ((int) PERMITTED);
      this->authorization = NULL;
      return;
    }
  else
    {
      printf ("Sth. strange with requesting an authorization\n");
      this->done (-1);
      this->authorization = NULL;
      return;
    }

}
