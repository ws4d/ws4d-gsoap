/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * dpws_related.h
 *
 *  Created on: 09.04.2010
 *      Author: cht
 */

#ifndef DPWS_RELATED_H_
#define DPWS_RELATED_H_

#include "dpws_device.h"
#include "mhu_metadata.h"
#include "mhu_wsdl.h"
#include "config.h"
#include "mhu_services.h"
#include <unistd.h>             // at least for parsing options

/* security related */
#include "authentication_service.h"
#include "security_engine.h"

#ifdef __cplusplus
extern "C"
{
#endif

  typedef void (*ppin_handler) (struct security_engine_s *, ws4dPin);
  typedef void (*get_dyn_auth_handler) (struct soap *,
                                        struct security_engine_s *, void *,
                                        struct authorization_s *);

  int dpws_start_device (struct soap *hosting,
                         struct soap *remoteKeyboardService,
                         struct soap *carDataService,
                         struct soap *authentication_service,
                         struct security_engine_s *sec_engine,
                         struct dpws_s *device, const char *interface,
                         const char *uuid, ppin_handler propagate_pin,
                         get_dyn_auth_handler auth_handler);

  int dpws_shutdown_device (struct dpws_s *device,
                            struct soap *authentication_service,
                            struct soap *remoteKeyboardService,
                            struct soap *carDataService,
                            struct soap *hosting);

#ifdef __cplusplus
}
#endif

#endif                          /* DPWS_RELATED_H_ */
