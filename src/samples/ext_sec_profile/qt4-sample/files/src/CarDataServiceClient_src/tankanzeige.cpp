/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "tankanzeige.h"
#include "ui_tankanzeige.h"

Tankanzeige::Tankanzeige (QWidget * parent):
QWidget (parent), m_ui (new Ui::Tankanzeige)
{
  m_ui->setupUi (this);
  this->tankAlarmsSent = -1;
}

Tankanzeige::~Tankanzeige ()
{
  delete m_ui;
}

void
Tankanzeige::changeEvent (QEvent * e)
{
  QWidget::changeEvent (e);
  switch (e->type ())
    {
    case QEvent::LanguageChange:
      m_ui->retranslateUi (this);
      break;
    default:
      break;
    }
}

void
Tankanzeige::setValue (int value)
{
  if ((value >= 0) && (value <= this->m_ui->TankBar->maximum ()))
    {
      this->m_ui->TankBar->setValue (value);

      QPalette p = this->m_ui->TankBar->palette ();
      p.setColor (QPalette::Highlight,
                  QColor (255 - (int) (0.255 * value), (int) (0.255 * value),
                          10, 255));
      this->m_ui->TankBar->setPalette (p);


      p = this->m_ui->AlarmButton->palette ();
      if (value > 150)
        {
          if (this->tankAlarmsSent != 0)
            {
              p.setColor (QPalette::Button, QColor (0, 0xFF, 0, 0xFF));
              this->tankAlarmsSent = 0;
            }
        }
      else if (value < 50)
        {
          if (this->tankAlarmsSent != 2)
            {
              p.setColor (QPalette::Button, QColor (0xFF, 00, 0, 0xFF));
              emit this->TankAlarm (5);
              this->tankAlarmsSent = 2;
            }
        }
      else
        {
          if (this->tankAlarmsSent != 1)
            {
              p.setColor (QPalette::Button, QColor (0xFF, 200, 20, 0xFF));
              emit this->TankAlarm (15);
              this->tankAlarmsSent = 1;
            }
        }
      this->m_ui->AlarmButton->setPalette (p);
    }
}
