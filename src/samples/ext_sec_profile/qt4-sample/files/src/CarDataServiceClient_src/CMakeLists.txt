# Copyright (C) 2007  University of Rostock
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.

################################################################################
# Common for all ws4d-gsoap projects, don't change this !
################################################################################

include(${QT_USE_FILE})

# directory to put all generated files in
SET(gen_DIR ${CMAKE_CURRENT_BINARY_DIR}/gen)

# If the directory for the generated sources does not
# already exists, create it.
IF(NOT EXISTS ${gen_DIR})
	FILE(MAKE_DIRECTORY ${gen_DIR})
ENDIF(NOT EXISTS ${gen_DIR})

# set include paths
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR} ${GSOAP_INCLUDE_DIR} ${DPWS_INCLUDES} ${gen_DIR} ${GSOAP_INCLUDE_DIR}/plugin)

# set options for gSOAP runtime
GSOAP_SET_RUNTIME_FLAGS("-DWITH_NONAMESPACES -DWITH_UDP -DWITH_OPENSSL -DWITH_DOM")

###############################################################################
###############################################################################
##                                                                           ##
## Remote Keyboard Sercice GUI Client                                        ##
##                                                                           ##
###############################################################################
###############################################################################

###############################################################################
# Code generation - for custom projects this must be modified
###############################################################################

# generate code and C data binding operations of service
#GSOAP_GENERATE(RemoteKeyboardService.h rks ${gen_DIR})
GSOAP_GENERATE(CarDataService.h cds ${gen_DIR}) 
GSOAP_GENERATE(CarDataServiceEvents.h cdsinv ${gen_DIR})

# generate device description and setup code
DPWS_METADATA_GENERATE(MyHeadunitMetadata.xml mhu ${gen_DIR})

# embed wsdl files into c code
SET(AUTHENTICATIONSERVICE_WSDL ${SECSHELLPATH}/AuthenticationService.wsdl)
#SET(REMOTE_KEYBOARD_SERVICE_WSDL ${CMAKE_CURRENT_SOURCE_DIR}/RemoteKeyboardService.wsdl)
SET(CAR_DATA_SERVICE_WSDL ${CMAKE_CURRENT_SOURCE_DIR}/CarDataService.wsdl)

# simply append future service between brackets
DPWS_EMBEDD_WSDL(${gen_DIR} mhu ${AUTHENTICATIONSERVICE_WSDL}
                                ${CAR_DATA_SERVICE_WSDL}
                                ) 


################################################################################
# create config.h
################################################################################

CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/config.h.in
               ${gen_DIR}/config.h
               @ONLY)

###############################################################################
###############################################################################
##                                                                           ##
## Remote Keyboard GUI Client                                                ##
##                                                                           ##
###############################################################################
###############################################################################

################################################################################
# build simple client - for custom projects this must be modified
################################################################################

# set ui file(s), created by Qt-Designer
SET (cds_gui_UIS
     cardataserviceclient.ui
     drehzahlmesser.ui
     tacho.ui
     tankanzeige.ui
    )
# generate ui_*.h
QT4_WRAP_UI(cds_gui_UIS_H ${cds_gui_UIS})

# moc the files to be moc'ed
SET(cds_gui_MOC_HDRS
    cardataserviceclient.h
    drehzahlmesser.h
    tacho.h
    tankanzeige.h
    )
QT4_WRAP_CPP(cds_gui_MOC_SRCS ${cds_gui_MOC_HDRS})

#sources
SET(CarDataService_client_SRCS
    cds_client_main.cpp
    cardataserviceclient.cpp
    drehzahlmesser.cpp
    tacho.cpp
    tankanzeige.cpp
    dpws_startup.c
    dpws_cds.c
    dpws_cds_events.c
    ${GSOAP_STDSOAP2_SOURCE}
    ${gen_DIR}/cdsinvServerLib.c
    ${gen_DIR}/cdsClientLib.c
	${SECSHELLPATH}/src/authentication_engine.c
	${SECSHELLPATH}/src/cert_db.c
	${SECSHELLPATH}/src/security_handler.c
	${SECSHELLPATH}/src/crypto_engine.c
	)

ADD_EXECUTABLE(mhu_CarDataServiceClient_gui 
                    ${CarDataService_client_SRCS}
                    ${cds_gui_UIS_H}
                    ${cds_gui_MOC_SRCS}
                    )

EXTEND_TGT_COMPILE_FLAGS(mhu_CarDataServiceClient_gui FLAGS "-DDPWS_CLIENT -DWITH_OPENSSL -DWITH_DOM -DWITH_MUTEXES")

TARGET_LINK_LIBRARIES(mhu_CarDataServiceClient_gui 
                      ${DPWS_LIBRARIES} 
                      ${DPWS-CMT_LIBRARIES}
                      ${DPWS_AUTHC_SERVICE} 
                      ${WSSEAPI_LIB} 
                      -lssl -lcrypto
                      ${QT_LIBRARIES}
                      )
