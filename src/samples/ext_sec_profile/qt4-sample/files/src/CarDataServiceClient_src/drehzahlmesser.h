/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#ifndef DREHZAHLMESSER_H
#define DREHZAHLMESSER_H

#include <QtGui/QWidget>

namespace Ui
{
  class Drehzahlmesser;
}

class Drehzahlmesser:public QWidget
{
Q_OBJECT public:
    Drehzahlmesser (QWidget * parent = 0);
   ~Drehzahlmesser ();

  public slots:void setValue (int value);

protected:
  void changeEvent (QEvent * e);

private:
    Ui::Drehzahlmesser * m_ui;
};

#endif // DREHZAHLMESSER_H
