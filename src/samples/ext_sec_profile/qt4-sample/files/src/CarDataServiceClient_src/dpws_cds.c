/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * dpws_cds.c
 *
 *  Created on: 11.04.2010
 *      Author: cht
 */

#include "dpws_cds.h"
#include "cds.nsmap"

#include "wsseapi.h"
#include "smdevp.h"

int
cds_register_wsse (struct soap *soap,
                   struct security_engine_s *sec_engine,
                   int activate_signature_verification)
{
  int err;
  if (err =
      soap_register_plugin_arg (soap, soap_wsse,
                                sec_engine->cdb_wsse_token_handler))
    return err;
  if (activate_signature_verification)
    {
      soap_wsse_verify_init (soap);
      /* check signature */
      if (soap_wsse_verify_auto (soap, SOAP_SMD_NONE, NULL, 0))
        {
          fprintf (stderr,
                   "Could not activate automatic signature validation on service_client\n");
          return 1;
        }
    }
  return SOAP_OK;
}

int
dpws_get_tank_fill (struct soap *client, struct ws4d_epr *service,
                    struct security_engine_s *sec_engine,
                    struct ws4d_epr *device, float *fill)
{
  int ret, err;
  static EVP_PKEY *rsa_private_key = (EVP_PKEY *) NULL;
  FILE *fd;

  soap_set_namespaces (client, cds_namespaces);
  dpws_header_gen_request (client, NULL, ws4d_epr_get_Addrs (service), "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/GetTankFillIn", NULL, // wsa:action, btw...
                           NULL, sizeof (struct SOAP_ENV__Header));

  soap_set_omode (client, SOAP_XML_CANONICAL);

  if (rsa_private_key == NULL)
    {
      /* reading in private key */
      if ((fd = fopen (sec_engine->keyfile, "r")) == 0)
        {
          fprintf (stderr, "ERROR: pem file not found\n");
          return 0;
        }
      rsa_private_key =
        PEM_read_PrivateKey (fd, NULL, NULL,
                             (void *) sec_engine->keypassword);
      fclose (fd);
      if (!rsa_private_key)
        {
          fprintf (stderr, "ERROR: reading private key failed\n");
          return 0;
        }
    }

  /* reference the cert for signing */
  if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509 (client,
                                                        sec_engine->
                                                        cdb_lookup_cert_reference
                                                        (sec_engine,
                                                         device->Address)))
    {
      fprintf (stderr, "ERROR: add reference failed\n");
      return 1;
    }
  /* sign the msg  */
  if (soap_wsse_sign_body
      (client, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
    {
      fprintf (stderr, "GetTankFill: sign body failed\n");
      return 1;
    }

  // Let's eventually invoke the service
  if ((err =
       soap_call___cds1__GetTankFill (client, ws4d_epr_get_Addrs (service),
                                      NULL, NULL, fill)) == SOAP_OK)
    {

      if (soap_wsse_verify_body (client))
        {
          fprintf (stderr, "GetTankFill: Signature does not include body\n");
          soap_wsse_delete_Security (client);
          return soap_sender_fault (client, "Service operation not signed",
                                    NULL);
#ifdef DEBUG
        }
      else
        {
          printf ("GetTankFill: Signature includes body!\n");
#endif //DEBUG
        }

#ifdef DEBUG
      printf ("Got TankFill: %.1f\n", *fill);
#endif
    }
  else
    {
      fprintf (stderr, "Calling GetTankFill failed: %d\n", err);
      return err;
    }
  soap_end (client);
//  *fill *= 10;
  return err;
}
