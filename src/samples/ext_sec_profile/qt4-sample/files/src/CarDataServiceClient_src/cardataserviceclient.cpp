/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "cardataserviceclient.h"
#include "ui_cardataserviceclient.h"
#include <QMessageBox>
#include <QInputDialog>

/* global wrappers for connecting Qt (C++) and DPWS (C) */
CarDataServiceClient::CarDataServiceClient * global_widget = NULL;
void
setSpeedFromEvent (int speed)
{
  global_widget->emitSpeedSignal (speed);
}

void
setTorqueFromEvent (int torque)
{
  global_widget->emitTorqueSignal (torque);
}

void
raiseTankWarning (float level)
{
  global_widget->emitTankSignal (level);
}

ws4dPin
gui_pin_handler (struct security_engine_s *s, ws4dPin pin)
{
  return global_widget->ask_for_pin (s, NULL);
}

CarDataServiceClient::CarDataServiceClient (QWidget * parent):
QWidget (parent), m_ui (new Ui::CarDataServiceClient)
{
  m_ui->setupUi (this);

  global_widget = this;

  /* set IP and uuid to env-variables (if set) */
  m_ui->ipEdit->
    setText (QString ((const char *) getenv ("theIP")).trimmed ());
  m_ui->urnEdit->
    setText (QString ((const char *) getenv ("theDEVICE")).trimmed ());

  this->interface = NULL;
  this->urn = NULL;
  this->speedID = NULL;
  this->torqueID = NULL;
  this->dpws = NULL;
  this->device = NULL;
  this->service = NULL;
  this->services = NULL;
  this->sec_engine = NULL;
  this->client = NULL;
  this->service_client = NULL;
  this->auth_client = NULL;
  this->speed_events = NULL;
  this->torque_events = NULL;
  this->tank_events = NULL;

  connect (m_ui->connectButton, SIGNAL (clicked ()), this,
           SLOT (startDPWS ()));

  connect (m_ui->subSpeedButton, SIGNAL (clicked ()), this,
           SLOT (subscribeSpeed ()));
  connect (m_ui->usubSpeedButton, SIGNAL (clicked ()), this,
           SLOT (unsubscribeSpeed ()));
  connect (this, SIGNAL (speedEvent (int)), m_ui->TachoWidget,
           SLOT (setSpeed (int)));

  connect (m_ui->subTorqueButton, SIGNAL (clicked ()), this,
           SLOT (subscribeTorque ()));
  connect (m_ui->usubTorqueButton, SIGNAL (clicked ()), this,
           SLOT (unsubscribeTorque ()));
  connect (this, SIGNAL (torqueEvent (int)), m_ui->DrehzahlWidget,
           SLOT (setValue (int)));

  connect (m_ui->getTankButton, SIGNAL (clicked ()), this,
           SLOT (getTankFill ()));

  connect (this, SIGNAL (tankWarnEvent (float)), this,
           SLOT (showTankWarning (float)));

  connect (m_ui->QuitButton, SIGNAL (clicked ()), this,
           SLOT (closeApplication ()));
}

CarDataServiceClient::~CarDataServiceClient ()
{
  delete m_ui;
}

void
CarDataServiceClient::emitSpeedSignal (int speed)
{
  emit this->speedEvent (speed);
}

void
CarDataServiceClient::emitTorqueSignal (int torque)
{
  emit this->torqueEvent (torque);
}

void
CarDataServiceClient::emitTankSignal (float level)
{
  emit this->tankWarnEvent (level);
}

void
CarDataServiceClient::startDPWS (void)
{
  int err;
  /* initialize */
  this->interface = (char *) malloc (m_ui->ipEdit->text ().length () + 1);
  memset (this->interface, 0, m_ui->ipEdit->text ().length () + 1);
  strcpy (this->interface, m_ui->ipEdit->text ().toAscii ().data ());

  this->urn = (char *) malloc (m_ui->urnEdit->text ().length () + 1);
  memset (this->urn, 0, m_ui->urnEdit->text ().length () + 1);
  strcpy (this->urn, m_ui->urnEdit->text ().toAscii ().data ());

  this->dpws = (struct dpws_s *) malloc (sizeof (struct dpws_s));
  this->device = (struct ws4d_epr *) malloc (sizeof (struct ws4d_epr));
  this->services =
    (struct ws4d_abs_eprlist *) malloc (sizeof (struct ws4d_abs_eprlist));
  this->sec_engine =
    (struct security_engine_s *) malloc (sizeof (struct security_engine_s));
  this->client = (struct soap *) malloc (sizeof (struct soap));
  this->service_client = (struct soap *) malloc (sizeof (struct soap));
  this->auth_client = (struct soap *) malloc (sizeof (struct soap));
  this->speed_events = (struct soap *) malloc (sizeof (struct soap));
  this->torque_events = (struct soap *) malloc (sizeof (struct soap));
  this->tank_events = (struct soap *) malloc (sizeof (struct soap));

  if (err =
      dpws_start (&(this->alist), this->client, this->speed_events,
                  this->dpws, this->interface, this->urn, this->device,
                  &(this->service), this->services, &(this->speed_delivery),
                  &(this->speed_filter), this->torque_events,
                  &(this->torque_delivery), &(this->torque_filter),
                  this->tank_events, &(this->tank_delivery),
                  &(this->tank_filter), this->service_client,
                  this->sec_engine, this->auth_client, gui_pin_handler))
    {

      free (this->interface);
      free (this->urn);
      free (this->dpws);
      free (this->device);
      free (this->services);
      free (this->sec_engine);
      free (this->client);
      free (this->service_client);
      free (this->auth_client);
      free (this->speed_events);
      free (this->torque_events);
      free (this->tank_events);

      this->interface = NULL;
      this->speedID = NULL;
      this->torqueID = NULL;
      this->urn = NULL;
      this->dpws = NULL;
      this->device = NULL;
      this->service = NULL;
      this->services = NULL;
      this->sec_engine = NULL;
      this->client = NULL;
      this->service_client = NULL;
      this->auth_client = NULL;
      this->speed_events = NULL;
      this->torque_events = NULL;
      this->tank_events = NULL;

      printf ("Got Error %d \n", err);

      return;
    }
  m_ui->okEdit->setText (QString ("OK"));
  m_ui->subSpeedButton->setEnabled (true);
  m_ui->subTorqueButton->setEnabled (true);
  m_ui->getTankButton->setEnabled (true);
  this->subscribeTank ();
}

void
CarDataServiceClient::subscribeSpeed (void)
{
  int err;
  if (err =
      dpws_subscribe_to_speedevents (&(this->speedID), this->client,
                                     this->service, this->speed_delivery,
                                     this->speed_filter, this->speed_events,
                                     this->urn))
    {
      fprintf (stderr, "Error in Subscription to Speed events! (%d)\n", err);
      return;
    }
  m_ui->subSpeedButton->setDisabled (true);
  m_ui->usubSpeedButton->setEnabled (true);
}

void
CarDataServiceClient::unsubscribeSpeed (void)
{
  int err;
  if (err = dpws_unsubscribe_from_speed_events ())
    {
      fprintf (stderr, "Error in Unsubscribing from Speed Events! (%d)", err);
      return;
    }
  m_ui->subSpeedButton->setDisabled (false);
  m_ui->usubSpeedButton->setEnabled (false);
}

void
CarDataServiceClient::subscribeTorque (void)
{
  int err;
  if (err =
      dpws_subscribe_to_torqueevents (&(this->torqueID), this->client,
                                      this->service, this->torque_delivery,
                                      this->torque_filter,
                                      this->torque_events, this->urn))
    {
      fprintf (stderr, "Error in Subscription to Torque events! (%d)\n", err);
      return;
    }
  m_ui->subTorqueButton->setDisabled (true);
  m_ui->usubTorqueButton->setEnabled (true);
}

void
CarDataServiceClient::unsubscribeTorque (void)
{
  int err;
  if (err = dpws_unsubscribe_from_torque_events ())
    {
      fprintf (stderr,
               "Error in Unsubscribing from Speed Torque Events! (%d)", err);
      return;
    }
  m_ui->subTorqueButton->setDisabled (false);
  m_ui->usubTorqueButton->setEnabled (false);
}

void
CarDataServiceClient::subscribeTank (void)
{
  int err;
  if (err =
      dpws_subscribe_to_tankevents (&(this->tankID), this->client,
                                    this->service, this->tank_delivery,
                                    this->tank_filter, this->tank_events,
                                    this->urn))
    {
      fprintf (stderr, "Error in Subscription to tank events! (%d)\n", err);
      return;
    }
}

void
CarDataServiceClient::unsubscribeTank (void)
{
  int err;
  if (err = dpws_unsubscribe_from_tank_events ())
    {
      fprintf (stderr, "Error in Unsubscribing from Tank Events! (%d)", err);
      return;
    }
}

void
CarDataServiceClient::getTankFill (void)
{
  float tankFill;
  int err;
  if (err =
      dpws_get_tank_fill (this->service_client, this->service,
                          this->sec_engine, this->device, &tankFill))
    {
      fprintf (stderr, "Error Calling GetTankFill()! (%d)\n", err);
      return;
    }
  m_ui->TankWidget->setValue ((int) tankFill);
}

void
CarDataServiceClient::showTankWarning (float level)
{
  char *l_string;
  l_string = (char *) malloc (15);
  memset (l_string, 0, 15);
  snprintf (l_string, 14, "%.1f%%", level);
  if (level > 10)
    {
      QMessageBox::warning (this, QString ("Warnung! Tankstand!"),
                            QString ("Warnung! Tankstand bei ")
                            += QString::fromAscii (l_string)
                            += QString ("!"));
    }
  else
    {
      QMessageBox::critical (this, QString ("KRITISCH! Tankstand!"),
                             QString ("KRITISCH! Tankstand bei ")
                             += QString::fromAscii (l_string)
                             += QString ("!"));
    }
}

ws4dPin
  CarDataServiceClient::ask_for_pin (struct security_engine_s *sec_engine,
                                     ws4dPin pin)
{
  bool ok;
  if (pin == NULL)
    {
      pin = (ws4dPin) malloc (PIN_SIZE);
    }
  QString pinQString =
    QInputDialog::getText (this, "Please enter PIN", "Please enter PIN",
                           QLineEdit::Normal, "pin", &ok);
  for (int i = 0; i < PIN_SIZE; i++)
    pin[i] = pinQString.toAscii ().data ()[i];
  return pin;
}

void
CarDataServiceClient::closeApplication (void)
{

  this->unsubscribeTank ();

  // TODO: Clean up!

  if (this->interface)
    free (this->interface);
  if (this->urn)
    free (this->urn);
  if (this->dpws)
    free (this->dpws);
  if (this->device)
    free (this->device);
  if (this->services)
    free (this->services);
  if (this->sec_engine)
    free (this->sec_engine);
  if (this->client)
    free (this->client);
  if (this->service_client)
    free (this->service_client);
  if (this->speed_events)
    free (this->speed_events);
  if (this->torque_events)
    free (this->torque_events);
  if (this->tank_events)
    free (this->tank_events);
  qApp->quit ();
}


#ifdef __cplusplus
/* to come by that *** pseudo-C++ by gSoap */
soap::soap ()
{

}

soap::~soap ()
{

}
#endif
