/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * dpws_cds_events.c
 *
 *  Created on: 11.04.2010
 *      Author: cht
 */

#include "dpws_cds_events.h"
#include "cdsinv.nsmap"

#include "wsseapi.h"
#include "smdevp.h"

/*****************************************************/
/* TODO: Seriously refurbish this file - most was    */
/* copy'n'pasted - I'm quite sure, this file can be  */
/* stripped down to around one third since most of   */
/* the functions with similar names do very similar  */
/* things...                                         */
/*****************************************************/

int speed_shutdown_flag = 0;
struct soap *g_speed_events;
pthread_t speed_event_thread;
void *speed_event_loop ();

int torque_shutdown_flag = 0;
struct soap *g_torque_events;
pthread_t torque_event_thread;
void *torque_event_loop ();

int tank_shutdown_flag = 0;
struct soap *g_tank_events;
pthread_t tank_event_thread;
void *tank_event_loop ();


void *
speed_event_loop ()
{
  while (!speed_shutdown_flag)
    {

#ifdef DEBUG
      printf ("\nTachoEventListener: waiting for events\n");
#endif

      if (soap_accept (g_speed_events) != SOAP_INVALID_SOCKET)
        {

#ifdef DEBUG
          printf ("\nTachoEventListener: processing event from %s:%d\n",
                  inet_ntoa (g_speed_events->peer.sin_addr),
                  ntohs (g_speed_events->peer.sin_port));
#endif

          if (cdsinv_serve (g_speed_events))
            {
              soap_print_fault (g_speed_events, stderr);
            }

          /* clean up soaps internaly allocated memory */
          soap_end (g_speed_events);
        }
    }
}

void *
torque_event_loop ()
{
  while (!torque_shutdown_flag)
    {

#ifdef DEBUG
      printf ("\nTachoEventListener: waiting for events\n");
#endif

      if (soap_accept (g_torque_events) != SOAP_INVALID_SOCKET)
        {

#ifdef DEBUG
          printf ("\nTachoEventListener: processing event from %s:%d\n",
                  inet_ntoa (g_torque_events->peer.sin_addr),
                  ntohs (g_torque_events->peer.sin_port));
#endif

          if (cdsinv_serve (g_torque_events))
            {
              soap_print_fault (g_torque_events, stderr);
            }

          /* clean up soaps internaly allocated memory */
          soap_end (g_torque_events);
        }
    }
}

void *
tank_event_loop ()
{
  while (!tank_shutdown_flag)
    {

#ifdef DEBUG
      printf ("\nTankEventListener: waiting for events\n");
#endif

      if (soap_accept (g_tank_events) != SOAP_INVALID_SOCKET)
        {

#ifdef DEBUG
          printf ("\nTankEventListener: processing event from %s:%d\n",
                  inet_ntoa (g_tank_events->peer.sin_addr),
                  ntohs (g_tank_events->peer.sin_port));
#endif

          if (cdsinv_serve (g_tank_events))
            {
              soap_print_fault (g_tank_events, stderr);
            }

          /* clean up soaps internaly allocated memory */
          soap_end (g_tank_events);
        }
    }
}

void
set_cdsinv_namespaces (struct soap *handle)
{
  soap_set_namespaces (handle, cdsinv_namespaces);
}

int
cdsinv_register_wsse (struct soap *soap,
                      struct security_engine_s *sec_engine,
                      int activate_signature_verification)
{
  int err;
  if (err =
      soap_register_plugin_arg (soap, soap_wsse,
                                sec_engine->cdb_wsse_token_handler))
    return err;
  if (activate_signature_verification)
    {
      soap_wsse_verify_init (soap);
      /* check signature */
      if (soap_wsse_verify_auto (soap, SOAP_SMD_NONE, NULL, 0))
        {
          fprintf (stderr,
                   "Could not activate automatic signature validation on service_client\n");
          return 1;
        }
    }
  return SOAP_OK;
}

int
dpws_subscribe_to_speedevents (char **id, struct soap *client,
                               struct ws4d_epr *service,
                               struct ws4d_delivery_type *delivery,
                               struct ws4d_filter_type *filter,
                               struct soap *speed_events,
                               const char *endpoint)
{

  ws4d_time duration = 3600;
  int err;

  char *ep_cpy = malloc (strlen (endpoint) + 1);
  memset (ep_cpy, 0, strlen (endpoint) + 1);
  strcpy (ep_cpy, endpoint);

  g_speed_events = speed_events;

  /* Subscribing to Airconditioner Service */
#ifdef DEBUG
  printf
    ("\nTachoEventListener: Subscribe to SpeedEvents@CarDataService ... ");
#endif
  *id =
    (char *) dpws_subscribe (client, service, (const char *) ep_cpy,
                             &duration, delivery, filter);
  if (!(*id))
    {
      soap_print_fault (client, stderr);
      return 1;
    }

#ifdef DEBUG
  printf ("OK (%s %s)\n", ws4d_subsproxy_getsubsman (service, *id), *id);
#endif

  /* set accept timeout to one second */
  speed_events->accept_timeout = 1;

#ifdef DEBUG
  printf ("\nTachoEventListener: ready to receive Speed events ... \n");
#endif

  if (err =
      pthread_create (&speed_event_thread, NULL, speed_event_loop, NULL))
    {
      fprintf (stderr, "Could not launch thread to catch speed events!\n");
      return err;
    }

  return SOAP_OK;
}

int
dpws_unsubscribe_from_speed_events (void)
{
  speed_shutdown_flag = 1;
  return pthread_join (speed_event_thread, NULL);
}

int
dpws_subscribe_to_torqueevents (char **id, struct soap *client,
                                struct ws4d_epr *service,
                                struct ws4d_delivery_type *delivery,
                                struct ws4d_filter_type *filter,
                                struct soap *torque_events,
                                const char *endpoint)
{

  ws4d_time duration = 3600;
  int err;

  char *ep_cpy = malloc (strlen (endpoint) + 1);
  memset (ep_cpy, 0, strlen (endpoint) + 1);
  strcpy (ep_cpy, endpoint);

  g_torque_events = torque_events;

  /* Subscribing to Airconditioner Service */
#ifdef DEBUG
  printf
    ("\nTachoEventListener: Subscribe to SpeedTorqueEvents@CarDataService ... ");
#endif
  *id =
    (char *) dpws_subscribe (client, service, (const char *) ep_cpy,
                             &duration, delivery, filter);
  if (!(*id))
    {
      soap_print_fault (client, stderr);
      return 1;
    }

#ifdef DEBUG
  printf ("OK (%s %s)\n", ws4d_subsproxy_getsubsman (service, *id), *id);
#endif

  /* set accept timeout to one second */
  torque_events->accept_timeout = 1;

#ifdef DEBUG
  printf
    ("\nTachoEventListener: ready to receive Speed Torque events ... \n");
#endif

  if (err =
      pthread_create (&torque_event_thread, NULL, torque_event_loop, NULL))
    {
      fprintf (stderr, "Could not launch thread to catch torque events!\n");
      return err;
    }

  return SOAP_OK;
}

int
dpws_unsubscribe_from_torque_events (void)
{
  torque_shutdown_flag = 1;
  return pthread_join (torque_event_thread, NULL);
}

int
dpws_subscribe_to_tankevents (char **id, struct soap *client,
                              struct ws4d_epr *service,
                              struct ws4d_delivery_type *delivery,
                              struct ws4d_filter_type *filter,
                              struct soap *tank_events, const char *endpoint)
{

  ws4d_time duration = 3600;
  int err;

  char *ep_cpy = malloc (strlen (endpoint) + 1);
  memset (ep_cpy, 0, strlen (endpoint) + 1);
  strcpy (ep_cpy, endpoint);

  g_tank_events = tank_events;

  /* Subscribing to Airconditioner Service */
#ifdef DEBUG
  printf
    ("\nTachoEventListener: Subscribe to TankEvents@CarDataService ... ");
#endif
  *id =
    (char *) dpws_subscribe (client, service, (const char *) ep_cpy,
                             &duration, delivery, filter);
  if (!(*id))
    {
      soap_print_fault (client, stderr);
      return 1;
    }

#ifdef DEBUG
  printf ("OK (%s %s)\n", ws4d_subsproxy_getsubsman (service, *id), *id);
#endif

  /* set accept timeout to one second */
  tank_events->accept_timeout = 1;

#ifdef DEBUG
  printf ("\nTankEventListener: ready to receive Tank events ... \n");
#endif

  if (err = pthread_create (&tank_event_thread, NULL, tank_event_loop, NULL))
    {
      fprintf (stderr, "Could not launch thread to catch Tank events!\n");
      return err;
    }

  return SOAP_OK;
}

int
dpws_unsubscribe_from_tank_events (void)
{
  tank_shutdown_flag = 1;
  return pthread_join (tank_event_thread, NULL);
}

int
__cdsinv1__SpeedEvent (struct soap *soap, float cdsinv1__Speed)
{

  if (soap_wsse_verify_body (soap))
    {
      fprintf (stderr, "susbscription: Signature does not include body\n");
      soap_wsse_delete_Security (soap);
      return soap_sender_fault (soap, "Service operation not signed", NULL);
#ifdef DEBUG
    }
  else
    {
      printf ("subscription: Signature includes body!\n");
#endif //DEBUG
    }

  setSpeedFromEvent ((int) cdsinv1__Speed);
  return SOAP_OK;
}

int
__cdsinv1__SpeedTorqueEvent (struct soap *soap, float cdsinv1__SpeedTorque)
{
  if (soap_wsse_verify_body (soap))
    {
      fprintf (stderr, "susbscription: Signature does not include body\n");
      soap_wsse_delete_Security (soap);
      return soap_sender_fault (soap, "Service operation not signed", NULL);
#ifdef DEBUG
    }
  else
    {
      printf ("subscription: Signature includes body!\n");
#endif //DEBUG
    }

  setTorqueFromEvent ((int) cdsinv1__SpeedTorque);
  return SOAP_OK;
}

int
__cdsinv1__TankWarnEvent (struct soap *soap, float cdsinv1__TankFill)
{

  if (soap_wsse_verify_body (soap))
    {
      fprintf (stderr, "susbscription: Signature does not include body\n");
      soap_wsse_delete_Security (soap);
      return soap_sender_fault (soap, "Service operation not signed", NULL);
#ifdef DEBUG
    }
  else
    {
      printf ("subscription: Signature includes body!\n");
#endif //DEBUG
    }

  raiseTankWarning ((float) (cdsinv1__TankFill / 10));
  return SOAP_OK;
}

int
__cdsinv1__NaviTargetEvent (struct soap *soap,
                            struct cdsinv1__CoordsType *cdsinv1__Coords)
{
  ;
}

int
__cdsinv1__NaviPositionEvent (struct soap *soap,
                              struct cdsinv1__CoordsType *cdsinv1__Coords)
{
  ;
}
