/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * dpws_cds.h
 *
 *  Created on: 11.04.2010
 *      Author: cht
 */

#ifndef DPWS_CDS_H_
#define DPWS_CDS_H_

#include "dpws_client.h"
#include "ws4d_eprllist.h"

#include "security_engine.h"


#ifdef __cplusplus
extern "C"
{
#endif

  int cds_register_wsse (struct soap *soap,
                         struct security_engine_s *sec_engine,
                         int activate_signature_verification);

  int dpws_get_tank_fill (struct soap *client, struct ws4d_epr *service,
                          struct security_engine_s *sec_engine,
                          struct ws4d_epr *device, float *fill);




#ifdef __cplusplus
}
#endif

#endif                          /* DPWS_CDS_H_ */
