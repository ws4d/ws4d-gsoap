/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * dpws_startup.c
 *
 *  Created on: 11.04.2010
 *      Author: cht
 */

#include "dpws_startup.h"
#include "config.h"

int
dpws_start (struct ws4d_abs_allocator *alist, struct soap *event_client,
            struct soap *speed_events, struct dpws_s *dpws, char *interface,
            char *deviceaddr, struct ws4d_epr *device,
            struct ws4d_epr **service, struct ws4d_abs_eprlist *services,
            struct ws4d_delivery_type **speed_delivery,
            struct ws4d_filter_type **speed_filter,
            struct soap *torque_events,
            struct ws4d_delivery_type **torque_delivery,
            struct ws4d_filter_type **torque_filter, struct soap *tank_events,
            struct ws4d_delivery_type **tank_delivery,
            struct ws4d_filter_type **tank_filter,
            struct soap *service_client, struct security_engine_s *sec_engine,
            struct soap *auth_client, pin_handler retrieve_pin_function)
{
  ws4d_qnamelist service_type_list;
  char *XAddrs = NULL;
  char speed_handler_uri[DPWS_URI_MAX_LEN + 1] = "http://host:0/";
  char torque_handler_uri[DPWS_URI_MAX_LEN + 1] = "http://host:0/";
  char tank_handler_uri[DPWS_URI_MAX_LEN + 1] = "http://host:0/";
  int err;

  WS4D_ALLOCLIST_INIT (alist);

  /* initialize client soap handle */
  soap_init (event_client);
#ifdef DEBUG
  soap_set_omode (event_client, SOAP_XML_INDENT);
#endif
  set_cdsinv_namespaces (event_client);

  soap_init (service_client);
#ifdef DEBUG
  soap_set_omode (service_client, SOAP_XML_INDENT);
#endif

  soap_init (auth_client);
#ifdef DEBUG
  soap_set_omode (auth_client, SOAP_XML_INDENT);
#endif

  soap_init (speed_events);
#ifdef DEBUG
  soap_set_omode (speed_events, SOAP_XML_INDENT);
#endif
  set_cdsinv_namespaces (speed_events);

  soap_init (torque_events);
#ifdef DEBUG
  soap_set_omode (torque_events, SOAP_XML_INDENT);
#endif
  set_cdsinv_namespaces (torque_events);

  soap_init (tank_events);
#ifdef DEBUG
  soap_set_omode (tank_events, SOAP_XML_INDENT);
#endif
  set_cdsinv_namespaces (tank_events);

  /******************************/
  /* set up security sec_engine */
  /******************************/
  security_engine_init (sec_engine);
  /* set ca-, cert- and key-file */
  sec_engine->cafile = SECSHELLPATH "/certs/demoCA/cacert.pem";
  sec_engine->certfile = SECSHELLPATH "/certs/myClientCert2.pem";
  sec_engine->keyfile = SECSHELLPATH "/certs/myClientKey2.pem";
  sec_engine->keypassword = "password";
  /* set cert_db-file */
  sec_engine->cert_list_location = "cds_client.cdb";
  /* register security sec_engine's components - for clients, the authorization
   * components are not needed */
  /* forward declarations are created by EXTERN_INIT_TABLE("ws4d") */
  ws4d_init_authentication_engine (sec_engine);
  ws4d_init_certificate_db (sec_engine);

  sec_engine->ae_retrieve_pin = retrieve_pin_function;

  /* register security sec_engine plugin to soap handle - besides, this way,
   * a security sec_engine is associated with a soap handle */
  soap_register_plugin_arg (service_client, sh_plugin, sec_engine);
  soap_register_plugin_arg (event_client, sh_plugin, sec_engine);
  soap_register_plugin_arg (speed_events, sh_plugin, sec_engine);
  soap_register_plugin_arg (torque_events, sh_plugin, sec_engine);
  soap_register_plugin_arg (tank_events, sh_plugin, sec_engine);

  /* btw: wsse-plugin for the authentication service is registered internally */

  /* SetUp and configure WSSE-Plugin for the service-handle, this must be done
   * AFTER registering the certificate database, since this registers the
   * callback to handle binary security tokens, passed as a parameter in
   * soap_register_plugin_arg()*/

  cds_register_wsse (service_client, sec_engine, 1);
  cdsinv_register_wsse (event_client, sec_engine, 1);
  cdsinv_register_wsse (speed_events, sec_engine, 1);
  cdsinv_register_wsse (torque_events, sec_engine, 1);
  cdsinv_register_wsse (tank_events, sec_engine, 1);

  /* make client for hosted service ready for dealing with signatures */
  /* CA-Cert file */
  service_client->cafile = sec_engine->cafile;
  event_client->cafile = sec_engine->cafile;
  tank_events->cafile = sec_engine->cafile;

  /* initialize WS4D-gSOAP */
  if (dpws_init (dpws, interface) != SOAP_OK)
    {
      fprintf (stderr,
               "\nTachoEventListener: Could not initialize dpws handle\n");
      fprintf (stderr, "something seems to be wrong with ip address\n");
      USAGE dpws_done (dpws);
      return 1;
    }

  /* bind listener handle to an address */
  dpws_handle_init (dpws, speed_events);
  if (dpws_handle_bind
      (dpws, speed_events, speed_handler_uri, DPWS_URI_MAX_LEN,
       100) == SOAP_INVALID_SOCKET)
    {
      fprintf (stderr,
               "\nTachoEventListener: error calling dpws_handle_bind");
      USAGE dpws_done (dpws);
      return 1;
    }

  dpws_handle_init (dpws, torque_events);
  if (dpws_handle_bind
      (dpws, torque_events, torque_handler_uri, DPWS_URI_MAX_LEN,
       100) == SOAP_INVALID_SOCKET)
    {
      fprintf (stderr,
               "\nTorqueEventListener: error calling dpws_handle_bind");
      USAGE dpws_done (dpws);
      return 1;
    }

  dpws_handle_init (dpws, tank_events);
  if (dpws_handle_bind
      (dpws, tank_events, tank_handler_uri, DPWS_URI_MAX_LEN,
       100) == SOAP_INVALID_SOCKET)
    {
      fprintf (stderr, "\nTankEventListener: error calling dpws_handle_bind");
      USAGE dpws_done (dpws);
      return 1;
    }

  /* allocate and prepare device to resolve */
  ws4d_epr_init (device);
  ws4d_epr_set_Addrs (device, deviceaddr);

  /* resolve address */
  XAddrs = (char *) dpws_resolve_addr (dpws, device, NULL, 10000);
  if (XAddrs != NULL)
    {
#ifdef DEBUG
      printf ("\nEventListeners: Device %s found at addr %s\n",
              ws4d_epr_get_Addrs (device), XAddrs);
#endif
    }
  else
    {
      fprintf (stderr,
               "\nEventListeners: Device %s cannot be found\n",
               ws4d_epr_get_Addrs (device));
      USAGE dpws_done (dpws);
      return 1;
    }

  if (err =
      auths_authenticate_device (auth_client, device, dpws, NULL, sec_engine))
    {
      printf ("simple_client: Error in Authenticating Device\n");
      return 1;
    }

  /* prepare service type list */
  ws4d_qnamelist_init (&service_type_list);
  ws4d_qnamelist_addstring ("\"http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService\":CardDataService", &service_type_list, alist);        /* Todo: fix typo everywhere (Card) */

  /* look up service with matching service types on device */
  ws4d_eprlist_init (services, ws4d_eprllist_init, NULL);
  if (dpws_find_services (dpws, device, &service_type_list,
                          10000, services) == SOAP_OK)
    {
      /* use first service */
      *service = ws4d_eprlist_get_first (services);
#ifdef DEBUG
      printf
        ("\nTachoEventListener: Device offers CarDataService at %s\n",
         ws4d_epr_get_Addrs (*service));
#endif
    }
  else
    {
      fprintf (stderr,
               "\nTachoEventListener: CarDataService not found on %s\n",
               ws4d_epr_get_Addrs (device));
      exit (1);
    }

  /* prepare filter and delivery mode */
  *speed_delivery =
    dpws_gen_delivery_push (event_client,
                            dpws_handle_get_paddr (speed_events));

  *speed_filter =
    dpws_gen_filter_action (event_client,
                            "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/SpeedEventOut");

  *torque_delivery =
    dpws_gen_delivery_push (event_client,
                            dpws_handle_get_paddr (torque_events));

  *torque_filter =
    dpws_gen_filter_action (event_client,
                            "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/SpeedTorqueEventOut");

  *tank_delivery =
    dpws_gen_delivery_push (event_client,
                            dpws_handle_get_paddr (tank_events));

  *tank_filter =
    dpws_gen_filter_action (event_client,
                            "http://www.bmw-carit.de/dpws-eval/usecase1/CarDataService/TankWarnEventOut");


  return SOAP_OK;
}
