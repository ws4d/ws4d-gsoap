/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * dpws_cds_events.h
 *
 *  Created on: 11.04.2010
 *      Author: cht
 */

#ifndef DPWS_CDS_EVENTS_H_
#define DPWS_CDS_EVENTS_H_

#include "dpws_client.h"
#include "ws4d_eprllist.h"

#include "security_engine.h"


#ifdef __cplusplus
extern "C"
{
#endif


  /* Forward declarations - get resolved in cardataserviceclient.cpp */
  void setSpeedFromEvent (int speed);
  void setTorqueFromEvent (int torque);
  void raiseTankWarning (float level);

  int cdsinv_register_wsse (struct soap *soap,
                            struct security_engine_s *sec_engine,
                            int activate_signature_verification);

  void set_cdsinv_namespaces (struct soap *handle);

  int dpws_subscribe_to_speedevents (char **id, struct soap *client,
                                     struct ws4d_epr *service,
                                     struct ws4d_delivery_type *delivery,
                                     struct ws4d_filter_type *filter,
                                     struct soap *speed_events,
                                     const char *endpoint);

  int dpws_unsubscribe_from_speed_events (void);

  int dpws_subscribe_to_torqueevents (char **id, struct soap *client,
                                      struct ws4d_epr *service,
                                      struct ws4d_delivery_type *delivery,
                                      struct ws4d_filter_type *filter,
                                      struct soap *torque_events,
                                      const char *endpoint);

  int dpws_unsubscribe_from_torque_events (void);

  int dpws_subscribe_to_tankevents (char **id, struct soap *client,
                                    struct ws4d_epr *service,
                                    struct ws4d_delivery_type *delivery,
                                    struct ws4d_filter_type *filter,
                                    struct soap *tank_events,
                                    const char *endpoint);

  int dpws_unsubscribe_from_tank_events (void);


#ifdef __cplusplus
}
#endif

#endif                          /* DPWS_CDS_EVENTS_H_ */
