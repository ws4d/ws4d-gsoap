/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#ifndef CARDATASERVICECLIENT_H
#define CARDATASERVICECLIENT_H

#include <QtGui/QWidget>
#include "dpws_startup.h"

namespace Ui
{
  class CarDataServiceClient;
}

class CarDataServiceClient:public QWidget
{
Q_OBJECT public:
  CarDataServiceClient (QWidget * parent = 0);
  ~CarDataServiceClient ();

  void emitSpeedSignal (int speed);
  void emitTorqueSignal (int torque);
  void emitTankSignal (float level);
  ws4dPin ask_for_pin (struct security_engine_s *sec_engine, ws4dPin pin);

  public slots:void startDPWS (void);
  void closeApplication (void);
  void subscribeSpeed (void);
  void unsubscribeSpeed (void);
  void subscribeTorque (void);
  void unsubscribeTorque (void);
  void subscribeTank (void);
  void unsubscribeTank (void);

  void getTankFill (void);

  void showTankWarning (float level);

    signals:void speedEvent (int speed);
  void torqueEvent (int torque);
  void tankWarnEvent (float level);

private:
    Ui::CarDataServiceClient * m_ui;
  char *interface, *urn, *speedID, *torqueID, *tankID;
  struct dpws_s *dpws;
  struct ws4d_epr *device, *service;
  struct ws4d_abs_allocator alist;
  struct ws4d_abs_eprlist *services;
  struct security_engine_s *sec_engine;
  struct soap *client, *service_client;
  struct soap *auth_client;
  struct soap *speed_events, *torque_events, *tank_events;
  struct ws4d_delivery_type *speed_delivery, *torque_delivery, *tank_delivery;
  struct ws4d_filter_type *speed_filter, *torque_filter, *tank_filter;
};

#endif // CARDATASERVICECLIENT_H
