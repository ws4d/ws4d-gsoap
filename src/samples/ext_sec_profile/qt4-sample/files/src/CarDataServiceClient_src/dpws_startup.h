/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * dpws_startup.h
 *
 *  Created on: 11.04.2010
 *      Author: cht
 */

#ifndef DPWS_STARTUP_H_
#define DPWS_STARTUP_H_

#include "dpws_cds.h"
#include "dpws_cds_events.h"
#include "security_engine.h"

#include "dpws_client.h"
#include "ws4d_eprllist.h"

#include "config.h"

#ifdef __cplusplus
extern "C"
{
#endif

/* type for callback-wrapper-function */
  typedef ws4dPin (*pin_handler) (struct security_engine_s *, ws4dPin);

  int dpws_start (struct ws4d_abs_allocator *alist, struct soap *client,
                  struct soap *speed_events, struct dpws_s *dpws,
                  char *interface, char *deviceaddr, struct ws4d_epr *device,
                  struct ws4d_epr **service,
                  struct ws4d_abs_eprlist *services,
                  struct ws4d_delivery_type **speed_delivery,
                  struct ws4d_filter_type **speed_filter,
                  struct soap *torque_events,
                  struct ws4d_delivery_type **torque_delivery,
                  struct ws4d_filter_type **torque_filter,
                  struct soap *tank_events,
                  struct ws4d_delivery_type **tank_delivery,
                  struct ws4d_filter_type **tank_filter,
                  struct soap *service_client,
                  struct security_engine_s *sec_engine,
                  struct soap *auth_client,
                  pin_handler retrieve_pin_function);

#define USAGE printf("\nusage:\n"); \
  printf("\t%s -i client's IP -d device's UUID\n", "client"); \
  printf("\tUUID in form of urn:uuid:[uuid]\n"); \
  printf("\tYou might want to use the tool uuidgen to obtain a uuid\n"); \
  printf("Exmaple:\n\t%s -i 192.168.1.1 -d urn:uuid:`uuidgen`\n\n", "client");


#ifdef __cplusplus
}
#endif

#endif                          /* DPWS_STARTUP_H_ */
