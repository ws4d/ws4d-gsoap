/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * dpws_related.c
 *
 *  Created on: 07.04.2010
 *      Author: cht
 */

#include "dpws_related.h"
#include "rks.nsmap"
#include "smdevp.h"
#include "wsseapi.h"
#include "config.h"
EXTERN_INIT_TABLE ("ws4d")
     int setup_dpws_stuff (struct dpws_s *dpws, struct soap *client,
                           const char *interface, const char *deviceaddr,
                           struct ws4d_epr *device, struct ws4d_epr **service,
                           struct ws4d_abs_allocator *alist,
                           struct ws4d_abs_eprlist *services,
                           struct security_engine_s *sec_engine,
                           pin_handler retrieve_pin_function)
{

  char *XAddrs = NULL;
  int err;
  ws4d_qnamelist service_type_list;
  struct soap auth_client;
  // Initialize allocation list
  WS4D_ALLOCLIST_INIT (alist);

  // Initialize soap and dpws handle
  soap_init (client);
  soap_init (&auth_client);
#ifdef DEBUG
  soap_set_omode (client, SOAP_XML_INDENT);
  soap_set_omode (&auth_client, SOAP_XML_INDENT);
#endif
  soap_set_omode (client, SOAP_XML_CANONICAL);

  /******************************/
  /* set up security sec_engine */
  /******************************/
  security_engine_init (sec_engine);
  /* set ca-, cert- and key-file */
  sec_engine->cafile = SECSHELLPATH "/certs/demoCA/cacert.pem";
  sec_engine->certfile = SECSHELLPATH "/certs/myClientCert.pem";
  sec_engine->keyfile = SECSHELLPATH "/certs/myClientKey.pem";
  sec_engine->keypassword = "password";
  /* set cert_db-file */
  sec_engine->cert_list_location = "client.cdb";
  /* register security sec_engine's components - for clients, the authorization
   * components are not needed */
  /* forward declarations are created by EXTERN_INIT_TABLE("ws4d") */
  ws4d_init_authentication_engine (sec_engine);
  ws4d_init_certificate_db (sec_engine);

  sec_engine->ae_retrieve_pin = retrieve_pin_function;

  /* register security sec_engine plugin to soap handle - besides, this way,
   * a security sec_engine is associated with a soap handle */
  soap_register_plugin_arg (client, sh_plugin, sec_engine);

  /* btw: wsse-plugin for the authentication service is registered internally */

  /* SetUp and configure WSSE-Plugin for the service-handle, this must be done
   * AFTER registering the certificate database, since this registers the
   * callback to handle binary security tokens, passed as a parameter in
   * soap_register_plugin_arg()*/
  soap_register_plugin_arg (client, soap_wsse,
                            sec_engine->cdb_wsse_token_handler);

  /* make client for hosted service ready for dealing with signatures */
  /* CA-Cert file */
  client->cafile = sec_engine->cafile;
  soap_wsse_verify_init (client);

  /* check signature */
  if (soap_wsse_verify_auto (client, SOAP_SMD_NONE, NULL, 0))
    {
      fprintf (stderr, "Could not activate automatic signature validation\n");
      exit (1);
    }

  if ((err = dpws_init (dpws, interface)) != SOAP_OK)
    {
      printf ("dpws_init() failed with code %d\n", err);
      printf ("Maybe sth. wrong with interface address: <<%s>>\n", interface);
      dpws_done (dpws);
      return 2;
    }

  // Now, we initialize an epr structure and specify the logical address (uuin)

  ws4d_epr_init (device);
  ws4d_epr_set_Addrs (device, deviceaddr);


  // ... so we can use this structure to resolve the logical address into a physical one
  if ((XAddrs =
       (char *) dpws_resolve_addr (dpws, device, NULL, 10000)) == NULL)
    {
      printf ("Did not find Device %s\n", ws4d_epr_get_Addrs (device));
      dpws_done (dpws);
      return 3;
    }

  if (err =
      auths_authenticate_device (&auth_client, device, dpws, NULL,
                                 sec_engine))
    {
      printf ("simple_client: Error in Authenticating Device\n");
      return 1;
    }

  // Now, it's time to set up the Service Name List for filtering
  ws4d_qnamelist_init (&service_type_list);
  ws4d_qnamelist_addstring
    ("\"http://www.bmw-carit.de/dpws-eval/usecase1/RemoteKeyboard\":RemoteKeyboard",
     // [xmlnsmhus]:[Metadata/MetadataSection/Relationship/Hosted/Types]
     // see metadata.xml
     &service_type_list, alist);

  // Now, it's getting interesting. We use the device's epr and the Service Types
  // to find all the matching services on the device (most likely, this will only
  // be a single one, but you never know)
  ws4d_eprlist_init (services, ws4d_eprllist_init, NULL);

  if (dpws_find_services (dpws, device, &service_type_list, 10000, services)
      == SOAP_OK)
    {
      *service = ws4d_eprlist_get_first (services);
#ifdef DEBUG
      printf ("Device offers Service at %s\n", ws4d_epr_get_Addrs (*service));
#endif
    }
  else
    {
      printf ("Could not find matching service at %s\n",
              ws4d_epr_get_Addrs (device));
      dpws_done (dpws);
      return 4;
    }

#ifdef DEBUG
  printf ("Retrieving WSDL from service @ %s\n",
          ws4d_epr_get_Addrs (*service));
#endif
  return 0;
}

int
closedown_dpws_stuff (struct soap *client, struct dpws_s *dpws,
                      struct ws4d_epr *device,
                      struct ws4d_abs_allocator *alist,
                      struct ws4d_abs_eprlist *services)
{
  // Clean up...
  soap_done (client);
  dpws_done (dpws);

  ws4d_eprlist_done (services);
  ws4d_epr_done (device);
  ws4d_alloclist_done (alist);
  return 0;
}

int
dpws_send_character (struct soap *client, struct ws4d_epr *service, char chr,
                     struct security_engine_s *sec_engine,
                     struct ws4d_epr *device)
{
  int ret, err;
  struct rks1__RKKeyType RKsendKey;
  enum xsd__boolean returnValue;
  static EVP_PKEY *rsa_private_key = (EVP_PKEY *) NULL;
  FILE *fd;

  RKsendKey.Character.__ptr = (unsigned char *) malloc (sizeof (char));
  *(RKsendKey.Character.__ptr) = (unsigned char) chr;
  RKsendKey.Character.__size = 1;

  RKsendKey.Modifier.__ptr = (unsigned char *) malloc (sizeof (char));
  *(RKsendKey.Modifier.__ptr) = (unsigned char) '0';
  RKsendKey.Modifier.__size = 0;

  soap_set_namespaces (client, rks_namespaces); // see mhus1.nsmap
  dpws_header_gen_request (client, NULL, ws4d_epr_get_Addrs (service), "http://www.bmw-carit.de/dpws-eval/usecase1/RemoteKeyboard/sendKeyIn", NULL,     // wsa:action, btw...
                           NULL, sizeof (struct SOAP_ENV__Header));

  if (rsa_private_key == NULL)
    {
      /* reading in private key */
      if ((fd = fopen (sec_engine->keyfile, "r")) == 0)
        {
          fprintf (stderr, "ERROR: pem file not found\n");
          return 0;
        }
      rsa_private_key =
        PEM_read_PrivateKey (fd, NULL, NULL,
                             (void *) sec_engine->keypassword);
      fclose (fd);
      if (!rsa_private_key)
        {
          fprintf (stderr, "ERROR: reading private key failed\n");
          return 0;
        }
    }

  /* reference the cert for signing */
  if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509 (client,
                                                        sec_engine->cdb_lookup_cert_reference
                                                        (sec_engine,
                                                         device->Address)))
    {
      fprintf (stderr, "ERROR: add reference failed\n");
      exit (1);
    }
  /* sign the msg  */
  if (soap_wsse_sign_body
      (client, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
    {
      fprintf (stderr, "sign body failed\n");
      exit (1);
    }

  // Let's eventually invoke the service
  if ((err =
       soap_call___rks1__sendKey (client, ws4d_epr_get_Addrs (service), NULL,
                                  &RKsendKey, &returnValue)) == SOAP_OK)
    {
      if (returnValue == xsd__boolean__true_)
        {
          printf ("Received true\n");
          ret = 0;
        }
      else
        {
          printf ("Received false\n");
          ret = 1;
        }
    }
  else
    {
      printf ("Calling sendKey failed: %d\n", err);
      return err;
    }
  soap_end (client);
  return ret;
}
