/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QtGui/QWidget>

#include "dpws_client.h"
#include "ws4d_eprllist.h"
#include "ws4d_targetcache.h"
#include "ws4d_servicecache.h"
/* security related */

#include "security_engine.h"
#include "authentication_service_usage.h"

namespace Ui
{
  class mainWidget;
}

class mainWidget:public QWidget
{
Q_OBJECT public:
  mainWidget (QWidget * parent = 0);
  ~mainWidget ();
  int closeDPWS (void);
  ws4dPin ask_for_pin (struct security_engine_s *sec_engine, ws4dPin pin);

    signals:void sendCharacter (char character);

  public slots:void setCharacter (char caracter);
  void react (void);
  void closeApplication (void);
  int setupDPWS (void);

private:
  const char *interface, *urn;
    Ui::mainWidget * ui;
  struct dpws_s *dpws;
  struct ws4d_epr *device, *service;
  unsigned int old_size;
  struct ws4d_abs_allocator alist;
  struct ws4d_abs_eprlist *services;
  struct security_engine_s *sec_engine;
  struct soap *client;
};

#endif // MAINWIDGET_H
