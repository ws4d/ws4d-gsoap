/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * dpws_related.h
 *
 *  Created on: 07.04.2010
 *      Author: cht
 */

#ifndef DPWS_RELATED_H_
#define DPWS_RELATED_H_

#include "dpws_client.h"
#include "ws4d_eprllist.h"
#include "ws4d_targetcache.h"
#include "ws4d_servicecache.h"
/* security related */

#include "security_engine.h"
#include "authentication_service_usage.h"

#ifdef __cplusplus
extern "C"
{
#endif

/* type for callback-wrapper-function */
  typedef ws4dPin (*pin_handler) (struct security_engine_s *, ws4dPin);

  int setup_dpws_stuff (struct dpws_s *dpws, struct soap *client,
                        const char *interface, const char *deviceaddr,
                        struct ws4d_epr *device, struct ws4d_epr **service,
                        struct ws4d_abs_allocator *alist,
                        struct ws4d_abs_eprlist *services,
                        struct security_engine_s *sec_engine,
                        pin_handler retrieve_pin_function);

  int closedown_dpws_stuff (struct soap *client, struct dpws_s *dpws,
                            struct ws4d_epr *device,
                            struct ws4d_abs_allocator *alist,
                            struct ws4d_abs_eprlist *services);

  int dpws_send_character (struct soap *client, struct ws4d_epr *service,
                           char chr, struct security_engine_s *sec_engine,
                           struct ws4d_epr *device);


#ifdef __cplusplus
}
#endif

#endif                          /* DPWS_RELATED_H_ */
