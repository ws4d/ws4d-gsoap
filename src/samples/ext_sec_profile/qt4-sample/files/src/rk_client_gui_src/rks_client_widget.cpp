/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "rks_client_widget.h"
#include "ui_rks_client_widget.h"
#include <stdlib.h>
#include <QInputDialog>
#include "dpws_related.h"
#include <stdio.h>

/* call back wrapper */
mainWidget::mainWidget * global_widget;

ws4dPin
gui_pin_handler (struct security_engine_s *s, ws4dPin pin)
{
  return global_widget->ask_for_pin (s, NULL);
}

mainWidget::mainWidget (QWidget * parent):QWidget (parent),
ui (new Ui::mainWidget)
{
  ui->setupUi (this);
  ui->plainTextEdit->setDisabled (true);
  connect (ui->pushButton, SIGNAL (clicked ()), this,
           SLOT (closeApplication ()));
  connect (ui->plainTextEdit, SIGNAL (textChanged ()), this, SLOT (react ()));
  connect (this, SIGNAL (sendCharacter (char)), this,
           SLOT (setCharacter (char)));

  connect (ui->connectButton, SIGNAL (clicked ()), this, SLOT (setupDPWS ()));

  ui->ipEdit->setText (QString ((const char *) getenv ("theIP")).trimmed ());
  ui->urnEdit->
    setText (QString ((const char *) getenv ("theDEVICE")).trimmed ());

  this->client = NULL;
  this->dpws = NULL;
  this->sec_engine = NULL;
  this->services = NULL;

  this->old_size = 0;

  global_widget = this;
}

mainWidget::~mainWidget ()
{
  delete ui;
}

int
mainWidget::setupDPWS (void)
{
  if ((ui->ipEdit->text ().length () > 8)
      && (ui->urnEdit->text ().length () > 20))
    {
      this->device = (struct ws4d_epr *) malloc (sizeof (struct ws4d_epr));
      int err;

      this->interface =
        (const char *) malloc (ui->ipEdit->text ().length () + 1);
      this->urn = (const char *) malloc (ui->urnEdit->text ().length () + 1);

      memset ((void *) this->interface, 0, ui->ipEdit->text ().length () + 1);
      memset ((void *) this->urn, 0, ui->urnEdit->text ().length () + 1);

      strcpy ((char *) this->interface,
              (char *) ui->ipEdit->text ().toAscii ().data ());
      strcpy ((char *) this->urn,
              (char *) ui->urnEdit->text ().toAscii ().data ());

      this->client = (struct soap *) malloc (sizeof (struct soap));
      this->dpws = (struct dpws_s *) malloc (sizeof (struct dpws_s));
      this->sec_engine = (struct security_engine_s *)
        malloc (sizeof (struct security_engine_s));
      this->services = (struct ws4d_abs_eprlist *)
        malloc (sizeof (struct ws4d_abs_eprlist));

      this->old_size = 0;

      err = setup_dpws_stuff (this->dpws, this->client,
                              this->interface, this->urn,
                              this->device, &(this->service),
                              &(this->alist), this->services,
                              this->sec_engine, gui_pin_handler);

      if (err == 0)
        {
          ui->stateEdit->setText (QString ("OK"));
          ui->connectButton->setDisabled (true);
          ui->plainTextEdit->setDisabled (false);
        }
      else
        {
          if (this->device)
            free (this->device);
          if (this->client)
            free (this->client);
          if (this->dpws)
            free (this->dpws);
          if (this->sec_engine)
            free (this->sec_engine);
          if (this->service)
            free (this->services);

          this->device = NULL;
          this->client = NULL;
          this->dpws = NULL;
          this->sec_engine = NULL;
          this->services = NULL;
        }
    }
}

int
mainWidget::closeDPWS (void)
{
  if (this->device)
    closedown_dpws_stuff (this->client, this->dpws, this->device,
                          &(this->alist), this->services);
  if (this->device)
    free (this->device);
  if (this->client)
    free (this->client);
  if (this->dpws)
    free (this->dpws);
  if (this->sec_engine)
    free (this->sec_engine);
  if (this->service)
    free (this->services);

  this->device = NULL;
  this->client = NULL;
  this->dpws = NULL;
  this->sec_engine = NULL;
  this->services = NULL;
}

void
mainWidget::react (void)
{
  char chr;
  if ((ui->plainTextEdit->toPlainText ().length () > this->old_size)
      || (old_size == 0)
      || ((ui->plainTextEdit->toPlainText ().length () == 0))
      && (this->old_size != 1))
    {
      /* so, a new character was appended */
      QString text = ui->plainTextEdit->toPlainText ().right (1);
      chr = text[0].toAscii ();
    }
  else if (this->old_size > 0)
    {
      /* a character is missing now (most likely, because somebody hit backspace) */
      chr = '\b';
    }

  if (ui->plainTextEdit->toPlainText ().length () != this->old_size)
    // I'm really sorry for this but I just don't get it... for some reason,
    // whenever backspace is hit, react() is invoked twice
    emit sendCharacter (chr);

  this->old_size = ui->plainTextEdit->toPlainText ().length ();
  if (chr == '\n')
    ui->plainTextEdit->clear ();
}

void
mainWidget::setCharacter (char character)
{
  if (character)
    {
      ui->lineEdit->setText (QString (QChar (character)));
      ui->spinBox->setValue ((int) character);
      if (dpws_send_character
          (this->client, this->service, character, this->sec_engine,
           this->device))
        {
          ui->plainTextEdit->undo ();
        }
    }
}

void
mainWidget::closeApplication (void)
{
  this->closeDPWS ();
  qApp->quit ();
}

ws4dPin
  mainWidget::ask_for_pin (struct security_engine_s *sec_engine, ws4dPin pin)
{
  bool ok;
  if (pin == NULL)
    {
      pin = (ws4dPin) malloc (PIN_SIZE);
    }
  QString pinQString =
    QInputDialog::getText (this, "Please enter PIN", "Please enter PIN",
                           QLineEdit::Normal, "pin", &ok);
  for (int i = 0; i < PIN_SIZE; i++)
    pin[i] = pinQString.toAscii ().data ()[i];
  return pin;
}

#ifdef __cplusplus
/* to come by that *** pseudo-C++ by gSoap */
soap::soap ()
{

}

soap::~soap ()
{

}
#endif
