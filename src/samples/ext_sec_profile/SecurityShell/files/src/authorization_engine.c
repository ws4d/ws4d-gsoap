/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * authorization_engine.c
 *
 *  Created on: 24.03.2010
 *      Author: cht
 */

#include "authorization_engine.h"
#include "dpwsStub.h"           /* needed for soap-header-types */

struct security_engine_s *
ws4d_init_authorization_engine (struct security_engine_s *s)
{
  s->authe_set_dynamic_authorization = ws4d_authe_set_dynamic_authorization;
  s->authe_authorize_request = ws4d_authe_authorize_request;
  s->authe_set_authorization_mode = ws4d_authe_set_authorization_mode;
  s->authe_activate_authorization = ws4d_authe_activate_authorization;
  s->authe_deactivate_authorization = ws4d_authe_deactivate_authorization;
  s->authe_request_dynamic_authorization_oob =
    ws4d_authe_request_dynamic_authorization_oob;
  s->authe_get_credential = ws4d_authe_get_credential;
  return s;
}

struct security_engine_s *
ws4d_del_authorization_engine (struct security_engine_s *s)
{
  s->authe_set_dynamic_authorization = NULL;
  s->authe_authorize_request = NULL;
  s->authe_set_authorization_mode = NULL;
  s->authe_activate_authorization = NULL;
  s->authe_deactivate_authorization = NULL;
  s->authe_request_dynamic_authorization_oob = NULL;
  s->authe_get_credential = NULL;
  return s;
}

int
ws4d_authe_set_dynamic_authorization (struct security_engine_s *s,
                                      void *ressource,
                                      permission_t permission,
                                      ressource_type_t res_type)
{
  struct authorization_s *auth = NULL;
  auth =
    s->authdb_lookup_authorization (s, DYNAMIC_AUTHORIZATION, ressource,
                                    CredDYNAMIC);
#ifdef DEBUG
  if (auth)
    fprintf (stderr, "Found existing dynamic auth-entry for %s\n",
             (char *) ressource);
  else
    fprintf (stderr, "Adding new dynamic auth-entry permission for %s\n",
             (char *) ressource);
#endif //DEBUG
  if (auth)                     // existing one - just update
    auth->permission = permission;
  else
    {
      auth =
        (struct authorization_s *) malloc (sizeof (struct authorization_s));

      auth->credential = (void *) malloc (DYNAUTSZ + 1);
      memset (auth->credential, 0, DYNAUTSZ + 1);
      strncpy ((char *) auth->credential, DYNAMIC_AUTHORIZATION, DYNAUTSZ);

      auth->ressource = (void *) malloc (strlen ((char *) ressource) + 1);
      memset (auth->ressource, 0, strlen ((char *) ressource) + 1);
      strcpy ((char *) auth->ressource, ressource);

      auth->credential_type = CredDYNAMIC;
      auth->permission = permission;
      auth->validity = 0;
      auth->cred_match_handler = ws4d_dynamic_authorization_handler;
      auth->ressource_type = res_type;

      s->authdb_save_authorization (s, auth);
    }
  return ERR_OK;
}

permission_t
ws4d_authe_authorize_request (struct security_engine_s * s,
                              struct soap * soap, unsigned int credentials,
                              ressource_type_t res_type)
{
  /* Here's the deal:
   * 1st: check if there is already a matching authorization inside authorization_db
   * 2nd: check if dynamic authorization is allowed
   * 3rd: if (2nd) { ask for dynamic authorization, save decision for later requests}
   */
  struct authorization_s *pos = NULL, *next = NULL;
  permission_t result = UNKNOWN;
  int dynamic_allowed = 0;
  int count = 0;
  void *credential = NULL;
  void *ressource = NULL;

  /* check out ressource for later use */
  ressource = ws4d_authe_get_ressource (soap, res_type, 0);
#ifdef DEBUG
  fprintf (stderr, "questionable ressource: %s\n", (char *) ressource);
#endif

  /* check for existing authorizations depending on current credentials */
  /* besides, check if dynamic authorization is allowed */
  ws4d_list_foreach (pos, next, &(s->auth_list_head), struct authorization_s,
                     list)
  {
    count++;
    if ((pos->credential_type & credentials)
        || (pos->credential_type == CredDYNAMIC))
      {
        // credential is acceptable
#ifdef DEBUG
        fprintf (stderr,
                 "Regarding following entry:\n\tcredential type: %d\n",
                 pos->credential_type);
#endif //DEBUG
      }
    else
      continue;

#ifdef DEBUG
    fprintf (stderr, "Ressource type %d", (int) pos->ressource_type);
#endif // DEBUG
    /* ressource type okay? */
    if (!(pos->ressource_type == res_type))
      {
#ifdef DEBUG
        fprintf (stderr, " not acceptable - skipping\n");
#endif // DEBUG
        continue;
#ifdef DEBUG
      }
    else
      {
        fprintf (stderr, " acceptable. Proceeding...\n");
#endif //DEBUG
      }

#ifdef DEBUG
    fprintf (stderr, "\tpos->ressource: <%s>\n", (char *) pos->ressource);
#endif // DEBUG
    /* is it the questionable ressource..? */
    if (ws4d_authe_compare_ressources
        (pos->ressource, ressource, pos->ressource_type))
      {
#ifdef DEBUG
        fprintf (stderr, "\tnot the one I'm looking for! Skipping\n");
#endif // DEBUG
        continue;
#ifdef DEBUG
      }
    else
      {
        fprintf (stderr, "\tis the one I'm looking for.\n");
#endif //DEBUG
      }


    /* does the credential match? */
    credential = ws4d_authe_get_credential (soap, pos->credential_type);
    if (!credential)
      continue;
#ifdef DEBUG
    fprintf (stderr, "\tFound Credential (%d)\n", pos->credential_type);
#endif //DEBUG
    if (!pos->cred_match_handler (pos, credential))
      {
        permission_t tmp_result;
#ifdef DEBUG
        fprintf (stderr, "\tMATCHES!\n");
#endif // DEBUG
        /* We have to check whether this is a dynamic-auth-credential or a real one */
        tmp_result = ws4d_authe_get_permission (s, pos);
        if ((pos->credential_type == CredDYNAMIC)
            && (tmp_result == PERMITTED))
          dynamic_allowed = 1;
        else
          result = tmp_result;
      }
  }

  fprintf (stderr, "Counted %d items in list!\n", count);

  /* right now, several things could happen:
   * - result could be PERMITTED or DENIED -> return this result
   * - result could be UNKNOWN or PERMITTED_TEMPORARY -> dynamic authorization
   * - something else -> I really messed something up...
   */
  if ((result == PERMITTED) || (result == DENIED))
    return result;
  else if (((result == UNKNOWN) || (result == PERMITTED_TEMPORARY))
           && (dynamic_allowed))
    {
      /* ask for dynamic permission */
      result =
        ws4d_authe_request_dynamic_authorization (s, soap, ressource,
                                                  res_type);
    }
  else
    {
      fprintf (stderr, "Error: Unknown permission state: %d\n", result);
    }
  return result;
}

/* TODO: in near future, this should be more dynamic... */
/* consider something like match_handler for credentials */
int
ws4d_authe_compare_ressources (void *r1, void *r2, ressource_type_t type)
{
  switch (type)
    {
    case SERVICE:
      return strcmp ((char *) r1, (char *) r2);
      break;
    case METHOD:
      return strcmp ((char *) r1, (char *) r2);
      break;
    default:
      break;
    }
  return ERR_HANDLER;
}

void *
ws4d_authe_get_ressource (struct soap *soap, ressource_type_t type, int cp)
{
  /* parameter cp says wheter data is copied or just referenced */
  void *ressource = NULL;
  switch (type)
    {
    case SERVICE:
      if (cp)
        {
          ressource = (void *) malloc (strlen (soap->header->wsa__To) + 1);
          memset (ressource, 0, strlen (soap->header->wsa__To) + 1);
          strcpy ((char *) ressource, soap->header->wsa__To);
        }
      else
        ressource = (void *) soap->header->wsa__To;
      break;
    case METHOD:
      /* um... is XML case-sensitive...? */
      if (strcmp
          (soap->header->wsa__Action,
           "http://schemas.xmlsoap.org/ws/2004/08/eventing/Subscribe"))
        {
          /* ordinary method call */
          if (cp)
            {
              ressource =
                (void *) malloc (strlen (soap->header->wsa__Action) + 1);
              memset (ressource, 0, strlen (soap->header->wsa__Action) + 1);
              strcpy ((char *) ressource, soap->header->wsa__Action);
            }
          else
            ressource = (void *) soap->header->wsa__Action;
        }
      else
        {
          /* subscription */
          /* since we have DOM here, we use it to find it */
          struct soap_dom_element *iter;
          for (iter = soap->dom; iter; iter = soap_dom_next_element (iter))
            {
              if (!strcmp (iter->name, "wse:Filter"))
                {
                  break;
                }
            }
          if (!iter)
            {
              fprintf (stderr, "No wse:Filter - Element Found!\n");
            }
          else
            {
              ressource = (void *) malloc (strlen (iter->data) + 56 + 2);
              memset (ressource, 0, strlen (iter->data) + 56 + 2);
              sprintf ((char *) ressource, "%s;%s", soap->header->wsa__Action,
                       iter->data);
#ifdef DEBUG
              fprintf (stderr, "Found ressource: <%s>\n", (char *) ressource);
#endif
            }
        }
      break;
    default:
      ressource = NULL;
      break;
    }
  return ressource;
}

/* TODO: In near future this should be more dynamic */
void *
ws4d_authe_get_credential (struct soap *soap, credential_type_t type)
{
  void *credential = NULL;
  switch (type)
    {
    case CredDYNAMIC:
      credential = DYNAMIC_AUTHORIZATION;
      break;
    case CredX509Cert:
      if (!soap->header->wsse__Security->BinarySecurityToken->__item)
        break;
      credential =
        (void *)
        malloc (strlen
                (soap->header->wsse__Security->BinarySecurityToken->__item) +
                1);
      memset (credential, 0,
              strlen (soap->header->wsse__Security->BinarySecurityToken->
                      __item) + 1);
      strcpy ((char *) credential,
              soap->header->wsse__Security->BinarySecurityToken->__item);
      break;
    case CredX509CertRef:
      if (!soap->header->wsse__Security->ds__Signature->KeyInfo->
          wsse__SecurityTokenReference->Reference->URI)
        break;
      credential =
        (void *)
        malloc (strlen
                (soap->header->wsse__Security->ds__Signature->KeyInfo->
                 wsse__SecurityTokenReference->Reference->URI) + 1);
      memset (credential, 0,
              strlen (soap->header->wsse__Security->ds__Signature->KeyInfo->
                      wsse__SecurityTokenReference->Reference->URI) + 1);
      strcpy ((char *) credential,
              (char *) soap->header->wsse__Security->ds__Signature->KeyInfo->
              wsse__SecurityTokenReference->Reference->URI);
      break;
    default:
      fprintf (stderr,
               "Sorry, credentials of type %d are not supported, yet\n",
               type);
      break;
    }
  return credential;
}

permission_t
ws4d_authe_get_permission (struct security_engine_s * s,
                           struct authorization_s * auth)
{
  time_t now;
  permission_t result;

  now = time (&now);

  switch (auth->permission)
    {
    case DENIED:
      /* it depends on the timestamp whether we deny immediately or give the
       * chance for another dynamic authorization
       */
      if ((auth->validity == 0) || (auth->validity >= now))
        {
          result = DENIED;      /* really absolutely denied */
        }
      else
        {
          s->authdb_delete_authorization (s, auth);
          result = UNKNOWN;
        }
      break;

    case PERMITTED_ONCE:
      /* I don't think, that will ever happen... */
      auth->permission = DENIED;
      result = PERMITTED;
      break;

    case PERMITTED_TEMPORARY:
#ifdef DEBUG
      fprintf (stderr, "auth->validity: %d, now: %d\n", (int) auth->validity,
               (int) now);
#endif //DEBUG
      if (auth->validity >= now)
        result = PERMITTED;
      else
        {
          /* if there has been a former temporary authorization which has expired,
           * then delete it and return an unknown permission */
          s->authdb_delete_authorization (s, auth);
          result = UNKNOWN;     /* so there still is the dynamic chance */
        }
      break;

    case PERMITTED:            /* easiest case... */
      result = PERMITTED;
      break;

    default:
      /* wait... what?! */
      result = DENIED;
      break;
    }
  return result;
}

permission_t
ws4d_authe_request_dynamic_authorization (struct security_engine_s * s,
                                          struct soap * soap,
                                          void *ressource,
                                          ressource_type_t type)
{
  struct authorization_s *auth;
  permission_t permission;
  int store = 0;                /* store static authorization?
                                 * 0 means discard
                                 * 1 means store
                                 * 2 means update */
  time_t now;

  auth = (AuthStorage *) malloc (sizeof (AuthStorage));
  time (&now);

  /* the following call must save credential, credential type
   * and the permission to the allocated structure auth
   * also: if temporary allowed, duration must be saved*/
  if (s->authe_request_dynamic_authorization_oob)
    s->authe_request_dynamic_authorization_oob (soap, s, ressource, auth);
  else
    {
      fprintf (stderr, "No Callback to request dynamic authorization OOB\n");
      return DENIED;
    }

  ws4d_set_credential_handler (auth, auth->credential_type);
  auth->ressource = ws4d_authe_get_ressource (soap, type, 1);   /* make a copy */
  auth->ressource_type = type;

  switch (auth->permission)
    {
    case DENIED:
      /* For testing and all, a denial lasts 60 seconds */
      store = 1;
      auth->validity = now + DEFAULT_DENIAL_DURATION;
      permission = DENIED;
      break;
    case PERMITTED_ONCE:       /* permit but don't save */
      store = 0;
      auth->validity = 0;
      permission = PERMITTED;
      break;
    case PERMITTED_TEMPORARY:
      store = 1;
      /* validity is set by oob_handler */
      permission = PERMITTED;
      break;
    case PERMITTED:
      store = 1;
      auth->validity = 0;
      permission = PERMITTED;
      break;
    default:
      /* what?! */
      store = 0;
      auth->validity = 0;
      permission = DENIED;
      break;
    }

  switch (store)
    {
    case 1:                    /* save new */
      s->authdb_save_authorization (s, auth);
      break;
    case 0:                    /* discard */
      free (auth);
      break;
    }
  return permission;
}

/* This is where you ask a user for a decision - and actually nothing else,
 * because this is handled by ws4d_authe_request_dynamic_authorization()
 */
void
ws4d_authe_request_dynamic_authorization_oob (struct soap *soap,
                                              struct security_engine_s *s,
                                              void *ressource,
                                              struct authorization_s *auth)
{
  char decision;
  time_t now;
  /* Let's check, whether there is a certificate reference */
  auth->credential = ws4d_authe_get_credential (soap, CredX509CertRef);
  if (auth->credential)
    {
      auth->credential_type = CredX509CertRef;
    }
  else
    {                           /* nope - let's check for a certificate */
      auth->credential = ws4d_authe_get_credential (soap, CredX509Cert);
      if (auth->credential)
        {
          auth->credential_type = CredX509Cert;
        }
      else
        {
          fprintf (stderr, "Could not find any suitable credential!\n"
                   "Right now, only certificates and certificate references "
                   "are supported\n");
          auth->permission = DENIED;
        }
    }
  printf ("A Client identified as \n"
          "-------------------------------------------\n"
          "%s\n-------------------------------------------\n"
          "wants to access ressource \"%s\"\n",
          s->cdb_get_credential_identifier (s, auth->credential,
                                            auth->credential_type),
          (char *) ressource);
  printf ("Do you want to grant access?\n");
  printf ("\t0 - No! Deny access!\n");
  printf ("\t1 - Yes, but only once!\n");
  printf ("\t2 - Yes, but only for the next 30 seconds!\n");
  printf ("\t3 - Yes, permanently\n");
  do
    {
      printf (" > ");
      scanf ("%c", &decision);
      getchar ();               // catch newline
    }
  while (decision < '0' || decision > '3');

  auth->permission = decision - '0';
  /* Please, don't beat me to death - it's just a prototype in pre pre pre alpha */

  /* permission is temporary, we must set it here */
  if (decision == PERMITTED_TEMPORARY)
    {
      time (&now);
      auth->validity = now + 30;
    }

}

/********************************************
 * Handlers
 ********************************************/
void
ws4d_set_credential_handler (struct authorization_s *a,
                             credential_type_t type)
{
  switch (type)
    {
    case CredX509Cert:
      a->cred_match_handler = ws4d_x509_credential_handler;
      break;
    case CredX509CertRef:
      a->cred_match_handler = ws4d_x509_ref_credential_handler;
      break;
    case CredDYNAMIC:
      a->cred_match_handler = ws4d_dynamic_authorization_handler;
      break;
    }
}

int
ws4d_dynamic_authorization_handler (struct authorization_s *a,
                                    void *credential)
{
  return 0;                     /* means all okay... */
}

int
ws4d_x509_credential_handler (struct authorization_s *s, void *certificate)
{
  /* as long as we take care for our credentials to be saved as \0-terminated
   * strings, we can use strcmp() */
  return strcmp ((char *) s->credential, (char *) certificate);
}

int
ws4d_x509_ref_credential_handler (struct authorization_s *s, void *ref)
{
  return strcmp ((char *) s->credential, (char *) ref);
}

int
ws4d_authe_set_authorization_mode (struct soap *soap, int mode)
{
  struct sh_plugin_data *data = NULL;
  data = (struct sh_plugin_data *) soap_lookup_plugin (soap, SH_PLUGIN_ID);
  if (!data)
    {
      fprintf (stderr, "Error! Could not look up plugin data to set mode!\n");
      return -1;
    }
  data->insist_on_authorization = mode;
  return ERR_OK;
}

int
ws4d_authe_activate_authorization (struct soap *soap)
{
  return ws4d_authe_set_authorization_mode (soap, AUTHORIZATION_ACTIVATED);
}

int
ws4d_authe_deactivate_authorization (struct soap *soap)
{
  return ws4d_authe_set_authorization_mode (soap, AUTHORIZATION_DEACTIVATED);
}
