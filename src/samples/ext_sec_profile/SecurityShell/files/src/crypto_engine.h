/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * crypto_engine.h
 *
 *  Created on: 23.07.2010
 *      Author: su009
 *
 *  public API for WS4D Crypto Engine
 *  see crypto_engine.c for further details
 *
 */

#ifndef CRYPTO_ENGINE_H_
#define CRYPTO_ENGINE_H_

#include "security_engine.h"
#include <openssl/evp.h>

struct security_engine_s *ws4d_init_crypto_engine (struct security_engine_s
                                                   *);
struct security_engine_s *ws4d_del_crypto_engine (struct security_engine_s *);

#endif /* CRYPTO_ENGINE_H_ */
