/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * authorization_engine.h
 *
 *  Created on: 24.03.2010
 *      Author: cht
 */

#ifndef AUTHORIZATION_ENGINE_H_
#define AUTHORIZATION_ENGINE_H_

#include "security_shell_types.h"
#include "security_engine.h"

struct security_engine_s *ws4d_init_authorization_engine (struct
                                                          security_engine_s
                                                          *s);
struct security_engine_s *ws4d_del_authorization_engine (struct
                                                         security_engine_s
                                                         *s);

int ws4d_authe_set_dynamic_authorization (struct security_engine_s *s,
                                          void *ressource,
                                          permission_t permission,
                                          ressource_type_t res_type);

permission_t ws4d_authe_authorize_request (struct security_engine_s *s,
                                           struct soap *soap,
                                           unsigned int credentials,
                                           ressource_type_t res_type);

int ws4d_authe_compare_ressources (void *r1, void *r2, ressource_type_t type);
void *ws4d_authe_get_ressource (struct soap *soap, ressource_type_t type,
                                int cp);
void *ws4d_authe_get_credential (struct soap *soap, credential_type_t type);
permission_t ws4d_authe_get_permission (struct security_engine_s *s,
                                        struct authorization_s *auth);
permission_t ws4d_authe_request_dynamic_authorization (struct
                                                       security_engine_s *s,
                                                       struct soap *soap,
                                                       void *ressource,
                                                       ressource_type_t type);
void ws4d_authe_request_dynamic_authorization_oob (struct soap *soap,
                                                   struct security_engine_s
                                                   *s, void *ressource,
                                                   struct authorization_s
                                                   *auth);

void ws4d_set_credential_handler (struct authorization_s *a,
                                  credential_type_t type);
int ws4d_dynamic_authorization_handler (struct authorization_s *a,
                                        void *credential);
int ws4d_x509_credential_handler (struct authorization_s *s,
                                  void *certificate);
int ws4d_x509_ref_credential_handler (struct authorization_s *s, void *ref);


int ws4d_authe_set_authorization_mode (struct soap *soap, int mode);
int ws4d_authe_activate_authorization (struct soap *soap);
int ws4d_authe_deactivate_authorization (struct soap *soap);

#endif /* AUTHORIZATION_ENGINE_H_ */
