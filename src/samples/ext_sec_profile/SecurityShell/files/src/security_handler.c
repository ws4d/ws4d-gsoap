/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * security_handler.c
 *
 *  Created on: 26.07.2010
 *      Author: su009
 */

#include "security_handler.h"

/***********************************************************/
/*   The Security Handler Plug In                          */
/***********************************************************/
static const char plugin_id[] = SH_PLUGIN_ID;   // the plugin id

static int sh_plugin_init (struct soap *soap, struct sh_plugin_data *data,
                           struct security_engine_s *s);

static void sh_plugin_delete (struct soap *soap, struct soap_plugin *p);

int sh_plugin_fdisconnect (struct soap *soap);

int
sh_plugin (struct soap *soap, struct soap_plugin *p, void *arg)
{
  p->id = plugin_id;
  p->data = (void *) malloc (sizeof (struct sh_plugin_data));
  p->fcopy = NULL;
  p->fdelete = sh_plugin_delete;
  if (p->data)
    if (sh_plugin_init (soap, (struct sh_plugin_data *) p->data,
                        (struct security_engine_s *) arg))
      {
        free (p->data);         // error: could not init
        return SOAP_EOM;        // return error
      }
  return SOAP_OK;
}

static int
sh_plugin_init (struct soap *soap, struct sh_plugin_data *data,
                struct security_engine_s *s)
{
  if (!s)
    {
      fprintf (stderr, "Security Handler Plugin: Can't register - "
               "need Security Engine handle\n");
      return 1;
    }
  data->sec_engine = s;
  data->fdisconnect = soap->fdisconnect;
  soap->fdisconnect = sh_plugin_fdisconnect;
  data->insist_on_authentication = 0;
  data->insist_on_authorization = 0;
  return 0;
}

static void
sh_plugin_delete (struct soap *soap, struct soap_plugin *p)
{
  struct sh_plugin_data *data = NULL;
  data = (struct sh_plugin_data *) p->data;
  if (data->fdisconnect)
    soap->fdisconnect = data->fdisconnect;
  free (p->data);
}

/* I suppose this to be removable later, when crappy wsse-plugin is removed */
X509 *soap_wsse_get_BinarySecurityTokenX509 (struct soap *, const char *);


int
sh_plugin_fdisconnect (struct soap *soap)
{
  struct sh_plugin_data *data = NULL;
  ws4dX509 client_cert = NULL;
  ws4dCertRef client_cert_ref = NULL;
  permission_t permission;
  data = (struct sh_plugin_data *) soap_lookup_plugin (soap, SH_PLUGIN_ID);
  if (!data)
    {
      fprintf (stderr, "No Security Handler Plugin Data!\n");
      goto out;
    }

  /*************************************************/
  /*  Check Authentication                         */
  /*************************************************/

  if (data->insist_on_authentication)
    {
      /* look up certificate */
      client_cert_ref =
        (char *) soap_wsse_get_KeyInfo_SecurityTokenReferenceURI (soap);

      if (client_cert_ref)
        {                       /* there IS a reference */
          client_cert =
            data->sec_engine->cdb_lookup_cert_by_ref (data->sec_engine,
                                                      client_cert_ref);
        }
      else
        {                       /* there is no reference - check for embedded certificate */
          client_cert =
            soap_wsse_get_BinarySecurityTokenX509 (soap, "X509Token");
        }

      if (!client_cert)
        {
          fprintf (stderr,
                   "Neither a certificate reference nor an embedded token"
                   " could be found in message!");
          goto out;
        }

      /* finally check, whether certificate has been authenticated */
      if (data->sec_engine->ae_check_client_authentication
          (data->sec_engine, client_cert))
        {
          return soap_receiver_fault (soap, "Not Authenticated!",
                                      "<errorcode xmlns=\"http://www.ws4d.org/AuthenticationEngine/\">"
                                      "401</errorcode>");
        }
    }

  /*************************************************/
  /*  Check Authorization                          */
  /*************************************************/

  if (data->insist_on_authorization)
    {
      /* check authorization */
      permission = data->sec_engine->authe_authorize_request
        (data->sec_engine, soap,
         CredX509Cert | CredX509CertRef | CredUserPass, SERVICE);

#ifdef DEBUG
      fprintf (stderr, "Got permission code %d for Ressource type SERVICE\n",
               (int) permission);
#endif // DEBUG

      if (permission == UNKNOWN)
        {
          /* this actually means, that there is no permssion set on SERVICE-level
           * so let's check for permissions on METHOD-level */
          permission = data->sec_engine->authe_authorize_request
            (data->sec_engine, soap,
             CredX509Cert | CredX509CertRef | CredUserPass, METHOD);

#ifdef DEBUG
          fprintf (stderr,
                   "Got permission code %d for Ressource type SERVICE\n",
                   (int) permission);
#endif // DEBUG
        }


    /*******************************************************/
      /*                                                     */
      /*     place further checks for authorization here     */
      /*                                                     */
    /*******************************************************/


      /* if still unknown, sth. mus be wrong */
      if (permission == UNKNOWN)
        fprintf (stderr,
                 "After all checks, permission is UNKNOWN! This most likely means that something\n"
                 "is wrong with setting permissions on application level.\n");

      if (permission != PERMITTED)
        {
          return soap_receiver_fault (soap, "Access DENIED!",
                                      "<errorcode xmlns=\"http://www.ws4d.org/AuthorizationEngine/\">"
                                      "403</errorcode>");
        }
    }

out:
  if (data->fdisconnect)
    return data->fdisconnect (soap);
  else
    return ERR_OK;
}

void
sh_insist_on_authentication (struct soap *soap, int mode)
{
  struct sh_plugin_data *data = NULL;
  data = (struct sh_plugin_data *) soap_lookup_plugin (soap, SH_PLUGIN_ID);
  if (!data)
    {
      fprintf (stderr, "Could not insist on authentication since I could "
               "not lookup plugin data!\n");
      return;
    }
  data->insist_on_authentication = mode;
}
