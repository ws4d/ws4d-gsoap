/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * authorization_db.c
 *
 *  Created on: 24.03.2010
 *      Author: cht
 */

#include "authorization_db.h"
#include <string.h>

struct security_engine_s *
ws4d_init_authorization_db (struct security_engine_s *s)
{
  WS4D_INIT_LIST (&(s->auth_list_head));
  s->authdb_save_authorization = ws4d_authdb_save_authorization;
  s->authdb_delete_authorization = ws4d_authdb_delete_authorization;
  s->authdb_lookup_authorization = ws4d_authdb_lookup_authorization;
  s->authdb_update_authorization = ws4d_authdb_update_authorization;
  return s;
}

struct security_engine_s *
ws4d_del_authorization_db (struct security_engine_s *s)
{
  s->authdb_save_authorization = NULL;
  s->authdb_delete_authorization = NULL;
  s->authdb_update_authorization = NULL;
  s->authdb_lookup_authorization = NULL;
  return s;
}

int
ws4d_authdb_save_authorization (struct security_engine_s *s,
                                struct authorization_s *new)
{
  ws4d_list_add (&(new->list), &(s->auth_list_head));
#ifdef DEBUG
  fprintf (stderr, "authorization_db: saved new authorization:\n");
  fprintf (stderr, "\tcred-type: %d\n\tressource: %s\n\tvalidity: %d / %s\n",
           new->credential_type, (char *) new->ressource, (int) new->validity,
           ctime (&(new->validity)));
#endif //DEBUG
  /* TODO: make database persistent */
  return ERR_OK;
}

int
ws4d_authdb_delete_authorization (struct security_engine_s *s,
                                  struct authorization_s *a)
{
  WS4D_UNUSED_PARAM (s);
#ifdef DEBUG
  fprintf (stderr, "authorization_db: deleted authorization:\n");
  fprintf (stderr, "\tcred-type: %d\n\tressource: %s\n\tvalidity: %d / %s\n",
           a->credential_type, (char *) a->ressource, (int) a->validity,
           ctime (&(a->validity)));
#endif //DEBUG
  ws4d_list_del (&(a->list));
  return ERR_OK;
}

struct authorization_s *
ws4d_authdb_lookup_authorization (struct security_engine_s *s,
                                  void *credential, void *ressource,
                                  credential_type_t type)
{
  struct authorization_s *pos = NULL, *next = NULL;
  int l1, l2, l, i;
  if (!(credential || ressource))
    return NULL;
  ws4d_list_foreach (pos, next, &(s->auth_list_head), struct authorization_s,
                     list)
  {
    if (type != pos->credential_type)
      continue;
    if (credential)
      {
        l1 = strlen ((char *) credential);
        l2 = strlen ((char *) pos->credential);
        l = (l1 < l2) ? l1 : l2;
        if (memcmp (pos->credential, credential, l))
          continue;
      }
    if (ressource)
      {
        l1 = strlen ((char *) ressource);
        l2 = strlen ((char *) pos->ressource);
        l = (l1 < l2) ? l1 : l2;
        if (memcmp (pos->ressource, ressource, l))
          continue;
      }
    return pos;
  }
  return NULL;
}

int
ws4d_authdb_update_authorization (struct security_engine_s *s,
                                  struct authorization_s *auth)
{
  struct authorization_s *elem = NULL;

  elem = ws4d_authdb_lookup_authorization (s, auth->credential,
                                           auth->ressource,
                                           auth->credential_type);

  if (elem == NULL)
    return ERR_NO_ELEM;

  elem->permission = auth->permission;
  elem->validity = auth->validity;
  return ERR_OK;
}
