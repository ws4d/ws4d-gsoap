/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * crypto_engine.c
 *
 *  Created on: 23.07.2010
 *      Author: su009
 *
 *  Supposed to offer interface to OpenSSL, used by WS4D-Security-Profile
 *  implementation. Distant goal is to be compliant with
 *    a) DPWS 1.1, cpt. 6 (Security) incl. WS-DD 1.1 cpt. 8 (Security)
 *    b) WS4D Security Profile
 *    c) maybe more to come...
 *
 *  The crypto engine will most likely be used by the security handler only
 *
 *  For getting started:
 *    - Timestamps
 *    - Signatures      , by means of
 *      a) WS-DD Compact Signatures
 *      b) XML-Signature / WS-Security
 *    - XML-Encryption
 *
 *  Except for Timestamps, the OpenSSL-EVP-Interface actually offers everything
 *  we need for our purpose. So, let's give it a try...
 *
 */

#include "crypto_engine.h"

struct security_engine_s *
ws4d_init_crypto_engine (struct security_engine_s *sec_engine)
{

}


struct security_engine_s *
ws4d_del_crypto_engine (struct security_engine_s *sec_engine)
{

}
