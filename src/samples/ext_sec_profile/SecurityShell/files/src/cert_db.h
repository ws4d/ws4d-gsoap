/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * cert_db.h
 *
 *  Created on: 09.03.2010
 *      Author: cht
 */

#ifndef CERT_DB_H_
#define CERT_DB_H_

#include "security_engine.h"
#include "security_shell_types.h"


void ws4d_cdb_update_challenge (struct security_engine_s *s,
                                ws4dX509 cert, ws4dChallenge challenge);

void ws4d_cdb_update_pin (CertStorage * cert_elem, ws4dPin pin);

struct security_engine_s *ws4d_init_certificate_db (struct security_engine_s
                                                    *s);

struct security_engine_s *ws4d_del_certificate_db (struct security_engine_s
                                                   *s);

ws4dCertRef ws4d_cdb_save_certificate (struct security_engine_s *s,
                                       ws4dX509 cert, ws4dPin pin);

ws4dPin ws4d_cdb_lookup_certificate_pin (struct security_engine_s *s,
                                         ws4dX509 cert);
struct _cert_list_elem *ws4d_cdb_lookup_certificate (struct security_engine_s
                                                     *s, ws4dX509 cert);

int ws4d_cdb_cert_compare (ws4dX509 c1, ws4dX509 c2);

char *ws4d_cdb_compute_reference (CertStorage * cert_data);

void ws4d_cdb_save_cert_reference (struct security_engine_s *s,
                                   char *uri, ws4dCertRef ref, ws4dX509 cert);

ws4dCertRef ws4d_cdb_lookup_cert_reference (struct security_engine_s *s,
                                            char *uri);

ws4dX509 ws4d_cdb_lookup_cert_by_ref (struct security_engine_s *s,
                                      ws4dCertRef ref);

int ws4d_cdb_update_persistent_db (struct security_engine_s *s);
int ws4d_cdb_read_persistent_db (struct security_engine_s *s);

/* supposed to lookup certificate from cert_db according to reference
 * gets invoked by soap_wsse-plugin when retrieving certificate to verify
 * signature  */
const void *ws4d_cdb_wsse_token_handler (struct soap *soap, int alg,
                                         int *keylen);

const char *ws4d_cdb_get_credential_identifier (struct security_engine_s *s,
                                                void *credential,
                                                credential_type_t type);

#endif /* CERT_DB_H_ */
