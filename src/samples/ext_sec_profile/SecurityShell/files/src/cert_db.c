/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * cert_db.c
 *
 *  Created on: 09.03.2010
 *      Author: cht
 */

#include "cert_db.h"
#include "ws4d_misc.h"
#include "ws4d_list.h"
#include "smdevp.h"

void
ws4d_cdb_update_challenge (struct security_engine_s *s,
                           ws4dX509 cert, ws4dChallenge challenge)
{
  struct _cert_list_elem *pos = NULL, *next = NULL;

  ws4d_list_foreach (pos, next, &(s->cert_list_head), struct _cert_list_elem,
                     list)
  {
    if (!ws4d_cdb_cert_compare (pos->cert, cert))
      {
        pos->challenge = challenge;
        return;
      }
  }
}

void
ws4d_cdb_update_pin (CertStorage * cert_elem, ws4dPin pin)
{
  free (cert_elem->pin);
  cert_elem->pin = pin;
}

struct security_engine_s *
ws4d_init_certificate_db (struct security_engine_s *s)
{

  WS4D_INIT_LIST (&(s->cert_list_head));
  s->cdb_save_certificate = ws4d_cdb_save_certificate;
  s->cdb_lookup_certificate_pin = ws4d_cdb_lookup_certificate_pin;
  s->cdb_lookup_certificate = ws4d_cdb_lookup_certificate;
  s->cdb_update_challenge = ws4d_cdb_update_challenge;
  s->cdb_update_pin = ws4d_cdb_update_pin;
  s->cdb_cert_compare = ws4d_cdb_cert_compare;
  s->cdb_compute_reference = ws4d_cdb_compute_reference;
  s->cdb_save_cert_reference = ws4d_cdb_save_cert_reference;
  s->cdb_lookup_cert_reference = ws4d_cdb_lookup_cert_reference;
  s->cdb_update_persistent_db = ws4d_cdb_update_persistent_db;
  s->cdb_read_persistent_db = ws4d_cdb_read_persistent_db;
  s->cdb_lookup_cert_by_ref = ws4d_cdb_lookup_cert_by_ref;
  s->cdb_wsse_token_handler = ws4d_cdb_wsse_token_handler;
  s->cdb_get_credential_identifier = ws4d_cdb_get_credential_identifier;

  s->cdb_read_persistent_db (s);
  return s;
}

struct security_engine_s *
ws4d_del_certificate_db (struct security_engine_s *s)
{

  s->cdb_save_certificate = NULL;
  s->cdb_lookup_certificate_pin = NULL;
  s->cdb_lookup_certificate = NULL;
  s->cdb_update_challenge = NULL;
  s->cdb_update_pin = NULL;
  s->cdb_cert_compare = NULL;
  s->cdb_compute_reference = NULL;
  s->cdb_save_cert_reference = NULL;
  s->cdb_lookup_cert_reference = NULL;
  s->cdb_update_persistent_db = NULL;
  s->cdb_read_persistent_db = NULL;
  s->cdb_lookup_cert_by_ref = NULL;
  return s;
}

ws4dCertRef
ws4d_cdb_save_certificate (struct security_engine_s * s,
                           ws4dX509 cert, ws4dPin pin)
{
  long int size;
  if (pin)
    {                           /* temporary, pin to be acknowledged */
      struct _cert_list_elem *new = malloc (sizeof (struct _cert_list_elem));

      new->cert = X509_dup (cert);

      new->ack = 0;
      new->challenge = NULL;
      new->pin = pin;
      new->ref = NULL;
      new->count = 0;
      new->block_until = 0;
      new->uri = NULL;
      ws4d_list_add (&(new->list), &(s->cert_list_head));
    }
  else
    {                           /* persistent, pin has been acknowledged */

    }

  return NULL;
}

ws4dPin
ws4d_cdb_lookup_certificate_pin (struct security_engine_s * s, ws4dX509 cert)
{
  /* We need to know, whether there has been a PIN in the PAST
   * which hasn't been confirmed yet
   */
  ws4dPin pin = NULL;
  struct _cert_list_elem *pos = NULL, *next = NULL;
  ws4d_list_foreach (pos, next, &(s->cert_list_head), struct _cert_list_elem,
                     list)
  {
    printf ("lookup_certificate_pin:\nref: <%s>\nname: <%s>\n\n", pos->ref,
            pos->cert->name);
    if (!ws4d_cdb_cert_compare (pos->cert, cert))
      {
        pin = pos->pin;
        break;
      }
  }
  return pin;
}

struct _cert_list_elem *
ws4d_cdb_lookup_certificate (struct security_engine_s *s, ws4dX509 cert)
{
  struct _cert_list_elem *result = NULL;
  struct _cert_list_elem *pos = NULL, *next = NULL;
  ws4d_list_foreach (pos, next, &(s->cert_list_head), struct _cert_list_elem,
                     list)
  {
    if (!ws4d_cdb_cert_compare (pos->cert, cert))
      {
        result = pos;
        break;
      }
  }
  return result;
}

int
ws4d_cdb_cert_compare (ws4dX509 c1, ws4dX509 c2)
{
  return X509_cmp ((const ws4dX509) c1, (const ws4dX509) c2);
}

char *
ws4d_cdb_compute_reference (CertStorage * cert_data)
{
  int i;
  if (!cert_data->ref)
    {                           /* we cover the case that we once assigned a reference already */
      cert_data->ref = malloc (REFERENCE_SIZE + 1);
      memset (cert_data->ref, 0, REFERENCE_SIZE + 1);
      strncpy (cert_data->ref, "http://ws4d.org/certificate-reference/", 38);
      for (i = 38; i < REFERENCE_SIZE; i++)
        cert_data->ref[i] = (char) ('A' + (rand () % ('z' - 'A' + 1)));
    }

#ifdef DEBUG
  fprintf (stderr, "Saved Certificate Reference:\n\t%s\n", cert_data->ref);
#endif // DEBUG

  return cert_data->ref;
}

void
ws4d_cdb_save_cert_reference (struct security_engine_s *s,
                              char *uri, ws4dCertRef ref, ws4dX509 cert)
{
  if (uri && ref && cert)
    {
      struct _cert_list_elem *new = malloc (sizeof (struct _cert_list_elem));
      /* init */
      new->cert = cert;
      new->ack = 1;
      new->challenge = NULL;
      new->pin = NULL;
      new->count = 0;
      new->block_until = 0;

      /* copy parameters */
      new->ref = (unsigned char *) malloc (strlen (ref) + 1);
      memset (new->ref, 0, strlen (ref) + 1);
      strcpy (new->ref, ref);

      new->uri = (char *) malloc (strlen (uri) + 1);
      memset (new->uri, 0, strlen (uri) + 1);
      strcpy (new->uri, uri);

#ifdef DEBUG
      fprintf (stderr, "saved certificate reference:\n\t<%s>:<%s>\n", uri,
               ref);
#endif // DEBUG

      ws4d_list_add (&(new->list), &(s->cert_list_head));
      /* update persistent database */
      s->cdb_update_persistent_db (s);
    }
}

ws4dCertRef
ws4d_cdb_lookup_cert_reference (struct security_engine_s *s, char *uri)
{
  ws4dCertRef result = NULL;
  struct _cert_list_elem *pos, *next;
  ws4d_list_foreach (pos, next, &(s->cert_list_head), struct _cert_list_elem,
                     list)
  {
    if (!pos->uri)
      continue;
    int len = 0;
    len =
      (strlen (uri) < strlen (pos->uri)) ? strlen (uri) : strlen (pos->uri);
    if (!strncmp (uri, pos->uri, len))
      {
        result = pos->ref;
        break;
      }
  }
  return result;
}

/* designated order for elements in persistent "database":
 * btw: only save authenticated certifiactes
 * ref (1st, because definitely != NULL)
 * cert
 * uri
 */

int
ws4d_cdb_update_persistent_db (struct security_engine_s *s)
{
  /* simple approach: foreach element write each member in a designated order in
   * one row of a plain txt-file */
  /* even simpler approach: don't really update but rewrite completely */
  struct _cert_list_elem *pos, *next;
  FILE *fd;
  if (s->cert_list_location == NULL)
    {
      return ERR_OK;
    }
  if (!(fd = fopen (s->cert_list_location, "w")))
    {
      return ERR_FILEIO;
    }
  ws4d_list_foreach (pos, next, &(s->cert_list_head), struct _cert_list_elem,
                     list)
  {
    if (pos->ack)
      {                         /* authenticated certificate */
        fprintf (fd, "%s\n", (pos->ref) ? pos->ref : "");
        /* certificate is in saved in structured form - we need it bas64-coded */
        if (pos->cert)
          {
            struct x509_st *cert_copy = NULL;
            unsigned char *bin_cert = NULL;
            char *b64_cert = NULL;
            int size = 0;
            struct soap soap;
            soap_init (&soap);
            soap_done (&soap);

            cert_copy = X509_dup (pos->cert);

            /* convert structred type to binary form */
            size = i2d_X509 (pos->cert, &bin_cert);
            /* convert binary form to base64-coded form */
            b64_cert =
              soap_s2base64 (&soap, (const unsigned char *) bin_cert, NULL,
                             size);

            fprintf (fd, "%s\n", b64_cert);
            soap_done (&soap);
            soap_end (&soap);
          }
        else
          {
            fprintf (fd, "\n"); /* empty row - no cert */
          }
        fprintf (fd, "%s\n", (pos->uri) ? pos->uri : "");
      }
  }
  fclose (fd);

  return ERR_OK;
}

int
ws4d_cdb_read_persistent_db (struct security_engine_s *s)
{
  int state = 0;                /* 0 - read red, 1 - read cert, 2 - read uri */
  char *row = (char *) malloc (5000);
  struct _cert_list_elem **new_list = NULL;
  int count = 0;
  FILE *fd;

  if (s->cert_list_location == NULL)
    {
      return ERR_OK;
    }

  if (!(fd = fopen (s->cert_list_location, "r")))
    {
      return ERR_FILEIO;
    }

  memset (row, 0, 5000);

  while (row = fgets (row, 5000, fd))
    {
      switch (state)
        {
        case 0:
          if (row[0] != '\n')
            {                   /* skip empty rows */
              count++;
              /* new item */
              new_list = (struct _cert_list_elem **)
                realloc (new_list, count * sizeof (struct _cert_list_elem *));
              new_list[count - 1] = (struct _cert_list_elem *)
                malloc (sizeof (struct _cert_list_elem));

              /* copy reference */
              row[strlen (row) - 1] = '\0';     /* kill newline */
              new_list[count - 1]->ref =
                (unsigned char *) malloc (strlen (row) + 1);
              memset (new_list[count - 1]->ref, 0, strlen (row) + 1);
              strcpy (new_list[count - 1]->ref, row);

              state = 1;
            }
          break;

        case 1:
          if (row[0] == '\n')
            {                   /* empty row - no certificate */
              new_list[count - 1]->cert = NULL;
            }
          else
            {
              char *bin_cert;
              int size;
              struct soap soap;

              soap_init (&soap);
              /* kill newline */
              row[strlen (row) - 1] = '\0';

              bin_cert = (char *) soap_base642s (&soap, row, NULL, 0, &size);
              new_list[count - 1]->cert = d2i_X509 (NULL,
                                                    (const unsigned char **)
                                                    &bin_cert, size);
            }

          state = 2;
          break;

        case 2:
          if (row[0] == '\n')
            {
              new_list[count - 1]->uri = NULL;
            }
          else
            {
              row[strlen (row) - 1] = '\0';     /* kill newline */
              new_list[count - 1]->uri = (char *) malloc (strlen (row) + 1);
              memset (new_list[count - 1]->uri, 0, strlen (row) + 1);
              strcpy (new_list[count - 1]->uri, row);
            }
          /* filling the defaults */
          new_list[count - 1]->ack = 1;
          new_list[count - 1]->block_until = 0;
          new_list[count - 1]->challenge = NULL;
          new_list[count - 1]->count = 0;
          new_list[count - 1]->pin = 0;

          ws4d_list_add (&(new_list[count - 1]->list), &(s->cert_list_head));

          state = 0;
          break;
        }
    }

  if (state)
    fprintf (stderr, "Something seems to be wrong with reading the persistent"
             " database.\nI am currently in state %d - I assume though,"
             " I should be in state 0\n", state);

  return ERR_OK;
}

ws4dX509
ws4d_cdb_lookup_cert_by_ref (struct security_engine_s * s, ws4dCertRef ref)
{
  struct _cert_list_elem *pos, *next;
  ws4d_list_foreach (pos, next, &(s->cert_list_head), struct _cert_list_elem,
                     list)
  {
    if (!strcmp (pos->ref, ref))
      return X509_dup (pos->cert);
  }
  return NULL;
}

const void *
ws4d_cdb_wsse_token_handler (struct soap *soap, int alg, int *keylen)
{

  ws4dX509 cert = NULL;
  ws4dCertRef ref = NULL;
  struct sh_plugin_data *data = (struct sh_plugin_data *)
    soap_lookup_plugin (soap, SH_PLUGIN_ID);
  if (!data || !data->sec_engine)
    {
      fprintf (stderr, "Error looking up Plug-In data or security engine not"
               " assigned!\nWon't be able to look up a certificate\n");
      return NULL;
    }
  switch (alg)
    {
    case SOAP_SMD_VRFY_DSA_SHA1:
    case SOAP_SMD_VRFY_RSA_SHA1:
      ref = (char *) soap_wsse_get_KeyInfo_SecurityTokenReferenceURI (soap);
      if (ref == (char *) NULL)
        {
          fprintf (stderr,
                   "Could not retrieve certificate reference from message!\n");
          cert = NULL;
          break;
        }
      cert = ws4d_cdb_lookup_cert_by_ref (data->sec_engine, ref);
      if (cert == (ws4dX509) NULL)
        {
          fprintf (stderr,
                   "Did not find any certificate with reference\n\t%s\n",
                   ref);
        }

      break;
    default:
      fprintf (stderr,
               "Unknown Algorithm! Will not be able to return a certificate\n");
      cert = NULL;
      break;
    }
  return cert;
}

const char *
ws4d_cdb_get_credential_identifier (struct security_engine_s *s,
                                    void *credential, credential_type_t type)
{
  char *result = NULL;
  int result_len = 0;
  ws4dX509 cert = NULL;
  switch (type)
    {
    case CredX509CertRef:
      cert = s->cdb_lookup_cert_by_ref (s, (ws4dCertRef) credential);
      break;
    case CredX509Cert:
      cert = (ws4dX509) credential;
      break;
    default:
      fprintf (stderr,
               "Error! Credential type %d not supported to look up identifier\n",
               type);
      break;
    }
  if (cert)
    {
      /*/C=[CntryCde]/ST=[State]/L=[City]/O=[Org]/CN=[name]/emailAddress=[eMail] */
      char *ptr, *ptr2, *ptr3, *tmp = NULL;
      char *ctry = NULL, *st = NULL, *city = NULL, *org = NULL, *name =
        NULL, *mail = NULL;

      ptr = cert->name;

      while (ptr = strchr (ptr, '/'))
        {
          ptr++;                /* skip '/' */
          ptr2 = strchr (ptr, '/');
          if (!ptr2)
            {
              ptr2 = ptr + strlen (ptr);        /* points at \0 */
            }
          ptr3 = strchr (ptr, '=');
          tmp = (char *) malloc (ptr2 - ptr3);
          memset (tmp, 0, ptr2 - ptr3);
          strncpy (tmp, ptr3 + 1, ptr2 - ptr3 - 1);

          if (ptr[0] == 'C' && ptr[1] != 'N')
            {
              ctry = tmp;
            }
          else if (ptr[0] == 'S' && ptr[1] == 'T')
            {
              st = tmp;
            }
          else if (ptr[0] == 'L')
            {
              city = tmp;
            }
          else if (ptr[0] == 'O')
            {
              org = tmp;
            }
          else if (ptr[0] == 'C' && ptr[1] == 'N')
            {
              name = tmp;
            }
          else if (!strncmp (ptr, "emailAddress", 12))
            {
              mail = tmp;
            }
          else
            {
              fprintf (stderr, "Can't work with substring %s!\n", ptr);
            }
          tmp = NULL;
        }
      result_len = strlen (ctry) + 1 + strlen (st) + 1 + strlen (city) + 1 +
        strlen (org) + 1 + strlen (name) + 1 + strlen (mail) + 1;
      result = (char *) malloc (result_len);
      ptr = result;

      sprintf (result, "%s\n%s\n%s\n%s\n%s\n%s", ctry, st, city, org, name,
               mail);
    }
  return (const char *) result;
}
