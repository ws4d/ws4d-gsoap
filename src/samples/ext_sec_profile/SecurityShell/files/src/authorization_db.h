/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * authorization_db.h
 *
 *  Created on: 24.03.2010
 *      Author: cht
 */

#ifndef AUTHORIZATION_DB_H_
#define AUTHORIZATION_DB_H_

#include <time.h>
#include "security_shell_types.h"
#include "security_engine.h"

struct security_engine_s *ws4d_init_authorization_db (struct security_engine_s
                                                      *s);
struct security_engine_s *ws4d_del_authorization_db (struct security_engine_s
                                                     *s);

int ws4d_authdb_save_authorization (struct security_engine_s *s,
                                    struct authorization_s *new);

int ws4d_authdb_delete_authorization (struct security_engine_s *s,
                                      struct authorization_s *a);

/* lookup 1st element in list, with matching credential, ressource or both */
/*   but definitely with matching type
/* simply set credential OR ressource NULL to only match the other parameter */
struct authorization_s *ws4d_authdb_lookup_authorization (struct
                                                          security_engine_s
                                                          *s,
                                                          void *credential,
                                                          void *ressource,
                                                          credential_type_t
                                                          type);

int ws4d_authdb_update_authorization (struct security_engine_s *s,
                                      struct authorization_s *auth);

#endif /* AUTHORIZATION_DB_H_ */
