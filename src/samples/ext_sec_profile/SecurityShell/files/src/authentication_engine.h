/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * authentication_engine.h
 *
 *  Created on: 09.03.2010
 *      Author: cht
 */

#ifndef AUTHENTICATION_ENGINE_H_
#define AUTHENTICATION_ENGINE_H_

#include "security_engine.h"
#include "security_shell_types.h"
#include <string.h>


struct security_engine_s *ws4d_init_authentication_engine (struct
                                                           security_engine_s
                                                           *s);

int ws4d_ae_process_request_one (struct security_engine_s *s, ws4dX509 cert,
                                 ws4dChallenge challenge);

int ws4d_ae_process_request_two (struct security_engine_s *s,
                                 ws4dX509 cert,
                                 ws4dResponse client_response,
                                 ws4dChallenge challenge,
                                 ws4dResponse device_response,
                                 ws4dCertRef cert_reference);

char *ws4d_ae_request_pin (struct security_engine_s *s, ws4dX509 client_cert);

char *ws4d_ae_generate_new_pin (void);

ws4dChallenge ws4d_ae_generate_challenge (struct security_engine_s *s,
                                          ws4dChallenge challenge);

ws4dDerivedKey ws4d_ae_derive_key_from_pin (struct security_engine_s *s,
                                            ws4dPin pin);
ws4dResponse ws4d_ae_compute_response (struct security_engine_s *s,
                                       ws4dChallenge challenge,
                                       ws4dDerivedKey key);

int ws4d_ae_verify_response (struct security_engine_s *s,
                             ws4dChallenge challenge,
                             ws4dResponse response, ws4dDerivedKey key);

void ws4d_ae_propagate_pin (struct security_engine_s *s, ws4dPin pin);

/* returns negative number as error bitfield or 0 if everything is okay */
int ws4d_ae_tripple_des (ws4dChallenge response, ws4dChallenge plainResponse,
                         ws4dDerivedKey key, int mode);

/* check whether a client with a certain certificate / a device with a certain
 * UUID or cert has been authenticated before */
int ws4d_ae_check_client_authentication (struct security_engine_s *s,
                                         ws4dX509 cert);
int ws4d_ae_check_device_authentication (struct security_engine_s *s,
                                         char *uri, ws4dX509 cert);
ws4dPin ws4d_ae_retrieve_pin (struct security_engine_s *s, ws4dPin pin);


#endif /* AUTHENTICATION_ENGINE_H_ */
