/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * authentication_engine.c
 *
 *  Created on: 09.03.2010
 *      Author: cht
 */

#include "authentication_engine.h"
#include <openssl/des.h>


struct security_engine_s *
ws4d_init_authentication_engine (struct security_engine_s *s)
{

  s->ae_process_request_one = ws4d_ae_process_request_one;
  s->ae_process_request_two = ws4d_ae_process_request_two;
  s->ae_request_pin = ws4d_ae_request_pin;
  s->ae_derive_key_from_pin = ws4d_ae_derive_key_from_pin;
  s->ae_generate_challenge = ws4d_ae_generate_challenge;
  s->ae_compute_response = ws4d_ae_compute_response;
  s->ae_verify_response = ws4d_ae_verify_response;
  s->ae_check_client_authentication = ws4d_ae_check_client_authentication;
  s->ae_check_device_authentication = ws4d_ae_check_device_authentication;
  s->ae_retrieve_pin = ws4d_ae_retrieve_pin;
  s->ae_propagate_pin = ws4d_ae_propagate_pin;
  return s;
}

struct security_engine_s *
ws4d_del_authentication_engine (struct security_engine_s *s)
{

  s->ae_process_request_one = NULL;
  s->ae_process_request_two = NULL;
  s->ae_request_pin = NULL;
  s->ae_derive_key_from_pin = NULL;
  s->ae_generate_challenge = NULL;
  s->ae_compute_response = NULL;
  s->ae_verify_response = NULL;
  s->ae_check_client_authentication = NULL;
  s->ae_check_device_authentication = NULL;
  s->ae_retrieve_pin = NULL;
  s->ae_propagate_pin = NULL;
  return s;
}

int
ws4d_ae_process_request_one (struct security_engine_s *s, ws4dX509 cert,
                             ws4dChallenge challenge)
{
  /* receive a PIN, generate Challenge and Pin, save cert with challenge and PIN */
  char *pin;
  time_t now;
  CertStorage *cert_data = s->cdb_lookup_certificate (s, cert);

  /* check for counter and penalties - we don't want to allow any bruteforcing */
  if (cert_data)
    {                           /* if this is the first time - nothing can happen... */
      time (&now);
      if (cert_data->block_until > now)
        {
          /* already blocked */
#ifdef DEBUG
          fprintf (stderr, "Cert is blocked until %s\n",
                   ctime (&(cert_data->block_until)));
#endif //DEBUG
          return ERR_BLOCKED;
        }

      if ((cert_data->block_until < now) && (cert_data->block_until != 0))
        {
          /* used to be blocked, but penalty is over */
#ifdef DEBUG
          fprintf (stderr, "Cert was blocked - penalty over\n");
#endif //DEBUG
          cert_data->count = 0;
          cert_data->block_until = 0;
        }

      if (cert_data->count > MAX_WRONG_PINS)
        {
          /* one too many... block now */
          cert_data->count = 0;
          cert_data->block_until = now + WRONG_PIN_PENALTY;
#ifdef DEBUG
          fprintf (stderr, "Cert is getting blocked now until %s\n",
                   ctime (&(cert_data->block_until)));
#endif //DEBUG
          return ERR_BLOCKED;
        }
    }

  pin = ws4d_ae_request_pin (s, cert);

  ws4d_ae_generate_challenge (s, challenge);

  if (s->cdb_update_challenge)
    (*s->cdb_update_challenge) (s, cert, challenge);



  return ERR_OK;
}

int
ws4d_ae_process_request_two (struct security_engine_s *s,
                             ws4dX509 cert,
                             ws4dResponse client_response,
                             ws4dChallenge challenge,
                             ws4dResponse device_response,
                             ws4dCertRef cert_reference)
{
  /* check sequence and client's response, generate own response */

  struct _cert_list_elem *certificate_data = NULL;
  ws4dDerivedKey key;
  ws4dResponse res;
  int i;
  time_t now;

  /* is there an unconfirmed pin? */
  certificate_data = (*s->cdb_lookup_certificate) (s, cert);
  if (!certificate_data || !certificate_data->pin)
    {                           /* no former request */
      printf ("No former request found!\n");
      return ERR_SEQUENCE;
    }

  /* certificate blocked? */
  time (&now);
  if (certificate_data->block_until > now)
    return ERR_BLOCKED;

  key = ws4d_ae_derive_key_from_pin (s, certificate_data->pin);

  /* valid response? */
  if (ws4d_ae_verify_response
      (s, certificate_data->challenge, client_response, key) != ERR_OK)
    {
      printf ("Client Response invalid!");
      /* remember bad pin */
      certificate_data->count++;
#ifdef DEBUG
      fprintf (stderr, "Entered wrong pin %d time(s)\n",
               certificate_data->count);
#endif
      return ERR_RESPONSE;
    }

  /* PIN okay - reset counter */
  certificate_data->count = 0;

  /* compute own response on client's challenge */

  res = ws4d_ae_compute_response (s, challenge, key);

  /* okay, mark certificate as authenticated */
  certificate_data->ack = 1;
  certificate_data->challenge = NULL;
  certificate_data->pin = NULL;
  if (s->cdb_compute_reference)
    {
      s->cdb_compute_reference (certificate_data);
    }

  s->cdb_update_persistent_db (s);

  memcpy (cert_reference, certificate_data->ref, REFERENCE_SIZE);
  memcpy (device_response, res, CHALLENGE_SIZE);

  return ERR_OK;
}

char *
ws4d_ae_request_pin (struct security_engine_s *s, ws4dX509 client_cert)
{
  char *pin;
  if (s->cdb_lookup_certificate_pin)
    {
      pin = (*s->cdb_lookup_certificate_pin) (s, client_cert);
      if (pin)
        {
          int i;
#ifdef DEBUG
          fprintf (stderr, "Found unconfirmed pin: >>");
          for (i = 0; i < 6; i++)
            fprintf (stderr, "%c", pin[i]);
          fprintf (stderr, "<<\n");
#endif //DEBUG
        }
    }
  if (pin == NULL)
    {                           /* no pin waiting to be confirmed */
      CertStorage *elem = NULL;
      pin = ws4d_ae_generate_new_pin ();
      /* however, it could be, that this certificate has been saved earlier */
      if (s->cdb_lookup_certificate)
        {
          elem = s->cdb_lookup_certificate (s, client_cert);
        }
      if (elem)
        {
#ifdef DEBUG
          fprintf (stderr,
                   "I already know (and most likely authenticated) this cert\n\n");
#endif //DEBUG
          if (s->cdb_update_pin)
            s->cdb_update_pin (elem, pin);
        }
      else
        {
#ifdef DEBUG
          fprintf (stderr, "New Certificate - will save it\n\n");
#endif // DEBUG
          if (s->cdb_save_certificate)
            *(s->cdb_save_certificate) (s, client_cert, pin);
        }
      if (s->ae_propagate_pin)
        s->ae_propagate_pin (s, pin);
    }
  return pin;
}

char *
ws4d_ae_generate_new_pin (void)
{
  int i;
  char *pin = (char *) malloc (PIN_SIZE * sizeof (char));

  for (i = 0; i < PIN_SIZE; i++)
    {
      pin[i] = 35 + (rand () % 90);     /* Ascii 33..127 */
    }
  return pin;
}

ws4dChallenge
ws4d_ae_generate_challenge (struct security_engine_s * s,
                            ws4dChallenge challenge)
{
  int i;
  if (challenge == NULL)
    challenge = (ws4dChallenge) malloc (CHALLENGE_SIZE);
  for (i = 0; i < CHALLENGE_SIZE; i++)
    {
      challenge[i] = 35 + (rand () % 90);
    }
  return challenge;
}

ws4dDerivedKey
ws4d_ae_derive_key_from_pin (struct security_engine_s * s, ws4dPin pin)
{
  /* Do something with Pin to retrieve a longer key
   * with higher entropy
   *
   * 1. generate second pin, bytes inverted bitwise
   * 2. generate long tmp pass
   * 3. fold with some primes
   */
  int i, j;
  int seq[] = { 5, 11, 17, 29, 37, 43, 47, 59, 0 };
  ws4dDerivedKey result = (ws4dDerivedKey) malloc (KEY_SIZE * sizeof (char));
  ws4dPin inverted = (ws4dPin) malloc (PIN_SIZE);

  /* 1. */
  for (i = 0; i < PIN_SIZE; i++)
    {
      inverted[i] = (unsigned char) (0xFF - pin[i]);
    }

  /* 2
   * be p = pin and i = inverted
   * result = cycle(p[0]..p[5]i[0]..i[5]p[5]..p[0]i[5]..i[0])*/
  for (i = 0; i < KEY_SIZE; i++)
    {
      if (!((i - i % PIN_SIZE) % (4 * PIN_SIZE)))       /* pin, forward */
        result[i] = pin[i % PIN_SIZE];
      if (!((i + (3 * PIN_SIZE) - i % PIN_SIZE) % (4 * PIN_SIZE)))      /* inverted, forward */
        result[i] = inverted[i % PIN_SIZE];
      if (!((i + (2 * PIN_SIZE) - i % PIN_SIZE) % (4 * PIN_SIZE)))      /* pin, backward */
        result[i] = pin[PIN_SIZE - 1 - (i % PIN_SIZE)];
      if (!((i + PIN_SIZE - i % PIN_SIZE) % (4 * PIN_SIZE)))    /* inverted, backward */
        result[i] = inverted[PIN_SIZE - 1 - (i % PIN_SIZE)];
    }

  /* 3
   * depending on the sequence (e.g.) 5,11,17,29,37,43,47,59
   * take result, rotate right bytewise according to sequence,
   * add to former result*/
  for (i = 0; seq[i] > 0; i++)
    {
      for (j = 0; j < KEY_SIZE; j++)
        {
          result[j] =
            (unsigned char) (result[j] +
                             result[(j + seq[i]) % KEY_SIZE]) & 0xFF;
        }
    }

  return result;
}

ws4dResponse
ws4d_ae_compute_response (struct security_engine_s * s,
                          ws4dChallenge challenge, ws4dDerivedKey key)
{
  ws4dResponse response = (ws4dResponse) malloc (CHALLENGE_SIZE + 1);

  if (ws4d_ae_tripple_des (challenge, response, key, DES_ENCRYPT))
    {
      free (response);
      return NULL;
    }

  return response;
}

int
ws4d_ae_verify_response (struct security_engine_s *s,
                         ws4dChallenge challenge,
                         ws4dResponse response, ws4dDerivedKey key)
{
  int i;
  ws4dChallenge plainResponse = (ws4dChallenge) malloc (CHALLENGE_SIZE + 1);

  if (ws4d_ae_tripple_des (response, plainResponse, key, DES_DECRYPT))
    {
      free (plainResponse);
      return 1;
    }

  return memcmp ((void *) challenge, plainResponse, CHALLENGE_SIZE);
}

void
ws4d_ae_propagate_pin (struct security_engine_s *s, ws4dPin pin)
{
  /* This is where some magic happens and a PIN is displayed in
   * a freaky screen in a car...
   */
  int i;
  printf ("Generated PIN (>>>PIN<<<):\n\t>>>");
  for (i = 0; i < PIN_SIZE; i++)
    printf ("%c", pin[i]);
  printf ("<<<\n");
}

int
ws4d_ae_tripple_des (unsigned char *input, unsigned char *output,
                     ws4dDerivedKey key, int mode)
{
  long int pl, cl;
  const_DES_cblock cb1, cb2, cb3, ivec;
  DES_key_schedule mks1, mks2, mks3;
  int i, j, err = 0;
  unsigned char sum[4];

  /* derive 3 8-byte-keys and an init-vector from 256-byte key
   * - consider 256 bytes as an (8,64) matrix
   * - divide this array into 4 8*8 arrays
   * - sum up each column (8 bytes) % 256 to one byte of key*/

  for (i = 0; i < 8; i++)
    {
      sum[0] = sum[1] = sum[2] = sum[3] = 0;
      for (j = 0; j < 8; j++)
        {
          sum[0] = (unsigned char) ((sum[0] + key[8 * j + i]) % 0xFF);
          sum[1] = (unsigned char) ((sum[1] + key[8 * j + 64 + i]) % 0xFF);
          sum[2] = (unsigned char) ((sum[2] + key[8 * j + 128 + i]) % 0xFF);
          sum[3] = (unsigned char) ((sum[3] + key[8 * j + 192 + i]) % 0xFF);
        }
      cb1[i] = sum[0];
      cb2[i] = sum[1];
      cb3[i] = sum[2];
      ivec[i] = sum[3];
    }

  /* DES-Keys need odd parity - set the LSB accordingly */
  DES_set_odd_parity (&cb1);
  DES_set_odd_parity (&cb2);
  DES_set_odd_parity (&cb3);

  /* convert plain keys into structured ones (this implementation
   * prefers them structured). At the same time, check their parity
   * as well as their strength   */
  err += 1 * DES_set_key_checked (&cb1, &mks1);
  err += 4 * DES_set_key_checked (&cb2, &mks2);
  err += 16 * DES_set_key_checked (&cb3, &mks3);

  if (err)
    {
      fprintf (stderr,
               "Something not okay with derived keys! Error-Bit-Field: %d\n",
               err);
      return err;
    }

  DES_ede3_cbc_encrypt ((const unsigned char *) input,
                        (unsigned char *) output,
                        CHALLENGE_SIZE, &mks1, &mks2, &mks3, &ivec, mode);

  return 0;
}

int
ws4d_ae_check_client_authentication (struct security_engine_s *s,
                                     ws4dX509 cert)
{
  CertStorage *cert_entry;
  cert_entry = s->cdb_lookup_certificate (s, cert);
  /* 1st of all - does this Certificate occur in our database..? */
  if (!cert_entry)
    {
      return ERR_AUTHENTICAT;
    }
  /* 2nd - check, whether ack-flag is set */
  if (cert_entry->ack == 0)
    {
      /* though we know this certificate, authentication was not completed */
      return ERR_AUTHENTICAT;
    }
  return ERR_OK;
}

int
ws4d_ae_check_device_authentication (struct security_engine_s *s,
                                     char *uri, ws4dX509 cert)
{
  struct _cert_list_elem *pos, *next;
  if (uri)
    {                           /* test on uri */
      ws4d_list_foreach (pos, next, &(s->cert_list_head), CertStorage, list)
      {
        if (!strcmp (pos->uri, uri))
          return ERR_OK;
      }
    }
  else if (cert)
    {                           /* test on cert */
      ws4d_list_foreach (pos, next, &(s->cert_list_head), CertStorage, list)
      {
        if (!X509_cmp ((const ws4dX509) pos->cert, (const ws4dX509) cert))
          return ERR_OK;
      }
    }
  return ERR_AUTHENTICAT;
}

ws4dPin
ws4d_ae_retrieve_pin (struct security_engine_s * s, ws4dPin pin)
{
  unsigned char pin_str[256];
  int i;
  if (pin == NULL)
    pin = (ws4dPin) malloc (PIN_SIZE * sizeof (char));
  printf ("Okay, need pin!\n > ");
  /* TODO: Ask for pin safely */
  scanf ("%s", pin_str);
  for (i = 0; i < PIN_SIZE; i++)
    printf ("%c", (pin[i] = pin_str[i]));
  printf ("\n");
  return pin;
}
