/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "acs1.nsmap"
#include "dpws_device.h"
#include "acs_metadata.h"
#include "acs_wsdl.h"
#include "smdevp.h"
#include "wsseapi.h"
#include "config.h"
#include <signal.h>

#include "ws4d_scopelist.h"
#include "authentication_service.h"
#include "security_engine.h"

struct soap service;
struct soap hosting_service;
struct soap authentication_service;
struct dpws_s device;

#define USAGE printf("\nusage:\n"); \
        printf("\t%s -i device's IP -u device's UUID\n", orig_argv_0); \
        printf("\tUUID in form of urn:uuid:[uuid]\n"); \
        printf("\tYou might want to use the tool uuidgen to obtain a uuid\n"); \
        printf("Exmaple:\n\t%s -i 192.168.1.1 -u urn:uuid:`uuidgen`\n\n", orig_argv_0);

/* forward declarations for security-shell-related init-functions */
EXTERN_INIT_TABLE ("ws4d")
     struct security_engine_s sec_engine;       /* security engine plugin-socket */

     void service_exit ()
{
#ifdef DEBUG
  printf ("\nAirconditioner: shutting down...\n");
#endif

  dpws_deactivate_hosting_service (&device);
  soap_done (&service);
  soap_done (&hosting_service);
  soap_done (&authentication_service);
  dpws_done (&device);

  exit (0);
}

int
main (int argc, char **argv)
{
#ifndef WIN32
  struct sigaction sa;
#endif
  const char *orig_argv_0 = argv[0];
  const char *interf = NULL;
  const char *uuid = NULL;
  const char *scope = NULL;
  struct ws4d_list_node scopelist;
  struct ws4d_abs_allocator alist;

  const char *ac_service_uri = (const char *) NULL;

  WS4D_ALLOCLIST_INIT (&alist);
  ws4d_sl_init (&scopelist);

  /* parsing command line options */
  while (argc > 1)
    {
      if (argv[1][0] == '-')
        {
          char *option = &argv[1][1];

          switch (option[0])
            {
            case 'i':          /* set interf with option -i */
              if (strlen (option) > 2)
                {
                  ++option;
                  interf = option;
                }
              else
                {
                  --argc;
                  ++argv;
                  interf = argv[1];
                }
#ifdef DEBUG
              printf ("\nAirconditioner: Set interface to \"%s\"\n", interf);
#endif
              break;
            case 'u':          /* set id with option -u */
              if (strlen (option) > 2)
                {
                  ++option;
                  uuid = option;
                }
              else
                {
                  --argc;
                  ++argv;
                  uuid = argv[1];
                }
#ifdef DEBUG
              printf ("\nAirconditioner: Set uuid to \"%s\"\n", uuid);
#endif
              break;
            case 's':
              if (strlen (option) > 2)
                {
                  ++option;
                  scope = option;
                }
              else
                {
                  --argc;
                  ++argv;
                  scope = argv[1];
                }
#ifdef DEBUG
              printf ("\nAirconditioner: Adding scope \"%s\"\n", scope);
#endif
              ws4d_sl_add (&scopelist, scope, &alist);
              break;
            default:
              fprintf (stderr, "\nAirconditioner: Bad option %s\n", argv[1]);
              USAGE exit (1);
            }
        }
      --argc;
      ++argv;
    }

  if (interf == NULL)
    {
      fprintf (stderr,
               "\nAirconditioner: No interface address was specified!\n");
      fprintf (stderr, "Expecting device's IP address\n");
      USAGE exit (1);
    }

  /* initialize soap handle */
  soap_init (&service);
  soap_init (&hosting_service);
  soap_init (&authentication_service);
#ifdef DEBUG
  soap_set_omode (&service, SOAP_XML_INDENT);
  soap_set_omode (&hosting_service, SOAP_XML_INDENT);
  soap_set_omode (&authentication_service, SOAP_XML_INDENT);
#endif
  soap_set_namespaces (&service, acs1_namespaces);
  soap_set_namespaces (&hosting_service, acs1_namespaces);
  wsa_register_handle (&hosting_service);

  /******************************/
  /* set up security sec_engine */
  /******************************/
  security_engine_init (&sec_engine);
  /* register ca-, cert- and keyfile */
  sec_engine.cafile = SECSHELLPATH "/certs/demoCA/cacert.pem";
  sec_engine.certfile = SECSHELLPATH "/certs/myDeviceCert.pem";
  sec_engine.keyfile = SECSHELLPATH "/certs/myDeviceKey.pem";

  /* password to access private key(s) */
  sec_engine.keypassword = "password";

  /* register cert_db-location */
  sec_engine.cert_list_location = "device.cdb";

  /* forward declarations are created by EXTERN_INIT_TABLE("ws4d") */
  /* register authentication engine */
  ws4d_init_authentication_engine (&sec_engine);
  /* register certificate database */
  ws4d_init_certificate_db (&sec_engine);
  /* register authorization-engine and -database */
  ws4d_init_authorization_engine (&sec_engine);
  ws4d_init_authorization_db (&sec_engine);

  /* register security-handler-plugin to soap handle - besides, this way,
   * a security engine is associated with a soap handle */
  soap_register_plugin_arg (&service, sh_plugin, &sec_engine);
  soap_register_plugin_arg (&authentication_service, sh_plugin, &sec_engine);

  /* SetUp and configure WSSE-Plugin for the service-handle, this must be done
   * AFTER registering the certificate database, since this registers the
   * callback to handle binary security tokens, passed as a parameter in
   * soap_register_plugin_arg()*/

  soap_register_plugin_arg (&service, soap_wsse,
                            sec_engine.cdb_wsse_token_handler);
  service.cafile = sec_engine.cafile;

  /* wsse plugin for authentication service is registered automatically if you didn't do it */

  /* activate automatic verification of signatures for hosted service */
  soap_wsse_verify_init (&service);
  soap_wsse_verify_auto (&service, SOAP_SMD_NONE, NULL, 0);

  /* eventually, set configuration so that only authenticated and
   * authorized clients may access the temperatur service */
  sh_insist_on_authentication (&service, 1);
  sec_engine.authe_activate_authorization (&service);

  /* initialize device and services */
  if (dpws_init (&device, interf))
    {
      fprintf (stderr,
               "Can't init device. Something wrong with interface address\n");
      USAGE dpws_done (&device);
      exit (1);
    }
  if (acs_setup_HostingService (&device, &hosting_service, uuid, 100))
    {
      fprintf (stderr,
               "Can't init service. Something wrong with interface uuid\n");
      USAGE dpws_done (&device);
      exit (1);
    }
  if (acs_setup_AirConditioner (&device, &service, AIRCONDITIONER_WSDL, 100))
    {
      fprintf (stderr, "\nAirconditioner: Can't init device and services\n");
      USAGE dpws_done (&device);
      exit (1);
    }
  /* register and Authentication Service */
  if (setup_AuthenticationService
      (&device, &authentication_service, AUTHENTICATIONSERVICE_WSDL, 100))
    {
      fprintf (stderr,
               "\nAuthentication Service: Error in setting up Service!\n");
      dpws_done (&device);
      return 1;
    }

  /* Configure authorization :
   * This is where you can add hard-coded preset authorization decisions.
   * The only decision made here is that the AirConditioner's user is
   * allowed to decide whether requests are authorized or not
   *
   * However, instead of restricting access on SERVICE level, access
   * is restricted on METHOD level */
  sec_engine.authe_set_dynamic_authorization (&sec_engine,
                                              (void *)
                                              "http://www.ws4d.org/axis2/tutorial/AirConditioner/GetStatusIn",
                                              PERMITTED, METHOD);
  sec_engine.authe_set_dynamic_authorization (&sec_engine,
                                              (void *)
                                              "http://www.ws4d.org/axis2/tutorial/AirConditioner/SetTargetTemperatureIn",
                                              PERMITTED, METHOD);

  /* Initialization and configuration of the security engine are done now. See
   * the service's methods' implementation for the rest
   */

  /* Set Metadata */
  acs_set_Metadata (&device);
  acs_set_wsdl (&device);

  /* Set Scopes */
  if (!ws4d_sl_isempty (&scopelist))
    {
      dpws_add_scope (&device, ws4d_sl_tostr (&scopelist, &alist));
    }

  /* Update Metadata */
  if (dpws_update_Metadata (&device))
    {
      fprintf (stderr, "\nAirconditioner: Can't init metadata\n");
      dpws_done (&device);
      exit (1);
    }

  /* install signal handler for SIGINT or Ctrl-C */
#ifdef WIN32
  signal (SIGINT, service_exit);
#else
  memset (&sa, 0, sizeof (sa));
  sa.sa_handler = service_exit;
  sigaction (SIGINT, &sa, NULL);
#endif

  /* Tell hosting service to start advertising its hosted services */
  if (dpws_activate_hosting_service (&device))
    {
      fprintf (stderr, "\nAirconditioner: Can't activate device\n");
      dpws_done (&device);
      exit (1);
    }

  ws4d_sl_done (&scopelist);
  ws4d_alloclist_done (&alist);

#ifdef DEBUG
  printf ("\nAirconditioner: ready to serve... (Ctrl-C for shut down)\n");
#endif

  for (;;)
    {
      struct soap *handle = NULL, *soap_set[] =
        SOAP_HANDLE_SET (&service, &hosting_service, &authentication_service);
      int (*serve_requests[]) (struct soap * soap) =
        SOAP_SERVE_SET (acs1_serve_request, auths_serve_request);

#ifdef DEBUG
      printf ("\nAirconditioner: waiting for request\n");
#endif

      /* waiting for new messages */
      handle = dpws_maccept (&device, 100000, 3, soap_set);

      if (handle)
        {

#ifdef DEBUG
          printf ("\nAirconditioner: processing request from %s:%d\n",
                  inet_ntoa (handle->peer.sin_addr),
                  ntohs (handle->peer.sin_port));
#endif

          /* dispatch messages */
          if (dpws_mserve (handle, 2, serve_requests))
            {
              soap_print_fault (handle, stderr);
            }

          /* clean up soaps internaly allocated memory */
          soap_end (handle);
        }
    }

  return -1;
}

int temp = 23;

/* service's methods' implementation - most of the signature-related code
 * is supposed to be integrated into the security handler (compare with
 * authentication and authorization - there are no explicit tests on that here
 * because this is handled in the background by the security shell).
 */

int
__acs1__GetStatus (struct soap *soap,
                   struct _acs1__EmptyMessage *acs1__GetStatus,
                   struct acs1__ACStateType *acs1__ACState)
{
  /* fill response message */
  acs1__ACState->CurrentTemp = temp;
  acs1__ACState->TargetTemp = temp;

  int err;
  ws4dCertRef client_cert_ref = NULL;
  static EVP_PKEY *rsa_private_key = (EVP_PKEY *) NULL;
  FILE *fd;

  struct sh_plugin_data *data =
    (struct sh_plugin_data *) soap_lookup_plugin (soap, SH_PLUGIN_ID);

  if (soap_wsse_verify_body (soap))
    {
      fprintf (stderr, "Signature does not include the body\n");
      soap_wsse_delete_Security (soap);
      return soap_sender_fault (soap, "Service operation not signed", NULL);
#ifdef DEBUG
    }
  else
    {
      printf ("Signature includes body - all fine.\n");
#endif // DEBUG
    }

  if (rsa_private_key == NULL)
    {
      /* reading in private key */
      if ((fd = fopen (data->sec_engine->keyfile, "r")) == 0)
        {
          fprintf (stderr, "ERROR: pem file not found\n");
          return 0;
        }
      rsa_private_key =
        PEM_read_PrivateKey (fd, NULL, NULL,
                             (void *) data->sec_engine->keypassword);
      fclose (fd);
      if (!rsa_private_key)
        {
          fprintf (stderr, "ERROR: reading private key failed\n");
          return 0;
        }
    }

  client_cert_ref =
    (char *) soap_wsse_get_KeyInfo_SecurityTokenReferenceURI (soap);

  /* create response header */
  if (err =
      dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                "http://www.ws4d.org/axis2/tutorial/AirConditioner/GetStatusOut",
                                wsa_header_get_MessageId (soap),
                                sizeof (struct SOAP_ENV__Header)))
    {
      return err;
    }
  /* DON'T add cert to security header but use reference */
  /* reference the cert for signing */
  if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509
      (soap, client_cert_ref))
    {
      printf ("ERROR: add reference failed\n");
      return SOAP_ERR;
    }

  /* sign the msg */
  if (soap_wsse_sign_body (soap, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
    {
      printf ("sign body failed\n");
      return SOAP_ERR;
    }

  return SOAP_OK;
}


int
__acs1__SetTargetTemperature (struct soap *soap,
                              int acs1__TargetTemperature,
                              struct acs1__ACStateType *acs1__ACState)
{
  /* process request message */
  temp = acs1__TargetTemperature;

  /* fill response message */
  acs1__ACState->CurrentTemp = temp;
  acs1__ACState->TargetTemp = temp;
  int err;
  ws4dCertRef client_cert_ref = NULL;
  static EVP_PKEY *rsa_private_key = (EVP_PKEY *) NULL;
  FILE *fd;

  struct sh_plugin_data *data =
    (struct sh_plugin_data *) soap_lookup_plugin (soap, SH_PLUGIN_ID);

  if (soap_wsse_verify_body (soap))
    {
      fprintf (stderr, "Signature does not include the body\n");
      soap_wsse_delete_Security (soap);
      return soap_sender_fault (soap, "Service operation not signed", NULL);
#ifdef DEBUG
    }
  else
    {
      printf ("Signature includes body - all fine.\n");
#endif // DEBUG
    }

  if (rsa_private_key == NULL)
    {
      /* reading in private key */
      if ((fd = fopen (data->sec_engine->keyfile, "r")) == 0)
        {
          fprintf (stderr, "ERROR: pem file not found\n");
          return 0;
        }
      rsa_private_key =
        PEM_read_PrivateKey (fd, NULL, NULL,
                             (void *) data->sec_engine->keypassword);
      fclose (fd);
      if (!rsa_private_key)
        {
          fprintf (stderr, "ERROR: reading private key failed\n");
          return 0;
        }
    }

  client_cert_ref =
    (char *) soap_wsse_get_KeyInfo_SecurityTokenReferenceURI (soap);

  /* create response header */
  if (err =
      dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                "http://www.ws4d.org/axis2/tutorial/AirConditioner/SetTargetTemperatureOut",
                                wsa_header_get_MessageId (soap),
                                sizeof (struct SOAP_ENV__Header)))
    {
      return err;
    }
  /* DON'T add cert to security header but use reference */
  /* reference the cert for signing */
  if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509
      (soap, client_cert_ref))
    {
      printf ("ERROR: add reference failed\n");
      return SOAP_ERR;
    }

  /* sign the msg */
  if (soap_wsse_sign_body (soap, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
    {
      printf ("sign body failed\n");
      return SOAP_ERR;
    }

  return SOAP_OK;
}
