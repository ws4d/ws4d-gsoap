// $Id$

// NB Library Definitions
#include "predef.h"

// C Standard Library
#include <stdio.h>
#include <string.h>

// Portability & uCos Definitions
#include <includes.h>

// NB Runtime Libraries
#include <constants.h>
#include <utils.h>
#include <buffers.h>
#include <system.h>

// NB Network Stack
#include <arp.h>
#include <ip.h>
#include <tcp.h>
#include <netinterface.h>

#include <dhcpclient.h>
#include <autoupdate.h>
#include <taskmon.h>
#include <smarttrap.h>
#include <ethernet.h>

#include "pnp.h"

// declaration from ip.cpp
extern OS_FIFO ipRxFifo;
void IPTask (void *p);
extern DWORD IPStk[IP_STK_SIZE];
extern OS_FIFO *IcmpFifo;
void DoIPPacket (PoolPtr pp, PEFRAME pF, WORD csum);
extern "C"
{
#if 0
}
#endif
static DhcpObject *dhcp = NULL;
void
dhcp_start ()
{
  if (!dhcp)
    {
      // first time dhcp has to start
      dhcp = new DhcpObject (GetFirstInterface ());
    }
  dhcp->StartDHCP ();
}

void
dhcp_stop ()
{
  if (dhcp)
    {
      dhcp->StopDHCP ();
    }
}

int
LinkUp ()
{
  return EtherLink ();
}

IPADDR GetIP ()
{
  return InterfaceIP (GetFirstInterface ());
}

void
myInit ()
{
  // Code from InitializeStack()
  bTTL_Default = 60;        // probably the TTL for IP packets
  wArpLifetime = 120;       // Default ARP aging value
  pPromisciousPacketFunc = NULL;
  InitBuffers ();           // init onetime system buffers
  InitializeArp ();         // init the ARP table
  InitTCP ();               // init TCP socket structs and creates TCPD
  OSFifoInit (&ipRxFifo);
  OSTaskCreatewName (IPTask,  // sleeps for 0.5 secs
      (void *) 0,      // data
      (void *) &IPStk[IP_STK_SIZE],
      (void *) IPStk,      IP_PRIO, "IP");
  IcmpFifo = NULL;

  // Don't know how often to call this function.
  // The difference is only the call to EtherInit() in _DEBUG mode
  InitializeNetwork (&DoIPPacket, &processArp);

  // End of modified InitializeStack();

  OSChangePrio (MAIN_PRIO);
  EnableAutoUpdate ();
  EnableTaskMonitor ();
#ifndef _DEBUG
  EnableSmartTraps ();
#endif
}

// This functions creates a unique UUID from the MAC address with
// a constant prefix since it is not so easy to get the current time.
void
getDeviceUUID (char *out)
{
  MACADR mac = InterfaceMAC (GetFirstInterface ());
  // HACK: create a device UUID from the MAC address,
  // similar to UUID version 1. Instead of using the current time
  // currently a constant is used.
  sprintf (out, "urn:uuid:1d472890-5115-11df-9879-%04x%04x%04x",
           mac.phywadr[0], mac.phywadr[1], mac.phywadr[2]);
}

#if 0
extern "C"
{
#endif
}
