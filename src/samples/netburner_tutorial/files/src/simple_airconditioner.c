/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "acs1.nsmap"
#include "dpws_device.h"
#include "acs_metadata.h"
#include "acs_wsdl.h"
#include "config.h"
#include "event.h"

#include "ws-discovery.h"
#include "ws4d_misc.h"

#include "pnp.h"

#include <constants.h>
#include <utils.h>
#include <ucos.h>

struct soap service, discovery;
struct dpws_s device;

// Temperature
const long TEMP_EVENT_INT = 5000;       // 5 secs

enum ac_states
{ OFF, IDLE, COOLING };         /* Supported states of the air conditioner. */
int currentState = OFF;         /* Initial state is turned off. */
int currentTemp = 24;           /* Initial temperature in degrees celsius. */
int targetTemp = 24;            /* Initial target temperature in degrees celsius. */

// NetBurner
void UserMain (void *pd);
const char *AppName = "ws4d-gsoap pre-0.9";

#define DPWS_TASK_STK_SIZE (8*USER_TASK_STK_SIZE)
// Allocate task stack for DPWS task
// Make sure they're 4 byte aligned to keep the Coldfire happy
asm (" .align 4 ");
DWORD DpwsTaskStack[DPWS_TASK_STK_SIZE] __attribute__ ((aligned (4)));
void DpwsServerTask (void *pd);


// status variables to track state of Ethernet link
enum eth_mon_state
{
  UNKNOWN = 0,
  OFFLINE,
  NO_IP,
  DYNAMIC_IP,
  STATIC_IP,
};


int
device_start (IPADDR ip)
{
  char interf[INET_ADDRSTRLEN];
  char uuid[WS4D_UUID_SCHEMA_SIZE];

  PBYTE ipb = (PBYTE) & ip;
  snprintf (interf, sizeof (interf), "%d.%d.%d.%d",
            (int) ipb[0], (int) ipb[1], (int) ipb[2], (int) ipb[3]);
#ifdef DEBUG
  printf ("Airconditioner: Set interface to \"%s\"\n", interf);
#endif

  getDeviceUUID ((char *) &uuid);
#ifdef DEBUG
  printf ("Airconditioner: Set uuid to \"%s\"\n", uuid);
#endif

  /* initialize soap handle */
  soap_init (&discovery);
  gsoap_nb_register_handle (&discovery);

  soap_init (&service);
  gsoap_nb_register_handle (&service);
#ifdef DEBUG
  soap_set_omode (&service, SOAP_XML_INDENT);
#endif
  soap_set_namespaces (&service, acs1_namespaces);

  /* initialize device and services */
  if (dpws_init (&device, interf))
    {
      fprintf (stderr,
               "Can't init device. Something wrong with interface address\n");
      dpws_done (&device);
      return WS4D_ERR;
    }
  if (dpws_init_target (&device, &discovery, 100))
    {
      fprintf (stderr, "discovery init failed\n");
      return WS4D_ERR;
    }
  if (acs_setup_HostingService (&device, &service, uuid, 100))
    {
      fprintf (stderr,
               "Can't init service. Something wrong with interface uuid\n");
      dpws_done (&device);
      return WS4D_ERR;
    }
  if (acs_setup_AirConditioner (&device, &service, AIRCONDITIONER_WSDL, 100))
    {
      fprintf (stderr, "\nAirconditioner: Can't init device and services\n");
      dpws_done (&device);
      return WS4D_ERR;
    }

  /* Set Metadata */
  acs_set_Metadata (&device);
  acs_set_wsdl (&device);

  /* Update Metadata */
  if (dpws_update_Metadata (&device))
    {
      fprintf (stderr, "\nAirconditioner: Can't init metadata\n");
      dpws_done (&device);
      return WS4D_ERR;
    }

  /* activate eventing. */
  if (dpws_activate_eventsource (&device, &service))
    {
      printf ("\nAirconditioner: Can't activate eventing\n");
      dpws_done (&device);
      return WS4D_ERR;
    }

  /* Tell hosting service to start advertising its hosted services */
  if (dpws_activate_hosting_service (&device))
    {
      fprintf (stderr, "\nAirconditioner: Can't activate device\n");
      dpws_done (&device);
      return WS4D_ERR;
    }

#ifdef DEBUG
  printf ("Airconditioner: ready to serve...\n");
#endif
  return WS4D_OK;
}

void
device_stop ()
{
#ifdef DEBUG
  iprintf ("Shutting down ...\n");
#endif
  dpws_deactivate_hosting_service (&device);
  soap_done (&service);
  dpws_done (&device);
}


void
DpwsServerTask (void *pd)
{
  enum eth_mon_state state = UNKNOWN;
  IPADDR ip = 0;
  int link_timeout_ms = 1000;
  int next_event, timeout_ms, next_link;

  struct soap *soap_set[] = SOAP_HANDLE_SET (&service);
  int (*serve_requests[]) (struct soap * soap) =
    SOAP_SERVE_SET (acs1_serve_request);

  next_event = ws4d_systime_ms () + TEMP_EVENT_INT;
  next_link = ws4d_systime_ms ();

  while (1)
    {
      int current_time = ws4d_systime_ms ();

      // check IP status
      if (next_link <= current_time)
        {
          link_timeout_ms = 1000;
          if (LinkUp ())
            {
              switch (state)
                {
                  IPADDR n_ip;
                case OFFLINE:
                case UNKNOWN:
                  // Link activated
                  n_ip = GetIP ();      // get configured IP
                  if (n_ip)
                    {
                      state = STATIC_IP;
#ifdef DEBUG
                      iprintf ("%06d: Static IP: ", ws4d_systime_ms ());
                      ShowIP (n_ip);
                      iprintf ("\n");
#endif
                      if (ip)
                        device_stop ();
                      ip = n_ip;
                      device_start (ip);
                    }
                  else
                    {           // we are in dynamic IP mode
#ifdef DEBUG
                      iprintf ("%06d: Starting DHCP ...\n",
                               ws4d_systime_ms ());
#endif
                      state = NO_IP;
                      dhcp_start ();
                      link_timeout_ms = 500;    // speed up next check
                    }
                  break;
                case NO_IP:
                  n_ip = GetIP ();
                  if (n_ip)
                    {
                      state = DYNAMIC_IP;
#ifdef DEBUG
                      iprintf ("%06d: Dynamic IP: ", ws4d_systime_ms ());
                      ShowIP (n_ip);
                      iprintf ("\n");
#endif
                      if (n_ip != ip)
                        {
                          if (ip)
                            device_stop ();
                          ip = n_ip;
                          device_start (ip);
                        }
                    }
                  else
                    {
                      link_timeout_ms = 500;    // still no IP, speed up next check
                    }
                  break;
                case STATIC_IP:
                  break;
                default:
                  break;
                }
            }
          else
            {                   // link disappeared
              if (state == OFFLINE)
                {
                  // still offline, speed up
                  link_timeout_ms = 500;
                }
              else
                {
#ifdef DEBUG
                  iprintf ("%06d: Link down.\n", ws4d_systime_ms ());
#endif
                  state = OFFLINE;
                  dhcp_stop ();
                }
            }
          next_link += link_timeout_ms;
        }                       // check IP status

      // check event timeout
      if (next_event <= current_time)
        {
          if (targetTemp < currentTemp)
            {
              if (currentState != COOLING)
                printf
                  ("Airconditioner: Current Temperature %d; Switching to state COOLING.\n",
                   currentTemp);
              else
                printf ("Airconditioner: Cooling ...\n");
              currentState = COOLING;

              currentTemp--;
            }
          else
            {
              if (currentState != IDLE)
                {
                  printf
                    ("Airconditioner: Reached target temperature: %d; Switching to state IDLE.\n",
                     targetTemp);
                  currentState = IDLE;

                  if (state == DYNAMIC_IP || state == STATIC_IP)
                    deliver_event (currentTemp, targetTemp);
                }
              currentTemp += 2;
            }
          next_event += TEMP_EVENT_INT;
        }                       // temperature event

      // next link/event in ...
      timeout_ms =
        (next_event <
         next_link ? next_event : next_link) - ws4d_systime_ms ();

      if ((state == DYNAMIC_IP || state == STATIC_IP) && (timeout_ms > 0))
        {                       // time to handle requests?
          struct soap *handle = NULL;
#ifdef DEBUG
          printf ("Airconditioner: waiting for request\n");
#endif
          handle = dpws_maccept (&device, timeout_ms, 1, soap_set);
          if (handle)
            {
#ifdef DEBUG
              int port;
              struct sockaddr_in *in =
                (struct sockaddr_in *) soap_getpeer (handle);
              char straddr[INET_ADDRSTRLEN];

              ws4d_inet_ntop (AF_INET, &in->sin_addr, straddr,
                              sizeof (straddr));
              port = ntohs (in->sin_port);

              printf ("Airconditioner: processing request from %s:%d\n",
                      straddr, port);
#endif

              /* dispatch messages */
              if (dpws_mserve (handle, 1, serve_requests))
                {
                  soap_print_fault (handle, stderr);
                }

              /* clean up soaps internaly allocated memory */
              soap_end (handle);
            }                   // end handle
        }                       // dpws_maccept
      else if (timeout_ms > 0)
        {
          OSTimeDly (TICKS_PER_SECOND * timeout_ms / 1000);
        }
    }                           // main loop
}

void
UserMain (void *pd)
{
  myInit ();

#ifdef _DEBUG
  // does not work with non debug libs from netburner
  //InitializeNetworkGDB_and_Wait();
#endif

  OSTaskCreatewName (DpwsServerTask, NULL, &DpwsTaskStack[DPWS_TASK_STK_SIZE],  // stack top
                     DpwsTaskStack,     // stack bottom
                     MAIN_PRIO + 1,     // priority
                     "DPWS main loop");
}

int
__acs1__GetStatus (struct soap *soap,
                   struct _acs1__EmptyMessage *acs1__GetStatus,
                   struct acs1__ACStateType *acs1__ACState)
{
  /* fill response message */
  acs1__ACState->CurrentTemp = currentTemp;
  acs1__ACState->TargetTemp = targetTemp;

  /* create response header */
  return dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                   "http://www.ws4d.org/axis2/tutorial/AirConditioner/GetStatusOut",
                                   wsa_header_get_MessageId (soap),
                                   sizeof (struct SOAP_ENV__Header));
}


int
__acs1__SetTargetTemperature (struct soap *soap,
                              int acs1__TargetTemperature,
                              struct acs1__ACStateType *acs1__ACState)
{
  /* process request message */
  targetTemp = acs1__TargetTemperature;
  if (currentState == OFF)
    {
      currentState = IDLE;
    }

  /* fill response message */
  acs1__ACState->CurrentTemp = currentTemp;
  acs1__ACState->TargetTemp = targetTemp;

  /* create response header */
  return dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                   "http://www.ws4d.org/axis2/tutorial/AirConditioner/SetTargetTemperatureOut",
                                   wsa_header_get_MessageId (soap),
                                   sizeof (struct SOAP_ENV__Header));
}
