// $Id$

#ifndef PNP_H_
#define PNP_H_
  
#ifdef __cplusplus
extern "C"
{
#if 0
} 
#endif
#endif
 
#include <nettypes.h>
 
// This function initializes the stack for a plug and play device.
// It should the be first function to be called in UserMain().
void myInit ();

// starts the dhcp server.
void dhcp_start ();

// stops the dhcp server
void dhcp_stop ();

// return true in case of active link
int LinkUp ();

// return the current IP address
  IPADDR GetIP ();

// writes the device UUID (for example urn:uuid:1d472890-5115-11df-9879-
// 002345123456) to the string.
void getDeviceUUID (char *out);

#ifdef __cplusplus
#if 0
extern "C"
{
#endif
} 
#endif
 
#endif  /* PNP_H_ */
