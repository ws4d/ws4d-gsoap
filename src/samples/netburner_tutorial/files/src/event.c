#include "acs_inv1.nsmap"
#include "dpws_device.h"

extern struct dpws_s device;

/* NetBurner stack size is too small for soap struct */
static struct soap soap;

void
deliver_event (int CurrentTemp, int TargetTemp)
{
  int init = 0;
  struct ws4d_subscription *subs, *next;
  struct acsinv1__ACStateType event;

  dpws_for_each_subs (subs, next, &device,
                      "http://www.ws4d.org/axis2/tutorial/AirConditioner/TemperatureEventOut")
  {
    if (!init)
      {
        init = 1;
        event.CurrentTemp = CurrentTemp;
        event.TargetTemp = TargetTemp;
        soap_init (&soap);
        gsoap_nb_register_handle (&soap);
        soap_set_namespaces (&soap, acs_inv1_namespaces);
      }
    struct __acsinv1__TemperatureEvent response;
    char *deliverto = dpws_subsm_get_deliveryPush_address (&device, subs);

    if (!deliverto)
      continue;

    dpws_header_gen_oneway (&soap, NULL, deliverto,
                            "http://www.ws4d.org/axis2/tutorial/AirConditioner/TemperatureEventOut",
                            NULL, sizeof (struct SOAP_ENV__Header));

    dpws_header_gen_wseIdentifier (&soap, &device, subs);

    printf ("Sending Event to %s\n", deliverto);

    if (soap_send___acsinv1__TemperatureEvent
        (&soap, deliverto, NULL, &event))
      {
        soap_print_fault (&soap, stderr);
      }
    else
      {
        /* TODO: check for http 202 response code */
        soap_recv___acsinv1__TemperatureEvent (&soap, &response);
      }

    soap_end (&soap);
  }

  if (init)
    soap_done (&soap);
}
