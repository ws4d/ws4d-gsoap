/* describe - retrieve description metadata from dpws devices
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "dpws.nsmap"
#include "dpws_client.h"
#include "ws4d_targetcache.h"
#include "ws4d_servicecache.h"
#include "tools_config.h"

#ifdef WITH_WS4D_SECURITY_SHELL
#include "plugin/wsseapi.h"
#include "plugin/smdevp.h"
#endif

#include <signal.h>

#include "ws4d_eprllist.h"

struct dpws_s dpws;
struct soap client;

#ifdef OPENSSL_FOUND
static void
sigpipe_handle (int x)
{
  WS4D_UNUSED_PARAM (x);
}
#endif

enum operation_type
{
  none = 0,
  get_ThisDevice = 1,
  get_ThisModel = 2,
  get_Relationship = 3,
  get_WSDL = 4
};

int
main (int argc, char **argv)
{
  struct ws4d_abs_allocator alist;
  char *interf = NULL, *device = NULL, *service = NULL;
  char *keyfile = NULL, *cert = NULL, *operation = NULL;
  int ret;
  enum operation_type describe_operation = none;

  struct _wsm__Metadata metadata;

  WS4D_ALLOCLIST_INIT (&alist);

  /* parsing command line options */
  while (argc > 1)
    {
      if (argv[1][0] == '-')
        {
          char *option = &argv[1][1];
          switch (option[0])
            {
            case 'i':
              if (strlen (option) > 2)
                {
                  ++option;
                  interf = option;
                }
              else
                {
                  --argc;
                  ++argv;
                  interf = argv[1];
                }
              printf ("Using interface %s\n", interf);
              fflush (NULL);
              break;
            case 'd':
              if (strlen (option) > 2)
                {
                  ++option;
                  device = option;
                }
              else
                {
                  --argc;
                  ++argv;
                  device = argv[1];
                }
              printf ("Using device %s\n", device);
              fflush (NULL);
              break;
            case 's':
              if (strlen (option) > 2)
                {
                  ++option;
                  service = option;
                }
              else
                {
                  --argc;
                  ++argv;
                  service = argv[1];
                }
              printf ("Using service %s\n", service);
              fflush (NULL);
              break;
            case 'k':
              if (strlen (option) > 2)
                {
                  ++option;
                  keyfile = option;
                }
              else
                {
                  --argc;
                  ++argv;
                  keyfile = argv[1];
                }
              printf ("Using key file %s\n", keyfile);
              fflush (NULL);
              break;
            case 'c':
              if (strlen (option) > 2)
                {
                  ++option;
                  cert = option;
                }
              else
                {
                  --argc;
                  ++argv;
                  cert = argv[1];
                }
              printf ("Using certificate %s\n", cert);
              fflush (NULL);
              break;
            case 'o':
              if (strlen (option) > 1)
                {
                  ++option;
                  operation = option;
                }
              else
                {
                  --argc;
                  ++argv;
                  operation = argv[1];
                }
              if (operation)
                {
                  switch (operation[0])
                    {
                    case 'd':  /* get ThisDevice */
                      describe_operation = get_ThisDevice;
                      break;
                    case 'm':  /* get ThisModel */
                      describe_operation = get_ThisModel;
                      break;
                    case 'r':  /* get Relationship */
                      describe_operation = get_Relationship;
                      break;
                    case 'w':  /* get wsdl */
                      describe_operation = get_WSDL;
                      break;
                    default:
                      fprintf (stderr, "Unknown operation %1s", operation);
                      exit (1);
                    }
                }
              break;
            default:
              fprintf (stderr, "\nBad option %s", argv[1]);
              exit (1);
            }
        }
      --argc;
      ++argv;
    }

  if (interf == NULL)
    {
      fprintf (stderr, "\nNo host was specified!");
      exit (1);
    }

  if (operation == NULL)
    {
      fprintf (stderr, "\nNo operation was specified!");
      fflush (NULL);
      exit (1);
    }

  if ((device == NULL) && (service == NULL))
    {
      fprintf (stderr,
               "\nAt least device or service address must be specified!");
      fflush (NULL);
      exit (1);
    }

  /* initialize stack */
  if (dpws_init (&dpws, interf) != SOAP_OK)
    {
      fprintf (stderr, "\nCould not initialize dpws handle");
      fflush (NULL);
      dpws_done (&dpws);
      exit (1);
    }

  soap_init (&client);
#ifdef DEBUG
  soap_set_omode (&client, SOAP_XML_INDENT);
#endif

  if (device)
    {
      struct ws4d_epr device_epr;
      struct ws4d_abs_eprlist services;
      char *XAddrs = NULL;
      struct ws4d_thisDevice ThisDevice;
      struct ws4d_thisModel ThisModel;

      ws4d_epr_init (&device_epr);
      ws4d_epr_set_Addrs (&device_epr, device);

      XAddrs = (char *) dpws_resolve_addr (&dpws, &device_epr, NULL, 10000);
      if (!XAddrs)
        {
          fprintf (stderr, "\nCan't resolve device address %s\n", device);
          exit (1);
        }

      if (dpws_device_issecured (XAddrs))
        {
          /* initialize ssl */
#ifdef OPENSSL_FOUND
          soap_ssl_init ();
          signal (SIGPIPE, sigpipe_handle);

          if (soap_ssl_client_context (&client,
                                       SOAP_SSL_DEFAULT |
                                       SOAP_SSL_REQUIRE_SERVER_AUTHENTICATION
                                       | SOAP_SSL_SKIP_HOST_CHECK, keyfile,
                                       "password", cert, NULL, NULL))
            {
              soap_print_fault (&client, stderr);
              exit (1);
            }
#else
          fprintf (stderr,
                   "\nSecurity support not available! Please compile ws4d-gsoap with OpenSSL support!\n");
          exit (1);
#endif
        }

      ret =
        dpws_device_metadata_get (&dpws, &client, &device_epr, &metadata,
                                  10000);
      if (ret != SOAP_OK)
        {
          fprintf (stderr, "\nCan't get metadata of device %s\n", device);
          fflush (NULL);
          exit (1);
        }

      switch (describe_operation)
        {
        case get_ThisDevice:
          if (!dpws_metadata_getThisDevice (&metadata, &alist, &ThisDevice))
            {
              printf ("Device description:\n");
              if ((ThisDevice.__sizeFriendlyName > 0)
                  && ws4d_locstring_get (ThisDevice.FriendlyName,
                                         ThisDevice.__sizeFriendlyName, "de"))
                {
                  printf (" + FriendlyName (de): %s\n",
                          ws4d_locstring_get (ThisDevice.FriendlyName,
                                              ThisDevice.__sizeFriendlyName,
                                              "de"));
                }

              if (ThisDevice.FirmwareVersion)
                {
                  printf (" + FirmwareVersion: %s\n",
                          ThisDevice.FirmwareVersion);
                }

              if (ThisDevice.SerialNumber)
                {
                  printf (" + SerialNumber: %s\n", ThisDevice.SerialNumber);
                }
            }
          break;
        case get_ThisModel:

          if (!dpws_metadata_getThisModel (&metadata, &alist, &ThisModel))
            {
              printf ("Model description:\n");

              if ((ThisModel.__sizeManufacturer > 0)
                  && ws4d_locstring_get (ThisModel.Manufacturer,
                                         ThisModel.__sizeManufacturer, "de"))
                {
                  printf (" + Manufacturer (de): %s\n",
                          ws4d_locstring_get (ThisModel.Manufacturer,
                                              ThisModel.__sizeManufacturer,
                                              "de"));
                }

              if (ThisModel.ManufacturerUrl)
                {
                  printf (" + ManufacturerUrl: %s\n",
                          ThisModel.ManufacturerUrl);
                }

              if ((ThisModel.__sizeModelName > 0)
                  && ws4d_locstring_get (ThisModel.ModelName,
                                         ThisModel.__sizeModelName, "de"))
                {
                  printf (" + ModelName (de): %s\n",
                          ws4d_locstring_get (ThisModel.ModelName,
                                              ThisModel.__sizeModelName,
                                              "de"));
                }

              if (ThisModel.ModelNumber)
                {
                  printf (" + ModelNumber: %s\n", ThisModel.ModelNumber);
                }

              if (ThisModel.ModelUrl)
                {
                  printf (" + ModelUrl: %s\n", ThisModel.ModelUrl);
                }

            }
          break;
        case get_Relationship:
          dpws_metadata_cacheservices (&dpws, &device_epr, &metadata);

          ws4d_eprlist_init (&services, ws4d_eprllist_init, NULL);
          if (!dpws_find_cached_services
              (&dpws, &device_epr, NULL, &services))
            {
              register struct ws4d_epr *elem, *iter;

              printf ("Services:\n");

              ws4d_eprlist_foreach (elem, iter, &services)
              {
                ws4d_epr_lock (elem);
                if (ws4d_epr_isvalid (elem))
                  {
                    register struct ws4d_qname *cur, *qname_iter;

                    printf (" + %s\n", ws4d_serviceep_getid (elem));
                    printf (" \t+ Address: %s\n", ws4d_epr_get_Addrs (elem));
                    printf (" \t+ Types: \n");
                    ws4d_qnamelist_foreach (cur, qname_iter,
                                            ws4d_serviceep_gettypelist (elem))
                    {
                      printf (" \t\t+ (%s):%s\n", ws4d_qname_getNS (cur),
                              ws4d_qname_getName (cur));
                    }
                  }
                ws4d_epr_unlock (elem);
              }
            }
          ws4d_eprlist_done (&services);
          break;
        default:
          fprintf (stderr, "Operation not supported with a device");

        }

      ws4d_epr_done (&device_epr);
    }


  if (service)
    {
      char *wsdl_content;
      struct ws4d_epr service_epr;

      ws4d_epr_init (&service_epr);
      ws4d_epr_set_Addrs (&service_epr, service);

      ret =
        dpws_service_metadata_get (&dpws, &client, &service_epr, NULL, NULL,
                                   &metadata, 10000);
      if (ret != SOAP_OK)
        {
          fprintf (stderr, "\nCan't get metadata from %s\n", device);
          fflush (NULL);
          exit (1);
        }

      switch (describe_operation)
        {
        case get_WSDL:
          if (!dpws_metadata_getWSDL (&metadata, &wsdl_content))
            {
              printf ("WSDL:\n%s\n", wsdl_content);
            }
          break;
        default:
          fprintf (stderr, "Operation not supported with a service");

        }

      ws4d_epr_done (&service_epr);
    }

  fflush (NULL);
  exit (0);
}
