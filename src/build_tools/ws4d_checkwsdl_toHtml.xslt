<?xml version="1.0" encoding="UTF-8"?>

<!--
 _one line to give the program's name and a brief idea of what it does._
 Copyright (C) 2007  University of Rostock

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301 USA.
-->

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:xs="http://www.w3.org/2001/XMLSchema"
				xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
				xmlns:sch="http://www.ascc.net/xml/schematron"
				xmlns:iso="http://purl.oclc.org/dsdl/schematron"
				xmlns="http://www.w3.org/1999/xhtml">

<xsl:output encoding="UTF-8" method="html" />

<xsl:template match="/">
	<html>
		<head>
			<title><xsl:value-of select="svrl:schematron-output/@title" /></title>
		</head>
		<body>
			<h1><xsl:value-of select="svrl:schematron-output/@title" /></h1>

		<xsl:choose>
			<xsl:when test="(count(svrl:schematron-output/svrl:successful-report) + count(svrl:schematron-output/svrl:failed-assert)) != 0">
				<h3>Results: The file is Not Validated!</h3>
			</xsl:when>
			<xsl:otherwise>
				<h3>Results: The file is Validated!</h3>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:apply-templates />

		</body>
	</html>
</xsl:template>

<xsl:template match="svrl:successful-report">
<b>Description:</b> <xsl:value-of select="svrl:text" />
<br/>
</xsl:template>

<xsl:template match="svrl:failed-assert">
<b>Description:</b> <xsl:value-of select="svrl:text" />
<br/>
</xsl:template>

<xsl:template match="text()">
</xsl:template>

</xsl:stylesheet>

