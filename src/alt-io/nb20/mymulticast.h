#ifndef _multicast_h_
#define _multicast_h_


void myMultiProcessFunc (PoolPtr p, WORD csum);

void myRegisterMulticastFifo (IPADDR group, WORD dest_port, OS_FIFO * pfifo);

void myUnregisterMulticastFifo (IPADDR group, WORD destination_port);

#endif // _multicast_h_
