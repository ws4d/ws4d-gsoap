# Copyright (C) 2007  University of Rostock
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.

SET(WITH_ALT_IO true)

# This is required to enable for the NetBurner alternative I/O since it
# uses cpp files
ENABLE_LANGUAGE(CXX)

################################################################################
# netburner runtime library 2.0 io sources                                     #
################################################################################

# directory to put all generated files in
SET(gen_DIR ${CMAKE_CURRENT_BINARY_DIR}/gen)

# If the directory for the generated sources does not
# already exists, create it.
IF(NOT EXISTS ${gen_DIR})
  FILE(MAKE_DIRECTORY ${gen_DIR})
ENDIF(NOT EXISTS ${gen_DIR})

# Find original NetBurner multicast.cpp file
FIND_FILE(MULTICAST_SOURCE_FILE multicast.cpp PATHS "${NBROOT}/system/")

SET(MULTICAST_PATCH ${CMAKE_CURRENT_SOURCE_DIR}/multicast.patch)
SET(PATCHED_MULTICAST_SOURCE "${gen_DIR}/multicast.cpp")

ADD_CUSTOM_COMMAND(OUTPUT ${PATCHED_MULTICAST_SOURCE}
  COMMAND ${CMAKE_COMMAND} -E copy ${MULTICAST_SOURCE_FILE} ${gen_DIR}
  COMMAND ${GNU_PATCH_EXECUTABLE} -u -d ${gen_DIR} -i ${MULTICAST_PATCH}
  DEPENDS ${MULTICAST_SOURCE_FILE} ${MULTICAST_PATCH}
)

# required for ws4d stack
SET(DPWS_ALT_IO_INCLUDES ${CMAKE_CURRENT_SOURCE_DIR} CACHE INTERNAL "")

INCLUDE_DIRECTORIES(AFTER ${GSOAP_INCLUDE_DIR} ${DPWS_INCLUDES} ${CMAKE_CURRENT_SOURCE_DIR})

SET(nb20_io_SRCS
  gsoap-nb-io.c
  cpp_wrap.cpp
  ${PATCHED_MULTICAST_SOURCE}
)

################################################################################
# netburner io static library                                                  #
################################################################################

ADD_LIBRARY(dpwsIO STATIC ${nb20_io_SRCS})

EXTEND_TGT_COMPILE_FLAGS(dpwsIO FLAGS "-DWITH_NOIO -DWITH_SOAPDEFS_H")

IF (DPWS_COMPILE_FLAGS)
  EXTEND_TGT_COMPILE_FLAGS(dpwsIO FLAGS "${DPWS_COMPILE_FLAGS}")
ENDIF (DPWS_COMPILE_FLAGS)

INSTALL(TARGETS dpwsIO ARCHIVE DESTINATION lib/ws4d-gSOAP)

SET(ALTIO_LIBRARIES dpwsIO CACHE STRING "" FORCE)

INSTALL(FILES alt_io.h gsoap-nb-io.h soapdefs.h
  DESTINATION include/ws4d-gSOAP)

