#include "gsoap-nb-io.h"
#include "stdsoap2.h"

// NetBurner header
#include <constants.h>          // TICKS_PER_SECOND
#include <utils.h>              // AsciiToIp()
#include <iosys.h>              // close(), select()
#include <ip.h>                 // EthernetIP, ...
#include <errno.h>

#include "cpp_wrap.h"

#include "ws4d_misc.h"

// gsoap always sets the errno macros
#undef  soap_errno
#define soap_errno errno
#undef  soap_socket_errno
#define soap_socket_errno(s) errno
#undef  soap_reset_errno
#define soap_reset_errno (errno = 0)



// copy from NetBurner dns.h
#define DNS_OK (0)
#define DNS_TIMEOUT (1)
#define DNS_NOSUCHNAME (2)
#define DNS_ERR (3)

int GetHostByName (const char *name, IPADDR * pIpaddr, IPADDR dns_server,
                   WORD timeout);



// copy from NetBurner tcp.h
#define TCP_ERR_NORMAL              (0)
#define TCP_ERR_TIMEOUT             (-1)
#define TCP_ERR_NOCON               (-2)
#define TCP_ERR_CLOSING             (-3)
#define TCP_ERR_NOSUCH_SOCKET       (-4)
#define TCP_ERR_NONE_AVAIL          (-5)
#define TCP_ERR_CON_RESET           (-6)
#define TCP_ERR_CON_ABORT           (-7)

#define INADDR_ANY                  (0)




// **********************************************************************
// plugin related declarations
static const char nb_plugin_id[] = NETBURNER_PLUGIN_ID;

struct nb_plugin_data
{                               // local plugin data
  struct sockaddr_in peer;      // IPv4: set by soap_connect/soap_accept and by UDP recv
};

static int nb_plugin (struct soap *soap, struct soap_plugin *p, void *arg);
static int nb_plugin_init (struct soap *soap, struct nb_plugin_data *data);
static int nb_plugin_copy (struct soap *soap, struct soap_plugin *dst,
                           struct soap_plugin *src);
static void nb_plugin_delete (struct soap *soap, struct soap_plugin *p);


// **********************************************************************
// IO related declarations

static int fsend (struct soap *, const char *, size_t);
static size_t frecv (struct soap *, char *, size_t);

// Only for windows
//static int tcp_init(struct soap*);

static const char *tcp_error (struct soap *);

// Does not work with in_addr. Therefore soap->fresolve is not available
//static int tcp_gethost(struct soap*, const char *addr, struct in_addr *inaddr);
static int gethost (struct soap *soap, const char *host, IPADDR * addr);

static SOAP_SOCKET tcp_connect (struct soap *, const char *endpoint,
                                const char *host, int port);

// is used one time soap->faccept, accept() call different,
// so it must be rewritten in soap_accept();
//static SOAP_SOCKET tcp_accept(struct soap*, SOAP_SOCKET, struct sockaddr*, int*);

static int tcp_select (struct soap *, SOAP_SOCKET, int, int);
static int tcp_disconnect (struct soap *);
static int tcp_closesocket (struct soap *, SOAP_SOCKET);

// not needed, just use soap->fclosesocket
//static int tcp_shutdownsocket(struct soap*, SOAP_SOCKET, int);

static const char *soap_strerror (struct soap *);


#define SOAP_TCP_SELECT_RCV 0x1
#define SOAP_TCP_SELECT_SND 0x2
#define SOAP_TCP_SELECT_ERR 0x4
#define SOAP_TCP_SELECT_ALL 0x7

//#define SOAP_SOCKBLOCK(fd) fcntl(fd, F_SETFL, fcntl(fd, F_GETFL)&~O_NONBLOCK);
//#define SOAP_SOCKNONBLOCK(fd) fcntl(fd, F_SETFL, fcntl(fd, F_GETFL)|O_NONBLOCK);


// copy past from gsoap 2.7.15
#ifndef WITH_LEAN
static const struct soap_code_map h_error_codes[] = {
#ifdef HOST_NOT_FOUND
  {HOST_NOT_FOUND, "Host not found"},
#endif
#ifdef TRY_AGAIN
  {TRY_AGAIN, "Try Again"},
#endif
#ifdef NO_RECOVERY
  {NO_RECOVERY, "No Recovery"},
#endif
#ifdef NO_DATA
  {NO_DATA, "No Data"},
#endif
#ifdef NO_ADDRESS
  {NO_ADDRESS, "No Address"},
#endif
  {0, NULL}
};
#endif


// **********************************************************************
// gSOAP plugin specific function for registration, initialization, ...

// The plugin registry function, called from gsoap_nb_register_handle().
static int
nb_plugin (struct soap *soap, struct soap_plugin *p, void *arg)
{
  p->id = nb_plugin_id;
  p->fcopy = nb_plugin_copy;    /* optional: when set the plugin must copy its local data */
  p->fdelete = nb_plugin_delete;

  p->data = (void *) malloc (sizeof (struct nb_plugin_data));
  if (!(p->data))
    return SOAP_EOM;

  return nb_plugin_init (soap, (struct nb_plugin_data *) p->data);
}

static int
nb_plugin_init (struct soap *soap, struct nb_plugin_data *data)
{
  // TODO: Save old callbacks
  soap->fresolve = NULL;
  soap->fopen = tcp_connect;
  soap->fclose = tcp_disconnect;
  soap->fclosesocket = tcp_closesocket;
  soap->fsend = fsend;
  soap->frecv = frecv;
  //soap->fpoll = soap_poll;

  return SOAP_OK;
}

// copy plugin data, called by soap_copy()
// This is important: we need a deep copy to avoid data sharing by two run-time environments
static int
nb_plugin_copy (struct soap *soap, struct soap_plugin *dst,
                struct soap_plugin *src)
{
  if (!src->data)               // no data to copy
    return SOAP_OK;

  if (!
      (dst->data =
       (struct nb_plugin_data *) malloc (sizeof (struct nb_plugin_data))))
    return SOAP_EOM;
  memcpy (dst->data, src->data, sizeof (struct nb_plugin_data));
  return SOAP_OK;
}

// plugin deletion, called by soap_done()
static void
nb_plugin_delete (struct soap *soap, struct soap_plugin *p)
{
  // TODO: restore old callbacks

  if (p->data)
    free (p->data);             // free allocated plugin data
  p->data = NULL;
}

static struct nb_plugin_data *
nb_get_data (struct soap *soap)
{
  return (struct nb_plugin_data *) soap_lookup_plugin (soap,
                                                       NETBURNER_PLUGIN_ID);
}


int
gsoap_nb_register_handle (struct soap *soap)
{
  return soap_register_plugin (soap, nb_plugin);
}


// **********************************************************************
// ws4d interface function

ws4d_time
ws4d_systime_ms (void)
{
  return TimeTick * 1000 / TICKS_PER_SECOND;
}

ws4d_time
ws4d_systime_s (void)
{
  return TimeTick / TICKS_PER_SECOND;
}

void *
soap_getpeer (struct soap *soap)
{
  struct nb_plugin_data *data = nb_get_data (soap);
  return &data->peer;
}

size_t
soap_getpeerlen (struct soap * soap)
{
  return sizeof (struct sockaddr_in);
}

int
getsockname (int sockfd, struct sockaddr *addr, socklen_t * addrlen)
{
  // HACK: we always have only one interface
  struct sockaddr_in *a = (struct sockaddr_in *) addr;

  a->sin_family = AF_INET;
  a->sin_port = GetSocketLocalPort (sockfd);

  // GetSocketLocalAddr(sockfd) only works on multihomed netburner devices
  a->sin_addr.s_addr = EthernetIP;

  return SOAP_OK;
}

int
inet_pton (int af, const char *src, void *dst)
{
  IPADDR *ip;
  if (af != AF_INET)
    {
      errno = EAFNOSUPPORT;
      return -1;                // af invalid (errno = EAFNOSUPPORT)
    }

  ip = (IPADDR *) dst;
  *ip = AsciiToIp (src);
  return (*ip != INADDR_ANY);   // 1 = success, 0 = src invalid
}

const char *
inet_ntop (int af, const void *src, char *dst, socklen_t size)
{
  int s;
  PBYTE ipb;

  if (af != AF_INET)
    {
      errno = EAFNOSUPPORT;
      return NULL;
    }

  ipb = (PBYTE) src;
  s = snprintf (dst, size, "%d.%d.%d.%d",
                (int) ipb[0], (int) ipb[1], (int) ipb[2], (int) ipb[3]);
  if (s > 0)
    return dst;
  return NULL;
}

unsigned int
sleep (unsigned int seconds)
{
  OSTimeDly (seconds * TICKS_PER_SECOND);
  return 0;
}

// copy past from lwip alt-io with the following modifications
// (1) replace struct timeval with unit32_t
struct soap *
soap_maccept (ws4d_time timeout, int count, struct soap **soap_handles)
{
  uint32_t timeout_t;
  register int i, r = -1;
  SOAP_SOCKET bigfd = SOAP_INVALID_SOCKET;
  fd_set fd;
  struct soap *result = NULL;

  FD_ZERO (&fd);

  if (!soap_handles)
    return result;

  for (i = 0; i < count; i++)
    {
      if (soap_valid_socket (soap_handles[i]->master))
        {
          FD_SET ((SOAP_SOCKET) soap_handles[i]->master, &fd);
          if ((SOAP_SOCKET) soap_handles[i]->master > bigfd)
            bigfd = soap_handles[i]->master;
        }
      else
        {
          printf ("register soap handle number %d first\n", i);
          goto exit;
        }
    }

  if (timeout != DPWS_SYNC)
    {
      timeout_t = timeout * TICKS_PER_SECOND / 1000;
    }



  while (r < 0)
    {
      r = select (bigfd + 1, &fd, NULL, NULL, timeout_t);
      if (r > 0)
        {
          for (i = 0; i < count; i++)
            {
              if (soap_valid_socket (soap_handles[i]->master))
                {
                  if (FD_ISSET ((SOAP_SOCKET) soap_handles[i]->master, &fd))
                    {
                      result = soap_handles[i];
                      goto exit;
                    }
                }
            }
        }
    }

exit:

  if (result)
    soap_accept (result);

  return result;
}



int
wsd_bind_multicastudp (struct soap *soap, const char *host,
                       const char *group, short int port, int backlog)
{
  IPADDR g_ip;
  int err;

  // test parameters
  if (!soap || !group || !port)
    return SOAP_ERR;

  g_ip = AsciiToIp (group);
  if (!g_ip)
    return SOAP_ERR;

  err = wsd_bind_udp (soap, "0.0.0.0", port, backlog);
  if (err != SOAP_OK)
    return err;

  // register multicast for same fifo
  if (join_multicast (soap->master, g_ip))
    return SOAP_ERR;

  return SOAP_OK;
}



// **********************************************************************
// IO related functions

static int
gethost (struct soap *soap, const char *host, IPADDR * addr)
{
  if (!host                     // null
      || !strcmp (host, "0.0.0.0") || !strcmp (host, ""))
    {
      addr = INADDR_ANY;
      return SOAP_OK;
    }

  *addr = AsciiToIp (host);
  if (addr)
    return SOAP_OK;

  int r = GetHostByName (host, addr, INADDR_ANY, TICKS_PER_SECOND * 10);
  if (r == DNS_OK)
    return SOAP_OK;
  if (r == DNS_TIMEOUT)
    {
      iprintf ("Timeout resolving %s.\n", host);
      return SOAP_ERR;
    }
  if (r == DNS_NOSUCHNAME)
    {
      iprintf ("No such name (%s).\n", host);
      return SOAP_ERR;
    }
  iprintf ("Unknown DNS error.");
  return SOAP_ERR;
}


SOAP_SOCKET
soap_bind (struct soap * soap, const char *host, int port, int backlog)
{
  IPADDR addr = INADDR_ANY;     // 0.0.0.0 for default binding

  if (soap_valid_socket (soap->master))
    {
      soap->fclosesocket (soap, soap->master);
      soap->master = SOAP_INVALID_SOCKET;
    }
  soap->socket = SOAP_INVALID_SOCKET;
  // socket() is obsolete for netburner lib. A lot of gSOAP code is skipped

  soap->errmode = 0;
  if (host)
    {
      if (gethost (soap, host, &addr))
        {
          soap_set_receiver_error (soap, tcp_error (soap),
                                   "get host by name failed in soap_bind()",
                                   SOAP_TCP_ERROR);
          return SOAP_INVALID_SOCKET;
        }
    }

  if ((soap->omode & SOAP_IO_UDP))
    {                           // UDP
      soap->master = nb_CreateRxTxUdpSocket (addr, 0, port);
      if (soap->master < 1)
        {
          soap->errnum = soap_socket_errno (soap->master);
          soap->master = SOAP_INVALID_SOCKET;
#ifdef _DEBUG
          printf ("soap_bind(): UDP socket failed\n");
#endif
          soap_set_receiver_error (soap, "udp_error",
                                   "CreateRxTxUdpSocket failed in soap_bind()",
                                   SOAP_TCP_ERROR);
          return SOAP_INVALID_SOCKET;
        }
      return soap->socket = soap->master;
    }

  // TCP
  soap->master =
    nb_listen (INADDR_ANY, port, (backlog > 0xff ? 0xff : backlog));
  if (soap->master < 1)
    {
      soap->errnum = soap_socket_errno (soap->master);
      soap->master = SOAP_INVALID_SOCKET;
      soap_set_receiver_error (soap, "tcp_error",
                               "listen failed in soap_bind()",
                               SOAP_TCP_ERROR);
      return SOAP_INVALID_SOCKET;
    }

  // Just for TCP sockets
  if (SetSocketRxBuffers (soap->master, SOAP_BUFLEN))
    {
      soap->errnum = soap_socket_errno (soap->master);
      soap_set_receiver_error (soap, "socket_error",
                               "SetSocketRxBuffers failed in soap_bind()",
                               SOAP_TCP_ERROR);
      soap_closesock (soap);
      return SOAP_INVALID_SOCKET;
    }
  if (SetSocketTxBuffers (soap->master, SOAP_BUFLEN))
    {
      soap->errnum = soap_socket_errno (soap->master);
      soap_set_receiver_error (soap, "socket_error",
                               "SetSocketTxBuffers failed in soap_bind()",
                               SOAP_TCP_ERROR);
      soap_closesock (soap);
      return SOAP_INVALID_SOCKET;
    }
  return soap->master;
}


SOAP_SOCKET
soap_accept (struct soap * soap)
{
  // if UDP socket, return socket
  if ((soap->omode & SOAP_IO_UDP))
    return soap->socket = soap->master;

  if (!soap_valid_socket (soap->master))
    {
      soap->errnum = 0;
      soap_set_receiver_error (soap, "invalid socket",
                               "no master socket in soap_accept()",
                               SOAP_TCP_ERROR);
      return SOAP_INVALID_SOCKET;
    }
  IPADDR client_ip;
  WORD client_port;
  WORD timeout = 0;
  if (soap->accept_timeout < 0)
    {                           // value in usecs
      timeout = -(soap->accept_timeout) * TICKS_PER_SECOND / 1000000;
      if (!timeout)
        timeout = 1;            // avoid a value of 0
    }
  else
    {                           // value in secs
      if (soap->accept_timeout)
        timeout = soap->accept_timeout * TICKS_PER_SECOND;
      else
        timeout = 60 * TICKS_PER_SECOND;
    }
  int fda = accept (soap->master, &client_ip, &client_port, timeout);
  if (fda > 0)
    {
      struct nb_plugin_data *data = nb_get_data (soap);
      data->peer.sin_family = AF_INET;
      data->peer.sin_port = client_port;
      data->peer.sin_addr.s_addr = client_ip;
      soap->ip = client_ip;
      soap->port = client_port;
      return soap->socket = fda;
    }
  else
    {
      int err = soap_socket_errno (soap->socket);
      if (err != 0)
        {
          soap->errnum = err;
          soap_set_receiver_error (soap, "no accept",
                                   "accept failed in soap_accept()",
                                   SOAP_TCP_ERROR);
          soap_closesock (soap);
          return SOAP_INVALID_SOCKET;
        }
      iprintf ("should never happen. gjhgfjhsag");
      return SOAP_INVALID_SOCKET;
    }
}




// copied from gsoap 2.7.15 and removed
// (1) C++ code at the beginning, modified
// (2) sendto() twice in UDP code as well as
// (3) send() to write() for TCP code.
static int
fsend (struct soap *soap, const char *s, size_t n)
{
  register int nwritten, err;
  while (n)
    {
      if (soap_valid_socket (soap->socket))
        {
#ifndef WITH_LEAN
          if (soap->send_timeout)
            {
              for (;;)
                {
                  register int r;
#ifdef WITH_OPENSSL
                  if (soap->ssl)
                    r =
                      tcp_select (soap, soap->socket, SOAP_TCP_SELECT_ALL,
                                  soap->send_timeout);
                  else
#endif
                    r =
                      tcp_select (soap, soap->socket,
                                  SOAP_TCP_SELECT_SND | SOAP_TCP_SELECT_ERR,
                                  soap->send_timeout);
                  if (r > 0)
                    break;
                  if (!r)
                    return SOAP_EOF;
                  err = soap->errnum;
                  if (!err)
                    return soap->error;
                  if (err != SOAP_EINTR && err != SOAP_EAGAIN
                      && err != SOAP_EWOULDBLOCK)
                    return SOAP_EOF;
                }
            }
#endif
#ifdef WITH_OPENSSL
          if (soap->ssl)
            nwritten = SSL_write (soap->ssl, s, (int) n);
          else if (soap->bio)
            nwritten = BIO_write (soap->bio, s, (int) n);
          else
#endif
#ifndef WITH_LEAN
          if ((soap->omode & SOAP_IO_UDP))
            {
              // Modified for NetBurner lib
              struct sockaddr_in *sock =
                (struct sockaddr_in *) soap_getpeer (soap);
              nwritten =
                nb_sendto (soap->socket, (unsigned char *) s, n,
                           sock->sin_addr.s_addr, sock->sin_port);

              /* retry and back-off algorithm */
              /* TODO: this is not very clear from specs so verify and limit conditions under which we should loop (e.g. ENOBUFS) */
              if (nwritten < 0)
                {
                  int udp_repeat;
                  int udp_delay;
                  if ((soap->connect_flags & SO_BROADCAST))
                    udp_repeat = 3;     /* SOAP-over-UDP MULTICAST_UDP_REPEAT - 1 */
                  else
                    udp_repeat = 1;     /* SOAP-over-UDP UNICAST_UDP_REPEAT - 1 */
                  udp_delay = (soap_random % 201) + 50; /* UDP_MIN_DELAY .. UDP_MAX_DELAY */
                  do
                    {
                      tcp_select (soap, soap->socket, SOAP_TCP_SELECT_ERR,
                                  -1000 * udp_delay);
                      // Modified, sendto() in netburner lib
                      nwritten =
                        nb_sendto (soap->socket, (unsigned char *) s, n,
                                   sock->sin_addr.s_addr, sock->sin_port);
                      udp_delay <<= 1;
                      if (udp_delay > 500)      /* UDP_UPPER_DELAY */
                        udp_delay = 500;
                    }
                  while (nwritten < 0 && --udp_repeat > 0);
                }
            }                   // end udp
          else
#endif
            // modified for tcp connection
            nwritten = write (soap->socket, s, (int) n);

          if (nwritten <= 0)
            {
#if defined(WITH_OPENSSL) || !defined(WITH_LEAN)
              register int r = 0;
#endif
              err = soap_socket_errno (soap->socket);
#ifdef WITH_OPENSSL
              if (soap->ssl
                  && (r =
                      SSL_get_error (soap->ssl, nwritten)) != SSL_ERROR_NONE
                  && r != SSL_ERROR_WANT_READ && r != SSL_ERROR_WANT_WRITE)
                {
                  soap->errnum = err;
                  return SOAP_EOF;
                }
#endif
              if (err == SOAP_EWOULDBLOCK || err == SOAP_EAGAIN)
                {
#ifndef WITH_LEAN
#ifdef WITH_OPENSSL
                  if (soap->ssl && r == SSL_ERROR_WANT_READ)
                    r =
                      tcp_select (soap, soap->socket,
                                  SOAP_TCP_SELECT_RCV | SOAP_TCP_SELECT_ERR,
                                  soap->send_timeout ? soap->
                                  send_timeout : -10000);
                  else
                    r =
                      tcp_select (soap, soap->socket,
                                  SOAP_TCP_SELECT_SND | SOAP_TCP_SELECT_ERR,
                                  soap->send_timeout ? soap->
                                  send_timeout : -10000);
#else
                  r =
                    tcp_select (soap, soap->socket,
                                SOAP_TCP_SELECT_SND | SOAP_TCP_SELECT_ERR,
                                soap->send_timeout ? soap->
                                send_timeout : -10000);
#endif
                  if (!r && soap->send_timeout)
                    return SOAP_EOF;
                  if (r < 0 && soap->errnum != SOAP_EINTR)
                    return SOAP_EOF;
#endif
                }
              else if (err && err != SOAP_EINTR)
                {
                  soap->errnum = err;
                  return SOAP_EOF;
                }
              nwritten = 0;     /* and call write() again */
            }
        }
      else
        {
#ifdef WITH_FASTCGI
          nwritten = fwrite ((void *) s, 1, n, stdout);
          fflush (stdout);
#else
#ifdef UNDER_CE
          nwritten = fwrite (s, 1, n, soap->sendfd);
#else
#ifdef VXWORKS
#ifdef WMW_RPM_IO
          if (soap->rpmreqid)
            nwritten = (httpBlockPut (soap->rpmreqid, s, n) == 0) ? n : -1;
          else
#endif
            nwritten =
              fwrite (s, sizeof (char), n, fdopen (soap->sendfd, "w"));
#else
#ifdef WIN32
          nwritten = _write (soap->sendfd, s, (unsigned int) n);
#else
          nwritten = write (soap->sendfd, s, (unsigned int) n);
#endif
#endif
#endif
#endif
          if (nwritten <= 0)
            {
#ifndef WITH_FASTCGI
              err = soap_errno;
#else
              err = EOF;
#endif
              if (err && err != SOAP_EINTR && err != SOAP_EWOULDBLOCK
                  && err != SOAP_EAGAIN)
                {
                  soap->errnum = err;
                  return SOAP_EOF;
                }
              nwritten = 0;     /* and call write() again */
            }
        }
      n -= nwritten;
      s += nwritten;
    }
  return SOAP_OK;
}

// just copied from gsoap 2.7.15 with the following modifications
// (1) removed c++ code at the beginning
// (2) modified recvfrom() and
// (3) replaced the following recv() to read() for TCP
static size_t
frecv (struct soap *soap, char *s, size_t n)
{
  register int r;
#ifndef WITH_LEAN
  register int retries = 100;   /* max 100 retries with non-blocking sockets */
#endif
  soap->errnum = 0;
  if (soap_valid_socket (soap->socket))
    {
      for (;;)
        {
#ifdef WITH_OPENSSL
          register int err = 0;
#endif
#ifndef WITH_LEAN
#ifdef WITH_OPENSSL
          if (soap->recv_timeout && !soap->ssl) /* SSL: sockets are nonblocking */
#else
          if (soap->recv_timeout)
#endif
            {
              for (;;)
                {
                  r =
                    tcp_select (soap, soap->socket,
                                SOAP_TCP_SELECT_RCV | SOAP_TCP_SELECT_ERR,
                                soap->recv_timeout);
                  if (r > 0)
                    break;
                  if (!r)
                    return 0;
                  r = soap->errnum;
                  if (r != SOAP_EINTR && r != SOAP_EAGAIN
                      && r != SOAP_EWOULDBLOCK)
                    return 0;
                }
            }
#endif
#ifdef WITH_OPENSSL
          if (soap->ssl)
            {
              r = SSL_read (soap->ssl, s, (int) n);
              if (r > 0)
                return (size_t) r;
              err = SSL_get_error (soap->ssl, r);
              if (err != SSL_ERROR_NONE && err != SSL_ERROR_WANT_READ
                  && err != SSL_ERROR_WANT_WRITE)
                return 0;
            }
          else if (soap->bio)
            {
              r = BIO_read (soap->bio, s, (int) n);
              if (r > 0)
                return (size_t) r;
              return 0;
            }
          else
#endif
            {
#ifndef WITH_LEAN
              if ((soap->omode & SOAP_IO_UDP))
                {               // modified for netburner lib
                  IPADDR addr;
                  WORD port;
                  r =
                    recvfrom (soap->socket, (unsigned char *) s, n, &addr,
                              NULL, &port);
                  struct nb_plugin_data *data = nb_get_data (soap);
                  data->peer.sin_family = AF_INET;
                  data->peer.sin_port = ntohs (port);
                  data->peer.sin_addr.s_addr = ntohl (addr);
                  soap->ip = ntohl (addr);
                  soap->port = ntohs (port);
                }
              else
#endif
                {
                  // modified for tcp
                  r = read (soap->socket, s, (int) n);
                }
              if (r >= 0)
                return (size_t) r;
              r = soap_socket_errno (soap->socket);
              if (r != SOAP_EINTR && r != SOAP_EAGAIN
                  && r != SOAP_EWOULDBLOCK)
                {
                  soap->errnum = r;
                  return 0;
                }
            }
#ifndef WITH_LEAN
          {
#ifdef WITH_OPENSSL
            if (soap->ssl && err == SSL_ERROR_WANT_WRITE)
              r =
                tcp_select (soap, soap->socket,
                            SOAP_TCP_SELECT_SND | SOAP_TCP_SELECT_ERR,
                            soap->recv_timeout ? soap->recv_timeout : 5);
            else
              r =
                tcp_select (soap, soap->socket,
                            SOAP_TCP_SELECT_RCV | SOAP_TCP_SELECT_ERR,
                            soap->recv_timeout ? soap->recv_timeout : 5);
#else
            r =
              tcp_select (soap, soap->socket,
                          SOAP_TCP_SELECT_RCV | SOAP_TCP_SELECT_ERR,
                          soap->recv_timeout ? soap->recv_timeout : 5);
#endif
            if (!r && soap->recv_timeout)
              return 0;
            if (r < 0)
              {
                r = soap->errnum;
                if (r != SOAP_EINTR && r != SOAP_EAGAIN
                    && r != SOAP_EWOULDBLOCK)
                  return 0;
              }
            if (retries-- <= 0)
              return 0;
          }
#endif
#ifdef PALM
          r = soap_socket_errno (soap->socket);
          if (r != SOAP_EINTR && retries-- <= 0)
            {
              soap->errnum = r;
              return 0;
            }
#endif
        }
    }
#ifdef WITH_FASTCGI
  return fread (s, 1, n, stdin);
#else
#ifdef UNDER_CE
  return fread (s, 1, n, soap->recvfd);
#else
#ifdef WMW_RPM_IO
  if (soap->rpmreqid)
    r = httpBlockRead (soap->rpmreqid, s, n);
  else
#endif
#ifdef WIN32
    r = _read (soap->recvfd, s, (unsigned int) n);
#else
    r = read (soap->recvfd, s, (unsigned int) n);
#endif
  if (r >= 0)
    return (size_t) r;
  soap->errnum = soap_errno;
  return 0;
#endif
#endif
}


// just copy paste from gsoap-2.7.15 without modifications
static const char *
tcp_error (struct soap *soap)
{
  register const char *msg = NULL;
  switch (soap->errmode)
    {
    case 0:
      msg = soap_strerror (soap);
      break;
    case 1:
      msg = "WSAStartup failed";
      break;
    case 2:
      {
#ifndef WITH_LEAN
        msg = soap_code_str (h_error_codes, soap->errnum);
        if (!msg)
#endif
          {
            sprintf (soap->msgbuf, "TCP/UDP IP error %d", soap->errnum);
            msg = soap->msgbuf;
          }
      }
    }
  return msg;
}


// a lot of modifications from the original gsoap 2.7.15 version of the file
static SOAP_SOCKET
tcp_connect (struct soap *soap, const char *endpoint, const char *host,
             int port)
{
  IPADDR addr;
  SOAP_SOCKET fd;
  if (soap_valid_socket (soap->socket))
    soap->fclosesocket (soap, soap->socket);
  soap->socket = SOAP_INVALID_SOCKET;


  soap->errmode = 2;

  // proxy
  if (soap->proxy_host)
    {
      if (gethost (soap, soap->proxy_host, &addr))
        {
          soap_set_sender_error (soap, tcp_error (soap),
                                 "get proxy host by name failed in tcp_connect()",
                                 SOAP_TCP_ERROR);
          return SOAP_INVALID_SOCKET;
        }
      port = HTONS ((short) soap->proxy_port);
    }
  else
    {
      if (gethost (soap, host, &addr))
        {
          soap_set_sender_error (soap, tcp_error (soap),
                                 "get host by name failed in tcp_connect()",
                                 SOAP_TCP_ERROR);
          return SOAP_INVALID_SOCKET;
        }
    }


  soap->errmode = 0;

#ifndef WITH_LEAN
  if ((soap->omode & SOAP_IO_UDP))
    {
      struct sockaddr_in *sock = (struct sockaddr_in *) soap_getpeer (soap);
      sock->sin_addr.s_addr = addr;
      sock->sin_port = port;
      fd = nb_CreateRxTxUdpSocket (addr, port, 0);
      if (fd < 1)
        {
          soap->errnum = soap_socket_errno (fd);
          soap_set_sender_error (soap, "udp",
                                 "CreateRxTxUdpSocket failed in tcp_connect()",
                                 SOAP_TCP_ERROR);
          return SOAP_INVALID_SOCKET;
        }
      return fd;
    }
#endif

  unsigned long timeout;
  if (soap->connect_timeout < 0)
    {                           // value in usecs
      timeout = -(soap->connect_timeout) * TICKS_PER_SECOND / 1000000;
      if (!timeout)
        timeout = 1;            // avoid a value of 0
    }
  else
    {                           // value in secs
      timeout = soap->connect_timeout * TICKS_PER_SECOND;
    }
  fd = connect (addr, 0, port, timeout);
  if (fd == TCP_ERR_TIMEOUT)
    {
      soap_set_sender_error (soap, "Timeout",
                             "connect failed in tcp_connect()",
                             SOAP_TCP_ERROR);
      return SOAP_INVALID_SOCKET;
    }
  if (fd == TCP_ERR_NONE_AVAIL)
    {
      soap_set_sender_error (soap, "None available",
                             "connect failed in tcp_connect()",
                             SOAP_TCP_ERROR);
      return SOAP_INVALID_SOCKET;
    }
  if (fd < 1)
    {
      soap->errnum = soap_socket_errno (fd);
      soap_set_sender_error (soap, tcp_error (soap),
                             "connect failed in tcp_connect()",
                             SOAP_TCP_ERROR);
      return SOAP_INVALID_SOCKET;
    }

#ifndef WITH_LEAN
  if (SetSocketRxBuffers (fd, SOAP_BUFLEN))
    {
      soap->errnum = soap_socket_errno (fd);
      soap_set_receiver_error (soap, "",
                               "SetSocketRxBuffers failed in tcp_connect()",
                               SOAP_TCP_ERROR);
      soap_closesock (soap);
      return SOAP_INVALID_SOCKET;
    }
  if (SetSocketTxBuffers (fd, SOAP_BUFLEN))
    {
      soap->errnum = soap_socket_errno (fd);
      soap_set_receiver_error (soap, "",
                               "SetSocketTxBuffers failed in tcp_connect()",
                               SOAP_TCP_ERROR);
      soap_closesock (soap);
      return SOAP_INVALID_SOCKET;
    }
#endif

  soap->errmode = 0;
  soap->socket = fd;
  soap->imode &= ~SOAP_ENC_SSL;
  soap->omode &= ~SOAP_ENC_SSL;
  if (!soap_tag_cmp (endpoint, "https:*"))
    {
      // SSL endpoint, abort
      soap->fclosesocket (soap, fd);
      soap->error = SOAP_SSL_ERROR;
      return SOAP_INVALID_SOCKET;
    }
  return fd;
}




// just copy paste from gsoap-2.7.15 and modified the
// (1) timeout calculation and
// (2) the select() call
static int
tcp_select (struct soap *soap, SOAP_SOCKET s, int flags, int timeout)
{
  register int r;
  unsigned long tv;             // new type
  fd_set fd[3], *rfd, *sfd, *efd;
  soap->errnum = 0;
#ifndef WIN32
  /* if fd max set size exceeded, use poll() when available */
#if defined(__QNX__) || defined(QNX)    /* select() is not MT safe on some QNX */
  if (1)
#else
  if ((int) s >= (int) FD_SETSIZE)
#endif
#ifdef HAVE_POLL
    {
      struct pollfd pollfd;
      int retries = 0;
      pollfd.fd = (int) s;
      pollfd.events = 0;
      if (flags & SOAP_TCP_SELECT_RCV)
        pollfd.events |= POLLIN;
      if (flags & SOAP_TCP_SELECT_SND)
        pollfd.events |= POLLOUT;
      if (flags & SOAP_TCP_SELECT_ERR)
        pollfd.events |= POLLERR;
      if (timeout < 0)
        timeout /= -1000;       /* -usec -> ms */
      else if (timeout <= 1000000)      /* avoid overflow */
        timeout *= 1000;        /* sec -> ms */
      else
        {
          retries = timeout / 1000000;
          timeout = 1000000000;
        }
      do
        r = poll (&pollfd, 1, timeout);
      while (r == 0 && retries--);
      if (r > 0)
        {
          r = 0;
          if ((flags & SOAP_TCP_SELECT_RCV) && (pollfd.revents & POLLIN))
            r |= SOAP_TCP_SELECT_RCV;
          if ((flags & SOAP_TCP_SELECT_SND) && (pollfd.revents & POLLOUT))
            r |= SOAP_TCP_SELECT_SND;
          if ((flags & SOAP_TCP_SELECT_ERR) && (pollfd.revents & POLLERR))
            r |= SOAP_TCP_SELECT_ERR;
        }
      else if (r < 0)
        soap->errnum = soap_socket_errno (s);
      return r;
    }
#else
    {
      soap->error = SOAP_FD_EXCEEDED;
      return -1;
    }
#endif
#endif
  rfd = sfd = efd = NULL;
  if (flags & SOAP_TCP_SELECT_RCV)
    {
      rfd = &fd[0];
      FD_ZERO (rfd);
      FD_SET (s, rfd);
    }
  if (flags & SOAP_TCP_SELECT_SND)
    {
      sfd = &fd[1];
      FD_ZERO (sfd);
      FD_SET (s, sfd);
    }
  if (flags & SOAP_TCP_SELECT_ERR)
    {
      efd = &fd[2];
      FD_ZERO (efd);
      FD_SET (s, efd);
    }
  /*
   * // start old code
   * if (timeout >= 0)
   * { tv.tv_sec = timeout;
   * tv.tv_usec = 0;
   * }
   * else
   * { tv.tv_sec = -timeout / 1000000;
   * tv.tv_usec = -timeout % 1000000;
   * }
   * r = select((int)s + 1, rfd, sfd, efd, &tv);
   * // end old code
   */
  // start new code
  if (timeout < 0)
    {                           // value in usecs
      tv = -timeout * TICKS_PER_SECOND / 1000000;
      if (!tv)
        tv = 1;                 // avoid a value of 0
    }
  else
    {                           // value in secs
      tv = timeout * TICKS_PER_SECOND;
    }

  r = select ((int) s + 1, rfd, sfd, efd, tv);
  // end new code
  if (r > 0)
    {
      r = 0;
      if ((flags & SOAP_TCP_SELECT_RCV) && FD_ISSET (s, rfd))
        r |= SOAP_TCP_SELECT_RCV;
      if ((flags & SOAP_TCP_SELECT_SND) && FD_ISSET (s, sfd))
        r |= SOAP_TCP_SELECT_SND;
      if ((flags & SOAP_TCP_SELECT_ERR) && FD_ISSET (s, efd))
        r |= SOAP_TCP_SELECT_ERR;
    }
  else if (r < 0)
    soap->errnum = soap_socket_errno (s);
  return r;
}


// just copy paste from gsoap-2.7.15 and commented out the
// soap->fshutdownsocket line at the end of the function
static int
tcp_disconnect (struct soap *soap)
{
#ifdef WITH_OPENSSL
  if (soap->ssl)
    {
      int r, s = 0;
      if (soap->session)
        {
          SSL_SESSION_free (soap->session);
          soap->session = NULL;
        }
      if (*soap->host)
        {
          soap->session = SSL_get1_session (soap->ssl);
          if (soap->session)
            {
              strcpy (soap->session_host, soap->host);
              soap->session_port = soap->port;
            }
        }
      r = SSL_shutdown (soap->ssl);
      /* SSL shutdown does not work when reads are pending */
      while (SSL_want_read (soap->ssl))
        {
          SSL_read (soap->ssl, NULL, 0);
          if (soap_socket_errno (soap->socket) != SOAP_EAGAIN)
            {
              r = SSL_shutdown (soap->ssl);
              break;
            }
        }
      if (r == 0)
        {
          if (soap_valid_socket (soap->socket))
            {
              if (!soap->fshutdownsocket (soap, soap->socket, 1))
                {               /*
                                 * wait up to 10 seconds for close_notify to be sent by peer (if peer not
                                 * present, this avoids calling SSL_shutdown() which has a lengthy return
                                 * timeout)
                                 */
                  r =
                    tcp_select (soap, soap->socket,
                                SOAP_TCP_SELECT_RCV | SOAP_TCP_SELECT_ERR,
                                10);
                  if (r <= 0 && soap->errnum != SOAP_EINTR)
                    {
                      soap->errnum = 0;
                      DBGLOG (TEST,
                              SOAP_MESSAGE (fdebug, "Connection lost...\n"));
                      soap->fclosesocket (soap, soap->socket);
                      soap->socket = SOAP_INVALID_SOCKET;
                      ERR_remove_state (0);
                      return SOAP_OK;
                    }
                }
            }
        }
      if (r != 1)
        {
          s = ERR_get_error ();
          if (s)
            {
              DBGLOG (TEST,
                      SOAP_MESSAGE (fdebug, "Shutdown failed: %d\n",
                                    SSL_get_error (soap->ssl, r)));
              if (soap_valid_socket (soap->socket)
                  && !(soap->omode & SOAP_IO_UDP))
                {
                  soap->fclosesocket (soap, soap->socket);
                  soap->socket = SOAP_INVALID_SOCKET;
                }
            }
        }
      SSL_free (soap->ssl);
      soap->ssl = NULL;
      if (s)
        return SOAP_SSL_ERROR;
      ERR_remove_state (0);
    }
#endif
  if (soap_valid_socket (soap->socket) && !(soap->omode & SOAP_IO_UDP))
    {                           //soap->fshutdownsocket(soap, soap->socket, 2); // redundant
      soap->fclosesocket (soap, soap->socket);
      soap->socket = SOAP_INVALID_SOCKET;
    }
  return SOAP_OK;
}

// just copy paste from gsoap-2.7.15 without modifications
static int
tcp_closesocket (struct soap *soap, SOAP_SOCKET fd)
{
  DBGLOG (TEST, SOAP_MESSAGE (fdebug, "Close socket %d\n", (int) fd));
  return soap_closesocket (fd);
}

// just copy paste from gsoap-2.7.15 without modifications
static const char *
soap_strerror (struct soap *soap)
{
  register int err = soap->errnum;
  if (err)
    {
      return strerror (err);
    }
#ifndef WITH_LEAN
  if (soap->recv_timeout > 0)
    {
      if (soap->send_timeout > 0)
        sprintf (soap->msgbuf,
                 "Operation interrupted or timed out after %ds send or %ds receive delay",
                 soap->send_timeout, soap->recv_timeout);
      else
        sprintf (soap->msgbuf,
                 "Operation interrupted or timed out after %ds receive delay",
                 soap->recv_timeout);
      return soap->msgbuf;
    }
#endif
  return "Operation interrupted or timed out";
}

/*
SOAP_FMAC1
int
SOAP_FMAC2
soap_poll(struct soap *soap)
{
	iprintf("soap_poll(), don't know\n");
#ifndef WITH_LEAN
  register int r;
  if (soap_valid_socket(soap->socket))
  { r = tcp_select(soap, soap->socket, SOAP_TCP_SELECT_ALL, 0);
    if (r > 0 && (r & SOAP_TCP_SELECT_ERR))
      r = -1;
  }
  else if (soap_valid_socket(soap->master))
    r = tcp_select(soap, soap->master, SOAP_TCP_SELECT_SND, 0);
  else
    return SOAP_OK;
  if (r > 0)
  {
#ifdef WITH_OPENSSL
    if (soap->imode & SOAP_ENC_SSL)
    {
      if (soap_valid_socket(soap->socket)
       && (r & SOAP_TCP_SELECT_SND)
       && (!(r & SOAP_TCP_SELECT_RCV)
        || SSL_peek(soap->ssl, soap->tmpbuf, 1) > 0))
        return SOAP_OK;
    }
    else
#endif
      if (soap_valid_socket(soap->socket)
       && (r & SOAP_TCP_SELECT_SND)
       && (!(r & SOAP_TCP_SELECT_RCV)
        || dataavail(soap->socket) // recv(soap->socket, soap->tmpbuf, 1, MSG_PEEK)
              > 0))
        return SOAP_OK;
  }
  else if (r < 0)
  { if ((soap_valid_socket(soap->master) || soap_valid_socket(soap->socket)) && soap_socket_errno(soap->master) != SOAP_EINTR)
    { soap_set_receiver_error(soap, tcp_error(soap), "select failed in soap_poll()", SOAP_TCP_ERROR);
      return soap->error = SOAP_TCP_ERROR;
    }
  }
  DBGLOG(TEST,SOAP_MESSAGE(fdebug, "Polling: other end down on socket=%d select=%d\n", soap->socket, r));
  return SOAP_EOF;
#else
  return SOAP_OK;
#endif
} // soap_poll
*/
