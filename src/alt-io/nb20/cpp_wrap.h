#ifndef _cpp_wrap_h_
#define _cpp_wrap_h_

#include <nettypes.h>

#ifdef __cplusplus
extern "C"
{
#if 0
}
#endif
#endif

// wrap tcp.h
WORD
GetSocketLocalPort (int i)
{
  return _GetSocketLocalPort (i);
}

int
SetSocketRxBuffers (int fd, int n)
{
  return _SetSocketRxBuffers (fd, n);
}

int
SetSocketTxBuffers (int fd, int n)
{
  return _SetSocketTxBuffers (fd, n);
}

int
accept (int listening_socket, IPADDR * address, WORD * port, WORD timeout)
{
  return _accept (listening_socket, address, port, timeout);
}

int
connect (IPADDR ipAddress, WORD localPort, WORD remotePort, DWORD timeout)
{
  return _connect (ipAddress, localPort, remotePort, timeout);
}

int nb_listen (IPADDR addr, WORD port, BYTE maxpend);
int nb_CreateRxTxUdpSocket (IPADDR send_to_addr, WORD send_to_remote_port,
                            WORD local_port);

/**
 * According to the NetBurner "A value of 0 will automatically select a random port number."
 * Since this is NOT the case this function will create a new local port number.
 */
WORD nb_new_local_port ();

/**
 * this function implements the sendto() function with support for UDP
 * packet fragmentation. The original NetBurner function does not support
 * this. Internally it uses SendFragmentedUdpPacket() provided in the
 * NetBurner library.
 */
int nb_sendto (int sock, PBYTE what_to_send, int len_to_send, IPADDR to_addr,
               WORD remote_port);

// multicast function
int join_multicast (int fd, IPADDR g_ip);


#ifdef __cplusplus
#if 0
{
#endif
}
#endif

#endif // _cpp_wrap_h_
