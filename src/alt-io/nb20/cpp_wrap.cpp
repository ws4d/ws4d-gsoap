#include <stdio.h>
#include <constants.h>
#include <utils.h>
#include <iosys.h>
#include <ip.h> // required by tcp.h
#include <udp.h>
#include <tcp.h>
#include <iointernal.h> // GetExtraData()
#include <nettimer.h>
#include <buffers.h>

#include "mymulticast.h"

// copy from NetBurner udpsocket.cpp
struct UdpSocketDataSet {
  OS_FIFO the_fifo;
  int fd;
  IPADDR address;
  WORD rxport;
  WORD lport;
  WORD rport;
};

extern "C" {
#if 0
}
#endif
  
WORD _GetSocketLocalPort( int i ) {

  // 0 – 2 for stdin, stdout and stderr
  // 3 – 4 for the first two UART serial ports, 0 and 1.

  if (i < TCP_SOCKET_OFFSET)
    return 0;

  if (i < TCP_SOCKET_OFFSET+TCP_SOCKET_STRUCTS)
    return GetSocketLocalPort(i);

  // check udp ports
  UdpSocketDataSet * pSet=(UdpSocketDataSet *)GetExtraData(i);
  if (pSet) return pSet->rxport;

  return 0;
}

int _SetSocketRxBuffers( int fd, int n ) {
  return SetSocketRxBuffers(fd, n);
}
int _SetSocketTxBuffers( int fd, int n ) {
  return SetSocketTxBuffers(fd, n);
}

int _accept( int listening_socket, IPADDR *address, WORD *port, WORD timeout ) {
  return accept(listening_socket, address, port, timeout);
}

int _connect( IPADDR ipAddress, WORD localPort, WORD remotePort, DWORD timeout) {
  return connect(ipAddress, localPort, remotePort, timeout);
}

WORD nb_new_local_port() {
  // Hack, NetBurner functions always need a local port number
  // we are using 16369 and following
  // 0x8000 (32768) and following outgoing tcp sockets start,
  // thus we will start at the beginning

  static WORD next_port = 0;
  WORD port = 0;

  while (!port) {

    // checking lower and upper boundary
    if ((next_port < 16369) || (next_port & 0x8000))
      next_port = 16369;

    // check if port is already in use
    if (!_GetSocketLocalPort(next_port)) {
      port = next_port;
    }

    next_port++;
  }

  return port;
}

int nb_CreateRxTxUdpSocket(IPADDR send_to_addr, WORD send_to_remote_port, WORD local_port) {
  if (!local_port) local_port = nb_new_local_port();
  return CreateRxTxUdpSocket(send_to_addr, send_to_remote_port, local_port);
}

int nb_listen(IPADDR addr, WORD port, BYTE maxpend) {
  if (!port) port = nb_new_local_port();
  return listen(addr, port, maxpend);
}

int nb_sendto(int sock, PBYTE what_to_send, int len_to_send, IPADDR to_addr, WORD remote_port) {
  UdpSocketDataSet * pSet=(UdpSocketDataSet *)GetExtraData(sock);
  if (pSet==NULL) return UDP_ERR_NOSUCH_SOCKET;
  if (pSet->lport==0) return UDP_ERR_NOTOPEN_TO_WRITE;
  return SendFragmentedUdpPacket(to_addr, pSet->lport, remote_port, what_to_send, len_to_send);
}

// ***********************************************************************
// Multicast code


struct UDP_Fifo_rec
{
   OS_FIFO *pfifo;
   udp_data_notify* pNotify;
   WORD dest_port;
};

extern UDP_Fifo_rec udp_fifos[ UDP_DISPATCH_SIZE ];
extern int UDP_fifos_used;

void notify_fifo(OS_FIFO *fifo) {
  register int i = 0;
  for (i = 0; i < UDP_fifos_used; i++) {
    if (udp_fifos[i].pfifo == fifo) {

      if (udp_fifos[i].pNotify) {
	udp_fifos[i].pNotify(udp_fifos[i].pfifo, udp_fifos[i].dest_port);
      }

      return;
    }
  }
}






// multicast function
int join_multicast(int fd, IPADDR g_ip) {
  UdpSocketDataSet *pSet = (UdpSocketDataSet *) GetExtraData(fd);
  if (pSet==NULL)
    return -1;
  if (pSet->rxport==0)
    return -2;

  // overwritten register multicast function
  myRegisterMulticastFifo(g_ip, pSet->rxport, &(pSet->the_fifo));

  // register own function for multicast packets
  // the default handling ignores notify which is
  // necessary for select()
  //pMultiCastFunc = myMultiProcessFunc;

  return 0;
}



} // extern "C"
