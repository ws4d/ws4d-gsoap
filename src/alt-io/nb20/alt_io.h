#ifndef _ALT_IO_H_
#define _ALT_IO_H_

#include <basictypes.h>

// SO_BROADCAST is not defined in NetBurner Library.
// However, it is required to tag outgoing UDP requests
#ifndef SO_BROADCAST
#define SO_BROADCAST 0x0020
#endif

// equivalent netburner macros
#ifndef ntohl
#define ntohl(x) (x)
#define ntohs(x) (x)
#define htonl(x) (x)
#define htons(x) (x)
#endif

// If not defined, gSOAP will set it to 8192 byte and
// allocates that amount on the stack when calling
// http_parse(). The NetBurner device will corrupt the
// stack so it must be lowered. This value is important
// for WITH_COOKIES since cookies are transported in
// http headers.
#ifndef SOAP_HDRLEN
#define SOAP_HDRLEN 256
#endif

#ifdef WITH_COOKIES
# ifndef FORCE_WITH_COOKIES
#  error "Cannot build WITH_COOKIES unless FORCE_WITH_COOKIES is defined"
# endif
#endif


#ifdef __cplusplus
extern "C"
{
#if 0
}
#endif
#endif


struct in_addr
{
  unsigned long s_addr;         // load with inet_aton()
};

struct sockaddr
{
  unsigned short sa_family;     // address family, AF_xxx
  char sa_data[14];             // 14 bytes of protocol address
};

struct sockaddr_in
{
  short sin_family;             // e.g. AF_INET
  unsigned short sin_port;      // e.g. htons(3490)
  struct in_addr sin_addr;      // see struct in_addr, below
  char sin_zero[8];             // zero this if you want to
};


// exported from socket.h
#ifndef AF_UNSPEC
#define AF_UNSPEC	0
#endif

#ifndef AF_INET
#define AF_INET		2
#endif

#define socklen_t uint32_t

// from <sys/socket.h>
int getsockname (int sockfd, struct sockaddr *addr, socklen_t * addrlen);


#define HAVE_INET_PTON_H
int inet_pton (int af, const char *src, void *dst);

#define HAVE_INET_NTOP_H
const char *inet_ntop (int af, const void *src, char *dst, socklen_t size);

unsigned int sleep (unsigned int seconds);


#ifdef __cplusplus
#if 0
{
#endif
}
#endif


#endif // _ALT_IO_H_
