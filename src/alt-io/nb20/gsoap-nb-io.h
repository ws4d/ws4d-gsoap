#ifndef _GSOAP_NB_IO_H_
#define _GSOAP_NB_IO_H_

#include "stdsoap2.h"

#define NETBURNER_PLUGIN_ID "NetBurner-PLUGIN-0.1"

#ifdef __cplusplus
extern "C"
{
#if 0
}
#endif
#endif

int gsoap_nb_register_handle (struct soap *soap);

#ifdef __cplusplus
#if 0
{
#endif
}
#endif

#endif // _GSOAP_NB_IO_H_
