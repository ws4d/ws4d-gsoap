/* lcm client- to control the life-cycle of a service
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include "lcm.nsmap"
#include "dpws_client.h"
#include "fileio.h"
#include "lcm-defs.h"
#include "zlib.h"

#include "ws4d_eprllist.h"

#define MAX_NUM_ARGS 5

char *interf = NULL;

static unsigned char buffer[10000000];  // rund 10MB
ws4d_qnamelist type_list;

ws4d_qnamelist service_list;

ws4d_alloc_list alist;

struct soap discovery, client;

struct dpws_s dpws;

struct ws4d_qname *service_type;

struct ws4d_abs_eprlist lcm_services;
struct ws4d_epr *selected_service = NULL;

struct ws4d_epr device;

enum cmdloop_cmds
{
  PRTHELP = 0,
  INSTALL,
  UNINSTALL,
  START,
  STOP,
  RESOLVE,
  STATE,
  ALL,
  UPDATE,
  EXITCMD
};

struct cmdloop_commands
{
  const char *str;              /* the string */
  int cmdnum;                   /* the command */
  int numargs;                  /* the number of arguments */
  const char *args;             /* the args */
  const char *help;             /* help for the commands */
} cmdloop_commands;

static struct cmdloop_commands cmdloop_cmdlist[] = {
  {"Help", PRTHELP, 0, "", "Print this help info"},
  {"install", INSTALL, 1, "", "install a service "},
  {"uninstall", UNINSTALL, 1, "", "uninstall a service ( serviceID )"},
  {"start", START, 1, "", "start a service ( serviceID )"},
  {"stop", STOP, 1, "", "stop a service ( serviceID )"},
  {"resolve", RESOLVE, 1, "", "resolve dependencies ( serviceD )"},
  {"state", STATE, 1, "", "print state of a service ( serviceID )"},
  {"all", ALL, 0, "", "print all services ( none )"},
  {"update", UPDATE, 1, "", "update a service ( serviceID )"},
  {"Exit", EXITCMD, 0, "", "exit the lcm-client application"}
};

/* commands */

static void
install (int argc, char **argv)
{
  struct _ns1__install query;
  struct _ns1__installResponse Response;
  char *filename = argv[0];
  char MsgId[DPWS_MSGID_SIZE];

  WS4D_UNUSED_PARAM (argc);

  fprintf (stdout, "\nInstalling service %s on lcm %s ...", filename,
           ws4d_epr_get_Addrs (selected_service));

  memset (&query, 0, sizeof (struct _ns1__install));
  query.CRC = 0;
  query.Param.__ptr = buffer;
  query.Param.__size = readfile (filename, query.Param.__ptr);
  query.Param.type = (char *) "application/zip";

  if (query.Param.__size < 0)
    {
      fprintf (stderr, "Cannot read file for attachment\n");
      return;
    }

  soap_set_namespaces (&client, lcm_namespaces);
  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  dpws_header_gen_request (&client, MsgId,
                           ws4d_epr_get_Addrs (selected_service),
                           "http://www.ws4d.org/services/life-cycle-manager/installRequest",
                           NULL, NULL, sizeof (struct SOAP_ENV__Header));

  if (soap_call___ns1__install
      (&client, ws4d_epr_get_Addrs (selected_service), NULL, &query,
       &Response) == SOAP_OK)
    {
      if (Response.status == LCM_OK)
        {
          fprintf (stdout, "Done: status: %d, ServiceID: %s \n",
                   Response.status, Response.serviceID);
        }
      else
        {
          fprintf (stderr, "\n--> %s <--\n",
                   error_list[Response.status].string);
        }
    }
  else
    {
      fprintf (stderr, "\nERROR: could not call installService\n");
      soap_print_fault (&client, stderr);
    }
}


static void
uninstall (int argc, char **argv)
{
  struct _ns1__uninstall query;
  struct _ns1__uninstallResponse Response;
  char *service_id = argv[0];
  char MsgId[DPWS_MSGID_SIZE];

  WS4D_UNUSED_PARAM (argc);

  fprintf (stdout, "\nUninstalling service %s on lcm %s ...", service_id,
           ws4d_epr_get_Addrs (selected_service));

  memset (&query, 0, sizeof (struct _ns1__uninstall));
  query.serviceID = service_id;

  soap_set_namespaces (&client, lcm_namespaces);
  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  dpws_header_gen_request (&client, MsgId,
                           ws4d_epr_get_Addrs (selected_service),
                           "http://www.ws4d.org/services/life-cycle-manager/uninstallRequest",
                           NULL, NULL, sizeof (struct SOAP_ENV__Header));

  if (soap_call___ns1__uninstall
      (&client, ws4d_epr_get_Addrs (selected_service), NULL, &query,
       &Response) == SOAP_OK)
    {

      if (Response.status == LCM_OK)
        {
          fprintf (stdout, "Done: status: %d\n\n", Response.status);
        }
      else
        {
          fprintf (stderr, "\n--> %s <--\n",
                   error_list[Response.status].string);
        }
    }
  else
    {
      fprintf (stderr, "\nERROR: could not call uninstallService\n");
      soap_print_fault (&client, stderr);
    }
}


static void
start (int argc, char **argv)
{
  struct _ns1__start query;
  struct _ns1__startResponse Response;
  char *serviceid = argv[0];
  char MsgId[DPWS_MSGID_SIZE];

  WS4D_UNUSED_PARAM (argc);

  fprintf (stdout, "\nStarting service %s on lcm %s ...", serviceid,
           ws4d_epr_get_Addrs (selected_service));

  memset (&query, 0, sizeof (struct _ns1__start));
  query.serviceID = serviceid;

  soap_set_namespaces (&client, lcm_namespaces);
  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  dpws_header_gen_request (&client, MsgId,
                           ws4d_epr_get_Addrs (selected_service),
                           "http://www.ws4d.org/services/life-cycle-manager/startRequest",
                           NULL, NULL, sizeof (struct SOAP_ENV__Header));

  if (soap_call___ns1__start
      (&client, ws4d_epr_get_Addrs (selected_service), NULL, &query,
       &Response) == SOAP_OK)
    {
      if (Response.status == LCM_OK)
        {
          fprintf (stdout, "Done: status: %d\n\n", Response.status);
        }
      else
        {
          fprintf (stderr, "\n--> %s <--\n",
                   error_list[Response.status].string);
        }
    }
  else
    {
      soap_print_fault (&client, stderr);
    }
}


static void
stop (int argc, char **argv)
{
  struct _ns1__stop query;
  struct _ns1__stopResponse Response;
  char *serviceid = argv[0];
  char MsgId[DPWS_MSGID_SIZE];

  WS4D_UNUSED_PARAM (argc);

  fprintf (stdout, "\nStopping service %s on lcm %s ...", serviceid,
           ws4d_epr_get_Addrs (selected_service));

  memset (&query, 0, sizeof (struct _ns1__stop));
  query.serviceID = serviceid;

  soap_set_namespaces (&client, lcm_namespaces);
  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  dpws_header_gen_request (&client, MsgId,
                           ws4d_epr_get_Addrs (selected_service),
                           "http://www.ws4d.org/services/life-cycle-manager/stopRequest",
                           NULL, NULL, sizeof (struct SOAP_ENV__Header));

  if (soap_call___ns1__stop
      (&client, ws4d_epr_get_Addrs (selected_service), NULL, &query,
       &Response) == SOAP_OK)
    {
      if (Response.status == LCM_OK)
        {
          fprintf (stdout, "Done: status: %d\n\n", Response.status);
        }
      else
        {
          fprintf (stderr, "\n--> %s <--\n",
                   error_list[Response.status].string);
        }
    }
  else
    {
      soap_print_fault (&client, stderr);
    }
}

static void
resolve (int argc, char **argv)
{
  struct _ns1__resolve query;
  struct _ns1__resolveResponse Response;
  char *serviceid = argv[0];
  char MsgId[DPWS_MSGID_SIZE];

  WS4D_UNUSED_PARAM (argc);

  fprintf (stdout, "\nResolvinging service %s on lcm %s ...", serviceid,
           ws4d_epr_get_Addrs (selected_service));

  memset (&query, 0, sizeof (struct _ns1__resolve));
  query.serviceID = serviceid;

  soap_set_namespaces (&client, lcm_namespaces);
  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  dpws_header_gen_request (&client, MsgId,
                           ws4d_epr_get_Addrs (selected_service),
                           "http://www.ws4d.org/services/life-cycle-manager/resolveRequest",
                           NULL, NULL, sizeof (struct SOAP_ENV__Header));

  if (soap_call___ns1__resolve
      (&client, ws4d_epr_get_Addrs (selected_service), NULL, &query,
       &Response) == SOAP_OK)
    {
      if (Response.status == LCM_OK)
        {
          fprintf (stdout, "Done: status: %d\n\n", Response.status);
        }
      else
        {
          fprintf (stderr, "\n--> %s <--\n",
                   error_list[Response.status].string);
        }
    }
  else
    {
      soap_print_fault (&client, stderr);
    }
}

static void
allServices (int argc, char **argv)
{
  struct _ns1__ListAll query;
  struct _ns1__ListAllResponse Response;
  char MsgId[DPWS_MSGID_SIZE];

  WS4D_UNUSED_PARAM (argc);
  WS4D_UNUSED_PARAM (argv);

  fprintf (stdout, "\nList all services installed in lcm %s ...",
           ws4d_epr_get_Addrs (selected_service));

  memset (&query, 0, sizeof (struct _ns1__ListAll));

  soap_set_namespaces (&client, lcm_namespaces);
  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  dpws_header_gen_request (&client, MsgId,
                           ws4d_epr_get_Addrs (selected_service),
                           "http://www.ws4d.org/services/life-cycle-manager/ListAllRequest",
                           NULL, NULL, sizeof (struct SOAP_ENV__Header));

  if (soap_call___ns1__ListAll
      (&client, ws4d_epr_get_Addrs (selected_service), NULL, &query,
       &Response) == SOAP_OK)
    {
      if (Response.status == LCM_OK)
        {
          int i = 0;

          printf ("\nStatus: %d, Number of Services: %d\n",
                  Response.status, Response.__sizeservices);

          for (i = 0; i < Response.__sizeservices; i++)
            {
              printf ("\n\t service %d: %s", i, Response.services[i]);
            }
        }
      else
        {
          fprintf (stderr, "\n--> %s <--\n",
                   error_list[Response.status].string);
        }
    }
  else
    {
      soap_print_fault (&client, stderr);
    }
}

static void
state (int argc, char **argv)
{
  struct _ns1__getState query;
  struct _ns1__getStateResponse Response;
  char *serviceid = argv[0];
  char MsgId[DPWS_MSGID_SIZE];

  WS4D_UNUSED_PARAM (argc);

  fprintf (stdout, "\nState of service %s on lcm %s ...", serviceid,
           ws4d_epr_get_Addrs (selected_service));

  memset (&query, 0, sizeof (struct _ns1__getState));
  query.serviceID = serviceid;

  soap_set_namespaces (&client, lcm_namespaces);
  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  dpws_header_gen_request (&client, MsgId,
                           ws4d_epr_get_Addrs (selected_service),
                           "http://www.ws4d.org/services/life-cycle-manager/getStateRequest",
                           NULL, NULL, sizeof (struct SOAP_ENV__Header));

  if (soap_call___ns1__getState
      (&client, ws4d_epr_get_Addrs (selected_service), NULL, &query,
       &Response) == SOAP_OK)
    {
      if (Response.status == LCM_OK)
        {
          printf ("\n\t\t serviceState : %d", Response.serviceState);
          printf ("\n\t\t valid       : %d", Response.valid);
          printf ("\n\t\t id        : %s",
                  (Response.uuid) ? (Response.uuid) : ("NULL"));
          printf ("\n\t\t File      : %s",
                  (Response.soFile) ? (Response.soFile) : ("NULL"));
          printf ("\n\t\t host        : %s",
                  (Response.host) ? (Response.host) : ("NULL"));
          printf ("\n\t\t version     : %s",
                  (Response.version) ? (Response.version) : ("NULL"));
        }
      else
        {
          fprintf (stderr, "\n--> %s <--\n",
                   error_list[Response.status].string);
        }
    }
  else
    {
      soap_print_fault (&client, stderr);
    }
}

static void
update (int argc, char **argv)
{
  struct _ns1__update query;
  struct _ns1__updateResponse Response;
  char *serviceid = argv[0];
  char *file = argv[1];
  char MsgId[DPWS_MSGID_SIZE];

  WS4D_UNUSED_PARAM (argc);

  fprintf (stdout, "\nUpdating of service %s on lcm %s ...", serviceid,
           ws4d_epr_get_Addrs (selected_service));

  memset (&query, 0, sizeof (struct _ns1__update));
  query.serviceID = serviceid;

  query.Param.type = (char *) "application/zip";
  query.Param.__ptr = buffer;
  query.Param.__size = readfile (file, query.Param.__ptr);
  if (query.Param.__size < 0)
    {
      fprintf (stderr, "\nCannot read file for attachment\n");
      return;
    }

  soap_set_namespaces (&client, lcm_namespaces);
  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  dpws_header_gen_request (&client, MsgId,
                           ws4d_epr_get_Addrs (selected_service),
                           "http://www.ws4d.org/services/life-cycle-manager/updateRequest",
                           NULL, NULL, sizeof (struct SOAP_ENV__Header));

  if (soap_call___ns1__update
      (&client, ws4d_epr_get_Addrs (selected_service), NULL, &query,
       &Response) == SOAP_OK)
    {
      if (Response.status == LCM_OK)
        {
          fprintf (stdout, "Done: status: %d\n\n", Response.status);
        }
      else
        {
          fprintf (stderr, "\n--> %s <--\n",
                   error_list[Response.status].string);
        }
    }
  else
    {
      soap_print_fault (&client, stdout);
    }
}

static void
print_help ()
{
  int numofcmds = sizeof (cmdloop_cmdlist) / sizeof (cmdloop_commands);
  int i;

  printf ("\n");
  printf ("***********************************\n");
  printf ("* ws4d lcm_client Help Info       *\n");
  printf ("***********************************\n");
  printf ("\n");
  printf ("Commands:\n");
  printf ("\n");

  for (i = 0; i < numofcmds; i++)
    {
      printf ("\t%s: (No. of args:%d)\n", cmdloop_cmdlist[i].str,
              cmdloop_cmdlist[i].numargs);
      printf ("\t\t%s\n", cmdloop_cmdlist[i].help);
      printf ("\n");
    }

}

static void
exit_lcm_client ()
{

  soap_end (&discovery);
  soap_done (&discovery);

  dpws_done (&dpws);
  ws4d_alloclist_done (&alist);

  exit (0);
}

static int
process_command (char *cmdline)
{

  char *cmd = NULL, *save_ptr = NULL, **args = NULL;
  int cmdnum = -1;
  int numofcmds = sizeof (cmdloop_cmdlist) / sizeof (cmdloop_commands);
  int cmdfound = 0, num_args = 0, invalid_args = 0;

  int i;

  args = ws4d_malloc (sizeof (char *) * MAX_NUM_ARGS);

  while (memchr (cmdline, '\n', strlen (cmdline)))
    {
      char *ret = NULL;

      ret = memchr (cmdline, '\n', strlen (cmdline));
      ret[0] = '\0';
    }

  cmd = STRTOK (cmdline, " ", &save_ptr);

  do
    {
      args[num_args] = STRTOK (NULL, " ", &save_ptr);

      if (args[num_args] != NULL)
        {
          num_args++;
        }
      else
        {
          break;
        }
    }

  while (num_args < MAX_NUM_ARGS);

  for (i = 0; i < numofcmds; i++)
    {
      if (cmd && STRCASECMP (cmd, cmdloop_cmdlist[i].str) == 0)
        {
          cmdnum = cmdloop_cmdlist[i].cmdnum;
          cmdfound++;

          if (num_args < cmdloop_cmdlist[i].numargs)
            {
              invalid_args = 1;
            }

          break;
        }
    }

  if (!cmdfound)
    {
      fprintf (stdout, "\n\nCommand not found\n");
      print_help ();
      return 0;
    }

  if (invalid_args)
    {
      fprintf (stdout, "\n\nInvalid arguments\n");
      print_help ();
      return 0;
    }

  switch (cmdnum)
    {
    case PRTHELP:
      print_help ();
      break;

    case INSTALL:
      install (num_args, args);
      break;

    case UNINSTALL:
      uninstall (num_args, args);
      break;

    case START:
      start (num_args, args);
      break;

    case STOP:
      stop (num_args, args);
      break;

    case RESOLVE:
      resolve (num_args, args);
      break;

    case STATE:
      state (num_args, args);
      break;

    case ALL:
      allServices (num_args, args);
      break;

    case UPDATE:
      update (num_args, args);
      break;

    case EXITCMD:
      exit_lcm_client ();
      break;

    default:
      printf ("\n\nInvalid command\n");
      print_help ();
      break;

    }
  return 0;
}

int
main (int argc, char **argv)
{

  char *XAddrs = NULL;
  char cmdline[100];
  char *deviceid = NULL;

  while ((argc > 1) && (argv[1][0] == '-'))
    {
      char *option = &argv[1][1];

      switch (option[0])
        {
        case 'i':
          if (strlen (option) > 2)
            {
              ++option;
              interf = option;
            }
          else
            {
              --argc;
              ++argv;
              interf = argv[1];
            }
          printf ("\nSet host to \"%s\"", interf);
          fflush (NULL);
          break;

        case 'd':
          if (strlen (option) > 2)
            {
              ++option;
              deviceid = option;
            }
          else
            {
              --argc;
              ++argv;
              deviceid = argv[1];
            }
          printf ("\nUsing device \"%s\"", interf);
          fflush (NULL);
          break;

        default:
          fprintf (stderr, "\nBad option %s", argv[1]);
        }

      --argc;
      ++argv;
    }

  if (interf == NULL)
    {
      printf ("\nNo host was specified!");
      exit (1);
    }

  if (deviceid == NULL)
    {
      printf ("\nNo lcm capable device was specified!");
      exit (1);
    }

  WS4D_ALLOCLIST_INIT (&alist);

  soap_init (&client);
#ifdef DEBUG
  soap_set_omode (&client, SOAP_XML_INDENT);
#endif

  if (dpws_init (&dpws, interf) != SOAP_OK)
    {
      fprintf (stderr, "\nCould not initialize dpws handle\n");
      dpws_done (&dpws);
      exit (1);
    }

  dpws_handle_init (&dpws, &client);
  ws4d_epr_init (&device);
  ws4d_epr_set_Addrs (&device, deviceid);

  ws4d_qnamelist_init (&type_list);
  ws4d_qnamelist_addstring
    ("\"http://www.uni-rostock.de/schemas/hostingService\":hostingService",
     &type_list, &alist);

  XAddrs = (char *) dpws_resolve_addr (&dpws, &device, NULL, 10000);
  if (!XAddrs)
    {
      fprintf (stderr, "\nCan't resolve device address %s\n", deviceid);
    }

  /* TODO: dprobe for type */

  ws4d_eprlist_init (&lcm_services, ws4d_eprllist_init, NULL);

  ws4d_qnamelist_init (&service_list);
  ws4d_qnamelist_addstring
    ("\"http://www.ws4d.org/services/life-cycle-manager\":life-cycle-manager",
     &service_list, &alist);

  if (dpws_find_services (&dpws, &device, &service_list, 10000, &lcm_services)
      == SOAP_OK)
    {
      selected_service = ws4d_eprlist_get_first (&lcm_services);

      fprintf (stdout,
               "\nDevice offers LCM-Service(s) - selected service %s\n",
               ws4d_epr_get_Addrs (selected_service));
    }
  else
    {
      fprintf (stderr, "\nLCM-Service not found on %s\n",
               ws4d_epr_get_Addrs (&device));
      exit (1);
    }

  while (1)
    {
      printf ("\n>> ");
      fgets (cmdline, 1024, stdin);
      process_command (cmdline);
    }

  exit_lcm_client ();
  return 0;

}
