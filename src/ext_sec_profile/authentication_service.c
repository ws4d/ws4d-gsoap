/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */


#include "auths.nsmap"
#include "dpws_device.h"
#include "security_engine.h"
#include "smdevp.h"
#include "wsseapi.h"

int
setup_AuthenticationService (struct dpws_s *device, struct soap *handle,
                             const char *wsdl, int backlog);

int
setup_AuthenticationService (struct dpws_s *device, struct soap *handle,
                             const char *wsdl, int backlog)
{
  char uri[DPWS_URI_MAX_LEN] = "http://host:0/AuthenticationService";
  struct ws4d_qname *service_type;
  struct ws4d_epr *service = NULL;
  struct soap_wsse_data *data = NULL;
  int err;
  struct security_engine_s *sec_engine = NULL;
  struct sh_plugin_data *sh_data = NULL;

  /* get security engine attached to soap handle */
  sh_data =
    (struct sh_plugin_data *) soap_lookup_plugin (handle, SH_PLUGIN_ID);
  if (!sh_data)
    {
      fprintf (stderr,
               "Error! Could not setup Authentication Service because "
               "no Security Engine has been registered!\n");
      return 1;
    }

  sec_engine = sh_data->sec_engine;

  /* check if wsse-plugin has already been registered */
  data = (struct soap_wsse_data *) soap_lookup_plugin (handle, soap_wsse_id);
  if (data == NULL)
    {                           /* plugin hasn't been registered yet */
      soap_register_plugin (handle, soap_wsse);
#ifdef DEBUG
      printf ("libauthsS: registered wsse-plugin\n");
    }
  else
    {
      printf ("libauthsS: wsse-plugin already registered\n");
#endif //DEBUG
    }
  soap_wsse_verify_init (handle);
  //TODO: Check, if there is any signature to validate
  /* check signature */
  if (soap_wsse_verify_auto (handle, SOAP_SMD_NONE, NULL, 0))
    {
      fprintf (stderr,
               "libauthsS: Could not activate automatic signature validation\n");
      return 1;
    }
  /* CA-Cert file */
  handle->cafile = sec_engine->cafile;


  soap_set_namespaces (handle, auths_namespaces);

  service = dpws_service_init (device, "AuthenticationService");

  if (dpws_service_bind
      (device, service, handle, uri, DPWS_URI_MAX_LEN, backlog))
    {
      return WS4D_ERR;
    }

  service_type = NULL;

  service_type = ws4d_qname_alloc (1, &device->alloc_list);
  service_type->ns =
    ws4d_strdup
    ("http://www.ws4d.org/2010/01/SecurityShell/AuthenticationService",
     &device->alloc_list);
  service_type->name =
    ws4d_strdup ("AuthenticationService", &device->alloc_list);
  if (dpws_service_add_type (service, service_type))
    {
      return WS4D_ERR;
    }


  if (wsdl && *wsdl)
    {
      if (dpws_service_set_wsdl (service, wsdl))
        {
          return WS4D_ERR;
        }
    }

  if ((err =
       dpws_add_hosted_service (device, service, uri, DPWS_URI_MAX_LEN)))
    {
      fprintf (stderr, "Error in setting up Authentication Service: %d\n",
               err);
      return err;
    }

  return ERR_OK;
}

int
__auths__VerifyOOBPin1 (struct soap *soap,
                        struct _auths__EmptyMessage *auths__VerifyOOBPin1,
                        struct __auths__VerifyOOBPin1Response *_param_1)
{

  char *cert_data;
  ws4dX509 client_certificate;
  ws4dChallenge challenge = (unsigned char *) malloc ((1 + CHALLENGE_SIZE) *
                                                      sizeof (char));
  int size;
  int result;
  EVP_PKEY *rsa_private_key;
  X509 *cert;
  FILE *fd;
  struct security_engine_s *sec_engine = NULL;
  struct sh_plugin_data *sh_data = NULL;

  /* get security engine attached to soap handle */
  sh_data = (struct sh_plugin_data *) soap_lookup_plugin (soap, SH_PLUGIN_ID);
  if (!sh_data)
    {
      fprintf (stderr,
               "Error! Could not setup Authentication Service because "
               "no Security Engine has been registered!\n");
      return 1;
    }

  sec_engine = sh_data->sec_engine;

  memset (challenge, 0, CHALLENGE_SIZE + 1);

  /* read certificate */
  if ((fd = fopen (sec_engine->certfile, "r")) == 0)
    {
      fprintf (stderr, "libauthsS: ERROR: pem file not found\n");
      return 1;
    }
  cert = PEM_read_X509 (fd, NULL, NULL, NULL);
  fclose (fd);
  /* reading in private key */
  if ((fd = fopen (sec_engine->keyfile, "r")) == 0)
    {
      fprintf (stderr, "libauthsS: ERROR: pem file not found\n");
      return 1;
    }
  rsa_private_key =
    PEM_read_PrivateKey (fd, NULL, NULL, (void *) sec_engine->keypassword);
  fclose (fd);
  if (!rsa_private_key)
    {
      fprintf (stderr, "libauthsS: ERROR: reading private key failed\n");
      return 1;
    }

  if (soap_wsse_verify_body (soap))
    {
      printf ("libauthsS: Signature does not include body\n");
      soap_wsse_delete_Security (soap);
      return soap_sender_fault (soap, "Service operation not signed", NULL);
#ifdef DEBUG
    }
  else
    {
      printf ("libauthsS: Signature includes body!\n");
#endif //DEBUG
    }

  /* allocate prepare outgoing fields */
  _param_1->auths__AuthError = (struct auths__AuthErrorType *)
    malloc (sizeof (struct auths__AuthErrorType));
  _param_1->auths__AuthError->AuthErrorMsg = (char *) malloc (10);

  /* read certificate string - conversion is done by web service
   * to keep soap-related stuff outside application logic
   */

  /* convert received base64-coded certificate to binary coded */
  cert_data = (char *) soap_base642s (soap,
                                      soap->header->wsse__Security->
                                      BinarySecurityToken->__item, NULL, 0,
                                      &size);
  if (!cert_data)
    {
      printf ("libauthsS: Could not read Certificate String!\n");
    }

  /* convert binary coded certificate to structured type */
  client_certificate =
    d2i_X509 (NULL, (const unsigned char **) &cert_data, size);

  if (sec_engine->ae_process_request_one)
    {
      result = sec_engine->ae_process_request_one
        (sec_engine, client_certificate, challenge);
      /*TODO: Create error table and use useful error-codes */
    }


  _param_1->auths__AuthError->AuthErrorCode = result;
  _param_1->auths__AuthError->AuthErrorMsg = (char *) "";
  _param_1->auths__Challenge =
    soap_s2base64 (soap, (const unsigned char *) challenge, NULL,
                   CHALLENGE_SIZE);


  if ((result =
       dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                 "http://www.ws4d.org/2010/01/SecurityShell/AuthenticationService/VerifyOOBPin/Response1",
                                 wsa_header_get_MessageId (soap),
                                 sizeof (struct SOAP_ENV__Header))))
    {
      return result;
    }

  /* add cert to security header */
  if (soap_wsse_add_BinarySecurityTokenX509 (soap, "X509Token", cert))
    {
      fprintf (stderr, "libauthsS: ERROR: add x509 token failed\n");
      return 1;
    }
  /* reference the cert for signing */
  if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509 (soap, "#X509Token"))
    {
      fprintf (stderr, "libauthsS: ERROR: add reference failed\n");
      return 1;
    }
  /* sign the msg  */
  if (soap_wsse_sign_body (soap, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
    {
      fprintf (stderr, "libauthsS: sign body failed\n");
      return 1;
    }

  return SOAP_OK;
}

int
__auths__VerifyOOBPin2 (struct soap *soap, char *auths__Challenge,
                        char *auths__Response,
                        struct auths__AuthErrorType *auths__AuthError,
                        struct __auths__VerifyOOBPin2Response *_param_2)
{

  int result, size;
  char *cert_data;
  ws4dX509 client_certificate;
  ws4dChallenge bin_challenge;
  ws4dResponse bin_response;
  ws4dResponse device_response =
    (unsigned char *) malloc (CHALLENGE_SIZE + 1);
  struct security_engine_s *sec_engine = NULL;
  struct sh_plugin_data *sh_data = NULL;

  /* get security engine attached to soap handle */
  sh_data = (struct sh_plugin_data *) soap_lookup_plugin (soap, SH_PLUGIN_ID);
  if (!sh_data)
    {
      fprintf (stderr,
               "Error! Could not setup Authentication Service because "
               "no Security Engine has been registered!\n");
      return 1;
    }

  sec_engine = sh_data->sec_engine;

//  struct soap_wsse_data *data = NULL;
  EVP_PKEY *rsa_private_key;
  X509 *cert;
  FILE *fd;

  /* read certificate */
  if ((fd = fopen (sec_engine->certfile, "r")) == 0)
    {
      fprintf (stderr, "libauthsS: ERROR: pem file not found\n");
      return 1;
    }
  cert = PEM_read_X509 (fd, NULL, NULL, NULL);
  fclose (fd);
  /* reading in private key */
  if ((fd = fopen (sec_engine->keyfile, "r")) == 0)
    {
      fprintf (stderr, "libauthsS: ERROR: pem file not found\n");
      return 1;
    }
  rsa_private_key =
    PEM_read_PrivateKey (fd, NULL, NULL, (void *) sec_engine->keypassword);
  fclose (fd);
  if (!rsa_private_key)
    {
      fprintf (stderr, "libauthsS: ERROR: reading private key failed\n");
      return 1;
    }

  if (soap_wsse_verify_body (soap))
    {
      printf ("libauthsS: Signature does not include body\n");
      soap_wsse_delete_Security (soap);
      return soap_sender_fault (soap, "Service operation not signed", NULL);
#ifdef DEBUG
    }
  else
    {
      printf ("libauthsS: Signature includes body!\n");
#endif //DEBUG
    }

  _param_2->auths__AuthError = (struct auths__AuthErrorType *)
    malloc (sizeof (struct auths__AuthErrorType));
  _param_2->auths__AuthError->AuthErrorMsg = (char *) malloc (255);
  _param_2->auths__AuthError->AuthErrorCode = 0;
  _param_2->auths__CertReference = (char *) malloc (REFERENCE_SIZE + 1);


  memset (_param_2->auths__CertReference, 0, REFERENCE_SIZE + 1);
  memset (device_response, 0, CHALLENGE_SIZE + 1);

  /* convert base64-coded certificate to binary coded */
  cert_data = (char *) soap_base642s (soap,
                                      soap->header->wsse__Security->
                                      BinarySecurityToken->__item, NULL, 0,
                                      &size);
  if (!cert_data)
    {
      printf ("Could not read Certificate String!\n");
    }

  /* convert binary coded certificate to structured type */
  client_certificate =
    d2i_X509 (NULL, (const unsigned char **) &cert_data, size);
  bin_response =
    (unsigned char *) soap_base642s (soap, auths__Response, NULL, 0, &size);
  bin_challenge =
    (unsigned char *) soap_base642s (soap, auths__Challenge, NULL, 0, &size);

  if (sec_engine->ae_process_request_two)
    {
      result = sec_engine->ae_process_request_two
        (sec_engine, client_certificate, bin_response,
         bin_challenge, device_response, _param_2->auths__CertReference);
    }

  _param_2->auths__AuthError->AuthErrorCode = result;
  /* TODO: Generate Error Message */
  memset (_param_2->auths__AuthError->AuthErrorMsg, 0, 255);
  strcpy (_param_2->auths__AuthError->AuthErrorMsg, "All Fine!");
  _param_2->auths__Response =
    (char *) soap_s2base64 (soap, (const unsigned char *) device_response,
                            NULL, CHALLENGE_SIZE);


  if ((result =
       dpws_header_gen_response (soap, NULL, wsa_header_get_ReplyTo (soap),
                                 "http://www.ws4d.org/2010/01/SecurityShell/AuthenticationService/VerifyOOBPin/Response2",
                                 wsa_header_get_MessageId (soap),
                                 sizeof (struct SOAP_ENV__Header))))
    {
      return result;
    }

  /* add cert to security header */
  if (soap_wsse_add_BinarySecurityTokenX509 (soap, "X509Token", cert))
    {
      fprintf (stderr, "libauthsS: ERROR: add x509 token failed\n");
      return 1;
    }
  /* reference the cert for signing */
  if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509 (soap, "#X509Token"))
    {
      fprintf (stderr, "libauthsS: ERROR: add reference failed\n");
      return 1;
    }
  /* sign the msg  */
  if (soap_wsse_sign_body (soap, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
    {
      fprintf (stderr, "libauthsS: sign body failed\n");
      return 1;
    }

  return SOAP_OK;

}
