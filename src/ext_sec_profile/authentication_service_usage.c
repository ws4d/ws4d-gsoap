/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * authentication_service_usage.c
 *
 *  Created on: 23.03.2010
 *      Author: cht
 */

#include "authentication_service_usage.h"
#include "auths.nsmap"



#include "smdevp.h"
#include "wsseapi.h"

struct ws4d_epr *
auths_find_authentication_service (struct dpws_s *dpws,
                                   struct ws4d_epr *device)
{
  struct ws4d_epr *auth_service = NULL;
  ws4d_qnamelist service_type_list;
  struct ws4d_abs_allocator alist;
  struct ws4d_abs_eprlist services;

  WS4D_ALLOCLIST_INIT (&alist);

  /* prepare service type list */
  ws4d_qnamelist_init (&service_type_list);
  ws4d_qnamelist_addstring
    ("\"http://www.ws4d.org/2010/01/SecurityShell/AuthenticationService\":AuthenticationService",
     &service_type_list, &alist);

  /* look up service with matching service types on device */
  ws4d_eprlist_init (&services, ws4d_eprllist_init, NULL);
  if (dpws_find_services (dpws, device, &service_type_list, 10000, &services)
      == SOAP_OK)
    {
      /* use first service */
      auth_service = ws4d_eprlist_get_first (&services);
#ifdef DEBUG
      printf ("\nlibauthsC: device offers AuthenticationService at %s\n",
              ws4d_epr_get_Addrs (auth_service));
#endif
    }
  else
    {
      fprintf (stderr,
               "\nauths_client: AuthenticationService not found on %s\n",
               ws4d_epr_get_Addrs (device));
      return NULL;
    }
  return auth_service;
}

int
auths_authenticate_device (struct soap *soap, struct ws4d_epr *device,
                           struct dpws_s *dpws,
                           struct ws4d_epr *service,
                           struct security_engine_s *s)
{
  int size;
  struct ws4d_epr *auth_service = NULL;
  unsigned char *bin_challenge = NULL;
  unsigned char *bin_response = NULL;
  unsigned char *base64_response = NULL;
  unsigned char *base64_challenge = NULL;
  ws4dResponse client_response = NULL;
  ws4dChallenge device_challenge = NULL;
  ws4dDerivedKey key = NULL;
  struct __auths__VerifyOOBPin1Response response1;
  struct __auths__VerifyOOBPin2Response response2;
  ws4dPin pin = NULL;
  struct auths__AuthErrorType error;
  struct soap_wsse_data *data = NULL;
  EVP_PKEY *rsa_private_key;
  X509 *cert;
  FILE *fd;

  /* first, let's check if we already authenticated this device by using its URI */
  if (!(s->ae_check_device_authentication (s, device->Address, NULL)))
    {
      /* yes, we already know this one! */
      return SOAP_OK;
    }

  if (!service)
    {
      auth_service = auths_find_authentication_service (dpws, device);
    }
  else
    {
      auth_service = service;
    }

  if (!auth_service)
    return 2;

  /* check if wsse-plugin has already been registered */
  data = (struct soap_wsse_data *) soap_lookup_plugin (soap, soap_wsse_id);
  if (data == NULL)
    {                           /* plugin hasn't been registered yet */
      soap_register_plugin (soap, soap_wsse);
#ifdef DEBUG
      printf ("libauthsC: registered wsse-plugin\n");
    }
  else
    {
      printf ("libauthsC: wsse-plugin already registered\n");
#endif //DEBUG
    }

  /* CA-Cert file */
  soap->cafile = s->cafile;
  soap_wsse_verify_init (soap);
  //TODO: Check, if there is any signature to validate
  /* check signature */
  if (soap_wsse_verify_auto (soap, SOAP_SMD_NONE, NULL, 0))
    {
      fprintf (stderr, "Could not activate automatic signature validation\n");
      return 1;
    }
  /* read certificate */
  if ((fd = fopen (s->certfile, "r")) == 0)
    {
      fprintf (stderr, "ERROR: pem file not found\n");
      return 1;
    }
  cert = PEM_read_X509 (fd, NULL, NULL, NULL);
  fclose (fd);
  /* reading in private key */
  if ((fd = fopen (s->keyfile, "r")) == 0)
    {
      fprintf (stderr, "ERROR: pem file not found\n");
      return 1;
    }
  rsa_private_key = PEM_read_PrivateKey (fd, NULL, NULL, (void *) "password");
  fclose (fd);
  if (!rsa_private_key)
    {
      fprintf (stderr, "ERROR: reading private key failed\n");
      return 1;
    }

  /* prepare soap handel to use service */
  soap_set_namespaces (soap, auths_namespaces);
  dpws_header_gen_request (soap, NULL, ws4d_epr_get_Addrs (auth_service),
                           "http://www.ws4d.org/2010/01/SecurityShell/AuthenticationService/VerifyOOBPin/Request1",
                           NULL, NULL, sizeof (struct SOAP_ENV__Header));

  /* add cert to security header */
  if (soap_wsse_add_BinarySecurityTokenX509 (soap, "X509Token", cert))
    {
      fprintf (stderr, "libauthsC: ERROR: add x509 token failed\n");
      return 1;
    }
  /* reference the cert for signing */
  if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509 (soap, "#X509Token"))
    {
      fprintf (stderr, "libauthsC: ERROR: add reference failed\n");
      return 1;
    }
  /* sign the msg  */
  if (soap_wsse_sign_body (soap, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
    {
      fprintf (stderr, "libauthsC: sign body failed\n");
      return 1;
    }

  /*
   * **********************************************
   * TODO: Check for errors, generate own errors
   * **********************************************
   */


  /* 1st part of handshake */
  if (soap_call___auths__VerifyOOBPin1
      (soap, ws4d_epr_get_Addrs (auth_service), NULL, NULL,
       &response1) == SOAP_OK)
    {
#ifdef DEBUG
      printf ("\nlibauthsC: got response from %s\n",
              ws4d_epr_get_Addrs (auth_service));
#endif
    }
  else
    {
      fprintf (stderr, "\nlibauthsC: error calling VerifyOOBPin1 on %s\n",
               ws4d_epr_get_Addrs (auth_service));
      return 1;
    }

  if (soap_wsse_verify_body (soap))
    {
      printf ("libauthsC: Signature does not include body\n");
      soap_wsse_delete_Security (soap);
      return soap_sender_fault (soap, "Service operation not signed", NULL);
#ifdef DEBUG
    }
  else
    {
      printf ("libauthsC: Signature includes body!\n");
#endif //DEBUG
    }

  pin = s->ae_retrieve_pin (s, pin);

  key = s->ae_derive_key_from_pin (s, pin);

  bin_challenge = (unsigned char *) soap_base642s (soap,
                                                   response1.auths__Challenge,
                                                   NULL, 0, &size);
  client_response = s->ae_compute_response (s, bin_challenge, key);
  base64_response = (unsigned char *) soap_s2base64 (soap, client_response,
                                                     NULL, CHALLENGE_SIZE);
  device_challenge = s->ae_generate_challenge (s, device_challenge);
  base64_challenge = (unsigned char *) soap_s2base64 (soap, device_challenge,
                                                      NULL, CHALLENGE_SIZE);

  error.AuthErrorCode = 0;
  error.AuthErrorMsg = (char *) malloc (10);
  strncpy (error.AuthErrorMsg, "All fine", 9);


  /* 2nd part of Handshake */
  dpws_header_gen_request (soap, NULL, ws4d_epr_get_Addrs (auth_service),
                           "http://www.ws4d.org/2010/01/SecurityShell/AuthenticationService/VerifyOOBPin/Request2",
                           NULL, NULL, sizeof (struct SOAP_ENV__Header));

  /* add cert to security header */
  if (soap_wsse_add_BinarySecurityTokenX509 (soap, "X509Token", cert))
    {
      fprintf (stderr, "libauthsC: ERROR: add x509 token failed\n");
      return 1;
    }
  /* reference the cert for signing */
  if (soap_wsse_add_KeyInfo_SecurityTokenReferenceX509 (soap, "#X509Token"))
    {
      fprintf (stderr, "libauthsC: ERROR: add reference failed\n");
      return 1;
    }
  /* sign the msg  */
  if (soap_wsse_sign_body (soap, SOAP_SMD_SIGN_RSA_SHA1, rsa_private_key, 0))
    {
      fprintf (stderr, "libauthsC: sign body failed\n");
      return 1;
    }

  if (soap_call___auths__VerifyOOBPin2
      (soap, ws4d_epr_get_Addrs (auth_service), NULL,
       (char *) base64_challenge, (char *) base64_response, &error,
       &response2) == SOAP_OK)
    {
#ifdef DEBUG
      printf ("\nlibauthsC: got response from %s\n",
              ws4d_epr_get_Addrs (auth_service));
#endif

      if (soap_wsse_verify_body (soap))
        {
          printf ("libauthsC: Signature does not include body\n");
          soap_wsse_delete_Security (soap);
          return soap_sender_fault (soap, "Service operation not signed",
                                    NULL);
#ifdef DEBUG
        }
      else
        {
          printf ("libauthsC: Signature includes body!\n");
#endif //DEBUG
        }

      /* Finish Handshake by verifying Device's Response and saving Reference */
      bin_response = (unsigned char *) soap_base642s (soap,
                                                      response2.
                                                      auths__Response, NULL,
                                                      0, &size);

      if (s->ae_verify_response (s, device_challenge, bin_response, key) !=
          ERR_OK)
        {
          printf ("libauthsC: Device's Response invalid!\n");
        }

      /* save received reference with device's uuid and certificate */
      if (response2.auths__CertReference)
        {
          char *bin_cert;
          ws4dX509 device_cert;
#ifdef DEBUG
          fprintf (stderr,
                   "libauthsC: received Reference on my certificate:\n\t<<%s>>\n",
                   response2.auths__CertReference);
#endif // DEBUG
          bin_cert = (char *) soap_base642s (soap,
                                             (const char *) soap->header->
                                             wsse__Security->
                                             BinarySecurityToken->__item,
                                             NULL, 0, &size);
          device_cert =
            d2i_X509 (NULL, (const unsigned char **) &bin_cert, size);
          s->cdb_save_cert_reference (s, device->Address,
                                      response2.auths__CertReference,
                                      device_cert);
        }

    }
  else
    {
      fprintf (stderr, "\nlibauthsC: error calling VerifyOOBPin1 on %s\n",
               ws4d_epr_get_Addrs (auth_service));
    }
  soap_wsse_delete_Security (soap);
  return SOAP_OK;
}
