/* Copyright (C) 2007-2010  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

/*
 * security_engine.c
 *
 *  Created on: 09.03.2010
 *      Author: cht
 */

#include "security_engine.h"
#include "wsseapi.h"

/* TODO: double-check if all function pointers are initialized!
 * (Comment should never ever be removed (at least not until version 1.0)
 */

struct security_engine_s *
security_engine_init (struct security_engine_s *s)
{
  s->cafile = NULL;
  s->certfile = NULL;
  s->keyfile = NULL;
  s->keypassword = NULL;
  s->ae_process_request_one = NULL;
  s->ae_process_request_two = NULL;
  s->ae_request_pin = NULL;
  s->ae_derive_key_from_pin = NULL;
  s->ae_generate_challenge = NULL;
  s->ae_compute_response = NULL;
  s->ae_verify_response = NULL;
  s->ae_check_client_authentication = NULL;
  s->ae_check_device_authentication = NULL;
  s->ae_retrieve_pin = NULL;
  s->ae_propagate_pin = NULL;
  s->cert_list_location = NULL;
  s->cdb_save_certificate = NULL;
  s->cdb_lookup_certificate_pin = NULL;
  s->cdb_cert_compare = NULL;
  s->cdb_compute_reference = NULL;
  s->cdb_update_challenge = NULL;
  s->cdb_update_pin = NULL;
  s->cdb_save_cert_reference = NULL;
  s->cdb_lookup_cert_reference = NULL;
  s->cdb_update_persistent_db = NULL;
  s->cdb_read_persistent_db = NULL;
  s->cdb_lookup_cert_by_ref = NULL;
  s->cdb_wsse_token_handler = NULL;
  s->cdb_get_credential_identifier = NULL;
  s->authe_set_dynamic_authorization = NULL;
  s->authe_authorize_request = NULL;
  s->authe_set_authorization_mode = NULL;
  s->authe_activate_authorization = NULL;
  s->authe_deactivate_authorization = NULL;
  s->authe_request_dynamic_authorization_oob = NULL;
  s->authe_get_credential = NULL;
  s->authdb_save_authorization = NULL;
  s->authdb_delete_authorization = NULL;
  s->authdb_lookup_authorization = NULL;
  s->authdb_update_authorization = NULL;
  return s;
}
