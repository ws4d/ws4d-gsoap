/* hosting service - out of process service hosting
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * author: Guido Moritz, Elmar Zeeb and Christian Fabian
 */

#include "dpws_device.h"
#include "dpws_hosting.h"
#include "ws4d_target.h"
#include "config.h"
#include "fsbased_meta_repository.h"

#ifdef WIN32
#include <sys\stat.h>
#include <sys/stat.h>
#include <assert.h>
#include <winbase.h>
#include <process.h>
#include "windows.h"
#include "direct.h"
#include <signal.h>
#else
#include <signal.h>
#include <sys/stat.h>
#include "dirent.h"
#endif

volatile int hs_running = 1;
struct hosting_service hs;
struct fsbmr metadata;
struct ws4d_abs_allocator alist;
char *interf = NULL;

static void
service_exit ()
{
  printf ("\nHostingService: shutting down...\n");
  hs_running = 0;
}

static int
hosting_setup (struct ws4d_epr *mHost, struct ws4d_thisModel *mThisModel,
               struct ws4d_thisDevice *mThisDevice)
{
  int err;
  struct ws4d_thisModel *thisModel;
  struct ws4d_thisDevice *thisDevice;

  /* set up hosting service */
  err =
    hs_init (&hs, ws4d_epr_get_Addrs (mHost), ws4d_serviceep_getid (mHost),
             interf, 0, 100, 0, NULL);
  ws4d_fail (err != WS4D_OK, err);

  /* add device type */
  hs_addtypes (&hs, ws4d_targetep_get_Types (mHost));

  /* add device scopes */
  /* TODO */

  /* set thisModel meta data */
  thisModel = hs_metadata_change_thismodel (&hs);
  err =
    ws4d_thismodel_copy (thisModel,
                         (const struct ws4d_thisModel *) mThisModel, &alist);
  ws4d_fail (err != WS4D_OK, err);

  /* set thisDevice meta data */
  thisDevice = hs_metadata_change_thisdevice (&hs);
  err =
    ws4d_thisdevice_copy (thisDevice,
                          (const struct ws4d_thisDevice *) mThisDevice,
                          &alist);
  ws4d_fail (err != WS4D_OK, err);

  /* Update Metadata */
  err = hs_metadata_update (&hs);
  ws4d_fail (err != WS4D_OK, err);

  return WS4D_OK;
}

static int
hosted_setup (struct ws4d_epr *host, struct ws4d_abs_eprlist *hosted)
{
  struct ws4d_epr *service, *iter;
  int err;

  WS4D_UNUSED_PARAM (host);

  /* add services */
  ws4d_eprlist_foreach (service, iter, hosted)
  {
    char *addrs = ws4d_strdup (ws4d_epr_get_Addrs (service), &alist);
    hs_addservice (&hs, ws4d_serviceep_getid (service), addrs,
                   ws4d_serviceep_gettypestr (service));
    ws4d_free_alist (addrs);
  }

  /* Update Metadata */
  err = hs_metadata_update (&hs);
  ws4d_fail (err != WS4D_OK, err);

  return WS4D_OK;
}

static int
hosted_shutdown (struct ws4d_epr *host, struct ws4d_abs_eprlist *hosted)
{
  struct ws4d_epr *service, *iter;
  int err;

  WS4D_UNUSED_PARAM (host);

  /* add services */
  ws4d_eprlist_foreach (service, iter, hosted)
  {
    hs_removeservice (&hs, ws4d_serviceep_getid (service));
  }

  /* Update Metadata */
  err = hs_metadata_update (&hs);
  ws4d_fail (err != WS4D_OK, err);

  return WS4D_OK;
}

int
main (int argc, char **argv)
{
  int err;
  const char *configuration = HOSTING_SERVICE_CONFIG_FILE;
  const char *metadata_dir = HOSTING_SERVICE_METADATA_DIR;
#ifndef WIN32
  struct sigaction sa;
#endif

  /* parsing command line options */
  while ((argc > 1) && (argv[1][0] == '-'))
    {
      char *option = &argv[1][1];
      switch (option[0])
        {
        case 'i':
          if (strlen (option) > 2)
            {
              ++option;
              interf = option;
            }
          else
            {
              --argc;
              ++argv;
              interf = argv[1];
            }
          printf ("HostingService: Set host to \"%s\"\n", interf);
          break;
        case 'c':
          if (strlen (option) > 2)
            {
              ++option;
              configuration = option;
            }
          else
            {
              --argc;
              ++argv;
              configuration = argv[1];
            }
          printf ("HostingService: Set configuration to \"%s\"\n",
                  configuration);
          break;
        case 'm':
          if (strlen (option) > 2)
            {
              ++option;
              metadata_dir = option;
            }
          else
            {
              --argc;
              ++argv;
              metadata_dir = argv[1];
            }
          printf ("HostingService: Set metadata directory to \"%s\"\n",
                  metadata_dir);
          break;
        default:
          printf ("HostingService: Bad option %s\n", argv[1]);
          printf ("\t-i : IP-Adress\n");
          printf ("\t-c : location of configuration file\n");
          printf ("\t-l : location of the metadata directory\n");
          exit (1);
        }
      --argc;
      ++argv;
    }

  WS4D_ALLOCLIST_INIT (&alist);

  err = fsbmr_init (&metadata, configuration, metadata_dir,
                    hosting_setup, hosted_setup, hosted_shutdown);
  if (err != WS4D_OK)
    {
      printf ("\nHostingService: Can't init hosting service\n");
      hs_done (&hs);
      exit (1);
    }

  /* install signal handler for SIGINT or Ctrl-C */
#ifdef WIN32
  signal (SIGINT, service_exit);
#else
  memset (&sa, 0, sizeof (sa));
  sa.sa_handler = service_exit;
  sigaction (SIGINT, &sa, NULL);
#endif

  printf ("\nHostingService: ready to serve... (Ctrl-C for shut down)\n");

  hs_activate (&hs);

  while (hs_running)
    {
      printf ("\nHostingService: waiting for request\n");

      hs_dowork (&hs, 1000);

      if (fsbmr_do_monitor (&metadata))
        {
          hs_metadata_update (&hs);
        }
    }

  hs_deactivate (&hs);
  hs_done (&hs);
  fsbmr_done (&metadata);

  ws4d_allocator_done (&alist);

  printf ("\nHostingService: shut down done\n");

  return 0;
}
