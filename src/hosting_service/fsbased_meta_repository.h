/* hosting service - out of process service hosting
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 12.06.2011
 *      Author: elmex
 */

#ifndef FSBASED_META_REPOSITORY_H_
#define FSBASED_META_REPOSITORY_H_

#include "ws4d_misc.h"
#include "ws4d_abstract_eprlist.h"
#include "stdsoap2.h"

typedef int (*fsbmr_hosting_setup_cb) (struct ws4d_epr * host,
                                       struct ws4d_thisModel * thisModel,
                                       struct ws4d_thisDevice * thisDevice);

typedef int (*fsbmr_hosted_setup_cb) (struct ws4d_epr * host,
                                      struct ws4d_abs_eprlist * hosted);

typedef int (*fsbmr_hosted_shutdown_cb) (struct ws4d_epr * host,
                                         struct ws4d_abs_eprlist * hosted);

struct fsbmr
{
  const char *location;
  const char *configuration;
  struct
  {
    fsbmr_hosting_setup_cb hosting_setup;
    fsbmr_hosted_setup_cb hosted_setup;
    fsbmr_hosted_shutdown_cb hosted_shutdown;
  } callbacks;
  struct ws4d_list_node items;
  struct soap parser;
  struct ws4d_abs_allocator alist;
};

struct fsbmr_item
{
  struct ws4d_list_node list;
  char *file;
  int status;
  struct ws4d_epr host;
  struct ws4d_abs_eprlist hosted;
};

int fsbmr_init (struct fsbmr *repo,
                const char *configuration, const char *location,
                fsbmr_hosting_setup_cb hosting_setup,
                fsbmr_hosted_setup_cb hosted_setup,
                fsbmr_hosted_shutdown_cb hosted_shutdown);

int fsbmr_done (struct fsbmr *repo);

int fsbmr_do_monitor (struct fsbmr *repo);

#endif /* FSBASED_META_REPOSITORY_H_ */
