/* hosting service - out of process service hosting
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 14.03.2012
 *      Author: elmex
 */

#ifndef WITH_NOGLOBAL
#define WITH_NOGLOBAL
#endif

#define SOAP_FMAC3 static
#include "ws4ddmC.c"

#include "ws4ddmLib.h"

struct ws4ddm__ConfigurationType *
ws4d_get_ws4ddm__ConfigurationType (struct soap *soap,
                                    struct ws4ddm__ConfigurationType *p,
                                    const char *tag, const char *type)
{
  return soap_get_ws4ddm__ConfigurationType (soap, p, tag, type);
}

/* End of ws4ddmServerLib.c */
