/* hosting service - out of process service hosting
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 12.06.2011
 *      Author: elmex
 */
#include "ws4d_misc.h"
#include "soap_misc.h"
#include "fsbased_meta_repository.h"
#include "ws4ddm.nsmap"
#include "ws4ddmLib.h"
#include "ws4d_target.h"
#include "ws4d_service.h"
#include "ws4d_eprllist.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>


#define WS4D_FSBMR_CHECKED          0x00000001
#define MAX_CHAR_SIZE 512
#define BYTESCHUNK 4096

const char *thisModel_URI =
  "http://www.ws4d.org/2012/03/device_metadata/ThisModel";
size_t thisModel_URI_size;
const char *thisDevice_URI =
  "http://www.ws4d.org/2012/03/device_metadata/ThisDevice";
size_t thisDevice_URI_size;
const char *Relationship_URI =
  "http://www.ws4d.org/2012/03/device_metadata/Relationship";
size_t Relationship_URI_size;

static int fsbmr_clear (struct fsbmr *repo);
static int fsbmr_hostingservice_setup (struct fsbmr *repo);
static int fsbmr_hostedservice_setup (struct fsbmr *repo,
                                      struct fsbmr_item *item);
static int fsbmr_hostedservice_shutdown (struct fsbmr *repo,
                                         struct fsbmr_item *item);

int
fsbmr_init (struct fsbmr *repo,
            const char *configuration, const char *location,
            fsbmr_hosting_setup_cb hosting_setup,
            fsbmr_hosted_setup_cb hosted_setup,
            fsbmr_hosted_shutdown_cb hosted_shutdown)
{
  /* test parameters */
  ws4d_assert (repo && location && configuration, WS4D_EPARAM);

  thisModel_URI_size = strlen (thisModel_URI);
  thisDevice_URI_size = strlen (thisDevice_URI);
  Relationship_URI_size = strlen (Relationship_URI);

  repo->configuration = configuration;
  repo->location = location;
  repo->callbacks.hosting_setup = hosting_setup;
  repo->callbacks.hosted_setup = hosted_setup;
  repo->callbacks.hosted_shutdown = hosted_shutdown;
  WS4D_INIT_LIST (&repo->items);
  WS4D_ALLOCLIST_INIT (&repo->alist);
  soap_init (&repo->parser);
  soap_set_namespaces (&repo->parser, ws4ddm_namespaces);

  return fsbmr_hostingservice_setup (repo);
}

int
fsbmr_done (struct fsbmr *repo)
{
  int err;

  /* test parameters */
  ws4d_assert (repo && repo->location, WS4D_EPARAM);

  repo->configuration = NULL;
  repo->location = NULL;
  soap_done (&repo->parser);
  err = fsbmr_clear (repo);

  ws4d_alloclist_done (&repo->alist);

  return err;
}

static int
fsbmr_clear (struct fsbmr *repo)
{
  register struct fsbmr_item *item, *next;

  /* test parameters */
  ws4d_assert (repo, WS4D_EPARAM);

  ws4d_list_foreach (item, next, &repo->items, struct fsbmr_item, list)
  {
    /* TODO */
  }

  return WS4D_OK;
}

int
fsbmr_do_monitor (struct fsbmr *repo)
{
  register struct fsbmr_item *item, *next;
  char string_xmlFile[MAX_CHAR_SIZE];
  int found = 0, change = 0, err;

#ifdef WIN32
  WIN32_FIND_DATA w32fd;
  HANDLE hFind = NULL;
#else
  struct stat attribut;
  struct dirent *dir_entry_p;
  DIR *dir_p = opendir (repo->location);
#endif

  /* test parameters */
  ws4d_assert (repo && repo->location && *repo->location, WS4D_EPARAM);

  /* mark all entries unchecked */
  ws4d_list_foreach (item, next, &repo->items, struct fsbmr_item, list)
  {
    ws4d_int_clrbit (&item->status, WS4D_FSBMR_CHECKED);
  }

  /* check for XML-files in location */

#ifdef WIN32
  sprintf (string_xmlFile, "%s\\*.xml", location);
  hFind = FindFirstFile (string_xmlFile, &w32fd);
  ws4d_fail (hFind == INVALID_HANDLE_VALUE, WS4D_ERR);

  do
    {
      if (!strcmp (w32fd.cFileName, ".") || !strcmp (w32fd.cFileName, ".."))
        {
          continue;
        }

      SNPRINTF (string_xmlFile, MAX_CHAR_SIZE, "%s\\%s", location,
                w32fd.cFileName);

#else
  while (NULL != (dir_entry_p = readdir (dir_p)))
    {
      if (!stat (dir_entry_p->d_name, &attribut)
          || !strstr (dir_entry_p->d_name, ".xml"))
        {
          continue;
        }

      SNPRINTF (string_xmlFile, MAX_CHAR_SIZE, "%s/%s", repo->location,
                dir_entry_p->d_name);

#endif
      found = 0;
      /* a file was found -> search all known entries */
      ws4d_list_foreach (item, next, &repo->items, struct fsbmr_item, list)
      {
        if (item && item->file && !strcmp (item->file, string_xmlFile))
          {
            /* file found in list */
            found = 1;
            item->status = 1;
            break;
          }
      }

      if (!found)
        {
          struct fsbmr_item *new_item =
            ws4d_malloc_alist (sizeof (struct fsbmr_item), &repo->alist);
          ws4d_fail (new_item == NULL, 0);

          new_item->file = ws4d_strdup (string_xmlFile, &repo->alist);
          new_item->status = 1;
          ws4d_epr_init (&new_item->host);
          ws4d_eprlist_init (&new_item->hosted, ws4d_eprllist_init, NULL);
          ws4d_list_add (&new_item->list, &repo->items);

          /* parse file */
          err = fsbmr_hostedservice_setup (repo, new_item);
          if (err != WS4D_OK)
            {
              fprintf (stderr, "Failure adding up metadata file %s",
                       new_item->file);
            }

          change = 1;
        }
    }
#ifdef WIN32
  while (FindNextFile (hFind, &w32fd));
  FindClose (hFind);
#else
  closedir (dir_p);
#endif

  /* search for metadata files to remove */
  ws4d_list_foreach (item, next, &repo->items, struct fsbmr_item, list)
  {

    if (item && item->status == 0)
      {
        err = fsbmr_hostedservice_shutdown (repo, item);
        if (err != WS4D_OK)
          {
            fprintf (stderr, "Failure removing up metadata file %s",
                     item->file);
          }

        ws4d_list_del (&item->list);
        ws4d_free_alist (item->file);
        ws4d_epr_done (&item->host);
        ws4d_eprlist_done (&item->hosted);

        ws4d_free_alist (item);

        change = 1;
      }
  }

  return change;
}

static int
fsbmr_hostingservice_setup (struct fsbmr *repo)
{
  int i, err;
  struct ws4ddm__ConfigurationType configuration;
  struct ws4d_epr host;
  struct ws4d_thisModel *model = NULL;
  struct ws4d_thisDevice *device = NULL;

  /* test parameters */
  ws4d_assert (repo, WS4D_EPARAM);

  /* initialize host */
  ws4d_epr_init (&host);

  /* initialize parser */
  soap_begin (&repo->parser);
  repo->parser.recvfd = open (repo->configuration, O_RDONLY);
  ws4d_fail (repo->parser.recvfd == -1, WS4D_ERR);
  soap_begin_recv (&repo->parser);

  /* parse */
  memset (&configuration, 0, sizeof (configuration));
  ws4d_fail (ws4d_get_ws4ddm__ConfigurationType
             (&repo->parser, &configuration, "ws4ddm:Configuration",
              "") == NULL, WS4D_ERR);

  /* check configuration sections */
  for (i = 0; i < configuration.__sizeConfigurationSection; i++)
    {
      struct ws4ddm__ConfigurationSectionType *section =
        &configuration.ws4ddm__ConfigurationSection[i];

      if (section && section->ws4ddm__Dialect
          && !strncmp (section->ws4ddm__Dialect, Relationship_URI,
                       Relationship_URI_size))
        {
          struct ws4ddm__RelationshipType *relationship =
            &section->union_CS.ws4ddm__Relationship;
          char *expanded_type;

          ws4d_serviceep_setid (&host,
                                relationship->
                                ws4ddm__Host->ws4ddm__ServiceId);
          ws4d_epr_set_Addrs (&host,
                              relationship->ws4ddm__Host->ws4ddm__DeviceId);
          ws4d_targetep_set_XAddrs (&host,
                                    relationship->
                                    ws4ddm__Host->ws4ddm__XAddrs);

          expanded_type =
            (char *) soap_expand_QNames (&repo->parser,
                                         relationship->
                                         ws4ddm__Host->ws4ddm__Types);
          ws4d_targetep_add_Typestr (&host, expanded_type);

          continue;
        }

      if (section && section->ws4ddm__Dialect
          && !strncmp (section->ws4ddm__Dialect, thisModel_URI,
                       thisModel_URI_size))
        {
          model =
            (struct ws4d_thisModel *) &section->union_CS.ws4ddm__ThisModel;

          continue;
        }

      if (section && section->ws4ddm__Dialect
          && !strncmp (section->ws4ddm__Dialect, thisDevice_URI,
                       thisDevice_URI_size))
        {
          device =
            (struct ws4d_thisDevice *) &section->union_CS.ws4ddm__ThisDevice;

          continue;
        }
    }

  err = repo->callbacks.hosting_setup (&host, model, device);

  soap_end_recv (&repo->parser);
  soap_destroy (&repo->parser);
  soap_end (&repo->parser);

  ws4d_epr_done (&host);

  return err;
}

static int
fsbmr_hostedservice_setup (struct fsbmr *repo, struct fsbmr_item *item)
{
  int ret, i, j;
  struct ws4ddm__ConfigurationType configuration;

  /* test parameters */
  ws4d_assert (repo && item, WS4D_ERR);

  /* initialize parser */
  soap_begin (&repo->parser);
  repo->parser.recvfd = open (item->file, O_RDONLY);
  ws4d_fail (repo->parser.recvfd == -1, WS4D_ERR);
  soap_begin_recv (&repo->parser);

  /* parse */
  memset (&configuration, 0, sizeof (configuration));
  ws4d_fail (ws4d_get_ws4ddm__ConfigurationType
             (&repo->parser, &configuration, "ws4ddm:Configuration",
              "") == NULL, WS4D_ERR);

  /* check configuration sections */
  for (i = 0; i < configuration.__sizeConfigurationSection; i++)
    {
      struct ws4ddm__ConfigurationSectionType *section =
        &configuration.ws4ddm__ConfigurationSection[i];

      if (section && section->ws4ddm__Dialect
          && !strncmp (section->ws4ddm__Dialect, Relationship_URI,
                       Relationship_URI_size))
        {
          struct ws4ddm__RelationshipType *relationship =
            &section->union_CS.ws4ddm__Relationship;

          if (relationship->ws4ddm__Host)
            {
              if (relationship->ws4ddm__Host->ws4ddm__ServiceId)
                {
                  ws4d_serviceep_setid (&item->host,
                                        (const char *)
                                        relationship->ws4ddm__Host->
                                        ws4ddm__ServiceId);
                }

              if (relationship->ws4ddm__Host->ws4ddm__Types)
                {
                  ws4d_serviceep_addtypestr (&item->host,
                                             (const char *)
                                             relationship->ws4ddm__Host->
                                             ws4ddm__Types);
                }

              if (relationship->ws4ddm__Host->ws4ddm__XAddrs)
                {
                  ws4d_epr_set_Addrs (&item->host,
                                      (const char *)
                                      relationship->ws4ddm__Host->
                                      ws4ddm__XAddrs);
                }
            }

          for (j = 0; j < relationship->__sizeHosted; j++)
            {
              struct ws4ddm__HostedServiceType *hosted =
                &relationship->ws4ddm__Hosted[j];
              struct ws4d_epr *hosted_epr =
                ws4d_eprlist_alloc (&item->hosted);

              ws4d_serviceep_setid (hosted_epr,
                                    (const char *) hosted->ws4ddm__ServiceId);
              ws4d_epr_set_Addrs (hosted_epr,
                                  (const char *) hosted->ws4ddm__XAddrs);
              ws4d_serviceep_addtypestr (hosted_epr,
                                         (const char *)
                                         hosted->ws4ddm__Types);

              ws4d_eprlist_add (&item->hosted, hosted_epr);
            }
        }
    }

  ret = repo->callbacks.hosted_setup (&item->host, &item->hosted);

  soap_end_recv (&repo->parser);
  soap_destroy (&repo->parser);
  soap_end (&repo->parser);

  return ret;
}

static int
fsbmr_hostedservice_shutdown (struct fsbmr *repo, struct fsbmr_item *item)
{
  ws4d_assert (repo && item, WS4D_ERR);

  return repo->callbacks.hosted_shutdown (&item->host, &item->hosted);
}
