/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "ws4d_misc.h"
#include "ws4d_epr.h"
#include "ws4d_service.h"
#include "ws4d_abstract_eprlist.h"

#define WS4D_SERVICEEP_ID "WS4D-ServiceEp-0.1"

#define WS4D_SERVICEEP_INACTIVE (0)
#define WS4D_SERVICEEP_ACTIVE (1)

const char *ws4d_serviceep_id = WS4D_SERVICEEP_ID;

struct ws4d_serviceep_plugin
{
  int status;
  const char *id;
  struct ws4d_epr *device;
  ws4d_qnamelist types;
  const char *wsdl;
  const char *__any;
  void *transport_data;
};

/* forward declarations */

static struct ws4d_serviceep_plugin
  *ws4d_serviceep_plugindata (struct ws4d_epr *epr);

static int ws4d_serviceep_fcopy (struct ws4d_epr *epr,
                                 struct ws4d_epr_plugin *dst,
                                 struct ws4d_epr_plugin *src);

static void ws4d_serviceep_fdelete (struct ws4d_epr *epr,
                                    struct ws4d_epr_plugin *p);

static int ws4d_serviceep_finvalidate (struct ws4d_epr *epr,
                                        struct ws4d_epr_plugin *p);

static int ws4d_serviceep_fcreate (struct ws4d_epr *epr,
                                   struct ws4d_epr_plugin *plugin, void *arg);

static struct ws4d_serviceep_plugin
*ws4d_serviceep_checkregister_plugin (struct ws4d_epr *epr);

/* register callbacks */

static struct ws4d_epr_plugin_cb ws4d_serviceep_callbacks =
  { ws4d_serviceep_fdelete, ws4d_serviceep_fcopy, ws4d_serviceep_finvalidate };

/* implementation */

static struct ws4d_serviceep_plugin *
ws4d_serviceep_checkregister_plugin (struct ws4d_epr *epr)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_plugindata (epr);

  /* check if plugin is already registered */
  if (!plugin)
    {
      /* register plugin at epr */
      ws4d_epr_register_plugin (epr, ws4d_serviceep_fcreate, NULL);

      plugin = ws4d_serviceep_plugindata (epr);
      ws4d_assert (plugin, NULL);
    }

  return plugin;
}

static struct ws4d_serviceep_plugin *
ws4d_serviceep_plugindata (struct ws4d_epr *epr)
{
  return (struct ws4d_serviceep_plugin *)
    ws4d_epr_get_plugindata (epr, ws4d_serviceep_id);
}

static int
ws4d_serviceep_fcopy (struct ws4d_epr *epr, struct ws4d_epr_plugin *dst,
                      struct ws4d_epr_plugin *src)
{
  struct ws4d_serviceep_plugin *dst_data = NULL, *src_data = NULL;
  int res = 0;

  src_data = (struct ws4d_serviceep_plugin *) src->data;

  /* test parameters */
  ws4d_assert (src_data, WS4D_EPARAM);

  dst->id = ws4d_serviceep_id;
  dst->cb = &ws4d_serviceep_callbacks;

  if (dst->data)
    {
      dst_data = dst->data;
    }
  else
    {
      dst_data = ws4d_malloc (sizeof (struct ws4d_serviceep_plugin));
      ws4d_fail (!dst_data, WS4D_EOM);

      dst->data = dst_data;

      ws4d_qnamelist_init (&dst_data->types);
    }

  /* copy types */
  res = ws4d_qnamelist_copy (&src_data->types, &dst_data->types);
  ws4d_fail (res != WS4D_OK, res);

  /* reference device */
  dst_data->device = src_data->device;

  /* copy id */
  return ws4d_strdup2 (src_data->id, (const char **) &dst_data->id,
                       ws4d_epr_get_alist (epr));
}

static void
ws4d_serviceep_fdelete (struct ws4d_epr *epr, struct ws4d_epr_plugin *p)
{
  struct ws4d_serviceep_plugin *plugin = p->data;

  WS4D_UNUSED_PARAM (epr);

  /* test parameters */
  ws4d_assert (plugin,);

  ws4d_qnamelist_clear (&plugin->types);
  ws4d_qnamelist_done (&plugin->types);
  ws4d_free (plugin);

}

static int
ws4d_serviceep_finvalidate (struct ws4d_epr *epr, struct ws4d_epr_plugin *p)
{
  WS4D_UNUSED_PARAM (p);

  ws4d_epr_remove_plugin(epr, ws4d_serviceep_id);

  return WS4D_OK;
}

static int
ws4d_serviceep_fcreate (struct ws4d_epr *epr, struct ws4d_epr_plugin *plugin,
                        void *arg)
{
  struct ws4d_serviceep_plugin *data = NULL;

  WS4D_UNUSED_PARAM (arg);
  WS4D_UNUSED_PARAM (epr);

  plugin->id = ws4d_serviceep_id;
  plugin->cb = &ws4d_serviceep_callbacks;

  data = ws4d_malloc (sizeof (struct ws4d_serviceep_plugin));
  ws4d_fail (!data, WS4D_EOM);

  ws4d_qnamelist_init (&data->types);

  plugin->data = data;

  return WS4D_OK;
}

ws4d_qnamelist *
ws4d_serviceep_gettypelist (struct ws4d_epr * epr)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, NULL);

  /* return reference to types qname list */
  return &plugin->types;
}

int
ws4d_serviceep_setid (struct ws4d_epr *epr, const char *id)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_checkregister_plugin (epr);

  /* test parameters */
  ws4d_assert (plugin && id && *id, WS4D_EPARAM);

  /* duplicate id string */
  return ws4d_strdup2 (id, (const char **) &plugin->id,
                       ws4d_epr_get_alist (epr));
}

const char *
ws4d_serviceep_getid (struct ws4d_epr *epr)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, NULL);

  /* return copy of id string */
  return (const char *) plugin->id;
}

int
ws4d_serviceep_setdevice (struct ws4d_epr *epr, struct ws4d_epr *device)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_checkregister_plugin (epr);

  /* test parameters */
  ws4d_assert (plugin && epr, WS4D_EPARAM);

  /* duplicate id string */
  plugin->device = device;

  return WS4D_OK;
}

struct ws4d_epr *
ws4d_serviceep_getdevice (struct ws4d_epr *epr)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, NULL);

  /* return copy of id string */
  return plugin->device;
}

int ws4d_serviceep_id_mf(struct ws4d_epr *epr, void *matchingdata, int iterations)
{
  const char *id = ws4d_serviceep_getid (epr);

  switch (iterations) {
    case 1: return (id && !strcmp ((char *) matchingdata, id));
    case 2: return (id && (matchingdata == id));
  }

  return 0;
}

int
ws4d_serviceep_activate (struct ws4d_epr *epr)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_checkregister_plugin (epr);

  /* test parameters */
  ws4d_assert (plugin
               && (plugin->status == WS4D_SERVICEEP_INACTIVE), WS4D_EPARAM);

  plugin->status = WS4D_SERVICEEP_ACTIVE;
  return WS4D_OK;
}

int
ws4d_serviceep_deactivate (struct ws4d_epr *epr)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_checkregister_plugin (epr);

  /* test parameters */
  ws4d_assert (plugin
               && (plugin->status == WS4D_SERVICEEP_ACTIVE), WS4D_EPARAM);

  plugin->status = WS4D_SERVICEEP_INACTIVE;
  return WS4D_OK;
}

int
ws4d_serviceep_isactive (struct ws4d_epr *epr)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_plugindata (epr);

  ws4d_assert (plugin, 0);

  return plugin->status == WS4D_SERVICEEP_ACTIVE;
}

int
ws4d_serviceep_set_wsdl (struct ws4d_epr *epr, const char *wsdl)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_checkregister_plugin (epr);

  /* test parameters */
  ws4d_assert (plugin, WS4D_EPARAM);

  return ws4d_strdup2 (wsdl, (const char **) &plugin->wsdl,
                       ws4d_epr_get_alist (epr));
}

const char *
ws4d_serviceep_get_wsdl (struct ws4d_epr *epr)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, NULL);

  return plugin->wsdl;
}

int
ws4d_serviceep_set_ext (struct ws4d_epr *epr, const char *ext)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_checkregister_plugin (epr);

  /* test parameters */
  ws4d_assert (plugin && ext, WS4D_EPARAM);

  /* update extension */
  return ws4d_strdup2 (ext, (const char **) &plugin->__any,
                       ws4d_epr_get_alist (epr));
}

const char *
ws4d_serviceep_get_ext (struct ws4d_epr *epr)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, NULL);

  /* return ext string */
  return plugin->__any;
}

int
ws4d_serviceep_set_transportdata (struct ws4d_epr *epr, void *transport_data)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_checkregister_plugin (epr);

  /* test parameters */
  ws4d_assert (plugin, WS4D_EPARAM);

  plugin->transport_data = transport_data;

  return WS4D_OK;
}

void *
ws4d_serviceep_get_transportdata (struct ws4d_epr *epr)
{
  struct ws4d_serviceep_plugin *plugin = ws4d_serviceep_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, NULL);

  return plugin->transport_data;
}
