/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *  Created on: 14.08.2008
 *      Author: Elmar Zeeb
 */

#include "ws4d_misc.h"
#include "ws4d_abstract_eprlist.h"

int
ws4d_eprlist_init (struct ws4d_abs_eprlist *head,
                   int (*finit) (struct ws4d_abs_eprlist * head, void *arg),
                   void *arg)
{
  int ret = 0;

  ws4d_assert (head && finit, WS4D_ERR);

  memset (head, 0, sizeof (struct ws4d_abs_eprlist));
  ret = finit (head, arg);
  if (ret != WS4D_OK)
    {
      return ret;
    }

  ws4d_assert (head->callbacks && head->callbacks->fdone
               && head->callbacks->falloc && head->callbacks->ffree
               && head->callbacks->fadd && head->callbacks->fadd2
               && head->callbacks->fremove && head->callbacks->fclear
               && head->callbacks->fisempty && head->callbacks->fget_first
               && head->callbacks->fget_next
               && head->callbacks->fget_byMatch, WS4D_ERR);

  return WS4D_OK;
}

void
ws4d_eprlist_done (struct ws4d_abs_eprlist *head)
{
  if (!head || !head->callbacks)
    {
      return;
    }

  if (head->callbacks->fdone)
    {
      head->callbacks->fdone (head);
    }

  memset (head, 0, sizeof (struct ws4d_abs_eprlist));
}

struct ws4d_epr *
ws4d_eprlist_alloc (struct ws4d_abs_eprlist *head)
{
  ws4d_assert (head && head->callbacks && head->callbacks->falloc, NULL);

  return head->callbacks->falloc (head);
}

int
ws4d_eprlist_free (struct ws4d_abs_eprlist *head, struct ws4d_epr *epr)
{
  ws4d_assert (head && head->callbacks && head->callbacks->ffree && epr,
               WS4D_ERR);

  return head->callbacks->ffree (head, epr);
}

int
ws4d_eprlist_add (struct ws4d_abs_eprlist *head, struct ws4d_epr *epr)
{
  ws4d_assert (head && head->callbacks && head->callbacks->fadd && epr,
               WS4D_ERR);

  return head->callbacks->fadd (head, epr);
}

int
ws4d_eprlist_add2 (struct ws4d_abs_eprlist *head, struct ws4d_epr *epr)
{
  ws4d_assert (head && head->callbacks && head->callbacks->fadd2 && epr,
               WS4D_ERR);

  return head->callbacks->fadd2 (head, epr);
}

int
ws4d_eprlist_remove (struct ws4d_abs_eprlist *head, struct ws4d_epr *epr)
{
  ws4d_assert (head && head->callbacks && head->callbacks->fremove && epr,
               WS4D_ERR);

  return head->callbacks->fremove (head, epr);
}

int
ws4d_eprlist_isempty (struct ws4d_abs_eprlist *head)
{
  ws4d_assert (head && head->callbacks
               && head->callbacks->fisempty, WS4D_ERR);

  return head->callbacks->fisempty (head);
}

int
ws4d_eprlist_clear (struct ws4d_abs_eprlist *head)
{
  ws4d_assert (head && head->callbacks && head->callbacks->fclear, WS4D_ERR);

  return head->callbacks->fclear (head);
}

int
ws4d_eprlist_filter (struct ws4d_abs_eprlist *dst,
                     struct ws4d_abs_eprlist *src,
                     ws4d_eprt_matchingfunction mf, void *mdata, int *copies)
{
  ws4d_eprlist_clear (dst);

  return ws4d_eprlist_update(dst, src, mf, mdata, copies);
}

static int
_ws4d_eprlist_copy_or_update (struct ws4d_abs_eprlist *dst,
                              struct ws4d_epr *src)
{
  int err;
  struct ws4d_epr *copy_epr = NULL;

  if (!ws4d_eprlist_isempty (dst))
    {
      copy_epr = ws4d_eprlist_get_byAddr (dst, ws4d_epr_get_Addrs (src));
    }

  if (!copy_epr)
    {
      err = ws4d_eprlist_add2 (dst, src);
    }
  else
    {
      err = ws4d_epr_copy (copy_epr, src);
    }

  return err;
}

int
ws4d_eprlist_update (struct ws4d_abs_eprlist *dst,
                     struct ws4d_abs_eprlist *src,
                     ws4d_eprt_matchingfunction mf, void *mdata, int *copies)
{
  register struct ws4d_epr *item, *iter;
  int err, lcopies = 0;

  if (mf == NULL)
    {
      ws4d_eprlist_foreach (item, iter, src)
      {
        err = _ws4d_eprlist_copy_or_update (dst, item);
        ws4d_fail (err != WS4D_OK, err);
        lcopies++;
      }
    }
  else
    {
      ws4d_eprlist_foreach (item, iter, src)
      {
        if (mf (item, mdata, 1) == 1)
          {
            err = _ws4d_eprlist_copy_or_update (dst, item);
            ws4d_fail (err != WS4D_OK, err);
            lcopies++;
          }
      }
    }

  if (copies)
    {
      *copies = lcopies;
    }

  return WS4D_OK;
}

int
ws4d_eprlist_filterlen (struct ws4d_abs_eprlist *head,
                        ws4d_eprt_matchingfunction mf, void *mdata)
{
  register struct ws4d_epr *item, *iter;
  register int result = 0;

  if (mf == NULL)
    {
      ws4d_eprlist_foreach (item, iter, head)
      {
        result++;
      }
    }
  else
    {
      ws4d_eprlist_foreach (item, iter, head)
      {
        if (mf (item, mdata, 1) == 1)
          {
            result++;
          }
      }
    }

  return result;
}

struct ws4d_epr *
ws4d_eprlist_get_first (struct ws4d_abs_eprlist *head)
{
  ws4d_assert (head && head->callbacks && head->callbacks->fget_first, NULL);

  return head->callbacks->fget_first (head);
}

struct ws4d_epr *
ws4d_eprlist_get_next (struct ws4d_abs_eprlist *head, struct ws4d_epr *epr)
{
  ws4d_assert (head && head->callbacks && head->callbacks->fget_next, NULL);

  return head->callbacks->fget_next (head, epr);
}

struct ws4d_epr *
ws4d_eprlist_get_byMatch2 (struct ws4d_abs_eprlist *head,
                           ws4d_eprt_matchingfunction mf, void *mdata,
                           int iterations)
{
  int i;
  struct ws4d_epr *result;

  ws4d_assert (head && head->callbacks && head->callbacks->fget_byMatch && mf,
               NULL);

  for (i = iterations + 1, result = NULL; (i > 0) && (result == NULL); i--)
    {
      result = head->callbacks->fget_byMatch (head, mf, mdata, i);
    }

  return result;
}

int
ws4d_epr_AddrsS_mf (struct ws4d_epr *epr, void *matchingdata, int iterations)
{
  const char *Addrs = ws4d_epr_get_Addrs (epr);

  WS4D_UNUSED_PARAM (iterations);

  return (Addrs && !strcmp (matchingdata, Addrs));
}

int
ws4d_epr_AddrsL_mf (struct ws4d_epr *epr, void *matchingdata, int iterations)
{
  struct ws4d_stringlist *AddrL = ws4d_epr_get_AddrsList (epr);

  WS4D_UNUSED_PARAM (iterations);

  return ws4d_stringlist_compare (AddrL, matchingdata) == 0;
}

#ifdef WITH_MUTEXES
void
ws4d_eprlist_lock (struct ws4d_abs_eprlist *head)
{
  ws4d_assert (head && head->callbacks && head->callbacks->flock,);

  head->callbacks->flock (head);
}

void
ws4d_eprlist_unlock (struct ws4d_abs_eprlist *head)
{
  ws4d_assert (head && head->callbacks && head->callbacks->funlock,);

  head->callbacks->funlock (head);
}
#endif
