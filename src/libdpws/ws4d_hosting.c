/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "ws4d_misc.h"
#include "ws4d_service.h"
#include "ws4d_hostingservice.h"
#include "ws4d_abstract_eprlist.h"
#include "ws4d_eprllist.h"

#define WS4D_HOSTINGEP_ID "WS4D-HostedingEP-0.1"

const char *ws4d_hostingep_plugin_id = WS4D_HOSTINGEP_ID;

struct ws4d_hostingservice_plugin
{
  int change_tracker;
  struct ws4d_abs_eprlist hosted;
  struct ws4d_thisDevice device;
  struct ws4d_thisModel model;
};

/* forward declarations */

static struct ws4d_hostingservice_plugin
  *ws4d_hosting_plugindata (struct ws4d_epr *epr);

static void ws4d_hosting_fdelete (struct ws4d_epr *epr,
                                    struct ws4d_epr_plugin *p);

static int ws4d_hosting_fcreate (struct ws4d_epr *epr,
                                   struct ws4d_epr_plugin *plugin, void *arg);

/* register callbacks */

static struct ws4d_epr_plugin_cb ws4d_hosting_callbacks =
  { ws4d_hosting_fdelete, NULL, NULL };

/* implementation */

static struct ws4d_hostingservice_plugin *
ws4d_hosting_plugindata (struct ws4d_epr *epr)
{
  return (struct ws4d_hostingservice_plugin *)
    ws4d_epr_get_plugindata (epr, ws4d_hostingep_plugin_id);
}

static void
ws4d_hosting_fdelete (struct ws4d_epr *epr, struct ws4d_epr_plugin *p)
{
  struct ws4d_hostingservice_plugin *plugin = p->data;

  WS4D_UNUSED_PARAM (epr);

  /* test parameters */
  ws4d_assert (plugin,);

  ws4d_eprlist_clear (&plugin->hosted);
  ws4d_eprlist_done (&plugin->hosted);

  ws4d_free (plugin);
}

static int
ws4d_hosting_fcreate (struct ws4d_epr *epr, struct ws4d_epr_plugin *plugin,
                        void *arg)
{
  struct ws4d_hostingservice_plugin *data = NULL;

  WS4D_UNUSED_PARAM (arg);
  WS4D_UNUSED_PARAM (epr);

  plugin->id = ws4d_hostingep_plugin_id;
  plugin->cb = &ws4d_hosting_callbacks;

  data = ws4d_malloc (sizeof (struct ws4d_hostingservice_plugin));
  ws4d_fail (!data, WS4D_EOM);

  ws4d_eprlist_init (&data->hosted, ws4d_eprllist_init, NULL);

  plugin->data = data;

  return WS4D_OK;
}

int
ws4d_hosting_init (struct ws4d_epr *epr)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);

  if (plugin)
    {
      return WS4D_OK;
    }

  /* register plugin at epr */
  return ws4d_epr_register_plugin (epr, ws4d_hosting_fcreate, NULL);
}

int
ws4d_hosting_change_interf (struct ws4d_epr *epr, const char *hosts)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, WS4D_EPARAM);

  plugin->change_tracker |= WS4D_HS_RELCHANGED;

  return ws4d_targetep_set_XAddrs(epr, hosts);
}

struct ws4d_epr *
ws4d_hosting_add_service (struct ws4d_epr *epr, const char *serviceid)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);
  int err;
  struct ws4d_epr *service = NULL;

  /* test parameters */
  ws4d_assert (plugin && serviceid && *serviceid, NULL);

  /* avoid duplicates */
  service = ws4d_eprlist_getByServiceId(&plugin->hosted, serviceid);
  ws4d_fail (service, NULL);

  /* allocate new service epr */
  service = ws4d_eprlist_alloc (&plugin->hosted);
  ws4d_fail (!service, NULL);

  /* set service id */
  err = ws4d_serviceep_setid (service, serviceid);
  if (err != WS4D_OK)
    {
      ws4d_eprlist_free (&plugin->hosted, service);
      return NULL;
    }

  /* add service to list */
  err = ws4d_eprlist_add (&plugin->hosted, service);
  if (err != WS4D_OK)
    {
      ws4d_eprlist_free (&plugin->hosted, service);
      return NULL;
    }

  return service;
}

int
ws4d_hosting_bind_service (struct ws4d_epr *epr, struct ws4d_epr *service,
                           char *uri, size_t size)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin && service && uri && (size > 0), WS4D_EPARAM);

  /* check if service was already bound */
  if (ws4d_epr_get_Addrs(service) != NULL)
    {
      int match;

      /* create string list of uri */
      struct ws4d_stringlist uri_list;
      ws4d_stringlist_init3(&uri_list, uri);

      /* match service address with uri */
      match = ws4d_stringlist_compare(
                  ws4d_epr_get_AddrsList(service),
                  &uri_list);
      ws4d_stringlist_done(&uri_list);

      /* fail if addresses don't match */
      ws4d_fail(match != 0, WS4D_ERR);

      return WS4D_OK;
    }

  /* asign addresses */
  return ws4d_epr_set_Addrs (service, uri);
}

int
ws4d_hosting_activate_service (struct ws4d_epr *epr, struct ws4d_epr *service,
                               char *uri, size_t size)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);
  int err;

  /* test parameters */
  ws4d_assert (plugin && service && !ws4d_serviceep_isactive (service), WS4D_EPARAM);

  /* bind service to uri */
  err = ws4d_hosting_bind_service (epr, service, uri, size);
  ws4d_fail (err != WS4D_OK, err);

  /* activate service */
  err = ws4d_serviceep_activate (service);

  /* mark as changed */
  plugin->change_tracker |= WS4D_HS_RELCHANGED;

  return err;
}

int
ws4d_hosting_deactivate_service (struct ws4d_epr *epr, struct ws4d_epr *service)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);
  int err;

  /* test parameters */
  ws4d_assert (plugin && service && ws4d_serviceep_isactive (service), WS4D_EPARAM);

  err = ws4d_serviceep_deactivate (service);

  plugin->change_tracker |= WS4D_HS_RELCHANGED;

  return err;
}

int
ws4d_hosting_remove_service (struct ws4d_epr *epr, struct ws4d_epr *service)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);
  int ret;

  /* test parameters */
  ws4d_assert (plugin && service, WS4D_EPARAM);

  if (ws4d_serviceep_isactive (service))
    {
      ws4d_hosting_deactivate_service (epr, service);
    }

  ret = ws4d_eprlist_remove (&plugin->hosted, service);
  ws4d_fail (ret != WS4D_OK, ret);

  return ws4d_eprlist_free (&plugin->hosted, service);
}

struct ws4d_abs_eprlist *
ws4d_hosting_get_services (struct ws4d_epr *epr)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, NULL);

  return &plugin->hosted;
}

static int ws4d_hosted_isactive_mf(struct ws4d_epr *epr, void *matchingdata, int iterations)
{
  WS4D_UNUSED_PARAM(matchingdata);
  WS4D_UNUSED_PARAM(iterations);

  return ws4d_serviceep_isactive (epr);
}

int
ws4d_hosting_get_activeservices (struct ws4d_epr *epr, struct ws4d_abs_eprlist *list)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);
  int matches = 0;

  /* test parameters */
  ws4d_assert (plugin && list, matches);

  ws4d_eprlist_filter(list, &plugin->hosted, ws4d_hosted_isactive_mf, NULL, &matches);

  return matches;
}

int
ws4d_hosting_get_active_services_count (struct ws4d_epr *epr)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, WS4D_EPARAM);

  return ws4d_eprlist_filterlen(&plugin->hosted, ws4d_hosted_isactive_mf, NULL);
}

struct ws4d_relationship *
ws4d_hosting_get_relationship (struct ws4d_epr *epr,
                               struct ws4d_abs_allocator *alist)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);
  struct ws4d_relationship *result;

  /* test parameters */
  ws4d_assert (plugin && alist, NULL);

  result = ws4d_malloc_alist (sizeof (struct ws4d_relationship), alist);
  ws4d_fail (!result, NULL);

  result->hosted_count = ws4d_hosting_get_active_services_count (epr);
  if (result->hosted_count > 0)
    {
      struct ws4d_host *host;
      register struct ws4d_epr *service, *iter;

      result->hosted =
        ws4d_malloc_alist (sizeof (struct ws4d_host) * result->hosted_count,
                           alist);
      if (!result->hosted)
        {
          ws4d_free_alist (result);
          return NULL;
        }

      host = result->hosted;

      ws4d_eprlist_foreach (service, iter, &plugin->hosted)
      {
        ws4d_epr_lock (service);
        if (ws4d_serviceep_isactive (service))
          {
            host->Addr = ws4d_strdup (ws4d_epr_get_Addrs (service), alist);
            host->ServiceId = ws4d_strdup (ws4d_serviceep_getid (service), alist);
            ws4d_qnamelist_init (&host->types);
            ws4d_qnamelist_copy (ws4d_serviceep_gettypelist (service),
                                 &host->types);
            host->__any =
              ws4d_strdup ((char *) ws4d_serviceep_get_ext (service), alist);
            host++;
          }
        ws4d_epr_unlock (service);
      }
    }

  return result;
}

void
ws4d_hosting_free_relationship (struct ws4d_epr *epr,
                                struct ws4d_relationship *relationship)
{
  int i;

  /* test parameters */
  WS4D_UNUSED_PARAM(epr);
  ws4d_assert (relationship,);

  for (i = 0; i < relationship->hosted_count; i++)
    {
      ws4d_free_alist (relationship->hosted[i].Addr);
      ws4d_free_alist (relationship->hosted[i].ServiceId);
      ws4d_free_alist (relationship->hosted[i].__any);
      ws4d_qnamelist_done (&relationship->hosted[i].types);
    }
  ws4d_free_alist (relationship->hosted);
  ws4d_free_alist (relationship);
}

struct ws4d_thisDevice *
ws4d_hosting_get_thisdevice (struct ws4d_epr *epr)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, NULL);

  return &plugin->device;
}

struct ws4d_thisDevice *
ws4d_hosting_change_thisdevice (struct ws4d_epr *epr)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, NULL);

  plugin->change_tracker |= WS4D_HS_DEVCHANGED;

  return &plugin->device;
}

struct ws4d_thisModel *
ws4d_hosting_get_thismodel (struct ws4d_epr *epr)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, NULL);

  return &plugin->model;
}

struct ws4d_thisModel *
ws4d_hosting_change_thismodel (struct ws4d_epr *epr)
{
  struct ws4d_hostingservice_plugin *plugin = ws4d_hosting_plugindata (epr);

  /* test parameters */
  ws4d_assert (plugin, NULL);

  plugin->change_tracker |= WS4D_HS_MODCHANGED;

  return &plugin->model;
}

