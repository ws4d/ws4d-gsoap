/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *  Created on: 18.08.2008
 *      Author: Elmar Zeeb
 */

#include "ws4d_misc.h"
#include "ws4d_abstract_eprlist.h"
#include "ws4d_target.h"
#include "ws4d_targetservice.h"

#include "ws4d_eprllist.h"

/**
 * Scope matching functions
 */

struct ws4d_ts_matchby
{
  struct ws4d_list_node list;
  const char *MatchBy;
  ws4d_stringlist_matchingfunction scope_match_func;
};

static struct ws4d_ts_matchby *
ws4d_targetservice_getMatchBy (struct ws4d_targetservice *ts,
                               const char *MatchBy)
{
  register struct ws4d_ts_matchby *cur = NULL, *next = NULL;

  ws4d_assert (ts, NULL);

  ws4d_list_foreach (cur, next, &ts->MatchBy, struct ws4d_ts_matchby, list)
  {
    if (MatchBy && *MatchBy)
      {
        if (!strcmp (cur->MatchBy, MatchBy))
          {
            return cur;
          }
      }
    else
      {
        return cur;
      }
  }

  return NULL;
}

int
ws4d_targetservice_addMatchBy (struct ws4d_targetservice *ts,
                               const char *MatchBy,
                               ws4d_stringlist_matchingfunction
                               scope_match_func)
{
  struct ws4d_ts_matchby *elem;

  ws4d_assert (ts && MatchBy, WS4D_ERR);

  if (ws4d_targetservice_getMatchBy (ts, MatchBy) != NULL)
    {
      return WS4D_ERR;
    }

  elem = ws4d_malloc_alist (sizeof (struct ws4d_ts_matchby), &ts->alist);
  ws4d_assert (elem, WS4D_EOM);

  elem->MatchBy = MatchBy;
  elem->scope_match_func = scope_match_func;

  ws4d_list_add (&elem->list, &ts->MatchBy);

  return WS4D_OK;
}

int
ws4d_targetservice_delMatchBy (struct ws4d_targetservice *ts,
                               const char *MatchBy)
{
  struct ws4d_ts_matchby *elem;

  ws4d_assert (ts, WS4D_ERR);

  elem = ws4d_targetservice_getMatchBy (ts, MatchBy);
  if (elem)
    {
      ws4d_list_del (&elem->list);
      ws4d_free_alist (elem);

      return WS4D_OK;
    }

  return WS4D_ERR;
}

static int
ws4d_targetservice_setDefaultMatchBy (struct ws4d_targetservice *ts,
                                      const char *MatchBy)
{
  struct ws4d_ts_matchby *elem;

  ws4d_assert (ts, WS4D_ERR);

  elem = ws4d_targetservice_getMatchBy (ts, MatchBy);
  if (elem)
    {
      ws4d_list_move (&elem->list, &ts->MatchBy);

      return WS4D_OK;
    }

  return WS4D_ERR;
}

/**
 * target service
 */

int
ws4d_targetservice_init (struct ws4d_targetservice *ts,
                         const char *defMatchBy,
                         ws4d_stringlist_matchingfunction defscope_match_func)
{
  ws4d_assert (ts, WS4D_ERR);

  ts->as.MessageNumber = 0;
  ts->as.InstanceId = ws4d_systime_s ();

  WS4D_INIT_LIST (&ts->MatchBy);
  WS4D_ALLOCLIST_INIT (&ts->alist);

  /* add default match by */
  ws4d_targetservice_addMatchBy (ts, defMatchBy, defscope_match_func);
  ws4d_targetservice_setDefaultMatchBy (ts, defMatchBy);

  return ws4d_eprlist_init (&ts->targets, ws4d_eprllist_init, NULL);
}

int
ws4d_targetservice_done (struct ws4d_targetservice *ts)
{
  register struct ws4d_ts_matchby *cur = NULL, *next = NULL;

  ws4d_assert (ts, WS4D_ERR);

  ws4d_list_foreach (cur, next, &ts->MatchBy, struct ws4d_ts_matchby, list)
  {
    ws4d_list_del (&cur->list);
    ws4d_free_alist (cur);
  }

  ws4d_eprlist_done (&ts->targets);
  ws4d_alloclist_done (&ts->alist);

  return WS4D_OK;
}

struct ws4d_epr *
ws4d_targetservice_inittarget (struct ws4d_targetservice *ts,
                               const char *Addrs, const char *XAddrs)
{
  struct ws4d_epr *target;

  ws4d_assert (ts && Addrs && XAddrs, NULL);

  target = ws4d_eprlist_alloc (&ts->targets);
  ws4d_fail (target == NULL, NULL);

  /* set logical address */
  ws4d_epr_set_Addrs (target, Addrs);

  /* set pyhsical address */
  ws4d_targetep_set_XAddrs (target, XAddrs);

  ws4d_targetep_set_ts (target, ts);

  ws4d_eprlist_add (&ts->targets, target);

  return target;
}

int
ws4d_targetservice_deltarget (struct ws4d_targetservice *ts,
                              struct ws4d_epr *target)
{
  int err;
  struct ws4d_epr *del_target;

  del_target =
    ws4d_targetservice_gettarget_byAddr (ts, ws4d_epr_get_Addrs (target));
  ws4d_fail (del_target == NULL, WS4D_ERR);

  err = ws4d_eprlist_remove (&ts->targets, del_target);
  ws4d_fail (err != WS4D_OK, err);

  return ws4d_eprlist_free (&ts->targets, del_target);
}


struct ws4d_targetservice_matchingdata
{
  int scope_matches_always;
  int type_matches_always;
  struct ws4d_stringlist scopesToMatch;
  ws4d_stringlist_matchingfunction scope_match_func;
  struct ws4d_qnamelist typesToMatch;
};

static int
ws4d_targetservice_matchingfunction (struct ws4d_epr *target,
                                     void *matchingdata, int iterations)
{
  struct ws4d_targetservice_matchingdata *mdata = matchingdata;
  int scope_matches = 0;
  int type_matches = 0;

  WS4D_UNUSED_PARAM (iterations);

  /* don't test inactive targets */
  if (!ws4d_targetep_isactive (target))
    {
      return 0;
    }

  /* first match Scope */
  scope_matches = mdata->scope_matches_always
    || ws4d_targetep_matches_Scopes (target, &mdata->scopesToMatch,
                                     mdata->scope_match_func);

  /* then match Type */
  type_matches = mdata->type_matches_always
    || ws4d_targetep_matches_Types (target, &mdata->typesToMatch);

  return type_matches && scope_matches;
}

int
ws4d_targetservice_getmatches (struct ws4d_targetservice *ts,
                               const char *Scopes,
                               const char *MatchBy,
                               const char *Types,
                               struct ws4d_abs_eprlist *matches)
{
  struct ws4d_targetservice_matchingdata mdata;
  int err;

  ws4d_assert (ts && matches, WS4D_ERR);

  /* Scopes and Types match always if NULL or "" */
  mdata.scope_matches_always = (Scopes == NULL)
    || (Scopes[0] == '\0') ? 1 : 0;
  mdata.type_matches_always = (Types == NULL) || (Types[0] == '\0') ? 1 : 0;

  if (mdata.scope_matches_always == 0)
    {
      struct ws4d_ts_matchby *matchby;
      /* we have to match scopes */
      ws4d_stringlist_init3 (&mdata.scopesToMatch, Scopes);

      matchby = ws4d_targetservice_getMatchBy (ts, MatchBy);
      mdata.scope_match_func =
        matchby != NULL ? matchby->scope_match_func : NULL;
    }

  if (mdata.type_matches_always == 0)
    {
      /* we have to match types */
      ws4d_qnamelist_init2 (&mdata.typesToMatch, Types);
    }

  err = ws4d_eprlist_filter (matches, &ts->targets,
                             ws4d_targetservice_matchingfunction, &mdata,
                             NULL);

  if (mdata.scope_matches_always == 0)
    {
      ws4d_stringlist_done (&mdata.scopesToMatch);
    }

  if (mdata.type_matches_always == 0)
    {
      ws4d_qnamelist_done (&mdata.typesToMatch);
    }

  return err;
}

int
ws4d_targetservice_inc_MessageNumber (struct ws4d_targetservice *ts)
{
  ws4d_assert (ts, WS4D_ERR);

  ts->as.MessageNumber++;

  return WS4D_OK;
}

struct ws4d_appsequence *
ws4d_targetservice_get_appsequence (struct ws4d_targetservice *ts)
{
  ws4d_assert (ts, NULL);

  return &ts->as;
}
