/* utilizable services manager service - managing service references on devices
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * author: Elmar Zeeb
 */

#include "ws4d_misc.h"
#include "ws4d_abstract_eprlist.h"
#include "ws4d_eprllist.h"
#include "ws4d_target.h"

#include "ws4d_device_tracker.h"

const char *ws4d_dtracker_id = "WS4D DeviceTracker V 0.1";

struct ws4d_dtracker_data
{
  struct ws4d_abs_eprlist devices, new, to_check;
  ws4d_qnamelist *types;
  const char *scopes;
  struct ws4d_dtracker_hooks *hooks;
  void *hooks_context;
};

#define _ws4d_dtracker_getdata(dtrackerp) \
    (dtrackerp && (dtrackerp->id == ws4d_dtracker_id)) ? (struct ws4d_dtracker_data *) dtrackerp->data : NULL

static int
ws4d_dtracker_newdevice (void *context, struct ws4d_epr *device)
{
  struct ws4d_dtracker *self = context;
  struct ws4d_dtracker_data *data = _ws4d_dtracker_getdata (self);

  ws4d_assert (data, WS4D_ERR);

  /* TODO: scope matching */
  if (ws4d_targetep_matches_Types (device, data->types))
    {
      /* TODO: check return value */
      ws4d_dtracker_adddevice (self, device);
      ws4d_tw_addtotc (device);
    }
  else
    {
      /* TODO: check return value */
      ws4d_eprlist_add2 (&data->to_check, device);
      ws4d_tw_set_mode (self, WS4D_TRACKER_DOWORK);
    }

  return WS4D_OK;
}

static int
ws4d_dtracker_invaldiatedevice (void *context, struct ws4d_epr *device)
{
  struct ws4d_dtracker *self = context;

  WS4D_UNUSED_PARAM (device);

  ws4d_tw_set_mode (self, WS4D_TRACKER_DOWORK);

  return WS4D_OK;
}

static void
ws4d_dtracker_dowork_cb (void *context)
{
  register struct ws4d_epr *cur, *next;
  struct ws4d_dtracker *self = context;
  struct ws4d_dtracker_data *data = _ws4d_dtracker_getdata (self);

  /* test parameters */
  ws4d_assert (data,);

  /* callback for invalid devices */
  ws4d_eprlist_lock (&data->devices);
  ws4d_eprlist_foreach (cur, next, &data->devices)
  {
    if (!ws4d_epr_isvalid (cur))
      {
        if (data->hooks && data->hooks->InvalidateDevice)
          {
            data->hooks->InvalidateDevice (data->hooks_context, cur);
          }

        /* remove device */
        ws4d_eprlist_remove (&data->devices, cur);
        ws4d_eprlist_free (&data->devices, cur);
      }
  }
  ws4d_eprlist_unlock (&data->devices);

  /* check unchecked devices */
  ws4d_eprlist_lock (&data->to_check);
  ws4d_eprlist_foreach (cur, next, &data->to_check)
  {
    if (ws4d_tw_dodprobe (cur, data->types, data->scopes, 1000) == WS4D_OK)
      {
        ws4d_eprlist_add2 (&data->new, cur);

        /* remove device from to_check list */
        ws4d_eprlist_remove (&data->to_check, cur);
        ws4d_eprlist_free (&data->to_check, cur);
      }
  }
  ws4d_eprlist_unlock (&data->to_check);

  /* add new devices to devices list and trigger callbacks */
  ws4d_eprlist_lock (&data->new);
  ws4d_eprlist_foreach (cur, next, &data->new)
  {
    struct ws4d_epr *new_device;

    ws4d_eprlist_lock (&data->devices);

    if (ws4d_eprlist_add2 (&data->devices, cur) != WS4D_OK)
      {
        ws4d_eprlist_unlock (&data->devices);
        continue;
      }

    new_device =
      ws4d_eprlist_get_byAddr (&data->devices, ws4d_epr_get_Addrs (cur));
    ws4d_tw_getfromtc (ws4d_epr_get_Addrs (cur), new_device);

    ws4d_eprlist_unlock (&data->devices);

    if (new_device && data->hooks && data->hooks->NewDevice)
      {
        data->hooks->NewDevice (data->hooks_context, new_device);
      }

    /* remove device from new devices list */
    ws4d_eprlist_remove (&data->new, cur);
    ws4d_eprlist_free (&data->new, cur);
  }
  ws4d_eprlist_unlock (&data->new);

  ws4d_tw_clr_mode (self, WS4D_TRACKER_DOWORK);
}

static void
ws4d_dtracker_delete_cb (void *context)
{
  struct ws4d_dtracker *self = context;
  struct ws4d_dtracker_data *data = _ws4d_dtracker_getdata (self);

  /* test parameters */
  ws4d_assert (data,);

  ws4d_eprlist_done (&data->devices);
  ws4d_eprlist_done (&data->new);
  ws4d_eprlist_done (&data->to_check);
}

struct ws4d_trackerworker_hooks _ws4d_dtracker_hooks =
  { ws4d_dtracker_newdevice, ws4d_dtracker_invaldiatedevice,
  ws4d_dtracker_dowork_cb, ws4d_dtracker_delete_cb
};

static int
_ws4d_dtracker_init (struct ws4d_dtracker *dtracker)
{
  struct ws4d_dtracker_data *data;

  data = ws4d_malloc_alist (sizeof (struct ws4d_dtracker_data),
                            &dtracker->alist);
  ws4d_fail (data == NULL, WS4D_EOM);

  dtracker->id = ws4d_dtracker_id;
  ws4d_eprlist_init (&data->devices, ws4d_eprllist_init, NULL);
  ws4d_eprlist_init (&data->new, ws4d_eprllist_init, NULL);
  ws4d_eprlist_init (&data->to_check, ws4d_eprllist_init, NULL);
  dtracker->data = data;

  return WS4D_OK;
}

int
ws4d_dtracker_init (struct ws4d_dtracker *dtracker, ws4d_qnamelist * types,
                    const char *scopes, struct ws4d_dtracker_hooks *hooks,
                    void *hooks_context)
{
  int err;
  struct ws4d_dtracker_data *data;

  err = ws4d_tw_add_dtracker (dtracker, _ws4d_dtracker_init);
  ws4d_fail (err != WS4D_OK, err);

  dtracker->hooks = &_ws4d_dtracker_hooks;
  data = _ws4d_dtracker_getdata (dtracker);
  ws4d_fail (data == NULL, WS4D_ERR);

  data->hooks = hooks;
  data->hooks_context = hooks_context;
  data->types = types;
  data->scopes = scopes;

  return WS4D_OK;
}

int
ws4d_dtracker_done (struct ws4d_dtracker *dtracker)
{
  return ws4d_tw_del_dtracker (dtracker);
}

int
ws4d_dtracker_adddevice (struct ws4d_dtracker *dtracker,
                         struct ws4d_epr *device)
{
  struct ws4d_dtracker_data *data = _ws4d_dtracker_getdata (dtracker);

  /* test parameters */
  ws4d_assert (data, WS4D_EPARAM);

  /* trigger dowork flag */
  ws4d_tw_set_mode (dtracker, WS4D_TRACKER_DOWORK);

  return ws4d_eprlist_add2 (&data->new, device);
}

struct ws4d_abs_eprlist *
ws4d_dtracker_getdevices (struct ws4d_dtracker *dtracker)
{
  struct ws4d_dtracker_data *data = _ws4d_dtracker_getdata (dtracker);

  /* test parameters */
  ws4d_assert (data, NULL);

  return &data->devices;
}

struct ws4d_epr *
ws4d_dtracker_getdevice (struct ws4d_dtracker *dtracker, const char *Addrs)
{
  struct ws4d_dtracker_data *data = _ws4d_dtracker_getdata (dtracker);

  /* test parameters */
  ws4d_assert (data, NULL);

  return ws4d_eprlist_get_byAddr (&data->devices, Addrs);
}

int
ws4d_dtracker_doProbe (struct ws4d_dtracker *dtracker, ws4d_time timeout)
{
  struct ws4d_dtracker_data *data = _ws4d_dtracker_getdata (dtracker);

  /* test parameters */
  ws4d_assert (data, WS4D_EPARAM);

  return ws4d_tw_doprobe (data->types, data->scopes, timeout, 100);
}

struct ws4d_dtracker_hooks *
ws4d_dtracker_sethooks (struct ws4d_dtracker *dtracker,
                        struct ws4d_dtracker_hooks *new_hooks)
{
  struct ws4d_dtracker_data *data = _ws4d_dtracker_getdata (dtracker);
  struct ws4d_dtracker_hooks *old_hooks = NULL;

  /* test parameters */
  ws4d_assert (data && new_hooks, NULL);

  old_hooks = data->hooks;
  data->hooks = new_hooks;

  return old_hooks;
}

void *
ws4d_dtracker_sethooks_context (struct ws4d_dtracker *dtracker,
                                void *new_hooks_contect)
{
  struct ws4d_dtracker_data *data = _ws4d_dtracker_getdata (dtracker);
  void *old_hooks_contect = NULL;

  /* test parameters */
  ws4d_assert (data && new_hooks_contect, NULL);

  old_hooks_contect = data->hooks_context;
  data->hooks_context = new_hooks_contect;

  return old_hooks_contect;
}
