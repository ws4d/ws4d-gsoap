/* utilizable services manager service - managing service references on devices
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * author: Elmar Zeeb
 */

#include "ws4d_misc.h"
#include "ws4d_abstract_eprlist.h"
#include "ws4d_eprllist.h"
#include "ws4d_target.h"

#include "ws4d_device_tracker.h"
#include "ws4d_service_tracker.h"

const char *ws4d_stracker_id = "WS4D ServiceTracker V 0.1";

struct ws4d_stracker_data
{
  struct ws4d_abs_eprlist services, unchecked_devices;
  ws4d_qnamelist *types;
  struct ws4d_dtracker *dtracker;
  struct ws4d_dtracker_hooks *old_dtracker_hooks;
  void *old_dtracker_hooks_context;
  struct ws4d_stracker_hooks *hooks;
  void *hooks_context;
};

#define _ws4d_stracker_getdata(strackerp) \
    ((strackerp) && ((strackerp)->id == ws4d_stracker_id)) ? (struct ws4d_stracker_data *) (strackerp)->data : NULL

static int
ws4d_stracker_newdevice (void *context, struct ws4d_epr *device)
{
  struct ws4d_stracker *self = context;
  struct ws4d_stracker_data *data = _ws4d_stracker_getdata (self);

  ws4d_assert (data, WS4D_ERR);

  if (data->old_dtracker_hooks && data->old_dtracker_hooks->NewDevice)
    {
      data->old_dtracker_hooks->NewDevice (data->old_dtracker_hooks_context,
                                           device);
    }

  ws4d_eprlist_add2 (&data->unchecked_devices, device);
  ws4d_tw_set_mode (self, WS4D_TRACKER_DOWORK);

  return WS4D_OK;
}

static int
ws4d_stracker_invalidatedevice (void *context, struct ws4d_epr *device)
{
  struct ws4d_dtracker *self = context;
  struct ws4d_stracker_data *data = _ws4d_stracker_getdata (self);

  ws4d_assert (data, WS4D_ERR);

  if (data->old_dtracker_hooks && data->old_dtracker_hooks->InvalidateDevice)
    {
      data->old_dtracker_hooks->InvalidateDevice (data->
                                                  old_dtracker_hooks_context,
                                                  device);
    }

  ws4d_tw_set_mode (self, WS4D_TRACKER_DOWORK);

  return WS4D_OK;
}

static struct ws4d_dtracker_hooks ws4d_stracker_dtracker_hooks =
  { ws4d_stracker_newdevice, ws4d_stracker_invalidatedevice };

static void
ws4d_stracker_dowork_cb (void *context)
{
  register struct ws4d_epr *cur, *next;
  struct ws4d_stracker *self = context;
  struct ws4d_stracker_data *data = _ws4d_stracker_getdata (self);

  /* test parameters */
  ws4d_assert (data,);

  /* callback for invalid services */
  ws4d_eprlist_lock (&data->services);
  ws4d_eprlist_foreach (cur, next, &data->services)
  {
    if (!ws4d_epr_isvalid (cur))
      {
        if (data->hooks && data->hooks->InvalidateService)
          {
            data->hooks->InvalidateService (data->hooks_context, cur);
          }

        /* remove device */
        ws4d_eprlist_remove (&data->services, cur);
        ws4d_eprlist_free (&data->services, cur);
      }
  }
  ws4d_eprlist_unlock (&data->services);

  /* check unchecked devices */
  ws4d_eprlist_lock (&data->unchecked_devices);
  ws4d_eprlist_foreach (cur, next, &data->unchecked_devices)
  {
    struct ws4d_abs_eprlist new_services;
    struct ws4d_epr *dtracker_device =
      ws4d_dtracker_getdevice (data->dtracker, ws4d_epr_get_Addrs (cur));

    ws4d_eprlist_init (&new_services, ws4d_eprllist_init, NULL);
    if (ws4d_tw_dofindservices
        (dtracker_device, data->types, 1000, &new_services) == WS4D_OK)
      {
        /* add new services to service list and call hooks */
        register struct ws4d_epr *curs, *nexts;
        ws4d_eprlist_foreach (curs, nexts, &new_services)
        {
          ws4d_stracker_addservice (self, curs);
        }
      }
    ws4d_eprlist_done (&new_services);

    /* remove device from to_check list */
    ws4d_eprlist_remove (&data->unchecked_devices, cur);
    ws4d_eprlist_free (&data->unchecked_devices, cur);
  }
  ws4d_eprlist_unlock (&data->unchecked_devices);

  ws4d_tw_clr_mode (self, WS4D_TRACKER_DOWORK);
}

static void
ws4d_stracker_delete_cb (void *context)
{
  struct ws4d_stracker *self = context;
  struct ws4d_stracker_data *data = _ws4d_stracker_getdata (self);

  /* test parameters */
  ws4d_assert (data,);

  ws4d_eprlist_done (&data->unchecked_devices);
  ws4d_eprlist_done (&data->services);
}

static struct ws4d_trackerworker_hooks _ws4d_stracker_hooks =
  { NULL, NULL, ws4d_stracker_dowork_cb, ws4d_stracker_delete_cb };

static int
_ws4d_stracker_init (struct ws4d_stracker *stracker)
{
  struct ws4d_stracker_data *data;

  data = ws4d_malloc_alist (sizeof (struct ws4d_stracker_data),
                            &stracker->alist);
  ws4d_fail (data == NULL, WS4D_EOM);

  stracker->id = ws4d_stracker_id;
  ws4d_eprlist_init (&data->services, ws4d_eprllist_init, NULL);
  ws4d_eprlist_init (&data->unchecked_devices, ws4d_eprllist_init, NULL);
  stracker->data = data;

  return WS4D_OK;
}

int
ws4d_stracker_init (struct ws4d_stracker *stracker,
                    struct ws4d_dtracker *dtracker, ws4d_qnamelist * types,
                    struct ws4d_stracker_hooks *hooks, void *hooks_context)
{
  int err;
  struct ws4d_stracker_data *data;

  err = ws4d_tw_add_stracker (stracker, _ws4d_stracker_init);
  ws4d_fail (err != WS4D_OK, err);

  stracker->hooks = &_ws4d_stracker_hooks;
  data = _ws4d_stracker_getdata (stracker);

  if (data)
    {
      data->hooks = hooks;
      data->hooks_context = hooks_context;
      data->types = types;
      data->dtracker = dtracker;
      data->old_dtracker_hooks = ws4d_dtracker_sethooks (dtracker,
                                                         &ws4d_stracker_dtracker_hooks);
      data->old_dtracker_hooks_context =
        ws4d_dtracker_sethooks_context (dtracker, stracker);
    }

  return err;
}

int
ws4d_stracker_done (struct ws4d_stracker *stracker)
{
  return ws4d_tw_del_stracker (stracker);
}

int
ws4d_stracker_addservice (struct ws4d_stracker *stracker,
                          struct ws4d_epr *service)
{
  int err;
  struct ws4d_stracker_data *data = _ws4d_stracker_getdata (stracker);
  struct ws4d_epr *new_service;

  /* test parameters */
  ws4d_assert (data, WS4D_EPARAM);

  err = ws4d_eprlist_add2 (&data->services, service);
  ws4d_fail (err != WS4D_OK, err);

  new_service = ws4d_eprlist_get_byAddr (&data->services,
                                         ws4d_epr_get_Addrs (service));
  ws4d_assert (new_service, WS4D_ERR);

  if (data->hooks && data->hooks->NewService)
    {
      data->hooks->NewService (data->hooks_context, new_service);
    }

  return WS4D_OK;
}

struct ws4d_abs_eprlist *
ws4d_stracker_getservices (struct ws4d_stracker *stracker)
{
  struct ws4d_stracker_data *data = _ws4d_stracker_getdata (stracker);

  /* test parameters */
  ws4d_assert (data, NULL);

  return &data->services;
}
