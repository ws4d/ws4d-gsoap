/* utilizable services manager service - managing service references on devices
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * author: Elmar Zeeb
 */

#include "dpws_peer.h"
#include "ws4d_misc.h"
#include "ws4d_eprllist.h"
#include "ws4d_targetcache.h"

#include "ws4d_device_tracker.h"

struct dpws_s *ws4d_tracker_peer = NULL;
struct ws4d_tc_int ws4d_tracker_tc = { NULL, NULL };

struct ws4d_list_node ws4d_dtrackers_list;
struct ws4d_list_node ws4d_strackers_list;
WS4D_MUTEX (ws4d_tracker_mutex);

static void
ws4d_tw_newdevice (struct soap *soap, struct ws4d_epr *device)
{
  const char *address;
  WS4D_UNUSED_PARAM (soap);

  address = ws4d_epr_get_Addrs (device);

  /* TODO: should check if device found itself */
  if (address) /* && strcmp (address, ws4d_tracker_peer->hosting_addr)) */
    {
      register struct ws4d_dtracker *cur, *next;

      ws4d_mutex_lock (&ws4d_tracker_mutex);
      ws4d_list_foreach (cur, next, &ws4d_dtrackers_list,
                         struct ws4d_dtracker, list)
      {
        if (cur->hooks && cur->hooks->NewDevice)
          {
            cur->hooks->NewDevice (cur, device);
          }
      }
      ws4d_mutex_unlock (&ws4d_tracker_mutex);
    }
}

static void
ws4d_tw_invalidatedevice (struct soap *soap, struct ws4d_epr *device)
{
  WS4D_UNUSED_PARAM (soap);

  if (device && ws4d_epr_get_Addrs (device))
    {
      register struct ws4d_dtracker *cur, *next;

      ws4d_mutex_lock (&ws4d_tracker_mutex);
      ws4d_list_foreach (cur, next, &ws4d_dtrackers_list,
                         struct ws4d_dtracker, list)
      {
        if (cur->hooks && cur->hooks->InvalidateDevice)
          {
            cur->hooks->InvalidateDevice (cur, device);
          }
      }
      ws4d_mutex_unlock (&ws4d_tracker_mutex);
    }
}

struct dpws_discovery_hooks ws4d_tracker_discovery_hooks =
  { ws4d_tw_newdevice, ws4d_tw_invalidatedevice };

struct ws4d_tc_int *
ws4d_tw_create_tc (void *arg)
{
  int err;

  /* test if tracker is already initialized */
  ws4d_assert ((ws4d_tracker_tc.id == NULL)
               && (ws4d_tracker_tc.data == NULL), NULL);

  /* create target cache */
  err = ws4d_tc_init (&ws4d_tracker_tc, arg);
  ws4d_fail (err != WS4D_OK, NULL);

  return &ws4d_tracker_tc;
}

int
ws4d_tw_init (struct dpws_s *peer)
{
  /* test parameters */
  ws4d_assert (peer, WS4D_ERR);

  /* fail if tracker is already initialized */
  ws4d_fail (ws4d_tracker_peer, WS4D_ERR);

  ws4d_tracker_peer = peer;
  WS4D_INIT_LIST (&ws4d_dtrackers_list);
  WS4D_INIT_LIST (&ws4d_strackers_list);
  ws4d_mutex_init (&ws4d_tracker_mutex);

  return dpws_init_implicit_discovery2 (ws4d_tracker_peer, &ws4d_tracker_tc,
                                        NULL, &ws4d_tracker_discovery_hooks);
}

int
ws4d_tw_done (void)
{
  register struct ws4d_dtracker *dcur, *dnext;
  register struct ws4d_stracker *scur, *snext;

  ws4d_list_foreach (dcur, dnext, &ws4d_dtrackers_list, struct ws4d_dtracker,
                     list)
  {
    ws4d_tw_del_dtracker (dcur);
  }
  ws4d_list_foreach (scur, snext, &ws4d_strackers_list, struct ws4d_stracker,
                     list)
  {
    ws4d_tw_del_stracker (scur);
  }
  ws4d_mutex_destroy (&ws4d_tracker_mutex);

  return WS4D_OK;
}

int
ws4d_tw_doprobe (ws4d_qnamelist * Types, const char *Scopes,
                 ws4d_time timeout, int backlog)
{
  return dpws_probe (ws4d_tracker_peer, Types, Scopes, timeout, backlog,
                     &ws4d_tracker_discovery_hooks, &ws4d_tracker_tc, NULL);
}

int
ws4d_tw_dodprobe (struct ws4d_epr *device, ws4d_qnamelist * Types,
                  const char *Scopes, ws4d_time timeout)
{
  return dpws_dprobe (ws4d_tracker_peer, device, Types, Scopes,
                      &ws4d_tracker_tc, timeout);
}

int
ws4d_tw_dofindservices (struct ws4d_epr *device, ws4d_qnamelist * Types,
                        ws4d_time timeout, struct ws4d_abs_eprlist *result)
{
  return dpws_find_services (ws4d_tracker_peer, device, Types, timeout,
                             result);
}

int
ws4d_tw_add_dtracker (struct ws4d_dtracker *dtracker, ws4d_dt_init_cb init_cb)
{
  int err;

  /* test parameters */
  ws4d_assert (dtracker && init_cb, WS4D_EPARAM);

  memset (dtracker, 0, sizeof (struct ws4d_dtracker));
  WS4D_ALLOCLIST_INIT (&dtracker->alist);

  err = init_cb (dtracker);
  ws4d_fail (err != WS4D_OK, err);

  ws4d_mutex_lock (&ws4d_tracker_mutex);
  ws4d_list_add_tail (&dtracker->list, &ws4d_dtrackers_list);
  ws4d_mutex_unlock (&ws4d_tracker_mutex);

  return err;
}

int
ws4d_tw_del_dtracker (struct ws4d_dtracker *dtracker)
{
  /* test parameters */
  ws4d_assert (dtracker, WS4D_EPARAM);

  ws4d_mutex_lock (&ws4d_tracker_mutex);
  ws4d_list_del (&dtracker->list);
  ws4d_mutex_unlock (&ws4d_tracker_mutex);

  if (dtracker->hooks->Delete)
    {
      dtracker->hooks->Delete (dtracker);
    }

  ws4d_alloclist_done (&dtracker->alist);
  memset (dtracker, 0, sizeof (struct ws4d_dtracker));

  return WS4D_OK;
}

int
ws4d_tw_add_stracker (struct ws4d_stracker *stracker, ws4d_st_init_cb init_cb)
{
  int err;

  /* test parameters */
  ws4d_assert (stracker && init_cb, WS4D_EPARAM);

  memset (stracker, 0, sizeof (struct ws4d_stracker));
  WS4D_ALLOCLIST_INIT (&stracker->alist);

  err = init_cb (stracker);
  ws4d_fail (err != WS4D_OK, err);

  ws4d_mutex_lock (&ws4d_tracker_mutex);
  ws4d_list_add_tail (&stracker->list, &ws4d_strackers_list);
  ws4d_mutex_unlock (&ws4d_tracker_mutex);

  return err;
}

int
ws4d_tw_del_stracker (struct ws4d_stracker *stracker)
{
  /* test parameters */
  ws4d_assert (stracker, WS4D_EPARAM);

  ws4d_mutex_lock (&ws4d_tracker_mutex);
  ws4d_list_del (&stracker->list);
  ws4d_mutex_unlock (&ws4d_tracker_mutex);

  if (stracker->hooks->Delete)
    {
      stracker->hooks->Delete (stracker);
    }

  ws4d_alloclist_done (&stracker->alist);
  memset (stracker, 0, sizeof (struct ws4d_stracker));

  return WS4D_OK;
}

int
ws4d_tw_dowork (void)
{
  register struct ws4d_dtracker *dcur, *dnext;
  register struct ws4d_stracker *scur, *snext;

  ws4d_mutex_lock (&ws4d_tracker_mutex);
  ws4d_list_foreach (dcur, dnext, &ws4d_dtrackers_list, struct ws4d_dtracker,
                     list)
  {
    if (ws4d_tw_check_mode (dcur, WS4D_TRACKER_DOWORK) && dcur->hooks
        && dcur->hooks->DoWork)
      {
        dcur->hooks->DoWork (dcur);
      }
  }
  ws4d_list_foreach (scur, snext, &ws4d_strackers_list, struct ws4d_stracker,
                     list)
  {
    if (ws4d_tw_check_mode (scur, WS4D_TRACKER_DOWORK) && scur->hooks
        && scur->hooks->DoWork)
      {
        scur->hooks->DoWork (scur);
      }
  }
  ws4d_mutex_unlock (&ws4d_tracker_mutex);

  return WS4D_OK;
}

int
ws4d_tw_addtotc (struct ws4d_epr *device)
{
  ws4d_tc_checkadd_epr (&ws4d_tracker_tc, ws4d_epr_get_Addrs (device),
                        ws4d_targetep_get_MetadataVersion (device));

  return WS4D_OK;
}

int
ws4d_tw_getfromtc (const char *addr, struct ws4d_epr *result)
{
  return ws4d_tc_get_byAddr (&ws4d_tracker_tc, addr, result);
}
