/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "ws4d_misc.h"
#ifndef WIN32
#include <sys/time.h>
#endif
#include <time.h>

int
ws4d_dur_to_s (struct ws4d_dur *dur, ws4d_time * s)
{
  /* test parameters */
  ws4d_assert (dur && s, WS4D_ERR);

  /* calculate seconds */
  *s = ((31536000 * dur->years) +
        (2592000 * dur->months) +
        (86400 * dur->days) + (3600 * dur->hours) +
        (60 * dur->minutes) + dur->seconds);

  return WS4D_OK;
}

int
ws4d_s_to_dur (ws4d_time s, struct ws4d_dur *dur)
{
  /* test parameters */
  ws4d_assert (dur, WS4D_ERR);

  /* calculate duration */
  dur->years = s / 31536000;
  s = s % 31536000;
  dur->months = s / 2592000;
  s = s % 2592000;
  dur->days = s / 86400;
  s = s % 86400;
  dur->hours = s / 3600;
  s = s % 3600;
  dur->minutes = s / 60;
  s = s % 60;
  dur->seconds = s;

  return WS4D_OK;
}

int
ws4d_xsddt_to_dur (const char *xsddt, struct ws4d_dur *dur)
{
  char *period = NULL;
  char *dur_time = NULL;

  /* test parameters */
  ws4d_assert (xsddt && dur, WS4D_ERR);

  /* extract period and time from string */
  period = strchr (xsddt, 'P');
  dur_time = strchr (xsddt, 'T');

  /* parse period */
  if (period)
    {
      int cur = 0, last = 0;

      period++;

      while ((period[cur] != '\0') && (period[cur] != 'T'))
        {
          if (isdigit (period[cur]))
            {
              cur++;
            }
          else if (period[cur] == 'Y')
            {
              period[cur] = '\0';
              dur->years = atoi (period + last);
              cur++;
              last = cur;
            }
          else if (period[cur] == 'M')
            {
              period[cur] = '\0';
              dur->months = atoi (period + last);
              cur++;
              last = cur;
            }
          else if (period[cur] == 'D')
            {
              period[cur] = '\0';
              dur->days = atoi (period + last);
              cur++;
              last = cur;
            }
          else
            {
              /* parse error */
              return WS4D_ERR;
            }
        }
    }

  /* parse time */
  if (dur_time)
    {
      int cur = 0, last = 0;

      dur_time++;

      while (dur_time[cur] != '\0')
        {
          if (isdigit (dur_time[cur]))
            {
              cur++;
            }
          else if (dur_time[cur] == 'H')
            {
              dur_time[cur] = '\0';
              dur->hours = atoi (dur_time + last);
              cur++;
              last = cur;
            }
          else if (dur_time[cur] == 'M')
            {
              dur_time[cur] = '\0';
              dur->minutes = atoi (dur_time + last);
              cur++;
              last = cur;
            }
          else if (dur_time[cur] == 'S')
            {
              dur_time[cur] = '\0';
              dur->seconds = atoi (dur_time + last);
              cur++;
              last = cur;
            }
          else
            {
              /* parse error */
              return WS4D_ERR;
            }
        }
    }

  return WS4D_OK;
}

int
ws4d_dur_to_xsddt (struct ws4d_dur *dur, char *buf, int buf_len)
{
  int ret;

  /* test parameters */
  ws4d_assert (dur && buf && (buf_len > 0), WS4D_ERR);

  /* convert duration structure to xsd:datetime format */
  ret = SNPRINTF (buf, buf_len,
                  "P%dY%dM%dDT%dH%dM%dS", dur->years,
                  dur->months, dur->days, dur->hours, dur->minutes,
                  dur->seconds);
  ws4d_fail (ret > buf_len, WS4D_ERR);

  return WS4D_OK;
}

#ifndef WITH_OTHER_ARCH
ws4d_time
ws4d_systime_ms (void)
{
#ifndef WIN32
  struct timeval tv;
  gettimeofday (&tv, (struct timezone *) 0);

  return (ws4d_time) (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
#else
  return (ws4d_time) GetTickCount ();
#endif
}

ws4d_time
ws4d_systime_s (void)
{
#ifndef WIN32
  struct timeval tv;
  gettimeofday (&tv, (struct timezone *) 0);

  return (ws4d_time) tv.tv_sec;
#else
  return (ws4d_time) GetTickCount () / 1000;
#endif
}
#endif

const char *ws4d_uuid_schema_prefix = "urn:uuid:";
const char *ws4d_uuid_schema_format = "urn:uuid:%s";

static unsigned char
ws4d_generaterandhex ()
{
  int r = rand () % 15;
  return (r > 9) ? (r - 9) + 'a' : (r) + '0';
}

char *
ws4d_uuid_generate_random (char *buffer)
{
  register int i;
  char uuid_dig19_table[4] = { '8', '9', 'a', 'b' };

  /* test parameters */
  ws4d_assert (buffer, NULL);

  /* random seed */
  srand ((unsigned int) ws4d_systime_ms ());

  /* set random hex for specific digits */
  for (i = 0; i < (WS4D_UUID_SIZE - 1); i++)
    {
      if ((i != 8) && (i != 13) && (i != 14) && (i != 18) && (i != 19)
          && (i != 23))
        {
          buffer[i] = ws4d_generaterandhex ();
        }
    }

  /* set dashes and digits for remaining parts */
  buffer[8] = '-';
  buffer[13] = '-';
  buffer[14] = '4';
  buffer[18] = '-';
  buffer[19] = uuid_dig19_table[rand () % 3];
  buffer[23] = '-';

  /* terminate string */
  buffer[WS4D_UUID_SIZE - 1] = '\n';

  return buffer;
}

char *
ws4d_uuid_generate_schema(char *buffer, const char *uuid)
{
  /* test parameters */
  ws4d_assert (buffer && uuid, NULL);

  memcpy(buffer, WS4D_UUID_SCHEMA_PREFIX, WS4D_UUID_SCHEMA_PREFIX_LEN);
  memcpy(buffer + WS4D_UUID_SCHEMA_PREFIX_LEN, uuid, WS4D_UUID_SIZE);
  buffer[WS4D_UUID_SCHEMA_SIZE -1] = '\0';

  return buffer;
}

/*
** Translation Table as described in RFC1113
*/
static const char cb64[] =
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/* maybe, http://devenix.wordpress.com/2008/01/18/howto-base64-encode-and-decode-with-c-and-openssl-2/
 * gives a lead to how to do base64-de/encoding with OpenSSL... if this is preferable...
 */

/*
** encodeblock
**
** encode 3 8-bit binary bytes as 4 '6-bit' characters
*/
void
encodeblock (unsigned char in[3], unsigned char out[4], int len)
{
  out[0] = cb64[in[0] >> 2];
  out[1] = cb64[((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4)];
  out[2] =
    (unsigned char) (len >
                     1 ? cb64[((in[1] & 0x0f) << 2) | ((in[2] & 0xc0) >> 6)] :
                     '=');
  out[3] = (unsigned char) (len > 2 ? cb64[in[2] & 0x3f] : '=');
}

/**
 * Function encodes a binary array to base64
 *
 * @param pointer to unsigned char array containing binary data
 *
 * @return pointer to created array containing base64-coded data
 */
unsigned char *
ws4d_bin2b64 (unsigned char *binary)
{
  unsigned char in[3], out[4];
  int loop = 0, i, len, blocksout = 0;
  unsigned char *base64 = NULL;

  while (binary[loop * 3])
    {
      in[0] = in[1] = in[2] = '\0';
      len = 0;
      while (len < 3 && binary[3 * loop + len] != '\0')
        {
          in[len] = binary[loop * 3 + len];
          len++;
        }

      if (len)
        {
          encodeblock (in, out, len);
          base64 =
            (unsigned char *) realloc (base64,
                                       (loop +
                                        1) * 4 * sizeof (unsigned char));
          for (i = 0; i < 4; i++)
            {
              base64[4 * loop + i] = out[i];
            }
          blocksout++;
        }

      loop++;

      if (len != 3)
        break;

    }
  base64 = (unsigned char *) realloc (base64, 4 * loop + 1);
  base64[4 * loop] = (unsigned char) 0;
  return base64;
}

/*
** decodeblock
**
** decode 4 '6-bit' characters into 3 8-bit binary bytes
*/
void
decodeblock (unsigned char in[4], unsigned char out[3])
{
  out[0] = (unsigned char) (in[0] << 2 | in[1] >> 4);
  out[1] = (unsigned char) (in[1] << 4 | in[2] >> 2);
  out[2] = (unsigned char) (((in[2] << 6) & 0xc0) | in[3]);
}

/**
 * Function decodes a base64-coded data
 *
 * @param pointer to unsigned char array containing base64-coded data
 *
 * @return pointer to created array containing binary data
 */
unsigned char *
ws4d_b642bin (unsigned char *base64)
{
  unsigned char in[4], out[3], v;
  int i, len, loop = 0;
  unsigned char *binary = (unsigned char *) NULL;

  /* if length is not a multiple of 4, base64 does not contain a valid
   * base64-coded string. So, in this case, we just return NULL
   */
  for (len = 0; base64[len]; len++);
  if ((int) (len % 4))
    return (unsigned char *) NULL;

  binary = (unsigned char *) malloc (len / 4 * 3 + 1);
  binary[len / 4 * 3] = '\0';   /* so most likely we use too many bytes... */

  /* further, we assume, we can only get 'clean' base64 - so no error-checks */
  while (base64[4 * loop] != '\0')
    {
      for (i = 0; i < 4; i++)
        {
          v = base64[loop * 4 + i];
          if (v >= 'A' && v <= 'Z')
            in[i] = (unsigned char) v - 'A';
          else if (v >= 'a' && v <= 'z')
            in[i] = (unsigned char) v - 71;
          else if (v >= '0' && v <= '9')
            in[i] = (unsigned char) v + 4;
          else if (v == '+')
            in[i] = (unsigned char) 62;
          else if (v == '/')
            in[i] = (unsigned char) 63;
          else if (v == '=')
            in[i] = 0;
          else                  /* Something went wrong */
            return (unsigned char *) NULL;
        }

      decodeblock (in, out);
      for (i = 0; i < 3; i++)
        {
          binary[3 * loop + i] = out[i];
        }
      loop++;
    }
  return binary;
}


#ifndef HAVE_INET_PTON_H
int
ws4d_inet_pton (int af, const char *src, void *dst)
{
#ifdef WIN32
  unsigned long *in4 = NULL;
#else
  struct in_addr *in4 = NULL;
#endif

  ws4d_assert (src && dst, -1);

  switch (af)
    {
    case AF_INET:
      in4 = dst;
      *in4 = inet_addr (src);
      return *in4 != INADDR_NONE;
      break;
    default:
      return -1;
    }
}
#endif

#ifndef HAVE_INET_NTOP_H
const char *
ws4d_inet_ntop (int af, const void *src, char *dst, size_t size)
{
  char *result;
#ifdef WIN32
  struct in_addr *in4 = (void *) src;
#else
  in_addr_t *in4 = src;
#endif

  ws4d_assert (src && dst && (size > 0), NULL);

  switch (af)
    {
    case AF_INET:
      result = inet_ntoa (*in4);
      if (result != NULL)
        {
          if (strlen (result) < size)
            {
              strncpy (dst, result, size);
              return dst;
            }
        }
      return NULL;
      break;
    default:
      return NULL;
    }
}
#endif

#if !defined(HAVE_GETIFADDRS_H) && !defined(HAVE_FREEIFADDRS_H)

#include <alloca.h>
#include <assert.h>
#include <errno.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netpacket/packet.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>
#include <asm/types.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

struct netlink_res
{
  struct netlink_res *next;
  struct nlmsghdr *nlh;
  size_t size;			/* Size of response.  */
  uint32_t seq;			/* sequential number we used.  */
};


struct netlink_handle
{
  int fd;			/* Netlink file descriptor.  */
  pid_t pid;			/* Process ID.  */
  uint32_t seq;			/* The sequence number we use currently.  */
  struct netlink_res *nlm_list;	/* Pointer to list of responses.  */
  struct netlink_res *end_ptr;	/* For faster append of new entries.  */
};

/* There is a problem with this type.  The address length for
   Infiniband sockets is much longer than the 8 bytes allocated in the
   sockaddr_ll definition.  Hence we use here a special
   definition.  */
struct sockaddr_ll_max
  {
    unsigned short int sll_family;
    unsigned short int sll_protocol;
    int sll_ifindex;
    unsigned short int sll_hatype;
    unsigned char sll_pkttype;
    unsigned char sll_halen;
    unsigned char sll_addr[24];
  };

/* struct to hold the data for one ifaddrs entry, so we can allocate
   everything at once.  */
struct ifaddrs_storage
{
  struct ifaddrs ifa;
  union
  {
    /* Save space for the biggest of the four used sockaddr types and
       avoid a lot of casts.  */
    struct sockaddr sa;
    struct sockaddr_ll_max sl;
    struct sockaddr_in s4;
    struct sockaddr_in6 s6;
  } addr, netmask, broadaddr;
  char name[IF_NAMESIZE + 1];
};

static void
__netlink_free_handle (struct netlink_handle *h)
{
  struct netlink_res *ptr;

  ptr = h->nlm_list;
  while (ptr != NULL)
    {
      struct netlink_res *tmpptr;

      tmpptr = ptr->next;
      free (ptr);
      ptr = tmpptr;
    }
}

static int
__netlink_sendreq (struct netlink_handle *h, int type)
{
  struct req
  {
    struct nlmsghdr nlh;
    struct rtgenmsg g;
    char pad[0];
  } req;
  struct sockaddr_nl nladdr;

  if (h->seq == 0)
    h->seq = time (NULL);

  req.nlh.nlmsg_len = sizeof (req);
  req.nlh.nlmsg_type = type;
  req.nlh.nlmsg_flags = NLM_F_ROOT | NLM_F_MATCH | NLM_F_REQUEST;
  req.nlh.nlmsg_pid = 0;
  req.nlh.nlmsg_seq = h->seq;
  req.g.rtgen_family = AF_UNSPEC;
  if (sizeof (req) != offsetof (struct req, pad))
  {
    memset (req.pad, '\0', sizeof (req) - offsetof (struct req, pad));
  }

  memset (&nladdr, '\0', sizeof (nladdr));
  nladdr.nl_family = AF_NETLINK;

  return TEMP_FAILURE_RETRY (sendto (h->fd, (void *) &req, sizeof (req), 0,
                                       (struct sockaddr *) &nladdr,
                                       sizeof (nladdr)));
}

static int
__netlink_request (struct netlink_handle *h, int type)
{
  struct netlink_res *nlm_next;
  struct sockaddr_nl nladdr;
  struct nlmsghdr *nlmh;
  ssize_t read_len;
  bool done = false;
  char *buf;
  struct iovec iov;

  iov.iov_len = getpagesize();
  iov.iov_base = malloc (iov.iov_len);
      if (iov.iov_base == NULL) {
        goto out_fail;
  }

  if (__netlink_sendreq (h, type) < 0)
    goto out_fail;

  while (! done)
    {
      struct msghdr msg =
        {
          (void *) &nladdr, sizeof (nladdr),
          &iov, 1,
          NULL, 0,
          0
        };

      read_len = TEMP_FAILURE_RETRY (recvmsg (h->fd, &msg, 0));
      if (read_len < 0)
        goto out_fail;

      if (nladdr.nl_pid != 0)
        continue;

      if (__builtin_expect (msg.msg_flags & MSG_TRUNC, 0))
        goto out_fail;

      size_t count = 0;
      size_t remaining_len = read_len;
      for (nlmh = (struct nlmsghdr *) buf;
           NLMSG_OK (nlmh, remaining_len);
           nlmh = (struct nlmsghdr *) NLMSG_NEXT (nlmh, remaining_len))
        {
          if ((pid_t) nlmh->nlmsg_pid != h->pid
              || nlmh->nlmsg_seq != h->seq)
            continue;

          ++count;
          if (nlmh->nlmsg_type == NLMSG_DONE)
            {
              /* We found the end, leave the loop.  */
              done = true;
              break;
            }
          if (nlmh->nlmsg_type == NLMSG_ERROR)
            {
              struct nlmsgerr *nlerr = (struct nlmsgerr *) NLMSG_DATA (nlmh);
              if (nlmh->nlmsg_len < NLMSG_LENGTH (sizeof (struct nlmsgerr)))
                errno = EIO;
              else
                errno = -nlerr->error;
              goto out_fail;
            }
        }

      /* If there was nothing with the expected nlmsg_pid and nlmsg_seq,
         there is no point to record it.  */
      if (count == 0)
        continue;

      nlm_next = (struct netlink_res *) malloc (sizeof (struct netlink_res)
                                                + read_len);
      if (nlm_next == NULL)
        goto out_fail;
      nlm_next->next = NULL;
      nlm_next->nlh = memcpy (nlm_next + 1, buf, read_len);
      nlm_next->size = read_len;
      nlm_next->seq = h->seq;
      if (h->nlm_list == NULL)
        h->nlm_list = nlm_next;
      else
        h->end_ptr->next = nlm_next;
      h->end_ptr = nlm_next;
    }

  free (buf);
  return 0;

out_fail:
  free (buf);
  return -1;
}

static void
__netlink_close (struct netlink_handle *h)
{
  close(h->fd);
}

static int
__netlink_open (struct netlink_handle *h)
{
  struct sockaddr_nl nladdr;

  h->fd = socket (PF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
  if (h->fd < 0)
    goto out;

  memset (&nladdr, '\0', sizeof (nladdr));
  nladdr.nl_family = AF_NETLINK;
  if (bind (h->fd, (struct sockaddr *) &nladdr, sizeof (nladdr)) < 0)
    {
    close_and_out:
      __netlink_close (h);
    out:
      return -1;
    }
  /* Determine the ID the kernel assigned for this netlink connection.
     It is not necessarily the PID if there is more than one socket
     open.  */
  socklen_t addr_len = sizeof (nladdr);
  if (getsockname (h->fd, (struct sockaddr *) &nladdr, &addr_len) < 0)
    goto close_and_out;
  h->pid = nladdr.nl_pid;
  return 0;
}

/* We know the number of RTM_NEWLINK entries, so we reserve the first
   # of entries for this type. All RTM_NEWADDR entries have an index
   pointer to the RTM_NEWLINK entry.  To find the entry, create
   a table to map kernel index entries to our index numbers.
   Since we get at first all RTM_NEWLINK entries, it can never happen
   that a RTM_NEWADDR index is not known to this map.  */
static int
map_newlink (int index, struct ifaddrs_storage *ifas, int *map, int max)
{
  int i;

  for (i = 0; i < max; i++)
    {
      if (map[i] == -1)
        {
          map[i] = index;
          if (i > 0)
            ifas[i - 1].ifa.ifa_next = &ifas[i].ifa;
          return i;
        }
      else if (map[i] == index)
        return i;
    }

  /* This means interfaces changed inbetween the reading of the
     RTM_GETLINK and RTM_GETADDR information.  We have to repeat
     everything.  */
  return -1;
}

/* Create a linked list of `struct ifaddrs' structures, one for each
   network interface on the host machine.  If successful, store the
   list in *IFAP and return 0.  On errors, return -1 and set `errno'.  */
static int
ws4d_getifaddrs_internal (struct ifaddrs **ifap)
{
  struct netlink_handle nh = { 0, 0, 0, NULL, NULL };
  struct netlink_res *nlp;
  struct ifaddrs_storage *ifas;
  unsigned int i, newlink, newaddr, newaddr_idx;
  int *map_newlink_data;
  size_t ifa_data_size = 0;  /* Size to allocate for all ifa_data.  */
  char *ifa_data_ptr;	/* Pointer to the unused part of memory for
                                ifa_data.  */
  int result = 0;

  *ifap = NULL;

  if (__netlink_open (&nh) < 0)
    {
      return -1;
    }

  /* Tell the kernel that we wish to get a list of all
     active interfaces, collect all data for every interface.  */
  if (__netlink_request (&nh, RTM_GETLINK) < 0)
    {
      result = -1;
      goto exit_free;
    }

  /* Now ask the kernel for all addresses which are assigned
     to an interface and collect all data for every interface.
     Since we store the addresses after the interfaces in the
     list, we will later always find the interface before the
     corresponding addresses.  */
  ++nh.seq;
  if (__netlink_request (&nh, RTM_GETADDR) < 0)
    {
      result = -1;
      goto exit_free;
    }

  /* Count all RTM_NEWLINK and RTM_NEWADDR entries to allocate
     enough memory.  */
  newlink = newaddr = 0;
  for (nlp = nh.nlm_list; nlp; nlp = nlp->next)
    {
      struct nlmsghdr *nlh;
      size_t size = nlp->size;

      if (nlp->nlh == NULL)
        continue;

      /* Walk through all entries we got from the kernel and look, which
         message type they contain.  */
      for (nlh = nlp->nlh; NLMSG_OK (nlh, size); nlh = NLMSG_NEXT (nlh, size))
        {
          /* Check if the message is what we want.  */
          if ((pid_t) nlh->nlmsg_pid != nh.pid || nlh->nlmsg_seq != nlp->seq)
            continue;

          if (nlh->nlmsg_type == NLMSG_DONE)
            break;		/* ok */

          if (nlh->nlmsg_type == RTM_NEWLINK)
            {
              /* A RTM_NEWLINK message can have IFLA_STATS data. We need to
                 know the size before creating the list to allocate enough
                 memory.  */
              struct ifinfomsg *ifim = (struct ifinfomsg *) NLMSG_DATA (nlh);
              struct rtattr *rta = IFLA_RTA (ifim);
              size_t rtasize = IFLA_PAYLOAD (nlh);

              while (RTA_OK (rta, rtasize))
                {
                  size_t rta_payload = RTA_PAYLOAD (rta);

                  if (rta->rta_type == IFLA_STATS)
                    {
                      ifa_data_size += rta_payload;
                      break;
                    }
                  else
                    rta = RTA_NEXT (rta, rtasize);
                }
              ++newlink;
            }
          else if (nlh->nlmsg_type == RTM_NEWADDR)
            ++newaddr;
        }
    }

  /* Return if no interface is up.  */
  if ((newlink + newaddr) == 0)
    goto exit_free;

  /* Allocate memory for all entries we have and initialize next
     pointer.  */
  ifas = (struct ifaddrs_storage *) calloc (1,
                                            (newlink + newaddr)
                                            * sizeof (struct ifaddrs_storage)
                                            + ifa_data_size);
  if (ifas == NULL)
    {
      result = -1;
      goto exit_free;
    }

  /* Table for mapping kernel index to entry in our list.  */
  map_newlink_data = alloca (newlink * sizeof (int));
  memset (map_newlink_data, '\xff', newlink * sizeof (int));

  ifa_data_ptr = (char *) &ifas[newlink + newaddr];
  newaddr_idx = 0;		/* Counter for newaddr index.  */

  /* Walk through the list of data we got from the kernel.  */
  for (nlp = nh.nlm_list; nlp; nlp = nlp->next)
    {
      struct nlmsghdr *nlh;
      size_t size = nlp->size;

      if (nlp->nlh == NULL)
        continue;

      /* Walk through one message and look at the type: If it is our
         message, we need RTM_NEWLINK/RTM_NEWADDR and stop if we reach
         the end or we find the end marker (in this case we ignore the
         following data.  */
      for (nlh = nlp->nlh; NLMSG_OK (nlh, size); nlh = NLMSG_NEXT (nlh, size))
        {
          int ifa_index = 0;

          /* Check if the message is the one we want */
          if ((pid_t) nlh->nlmsg_pid != nh.pid || nlh->nlmsg_seq != nlp->seq)
            continue;

          if (nlh->nlmsg_type == NLMSG_DONE)
            break;		/* ok */

          if (nlh->nlmsg_type == RTM_NEWLINK)
            {
              /* We found a new interface. Now extract everything from the
                 interface data we got and need.  */
              struct ifinfomsg *ifim = (struct ifinfomsg *) NLMSG_DATA (nlh);
              struct rtattr *rta = IFLA_RTA (ifim);
              size_t rtasize = IFLA_PAYLOAD (nlh);

              /* Interfaces are stored in the first "newlink" entries
                 of our list, starting in the order as we got from the
                 kernel.  */
              ifa_index = map_newlink (ifim->ifi_index - 1, ifas,
                                       map_newlink_data, newlink);
              if (__builtin_expect (ifa_index == -1, 0))
                {
                try_again:
                  result = -EAGAIN;
                  free (ifas);
                  goto exit_free;
                }
              ifas[ifa_index].ifa.ifa_flags = ifim->ifi_flags;

              while (RTA_OK (rta, rtasize))
                {
                  char *rta_data = RTA_DATA (rta);
                  size_t rta_payload = RTA_PAYLOAD (rta);

                  switch (rta->rta_type)
                    {
                    case IFLA_ADDRESS:
                      if (rta_payload <= sizeof (ifas[ifa_index].addr))
                        {
                          ifas[ifa_index].addr.sl.sll_family = AF_PACKET;
                          memcpy (ifas[ifa_index].addr.sl.sll_addr,
                                  (char *) rta_data, rta_payload);
                          ifas[ifa_index].addr.sl.sll_halen = rta_payload;
                          ifas[ifa_index].addr.sl.sll_ifindex
                            = ifim->ifi_index;
                          ifas[ifa_index].addr.sl.sll_hatype = ifim->ifi_type;

                          ifas[ifa_index].ifa.ifa_addr
                            = &ifas[ifa_index].addr.sa;
                        }
                      break;

                    case IFLA_BROADCAST:
                      if (rta_payload <= sizeof (ifas[ifa_index].broadaddr))
                        {
                          ifas[ifa_index].broadaddr.sl.sll_family = AF_PACKET;
                          memcpy (ifas[ifa_index].broadaddr.sl.sll_addr,
                                  (char *) rta_data, rta_payload);
                          ifas[ifa_index].broadaddr.sl.sll_halen = rta_payload;
                          ifas[ifa_index].broadaddr.sl.sll_ifindex
                            = ifim->ifi_index;
                          ifas[ifa_index].broadaddr.sl.sll_hatype
                            = ifim->ifi_type;

                          ifas[ifa_index].ifa.ifa_broadaddr
                            = &ifas[ifa_index].broadaddr.sa;
                        }
                      break;

                    case IFLA_IFNAME:	/* Name of Interface */
                      if ((rta_payload + 1) <= sizeof (ifas[ifa_index].name))
                        {
                          ifas[ifa_index].ifa.ifa_name = ifas[ifa_index].name;
                          memcpy (ifas[ifa_index].name, rta_data,
                                               rta_payload);
                          ifas[ifa_index].ifa.ifa_name[rta_payload] = '\0';
                        }
                      break;

                    case IFLA_STATS:	/* Statistics of Interface */
                      ifas[ifa_index].ifa.ifa_data = ifa_data_ptr;
                      ifa_data_ptr += rta_payload;
                      memcpy (ifas[ifa_index].ifa.ifa_data, rta_data,
                              rta_payload);
                      break;

                    case IFLA_UNSPEC:
                      break;
                    case IFLA_MTU:
                      break;
                    case IFLA_LINK:
                      break;
                    case IFLA_QDISC:
                      break;
                    default:
                      break;
                    }

                  rta = RTA_NEXT (rta, rtasize);
                }
            }
          else if (nlh->nlmsg_type == RTM_NEWADDR)
            {
              struct ifaddrmsg *ifam = (struct ifaddrmsg *) NLMSG_DATA (nlh);
              struct rtattr *rta = IFA_RTA (ifam);
              size_t rtasize = IFA_PAYLOAD (nlh);

              /* New Addresses are stored in the order we got them from
                 the kernel after the interfaces. Theoretically it is possible
                 that we have holes in the interface part of the list,
                 but we always have already the interface for this address.  */
              ifa_index = newlink + newaddr_idx;
              int idx = map_newlink (ifam->ifa_index - 1, ifas,
                                     map_newlink_data, newlink);
              if (__builtin_expect (idx == -1, 0))
                goto try_again;
              ifas[ifa_index].ifa.ifa_flags = ifas[idx].ifa.ifa_flags;
              if (ifa_index > 0)
                ifas[ifa_index - 1].ifa.ifa_next = &ifas[ifa_index].ifa;
              ++newaddr_idx;

              while (RTA_OK (rta, rtasize))
                {
                  char *rta_data = RTA_DATA (rta);
                  size_t rta_payload = RTA_PAYLOAD (rta);

                  switch (rta->rta_type)
                    {
                    case IFA_ADDRESS:
                      {
                        struct sockaddr *sa;

                        if (ifas[ifa_index].ifa.ifa_addr != NULL)
                          {
                            /* In a point-to-poing network IFA_ADDRESS
                               contains the destination address, local
                               address is supplied in IFA_LOCAL attribute.
                               destination address and broadcast address
                               are stored in an union, so it doesn't matter
                               which name we use.  */
                            ifas[ifa_index].ifa.ifa_broadaddr
                              = &ifas[ifa_index].broadaddr.sa;
                            sa = &ifas[ifa_index].broadaddr.sa;
                          }
                        else
                          {
                            ifas[ifa_index].ifa.ifa_addr
                              = &ifas[ifa_index].addr.sa;
                            sa = &ifas[ifa_index].addr.sa;
                          }

                        sa->sa_family = ifam->ifa_family;

                        switch (ifam->ifa_family)
                          {
                          case AF_INET:
                            /* Size must match that of an address for IPv4.  */
                            if (rta_payload == 4)
                              memcpy (&((struct sockaddr_in *) sa)->sin_addr,
                                      rta_data, rta_payload);
                            break;

                          case AF_INET6:
                            /* Size must match that of an address for IPv6.  */
                            if (rta_payload == 16)
                              {
                                memcpy (&((struct sockaddr_in6 *) sa)->sin6_addr,
                                        rta_data, rta_payload);
                                if (IN6_IS_ADDR_LINKLOCAL ((struct in6_addr *) rta_data)
                                    || IN6_IS_ADDR_MC_LINKLOCAL ((struct in6_addr *) rta_data))
                                  ((struct sockaddr_in6 *) sa)->sin6_scope_id
                                    = ifam->ifa_index;
                              }
                            break;

                          default:
                            if (rta_payload <= sizeof (ifas[ifa_index].addr))
                              memcpy (sa->sa_data, rta_data, rta_payload);
                            break;
                          }
                      }
                      break;

                    case IFA_LOCAL:
                      if (ifas[ifa_index].ifa.ifa_addr != NULL)
                        {
                          /* If ifa_addr is set and we get IFA_LOCAL,
                             assume we have a point-to-point network.
                             Move address to correct field.  */
                          ifas[ifa_index].broadaddr = ifas[ifa_index].addr;
                          ifas[ifa_index].ifa.ifa_broadaddr
                            = &ifas[ifa_index].broadaddr.sa;
                          memset (&ifas[ifa_index].addr, '\0',
                                  sizeof (ifas[ifa_index].addr));
                        }

                      ifas[ifa_index].ifa.ifa_addr = &ifas[ifa_index].addr.sa;
                      ifas[ifa_index].ifa.ifa_addr->sa_family
                        = ifam->ifa_family;

                      switch (ifam->ifa_family)
                        {
                        case AF_INET:
                          /* Size must match that of an address for IPv4.  */
                          if (rta_payload == 4)
                            memcpy (&ifas[ifa_index].addr.s4.sin_addr,
                                  rta_data, rta_payload);
                          break;

                        case AF_INET6:
                          /* Size must match that of an address for IPv6.  */
                          if (rta_payload == 16)
                            {
                              memcpy (&ifas[ifa_index].addr.s6.sin6_addr,
                                      rta_data, rta_payload);
                              if (IN6_IS_ADDR_LINKLOCAL ((struct in6_addr *) rta_data)
                                  || IN6_IS_ADDR_MC_LINKLOCAL ((struct in6_addr *) rta_data))
                                ifas[ifa_index].addr.s6.sin6_scope_id =
                                  ifam->ifa_index;
                            }
                          break;

                        default:
                          if (rta_payload <= sizeof (ifas[ifa_index].addr))
                            memcpy (ifas[ifa_index].addr.sa.sa_data,
                                    rta_data, rta_payload);
                          break;
                        }
                      break;

                    case IFA_BROADCAST:
                      /* We get IFA_BROADCAST, so IFA_LOCAL was too much.  */
                      if (ifas[ifa_index].ifa.ifa_broadaddr != NULL)
                        memset (&ifas[ifa_index].broadaddr, '\0',
                                sizeof (ifas[ifa_index].broadaddr));

                      ifas[ifa_index].ifa.ifa_broadaddr
                        = &ifas[ifa_index].broadaddr.sa;
                      ifas[ifa_index].ifa.ifa_broadaddr->sa_family
                        = ifam->ifa_family;

                      switch (ifam->ifa_family)
                        {
                        case AF_INET:
                          /* Size must match that of an address for IPv4.  */
                          if (rta_payload == 4)
                            memcpy (&ifas[ifa_index].broadaddr.s4.sin_addr,
                                    rta_data, rta_payload);
                          break;

                        case AF_INET6:
                          /* Size must match that of an address for IPv6.  */
                          if (rta_payload == 16)
                            {
                              memcpy (&ifas[ifa_index].broadaddr.s6.sin6_addr,
                                      rta_data, rta_payload);
                              if (IN6_IS_ADDR_LINKLOCAL ((struct in6_addr *) rta_data)
                                  || IN6_IS_ADDR_MC_LINKLOCAL ((struct in6_addr *) rta_data))
                                ifas[ifa_index].broadaddr.s6.sin6_scope_id
                                  = ifam->ifa_index;
                            }
                          break;

                        default:
                          if (rta_payload <= sizeof (ifas[ifa_index].addr))
                            memcpy (&ifas[ifa_index].broadaddr.sa.sa_data,
                                    rta_data, rta_payload);
                          break;
                        }
                      break;

                    case IFA_LABEL:
                      if (rta_payload + 1 <= sizeof (ifas[ifa_index].name))
                        {
                          ifas[ifa_index].ifa.ifa_name = ifas[ifa_index].name;
                          memcpy (ifas[ifa_index].name, rta_data,
                                               rta_payload);
                          ifas[ifa_index].name[rta_payload] = '\0';
                        }
                      else
                        abort ();
                      break;

                    case IFA_UNSPEC:
                      break;
                    case IFA_CACHEINFO:
                      break;
                    default:
                      break;
                    }

                  rta = RTA_NEXT (rta, rtasize);
                }

              /* If we didn't get the interface name with the
                 address, use the name from the interface entry.  */
              if (ifas[ifa_index].ifa.ifa_name == NULL)
                {
                  int idx = map_newlink (ifam->ifa_index - 1, ifas,
                                         map_newlink_data, newlink);
                  if (__builtin_expect (idx == -1, 0))
                    goto try_again;
                  ifas[ifa_index].ifa.ifa_name = ifas[idx].ifa.ifa_name;
                }

              /* Calculate the netmask.  */
              if (ifas[ifa_index].ifa.ifa_addr
                  && ifas[ifa_index].ifa.ifa_addr->sa_family != AF_UNSPEC
                  && ifas[ifa_index].ifa.ifa_addr->sa_family != AF_PACKET)
                {
                  uint32_t max_prefixlen = 0;
                  char *cp = NULL;

                  ifas[ifa_index].ifa.ifa_netmask
                    = &ifas[ifa_index].netmask.sa;

                  switch (ifas[ifa_index].ifa.ifa_addr->sa_family)
                    {
                    case AF_INET:
                      cp = (char *) &ifas[ifa_index].netmask.s4.sin_addr;
                      max_prefixlen = 32;
                      break;

                    case AF_INET6:
                      cp = (char *) &ifas[ifa_index].netmask.s6.sin6_addr;
                      max_prefixlen = 128;
                      break;
                    }

                  ifas[ifa_index].ifa.ifa_netmask->sa_family
                    = ifas[ifa_index].ifa.ifa_addr->sa_family;

                  if (cp != NULL)
                    {
                      char c;
                      unsigned int preflen;

                      if ((max_prefixlen > 0) &&
                          (ifam->ifa_prefixlen > max_prefixlen))
                        preflen = max_prefixlen;
                      else
                        preflen = ifam->ifa_prefixlen;

                      for (i = 0; i < (preflen / 8); i++)
                        *cp++ = 0xff;
                      c = 0xff;
                      c <<= (8 - (preflen % 8));
                      *cp = c;
                    }
                }
            }
        }
    }

  assert (ifa_data_ptr <= (char *) &ifas[newlink + newaddr] + ifa_data_size);

  if (newaddr_idx > 0)
    {
      for (i = 0; i < newlink; ++i)
        if (map_newlink_data[i] == -1)
          {
            /* We have fewer links then we anticipated.  Adjust the
               forward pointer to the first address entry.  */
            ifas[i - 1].ifa.ifa_next = &ifas[newlink].ifa;
          }

      if (i == 0 && newlink > 0)
        /* No valid link, but we allocated memory.  We have to
           populate the first entry.  */
        memmove (ifas, &ifas[newlink], sizeof (struct ifaddrs_storage));
    }

  *ifap = &ifas[0].ifa;

 exit_free:
  __netlink_free_handle (&nh);
  __netlink_close (&nh);

  return result;
}

int
ws4d_getifaddrs (struct ifaddrs **ifap)
{
  int res;

  do
    res = ws4d_getifaddrs_internal (ifap);
  while (res == -EAGAIN);

  return res;
}

void
ws4d_freeifaddrs (struct ifaddrs *ifa)
{
  free (ifa);
}

#endif
