/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *  Created on: 27.04.2011
 *      Author: elmex
 */

#include "ws4d_misc.h"

struct ws4d_stringlist_item
{
  struct ws4d_list_node list;
  char *string;
};

#define WS4D_STRINGLIST_DIRTY      0x00000001

#define _ws4d_stringlist_check(head) \
    (head) && ws4d_list_check(&head->strings)

int
ws4d_stringlist_init (struct ws4d_stringlist *head)
{
  return ws4d_stringlist_init2 (head, NULL);
}

int
ws4d_stringlist_init2 (struct ws4d_stringlist *head,
                       struct ws4d_abs_allocator *allocator)
{
  /* test parameters */
  ws4d_assert (head, WS4D_EPARAM);

  /* initialize structure */
  head->status = 0;
  head->string = NULL;
  WS4D_INIT_LIST (&head->strings);

  if (allocator != NULL)
    {
      ws4d_allocator_copy (allocator, &head->alist);
    }
  else
    {
      WS4D_ALLOCLIST_INIT (&head->alist);
    }

  ws4d_mutex_init (&head->lock);

  return WS4D_OK;
}

int
ws4d_stringlist_init3 (struct ws4d_stringlist *head, const char *items)
{
  int err = ws4d_stringlist_init (head);
  ws4d_fail (err != WS4D_OK, err);

  return ((items == NULL)
          || (*items == '\0')) ? WS4D_OK : ws4d_stringlist_add (head, items);
}

int
ws4d_stringlist_check (struct ws4d_stringlist *head)
{
  return _ws4d_stringlist_check (head);
}

int
ws4d_stringlist_done (struct ws4d_stringlist *head)
{
  /* test parameters */
  ws4d_assert (_ws4d_stringlist_check (head), WS4D_EPARAM);

  /* reset structure */
  ws4d_stringlist_clear (head);
  ws4d_allocator_done (&head->alist);
  ws4d_mutex_destroy (&head->lock);

  return WS4D_OK;
}

int
ws4d_stringlist_len (struct ws4d_stringlist *head)
{
  register int result = 0;
  void *it;
  const char *string;

  /* test parameters */
  ws4d_assert (_ws4d_stringlist_check (head), -1);

  ws4d_stringlist_foreach (string, it, head)
  {
    result++;
  }

  return result;
}

int
ws4d_stringlist_add (struct ws4d_stringlist *head, const char *strings)
{
  register const char *strt = NULL, *stop = NULL;

  /* test parameters */
  ws4d_assert (_ws4d_stringlist_check (head), WS4D_EPARAM);

  /* fail with empty items string */
  ws4d_fail ((strings == NULL) || (*strings == '\0'), WS4D_EPARAM);

  strt = strings;
  while (strlen (strt) > 0)
    {
      struct ws4d_stringlist_item *new_item;

      /* remove leading spaces */
      while (strt[0] == ' ')
        {
          strt++;
        }

      /* calculate beginning and end of one item */
      if (strlen (strt) > 0)
        {
          stop = memchr (strt, ' ', strlen (strt));
          if (stop == NULL)
            {
              stop = strt + strlen (strt);
            }
        }
      else
        {
          stop = NULL;
        }

      if (stop)
        {
          /* allocate new string list item */
          new_item = ws4d_malloc_alist (sizeof (struct ws4d_stringlist_item),
                                        &head->alist);
          ws4d_fail (!new_item, WS4D_EOM);

          /* allocate new string */
          new_item->string =
            ws4d_malloc_alist (stop - strt + 1, &head->alist);
          ws4d_fail (!new_item->string, WS4D_EOM);

          /* copy string */
          strncpy (new_item->string, strt, stop - strt);
          new_item->string[stop - strt] = '\0';

          /* append item to list */
          ws4d_stringlist_lock (head);
          ws4d_list_add_tail (&new_item->list, &head->strings);
          ws4d_int_setbit (&head->status, WS4D_STRINGLIST_DIRTY);
          ws4d_stringlist_unlock (head);

          /* old end is new start */
          strt = stop;
        }
    }

  return WS4D_OK;
}


int
ws4d_stringlist_set (struct ws4d_stringlist *head, const char *strings)
{
  int err = ws4d_stringlist_clear (head);
  ws4d_fail (err != WS4D_OK, err);

  return ws4d_stringlist_add (head, strings);
}


/* TODO: make uri contain a string itself and define macro with
 *       ws4d_stringlist_add and ws4d_uri_tostring with similar behavour */
int
ws4d_stringlist_adduri (struct ws4d_stringlist *head, struct ws4d_uri *uri)
{
  int length;
  struct ws4d_stringlist_item *new_item;

  /* test parameters */
  ws4d_assert (_ws4d_stringlist_check (head) && uri, WS4D_EPARAM);

  length = ws4d_uri_tostrlen (uri);

  /* allocate new string list item */
  new_item = ws4d_malloc_alist (sizeof (struct ws4d_stringlist_item),
                                &head->alist);
  ws4d_fail (!new_item, WS4D_EOM);

  /* allocate new string */
  new_item->string = ws4d_malloc_alist (length + 1, &head->alist);
  ws4d_fail (!new_item->string, WS4D_EOM);

  /* convert uri to string */
  ws4d_uri_tostr (uri, new_item->string, length + 1);

  /* append item to list */
  ws4d_stringlist_lock (head);
  ws4d_list_add_tail (&new_item->list, &head->strings);
  ws4d_int_setbit (&head->status, WS4D_STRINGLIST_DIRTY);
  ws4d_stringlist_unlock (head);

  return WS4D_OK;
}


int
ws4d_stringlist_addlist (struct ws4d_stringlist *head,
                         struct ws4d_stringlist *list)
{
  register struct ws4d_stringlist_item *litem, *next;
  int err = WS4D_ERR;

  /* test parameters */
  ws4d_assert (_ws4d_stringlist_check (head)
               && _ws4d_stringlist_check (list), WS4D_EPARAM);

  /* don't add an empty list */
  if (ws4d_stringlist_isempty (list))
    {
      return WS4D_OK;
    }

  /* add each element of list */
  ws4d_stringlist_lock (list);
  ws4d_list_foreach (litem, next, &list->strings, struct ws4d_stringlist_item,
                     list)
  {
    err = ws4d_stringlist_add (head, litem->string);
    if (err != WS4D_OK)
      {
        break;
      }
  }
  ws4d_stringlist_unlock (list);

  return err;
}

struct ws4d_stringlist_item *
ws4d_stringlist_finditem2 (struct ws4d_stringlist *head, const char *item,
                           ws4d_stringlist_matchingfunction mf, void *mfdata)
{
  register struct ws4d_stringlist_item *litem, *next;

  /* test parameters */
  ws4d_assert (_ws4d_stringlist_check (head) && item, NULL);

  if (mf != NULL)
    {
      ws4d_list_foreach (litem, next, &head->strings,
                         struct ws4d_stringlist_item, list)
      {
        if (mf (litem->string, item, mfdata))
          {
            return litem;
          }
      }
    }
  else
    {
      ws4d_list_foreach (litem, next, &head->strings,
                         struct ws4d_stringlist_item, list)
      {
        if (!strcmp (litem->string, item))
          {
            return litem;
          }
      }
    }

  return NULL;
}

int
ws4d_stringlist_compare2 (struct ws4d_stringlist *head1,
                          struct ws4d_stringlist *head2,
                          ws4d_stringlist_matchingfunction mf, void *mfdata)
{
  register struct ws4d_stringlist_item *litem, *next;

  /* test parameters */
  ws4d_assert (head1, -1);

  ws4d_list_foreach (litem, next, &head1->strings,
                     struct ws4d_stringlist_item, list)
  {
    if (ws4d_stringlist_finditem2 (head2, litem->string, mf, mfdata) == NULL)
      {
        /* list1 has more items than list2 */
        return +1;
      }
  }

  if (ws4d_stringlist_len (head1) - ws4d_stringlist_len (head2) == 0)
    {
      /* list1 exactly matches list2 */
      return 0;
    }
  else
    {
      /* list2 contains list1 */
      return -1;
    }
}


/* TODO: rename to ws4d_stringlist_remove */
int
ws4d_stringlist_delete (struct ws4d_stringlist *head, const char *item)
{
  struct ws4d_stringlist_item *slitem;

  /* test parameters */
  ws4d_assert (_ws4d_stringlist_check (head) && item, WS4D_EPARAM);

  /* find old item */
  ws4d_stringlist_lock (head);
  slitem = ws4d_stringlist_finditem (head, item);
  ws4d_stringlist_unlock (head);
  ws4d_fail (slitem == NULL, WS4D_ERR);

  /* remove item */
  ws4d_stringlist_lock (head);
  if (slitem->string)
    {
      ws4d_free_alist (slitem->string);
    }
  ws4d_list_del (&slitem->list);
  ws4d_free_alist (slitem);

  /* mark string dirty */
  ws4d_int_setbit (&head->status, WS4D_STRINGLIST_DIRTY);
  ws4d_stringlist_unlock (head);

  return WS4D_OK;
}


int
ws4d_stringlist_copy (struct ws4d_stringlist *src,
                      struct ws4d_stringlist *dst)
{
  /* clear destination list */
  int err = ws4d_stringlist_clear (dst);
  ws4d_fail (err != WS4D_OK, err);

  /* copy list items */
  return ws4d_stringlist_addlist (dst, src);
}


int
ws4d_stringlist_clear (struct ws4d_stringlist *head)
{
  /* test parameters */
  ws4d_assert (_ws4d_stringlist_check (head), WS4D_EPARAM);

  ws4d_stringlist_lock (head);

  /* free all items in string list */
  if (ws4d_allocator_iscopy (&head->alist))
    {
      register struct ws4d_stringlist_item *litem, *next;
      ws4d_list_foreach (litem, next, &head->strings,
                         struct ws4d_stringlist_item, list)
      {
        ws4d_allocator_free (&head->alist, litem);
      }
      if (head->string != NULL)
        {
          ws4d_allocator_free (&head->alist, (void *) head->string);
        }
    }
  else
    {
      ws4d_allocator_freeall (&head->alist);
    }

  /* reset structure */
  WS4D_INIT_LIST (&head->strings);
  head->string = NULL;

  ws4d_stringlist_unlock (head);

  return WS4D_OK;
}

#ifdef WITH_MUTEXES
void
ws4d_stringlist_lock (struct ws4d_stringlist *head)
{
  /* test parameters */
  ws4d_assert (_ws4d_stringlist_check (head),);

  /* lock mutex */
  ws4d_mutex_lock (&head->lock);
}
#endif

#ifdef WITH_MUTEXES
void
ws4d_stringlist_unlock (struct ws4d_stringlist *head)
{
  /* test parameters */
  ws4d_assert (_ws4d_stringlist_check (head),);

  /* lock mutex */
  ws4d_mutex_unlock (&head->lock);
}
#endif

const char *
ws4d_stringlist_iterate (const struct ws4d_stringlist *head, void **iterator)
{
  register struct ws4d_stringlist_item *item;

  /* test parameters */
  ws4d_assert (_ws4d_stringlist_check (head) && iterator, NULL);

  if (*iterator == NULL)
    {
      /* if *iterator is NULL, we start to got through the list */
      item = ws4d_list_getfirst (&head->strings, struct ws4d_stringlist_item,
                                 list);
      *iterator = item;
      return item->string;
    }
  else
    {
      /* get next element */
      item = *iterator;
      item = ws4d_list_getnext (item, &head->strings,
                                struct ws4d_stringlist_item, list);
      if (item != NULL)
        {
          *iterator = item;
          return item->string;
        }
      else
        {
          *iterator = NULL;
          return NULL;
        }
    }

  return NULL;
}

const char *
ws4d_stringlist_tostring (struct ws4d_stringlist *head)
{
  const char *string = NULL;
  void *it = NULL;
  int length = 0, entries = 0;

  /* test parameters */
  ws4d_assert (_ws4d_stringlist_check (head), NULL);

  ws4d_stringlist_lock (head);

  /* if string is up to date */
  if (!ws4d_int_chkbit (&head->status, WS4D_STRINGLIST_DIRTY))
    {
      ws4d_stringlist_unlock (head);
      return head->string;
    }

  /* free old string */
  ws4d_free_alist ((void *) head->string);
  head->string = NULL;

  /* calculate string length an entries */
  ws4d_stringlist_foreach (string, it, head)
  {
    if (entries != 0)
      {
        length += 1;
      }
    length += strlen (string);
    entries += 1;
  }

  if (length == 0)
    {
      /* mark list clean */
      ws4d_int_clrbit (&head->status, WS4D_STRINGLIST_DIRTY);
      ws4d_stringlist_unlock (head);

      return NULL;
    }

  /* allocate memory */
  head->string = ws4d_malloc_alist (length + 1, &head->alist);
  if (head->string == NULL)
    {
      ws4d_stringlist_unlock (head);
      return NULL;
    }

  /* concat create space separated string */
  ws4d_stringlist_foreach (string, it, head)
  {
    strcat ((char *) head->string, string);
    entries -= 1;
    if (entries > 0)
      {
        strcat ((char *) head->string, " ");
      }
  }

  /* mark list clean */
  ws4d_int_clrbit (&head->status, WS4D_STRINGLIST_DIRTY);
  ws4d_stringlist_unlock (head);

  return head->string;
}

int
ws4d_isstringlist (const char *s)
{
  /* test parameters */
  ws4d_assert (s, 0);

  return memchr (s, ' ', strlen (s)) != NULL;
}
