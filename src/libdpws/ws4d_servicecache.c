/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "ws4d_misc.h"
#include "ws4d_abstract_eprlist.h"
#include "ws4d_eprllist.h"
#include "ws4d_service.h"

#include "ws4d_servicecache.h"

/**
 * Service Cache Plugin
 */

#define WS4D_SERVICECACHE_ID "WS4D-ServiceCache-0.1"

const char *ws4d_servicecache_id = WS4D_SERVICECACHE_ID;

struct ws4d_servicecache_plugin
{
  struct ws4d_abs_eprlist cache;
#ifdef WITH_MUTEXES
    WS4D_MUTEX (lock);
#endif
};

INLINE struct ws4d_servicecache_plugin *ws4d_servicecache_plugindata (struct
                                                                      ws4d_epr
                                                                      *epr);

static int servicecache_fcopy (struct ws4d_epr *epr,
                               struct ws4d_epr_plugin *dst,
                               struct ws4d_epr_plugin *src);

static void servicecache_fdelete (struct ws4d_epr *epr,
                                  struct ws4d_epr_plugin *p);

static int servicecache_invalidate (struct ws4d_epr *epr,
                                    struct ws4d_epr_plugin *p);

static struct ws4d_epr_plugin_cb ws4d_servicecache_callbacks =
  { servicecache_fdelete, servicecache_fcopy, servicecache_invalidate };

INLINE struct ws4d_servicecache_plugin *
ws4d_servicecache_plugindata (struct ws4d_epr *epr)
{
  return (struct ws4d_servicecache_plugin *)
    ws4d_epr_get_plugindata (epr, ws4d_servicecache_id);
}

static int
servicecache_fcopy (struct ws4d_epr *epr, struct ws4d_epr_plugin *dst,
                    struct ws4d_epr_plugin *src)
{
  struct ws4d_servicecache_plugin *data = dst->data;
  struct ws4d_servicecache_plugin *src_data = src->data;

  WS4D_UNUSED_PARAM (epr);

  /* if destination has alread an servicecache plugin */
  if (!data)
    {
      dst->id = ws4d_servicecache_id;
      dst->cb = &ws4d_servicecache_callbacks;

      data = ws4d_malloc (sizeof (struct ws4d_servicecache_plugin));
      ws4d_fail (!data, WS4D_ERR);

      ws4d_eprlist_init (&data->cache, ws4d_eprllist_init, NULL);
      ws4d_mutex_init (&data->lock);

      dst->data = data;
    }

  return ws4d_eprlist_update(&data->cache, &src_data->cache, NULL, NULL, NULL);
}

static int
servicecache_invalidate (struct ws4d_epr *epr, struct ws4d_epr_plugin *p)
{
  struct ws4d_servicecache_plugin *data = p->data;

  WS4D_UNUSED_PARAM (epr);

  /* also invalidate services in service cache */
  if (data)
    {
      register struct ws4d_epr *service, *iter;

      ws4d_eprlist_foreach (service, iter, &data->cache)
      {
        ws4d_epr_invalidate (service);
      }
    }

  return WS4D_OK;
}

static void
servicecache_fdelete (struct ws4d_epr *epr, struct ws4d_epr_plugin *p)
{
  struct ws4d_servicecache_plugin *plugin = p->data;

  WS4D_UNUSED_PARAM (epr);

  if (plugin)
    {
      ws4d_eprlist_done (&plugin->cache);
      ws4d_mutex_destroy (&plugin->lock);

      ws4d_free (plugin);
    }
}

static int
servicecache_fcreate (struct ws4d_epr *epr,
                      struct ws4d_epr_plugin *plugin, void *arg)
{
  struct ws4d_servicecache_plugin *data = NULL;

  WS4D_UNUSED_PARAM (epr);
  WS4D_UNUSED_PARAM (arg);

  plugin->id = ws4d_servicecache_id;
  plugin->cb = &ws4d_servicecache_callbacks;

  data = ws4d_malloc (sizeof (struct ws4d_servicecache_plugin));
  ws4d_assert (data, WS4D_EOM);

  if (ws4d_eprlist_init (&data->cache, ws4d_eprllist_init, NULL) != WS4D_OK)
    {
      ws4d_free (data);
      return WS4D_ERR;
    }
  ws4d_mutex_init (&data->lock);

  plugin->data = data;

  return WS4D_OK;
}

/**
 * internal functions
 */

static struct ws4d_epr *servicecache_allocservice (struct ws4d_epr *device);

static int servicecache_addservice (struct ws4d_epr *device,
                                    struct ws4d_epr *service);

static struct ws4d_epr *servicecache_get_servicebyId (struct
                                                      ws4d_servicecache_plugin
                                                      *plugin,
                                                      const char *Id);

static int servicecache_get_services (struct ws4d_servicecache_plugin *plugin,
                                      struct ws4d_abs_eprlist *result);

static int servicecache_get_servicesByType (struct ws4d_servicecache_plugin
                                            *plugin, ws4d_qnamelist * Types,
                                            struct ws4d_abs_eprlist *result);

static struct ws4d_epr *
servicecache_allocservice (struct ws4d_epr *device)
{
  struct ws4d_servicecache_plugin *plugin =
    ws4d_servicecache_plugindata (device);
  struct ws4d_epr *result = NULL;

  /* test parameters */
  ws4d_assert (plugin, NULL);

  result = ws4d_eprlist_alloc (&plugin->cache);

  return result;
}

static int
servicecache_addservice (struct ws4d_epr *device, struct ws4d_epr *service)
{
  int err = WS4D_ERR;
  struct ws4d_servicecache_plugin *plugin =
    ws4d_servicecache_plugindata (device);

  /* test parameters */
  ws4d_assert (plugin, WS4D_ERR);

  /* register service ep plugin */
  err = ws4d_serviceep_setdevice (service, device);
  ws4d_fail (err != WS4D_OK, err);

  return ws4d_eprlist_add (&plugin->cache, service);
}

static struct ws4d_epr *
servicecache_get_servicebyId (struct ws4d_servicecache_plugin *plugin,
                              const char *Id)
{
  /* test parameters */
  ws4d_fail (!plugin, NULL);

  return ws4d_eprlist_getByServiceId(&plugin->cache, Id);
}

static struct ws4d_epr *
servicecache_get_servicebyAddr (struct ws4d_servicecache_plugin *plugin,
                                const char *Addr)
{
  /* test parameters */
  ws4d_fail (!plugin, NULL);

  return ws4d_eprlist_get_byAddr (&plugin->cache, Addr);
}


static int
servicecache_get_servicesByType (struct ws4d_servicecache_plugin *plugin,
                                 ws4d_qnamelist * Types,
                                 struct ws4d_abs_eprlist *result)
{
  register struct ws4d_epr *service, *iter;
  int ret = WS4D_NOTFOUND;

  /* test parameters */
  ws4d_assert (plugin, WS4D_ERR);

  /* if no types are specified, simply copy whole list */
  if (Types)
    {
      if (ws4d_qnamelist_isempty (Types))
        {
          return servicecache_get_services (plugin, result);
        }
    }
  else
    {
      return servicecache_get_services (plugin, result);
    }

  ws4d_eprlist_foreach (service, iter, &plugin->cache)
  {
    if (ws4d_epr_isvalid (service)
        && ws4d_serviceep_matchesTypes (service, Types))
      {
        ws4d_eprlist_add2 (result, service);
        ret = WS4D_OK;
      }
  }

  return ret;
}

static int
servicecache_get_services (struct ws4d_servicecache_plugin *plugin,
                           struct ws4d_abs_eprlist *result)
{
  register struct ws4d_epr *service, *iter;

  /* test parameters */
  ws4d_fail (!plugin, WS4D_ERR);

  if (ws4d_eprlist_isempty (&plugin->cache))
    {
      return WS4D_NOTFOUND;
    }
  else
    {
      ws4d_eprlist_foreach (service, iter, &plugin->cache)
      {
        if (ws4d_epr_isvalid (service))
          {
            ws4d_eprlist_add2 (result, service);
          }
      }
    }

  return WS4D_OK;
}

/**
 * Public functions
 */

int
ws4d_register_sc (struct ws4d_epr *device)
{
  struct ws4d_servicecache_plugin *plugin =
    ws4d_servicecache_plugindata (device);

  ws4d_assert (device, WS4D_ERR);

  /* check if plugin is already registered */
  if (plugin)
    return WS4D_OK;

  /* register plugin at epr */
  return ws4d_epr_register_plugin (device, servicecache_fcreate, NULL);
}

int
ws4d_has_sc (struct ws4d_epr *device)
{
  struct ws4d_servicecache_plugin *plugin =
    ws4d_servicecache_plugindata (device);

  return plugin != NULL;
}

int
ws4d_sc_check_service (struct ws4d_epr *device, const char *Id)
{
  struct ws4d_epr *service;
  struct ws4d_servicecache_plugin *plugin =
    ws4d_servicecache_plugindata (device);

  ws4d_fail (!plugin || !Id, WS4D_ERR);

  service = servicecache_get_servicebyId (plugin, Id);
  if (!service)
    {
      service = servicecache_allocservice (device);
      servicecache_addservice (device, service);
      ws4d_serviceep_setid (service, Id);
      ws4d_epr_validate (service);

      return WS4D_NEW;
    }
  else
    {
      if (ws4d_epr_isvalid (service))
        {
          return WS4D_VALID;
        }
      else
        {
          return WS4D_INVALID;
        }
    }
}

int
ws4d_sc_set_Types (struct ws4d_epr *device, char *Id, const char *Types)
{
  struct ws4d_epr *service;
  struct ws4d_servicecache_plugin *plugin =
    ws4d_servicecache_plugindata (device);

  service = servicecache_get_servicebyId (plugin, Id);
  if (!service)
    {
      return WS4D_NOTFOUND;
    }
  else
    {
      return ws4d_serviceep_addtypestr (service, Types);
    }
}

/* TODO: to be implemented */
int
ws4d_sc_set_Types2 (struct ws4d_epr *device, char *Id,
                    const ws4d_qnamelist * Types)
{
  WS4D_UNUSED_PARAM (device);
  WS4D_UNUSED_PARAM (Id);
  WS4D_UNUSED_PARAM (Types);

  return WS4D_ERR;
}

int
ws4d_sc_set_Addrs (struct ws4d_epr *device, char *Id, const char *Addrs)
{
  struct ws4d_epr *service;
  struct ws4d_servicecache_plugin *plugin =
    ws4d_servicecache_plugindata (device);

  ws4d_assert (plugin, WS4D_ERR)
    service = servicecache_get_servicebyId (plugin, Id);
  if (!service)
    {
      return WS4D_NOTFOUND;
    }
  else
    {
      return ws4d_epr_set_Addrs (service, Addrs);
    }
}

/* TODO: to be implemented */
int
ws4d_sc_set_WSDL (struct ws4d_epr *device, char *Id, const char *WSDL)
{
  WS4D_UNUSED_PARAM (device);
  WS4D_UNUSED_PARAM (Id);
  WS4D_UNUSED_PARAM (WSDL);

  return WS4D_ERR;
}

int
ws4d_sc_get_servicebyId (struct ws4d_epr *device, const char *Id,
                         struct ws4d_epr *result)
{
  struct ws4d_servicecache_plugin *plugin =
    ws4d_servicecache_plugindata (device);

  return ws4d_epr_copy (result, servicecache_get_servicebyId (plugin, Id));
}

int
ws4d_sc_get_servicebyAddr (struct ws4d_epr *device, const char *Addr,
                           struct ws4d_epr *result)
{
  struct ws4d_servicecache_plugin *plugin =
    ws4d_servicecache_plugindata (device);

  return ws4d_epr_copy (result,
                        servicecache_get_servicebyAddr (plugin, Addr));
}

int
ws4d_sc_get_servicesByType (struct ws4d_epr *device, const char *Types,
                            struct ws4d_abs_eprlist *result)
{
  int ret;
  ws4d_qnamelist service_type_list;

  /* test parameters */
  ws4d_assert (Types, WS4D_ERR);

  ws4d_qnamelist_init2 (&service_type_list, Types);
  ret = ws4d_sc_get_servicesByType2 (device, &service_type_list, result);
  ws4d_qnamelist_done (&service_type_list);

  return ret;
}

int
ws4d_sc_get_servicesByType2 (struct ws4d_epr *device,
                             const ws4d_qnamelist * Types,
                             struct ws4d_abs_eprlist *result)
{
  struct ws4d_servicecache_plugin *plugin =
    ws4d_servicecache_plugindata (device);

  return servicecache_get_servicesByType (plugin, (ws4d_qnamelist *) Types,
                                          result);
}

#ifdef WITH_MUTEXES
void
ws4d_servicecache_lock (struct ws4d_epr *device)
{
  struct ws4d_servicecache_plugin *plugin =
    ws4d_servicecache_plugindata (device);
  if (plugin)
    ws4d_mutex_lock (&plugin->lock);
}
#endif

#ifdef WITH_MUTEXES
void
ws4d_servicecache_unlock (struct ws4d_epr *device)
{
  struct ws4d_servicecache_plugin *plugin =
    ws4d_servicecache_plugindata (device);
  if (plugin)
    ws4d_mutex_unlock (&plugin->lock);
}
#endif
