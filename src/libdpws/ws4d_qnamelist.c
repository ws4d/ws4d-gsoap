/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *  Created on: 28.06.2011
 *      Author: elmex
 */

#include "ws4d_misc.h"

#define WS4D_QNAMELIST_DIRTY      0x00000001

#define _ws4d_qnamelist_check(head) \
    (head) && ws4d_list_check(&head->qnames)

int
ws4d_qnamelist_init (ws4d_qnamelist * head)
{
  /* test parameters */
  ws4d_assert (head, WS4D_EPARAM);

  /* initialize structure */
  head->status = 0;
  head->string = NULL;
  WS4D_INIT_LIST (&head->qnames);
  WS4D_ALLOCLIST_INIT (&head->alist);
  ws4d_mutex_init (&head->lock);

  return WS4D_OK;
}

/* TODO: rename to ws4d_qnamelist_init3 and implement ws4d_qnamelist_init2 for
         external allocator */
int
ws4d_qnamelist_init2 (ws4d_qnamelist * head, const char *string)
{
  int err = ws4d_qnamelist_init (head);
  ws4d_fail (err != WS4D_OK, err);

  return ws4d_qnamelist_addstring (head, string);
}

int
ws4d_qnamelist_done (ws4d_qnamelist * head)
{
  /* test parameters */
  ws4d_assert (_ws4d_qnamelist_check (head), WS4D_EPARAM);

  /* reset structure */
  ws4d_qnamelist_clear (head);
  ws4d_allocator_done (&head->alist);
  ws4d_mutex_destroy (&head->lock);

  return WS4D_OK;
}

#ifdef WITH_MUTEXES
void
ws4d_qnamelist_lock (ws4d_qnamelist * head)
{
  /* test parameters */
  ws4d_assert (_ws4d_qnamelist_check (head),);

  /* lock mutex */
  ws4d_mutex_lock (&head->lock);
}
#endif

#ifdef WITH_MUTEXES
void
ws4d_qnamelist_unlock (ws4d_qnamelist * head)
{
  /* test parameters */
  ws4d_assert (_ws4d_qnamelist_check (head),);

  /* lock mutex */
  ws4d_mutex_unlock (&head->lock);
}
#endif

int
ws4d_qnamelist_add (ws4d_qnamelist * head, const struct ws4d_qname *qname)
{
  struct ws4d_qname *copy;

  /* test parameters */
  ws4d_assert (qname && head, WS4D_EPARAM);

  /* allocate new qname */
  copy = ws4d_qname_dup (qname, &head->alist);
  ws4d_assert (copy != NULL, WS4D_EOM);

  /* append item to list */
  ws4d_qnamelist_lock (head);
  ws4d_list_add_tail (&copy->list, &head->qnames);

  /* mark list dirty */
  ws4d_int_setbit (&head->status, WS4D_QNAMELIST_DIRTY);
  ws4d_qnamelist_unlock (head);

  return WS4D_OK;
}

int
ws4d_qnamelist_addstring (ws4d_qnamelist * head, const char *qnames)
{
  register const char *strt = NULL, *stop = NULL;
  int err = WS4D_OK;

  /* test parameters */
  ws4d_assert (qnames && head, WS4D_EPARAM);

  /* fail with empty items string */
  ws4d_fail ((qnames == NULL) || (*qnames == '\0'), WS4D_EPARAM);

  strt = qnames;
  while ((strlen (strt) > 0) && (err == WS4D_OK))
    {
      /* remove leading spaces */
      while (strt[0] == ' ')
        {
          strt++;
        }

      /* calculate beginning and end of one item */
      if (strlen (strt) > 0)
        {
          stop = memchr (strt, ' ', strlen (strt));
          if (stop == NULL)
            {
              stop = strt + strlen (strt);
            }
        }
      else
        {
          stop = NULL;
        }

      if (stop)
        {
          struct ws4d_qname qname;
          ws4d_qname_init (&qname);

          /* parse qname */
          err = ws4d_qname_parse (strt, &qname, &head->alist);
          if (err == WS4D_OK)
            {
              /* add qname */
              ws4d_qnamelist_add (head, &qname);
            }
          ws4d_qname_done (&qname);

          /* old end is new start */
          strt = stop;
        }
    }

  return err;
}

int
ws4d_qnamelist_addlist (ws4d_qnamelist * head, struct ws4d_qnamelist *list)
{
  register struct ws4d_qname *qname, *next;

  /* test parameters */
  ws4d_assert (list, WS4D_ERR);

  ws4d_qnamelist_foreach (qname, next, list)
  {
    int err = ws4d_qnamelist_add (head, qname);
    ws4d_fail (err != WS4D_OK, err);
  }

  return WS4D_OK;
}

int
ws4d_qnamelist_setstring (ws4d_qnamelist * head, const char *string)
{
  int err;

  err = ws4d_qnamelist_clear (head);
  ws4d_fail (err != WS4D_OK, err);

  return ws4d_qnamelist_addstring (head, string);
}

int
ws4d_qnamelist_copy (struct ws4d_qnamelist * src, struct ws4d_qnamelist * dst)
{
  /* clear destination list */
  int err = ws4d_qnamelist_clear (dst);
  ws4d_fail (err != WS4D_OK, err);

  /* copy list items */
  return ws4d_qnamelist_addlist (dst, src);
}

struct ws4d_qname *
ws4d_qnamelist_finditem (struct ws4d_qnamelist *head,
                         struct ws4d_qname *qname)
{
  register struct ws4d_qname *item, *it;

  ws4d_qnamelist_foreach (item, it, head)
  {
    if (ws4d_qname_match (qname, item))
      {
        return item;
      }
  }

  return NULL;
}

int
ws4d_qnamelist_compare (struct ws4d_qnamelist *head1,
                        struct ws4d_qnamelist *head2)
{
  register struct ws4d_qname *item, *it;

  /* test parameters */
  ws4d_assert (_ws4d_qnamelist_check (head1), +1);

  ws4d_list_foreach (item, it, &head1->qnames, struct ws4d_qname, list)
  {
    if (ws4d_qnamelist_finditem (head2, item) == NULL)
      {
        /* list1 has more items than list2 */
        return +1;
      }
  }

  if (ws4d_qnamelist_len (head1) - ws4d_qnamelist_len (head2) == 0)
    {
      /* list1 exactly matches list2 */
      return 0;
    }
  else
    {
      /* list2 contains list1 */
      return -1;
    }
}

int
ws4d_qnamelist_len (struct ws4d_qnamelist *head)
{
  int result = 0;
  register struct ws4d_qname *item, *it;

  /* test parameters */
  ws4d_assert (_ws4d_qnamelist_check (head), -1);

  ws4d_qnamelist_foreach (item, it, head)
  {
    result++;
  }

  return result;
}

int
ws4d_qnamelist_remove (ws4d_qnamelist * head, struct ws4d_qname *qname)
{
  /* test parameters */
  ws4d_assert (_ws4d_qnamelist_check (head) && qname, WS4D_EPARAM);

  /* delete item */
  ws4d_qnamelist_lock (head);
  ws4d_list_del (&qname->list);

  /* mark list dirty */
  ws4d_int_setbit (&head->status, WS4D_QNAMELIST_DIRTY);
  ws4d_qnamelist_unlock (head);

  return WS4D_OK;
}

int
ws4d_qnamelist_clear (ws4d_qnamelist * head)
{
  /* test parameters */
  ws4d_assert (_ws4d_qnamelist_check (head), WS4D_EPARAM);

  ws4d_qnamelist_lock (head);

  /* reset structure */
  WS4D_INIT_LIST (&head->qnames);
  head->string = NULL;
  ws4d_allocator_freeall (&head->alist);

  ws4d_qnamelist_unlock (head);

  return WS4D_OK;
}

const char *
ws4d_qnamelist_tostring (ws4d_qnamelist * head)
{
  register struct ws4d_qname *qname, *next;
  char *result = NULL;
  int qname_count = 0;
  size_t string_length = 0;

  /* test parameters */
  ws4d_assert (head, NULL);

  ws4d_qnamelist_lock (head);

  /* if string is up to date */
  if (!ws4d_int_chkbit (&head->status, WS4D_QNAMELIST_DIRTY))
    {
      ws4d_qnamelist_unlock (head);
      return head->string;
    }

  /* free old string */
  ws4d_free_alist ((void *) head->string);
  head->string = NULL;

  /* calculate length of string */
  ws4d_qnamelist_foreach (qname, next, head)
  {
    size_t qname_length = ws4d_qname_strlen (qname);
    if (qname_length > 0)
      {
        qname_count++;
        string_length += qname_length;
      }
  }

  /* empty string when list is empty */
  if (qname_count == 0)
    {

      /* mark list clean */
      ws4d_int_clrbit (&head->status, WS4D_QNAMELIST_DIRTY);
      ws4d_qnamelist_unlock (head);
      return NULL;
    }

  /* allocate memory */
  head->string = ws4d_malloc_alist (string_length + 1, &head->alist);
  if (head->string == NULL)
    {
      ws4d_qnamelist_unlock (head);
      return NULL;
    }

  /* generate string */
  result = (char *) head->string;
  ws4d_qnamelist_foreach (qname, next, head)
  {
    size_t qname_length = ws4d_qname_strlen (qname);

    if (qname_length > 0)
      {
        char *res = ws4d_qname_tostring2 (qname, result, qname_length);
        ws4d_fail (res == NULL, NULL);
        qname_count--;

        if (qname_count > 0)
          {
            result[qname_length - 1] = ' ';
          }

        result += qname_length;
      }
  }

  /* mark list clean */
  ws4d_int_clrbit (&head->status, WS4D_QNAMELIST_DIRTY);
  ws4d_qnamelist_unlock (head);

  return head->string;
}
