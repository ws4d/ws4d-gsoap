/*
 * C Implementation: uricmp_rfc2396
 *
 * for further information see:
 * - http://www.ietf.org/rfc/rfc2396.txt
 * - http://www.faqs.org/rfcs/rfc822.html
 * - http://www.ietf.org/rfc/rfc2732.txt
 * - http://www.ietf.org/rfc/rfc2373.txt
 *
 * Author: Sascha Feldhorst <sascha.feldhorst@udo.edu>, (C) 2008
 *
 * Copyright: See COPYING file that comes with this distribution
 *
 */

#include "stdsoap2.h"
#include "ws4d_misc.h"

int
ws4d_uri_init (struct ws4d_uri *u)
{
  /* test parameters */
  ws4d_assert (u, WS4D_EPARAM);

  memset (u, 0, sizeof (struct ws4d_uri));
  WS4D_ALLOCLIST_INIT (&u->alist);

  return WS4D_OK;
}

int
ws4d_uri_init2 (struct ws4d_uri *u, const char *uri, int flags)
{
  int err = ws4d_uri_init (u);
  ws4d_fail (err != WS4D_OK, err);

  return ws4d_parse_uri (uri, u, flags);
}

int
ws4d_uri_set_scheme2 (struct ws4d_uri *u, const char *scheme, size_t len)
{
  /* test parameters */
  ws4d_assert (u, WS4D_EPARAM);

  return ws4d_strndup2 (scheme, len, (const char **) &u->scheme, &u->alist);
}

int
ws4d_uri_set_userinfo2 (struct ws4d_uri *u, const char *userinfo, size_t len)
{
  /* test parameters */
  ws4d_assert (u, WS4D_EPARAM);

  return ws4d_strndup2 (userinfo, len, (const char **) &u->userinfo,
                        &u->alist);
}

int
ws4d_uri_set_host2 (struct ws4d_uri *u, const char *host, size_t len)
{
  /* test parameters */
  ws4d_assert (u, WS4D_EPARAM);

  return ws4d_strndup2 (host, len, (const char **) &u->host, &u->alist);
}

int
ws4d_uri_set_port2 (struct ws4d_uri *u, const char *port, size_t len)
{
  /* test parameters */
  ws4d_assert (u, WS4D_EPARAM);

  return ws4d_strndup2 (port, len, (const char **) &u->port, &u->alist);
}

int
ws4d_uri_set_path2 (struct ws4d_uri *u, const char *path, size_t len)
{
  /* test parameters */
  ws4d_assert (u, WS4D_EPARAM);

  return ws4d_strndup2 (path, len, (const char **) &u->path, &u->alist);
}

int
ws4d_uri_set_query2 (struct ws4d_uri *u, const char *query, size_t len)
{
  /* test parameters */
  ws4d_assert (u, WS4D_EPARAM);

  return ws4d_strndup2 (query, len, (const char **) &u->query, &u->alist);
}

int
ws4d_uri_set_fragment2 (struct ws4d_uri *u, const char *fragment, size_t len)
{
  /* test parameters */
  ws4d_assert (u, WS4D_EPARAM);

  return ws4d_strndup2 (fragment, len, (const char **) &u->fragment,
                        &u->alist);
}

int
ws4d_uri_copy (struct ws4d_uri *dst, struct ws4d_uri *src)
{
  /* test parameters */
  ws4d_assert (dst && src, WS4D_EPARAM);

  /* TODO: check return values */
  ws4d_uri_set_scheme (dst, ws4d_uri_get_scheme (src));
  ws4d_uri_set_host (dst, ws4d_uri_get_scheme (src));
  ws4d_uri_set_port (dst, ws4d_uri_get_port (src));
  ws4d_uri_set_path (dst, ws4d_uri_get_path (src));
  ws4d_uri_set_fragment (dst, ws4d_uri_get_fragment (src));
  ws4d_uri_set_query (dst, ws4d_uri_get_query (src));

  return WS4D_OK;
}

int
ws4d_uri_done (struct ws4d_uri *u)
{
  /* test parameters */
  ws4d_assert (u, WS4D_EPARAM);

  ws4d_alloclist_done (&u->alist);
  memset (u, 0, sizeof (struct ws4d_uri));

  return WS4D_OK;
}

int
ws4d_is_escaped (const char *s, size_t size)
{
  /* test parameters */
  ws4d_assert (s, 0);

  return ((size > 3) && (s[0] == '%') && isxdigit (s[1]) && isxdigit (s[2]));
}

int
ws4d_is_hostname (const char *s, size_t size)
{
  unsigned int cur = 0;
  unsigned int last = 0;

  /* host name is not allowed to be empty */
  /* first and last character are not allowed to be dots or hyphens */
  if (s && (size > 0) && isalnum (s[0]) && isalnum (s[size - 1]))
    {
      for (; cur < size; cur++)
        {
          if (s[cur] == '.' || s[cur] == '-')
            {
              if (cur - last < 2)
                {
                  return 0;     /* validation failed */
                }
              last = cur;
            }
          else if (!isalnum (s[cur]))
            {
              return 0;         /* validation failed */
            }
        }

      /* Only succeed if the toplabel does not end with a dot nor begins with
       * none alphabetic character.
       */
      if (last < cur && isalpha (s[last + 1]))
        {
          return 1;             /* validation succeeded */
        }
    }
  return 0;
}

int
ws4d_is_ipv4 (const char *s, size_t size)
{
  struct in_addr ia;
  char str[INET_ADDRSTRLEN];

  strncpy (str, s, INET_ADDRSTRLEN > size ? INET_ADDRSTRLEN : size);
  return ws4d_inet_pton (AF_INET, str, &ia);
}

int
ws4d_is_ipv6 (const char *s, size_t size)
{
#ifdef WITH_IPV6
  struct in6_addr ia;
  char str[INET6_ADDRSTRLEN];

  strncpy (str, s, INET6_ADDRSTRLEN > size ? INET6_ADDRSTRLEN : size);
  return ws4d_inet_pton (AF_INET6, str, &ia);
#else
  WS4D_UNUSED_PARAM (s);
  WS4D_UNUSED_PARAM (size);

  return 0;
#endif
}

int
ws4d_is_urimark (char c)
{
  return c == '-' || c == '_' || c == '.' || c == '!' || c == '~' || c == '*'
    || c == '\'' || c == '(' || c == ')';
}

int
ws4d_is_notin (char c, const char *s, size_t size)
{
  return memchr (s, c, size) == NULL;
}

int
ws4d_is_urisegment (const char *s, size_t size)
{
  unsigned int i = 0;

  /* test parameters */
  ws4d_assert (s && (size > 0), 0);

  for (; i < size; i++)
    {
      /* check if we got a valid pchar here (see chapter 3.3) */
      /* maybe we got a escaped character here (see chapter 2.4.1) */
      if (!ws4d_is_uriunreserved (s[i])
          && ws4d_is_notin (s[i], ":@&=+$,", 7)
          && !ws4d_is_escaped (s + i, size - i))
        {
          return 0;             /* validation failed */
        }
    }

  return 1;
}

int
ws4d_is_uriunreserved (char c)
{
  /* part of the unreserved character set defined in spec chapter 2.3 */
  return isalnum (c) || ws4d_is_urimark (c);
}

int
ws4d_is_uric (const char *s, size_t size)
{
  unsigned int cur = 0;

  /* test parameters */
  ws4d_assert (s && (size > 0), 0);

  for (; cur < size; cur++)
    {
      /* is the current char element of the union of reserved and unreserved
       * characters ?
       */
      /* maybe we got a escaped character here (see chapter 2.4.1) */
      if (!ws4d_is_uriunreserved (s[cur])
          && ws4d_is_notin (s[cur], ";/?:@&=+$,", 10)
          && !ws4d_is_escaped (s + cur, size - cur))
        {
          return 0;             /* validation failed */
        }
    }

  return 1;
}

int
ws4d_uri_validate_scheme (const char *s, size_t size)
{
  unsigned int i = 0;

  /* test parameters */
  ws4d_assert (s && (size > 0), 0);

  /* only proceed if the first character is alphabetic */
  if (!isalpha (s[0]))
    {
      return 0;
    }

  for (; i < size; i++)
    {
      /* check if the only the scheme character set is used (see chapter 3.1) */
      if (!isalpha (s[i]) && !isdigit (s[i])
          && ws4d_is_notin (s[i], "+-.", 3))
        {
          return 0;             /* validation failed */
        }
    }


  return 1;
}

int
ws4d_uri_validate_userinfo (const char *s, size_t size)
{
  unsigned int i = 0;

  /* test parameters */
  ws4d_assert (s && (size > 0), 0);

  for (; i < size; i++)
    {
      /* check if the only the userinfo character set is used (see chapter 3.2.2) */
      /* maybe we got a escaped character here (see chapter 2.4.1) */
      if (!ws4d_is_uriunreserved (s[i]) && ws4d_is_notin (s[i], ";:=+$,", 6)
          && !ws4d_is_escaped (s + i, size - i))
        {
          return 0;             /* validation failed */
        }
    }

  return 1;
}

int
ws4d_uri_validate_host (const char *s, size_t size)
{
#ifdef WITH_IPV6
  return ws4d_is_hostname (s, size) || ws4d_is_ipv4 (s, size)
    || ws4d_is_ipv6 (s, size);
#else
  return ws4d_is_hostname (s, size) || ws4d_is_ipv4 (s, size);
#endif
}

int
ws4d_uri_validate_port (const char *s, size_t size)
{
  unsigned int i = 0;

  /* test parameters */
  ws4d_assert (s && (size > 0), 0);

  for (; i < size; i++)
    {
      if (!isdigit (s[i]))
        {
          return 0;
        }
    }

  return 1;
}

int
ws4d_uri_validate_path (const char *s, size_t size)
{
  unsigned int cur = 1;
  unsigned int last = 1;
  char *segment_end;

  /* test parameters */
  ws4d_assert (s && (size > 0), 0);

  if (s[0] != '/')
    {
      return 0;                 /* validation failed */
    }

  /* check all segments except last */
  while ((cur < size)
         && ((segment_end = memchr (s + cur, '/', size - cur)) != NULL))
    {
      cur = segment_end - s;
      if ((cur - last) > 0)
        {
          /* only proceed if we found a valid segment here */
          if (!ws4d_is_urisegment (s + last, cur - last))
            {
              return 0;         /* validation failed */
            }
        }
      cur++;
      last = cur;
    }

  /* no check if no segment, or check of last segment */
  return (size == 1) || ws4d_is_urisegment (s + last, size - last);
}

int
ws4d_uri_validate_query (const char *s, size_t size)
{
  return ws4d_is_uric (s, size);
}

int
ws4d_uri_validate_fragment (const char *s, size_t size)
{
  return ws4d_is_uric (s, size);
}

int
ws4d_parse_uri (const char *uri, struct ws4d_uri *res, int flags)
{
  register unsigned int cur = 0, i = 0;
  char *found;
  int err;
  size_t size;

  /* test parameters */
  ws4d_assert (uri && res, WS4D_EPARAM);

  size = strlen (uri);

  /* reset uri */
  ws4d_uri_set_scheme2 (res, NULL, 0);
  ws4d_uri_set_userinfo2 (res, NULL, 0);
  ws4d_uri_set_host2 (res, NULL, 0);
  ws4d_uri_set_port2 (res, NULL, 0);
  ws4d_uri_set_path2 (res, NULL, 0);
  ws4d_uri_set_query2 (res, NULL, 0);
  ws4d_uri_set_fragment2 (res, NULL, 0);

  /* ignore leading white spaces */
  while ((cur < size) && (uri[cur] == ' '))
    {
      cur++;
    }

  /* search for a : */
  found = memchr (uri + cur, ':', size - cur);

  /* search :// the character string before :// is the scheme */
  if (found != NULL)
    {
      i = found - uri - cur;

      /* only support structure scheme with // */
      ws4d_fail ((uri[cur + i + 1] != '/')
                 && (uri[cur + i + 2] != '/'), WS4D_ERR);

      if (flags & WITH_SCHEME)
        {

          /* set scheme */
          err = ws4d_uri_set_scheme2 (res, uri + cur, i);
          ws4d_fail (err != WS4D_OK, err);

          /* fail if ws4d_uri has invalid scheme */
          ws4d_fail (!ws4d_uri_validate_scheme (res->scheme, i), WS4D_ERR);

        }

      cur += 3 + i;
    }

  /* search for a @ */
  found = memchr (uri + cur, '@', size - cur);

  /* optional userinfo is handled here */
  if (found != NULL)
    {
      i = found - uri - cur;

      /* the character string before @ contains the userinfo */
      /* only copy and validate the string if the corresponding flag was set */
      if (flags & WITH_USERINFO)
        {
          /* set scheme */
          err = ws4d_uri_set_userinfo2 (res, uri + cur, i);
          ws4d_fail (err != WS4D_OK, err);

          /* validate the specified userinfo string */
          ws4d_fail (!ws4d_uri_validate_userinfo (res->userinfo, i),
                     WS4D_ERR);
        }

      /* skip the userinfo and the @ */
      cur += i + 1;
    }

  if (uri[cur] == '[')
    {
      cur++;

      i = 0;
      while (((cur + i) < size)
             && ((isascii (uri[cur + i]) && isxdigit (uri[cur + i]))
                 || uri[cur + i] == ':' || uri[cur + i] == '.'))
        {
          i++;
        }

      ws4d_fail (uri[cur + i] != ']', WS4D_ERR);
    }
  else
    {
      /* search for : or / or ? */
      i = 0;
      while (((cur + i) < size)
             && (uri[cur + i] != '/') && (uri[cur + i] != ':')
             && (uri[cur + i] != '?'))
        {
          i++;
        }
    }

  /* the non empty charachter string before : or /  or ? is the host */
  if ((cur + i) < size)
    {
      if (flags & WITH_HOST)
        {
          /* set host */
          err = ws4d_uri_set_host2 (res, uri + cur, i);
          ws4d_fail (err != WS4D_OK, err);

          /* fail if ws4d_uri has no host or host contains reserved symbols */
          ws4d_fail (!ws4d_uri_validate_host (res->host, i), WS4D_ERR);
        }

      cur += i;

      if (uri[cur] == ']')
        {
          cur++;
        }
    }

  /* stop parsing if we have reached the end of the ws4d_uri */
  if (cur >= size)
    {
      return WS4D_OK;
    }

  /* if there is a : the authority contains a port */
  if (uri[cur] == ':')
    {
      cur += 1;

      /* search for / or ? which terminates the authority, where the
       * is the last part (spec chapter 3.2), e.g.
       * <userinfo>@<host>:<port>
       */
      i = 0;
      while (((cur + i) < size)
             && (uri[cur + i] != '/') && (uri[cur + i] != '?'))
        {
          i++;
        }

      /* charachter string before / is the port */
      /* only copy and validate the string if the corresponding flag was set */
      if (flags & WITH_PORT)
        {
          /* set port */
          err = ws4d_uri_set_port2 (res, uri + cur, i);
          ws4d_fail (err != WS4D_OK, err);

          /* fail if port contains reserved characters */
          ws4d_fail (!ws4d_uri_validate_port (res->port, i), WS4D_ERR);
        }
      cur += i;
    }

  /* stop parsing if we have reached the end of the ws4d_uri */
  if (cur >= size)
    {
      return WS4D_OK;
    }

  /* character string between host and optional query or fragment is the path */
  /* search for ? or # */
  i = 0;
  while (((cur + i) < size) && (uri[cur + i] != '?') && (uri[cur + i] != '#'))
    {
      i++;
    }

  /* only copy and validate the path if the corresponding flag was set */
  if (flags & WITH_PATH)
    {
      /* set path */
      err = ws4d_uri_set_path2 (res, uri + cur, i);
      ws4d_fail (err != WS4D_OK, err);
      /* validate path */
      if (i > 0)
        {
          ws4d_fail (!ws4d_uri_validate_path (res->path, i), WS4D_ERR);
        }
    }

  cur += i;
  /* stop parsing if we have reached the end of the ws4d_uri */
  if ((cur + 1) >= size)
    {
      return WS4D_OK;
    }

  /* handle an optional query */
  if (uri[cur] == '?')
    {
      cur += 1;

      /* search for # */
      found = memchr (uri + cur, '#', size - cur);
      if (found != NULL)
        {
          i = found - uri - cur;
        }
      else
        {
          i = size - cur;
        }

      /* only copy and validate the string if the corresponding flag was set */
      if (flags & WITH_QUERY)
        {
          /* set query */
          err = ws4d_uri_set_query2 (res, uri + cur, i);
          ws4d_fail (err != WS4D_OK, err);

          /* fail if query is not welformed */
          ws4d_fail (!ws4d_uri_validate_query (res->query, i), WS4D_ERR);
        }

      cur += i;
    }

  /* stop parsing if we have reached the end of the ws4d_uri */
  if ((cur + 1) >= size)
    {
      return WS4D_OK;
    }

  /* handle an optional fragment */
  if (uri[cur] == '#')
    {
      cur += 1;

      /* only copy and validate the string if the corresponding flag was set */
      if (flags & WITH_FRAGMENT)
        {
          /* set fragment */
          err = ws4d_uri_set_fragment2 (res, uri + cur, size - cur);
          ws4d_fail (err != WS4D_OK, err);

          /* fail if fragment is not welformed */
          ws4d_fail (!ws4d_uri_validate_fragment (res->fragment, size - cur),
                     WS4D_ERR);
        }
    }

  return WS4D_OK;
}


int
ws4d_cmp_uri (const char *s1, const char *s2)
{
  int flags = WITH_ALL, len;
  struct ws4d_uri u1, u2;
  const char *p1 = NULL;
  const char *p2 = NULL;
  ws4d_uri_init (&u1);
  ws4d_uri_init (&u2);

  /* first create ws4d_uri structs from the specified ws4d_uri strings */
  if (ws4d_parse_uri (s1, &u1, flags) != SOAP_OK)
    {
      ws4d_uri_done (&u1);
      return SOAP_SYNTAX_ERROR;
    }

  if (ws4d_parse_uri (s2, &u2, flags) != SOAP_OK)
    {
      ws4d_uri_done (&u1);
      ws4d_uri_done (&u2);
      return SOAP_SYNTAX_ERROR;
    }

  /* s1 and s2 need to have the same scheme, e.g. http */
  if (STRCASECMP (u1.scheme, u2.scheme) != 0)
    {
      ws4d_uri_done (&u1);
      ws4d_uri_done (&u2);
      return SOAP_ERR;
    }

  /* s1 and s2 need to have the same authority; refering to rfc2396 an
   * authority has the following syntax:
   * [userinfo@]host[:port]
   */

  /* first compare the optional userinfos */
  if ((u1.userinfo || u2.userinfo)
      && strcmp ((u1.userinfo ? u1.userinfo : ""),
                 (u2.userinfo ? u2.userinfo : "")) != 0)
    {
      ws4d_uri_done (&u1);
      ws4d_uri_done (&u2);
      return SOAP_ERR;
    }

  /* compare the host parts; refering to chapter 6 of the rfc the host part
   * of an ws4d_uri is not case sensitive.
   */
  if (STRCASECMP (u1.host, u2.host) != 0)
    {
      ws4d_uri_done (&u1);
      ws4d_uri_done (&u2);
      return SOAP_ERR;
    }

  /* compare the optional ports */
  p1 = (u1.port ? u1.port : "");
  p2 = (u2.port ? u2.port : "");
  if ((u1.port || u2.port) && strcmp (p1, p2) != 0)
    {
      /* maybe the default port was used in one of the uris */
      if ((strlen (p1) != 0 || strcmp (p2, "80") != 0)
          && (strlen (p2) != 0 || strcmp (p1, "80") != 0))
        {
          ws4d_uri_done (&u1);
          ws4d_uri_done (&u2);
          return SOAP_ERR;
        }
    }

  /* compare the optional paths of the specified uris */
  if (!u1.path && !u2.path)
    {
      return SOAP_OK;
    }

  /* check if both uris have a path */
  if ((!u1.path && u2.path) || (u1.path && !u2.path))
    {
      ws4d_uri_done (&u1);
      ws4d_uri_done (&u2);
      return SOAP_ERR;
    }

  /* At first we have to ensure, that the path segments are absolute and do
   * not contain . or .. segments (see ws-discovery chapter 5.1).
   */
  if (strstr (u1.path, ".") || strstr (u1.path, "..")
      || strstr (u2.path, ".") || strstr (u2.path, ".."))
    {
      ws4d_uri_done (&u1);
      ws4d_uri_done (&u2);
      return SOAP_SYNTAX_ERROR;
    }

  /* compare the path segment (not string) wise with each other; refer
   * to ws-discovery spec chapter 5.1
   */
  len = strlen (u1.path);
  len = (u1.path[len - 1] == '/') ? len - 1 : len;      /* ommit trailing slashes */
  /* check if u1 is a segment wise prefix of u2 */
  if ((strncmp (u1.path, u2.path, len) != 0) || (u2.path[len] != '/'
                                                 && u2.path[len] != '\0'))
    {
      ws4d_uri_done (&u1);
      ws4d_uri_done (&u2);
      return SOAP_ERR;
    }

  /* free resources */
  ws4d_uri_done (&u1);
  ws4d_uri_done (&u2);
  /* the query and fragment parts can be ignored (refering to the spec) */
  return SOAP_OK;
}

int
ws4d_uri_tostrlen (struct ws4d_uri *res)
{
  register unsigned int length = 0;
  ws4d_assert (res, 0);
  /* check if minimal parts are there */
  if (!res->scheme || !res->host || !res->path)
    {
      return 0;
    }

  /* calculate length of ws4d_uri string */
  length += strlen (res->scheme);
  length += 3;                  /* strlen ("://") */
  if (res->userinfo)
    {
      length += strlen (res->userinfo);
      length += 1;              /* strlen ("@") */
    }
  length += strlen (res->host);
  if (ws4d_is_ipv6 (res->host, strlen (res->host)))
    {
      length += 2;              /* strlen ("[]") */
    }
  if (res->port)
    {
      length += 1;              /* strlen (":") */
      length += strlen (res->port);
    }
  if (res->path)
    {
      length += strlen (res->path);
      if (res->path[0] != '/')
        {
          length += 1;          /* strlen ("/") */
        }
    }
  if (res->query)
    {
      length += 1;              /* strlen ("?") */
      length += strlen (res->query);
    }
  if (res->fragment)
    {
      length += 1;              /* strlen ("#") */
      length += strlen (res->fragment);
    }

  return length;
}

int
ws4d_uri_tostr (struct ws4d_uri *res, char *uri, size_t size)
{
  register unsigned int length = 0;
  ws4d_assert (res && uri && (size > 0), WS4D_ERR);
  length = ws4d_uri_tostrlen (res);
  /* is ws4d_uri string length sufficient? */
  if (length > size)
    {
      return WS4D_ERR;
    }

  /* reset ws4d_uri string */
  memset (uri, 0, size);
  /* build ws4d_uri */
  strncat (uri, res->scheme, strlen (res->scheme));
  strncat (uri, "://", 3);
  if (res->userinfo)
    {
      strncat (uri, res->userinfo, strlen (res->userinfo));
      strncat (uri, "@", 1);
    }

  if (ws4d_is_ipv6 (res->host, strlen (res->host)))
    {
      strncat (uri, "[", 1);
    }
  strncat (uri, res->host, strlen (res->host));
  if (ws4d_is_ipv6 (res->host, strlen (res->host)))
    {
      strncat (uri, "]", 1);
    }

  if (res->port)
    {
      strncat (uri, ":", 1);
      strncat (uri, res->port, strlen (res->port));
    }
  if (res->path)
    {
      if (res->path[0] != '/')
        {
          strncat (uri, "/", 1);
        }
      strncat (uri, res->path, strlen (res->path));
    }
  if (res->query)
    {
      strncat (uri, "?", 1);
      strncat (uri, res->query, strlen (res->query));
    }
  if (res->fragment)
    {
      strncat (uri, "#", 1);
      strncat (uri, res->fragment, strlen (res->fragment));
    }

  return WS4D_OK;
}
