/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *  Created on: 14.08.2008
 *      Author: Elmar Zeeb
 */

#include "ws4d_misc.h"
#include "ws4d_epr.h"

#define _ws4d_epr_check(epr) \
    (epr) && ws4d_list_check(&epr->list) && ws4d_list_check(&epr->plugins)

/* forward declarations */

static struct ws4d_epr_plugin
  *ws4d_epr_get_plugin (struct ws4d_epr *epr, const char *id);

/* implementation */

int
ws4d_epr_init (struct ws4d_epr *epr)
{
  return ws4d_epr_init2 (epr, NULL);
}

int
ws4d_epr_init2 (struct ws4d_epr *epr, struct ws4d_abs_allocator *allocator)
{
  /* test parameters */
  ws4d_assert (epr, WS4D_EPARAM);

  epr->elementof = NULL;
  epr->valid = 0;

  WS4D_INIT_LIST (&epr->list);
  if (allocator != NULL)
    {
      ws4d_stringlist_init2 (&epr->Address, allocator);
      ws4d_allocator_copy (allocator, &epr->alist);
    }
  else
    {
      WS4D_ALLOCLIST_INIT (&epr->alist);
      ws4d_stringlist_init2 (&epr->Address, &epr->alist);
    }
  WS4D_INIT_LIST (&epr->plugins);
  WS4D_INIT_LIST (&epr->copies);
  WS4D_INIT_LIST (&epr->copyof);
  ws4d_mutex_init (&epr->lock);

  return WS4D_OK;
}

int
ws4d_epr_check (struct ws4d_epr *epr)
{
  return _ws4d_epr_check (epr);
}

#ifdef WITH_MUTEXES
void
ws4d_epr_lock (struct ws4d_epr *epr)
{
  /* test parameters */
  ws4d_assert (_ws4d_epr_check (epr),);

  ws4d_mutex_lock (&epr->lock);
}

void
ws4d_epr_unlock (struct ws4d_epr *epr)
{
  /* test parameters */
  ws4d_assert (_ws4d_epr_check (epr),);

  ws4d_mutex_unlock (&epr->lock);
}
#endif

int
ws4d_epr_copy (struct ws4d_epr *dst, struct ws4d_epr *src)
{
  register struct ws4d_epr_plugin *plugin, *next;
  int err = 0;

  /* test parameters */
  ws4d_assert ((src != dst) && _ws4d_epr_check (dst)
               && _ws4d_epr_check (src), WS4D_EPARAM);

  /* update address */
  ws4d_stringlist_clear (&dst->Address);
  err = ws4d_stringlist_add (&dst->Address,
                             ws4d_stringlist_tostring (&src->Address));
  ws4d_fail (err != WS4D_OK, err);

  /* update existing and append nonexisting plugins */
  ws4d_list_foreach (plugin, next, &src->plugins, struct ws4d_epr_plugin,
                     list)
  {
    if (plugin->id && plugin->cb && plugin->cb->fcopy)
      {
        int is_new = 0;
        struct ws4d_epr_plugin *copy = NULL;

        copy = ws4d_epr_get_plugin (dst, plugin->id);

        if (!copy)
          {
            is_new = 1;

            copy = ws4d_malloc (sizeof (struct ws4d_epr_plugin));
            ws4d_assert (copy, WS4D_EOM);

            copy->id = plugin->id;
          }

        err = plugin->cb->fcopy (dst, copy, plugin);
        if ((err == WS4D_OK) && plugin->cb && plugin->cb->fdelete)
          {
            if (is_new)
              {
                ws4d_list_add_tail (&copy->list, &dst->plugins);
              }
          }
        else
          {
            if (is_new)
              {
                ws4d_free (copy);
                continue;
              }
            return err;
          }

      }
    ws4d_list_move_tail (&dst->copyof, &src->copies);
  }

  /* update epr status */
  if (dst->valid != src->valid)
    {
      if (src->valid)
        {
          ws4d_epr_validate (dst);
        }
      else
        {
          ws4d_epr_invalidate (dst);
        }
    }

  return WS4D_OK;
}

int
ws4d_epr_done (struct ws4d_epr *epr)
{
  /* test parameters */
  ws4d_assert (_ws4d_epr_check (epr), WS4D_EPARAM);

  /* remove all copy references that refer to this epr */
  if (!ws4d_list_empty (&epr->copies))
    {
      register struct ws4d_epr *copy = NULL, *next = NULL;

      ws4d_list_foreach (copy, next, &epr->copies, struct ws4d_epr, copyof)
      {
        if (!ws4d_list_empty (&epr->copyof))
          {
            ws4d_list_move_tail (&copy->copyof, &epr->copyof);
          }
        else
          {
            ws4d_list_del (&copy->copyof);
          }
      }
    }

  /* remove copyof reference */
  if (!ws4d_list_empty (&epr->copyof))
    {
      ws4d_list_del (&epr->copyof);
    }

  /* delete all plugins */
  ws4d_epr_remove_plugins (epr);

  /* make _ws4d_epr_check() fail */
  ws4d_stringlist_done (&epr->Address);
  epr->valid = 0;
  WS4D_LIST_DONE (&epr->list);
  WS4D_LIST_DONE (&epr->plugins);

  /* free all memory related to this epr */
  ws4d_alloclist_done (&epr->alist);

  ws4d_mutex_destroy (&epr->lock);

  return WS4D_OK;
}

int
ws4d_epr_validate (struct ws4d_epr *epr)
{
  /* test parameters */
  ws4d_assert (_ws4d_epr_check (epr) && (epr->valid == 0), WS4D_EPARAM);

  epr->valid = 1;

  return WS4D_OK;
}

int
ws4d_epr_invalidate (struct ws4d_epr *epr)
{
  register struct ws4d_epr_plugin *plugin, *nextp;
  register struct ws4d_epr *copy, *nextc;

  /* test parameters */
  ws4d_assert (_ws4d_epr_check (epr) && (epr->valid == 1), WS4D_EPARAM);

  epr->valid = 0;

  ws4d_list_foreach (copy, nextc, &epr->copies, struct ws4d_epr, copyof)
  {
    ws4d_epr_invalidate (copy);
  }

  ws4d_list_foreach (plugin, nextp, &epr->plugins,
                     struct ws4d_epr_plugin, list)
  {
    if (plugin->cb && plugin->cb->finvalidate)
      {
        plugin->cb->finvalidate (epr, plugin);
      }
  }

  return WS4D_OK;
}

int
ws4d_epr_set_Addrs (struct ws4d_epr *epr, const char *Addrs)
{
  /* test parameters */
  ws4d_assert (_ws4d_epr_check (epr) && Addrs, WS4D_EPARAM);

  if (!ws4d_stringlist_isempty (&epr->Address))
    {
      ws4d_stringlist_clear (&epr->Address);
    }

  return ws4d_stringlist_add (&epr->Address, Addrs);
}

int
ws4d_epr_add_Addrs (struct ws4d_epr *epr, const char *Addrs)
{
  /* test parameters */
  ws4d_assert (_ws4d_epr_check (epr) && Addrs, WS4D_EPARAM);

  return ws4d_stringlist_add (&epr->Address, Addrs);
}

int
ws4d_epr_register_plugin (struct ws4d_epr *epr,
                          int (*fcreate) (struct ws4d_epr *epr,
                                          struct ws4d_epr_plugin *,
                                          void *), void *arg)
{
  int result;
  struct ws4d_epr_plugin *plugin;

  /* test parameters */
  ws4d_assert (_ws4d_epr_check (epr) && fcreate, WS4D_EPARAM);

  plugin = ws4d_malloc (sizeof (struct ws4d_epr_plugin));
  ws4d_fail (!plugin, WS4D_EOM);

  result = fcreate (epr, plugin, arg);
  if ((result == WS4D_OK) && plugin->cb && plugin->cb->fdelete)
    {
      ws4d_list_add_tail (&plugin->list, &epr->plugins);
      return WS4D_OK;
    }
  else
    {
      ws4d_free (plugin);
      return result;
    }
}

int
ws4d_epr_register_plugin_singleton (struct ws4d_epr *epr,
                                    int (*fcreate) (struct ws4d_epr * epr,
                                                    struct ws4d_epr_plugin *,
                                                    void *), void *arg,
                                    const char *id, void **plugin)
{
  int ret;

  /* test parameters */
  ws4d_assert (plugin, WS4D_EPARAM);

  /* check if plugin is already registered */
  *plugin = ws4d_epr_get_plugindata (epr, id);
  if (*plugin != NULL)
    {
      return WS4D_DUPLICATE;
    }

  /* register plugin */
  ret = ws4d_epr_register_plugin (epr, fcreate, arg);

  /* get plugin data */
  *plugin = ws4d_epr_get_plugindata (epr, id);
  ws4d_assert(*plugin, WS4D_ERR);

  return ret;
}


static struct ws4d_epr_plugin *
ws4d_epr_get_plugin (struct ws4d_epr *epr, const char *id)
{
  register struct ws4d_epr_plugin *entry, *next;

  /* test parameters */
  ws4d_assert (_ws4d_epr_check (epr) && id, NULL);

  ws4d_list_foreach (entry, next, &epr->plugins, struct ws4d_epr_plugin, list)
  {
    if (entry->id == id)
      {
        return entry;
      }
  }

  ws4d_list_foreach (entry, next, &epr->plugins, struct ws4d_epr_plugin, list)
  {
    if (!strcmp (entry->id, id))
      {
        return entry;
      }
  }

  return NULL;
}

void *
ws4d_epr_get_plugindata (struct ws4d_epr *epr, const char *id)
{
  struct ws4d_epr_plugin *entry;

  /* test parameters */
  ws4d_assert (_ws4d_epr_check (epr) && id, NULL);

  entry = ws4d_epr_get_plugin (epr, id);

  return entry == NULL ? NULL : entry->data;
}

int
ws4d_epr_remove_plugin (struct ws4d_epr *epr, const char *id)
{
  struct ws4d_epr_plugin *plugin = NULL;

  /* test parameters */
  ws4d_assert (_ws4d_epr_check (epr) && id, WS4D_EPARAM);

  plugin = ws4d_epr_get_plugin (epr, id);
  ws4d_fail (plugin == NULL, WS4D_ERR);

  if (plugin->cb && plugin->cb->fdelete)
    {
      plugin->cb->fdelete (epr, plugin);
    }

  ws4d_list_del (&plugin->list);
  ws4d_free (plugin);

  return WS4D_OK;
}

void
ws4d_epr_remove_plugins (struct ws4d_epr *epr)
{
  register struct ws4d_epr_plugin *plugin = NULL, *next = NULL;

  /* test parameters */
  ws4d_assert (_ws4d_epr_check (epr),);

  ws4d_list_foreach (plugin, next, &epr->plugins, struct ws4d_epr_plugin,
                     list)
  {
    if (plugin->cb && plugin->cb->fdelete)
      {
        plugin->cb->fdelete (epr, plugin);
      }

    ws4d_list_del (&plugin->list);
    ws4d_free (plugin);
  }
}
