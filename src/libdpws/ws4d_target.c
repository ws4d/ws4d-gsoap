/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *  Created on: 18.08.2008
 *      Author: Elmar Zeeb
 */

#include "ws4d_misc.h"
#include "ws4d_epr.h"
#include "ws4d_target.h"

#define WS4D_TARGET_INACTIVE   (0)
#define WS4D_TARGET_ACTIVE     (1)

#define WS4D_TARGETEP_ID "WS4D-TargetEp-0.1"

const char *ws4d_targetep_id = WS4D_TARGETEP_ID;

struct ws4d_targetep_msgids
{
  const char *ProbeMsgId;
  const char *ResolveMsgId;
};

struct ws4d_targetep_plugin
{
  int state;
  struct ws4d_stringlist XAddrs;
  ws4d_qnamelist Types;
  struct ws4d_stringlist Scopes;
  unsigned int MetadataVersion;
  struct ws4d_targetep_msgids *msgids;
  void *targetservice;
};

/* forward declarations */

static struct ws4d_targetep_plugin
  *ws4d_targetep_plugindata (struct ws4d_epr *epr);

static int ws4d_targetep_fcreate (struct ws4d_epr *epr,
                                  struct ws4d_epr_plugin *plugin, void *arg);

static int ws4d_targetep_fcopy (struct ws4d_epr *epr,
                                struct ws4d_epr_plugin *dst,
                                struct ws4d_epr_plugin *src);

static void ws4d_targetep_fdelete (struct ws4d_epr *epr,
                                   struct ws4d_epr_plugin *p);

static int ws4d_targetep_finvalidate (struct ws4d_epr *epr,
                                      struct ws4d_epr_plugin *p);

static struct ws4d_targetep_plugin
  *ws4d_targetep_checkregister_plugin (struct ws4d_epr *epr);

/* register callbacks */

static struct ws4d_epr_plugin_cb ws4d_targetep_callbacks = {
  ws4d_targetep_fdelete, ws4d_targetep_fcopy, ws4d_targetep_finvalidate
};

/* implementation */

static struct ws4d_targetep_plugin *
ws4d_targetep_checkregister_plugin (struct ws4d_epr *epr)
{
  struct ws4d_targetep_plugin *plugin = ws4d_targetep_plugindata (epr);

  /* check if plugin is already registered */
  if (!plugin)
    {
      /* register plugin at epr */
      ws4d_epr_register_plugin (epr, ws4d_targetep_fcreate, NULL);

      plugin = ws4d_targetep_plugindata (epr);
      ws4d_assert (plugin, NULL);
    }

  return plugin;
}

static struct ws4d_targetep_plugin *
ws4d_targetep_plugindata (struct ws4d_epr *epr)
{
  return (struct ws4d_targetep_plugin *) ws4d_epr_get_plugindata (epr,
                                                                  ws4d_targetep_id);
}

static int
ws4d_targetep_fcopy (struct ws4d_epr *epr, struct ws4d_epr_plugin *dst,
                     struct ws4d_epr_plugin *src)
{
  struct ws4d_targetep_plugin *dst_data = NULL, *src_data = NULL;
  int err;

  dst->cb = &ws4d_targetep_callbacks;

  src_data = src->data;
  if (dst->data)
    {
      dst_data = dst->data;
    }
  else
    {
      dst_data = ws4d_malloc (sizeof (struct ws4d_targetep_plugin));
      ws4d_fail (dst_data == NULL, WS4D_EOM);

      ws4d_qnamelist_init (&dst_data->Types);
      ws4d_stringlist_init (&dst_data->Scopes);
      ws4d_stringlist_init (&dst_data->XAddrs);

      dst->data = dst_data;
    }

  /* update MetadataVersion */
  dst_data->MetadataVersion = src_data->MetadataVersion;

  /* update message ids */
  if (src_data->msgids)
    {
      if (!dst_data->msgids)
        {
          dst_data->msgids =
            ws4d_malloc_alist (sizeof (struct ws4d_targetep_msgids),
                               ws4d_epr_get_alist (epr));
          ws4d_fail (dst_data->msgids == NULL, WS4D_EOM);
        }

      /* update ProbeMsgId */
      err =
        ws4d_strdup2 (src_data->msgids->ProbeMsgId,
                      (const char **) &dst_data->msgids->ProbeMsgId,
                      ws4d_epr_get_alist (epr));
      ws4d_fail (err != WS4D_OK, err);

      /* update ResolveMsgId */
      err =
        ws4d_strdup2 (src_data->msgids->ResolveMsgId,
                      (const char **) &dst_data->msgids->ResolveMsgId,
                      ws4d_epr_get_alist (epr));
      ws4d_fail (err != WS4D_OK, err);

    }
  else
    {
      if (dst_data->msgids)
        {
          ws4d_free_alist (dst_data->msgids);
          dst_data->msgids = NULL;
        }
    }

  /* update Types */
  err = ws4d_qnamelist_copy (&src_data->Types, &dst_data->Types);
  ws4d_fail (err != WS4D_OK, err);

  /* update Scopes */
  err = ws4d_stringlist_copy (&src_data->Scopes, &dst_data->Scopes);
  ws4d_fail (err != WS4D_OK, err);

  /* update target service */
  dst_data->targetservice = src_data->targetservice;

  /* update XAddrs */
  return ws4d_stringlist_copy (&src_data->XAddrs, &dst_data->XAddrs);
}

static void
ws4d_targetep_fdelete (struct ws4d_epr *epr, struct ws4d_epr_plugin *p)
{
  struct ws4d_targetep_plugin *data = NULL;

  WS4D_UNUSED_PARAM (epr);

  /* test parameters */
  ws4d_assert (p && p->data,);

  data = p->data;

  ws4d_stringlist_done (&data->Scopes);
  ws4d_stringlist_done (&data->XAddrs);
  ws4d_qnamelist_done (&data->Types);

  ws4d_free (data);
}

static int
ws4d_targetep_finvalidate (struct ws4d_epr *epr, struct ws4d_epr_plugin *p)
{
  WS4D_UNUSED_PARAM (p);

  ws4d_epr_remove_plugin(epr, ws4d_targetep_id);

  return WS4D_OK;
}

static int
ws4d_targetep_fcreate (struct ws4d_epr *epr, struct ws4d_epr_plugin *plugin,
                       void *arg)
{
  struct ws4d_targetep_plugin *data = NULL;

  WS4D_UNUSED_PARAM (epr);
  WS4D_UNUSED_PARAM (arg);

  plugin->id = ws4d_targetep_id;
  plugin->cb = &ws4d_targetep_callbacks;

  data = ws4d_malloc (sizeof (struct ws4d_targetep_plugin));
  ws4d_fail (data == NULL, WS4D_EOM);

  data->state = WS4D_TARGET_INACTIVE;
  ws4d_qnamelist_init (&data->Types);
  ws4d_stringlist_init (&data->Scopes);
  ws4d_stringlist_init (&data->XAddrs);
  data->MetadataVersion = -1;

  plugin->data = data;

  return WS4D_OK;
}

static int
_ws4d_targetep_changed (struct ws4d_targetep_plugin *plugin)
{
  if (plugin->state == WS4D_TARGET_ACTIVE)
    {
      plugin->MetadataVersion++;
    }

  return WS4D_OK;
}

int
ws4d_targetep_set_XAddrs (struct ws4d_epr *epr, const char *XAddrs)
{
  struct ws4d_targetep_plugin *plugin =
    ws4d_targetep_checkregister_plugin (epr);

  /* test parameters */
  ws4d_assert (plugin, WS4D_EPARAM);

  if (!ws4d_stringlist_isempty (&plugin->XAddrs))
    {
      ws4d_stringlist_clear (&plugin->XAddrs);
    }

  return ws4d_stringlist_add (&plugin->XAddrs, XAddrs);
}

struct ws4d_stringlist *
ws4d_targetep_get_XAddrsList (struct ws4d_epr *epr)
{
  struct ws4d_targetep_plugin *plugin = ws4d_targetep_plugindata (epr);

  /* test parameters */
  ws4d_fail (plugin == NULL, NULL);

  return &plugin->XAddrs;
}

int
ws4d_targetep_set_ProbeMsgId (struct ws4d_epr *epr, const char *ProbeMsgId)
{
  struct ws4d_targetep_plugin *plugin =
    ws4d_targetep_checkregister_plugin (epr);

  /* test parameters */
  ws4d_assert (plugin, WS4D_ERR);

  if (!plugin->msgids)
    {
      plugin->msgids =
        ws4d_malloc_alist (sizeof (struct ws4d_targetep_msgids),
                           ws4d_epr_get_alist (epr));
      ws4d_fail (!plugin->msgids, WS4D_EOM);
    }

  return ws4d_strdup2 (ProbeMsgId, &(plugin->msgids->ProbeMsgId),
                       ws4d_epr_get_alist (epr));
}

const char *
ws4d_targetep_get_ProbeMsgId (struct ws4d_epr *epr)
{
  struct ws4d_targetep_plugin *plugin = ws4d_targetep_plugindata (epr);

  /* test parameters */
  ws4d_fail (plugin == NULL, NULL);

  return plugin->msgids != NULL ? plugin->msgids->ProbeMsgId : NULL;
}

int
ws4d_targetep_set_ResolveMsgId (struct ws4d_epr *epr,
                                const char *ResolveMsgId)
{
  struct ws4d_targetep_plugin *plugin =
    ws4d_targetep_checkregister_plugin (epr);

  /* test parameters */
  ws4d_assert (plugin, WS4D_ERR);

  if (!plugin->msgids)
    {
      plugin->msgids =
        ws4d_malloc_alist (sizeof (struct ws4d_targetep_msgids),
                           ws4d_epr_get_alist (epr));
      ws4d_fail (!plugin->msgids, WS4D_EOM);
    }

  return ws4d_strdup2 (ResolveMsgId, &(plugin->msgids->ResolveMsgId),
                       ws4d_epr_get_alist (epr));
}

const char *
ws4d_targetep_get_ResolveMsgId (struct ws4d_epr *epr)
{
  struct ws4d_targetep_plugin *plugin = ws4d_targetep_plugindata (epr);

  /* test parameters */
  ws4d_fail (plugin == NULL, NULL);

  return plugin->msgids != NULL ? plugin->msgids->ResolveMsgId : NULL;
}

static ws4d_qnamelist *
_ws4d_targetep_get_TypeList(struct ws4d_targetep_plugin *plugin)
{
  /* test parameters */
  ws4d_fail (plugin == NULL, NULL);

  return &plugin->Types;
}

ws4d_qnamelist *
ws4d_targetep_change_TypeList (struct ws4d_epr *epr)
{
  int err;
  struct ws4d_targetep_plugin *plugin =
    ws4d_targetep_checkregister_plugin (epr);

  err = _ws4d_targetep_changed (plugin);
  ws4d_fail (err != WS4D_OK, NULL);

  return _ws4d_targetep_get_TypeList (plugin);
}

ws4d_qnamelist *
ws4d_targetep_get_TypeList (struct ws4d_epr * epr)
{
  return _ws4d_targetep_get_TypeList(ws4d_targetep_plugindata (epr));
}

int
ws4d_targetep_matches_Types (struct ws4d_epr *epr,
                             struct ws4d_qnamelist *Types)
{
  struct ws4d_targetep_plugin *plugin = ws4d_targetep_plugindata (epr);

  /* test parameters */
  ws4d_fail (plugin == NULL, 0);

  /* dont match with emtpy types */
  if (ws4d_qnamelist_isempty (&plugin->Types))
    {
      return 0;
    }

  return ws4d_qnamelist_compare(Types, &plugin->Types) < 1;
}

static struct ws4d_stringlist *_ws4d_targetep_get_ScopeList (struct ws4d_targetep_plugin *plugin)
{
  /* test paramaters */
  ws4d_assert (plugin, WS4D_ERR);

  return &plugin->Scopes;
}

struct ws4d_stringlist *ws4d_targetep_get_ScopeList (struct ws4d_epr *epr)
{
  return _ws4d_targetep_get_ScopeList(ws4d_targetep_checkregister_plugin (epr));
}

struct ws4d_stringlist *ws4d_targetep_change_ScopeList (struct ws4d_epr *epr)
{
  int err;
  struct ws4d_targetep_plugin *plugin =
    ws4d_targetep_checkregister_plugin (epr);

  /* test paramaters */
  ws4d_assert (plugin, WS4D_ERR);

  err = _ws4d_targetep_changed (plugin);
  ws4d_fail (err != WS4D_OK, NULL);

  return _ws4d_targetep_get_ScopeList(plugin);
}

int ws4d_targetep_matches_Scopes (struct ws4d_epr *epr,
                                  struct ws4d_stringlist *Scopes,
                                  ws4d_stringlist_matchingfunction scope_match_func)
{
  struct ws4d_targetep_plugin *plugin = ws4d_targetep_plugindata (epr);

  /* test parameters */
  ws4d_fail (plugin == NULL, 0);

  /* scopes list is empty */
  if (ws4d_stringlist_isempty (&plugin->Scopes))
    {
      return 0;
    }

  return ws4d_stringlist_compare2(Scopes, &plugin->Scopes, scope_match_func, NULL) < 1;
}

int
ws4d_targetep_set_MetadataVersion (struct ws4d_epr *epr, int MetadataVersion)
{
  struct ws4d_targetep_plugin *plugin =
    ws4d_targetep_checkregister_plugin (epr);

  /* test parameters */
  ws4d_assert (plugin, WS4D_ERR);

  plugin->MetadataVersion = MetadataVersion;

  return WS4D_OK;
}

int
ws4d_targetep_get_MetadataVersion (struct ws4d_epr *elem)
{
  struct ws4d_targetep_plugin *plugin = ws4d_targetep_plugindata (elem);

  /* test parameters */
  ws4d_fail (plugin == NULL, -1);

  return plugin->MetadataVersion;
}

/* TODO: set InstanceId if != 0 */
/* TODO: set MessageNumber if != 0 */
int
ws4d_targetep_activate (struct ws4d_epr *epr, int MessageNumber,
                        int InstanceId, int MetadataVersion)
{
  struct ws4d_targetep_plugin *plugin = ws4d_targetep_plugindata (epr);

  WS4D_UNUSED_PARAM (MessageNumber);
  WS4D_UNUSED_PARAM (InstanceId);

  /* test parameters */
  ws4d_fail (plugin == NULL, WS4D_ERR);

  ws4d_epr_lock (epr);
  if (plugin->state != WS4D_TARGET_INACTIVE)
    {
      ws4d_epr_unlock (epr);
      return WS4D_ERR;
    }

  plugin->state = WS4D_TARGET_ACTIVE;

  if (MetadataVersion > 0)
    {
      plugin->MetadataVersion = MetadataVersion;
    }
  else
    {
      plugin->MetadataVersion = ws4d_systime_s ();
    }

  ws4d_epr_unlock (epr);
  return WS4D_OK;
}

int
ws4d_targetep_deactivate (struct ws4d_epr *epr)
{
  struct ws4d_targetep_plugin *plugin = ws4d_targetep_plugindata (epr);

  /* test parameters */
  ws4d_fail (plugin == NULL, WS4D_ERR);

  ws4d_epr_lock (epr);
  if (plugin->state != WS4D_TARGET_ACTIVE)
    {
      ws4d_epr_unlock (epr);
      return WS4D_ERR;
    }

  plugin->state = WS4D_TARGET_INACTIVE;

  ws4d_epr_unlock (epr);
  return WS4D_OK;
}

int
ws4d_targetep_isactive (struct ws4d_epr *epr)
{
  struct ws4d_targetep_plugin *plugin = ws4d_targetep_plugindata (epr);

  /* test parameters */
  ws4d_fail (plugin == NULL, 0);

  return plugin->state == WS4D_TARGET_ACTIVE;
}

int
ws4d_targetep_changeMetadata (struct ws4d_epr *epr)
{
  struct ws4d_targetep_plugin *plugin = ws4d_targetep_plugindata (epr);

  /* test parameters */
  ws4d_fail (plugin == NULL, WS4D_ERR);

  return _ws4d_targetep_changed (plugin);
}

int
ws4d_targetep_set_ts (struct ws4d_epr *epr, void *targetservice)
{
  struct ws4d_targetep_plugin *plugin =
    ws4d_targetep_checkregister_plugin (epr);

  /* test parameters */
  ws4d_assert (plugin && targetservice, WS4D_ERR);

  plugin->targetservice = targetservice;

  return WS4D_OK;
}

void *
ws4d_targetep_get_ts (struct ws4d_epr *epr)
{
  struct ws4d_targetep_plugin *plugin = ws4d_targetep_plugindata (epr);

  /* test parameters */
  ws4d_fail (plugin == NULL, NULL);

  return plugin->targetservice;
}
