/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "ws4d_misc.h"

const char *
ws4d_locstring_get (struct ws4d_locstring *string, int size, const char *lang)
{
  register int i;

  /* test parameters */
  ws4d_assert (string && lang, NULL);

  for (i = 0; i < size; i++)
    {
      if (string[i].lang && !STRCASECMP (string[i].lang, lang))
        {
          return string[i].string;
        }
    }

  return NULL;
}

int
ws4d_locstring_copy (struct ws4d_locstring *dst, int dst_size,
                     struct ws4d_locstring *src, int src_size,
                     struct ws4d_abs_allocator *alist)
{
  register int i;

  /* test parameters */
  ws4d_assert (src && dst && alist, WS4D_EPARAM);

  ws4d_fail (src_size > dst_size, WS4D_EPARAM);

  for (i = 0; i < dst_size; i++)
    {
      if (i < src_size)
        {
          ws4d_strdup2 (src[i].lang, &dst[i].lang, alist);
          ws4d_strdup2 (src[i].string, &dst[i].string, alist);
        }
      else
        {
          ws4d_free_alist ((void *) dst[i].lang);
          ws4d_free_alist ((void *) dst[i].string);
        }
    }

  return WS4D_OK;
}

void
ws4d_locstring_free (struct ws4d_locstring *string, int size)
{
  register int i;

  ws4d_assert (string && (size > 0),);

  for (i = 0; i < size; i++)
    {
      ws4d_free_alist ((void *) string[i].lang);
      ws4d_free_alist ((void *) string[i].string);
    }
}
