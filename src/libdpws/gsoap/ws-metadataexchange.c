/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "stdsoap2.h"
#include "soap_misc.h"

#include "wst.nsmap"
#include "ws-addressing.h"
#include "ws-metadataexchange.h"

struct mex_section_type
{
  struct ws4d_list_node list;
  const char *dialect;
  const char *tag;
  const struct mex_section_type_serializers *serializers;
};

struct mex_section
{
  struct ws4d_list_node list;
  const char *endpoint;
  const char *dialect;
  const char *identifier;
  void *content;
};

struct mex_plugin_data
{
  struct ws4d_list_node types;
  struct ws4d_list_node sections;
  struct ws4d_abs_allocator alist;
#ifdef WITH_MUTEXES
    WS4D_MUTEX (lock);
#endif
};

#define MEX_PLUGIN_ID "WS-Metadata-PLUGIN-0.1"
const char *mex_plugin_id = MEX_PLUGIN_ID;

/* forward declarations */

INLINE struct mex_plugin_data *mex_get_plugindata (struct soap *soap);

static struct mex_section_type *_mex_get_metadata_section_type (struct
                                                                mex_plugin_data
                                                                *data,
                                                                const char
                                                                *dialect);

static struct mex_section_type *mex_get_metadata_section_type (struct soap
                                                               *soap,
                                                               const char
                                                               *dialect);

static void mex_free_metadata_section_type (struct mex_section_type *type);

static void mex_remove_metadata_types (struct mex_plugin_data *data);

static struct mex_section *_mex_get_metadata_section (struct mex_plugin_data
                                                      *data,
                                                      const char *endpoint,
                                                      const char *dialect,
                                                      const char *identifier);

static void mex_free_metadata_section (struct mex_section *section);

static void mex_remove_metadata (struct mex_plugin_data *data);


/* implementation */

INLINE struct mex_plugin_data *
mex_get_plugindata (struct soap *soap)
{
  return (struct mex_plugin_data *) soap_lookup_plugin (soap, MEX_PLUGIN_ID);
}


static void
mex_plugin_delete (struct soap *soap, struct soap_plugin *p)
{
  struct mex_plugin_data *data = p->data;

  WS4D_UNUSED_PARAM (soap);

  /* destroy plugin data */
  mex_remove_metadata (data);
  mex_remove_metadata_types (data);
  ws4d_alloclist_done (&data->alist);
  ws4d_mutex_destroy (&data->lock);

  /* free plugin data structure */
  ws4d_free (p->data);

  /* reset id */
  p->id = NULL;
}


static int
mex_plugin_init (struct soap *soap, struct soap_plugin *p, void *arg)
{
  struct mex_plugin_data *data;

  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (arg);

  /* allocate memory for plugin data */
  data = (void *) ws4d_malloc (sizeof (struct mex_plugin_data));
  ws4d_fail (data == NULL, SOAP_EOM);

  /* initialize plugin structure */
  p->id = mex_plugin_id;
  p->data = data;
  p->fdelete = mex_plugin_delete;

  /* initialize plugin data */
  WS4D_INIT_LIST (&data->sections);
  WS4D_INIT_LIST (&data->types);
  WS4D_ALLOCLIST_INIT (&data->alist);
  ws4d_mutex_init (&data->lock);

  return SOAP_OK;
}


int
mex_init (struct soap *soap)
{
  int err;
  struct Namespace *temp_namespaces = NULL;
  struct mex_plugin_data *data;

  /* test parameters */
  ws4d_assert (soap, SOAP_ERR);

  /* register ws-addressing plugin */
  err = wsa_register_handle (soap);
  ws4d_fail ((err != SOAP_OK) && (err != SOAP_PLUGIN_DUPLICATE), err);

  /* register soap handle as discovery multicast handle */
  err = soap_register_plugin_singleton (soap, mex_plugin_init, mex_plugin_id,
                                        (void **) &data);
  ws4d_fail (data == NULL, err);

  /* only if plugin is not already registered */
  if (err != SOAP_PLUGIN_DUPLICATE)
    {

      /* check if namespace table is initialized */
      if (soap->namespaces == NULL)
        {
          /* initialize namespace table */
          soap_set_namespaces (soap, wst_namespaces);
        }
      else
        {
          /* extend namespace table */
          temp_namespaces =
            soap_extend_namespaces ((struct Namespace *) soap->namespaces,
                                    wst_namespaces, &data->alist);
          if (temp_namespaces)
            {
              soap_set_namespaces (soap, temp_namespaces);
            }
        }

    }

  return err;
}


/******************************************************************************
 * metadata section type management                                           *
 ******************************************************************************/

int
mex_register_metadata_sectiontype (struct soap *soap, const char *dialect,
                                   const char *tag,
                                   const struct mex_section_type_serializers
                                   *serializers)
{
  struct mex_plugin_data *data = mex_get_plugindata (soap);
  struct mex_section_type *type;

  /* test parameters */
  ws4d_assert (data && dialect && tag && serializers, SOAP_ERR);

  /* check if type is already registered */
  type = _mex_get_metadata_section_type (data, dialect);
  ws4d_fail (type != NULL, SOAP_ERR);

  /* allocate new type structure */
  type = ws4d_malloc_alist (sizeof (struct mex_section_type), &data->alist);
  ws4d_fail (type == NULL, SOAP_EOM);

  /* assign structure content */
  type->dialect = dialect;
  type->tag = tag;
  type->serializers = serializers;

  /* add type to list */
  ws4d_list_add_tail (&type->list, &data->types);

  return SOAP_OK;
}


static struct mex_section_type *
_mex_get_metadata_section_type (struct mex_plugin_data *data,
                                const char *dialect)
{
  register struct mex_section_type *result, *next;

  /* test parameters */
  ws4d_assert (data && dialect, NULL);

  /* search for entry with matching dialect */
  ws4d_list_foreach (result, next, &data->types,
                     struct mex_section_type, list)
  {
    if (result->dialect
        && ((dialect == result->dialect)
            || !strcmp (dialect, result->dialect)))
      {
        return result;
      }
  }

  /* nothing found */
  return NULL;
}


static struct mex_section_type *
mex_get_metadata_section_type (struct soap *soap, const char *dialect)
{
  struct mex_plugin_data *data = mex_get_plugindata (soap);

  return _mex_get_metadata_section_type (data, dialect);
}


static void
mex_free_metadata_section_type (struct mex_section_type *type)
{
  /* test parameters */
  ws4d_assert (type,);

  /* remove type structure from list */
  ws4d_list_del (&type->list);

  /* free type structure */
  ws4d_free_alist (type);

  return;
}


int
mex_remove_metadata_section_type (struct soap *soap, const char *dialect)
{
  struct mex_plugin_data *data = mex_get_plugindata (soap);
  struct mex_section_type *type;

  /* test parameters */
  ws4d_assert (data && dialect, SOAP_ERR);

  /* find type */
  type = _mex_get_metadata_section_type (data, dialect);
  ws4d_fail (type == NULL, SOAP_ERR);

  /* free type */
  mex_free_metadata_section_type (type);

  return SOAP_OK;
}


static void
mex_remove_metadata_types (struct mex_plugin_data *data)
{
  register struct mex_section_type *entry, *next;

  /* test parameters */
  ws4d_assert (data,);

  /* free all section types in list */
  ws4d_list_foreach (entry, next, &data->types, struct mex_section_type, list)
  {
    mex_free_metadata_section_type (entry);
  }

  return;
}


/******************************************************************************
 * metadata section management                                                *
 ******************************************************************************/

int
mex_register_metadata_section (struct soap *soap, const char *endpoint,
                               const char *dialect, const char *identifier,
                               void *content)
{
  struct mex_plugin_data *data = mex_get_plugindata (soap);
  struct mex_section *section;
  struct mex_section_type *type;

  /* test parameters */
  ws4d_assert (data && endpoint && dialect && content, SOAP_ERR);

  /* check if type is already registered */
  type = _mex_get_metadata_section_type (data, dialect);
  ws4d_fail (type == NULL, SOAP_ERR);

  /* allocate section */
  section = ws4d_malloc_alist (sizeof (struct mex_section), &data->alist);
  ws4d_fail (section == NULL, SOAP_EOM);

  /* assign section fields */
  section->endpoint = ws4d_strdup (endpoint, &data->alist);
  section->dialect = dialect;
  if (identifier != NULL)
    {
      section->identifier = ws4d_strdup (identifier, &data->alist);
    }
  section->content = content;

  /* add section to list */
  ws4d_list_add_tail (&section->list, &data->sections);

  return SOAP_OK;
}


static struct mex_section *
_mex_get_metadata_section (struct mex_plugin_data *data,
                           const char *endpoint, const char *dialect,
                           const char *identifier)
{
  register struct mex_section *result, *next;

  /* test parameters */
  ws4d_assert (data && endpoint && dialect, NULL);

  /* search for entry with metching endpoint and dialect */
  ws4d_list_foreach (result, next, &data->sections, struct mex_section, list)
  {
    if (result->endpoint && result->dialect
        && ((endpoint == result->endpoint)
            || !strcmp (endpoint, result->endpoint))
        && ((dialect == result->dialect)
            || !strcmp (dialect, result->dialect)))
      {
        /* match identifier, if specified */
        if (identifier != NULL)
          {
            if (result->identifier
                && !strcmp (identifier, result->identifier))
              {
                return result;
              }
          }
        else
          {
            return result;
          }
      }
  }

  /* nothing found */
  return NULL;
}


void *
mex_get_metadata_section (struct soap *soap, const char *endpoint,
                          const char *dialect, const char *identifier)
{
  struct mex_plugin_data *data = mex_get_plugindata (soap);
  struct mex_section *result;

  /* test parameters */
  ws4d_assert (data, NULL);

  result = _mex_get_metadata_section (data, endpoint, dialect, identifier);

  return result != NULL ? result->content : NULL;
}


static void
mex_free_metadata_section (struct mex_section *section)
{
  /* test parameters */
  ws4d_assert (section,);

  /* free structure content */
  if (section->endpoint)
    {
      ws4d_free_alist ((void *) section->endpoint);
    }
  if (section->identifier)
    {
      ws4d_free_alist ((void *) section->identifier);
    }

  /* remove structure from list */
  ws4d_list_del (&section->list);

  /* free structure */
  ws4d_free_alist (section);

  return;
}


int
mex_remove_metadata_section (struct soap *soap, const char *endpoint,
                             const char *dialect, const char *identifier)
{
  struct mex_plugin_data *data = mex_get_plugindata (soap);
  struct mex_section *section;

  /* test parameters */
  ws4d_assert (data && endpoint && dialect, SOAP_ERR);

  /* find section */
  section = _mex_get_metadata_section (data, endpoint, dialect, identifier);
  ws4d_fail (section == NULL, SOAP_ERR);

  mex_free_metadata_section (section);

  return SOAP_OK;
}


static void
mex_remove_metadata (struct mex_plugin_data *data)
{
  register struct mex_section *entry, *next;

  /* test parameters */
  ws4d_assert (data,);

  /* free each section in list */
  ws4d_list_foreach (entry, next, &data->sections, struct mex_section, list)
  {
    mex_free_metadata_section (entry);
  }

  return;
}


/******************************************************************************
 * server side processing functions                                           *
 ******************************************************************************/

#ifdef MEX_SERVER
static int
mex_process (struct soap *soap, const char *endpoint, const char *dialect,
             const char *identifier, struct _wsm__Metadata *GetResponseMsg)
{
  struct mex_plugin_data *data = mex_get_plugindata (soap);
  register struct mex_section *entry, *next;
  int i, count = 0;

  /* test parameters */
  soap_assert (soap, data && endpoint, SOAP_ERR);

  /* TODO: implement dialect and identifier filter */
  WS4D_UNUSED_PARAM (dialect);
  WS4D_UNUSED_PARAM (identifier);

  /* count number of matching sections */
  ws4d_list_foreach (entry, next, &data->sections, struct mex_section, list)
  {
    if (entry->endpoint
        && ((endpoint == entry->endpoint)
            || !strcmp (endpoint, entry->endpoint)))
      {
        count++;
      }
  }

  /* allocate metadata section structure */
  GetResponseMsg->wsm__MetadataSection =
    soap_malloc (soap, count * sizeof (struct _wsm__MetadataSection));
  ws4d_fail (GetResponseMsg->wsm__MetadataSection == NULL, SOAP_EOM);

  GetResponseMsg->__sizeMetadataSection = count;

  /* assign matching sections */
  i = 0;
  ws4d_list_foreach (entry, next, &data->sections, struct mex_section, list)
  {
    if (entry->endpoint
        && ((endpoint == entry->endpoint)
            || !strcmp (endpoint, entry->endpoint)))
      {
        GetResponseMsg->wsm__MetadataSection[i].Dialect =
          (char *) entry->dialect;
        GetResponseMsg->wsm__MetadataSection[i].Identifier =
          (char *) entry->identifier;
        GetResponseMsg->wsm__MetadataSection[i]._content = entry->content;

        i++;
      }
  }

  return SOAP_OK;
}

int
mex_process_TransferGet (struct soap *soap, const char *endpoint,
                         struct _wsm__Metadata *GetResponseMsg)
{
  int err = mex_process (soap, endpoint, NULL, NULL, GetResponseMsg);

  wsa_header_gen_response (soap, NULL, (char *) wsa_anonymousURI,
                           "http://schemas.xmlsoap.org/ws/2004/09/transfer/GetResponse",
                           wsa_header_get_MessageId (soap),
                           sizeof (struct SOAP_ENV__Header));

  return err;
}

#ifdef DEVPROF_2009_01
int
mex_process_GetMetadata (struct soap *soap, const char *endpoint,
                         const char *dialect, const char *identifier,
                         struct _wsm__Metadata *GetResponseMsg)
{
  int err = mex_process (soap, endpoint, dialect, identifier, GetResponseMsg);

  wsa_header_gen_response (soap, NULL, (char *) wsa_anonymousURI,
                           "http://schemas.xmlsoap.org/ws/2004/09/mex/GetMetadata/Response",
                           wsa_header_get_MessageId (soap),
                           sizeof (struct SOAP_ENV__Header));

  return err;
}
#endif
#endif


/******************************************************************************
 * client side functions                                                      *
 ******************************************************************************/

#ifdef MEX_CLIENT
#ifdef DEVPROF_2009_01
int
mex_GetMetadata (struct soap *soap, char *MsgId, const char *Address,
                 const char *To, const char *Dialect, const char *Identifier,
                 struct _wsm__Metadata *metadata, ws4d_time timeout)
{
  /* test parameters */
  soap_assert (soap, soap && Address && metadata, SOAP_ERR);

  /* initialize metadata structure */
  soap_default__wsm__Metadata (soap, metadata);

  /* set timeout */
  soap->recv_timeout = -1000 * timeout;

  /* Prepare SOAP Header */
  wsa_header_gen_request (soap, MsgId, To,
                          "http://schemas.xmlsoap.org/ws/2004/09/mex/GetMetadata/Request",
                          NULL, NULL, sizeof (struct SOAP_ENV__Header));

  /* Make request-response call */
  return soap_call_wsm__GetMetadata (soap, Address, NULL, (char *) Dialect,
                                     (char *) Identifier, metadata);
}
#endif

int
mex_TransferGet (struct soap *soap, char *MsgId, const char *Address,
                 const char *To, struct _wsm__Metadata *metadata,
                 ws4d_time timeout)
{
  /* test parameters */
  soap_assert (soap, soap && Address && metadata, SOAP_ERR);

  /* initialize metadata structure */
  soap_default__wsm__Metadata (soap, metadata);

  /* set timeout */
  soap->recv_timeout = -1000 * timeout;

  /* Prepare SOAP Header */
  wsa_header_gen_request (soap, MsgId, To,
                          "http://schemas.xmlsoap.org/ws/2004/09/transfer/Get",
                          NULL, NULL, sizeof (struct SOAP_ENV__Header));

  /* Make request-response call */
  return soap_call___wst__Get (soap, Address, NULL, NULL, metadata);
}
#endif


/******************************************************************************
 * mex custom serializer functions                                            *
 ******************************************************************************/

static void
mex_serialize_listeral (struct soap *soap, const void *a)
{
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (a);
}

static int
mex_out_literal (struct soap *soap, const char *tag, int id, const void *a,
                 const char *type)
{
  WS4D_UNUSED_PARAM (id);
  WS4D_UNUSED_PARAM (type);
  return soap_outliteral (soap, tag, a, NULL);
}

static void *
mex_in_literal (struct soap *soap, const char *tag, void *a, const char *type)
{
  WS4D_UNUSED_PARAM (type);
  return soap_inliteral (soap, tag, a);
}

const struct mex_section_type_serializers mex_literal_serializers = {
  MEX_CAST_FSERIALIZE (mex_serialize_listeral),
  MEX_CAST_FOUT (mex_out_literal),
  MEX_CAST_FIN (mex_in_literal)
};

void
soap_default__wsm__MetadataSection (struct soap *soap,
                                    struct _wsm__MetadataSection *a)
{
  soap_default_string (soap, &a->Dialect);
  soap_default_string (soap, &a->Identifier);
  a->_content = NULL;
}


void
soap_serialize__wsm__MetadataSection (struct soap *soap,
                                      const struct _wsm__MetadataSection *a)
{
  struct mex_section_type *section_type =
    mex_get_metadata_section_type (soap, a->Dialect);
  if (section_type != NULL)
    {
      section_type->serializers->serialize (soap, a->_content);
    }
}


int
soap_out__wsm__MetadataSection (struct soap *soap, const char *tag, int id,
                                const struct _wsm__MetadataSection *a,
                                const char *type)
{
  struct mex_section_type *section_type;

  if (a->Dialect)
    {
      soap_set_attr (soap, "Dialect", a->Dialect);
    }

  if (a->Identifier)
    {
      soap_set_attr (soap, "Identifier", a->Identifier);
    }

  if (soap_element_begin_out
      (soap, tag,
       soap_embedded_id (soap, id, a, SOAP_TYPE__wsm__MetadataSection), type))
    {
      return soap->error;
    }

  section_type = mex_get_metadata_section_type (soap, a->Dialect);
  if (section_type != NULL)
    {
      if (section_type->
          serializers->out (soap, section_type->tag, -1, a->_content, ""))
        {
          return soap->error;
        }
    }

  return soap_element_end_out (soap, tag);
}


int
soap_put__wsm__MetadataSection (struct soap *soap,
                                const struct _wsm__MetadataSection *a,
                                const char *tag, const char *type)
{
  register int id = soap_embed (soap, (void *) a, NULL, 0, tag,
                                SOAP_TYPE__wsm__MetadataSection);

  if (soap_out__wsm__MetadataSection (soap, tag, id, a, type))
    {
      return soap->error;
    }

  return soap_putindependent (soap);
}


struct _wsm__MetadataSection *
soap_in__wsm__MetadataSection (struct soap *soap, const char *tag,
                               struct _wsm__MetadataSection *a,
                               const char *type)
{
  size_t soap_flag_content = 1;

  if (soap_element_begin_in (soap, tag, 0, type))
    {
      return NULL;
    }

  a =
    (struct _wsm__MetadataSection *) soap_id_enter (soap, soap->id, a,
                                                    SOAP_TYPE__wsm__MetadataSection,
                                                    sizeof (struct
                                                            _wsm__MetadataSection),
                                                    0, NULL, NULL, NULL);
  if (!a)
    {
      return NULL;
    }

  soap_default__wsm__MetadataSection (soap, a);

  if (soap_s2string (soap, soap_attr_value (soap, "Dialect", 1), &a->Dialect))
    {
      return NULL;
    }

  if (soap_s2string
      (soap, soap_attr_value (soap, "Identifier", 0), &a->Identifier))
    {
      return NULL;
    }

  if (soap->body && !*soap->href)
    {
      for (;;)
        {
          soap->error = SOAP_TAG_MISMATCH;
          if (soap_flag_content && soap->error == SOAP_TAG_MISMATCH)
            {
              struct mex_section_type *section_type =
                mex_get_metadata_section_type (soap, a->Dialect);

              if (section_type != NULL)
                {
                  if ((a->_content =
                       section_type->serializers->in (soap, section_type->tag,
                                                      a->_content, "")))
                    {
                      soap_flag_content--;
                      continue;
                    }
                }
            }

          if (soap->error == SOAP_TAG_MISMATCH)
            {
              soap->error = soap_ignore_element (soap);
            }

          if (soap->error == SOAP_NO_TAG)
            {
              break;
            }

          if (soap->error)
            {
              return NULL;
            }
        }
      if (soap_element_end_in (soap, tag))
        {
          return NULL;
        }
    }
  else
    {
      a =
        (struct _wsm__MetadataSection *) soap_id_forward (soap, soap->href,
                                                          (void *) a, 0,
                                                          SOAP_TYPE__wsm__MetadataSection,
                                                          0,
                                                          sizeof (struct
                                                                  _wsm__MetadataSection),
                                                          0, NULL);
      if (soap->body && soap_element_end_in (soap, tag))
        {
          return NULL;
        }
    }

  return a;
}


struct _wsm__MetadataSection *
soap_get__wsm__MetadataSection (struct soap *soap,
                                struct _wsm__MetadataSection *p,
                                const char *tag, const char *type)
{
  if ((p = soap_in__wsm__MetadataSection (soap, tag, p, type)))
    {
      if (soap_getindependent (soap))
        {
          return NULL;
        }
    }
  return p;
}
