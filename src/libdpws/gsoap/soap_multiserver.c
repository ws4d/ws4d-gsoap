/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *
 *  Created on: 15.04.2011
 *      Author: elmex
 */

#include "stdsoap2.h"
#include "soap_misc.h"

#ifndef WITH_NOIO

/* add sockets to an fdset */
static int
_fdset_add_socket (fd_set * fd, SOAP_SOCKET sock, SOAP_SOCKET * bigfd)
{
  int err = SOAP_ERR;
  if (soap_valid_socket (sock))
    {
      FD_SET ((SOAP_SOCKET) sock, fd);
      if ((SOAP_SOCKET) sock > *bigfd)
        {
          *bigfd = sock;
        }
      err = SOAP_OK;
    }

  return err;
}

struct soap *
soap_maccept (ws4d_time timeout, int count, struct soap **soap_handles)
{
  struct timeval timeout_t;
  register int i, sel_res = 0;
  SOAP_SOCKET bigfd = SOAP_INVALID_SOCKET, sock = SOAP_INVALID_SOCKET;
  fd_set fd;
  struct soap *result = NULL;

  FD_ZERO (&fd);

  if (!soap_handles)
    return result;

  /* register sockets in fdset for select */
  for (i = 0; i < count; i++)
    {
      if (soap_ismsocket (soap_handles[i]))
        {
          /* register sockets in fdset, if this is an msocket */
          void *iterator = NULL;
          do
            {
              sock = soap_msocket_iterate (soap_handles[i], &iterator);
              if (_fdset_add_socket (&fd, sock, &bigfd) != SOAP_OK)
                {
                  printf ("register soap handle number %d first\n", i);
                  goto exit;
                }
            }
          while (sock == SOAP_INVALID_SOCKET);
        }
      else
        {
          /* register socket of soap handle in fdset */
          if (_fdset_add_socket (&fd, soap_handles[i]->master, &bigfd)
              != SOAP_OK)
            {
              printf ("register soap handle number %d first\n", i);
              goto exit;
            }
        }
    }

  /* set timeout */
  if (timeout != DPWS_SYNC)
    {
      timeout_t.tv_sec = timeout / 1000;
      timeout_t.tv_usec = (timeout % 1000) * 1000;
    }

  /* run select */
  sel_res = select (bigfd + 1, &fd, NULL, NULL, &timeout_t);
  if (sel_res > 0)
    {
      /* check which socket is ready for reading */
      for (i = 0; i < count; i++)
        {
          if (soap_ismsocket (soap_handles[i]))
            {
              /* check if socket is in fdset, if this is an msocket */
              void *iterator = NULL;
              do
                {
                  sock = soap_msocket_iterate (soap_handles[i], &iterator);
                  if (FD_ISSET (sock, &fd))
                    {
                      soap_mswitch (soap_handles[i], sock);
                      result = soap_handles[i];
                      goto exit;
                    }
                }
              while (sock == SOAP_INVALID_SOCKET);
            }
          else
            {
              /* check if socket is in fdset for a regular soap handle */
              if (FD_ISSET (soap_handles[i]->master, &fd))
                {
                  result = soap_handles[i];
                  goto exit;
                }
            }
        }
    }
  /* TODO: should handle failures of select as well */

exit:

  if (result)
    {
      soap_accept (result);
    }

  return result;
}
#endif

int
soap_mserve (struct soap *soap, int count, int
             (*serve_requests[])(struct soap * soap))
{
  unsigned int k = soap->max_keep_alive;
  int i = 0, err = SOAP_ERR;

  if (!soap || !serve_requests || count < 1)
    {
      return SOAP_ERR;
    }

  do
    {

      soap_begin (soap);

      if (!--k)
        {
          soap->keep_alive = 0;
        }

      if (soap_begin_recv (soap))
        {
          if (soap->error < SOAP_STOP)
            {
              return soap_send_fault (soap);
            }
          soap_closesock (soap);

          continue;
        }

      if (soap_envelope_begin_in (soap) || soap_recv_header (soap)
          || soap_body_begin_in (soap))
        {
          if (soap->mode & SOAP_IO_UDP)
            {
              return soap->error;
            }
          else
            {
              return soap_send_fault (soap);
            }
        }

      if (!soap->action)
        {
          printf
            ("Warning: Incoming message on handle 0x%p has no soap action!\n",
             soap);
        }

      i = 0;
      err = SOAP_NO_METHOD;
      while ((i < count) && (err == SOAP_NO_METHOD))
        {
          err = serve_requests[i] (soap);
          i++;
        }

      if ((err != SOAP_OK) && (err != SOAP_STOP))
        {
          return soap_send_fault (soap);
        }

      if (soap->fserveloop && soap->fserveloop (soap))
        {
          return soap_send_fault (soap);
        }

    }
  while (soap->keep_alive);

  return SOAP_OK;
}
