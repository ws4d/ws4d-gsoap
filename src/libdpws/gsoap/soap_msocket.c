/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *
 *  Created on: 14.04.2011
 *      Author: elmex
 */

#include "stdsoap2.h"
#include "soap_misc.h"

#define SOAP_MSOCKET_PLUGIN_ID "gSOAP multi socket handle plugin 0.1"
const char *soap_msocket_plugin_id = SOAP_MSOCKET_PLUGIN_ID;

static struct soap_msocket_plugin_data *soap_msocket_get_plugindata (struct
                                                                     soap
                                                                     *soap);

static int soap_msocket_plugin_init (struct soap *soap, struct soap_plugin *p,
                                     void *arg);

static void
soap_msocket_plugin_delete (struct soap *soap, struct soap_plugin *p);

static int soap_msocket_register_handle (struct soap *soap);

/* entry of socket list */
struct soap_msocket_node
{
  struct ws4d_list_node list;
  SOAP_SOCKET socket;
};

/* msocket plugin data structure */
struct soap_msocket_plugin_data
{
  struct ws4d_list_node sockets;
  int count;

  struct ws4d_abs_allocator alist;
#ifdef WITH_MUTEXES
    WS4D_MUTEX (lock);
#endif
};

static struct soap_msocket_plugin_data *
soap_msocket_get_plugindata (struct soap *soap)
{
  return (struct soap_msocket_plugin_data *) soap_lookup_plugin (soap,
                                                                 soap_msocket_plugin_id);
}

static void
soap_msocket_plugin_delete (struct soap *soap, struct soap_plugin *p)
{
  struct soap_msocket_plugin_data *data =
    (struct soap_msocket_plugin_data *) p->data;

  WS4D_UNUSED_PARAM (soap);

  /* destroy plugin data */
  ws4d_alloclist_done (&data->alist);
  ws4d_mutex_destroy (&data->lock);

  /* free plugin data structure */
  ws4d_free (p->data);

  /* reset id */
  p->id = NULL;
}

static int
soap_msocket_plugin_init (struct soap *soap, struct soap_plugin *p, void *arg)
{
  struct soap_msocket_plugin_data *data;

  WS4D_UNUSED_PARAM (arg);
  WS4D_UNUSED_PARAM (soap);

  /* allocate memory for plugin data */
  data = (void *) ws4d_malloc (sizeof (struct soap_msocket_plugin_data));
  ws4d_fail (data == NULL, SOAP_EOM);

  /* initialize plugin structure */
  p->id = soap_msocket_plugin_id;
  p->data = data;
  p->fdelete = soap_msocket_plugin_delete;

  /* initialize plugin data */
  WS4D_INIT_LIST (&data->sockets);
  data->count = 0;
  WS4D_ALLOCLIST_INIT (&data->alist);
  ws4d_mutex_init (&data->lock);

  return SOAP_OK;
}

static int
soap_msocket_register_handle (struct soap *soap)
{
  struct soap_msocket_plugin_data *data = NULL;

  /* register soap handle as multisocket handle */
  return soap_register_plugin_singleton (soap, soap_msocket_plugin_init,
                                         soap_msocket_plugin_id,
                                         (void **) &data);
}

static int
soap_msocket_addsocket (struct soap *soap, SOAP_SOCKET sock)
{
  struct soap_msocket_plugin_data *data = soap_msocket_get_plugindata (soap);
  register struct soap_msocket_node *node, *next;
  int err = SOAP_ERR;

  /* test parameters */
  ws4d_assert (soap && data, SOAP_ERR);

  ws4d_mutex_lock (&data->lock);
  ws4d_list_foreach (node, next, &data->sockets, struct soap_msocket_node,
                     list)
  {
    if (node->socket == sock)
      {
        ws4d_mutex_unlock (&data->lock);

        return SOAP_OK;
      }
  }

  node = ws4d_malloc_alist (sizeof (struct soap_msocket_node), &data->alist);
  if (node == NULL)
    {
      err = SOAP_EOM;
    }
  else
    {
      node->socket = sock;
      ws4d_list_add_tail (&node->list, &data->sockets);
      data->count += 1;
      err = SOAP_OK;
    }

  ws4d_mutex_unlock (&data->lock);

  return err;
}

SOAP_SOCKET
soap_mbind (struct soap * soap, const char *host, int port, int backlog)
{
  struct soap_msocket_plugin_data *data = NULL;
  SOAP_SOCKET sock;
  int err;

  /* test parameters */
  ws4d_fail (!soap, SOAP_ERR);

  /* test if plugin is already registerd - handle was already initialized */
  data = soap_msocket_get_plugindata (soap);
  if (!data)
    {
      err = soap_msocket_register_handle (soap);
      ws4d_assert (err == SOAP_OK, SOAP_INVALID_SOCKET);

      data = soap_msocket_get_plugindata (soap);
      ws4d_assert (data != NULL, SOAP_INVALID_SOCKET);
    }

  /* check if current socket is already in socket list */
  if (soap->master != SOAP_INVALID_SOCKET)
    {
      soap_msocket_addsocket (soap, soap->master);
      soap->master = SOAP_INVALID_SOCKET;
    }

  /* create new socket with soap_bind */
  sock = soap_bind (soap, host, port, backlog);
  ws4d_fail (sock == SOAP_INVALID_SOCKET, SOAP_INVALID_SOCKET);

  /* save new socket in socketlist */
  soap_msocket_addsocket (soap, soap->master);

  return sock;
}

int
soap_mswitch (struct soap *soap, SOAP_SOCKET sock)
{
  struct soap_msocket_plugin_data *data = soap_msocket_get_plugindata (soap);
  register struct soap_msocket_node *node, *next;

  /* test parameters */
  ws4d_assert (soap, SOAP_ERR);

  /* test if msocket plugin is already registered */
  ws4d_fail (data == NULL, SOAP_ERR);

  /* search for socket in socket list */
  ws4d_mutex_lock (&data->lock);
  ws4d_list_foreach (node, next, &data->sockets, struct soap_msocket_node,
                     list)
  {
    if (node->socket == sock)
      {
        /* found the socket */
        ws4d_mutex_unlock (&data->lock);
        soap->master = sock;
        return SOAP_OK;
      }
  }
  ws4d_mutex_unlock (&data->lock);

  /* didn't find the socket */
  return SOAP_ERR;
}

int
soap_ismsocket (struct soap *soap)
{
  struct soap_msocket_plugin_data *data = soap_msocket_get_plugindata (soap);

  return data != NULL;
}

SOAP_SOCKET
soap_msocket_iterate (struct soap * soap, void **iterator)
{
  struct soap_msocket_plugin_data *data = soap_msocket_get_plugindata (soap);
  register struct soap_msocket_node *node;

  /* test parameters */
  ws4d_assert (soap && data && iterator, SOAP_INVALID_SOCKET);

  if (*iterator == NULL)
    {
      /* if *iterator is NULL, we start to got through the list */
      node = ws4d_list_getfirst (&data->sockets,
                                 struct soap_msocket_node, list);
      *iterator = node;
    }
  else
    {
      /* get next node */
      node = *iterator;
      node = ws4d_list_getnext (node, &data->sockets,
                                struct soap_msocket_node, list);
    }

  /* return socket related to node */
  return node == NULL ? SOAP_INVALID_SOCKET : node->socket;
}

int
soap_msocket_getsocketcount (struct soap *soap)
{
  struct soap_msocket_plugin_data *data = soap_msocket_get_plugindata (soap);

  /* test parameters */
  ws4d_assert (soap && data, -1);

  return data->count;
}
