/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "stdsoap2.h"

#include "dpwsH.h"
#include <stdarg.h>
#include "ws4d_misc.h"
#include "soap_misc.h"

#include "ws4d_abstract_eprlist.h"
#include "ws4d_eprllist.h"

#include "stddpws.h"
#include "ws-addressing.h"

#include "dpws_constants.h"

#ifdef DPWS_DEVICE
#ifndef WSD_TARGET
#define WSD_TARGET
#endif
#ifndef WSE_SERVER
#define WSE_SERVER
#endif
#endif

#ifdef DPWS_CLIENT
#ifndef WSD_CLIENT
#define WSD_CLIENT
#endif
#ifndef WSE_CLIENT
#define WSE_CLIENT
#endif
#endif

#include "ws-discovery.h"
#include "ws-metadataexchange.h"
#include "ws-eventing.h"

/* forward declarations */

int __wsd__Hello (struct soap *soap, struct wsd__HelloType *wsd__Hello);

int __wsd__Bye (struct soap *soap, struct wsd__ByeType *wsd__Bye);

int __wsd__Probe (struct soap *soap, struct wsd__ProbeType *wsd__Probe);

int
__wsd__ProbeMatches (struct soap *soap,
                     struct wsd__ProbeMatchesType *wsd__ProbeMatches);

int __wsd__Resolve (struct soap *soap, struct wsd__ResolveType *wsd__Resolve);

int
__wsd__ResolveMatches (struct soap *soap,
                       struct wsd__ResolveMatchesType *wsd__ResolveMatches);

int
wsm__GetMetadata (struct soap *soap, char *wsm__Dialect,
                  char *wsm__Identifier,
                  struct _wsm__Metadata *_GetMetadataResponseMsg);

int
__wst__Get (struct soap *soap, void *emptyMessage,
            struct _wsm__Metadata *_GetResponseMsg);

int
__wse__Subscribe (struct soap *soap, struct _wse__Subscribe *__req,
                  struct _wse__SubscribeResponse *_res);

int
__wse__Renew (struct soap *soap, struct _wse__Renew *__req,
              struct _wse__RenewResponse *_res);

int
__wse__GetStatus (struct soap *soap, struct _wse__GetStatus *__req,
                  struct _wse__GetStatusResponse *_res);

struct __wse__UnsubscribeResponse
{
  void *noresp;
};

int
__wse__Unsubscribe (struct soap *soap,
                    struct _wse__Unsubscribe *wse__Unsubscribe,
                    struct __wse__UnsubscribeResponse *_param_1);

int
__wse__SubscriptionEnd (struct soap *soap,
                        struct _wse__SubscriptionEnd *_req);


/* implementation */

/* dpws specific metadata exchange serializers */

const struct mex_section_type_serializers thisDevice_serializers = {
  MEX_CAST_FSERIALIZE (soap_serialize_wsdp__ThisDeviceType),
  MEX_CAST_FOUT (soap_out_wsdp__ThisDeviceType),
  MEX_CAST_FIN (soap_in_wsdp__ThisDeviceType)
};

const struct mex_section_type_serializers thisModel_serializers = {
  MEX_CAST_FSERIALIZE (soap_serialize_wsdp__ThisModelType),
  MEX_CAST_FOUT (soap_out_wsdp__ThisModelType),
  MEX_CAST_FIN (soap_in_wsdp__ThisModelType)
};

const struct mex_section_type_serializers relationship_serializers = {
  MEX_CAST_FSERIALIZE (soap_serialize__wsdp__Relationship),
  MEX_CAST_FOUT (soap_out__wsdp__Relationship),
  MEX_CAST_FIN (soap_in__wsdp__Relationship)
};

/* message relate functions */

char *
dpws_header_gen_MessageId (char *buffer, size_t size)
{
  char uuidstr[WS4D_UUID_SIZE];

  /* test parameters */
  ws4d_assert (buffer && (size >= DPWS_MSGID_SIZE), NULL);

  memset (buffer, 0, size);
  memset (uuidstr, 0, WS4D_UUID_SIZE);

  ws4d_uuid_generate_random (uuidstr);
  ws4d_uuid_generate_schema (buffer, uuidstr);

  return buffer;
}

int
dpws_header_gen_oneway (struct soap *soap, const char *MessageId,
                        const char *To, const char *Action,
                        const char *FaultTo, size_t size)
{
  char MsgId[DPWS_MSGID_SIZE];

  /* test parameters */
  soap_assert (soap, soap, SOAP_ERR);

  if (!MessageId)
    {
      dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
      MessageId = soap_strdup (soap, (char *) MsgId);
    }

  return wsa_header_gen_oneway (soap, MessageId, To, Action, FaultTo, size);
}

int
dpws_header_gen_request (struct soap *soap, const char *MessageId,
                         const char *To, const char *Action,
                         const char *FaultTo, const char *ReplyTo,
                         size_t size)
{
  char MsgId[DPWS_MSGID_SIZE];

  /* test parameters */
  soap_assert (soap, soap, SOAP_ERR);

  if (!MessageId)
    {
      dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
      MessageId = soap_strdup (soap, (char *) MsgId);
      if (!MessageId)
        {
          return soap->error;
        }
    }

  return wsa_header_gen_request (soap, MessageId, To, Action, FaultTo,
                                 ReplyTo, size);
}

int
dpws_header_gen_response (struct soap *soap, const char *MessageId,
                          const char *To, const char *Action,
                          const char *RelatesTo, size_t size)
{
  return wsa_header_gen_response (soap, MessageId, To, Action, RelatesTo,
                                  size);
}

int
dpws_sender_fault (struct soap *soap, const char *MessageId, const char *To,
                   const char *Action, const char *RelatesTo, size_t size,
                   const char *faultstring, const char *faultdetail)
{
  int ret = 0;

  if (!Action)
    {
      Action = DPWS_FAULT_ACTION;
    }

  ret = wsa_header_gen_fault (soap, MessageId, To, Action, RelatesTo, size);
  if (ret != SOAP_OK)
    return ret;

  return soap_sender_fault (soap, faultstring, faultdetail);
}

int
dpws_receiver_fault (struct soap *soap, const char *MessageId,
                     const char *To, const char *Action,
                     const char *RelatesTo, size_t size,
                     const char *faultstring, const char *faultdetail)
{
  int ret = 0;
  char MsgId[DPWS_MSGID_SIZE];

  if (!MessageId)
    {
      dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
      MessageId = soap_strdup (soap, (char *) MsgId);
    }

  ret = wsa_header_gen_fault (soap, MessageId, To, Action, RelatesTo, size);
  if (ret != SOAP_OK)
    return ret;

  return soap_sender_fault (soap, faultstring, faultdetail);
}

/* processing related functions */

struct soap *
dpws_maccept (struct dpws_s *dpws, ws4d_time timeout, int count,
              struct soap **soap_handles)
{
  register int i, found_dis_mca = 0;
  struct soap *result;
#ifdef DPWS_DEVICE
  int found_hosting = 0;
#endif
#ifdef DPWS_CLIENT
  int found_dis_uni = 0;
#endif

  /* test parameters */
  ws4d_assert (dpws && (count > 0) && soap_handles, NULL);

  for (i = 0; i < count; i++)
    {
      if (soap_handles[i])
        {
          if (soap_handles[i] == dpws->dis_mca)
            found_dis_mca = 1;
#ifdef DPWS_DEVICE
          if (soap_handles[i] == dpws->hosting_handle)
            found_hosting = 1;
#endif
#ifdef DPWS_CLIENT
          if (soap_handles[i] == dpws->dis_uni)
            found_dis_uni = 1;
#endif
        }
    }

  if (dpws->dis_mca && !found_dis_mca)
    {
      soap_handles[count] = dpws->dis_mca;
      count++;
    }
#ifdef DPWS_DEVICE
  if (dpws->hosting_handle && !found_hosting)
    {
      soap_handles[count] = dpws->hosting_handle;
      count++;
    }
#endif
#ifdef DPWS_CLIENT
  if (dpws->dis_uni && !found_dis_uni)
    {
      soap_handles[count] = dpws->dis_uni;
      count++;
    }
#endif

  result = soap_maccept (timeout, count, soap_handles);

  return result;
}

extern int target_serve_request (struct soap *soap);
extern int hosting_serve_request (struct soap *soap);

int
dpws_mserve (struct soap *soap, int count,
             int (*serve_requests[])(struct soap * soap))
{
#ifndef DPWS_HOSTED_SERVICE
  int found_discovery = 0;
#endif
  register int i, found_hosting = 0;

  /* test parameters */
  soap_assert (soap, soap && serve_requests, SOAP_ERR);

  if (count > 0)
    {
      for (i = 0; i < count; i++)
        {
          if (serve_requests[i])
            {
#ifndef DPWS_HOSTED_SERVICE
              if ((found_discovery == 0) && (serve_requests[i]
                                             == target_serve_request))
                {
                  found_discovery = 1;
                }
#endif
              if ((found_hosting == 0) && (serve_requests[i]
                                           == hosting_serve_request))
                {
                  found_hosting = 1;
                }
            }
        }
    }
  else
    {
      count = 0;
    }

#ifndef DPWS_HOSTED_SERVICE
  if (found_discovery == 0)
    {
      serve_requests[count] = target_serve_request;
      count++;
    }
#endif

  if (found_hosting == 0)
    {
      serve_requests[count] = hosting_serve_request;
      count++;
    }

  return soap_mserve (soap, count, serve_requests);
}

/**
 * DPWS structure logic
 */

#ifdef DPWS_CLIENT
#include "ws4d_targetcache.h"
#endif

#ifdef DPWS_DEVICE
#ifndef DPWS_HOSTED_SERVICE

#include "ws4d_targetservice.h"

struct ws4d_targetservice default_targetservice, *dpws_targetservice = NULL;

static int
dpws_init_targetservice (struct ws4d_targetservice *ts)
{
  if (dpws_targetservice == NULL)
    {
      if (ts)
        {
          dpws_targetservice = ts;
        }
      else
        {
          ws4d_targetservice_init (&default_targetservice,
                                   DPWS_STRCMP0_MATCHING_FUNC, NULL);
          dpws_targetservice = &default_targetservice;
        }

      return WS4D_OK;
    }
  else
    {
      return WS4D_ERR;
    }

}

static void
dpws_done_targetservice ()
{
  if (dpws_targetservice)
    {
      if (dpws_targetservice == &default_targetservice)
        {
          ws4d_targetservice_done (dpws_targetservice);
        }
      dpws_targetservice = NULL;
    }
}
#endif
#endif

#define _dpws_check_device(__device) \
  (__device && ws4d_epr_check(&(__device)->endpoint))

static int
dpws_check_hostaddr (struct ws4d_stringlist *netdevs)
{
  const char *host;
  void *it;

  /* test parameters */
  ws4d_assert (netdevs, 0);

  ws4d_fail (ws4d_stringlist_isempty (netdevs), 0);

  /* check each address in list */
  ws4d_stringlist_foreach (host, it, netdevs)
  {

    /* check if string is not longer than an IPV6 ip addr */
    if (memchr (host, '\0', INET6_ADDRSTRLEN))
      {
        int is_ipv4 = ws4d_is_ipv4 (host, strlen(host));
        int is_ipv6 = ws4d_is_ipv6 (host, strlen(host));
        if (!is_ipv4 && !is_ipv6)
          {
            /* invalid addr */
            return 0;
          }

        /* Any address is not supported */
        if (is_ipv4 && !strcmp (host, "0.0.0.0"))
          {
            return 0;
          }
      }
  }

  return 1;
}

static int
_dpws_init (struct dpws_s *dpws, const char *hosts, void *tc)
{
  int err = WS4D_OK;

#ifndef DPWS_CLIENT
  WS4D_UNUSED_PARAM (tc);
#endif

  /* test parameters */
  ws4d_assert (dpws, WS4D_EPARAM);

  /* initialize epr */
  memset (dpws, 0, sizeof (struct dpws_s));
  err = ws4d_epr_init(&dpws->endpoint);
  ws4d_fail (err != WS4D_OK, err);

#ifdef DPWS_DEVICE
#ifndef DPWS_HOSTED_SERVICE
  /* initialize target service */
  err = dpws_init_targetservice (NULL);
  ws4d_fail (err != WS4D_OK, err);
#endif
  /* initialize hosting service */
  err = ws4d_hosting_init (&dpws->endpoint);
  ws4d_fail (err != WS4D_OK, err);
#endif

  /* intialize interface list */
  err = dpws_set_hosts(dpws, hosts);
  ws4d_fail (err != WS4D_OK, err);

#ifdef DPWS_CLIENT
  /* initialize target cache */
  if (tc == NULL)
    {
      tc = dpws_create_tc (dpws, NULL);
    }
  err = dpws_init_tc (dpws, tc);
#endif

  /* check interface address */
  ws4d_fail (!dpws_check_hostaddr(dpws_get_hosts(dpws)), WS4D_ERR);

  return err;
}

int
dpws_init (struct dpws_s *dpws, const char *hosts)
{
  return _dpws_init (dpws, hosts, NULL);
}

int dpws_set_hosts(struct dpws_s *dpws, const char *hosts)
{
  int err;

  /* test parameters */
  ws4d_assert (dpws, WS4D_EPARAM);

#ifdef DPWS_DEVICE
  err = ws4d_hosting_change_interf(&dpws->endpoint, hosts);
#else
  err = ws4d_targetep_set_XAddrs(&dpws->endpoint, hosts);
#endif

  return err;
}

#ifdef DPWS_CLIENT
int
dpws_init2 (struct dpws_s *dpws, const char *host, struct ws4d_tc_int *tc)
{
  return _dpws_init (dpws, host, tc);
}
#endif

#ifdef DPWS_CLIENT

struct ws4d_tc_int *
dpws_create_tc (struct dpws_s *dpws, void *arg)
{
  struct ws4d_tc_int *result = NULL;
  int err;

  /* test parameters */
  ws4d_assert (_dpws_check_device (dpws), NULL);

  result =
    (struct ws4d_tc_int *) ws4d_malloc_alist (sizeof (struct ws4d_tc_int),
                                              dpws_get_alist(dpws));
  ws4d_assert (result, NULL);

  err = ws4d_tc_init (result, arg);
  if (err != WS4D_OK)
    {
      ws4d_free (result);
      return NULL;
    }

  return result;
}

int
dpws_init_tc (struct dpws_s *dpws, struct ws4d_tc_int *tc)
{
  /* test parameters */
  ws4d_assert (_dpws_check_device (dpws) && tc, WS4D_EPARAM);

  dpws->tc = tc;

  return WS4D_OK;
}

struct ws4d_tc_int *
dpws_get_tc (struct dpws_s *dpws)
{
  /* test parameters */
  ws4d_assert (_dpws_check_device (dpws), NULL);

  return dpws->tc;

}
#endif

#ifdef DPWS_CLIENT

int
dpws_init_implicit_discovery2 (struct dpws_s *dpws, struct ws4d_tc_int *tc,
                               struct soap *dis_mca,
                               struct dpws_discovery_hooks *hooks)
{
  int err = WS4D_OK;

  /* test parameters */
  ws4d_assert (_dpws_check_device (dpws) && tc, WS4D_EPARAM);

#ifdef DPWS_PEER
  ws4d_assert (dpws->dis_mca && !dis_mca, WS4D_ERR);

  dis_mca = dpws->dis_mca;
#else
  ws4d_assert (dis_mca, WS4D_ERR);
#endif

#ifdef DEBUG
  soap_set_omode (dis_mca, SOAP_XML_INDENT);
#endif

  err = wsa_register_handle (dis_mca);
  ws4d_fail (err != SOAP_OK, WS4D_ERR);

  err =
    wsd_soap_init_implicite (dis_mca, tc, dpws_get_hosts(dpws), 100, NULL,
                             (struct wsd_dis_hooks_t *) hooks);
  ws4d_fail (err != SOAP_OK, WS4D_ERR);

  dpws->dis_mca = dis_mca;

  return WS4D_OK;
}

int
dpws_init_implicit_discovery (struct dpws_s *dpws, struct soap *dis_mca,
                              struct dpws_discovery_hooks *dis_hooks)
{
  /* test parameters */
  ws4d_assert (_dpws_check_device (dpws), WS4D_EPARAM);

  return dpws_init_implicit_discovery2 (dpws, dpws->tc, dis_mca, dis_hooks);
}
#endif

#ifdef DPWS_DEVICE
#ifndef DPWS_HOSTED_SERVICE
int
dpws_init_target (struct dpws_s *dpws, struct soap *dis_mca, int backlog)
{
  int err = WS4D_OK;

  /* test parameters */
  ws4d_assert (_dpws_check_device (dpws), WS4D_EPARAM);

  if (!dis_mca)
    {
      dis_mca = ws4d_malloc_alist (sizeof (struct soap), dpws_get_alist(dpws));
      ws4d_fail (!dis_mca, WS4D_EOM);

      soap_init (dis_mca);
    }

#ifdef DEBUG
  soap_set_omode (dis_mca, SOAP_XML_INDENT);
#endif

  err = wsa_register_handle (dis_mca);
  ws4d_fail (err != SOAP_OK, WS4D_ERR);

  err =
    wsd_target_init (dis_mca, dpws_get_hosts(dpws), backlog,
                     dpws_targetservice, NULL);
  ws4d_fail (err != SOAP_OK, WS4D_ERR);

  dpws->dis_mca = dis_mca;

  return WS4D_OK;
}

#endif
#endif

int
dpws_done (struct dpws_s *dpws)
{
  /* test parameters */
  ws4d_assert (dpws, WS4D_EPARAM);

  /* is structure initialized ??? */
  ws4d_fail (!_dpws_check_device (dpws), WS4D_EPARAM);

#ifdef DPWS_DEVICE
  if (dpws->hosting_handle)
    {
      if (dpws->dis_mca)
        {
          soap_done (dpws->dis_mca);
        }
    }
#ifndef DPWS_HOSTED_SERVICE
  dpws_done_targetservice ();
#endif
#endif

#ifdef DPWS_CLIENT
  /* destroy target cache */
  ws4d_tc_done (dpws->tc);
#endif

  ws4d_epr_done (&dpws->endpoint);

  /* memset structure to mark handle unitialized */
  memset (dpws, 0, sizeof (struct dpws_s));

  return WS4D_OK;
}

#ifdef DPWS_DEVICE
#ifndef DPWS_HOSTED_SERVICE

/******
 * Hosting Service Logic
 *
 */

int
dpws_add_hosting_service (struct dpws_s *dpws, struct soap *hosting,
                          int port, const char *id, const char *uuid,
                          int backlog, int https)
{
  int err;
  char uuidstr[37];
  char portstr[6];
  char *host_uri = NULL;
  int host_uri_len = 0;
  struct ws4d_uri uri;
  struct ws4d_qname dpws_type;

  /* test parameters */
  ws4d_assert (_dpws_check_device (dpws) && hosting, WS4D_EPARAM);

  /* is handle or another handle already registered */
  ws4d_fail (dpws->hosting_handle, WS4D_ERR);

#ifdef DEBUG
  soap_set_omode (hosting, SOAP_XML_INDENT);
#endif

  /* generate logical address of hosting_handle service */
  if (uuid)
    {
      /* does the uuid include an urn:uuid: prefix ? */
      if (strncmp
          (uuid, WS4D_UUID_SCHEMA_PREFIX, WS4D_UUID_SCHEMA_PREFIX_LEN))
        {
          return WS4D_ERR;
        }

      ws4d_epr_set_Addrs(&(dpws)->endpoint, uuid);
    }
  else
    {
      char uuidschemastr[WS4D_UUID_SCHEMA_SIZE];

      memset (uuidstr, 0, WS4D_UUID_SIZE);
      memset (uuidschemastr, 0, WS4D_UUID_SCHEMA_SIZE);

      ws4d_uuid_generate_random (uuidstr);
      ws4d_uuid_generate_schema (uuidschemastr, uuidstr);

      ws4d_epr_set_Addrs(&(dpws)->endpoint, uuidschemastr);
    }

  /* generate uri for hosting service */
  memset (portstr, 0, 6);
  SNPRINTF (portstr, 6, "%d", port);

  ws4d_uri_init (&uri);
  uri.scheme = (char *) (https ? "https" : "http");
  uri.host = (char *) "host";
  uri.port = portstr;
  uri.path = (char *) (ws4d_epr_get_Addrs(&dpws->endpoint) + WS4D_UUID_SCHEMA_PREFIX_LEN);

  host_uri_len = ws4d_uri_tostrlen (&uri) + 20;
  host_uri = ws4d_malloc_alist (host_uri_len, dpws_get_alist(dpws));
  ws4d_assert (host_uri, WS4D_EOM);

  err = ws4d_uri_tostr (&uri, host_uri, host_uri_len);
  ws4d_fail (err != WS4D_OK, err);

  /* bind soap handle to uri */
  err = dpws_handle_bind (dpws, hosting, host_uri, host_uri_len, backlog);
  ws4d_fail (err == SOAP_INVALID_SOCKET, WS4D_ERR);

  ws4d_free_alist (host_uri);
  ws4d_uri_done (&uri);

  dpws->hosting_handle = hosting;

  /* check if hosting_target service is initialized */
  if (!dpws->dis_mca)
    {
      err = dpws_init_target (dpws, NULL, backlog);
      ws4d_fail (err != WS4D_OK, err);
    }

  /* make hosting_handle service a hosting_target service */
  dpws->hosting_target = wsd_target_add (dpws->dis_mca,
                                         ws4d_epr_get_Addrs(&dpws->endpoint),
                                         dpws_handle_get_paddrs(dpws->hosting_handle),
                                         NULL);
  ws4d_fail (!dpws->hosting_target, WS4D_ERR);

  if (!id)
    {
      ws4d_serviceep_setid(&dpws->endpoint,
                           "http://www.ws4d.org/ws4d-gsoap/hostingservice");
    }
  else
    {
      ws4d_serviceep_setid(&dpws->endpoint, id);
    }

  /* add dpws device type to device */
  ws4d_qname_init (&dpws_type);
  ws4d_qname_setPrefix(&dpws_type, "wsdp");
  ws4d_qname_setName(&dpws_type, "Device");
  ws4d_qname_setNS(&dpws_type, DPWS_NS);

  err = dpws_add_type (dpws, &dpws_type);
  ws4d_fail (err != WS4D_OK, err);

  /* initialize metadata exchange */
  err = mex_init (dpws->hosting_handle);
  ws4d_fail (err != SOAP_OK, WS4D_ERR);

  mex_register_metadata_sectiontype (dpws->hosting_handle, DPWS_MEX_MODEL,
                                     "wsdp:ThisModel",
                                     &thisModel_serializers);
  mex_register_metadata_sectiontype (dpws->hosting_handle, DPWS_MEX_DEVICE,
                                     "wsdp:ThisDevice",
                                     &thisDevice_serializers);
  mex_register_metadata_sectiontype (dpws->hosting_handle,
                                     DPWS_MEX_RELATIONSHIP,
                                     "wsdp:Relationship",
                                     &relationship_serializers);

  return WS4D_OK;
}

int
dpws_add_type (struct dpws_s *device, struct ws4d_qname *type)
{
  int err;

  /* test parameters */
  ws4d_assert (_dpws_check_device (device) && device->hosting_target
               && type, WS4D_EPARAM);

  /* add type to target end point */
  err = ws4d_targetep_add_Type (device->hosting_target, type);
  ws4d_fail (err, WS4D_ERR);

  /* add namespace related to type */
  err = wsd_target_set_namespaces (device->hosting_target, device->dis_mca,
                                   NULL, dpws_get_alist(device));
  ws4d_fail (err, WS4D_ERR);

  return WS4D_OK;
}

int
dpws_add_typestring (struct dpws_s *device, const char *types)
{
  int err;

  /* test parameters */
  ws4d_assert (_dpws_check_device (device) && device->hosting_target
               && types, WS4D_EPARAM);

  /* add types to target end point */
  err = ws4d_targetep_add_Typestr (device->hosting_target, types);
  ws4d_fail (err, WS4D_ERR);

  /* add namespaces related to types */
  err = wsd_target_set_namespaces (device->hosting_target, device->dis_mca,
                                   NULL, dpws_get_alist(device));
  ws4d_fail (err, WS4D_ERR);

  return WS4D_OK;
}

int
dpws_add_scope (struct dpws_s *device, const char *scope)
{
  int err;

  /* test parameters */
  ws4d_assert (_dpws_check_device (device) && device->hosting_target
               && scope, WS4D_EPARAM);

  /* add scope to target end point */
  err = ws4d_targetep_add_Scope (device->hosting_target, scope);
  ws4d_fail (err, WS4D_ERR);

  return WS4D_OK;
}

/*******
 * Discovery
 *
 */


int
dpws_send_Hello (struct dpws_s *device, const char *types)
{
  char MsgId[DPWS_MSGID_SIZE];
  int err;

  /* test parameters */
  ws4d_assert (_dpws_check_device (device)
               && device->hosting_target, WS4D_EPARAM);

  /* fail when target endpoint is not active */
  ws4d_fail (!ws4d_targetep_isactive (device->hosting_target), WS4D_ERR);

  /* send message */
  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  err =
    wsd_send_Hello (device->dis_mca, MsgId, device->hosting_target, types);
  soap_end (device->dis_mca);
  ws4d_fail (err != SOAP_OK, WS4D_ERR);

  return WS4D_OK;
}

int
dpws_send_Bye (struct dpws_s *device)
{
  char MsgId[DPWS_MSGID_SIZE];
  int err;

  /* test parameters */
  ws4d_assert (_dpws_check_device (device)
               && device->hosting_target, WS4D_EPARAM);

  /* fail when target endpoint is not active */
  ws4d_fail (!ws4d_targetep_isactive (device->hosting_target), WS4D_ERR);

  /* send message */
  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  err = wsd_send_Bye (device->dis_mca, MsgId, device->hosting_target);
  soap_end (device->dis_mca);
  ws4d_fail (err != SOAP_OK, WS4D_ERR);

  return WS4D_OK;
}

int
dpws_activate_hosting_service (struct dpws_s *dpws)
{
  /* test parameters */
  ws4d_assert (_dpws_check_device (dpws)
               && dpws->hosting_target, WS4D_EPARAM);

  /* try to activate target endpoint */
  if (ws4d_targetep_activate (dpws->hosting_target, 0, 0, 0) == WS4D_OK)
    {
      /* if successfull send hello message */
      return dpws_send_Hello (dpws, dpws_get_types(dpws));
    }

  return WS4D_ERR;
}

int
dpws_deactivate_hosting_service (struct dpws_s *dpws)
{
  /* test parameters */
  ws4d_assert (_dpws_check_device (dpws)
               && dpws->hosting_target, WS4D_EPARAM);

  /*
   * ignore return value. we want to deactivate the device and optionally
   * send a bye message
   */
  dpws_send_Bye (dpws);

  return ws4d_targetep_deactivate (dpws->hosting_target);
}

#endif
#endif

#ifndef DPWS_HOSTED_SERVICE
/**
 * Function to receive Probe messages and to send ProbeMatches messages
 * if the device matches the request. The function processed multicast
 * probes and directed probes. To dispatch these
 */
int
__wsd__Probe (struct soap *soap, struct wsd__ProbeType *wsd__Probe)
{
#ifdef DPWS_DEVICE
  struct ws4d_abs_eprlist matching_targets;

  if (wsa_message_isdup (soap))
    return SOAP_OK;

  ws4d_eprlist_init (&matching_targets, ws4d_eprllist_init, NULL);

  if (wsd_process_probe (soap, wsd__Probe, &matching_targets) == SOAP_OK)
    {

      if (!ws4d_eprlist_isempty (&matching_targets))
        {
          char MsgId[DPWS_MSGID_SIZE];
          struct wsd__ProbeMatchesType my__ProbeMatches;

          dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);

          wsd_gen_ProbeMatches (soap, &matching_targets, &my__ProbeMatches);

          wsd_send_async_ProbeMatches (soap, MsgId,
                                       wsd_gen_response_addr (soap, "/"),
                                       &matching_targets, soap,
                                       soap_get_allocator (soap),
                                       &my__ProbeMatches);

        }
      ws4d_eprlist_done (&matching_targets);

    }
  else
    {

      struct dpws_s *device = dpws_handle_get_device (soap);
      ws4d_fail (!device, SOAP_OK);

      soap_copy_local_namespaces (soap, device->dis_mca);

      if ((wsd_process_probe
           (device->dis_mca, wsd__Probe, &matching_targets) == SOAP_OK)
          && !ws4d_eprlist_isempty (&matching_targets))
        {
          char MsgId[DPWS_MSGID_SIZE];
          struct wsd__ProbeMatchesType wsd__ProbeMatches;

          soap_default_wsd__ProbeMatchesType (soap, &wsd__ProbeMatches);

          dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
          wsd_gen_ProbeMatches_header (soap, MsgId, "", &matching_targets,
                                       soap, soap->namespaces,
                                       soap_get_allocator (soap));

          wsd_gen_ProbeMatches (soap, &matching_targets, &wsd__ProbeMatches);

          /* directly send probe matches */
          soap_serializeheader (soap);
          soap_serialize_wsd__ProbeMatchesType (soap, &wsd__ProbeMatches);

          if (soap_begin_count (soap))
            {
              ws4d_eprlist_done (&matching_targets);
              return soap->error;
            }

          if (soap->mode & SOAP_IO_LENGTH)
            {
              if (soap_envelope_begin_out (soap) || soap_putheader (soap)
                  || soap_body_begin_out (soap)
                  || soap_put_wsd__ProbeMatchesType (soap,
                                                     &wsd__ProbeMatches,
                                                     "wsd:ProbeMatches",
                                                     "")
                  || soap_body_end_out (soap) || soap_envelope_end_out (soap))
                {
                  ws4d_eprlist_done (&matching_targets);
                  return soap->error;
                }
            };
          if (soap_end_count (soap) || soap_response (soap, SOAP_OK)
              || soap_envelope_begin_out (soap) || soap_putheader (soap)
              || soap_body_begin_out (soap)
              || soap_put_wsd__ProbeMatchesType (soap, &wsd__ProbeMatches,
                                                 "wsd:ProbeMatches", "")
              || soap_body_end_out (soap) || soap_envelope_end_out (soap)
              || soap_end_send (soap))
            {
              ws4d_eprlist_done (&matching_targets);
              return soap->error;
            }

          ws4d_eprlist_done (&matching_targets);

          return SOAP_OK;
        }
      else
        {
          ws4d_eprlist_done (&matching_targets);

          return soap_send_empty_response (soap, 202);
        }
    }
#else
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (wsd__Probe);
#endif
  return SOAP_STOP;
}

int
__wsd__Resolve (struct soap *soap, struct wsd__ResolveType *wsd__Resolve)
{
#ifdef DPWS_DEVICE
  struct ws4d_epr *target = NULL;
  int err;

  if (wsa_message_isdup (soap))
    return SOAP_OK;

  err = wsd_process_resolve (soap, wsd__Resolve, &target);
  if (target != NULL)
    {
      char MsgId[DPWS_MSGID_SIZE];

      dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
      wsd_send_ResolveMatches (soap, MsgId,
                               wsd_gen_response_addr (soap, "/"), target,
                               soap);
    }
  else
    {
      return err;
    }
#else
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (wsd__Resolve);
#endif
  return SOAP_OK;
}

int
__wsd__Hello (struct soap *soap, struct wsd__HelloType *wsd__Hello)
{
#ifdef DPWS_CLIENT
  if (wsa_message_isdup (soap))
    return SOAP_OK;

  return wsd_process_hello (soap, wsd__Hello);
#else
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (wsd__Hello);

  return SOAP_OK;
#endif
}

int
__wsd__Bye (struct soap *soap, struct wsd__ByeType *wsd__Bye)
{
#ifdef DPWS_CLIENT
  if (wsa_message_isdup (soap))
    return SOAP_OK;

  return wsd_process_bye (soap, wsd__Bye);
#else
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (wsd__Bye);

  return SOAP_OK;
#endif
}

int
__wsd__ProbeMatches (struct soap *soap,
                     struct wsd__ProbeMatchesType *wsd__ProbeMatches)
{
#ifdef DPWS_CLIENT
  if (wsa_message_isdup (soap))
    return SOAP_OK;

  return wsd_process_probematches (soap, wsd__ProbeMatches);
#else
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (wsd__ProbeMatches);

  return SOAP_OK;
#endif
}

int
__wsd__ResolveMatches (struct soap *soap,
                       struct wsd__ResolveMatchesType *wsd__ResolveMatches)
{
#ifdef DPWS_CLIENT
  if (wsa_message_isdup (soap))
    return SOAP_OK;

  return wsd_process_resolvematches (soap, wsd__ResolveMatches);
#else
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (wsd__ResolveMatches);

  return SOAP_OK;
#endif
}
#endif

#ifdef DPWS_CLIENT

#include "ws4d_discovery_ctx.h"

int
dpws_probe2 (struct dpws_s *dpws, ws4d_qnamelist * Types, const char *Scope,
             ws4d_time timeout, int backlog,
             struct dpws_discovery_hooks *dis_hooks, struct ws4d_tc_int *tc,
             int count, struct ws4d_abs_eprlist *result)
{
  struct dpws_probectx ctx;
  struct soap handle;
  char MsgId[DPWS_MSGID_SIZE];
  int ret = 0, matches = 0;

  /* test parameters */
  ws4d_assert (dpws, WS4D_ERR);

  if (!tc)
    {
      ws4d_assert (dpws->tc, WS4D_ERR);
      tc = dpws->tc;
    }

  soap_init (&handle);
#ifdef DEBUG
  soap_set_omode (&handle, SOAP_XML_INDENT);
#endif

  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);

  ret =
    ws4d_discoveryctx_init (&ctx, (char *) MsgId, dpws_get_hosts(dpws), tc,
                            &handle, (struct wsd_dis_hooks_t *) dis_hooks,
                            dpws->uni_hooks, backlog);
  if (ret != WS4D_OK)
    {
      ws4d_discoveryctx_done (&ctx);
      return WS4D_ERR;
    }

  if (Types)
    {
      ws4d_discoveryctx_set_Types (&ctx, Types);
    }

  if (Scope)
    {
      ws4d_discoveryctx_set_Scope (&ctx, Scope);
    }

  ret = ws4d_discoveryctx_probe (&ctx, timeout);
  if (ret == WS4D_OK)
    {
      ret = ws4d_discoveryctx_process_probe (&ctx, count);

      if ((ret == WS4D_OK) || (ret == WS4D_TO))
        {
          if (result != NULL)
            {
              ret = ws4d_discoveryctx_get_ProbeResults (&ctx, result, &matches);
              if (matches > 0)
                {
                  ret = WS4D_OK;
                }
              else
                {
                  ret = WS4D_TO;
                }
            }
          else
            {
              ret = WS4D_OK;
            }
        }
      else
        {
          ret = WS4D_ERR;
        }
    }
  else
    {
      ret = WS4D_ERR;
    }

  ws4d_discoveryctx_done (&ctx);

  return ret;
}

int
dpws_dprobe (struct dpws_s *dpws, struct ws4d_epr *epr,
             ws4d_qnamelist * Types, const char *Scope,
             struct ws4d_tc_int *tc, ws4d_time timeout)
{
  struct soap handle;
  struct dpws_probectx ctx;
  char MsgId[DPWS_MSGID_SIZE];
  int ret = 0;
  char *addr = NULL;

  /* test parameters */
  ws4d_assert (dpws, WS4D_ERR);

  if (!tc)
    {
      ws4d_assert (dpws->tc, WS4D_ERR);
      tc = dpws->tc;
    }

  addr = (char *) dpws_resolve_addr (dpws, epr, NULL, timeout);
  if (!addr)
    return WS4D_ERR;

  soap_init (&handle);
#ifdef DEBUG
  soap_set_omode (&handle, SOAP_XML_INDENT);
#endif

  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  ret =
    ws4d_discoveryctx_init_directed (&ctx, MsgId, dpws_get_hosts(dpws), tc,
                                     &handle, NULL, NULL, 100);
  if (ret != SOAP_OK)
    return WS4D_ERR;

  ws4d_discoveryctx_set_Types (&ctx, Types);
  if (Scope)
    {
      ws4d_discoveryctx_set_Scope (&ctx, Scope);
    }

  ret = ws4d_discoveryctx_dprobe (&ctx, addr, timeout);
  if (ret == SOAP_OK)
    {
      ws4d_discoveryctx_get_dProbeResult (&ctx, epr);
    }

  soap_end (&handle);
  soap_done (&handle);

  ws4d_discoveryctx_done (&ctx);

  return ret;
}

const char *
dpws_resolve_addr (struct dpws_s *dpws, struct ws4d_epr *epr,
                   struct ws4d_tc_int *tc, ws4d_time timeout)
{
  const char *XAddrs = NULL;

  /* test parameters */
  ws4d_assert (dpws && epr, NULL);

  if (!tc)
    {
      ws4d_assert (dpws->tc, NULL);
      tc = dpws->tc;
    }

  ws4d_tc_lock (tc);
  XAddrs = ws4d_tc_get_XAddrs (tc, ws4d_epr_get_Addrs (epr));
  ws4d_tc_unlock (tc);

  if (!XAddrs)
    {
      struct soap handle;
      struct dpws_probectx ctx;
      char MsgId[DPWS_MSGID_SIZE];
      int ret;

      soap_init (&handle);
#ifdef DEBUG
      soap_set_omode (&handle, SOAP_XML_INDENT);
#endif

      /* do we have to resolve this address ? */
      if (wsd_is_physical_addr (&handle, ws4d_epr_get_Addrs (epr)) == SOAP_OK)
        {
          /* TODO: do this without direct target cache api use */
          ws4d_targetep_set_XAddrs (epr, ws4d_epr_get_Addrs (epr));
          return ws4d_epr_get_Addrs (epr);
        }

      dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
      if (ws4d_discoveryctx_init
          (&ctx, MsgId, dpws_get_hosts(dpws), tc, &handle, NULL, NULL,
           100) != SOAP_OK)
        {
          return NULL;
        }

      ws4d_discoveryctx_set_Address (&ctx, ws4d_epr_get_Addrs (epr));

      ret = ws4d_discoveryctx_resolve (&ctx, timeout);
      if (ret == WS4D_OK)
        {
          ret = ws4d_discoveryctx_process_resolve (&ctx);

          if (ret == WS4D_OK)
            {
              ws4d_discoveryctx_get_ResolveResult (&ctx, epr);
              XAddrs = ws4d_targetep_get_XAddrs (epr);
            }
        }

      soap_end (&handle);
      soap_done (&handle);

      ws4d_discoveryctx_done (&ctx);
    }

  return XAddrs;
}

int
dpws_device_issecured (const char *XAddrs)
{
  /* test parameters */
  ws4d_assert (XAddrs, -1);

  return !strncmp (XAddrs, "https://", 8);
}
#endif

#ifdef DPWS_DEVICE

#include "ws4d_hostingservice.h"
#include "ws4d_eprllist.h"

struct ws4d_epr *
dpws_service_init (struct dpws_s *device, const char *serviceid)
{
  return ws4d_hosting_add_service (&device->endpoint, serviceid);
}

int
dpws_service_bind (struct dpws_s *device, struct ws4d_epr *service,
                   struct soap *handle, char *uri, size_t size, int backlog)
{
  int ret;

  /* test parameters */
  ws4d_assert (device && service && handle && uri && (size > 0), WS4D_ERR);

  ret = dpws_handle_bind (device, handle, uri, size, backlog);
  ws4d_fail (ret == SOAP_INVALID_SOCKET, WS4D_ERR);

  ret = ws4d_hosting_bind_service (&device->endpoint, service, uri, size);
  ws4d_fail (ret != WS4D_OK, ret);

  ws4d_serviceep_set_transportdata (service, handle);

  mex_init (handle);

  mex_register_metadata_sectiontype (handle, DPWS_MEX_WSDL, "-wsdl",
                                     &mex_literal_serializers);

  return WS4D_OK;
}

int
dpws_add_hosted_service (struct dpws_s *device,
                         struct ws4d_epr *service, char *uri, size_t size)
{
  /* test parameters */
  ws4d_assert (device, WS4D_ERR);

  return ws4d_hosting_activate_service (&device->endpoint, service, uri, size);
}

int
dpws_del_hosted_service (struct dpws_s *device, struct ws4d_epr *service)
{
  /* test parameters */
  ws4d_assert (device, WS4D_ERR);

  return ws4d_hosting_deactivate_service (&device->endpoint, service);
}

int
dpws_service_done (struct dpws_s *device, struct ws4d_epr *service)
{
  /* test parameters */
  ws4d_assert (device, WS4D_ERR);

  return ws4d_hosting_remove_service (&device->endpoint, service);
}

struct ws4d_epr *
dpws_get_hosted_service (struct dpws_s *device, const char *serviceid)
{
  /* test parameters */
  ws4d_assert (device, NULL);

  return ws4d_hosting_get_service(&device->endpoint, serviceid);
}

#endif
/******
 * Description
 *
 */

#ifdef DPWS_DEVICE

static int
dpws_register_Metadata (struct dpws_s *dpws, struct soap *soap,
                        const char *endpoint, const char *dialect,
                        const void *metadata)
{
  void *section_content = NULL;

  /* test parameters */
  ws4d_assert (dpws && soap && endpoint && *endpoint && dialect
               && *dialect && metadata, WS4D_ERR);

#ifndef DPWS_HOSTED_SERVICE
  if (!strcmp (dialect, DPWS_MEX_MODEL))
    {
      int err;
      const struct ws4d_thisModel *src = metadata;
      struct wsdp__ThisModelType *model;
      char *PresentationUrl;

      /* allocate memory for ThisModel */
      model = ws4d_malloc_alist (sizeof (struct wsdp__ThisModelType),
                                 dpws_get_alist (dpws));
      ws4d_fail (model == NULL, WS4D_EOM);

      section_content = model;

      /* copy model fields */
      err = ws4d_thismodel_copy ((struct ws4d_thisModel *) model, src,
                                 dpws_get_alist (dpws));
      ws4d_fail (err != WS4D_OK, err);

      PresentationUrl = (char *) dpws_presentation_getaddr (dpws);
      if (PresentationUrl)
        {
          model->PresentationUrl =
            ws4d_strdup (PresentationUrl, dpws_get_alist (dpws));
        }
    }
  else if (!strcmp (dialect, DPWS_MEX_DEVICE))
    {
      int err;
      const struct ws4d_thisDevice *src = metadata;
      struct wsdp__ThisDeviceType *device;

      /* allocate memory for ThisModel */
      device = ws4d_malloc_alist (sizeof (struct wsdp__ThisDeviceType),
                                  dpws_get_alist (dpws));
      ws4d_fail (device == NULL, WS4D_EOM);

      section_content = device;

      /* copy model fields */
      err = ws4d_thisdevice_copy ((struct ws4d_thisDevice *) device, src,
                                  dpws_get_alist (dpws));
      ws4d_fail (err != WS4D_OK, err);
    }
  else if (!strcmp (dialect, DPWS_MEX_RELATIONSHIP))
    {
      int i;
      const struct ws4d_relationship *src = metadata;
      struct _wsdp__Relationship *relationship;

      /* allocate memory for ThisModel */
      relationship = ws4d_malloc_alist (sizeof (struct _wsdp__Relationship),
                                        dpws_get_alist (dpws));
      ws4d_fail (relationship == NULL, WS4D_EOM);

      section_content = relationship;

      /* host section */
      relationship->wsdp__Host =
        ws4d_malloc_alist (sizeof (struct wsdp__HostServiceType),
                           dpws_get_alist (dpws));
      ws4d_fail (relationship->wsdp__Host == NULL, WS4D_EOM);

      relationship->wsdp__Host->wsa__EndpointReference.Address =
        ws4d_strdup (dpws_get_UUID(dpws), dpws_get_alist (dpws));
      relationship->wsdp__Host->wsdp__Types =
        ws4d_strdup ((char *) dpws_get_types(dpws), dpws_get_alist (dpws));

#if !defined(DEVPROF_2009_01)
      relationship->wsdp__Host->wsdp__ServiceId =
        ws4d_strdup (dpws_get_UUID(dpws), dpws_get_alist (dpws));
#endif

      /* hosted section */
      relationship->__sizeHosted = src->hosted_count;

#if !defined(DEVPROF_2009_01)
      relationship->wsdp__Hosted =
        ws4d_malloc_alist (sizeof (struct wsdp__HostServiceType) *
                           src->hosted_count, dpws_get_alist (dpws));
#else
      relationship->wsdp__Hosted =
        ws4d_malloc_alist (sizeof (struct wsdp__HostedServiceType) *
                           src->hosted_count, dpws_get_alist (dpws));
#endif

      for (i = 0; i < src->hosted_count; i++)
        {
          relationship->wsdp__Hosted[i].wsa__EndpointReference.Address =
            ws4d_strdup (src->hosted[i].Addr, dpws_get_alist (dpws));
          relationship->wsdp__Hosted[i].wsdp__ServiceId =
            ws4d_strdup (src->hosted[i].ServiceId, dpws_get_alist (dpws));
          relationship->wsdp__Hosted[i].wsdp__Types =
            ws4d_strdup ((char *)
                         ws4d_qnamelist_tostring (&src->hosted[i].types),
                         dpws_get_alist (dpws));

          if (src->hosted[i].__any)
            {
              relationship->wsdp__Hosted[i].__any =
                ws4d_malloc_alist (sizeof (char *), dpws_get_alist (dpws));
              relationship->wsdp__Hosted[i].__size = 1;
              relationship->wsdp__Hosted[i].__any[0] =
                ws4d_strdup (src->hosted[i].__any, dpws_get_alist (dpws));
            }
        }
    }
  else
    {
#endif
      section_content = (void *) metadata;
#ifndef DPWS_HOSTED_SERVICE
    }
#endif

  return mex_register_metadata_section (soap, endpoint, dialect, NULL,
                                        section_content);
}

static void *
dpws_lookup_Metadata (struct dpws_s *dpws, struct soap *soap,
                      const char *endpoint, const char *dialect,
                      const char *identifier)
{
  ws4d_assert (dpws && soap && endpoint && *endpoint && dialect
               && *dialect, NULL);

  return mex_get_metadata_section (soap, endpoint, dialect, identifier);
}

static int
dpws_free_Metadata (struct dpws_s *dpws, struct soap *soap,
                    const char *endpoint, const char *dialect)
{
  void *section_content;
  int err;

  /* test parameters */
  ws4d_assert (dpws && soap && endpoint && *endpoint && dialect
               && *dialect, WS4D_ERR);

  /* loop up metadata section */
  section_content =
    dpws_lookup_Metadata (dpws, soap, endpoint, dialect, NULL);
  ws4d_fail (section_content == NULL, WS4D_ERR);

  /* remove section */
  err = mex_remove_metadata_section (soap, endpoint, dialect, NULL);

  /* free section content */
  if (!strcmp (dialect, DPWS_MEX_MODEL))
    {
      struct wsdp__ThisModelType *model = section_content;

      /* free model fields */
      ws4d_free_alist (model->ManufacturerUrl);
      ws4d_free_alist (model->ModelNumber);
      ws4d_free_alist (model->ModelUrl);
      ws4d_free_alist (model->PresentationUrl);
      soap_locstring_free (model->Manufacturer, model->__sizeManufacturer);
      ws4d_free_alist (model->Manufacturer);
      soap_locstring_free (model->ModelName, model->__sizeModelName);
      ws4d_free_alist (model->ModelName);
      if (model->__size == 1)
        {
          ws4d_free_alist (model->__any[0]);
          ws4d_free_alist (model->__any);
        }

      /* free model */
      ws4d_free_alist (model);
    }
  else if (!strcmp (dialect, DPWS_MEX_DEVICE))
    {
      struct wsdp__ThisDeviceType *device = section_content;

      /* free device fields */
      ws4d_free_alist (device->FirmwareVersion);
      ws4d_free_alist (device->SerialNumber);
      soap_locstring_free (device->FriendlyName, device->__sizeFriendlyName);
      ws4d_free_alist (device->FriendlyName);
      if (device->__size == 1)
        {
          ws4d_free_alist (device->__any[0]);
          ws4d_free_alist (device->__any);
        }

      /* free device */
      ws4d_free_alist (device);
    }
  else if (!strcmp (dialect, DPWS_MEX_RELATIONSHIP))
    {
      struct _wsdp__Relationship *relationship = section_content;

      /* free relationship fields */
      ws4d_free_alist (relationship->wsdp__Host->wsa__EndpointReference.
                       Address);
      ws4d_free_alist (relationship->wsdp__Host->wsdp__Types);
#if !defined(DEVPROF_2009_01)
      ws4d_free_alist (relationship->wsdp__Host->wsdp__ServiceId);
#endif
      if (relationship->__sizeHosted > 0)
        {
          int i;

          for (i = 0; i < relationship->__sizeHosted; i++)
            {
              ws4d_free_alist (relationship->
                               wsdp__Hosted[i].wsa__EndpointReference.
                               Address);
              ws4d_free_alist (relationship->wsdp__Hosted[i].wsdp__Types);
              ws4d_free_alist (relationship->wsdp__Hosted[i].wsdp__ServiceId);
              if (relationship->wsdp__Hosted[i].__size == 1)
                {
                  ws4d_free_alist (relationship->wsdp__Hosted[i].__any[0]);
                  ws4d_free_alist (relationship->wsdp__Hosted[i].__any);
                }
            }
          ws4d_free_alist (relationship->wsdp__Hosted);
        }

      /* free relationship */
      ws4d_free_alist (relationship);
    }

  return err;
}

#ifndef DPWS_HOSTED_SERVICE

struct ws4d_thisDevice *
dpws_change_thisdevice (struct dpws_s *dpws)
{
  return ws4d_hosting_change_thisdevice (&dpws->endpoint);
}

/**
 * Function prepares device metadata section for GetMetadata
 *
 * @param dpws device
 *
 * @return
 */
int
dpws_updatemetadata_Device (struct dpws_s *dpws)
{
  int err = WS4D_OK;
  struct _wsm__MetadataSection *section = NULL;
  struct ws4d_thisDevice *_device =
    ws4d_hosting_get_thisdevice (&dpws->endpoint);

  section = dpws_lookup_Metadata (dpws, dpws->hosting_handle,
                                  dpws_get_UUID(dpws), DPWS_MEX_DEVICE, NULL);

  if (section)
    {
      dpws_free_Metadata (dpws, dpws->hosting_handle, dpws_get_UUID(dpws),
                          DPWS_MEX_DEVICE);
    }

  err = dpws_register_Metadata (dpws, dpws->hosting_handle,
                                dpws_get_UUID(dpws), DPWS_MEX_DEVICE, _device);
  if (err)
    return err;

  section = dpws_lookup_Metadata (dpws, dpws->hosting_handle,
                                  dpws_get_XAddrs(dpws), DPWS_MEX_DEVICE,
                                  NULL);

  if (section)
    {
      dpws_free_Metadata (dpws, dpws->hosting_handle,
                          dpws_get_XAddrs(dpws), DPWS_MEX_DEVICE);
    }

  return dpws_register_Metadata (dpws, dpws->hosting_handle,
                                 dpws_get_XAddrs(dpws), DPWS_MEX_DEVICE,
                                 _device);
}

struct ws4d_thisModel *
dpws_change_thismodel (struct dpws_s *dpws)
{
  return ws4d_hosting_change_thismodel (&dpws->endpoint);
}

/**
 * Function prepares model metadata section for GetMetadata
 *
 * @param dpws device
 *
 * @return
 */

int
dpws_updatemetadata_Model (struct dpws_s *dpws)
{
  int err = WS4D_OK;
  struct _wsm__MetadataSection *section = NULL;
  struct ws4d_thisModel *model = ws4d_hosting_get_thismodel (&dpws->endpoint);

  section = dpws_lookup_Metadata (dpws, dpws->hosting_handle,
                                  dpws_get_UUID(dpws), DPWS_MEX_MODEL, NULL);

  if (section)
    {
      dpws_free_Metadata (dpws, dpws->hosting_handle, dpws_get_UUID(dpws),
                          DPWS_MEX_MODEL);
    }

  err = dpws_register_Metadata (dpws, dpws->hosting_handle,
                                dpws_get_UUID(dpws), DPWS_MEX_MODEL, model);
  if (err)
    return err;

  section = dpws_lookup_Metadata (dpws, dpws->hosting_handle,
                                  dpws_get_XAddrs(dpws), DPWS_MEX_MODEL,
                                  NULL);

  if (section)
    {
      dpws_free_Metadata (dpws, dpws->hosting_handle,
                          dpws_get_XAddrs(dpws), DPWS_MEX_MODEL);
    }

  return dpws_register_Metadata (dpws, dpws->hosting_handle,
                                 dpws_get_XAddrs(dpws), DPWS_MEX_MODEL,
                                 model);

}

static int
dpws_updatemetadata_Relationship (struct dpws_s *dpws)
{
  struct ws4d_relationship *relationship;
  ws4d_qnamelist relationship_types;
  int err = WS4D_OK, i = 0;

  dpws_lock(dpws);
  relationship =
    ws4d_hosting_get_relationship (&dpws->endpoint, dpws_get_alist (dpws));
  dpws_unlock(dpws);

  ws4d_qnamelist_init (&relationship_types);
  ws4d_qnamelist_addlist (&relationship_types, dpws_get_typesList(dpws));

  for (i = 0; i < relationship->hosted_count; i++)
    {
      ws4d_qnamelist_addlist (&relationship_types,
                              &relationship->hosted[i].types);
    }

  if (dpws_lookup_Metadata (dpws, dpws->hosting_handle, dpws_get_UUID(dpws),
                            DPWS_MEX_RELATIONSHIP, NULL))
    {
      dpws_free_Metadata (dpws, dpws->hosting_handle, dpws_get_UUID(dpws),
                          DPWS_MEX_RELATIONSHIP);
    }

  err = dpws_register_Metadata (dpws, dpws->hosting_handle,
                                dpws_get_UUID(dpws), DPWS_MEX_RELATIONSHIP,
                                relationship);
  ws4d_fail (err != WS4D_OK, err);

  if (dpws_lookup_Metadata (dpws, dpws->hosting_handle,
                            dpws_get_XAddrs(dpws), DPWS_MEX_RELATIONSHIP, NULL))
    {
      dpws_free_Metadata (dpws, dpws->hosting_handle,
                          dpws_get_XAddrs(dpws), DPWS_MEX_RELATIONSHIP);
    }

  err = dpws_register_Metadata (dpws, dpws->hosting_handle,
                                dpws_get_XAddrs(dpws), DPWS_MEX_RELATIONSHIP,
                                relationship);
  ws4d_fail (err != WS4D_OK, err);

  if (!ws4d_qnamelist_isempty (&relationship_types))
    {
      struct Namespace *result;

      result = soap_qnamelist_extend_namespaces(&relationship_types,
                                                dpws->hosting_handle->namespaces,
                                                dpws_get_alist(dpws));

      if (result)
      {
          soap_set_namespaces (dpws->hosting_handle, result);
      }
    }

  ws4d_hosting_free_relationship (&dpws->endpoint, relationship);
  ws4d_qnamelist_done (&relationship_types);

  return WS4D_OK;
}
#endif

/* TODO: don't copy active service list here */
static int
dpws_updatemetadata_WSDL (struct dpws_s *dpws)
{
  register struct ws4d_epr *service = NULL, *iter;
  struct ws4d_abs_eprlist services;
  int err, service_count = 0;

  /* test parameters */
  ws4d_assert (dpws, WS4D_ERR);

  ws4d_eprlist_init (&services, ws4d_eprllist_init, NULL);

  dpws_lock(dpws);
  service_count = ws4d_hosting_get_activeservices (&dpws->endpoint, &services);
  dpws_unlock(dpws);

  if (service_count == 0)
    {
      return WS4D_OK;
    }

  ws4d_eprlist_foreach (service, iter, &services)
  {
    const char *buf = NULL;
    int d = 0;
    struct soap *service_handle = ws4d_serviceep_get_transportdata (service);
    const char *service_addr = ws4d_epr_get_Addrs (service);
    const char *wsdl_name = ws4d_serviceep_get_wsdl (service);

    if (!service_handle || !service_addr || !wsdl_name)
      continue;

    /* Test if wsdls are registered */
    ws4d_fail (dpws->wsdls == NULL, WS4D_ERR);

    dpws_lock (dpws);
    while (dpws->wsdls[d].content != NULL)
      {
        if (strstr (dpws->wsdls[d].name, wsdl_name))
          {
            buf = dpws->wsdls[d].content;
            break;
          }
        d++;
      }
    dpws_unlock (dpws);

    if (dpws_lookup_Metadata (dpws, service_handle,
                              service_addr, DPWS_MEX_WSDL, NULL))
      {
        dpws_free_Metadata (dpws, service_handle,
                            service_addr, DPWS_MEX_WSDL);
      }

    if (buf != NULL)
      {
        err =
          dpws_register_Metadata (dpws, service_handle, service_addr,
                                  DPWS_MEX_WSDL, (void *) buf);
        if (err != WS4D_OK)
          {
            return err;
          }
      }
  }

  ws4d_eprlist_done (&services);

  return WS4D_OK;
}

int
dpws_update_Metadata_hosting (struct dpws_s *device)
{
  ws4d_assert (device, WS4D_ERR);

#ifndef DPWS_HOSTED_SERVICE
  if (dpws_updatemetadata_Relationship (device)
      || dpws_updatemetadata_Model (device)
      || dpws_updatemetadata_Device (device))
    {
      return WS4D_ERR;
    }

  /* TODO: this should be done better */
  ws4d_targetep_changeMetadata (device->hosting_target);
  if (ws4d_targetep_isactive (device->hosting_target))
    {
      return dpws_send_Hello (device, dpws_get_types(device));
    }
#endif
  return WS4D_OK;
}

int
dpws_update_Metadata (struct dpws_s *device)
{
  ws4d_assert (device, WS4D_ERR);

  if (
#ifndef DPWS_HOSTED_SERVICE
       dpws_updatemetadata_WSDL (device)
       || dpws_update_Metadata_hosting (device)
#else
       dpws_updatemetadata_WSDL (device)
#endif
    )
    {
      return WS4D_ERR;
    }

#ifndef DPWS_HOSTED_SERVICE

#endif

  return WS4D_OK;
}
#endif

#if defined(DEVPROF_2009_01)
int
wsm__GetMetadata (struct soap *soap,
                  char *wsm__Dialect,
                  char *wsm__Identifier,
                  struct _wsm__Metadata *_GetMetadataResponseMsg)
{
#ifdef DPWS_DEVICE
  return mex_process_GetMetadata (soap, wsa_header_get_To (soap),
                                  (const char *) wsm__Dialect,
                                  (const char *) wsm__Identifier,
                                  _GetMetadataResponseMsg);
#else
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (wsm__Dialect);
  WS4D_UNUSED_PARAM (wsm__Identifier);
  WS4D_UNUSED_PARAM (_GetMetadataResponseMsg);

  return SOAP_ERR;
#endif
}
#endif

#if defined(DEVPROF_2006_02) || defined(DEVPROF_2009_01)
int
__wst__Get (struct soap *soap, void *emptyMessage,
            struct _wsm__Metadata *_GetResponseMsg)
{
#ifdef DPWS_DEVICE
  WS4D_UNUSED_PARAM (emptyMessage);

  return mex_process_TransferGet (soap, (char *) wsa_header_get_To (soap),
                                  _GetResponseMsg);
#else
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (emptyMessage);
  WS4D_UNUSED_PARAM (_GetResponseMsg);

  return SOAP_ERR;
#endif
}
#endif

#ifdef DPWS_CLIENT
#include "ws4d_servicecache.h"

int
dpws_find_cached_serviceById (struct dpws_s *dpws, struct ws4d_epr *device,
                              const char *id, struct ws4d_epr *result)
{
  int ret;

  /* test parameters */
  ws4d_assert (dpws && device && id && result, WS4D_ERR);

  ws4d_servicecache_lock (device);
  ret = ws4d_sc_get_servicebyId (device, id, result);
  ws4d_servicecache_unlock (device);

  return ret;
}

int
dpws_find_cached_serviceByAddr (struct dpws_s *dpws, struct ws4d_epr *device,
                                const char *Addr, struct ws4d_epr *result)
{
  int ret;

  /* test parameters */
  ws4d_assert (dpws && device && Addr && result, WS4D_ERR);

  ws4d_servicecache_lock (device);
  ret = ws4d_sc_get_servicebyAddr (device, Addr, result);
  ws4d_servicecache_unlock (device);

  return ret;
}

int
dpws_find_cached_services (struct dpws_s *dpws, struct ws4d_epr *device,
                           ws4d_qnamelist * types,
                           struct ws4d_abs_eprlist *result)
{
  int ret;

  /* test parameters */
  ws4d_assert (dpws && device && result, WS4D_ERR);

  ws4d_servicecache_lock (device);
  ret = ws4d_sc_get_servicesByType2 (device, types, result);
  ws4d_servicecache_unlock (device);

  return ret;
}

#ifdef DEVPROF_2009_01
#define HOSTED_SERVICE_TYPE wsdp__HostedServiceType
#else
#define HOSTED_SERVICE_TYPE wsdp__HostServiceType
#endif
/**
 * Function analyses the relationship section of a received
 * GetMetadata response.
 *
 * @param device
 * @param Relationship
 *
 * @return
 */
static int
dpws_analyse_Relationship (struct ws4d_epr *device, void *metadata)
{
  int i = 0;
  struct _wsm__MetadataSection *relationship_section = metadata;
  struct HOSTED_SERVICE_TYPE *service = NULL;
  struct _wsdp__Relationship *Relationship;

  /* test parameters */
  ws4d_assert (device && relationship_section, WS4D_ERR);

  Relationship = relationship_section->_content;
  ws4d_assert (Relationship, WS4D_ERR);

  if (Relationship->__sizeHosted < 1)
    {
      return WS4D_OK;
    }

  for (i = 0, service = Relationship->wsdp__Hosted;
       (i < Relationship->__sizeHosted) && service; i++, service++)
    {
      char *Id = service->wsdp__ServiceId;

      ws4d_assert (Id && service->wsa__EndpointReference.Address, WS4D_ERR);

      ws4d_servicecache_lock (device);

      if (ws4d_sc_check_service (device, Id) != WS4D_INVALID)
        {

          if (service->wsa__EndpointReference.Address)
            {
              ws4d_sc_set_Addrs (device, Id,
                                 (const char *)
                                 service->wsa__EndpointReference.Address);
            }

          if (service->wsdp__Types)
            {
              ws4d_sc_set_Types (device, Id,
                                 (const char *) service->wsdp__Types);
            }
        }

      ws4d_servicecache_unlock (device);
    }

  return WS4D_OK;
}

int
dpws_device_metadata_get (struct dpws_s *dpws, struct soap *soap,
                          struct ws4d_epr *epr, void *metadata,
                          ws4d_time timeout)
{
  char MsgId[DPWS_MSGID_SIZE];
  int err;
  char *addr = NULL;

  /* test parameters */
  ws4d_assert (soap && epr, WS4D_ERR);

  addr = (char *) dpws_resolve_addr (dpws, epr, NULL, timeout);
  ws4d_fail (!addr, WS4D_ERR);

  err = mex_init (soap);
  ws4d_fail (err, WS4D_ERR);

  mex_register_metadata_sectiontype (soap, DPWS_MEX_MODEL, "wsdp:ThisModel",
                                     &thisModel_serializers);
  mex_register_metadata_sectiontype (soap, DPWS_MEX_DEVICE, "wsdp:ThisDevice",
                                     &thisDevice_serializers);
  mex_register_metadata_sectiontype (soap, DPWS_MEX_RELATIONSHIP,
                                     "wsdp:Relationship",
                                     &relationship_serializers);
  mex_register_metadata_sectiontype (soap, DPWS_MEX_WSDL, "-wsdl",
                                     &mex_literal_serializers);

  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);

  err = mex_TransferGet (soap, MsgId, addr, ws4d_epr_get_Addrs (epr),
                         metadata, timeout);
  ws4d_fail (err != SOAP_OK, WS4D_ERR);

  return WS4D_OK;
}


int
dpws_service_metadata_get (struct dpws_s *dpws, struct soap *soap,
                           struct ws4d_epr *epr, char *dialect,
                           char *idendifier, void *metadata,
                           ws4d_time timeout)
{
#ifdef DEVPROF_2009_01
  char MsgId[DPWS_MSGID_SIZE];
  int err;

  /* test parameters */
  ws4d_assert (soap && epr, WS4D_ERR);

  WS4D_UNUSED_PARAM (dpws);

  err = mex_init (soap);
  ws4d_fail (err, WS4D_ERR);

  mex_register_metadata_sectiontype (soap, DPWS_MEX_WSDL, "-wsdl",
                                     &mex_literal_serializers);

  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);

  err = mex_GetMetadata (soap, MsgId, ws4d_epr_get_Addrs (epr),
                         ws4d_epr_get_Addrs (epr), dialect, idendifier,
                         metadata, timeout);
  ws4d_fail (err != SOAP_OK, WS4D_ERR);

  return WS4D_OK;
#else
  WS4D_UNUSED_PARAM (dialect);
  WS4D_UNUSED_PARAM (idendifier);

  return dpws_device_metadata_get (dpws, soap, epr, metadata, timeout);
#endif
}


static struct _wsm__MetadataSection *
dpws_metadata_filter (struct _wsm__Metadata
                      *metadata, const char *dialect, int *iter)
{
  if (dialect && (iter) && (metadata)
      && (metadata->__sizeMetadataSection > 0))
    {
      struct _wsm__MetadataSection *MetadataSection = NULL;
      while (*iter < metadata->__sizeMetadataSection)
        {
          MetadataSection = &metadata->wsm__MetadataSection[*iter];
          if (MetadataSection && MetadataSection->Dialect
              && !strcmp (MetadataSection->Dialect, dialect))
            {
              (*iter)++;
              return MetadataSection;
            }
          (*iter)++;
        }
    }
  return NULL;
}
#endif

int
ws4d_thisdevice_copy (struct ws4d_thisDevice *dst,
                      const struct ws4d_thisDevice *src,
                      struct ws4d_abs_allocator *alist)
{
  int err, i;

  /* test parameters */
  ws4d_assert (dst && src && alist, WS4D_ERR);

  memset (dst, 0, sizeof (struct ws4d_thisDevice));

  err = ws4d_strdup2 (src->FirmwareVersion, &dst->FirmwareVersion, alist);
  ws4d_fail (err != WS4D_OK, err);

  err = ws4d_strdup2 (src->SerialNumber, &dst->SerialNumber, alist);
  ws4d_fail (err != WS4D_OK, err);

  if (src->__sizeFriendlyName > 0)
    {
      dst->__sizeFriendlyName = src->__sizeFriendlyName;
      dst->FriendlyName = ws4d_locstring_alloc(src->__sizeFriendlyName, alist);

      err = ws4d_locstring_copy (dst->FriendlyName, dst->__sizeFriendlyName, src->FriendlyName, src->__sizeFriendlyName, alist);
      ws4d_fail (err != WS4D_OK, err);
    }

  if (src->__any)
    {
      dst->__any = ws4d_malloc_alist (sizeof (char *) * src->__size, alist);
      dst->__size = src->__size;
      for (i = 0; i < dst->__size; i++)
      {
          dst->__any[i] = ws4d_strdup (src->__any[i], alist);
      }
    }

  return WS4D_OK;
}

#ifdef DPWS_CLIENT
int
dpws_metadata_getThisDevice (void *metadata,
                             struct ws4d_abs_allocator *alist,
                             struct ws4d_thisDevice *result)
{
  struct _wsm__MetadataSection *device_section = NULL;
  int i = 0;

  /* test parameters */
  ws4d_assert (metadata && alist && result, WS4D_ERR);

  if ((device_section = dpws_metadata_filter (metadata, DPWS_MEX_DEVICE, &i)))
    {
      return ws4d_thisdevice_copy (result, device_section->_content, alist);
    }

  return WS4D_ERR;
}
#endif

int
ws4d_thismodel_copy (struct ws4d_thisModel *dst,
                     const struct ws4d_thisModel *src,
                     struct ws4d_abs_allocator *alist)
{
  int err, i;

  /* test parameters */
  ws4d_assert (dst && src && alist, WS4D_ERR);

  memset (dst, 0, sizeof (struct ws4d_thisModel));

  err =
    ws4d_strdup2 (src->ManufacturerUrl, (const char **) &dst->ManufacturerUrl,
                  alist);
  ws4d_fail (err != WS4D_OK, err);

  err =
    ws4d_strdup2 (src->ModelNumber, (const char **) &dst->ModelNumber, alist);
  ws4d_fail (err != WS4D_OK, err);

  err = ws4d_strdup2 (src->ModelUrl, (const char **) &dst->ModelUrl, alist);
  ws4d_fail (err != WS4D_OK, err);

  err =
    ws4d_strdup2 (src->PresentationUrl, (const char **) &dst->PresentationUrl,
                  alist);
  ws4d_fail (err != WS4D_OK, err);

  if (src->__sizeManufacturer > 0)
    {
      dst->__sizeManufacturer = src->__sizeManufacturer;
      dst->Manufacturer = ws4d_locstring_alloc(src->__sizeManufacturer, alist);

      err = ws4d_locstring_copy(dst->Manufacturer, dst->__sizeManufacturer, src->Manufacturer, src->__sizeManufacturer, alist);
      ws4d_fail (err != WS4D_OK, err);
    }

  if (src->__sizeModelName)
    {
      dst->__sizeModelName = src->__sizeModelName;
      dst->ModelName = ws4d_locstring_alloc(src->__sizeModelName, alist);

      err = ws4d_locstring_copy(dst->ModelName, dst->__sizeModelName, src->ModelName, src->__sizeModelName, alist);
      ws4d_fail (err != WS4D_OK, err);
    }

  if (src->__any)
    {
      dst->__any = ws4d_malloc_alist (sizeof (char *) * src->__size, alist);
      dst->__size = src->__size;
      for (i = 0; i < dst->__size; i++)
      {
          dst->__any[i] = ws4d_strdup (src->__any[i], alist);
      }
    }

  return WS4D_OK;
}

#ifdef DPWS_CLIENT
int
dpws_metadata_getThisModel (void *metadata,
                            struct ws4d_abs_allocator *alist,
                            struct ws4d_thisModel *result)
{
  struct _wsm__MetadataSection *model_section = NULL;
  int i = 0;

  /* test parameters */
  ws4d_assert (metadata && alist && result, WS4D_ERR);

  if ((model_section = dpws_metadata_filter (metadata, DPWS_MEX_MODEL, &i)))
    {
      return ws4d_thismodel_copy (result, model_section->_content, alist);
    }

  return WS4D_ERR;
}

int
dpws_metadata_getWSDL (void *metadata, char **wsdl)
{
  struct _wsm__MetadataSection *wsdl_section = NULL;
  int i = 0;

  /* test parameters */
  ws4d_assert (metadata && wsdl, WS4D_ERR);

  if ((wsdl_section = dpws_metadata_filter (metadata, DPWS_MEX_WSDL, &i)))
    {
      *wsdl = wsdl_section->_content;
      return WS4D_OK;
    }

  return WS4D_ERR;
}

int
dpws_metadata_cacheservices (struct dpws_s *dpws, struct ws4d_epr *device,
                             void *metadata)
{
  struct _wsm__MetadataSection *relationship_section = NULL;
  int i = 0, err;

  WS4D_UNUSED_PARAM (dpws);

  relationship_section = dpws_metadata_filter (metadata,
                                               DPWS_MEX_RELATIONSHIP, &i);
  if (relationship_section)
    {
      ws4d_register_sc (device);

      err = dpws_analyse_Relationship (device, relationship_section);
      if (err)
        return err;

      do
        {
          relationship_section = dpws_metadata_filter (metadata,
                                                       DPWS_MEX_RELATIONSHIP,
                                                       &i);
          if (relationship_section)
            {
              err = dpws_analyse_Relationship (device, relationship_section);
              if (err)
                return err;
            }

        }
      while (relationship_section);
    }

  return WS4D_OK;
}

int
dpws_find_services2 (struct dpws_s *dpws, struct soap *handle,
                     struct ws4d_epr *device, ws4d_qnamelist * types,
                     ws4d_time timeout, struct ws4d_abs_eprlist *result)
{
  int ret;
  struct _wsm__Metadata metadata;

  ret = dpws_device_metadata_get (dpws, handle, device, &metadata, timeout);
  ws4d_fail (ret != WS4D_OK, ret);

  ret = dpws_metadata_cacheservices (dpws, device, &metadata);
  ws4d_fail (ret != WS4D_OK, ret);

  return dpws_find_cached_services (dpws, device, types, result);
}

int
dpws_find_services (struct dpws_s *dpws, struct ws4d_epr *device,
                    ws4d_qnamelist * types, ws4d_time timeout,
                    struct ws4d_abs_eprlist *result)
{
  int ret;
  struct soap handle;

  soap_init (&handle);
#ifdef DEBUG
  soap_set_omode (&handle, SOAP_XML_INDENT);
#endif

  ret = dpws_find_services2 (dpws, &handle, device, types, timeout, result);
  if (ret != WS4D_OK)
    {
      soap_print_fault (&handle, stderr);
    }

  soap_end (&handle);
  soap_done (&handle);

  return ret;
}

int
dpws_find_service2 (struct dpws_s *dpws, struct soap *handle,
                    struct ws4d_epr *device, const char *id,
                    ws4d_time timeout, struct ws4d_epr *result)
{
  int ret;
  struct _wsm__Metadata metadata;

  ret = dpws_device_metadata_get (dpws, handle, device, &metadata, timeout);
  ws4d_fail (ret != WS4D_OK, ret);

  ret = dpws_metadata_cacheservices (dpws, device, &metadata);
  ws4d_fail (ret != WS4D_OK, ret);

  return dpws_find_cached_serviceById (dpws, device, id, result);
}

int
dpws_find_service (struct dpws_s *dpws, struct ws4d_epr *device,
                   const char *id, ws4d_time timeout, struct ws4d_epr *result)
{
  int ret;
  struct soap handle;

  soap_init (&handle);
#ifdef DEBUG
  soap_set_omode (&handle, SOAP_XML_INDENT);
#endif

  ret = dpws_find_service2 (dpws, &handle, device, id, timeout, result);
  if (ret != WS4D_OK)
    {
      soap_print_fault (&handle, stderr);
    }

  soap_end (&handle);
  soap_done (&handle);

  return ret;
}

#endif

const char *dpws_deliveryMode_push = DPWS_DELIVERYMODE_PUSH;
const char *dpws_filterType_action = DPWS_FILTERTYPE_ACTION;

#ifdef DPWS_DEVICE

struct dpws_deliveryMode_push_data
{
  char *Address;
  char *Identifier;
};

#define DPWS_SUBSCRIPTION_SERVICE "wse_subscription_manager"

int
dpws_header_gen_wseIdentifier (struct soap *soap, struct dpws_s *device,
                               struct ws4d_subscription *subs)
{
  char *identifier = NULL;

  identifier = dpws_subsm_get_deliveryPush_identifier (device, subs);
  return wse_header_set_Identifier (soap, identifier);
}

static int
dpws_processDelivery_push (struct ws4d_subscription *subs, void *data)
{
  struct wse__DeliveryType *delivery = data;

  /* test parameters */
  ws4d_assert (subs && delivery && delivery->wse__NotifyTo.Address, SOAP_ERR);

  if (delivery && delivery->wse__NotifyTo.Address)
    {
      struct dpws_deliveryMode_push_data *subs_data = NULL;

      subs->delivery_mode->DeliveryMode = dpws_deliveryMode_push;

      subs_data =
        ws4d_malloc_alist (sizeof (struct dpws_deliveryMode_push_data),
                           ws4d_subs_get_alist (subs));
      subs_data->Address =
        ws4d_strdup (delivery->wse__NotifyTo.Address,
                     ws4d_subs_get_alist (subs));
      subs_data->Identifier =
        ws4d_strdup (wse_subs_get_id (&delivery->wse__NotifyTo),
                     ws4d_subs_get_alist (subs));

      subs->delivery_mode->data = subs_data;

      return SOAP_OK;
    }

  return SOAP_ERR;
}

static int
dpws_cleanDelivery_push (struct ws4d_subscription *subs)
{
  WS4D_UNUSED_PARAM (subs);

  return WS4D_OK;
}

static int
dpws_processFilterType_action (struct ws4d_subscription *subs, void *data)
{
  struct wse__FilterType *Filter = data;

  /* test parameters */
  ws4d_assert (subs && Filter && Filter->__mixed, SOAP_ERR);

  if (Filter)
    {
      struct ws4d_stringlist *filters =
        ws4d_malloc_alist (sizeof (struct ws4d_stringlist),
                           ws4d_subs_get_alist (subs));
      ws4d_fail (filters == NULL, SOAP_EOM);

      ws4d_stringlist_init3 (filters, Filter->__mixed);

      subs->filter_mode->FilterType = dpws_filterType_action;
      subs->filter_mode->data = filters;

      return SOAP_OK;
    }

  return SOAP_ERR;
}

static int
dpws_cleanFilterType_action (struct ws4d_subscription *subs)
{
  if (subs->filter_mode && subs->filter_mode->data)
    {
      struct ws4d_stringlist *filters = subs->filter_mode->data;

      ws4d_stringlist_done (filters);
      ws4d_free_alist (filters);
    }

  return SOAP_OK;
}

struct ws4d_subsmanager *
dpws_get_subsman (struct dpws_s *device)
{
  /* test parameters */
  ws4d_assert (device, NULL);

  return wse_subsm_get (device->subsman);
}

#define EVENTSOURCE_URI_LEN 255

int
dpws_activate_eventsource2 (struct dpws_s *device,
                            struct soap *evsrc,
                            struct ws4d_subsmanager_hooks_t *hooks)
{
  /* test parameters */
  ws4d_assert (device && evsrc, WS4D_ERR);

  if (device->hosting_handle)
    {
      device->subsman = device->hosting_handle;
    }
  else
    {
      device->subsman = evsrc;
    }

  if (!wse_subsm_get_plugindata (device->subsman))
    {
      struct ws4d_epr *subsserv = NULL;
      struct ws4d_subsmanager *subsman;
      char uri[EVENTSOURCE_URI_LEN];
      char *p_addrs = (char *) dpws_handle_get_paddrs (device->subsman);

      subsserv = dpws_service_init (device, DPWS_SUBSCRIPTION_SERVICE);
      memset (uri, 0, EVENTSOURCE_URI_LEN);
      memcpy (uri, p_addrs, strlen (p_addrs) + 1);

      if (dpws_service_bind (device, subsserv, device->subsman, uri,
                             EVENTSOURCE_URI_LEN, 100))
        {
          return WS4D_ERR;
        }

      wse_subsm_register_handle (device->subsman,
                                 ws4d_epr_get_Addrs (subsserv));

      subsman = dpws_get_subsman (device);

      ws4d_subsm_register_defeventdelivery (subsman,
                                            dpws_deliveryMode_push,
                                            dpws_processDelivery_push,
                                            dpws_cleanDelivery_push);

      ws4d_subsm_register_defeventfilter (subsman,
                                          dpws_filterType_action,
                                          dpws_processFilterType_action,
                                          dpws_cleanFilterType_action);

      if (hooks)
        {
          ws4d_subsm_replacehooks (subsman, hooks, NULL);
        }
    }

  if (!wse_evsrc_get_plugindata (evsrc))
    {
      wse_evsrc_register_handle (evsrc, device->subsman);
      return WS4D_OK;
    }

  return WS4D_ERR;
}

int
dpws_check_subscriptions (struct dpws_s *device)
{
  struct ws4d_subsmanager *subsman;
  int err;

  /* test parameters */
  ws4d_assert (device, WS4D_ERR);

  subsman = wse_subsm_get (device->subsman);
  ws4d_subsm_lock (subsman);
  err = ws4d_subsm_check_allsubs (subsman);
  ws4d_subsm_unlock (subsman);

  return err;
}

int
dpws_end_subscription (struct dpws_s *device, struct ws4d_subscription *subs,
                       const char *status, char *reason)
{
  char MsgId[DPWS_MSGID_SIZE];

  /* test parameters */
  ws4d_assert (device && device->hosting_handle, WS4D_ERR);

  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  return wse_subsm_end_subs (device->subsman, MsgId, subs, status, reason);
}

static struct ws4d_subscription *
dpws_subsm_get_nextelem_by_action (struct ws4d_subsmanager *subsman,
                                   struct ws4d_subscription *cur,
                                   const char *action)
{
  register struct ws4d_subscription *res = NULL;
  struct ws4d_stringlist actions;

  res =
    ws4d_list_prepare_entry (cur, &subsman->subs_list,
                             struct ws4d_subscription, list);
  if (res == NULL)
    {
      return NULL;
    }

  ws4d_stringlist_init3 (&actions, action);

  ws4d_list_foreach_continue (res, &subsman->subs_list,
                              struct ws4d_subscription, list)
  {
    if (ws4d_subsm_is_filter (res, dpws_filterType_action))
      {
        const char *this_action;
        void *it;

        ws4d_stringlist_foreach (this_action, it, &actions)
        {
          struct ws4d_stringlist *filter_list = res->filter_mode->data;
          if (ws4d_stringlist_contains (filter_list, this_action))
            {
              ws4d_stringlist_done (&actions);
              return res;
            }
        }
      }
  }

  ws4d_stringlist_done (&actions);
  return NULL;
}

struct ws4d_subscription *
dpws_subsm_get_next_by_action (struct dpws_s *device,
                               struct ws4d_subscription *cur,
                               const char *action)
{
  struct ws4d_subsmanager *subsman;

  /* test parameters */
  ws4d_assert (device, NULL);

  subsman = wse_subsm_get (device->subsman);
  return dpws_subsm_get_nextelem_by_action (subsman, cur, action);
}

struct ws4d_subscription *
dpws_subsm_get_first_by_action (struct dpws_s *device, const char *action)
{
  struct ws4d_subsmanager *subsman;

  /* test parameters */
  ws4d_assert (device, NULL);

  subsman = wse_subsm_get (device->subsman);
  return dpws_subsm_get_nextelem_by_action (subsman, NULL, action);
}

char *
dpws_subsm_get_deliveryPush_address (struct dpws_s *device,
                                     struct ws4d_subscription *subs)
{
  WS4D_UNUSED_PARAM (device);

  if (ws4d_subsm_is_delivery (subs, dpws_deliveryMode_push))
    {
      struct dpws_deliveryMode_push_data *subs_data =
        subs->delivery_mode->data;
      return subs_data->Address;
    }
  else
    return NULL;
}

char *
dpws_subsm_get_deliveryPush_identifier (struct dpws_s *device,
                                        struct ws4d_subscription *subs)
{
  WS4D_UNUSED_PARAM (device);

  if (ws4d_subsm_is_delivery (subs, dpws_deliveryMode_push))
    {
      struct dpws_deliveryMode_push_data *subs_data =
        subs->delivery_mode->data;
      return subs_data->Identifier;
    }
  else
    return NULL;
}
#endif

int
__wse__Subscribe (struct soap *soap, struct _wse__Subscribe *__req,
                  struct _wse__SubscribeResponse *_res)
{
  int err = SOAP_OK;
#ifdef DPWS_DEVICE
  struct ws4d_subscription *subs = NULL;
  const char *to = NULL;

  err = wse_process_subscribe_req (soap, __req, &subs);
  if (err != SOAP_OK)
    return SOAP_ERR;

  to = wsa_header_get_ReplyTo (soap);
  if (!to)
    {
      to = wsa_anonymousURI;
    }

  return wse_process_subscribe_res (soap, (char *) to, subs, _res);
#else
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (__req);
  WS4D_UNUSED_PARAM (_res);

  return err;
#endif
}

int
__wse__Renew (struct soap *soap, struct _wse__Renew *__req,
              struct _wse__RenewResponse *_res)
{
  int err = SOAP_OK;
#ifdef DPWS_DEVICE
  struct ws4d_subscription *subs = NULL;
  const char *to = NULL;

  err = wse_process_renew_req (soap, __req, &subs);
  if (err != SOAP_OK)
    return SOAP_ERR;

  to = wsa_header_get_ReplyTo (soap);
  if (!to)
    {
      to = (char *) wsa_anonymousURI;
    }

  return wse_process_renew_res (soap, (char *) to, subs, _res);
#else
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (__req);
  WS4D_UNUSED_PARAM (_res);

  return err;
#endif
}

int
__wse__GetStatus (struct soap *soap, struct _wse__GetStatus *__req,
                  struct _wse__GetStatusResponse *_res)
{
  int err = SOAP_OK;
#ifdef DPWS_DEVICE
  struct ws4d_subscription *subs = NULL;
  const char *to = NULL;

  err = wse_process_getstatus_req (soap, __req, &subs);
  if (err != SOAP_OK)
    return SOAP_ERR;

  to = wsa_header_get_ReplyTo (soap);
  if (!to)
    {
      to = (char *) wsa_anonymousURI;
    }

  return wse_process_getstatus_res (soap, (char *) to, subs, _res);
#else
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (__req);
  WS4D_UNUSED_PARAM (_res);

  return err;
#endif
}

int
__wse__Unsubscribe (struct soap *soap,
                    struct _wse__Unsubscribe *wse__Unsubscribe,
                    struct __wse__UnsubscribeResponse *_param_1)
{
  int err = SOAP_OK;

#ifdef DPWS_DEVICE
  const char *to = NULL;

  WS4D_UNUSED_PARAM (_param_1);

  err = wse_process_unsubscribe_req (soap, wse__Unsubscribe);
  if (err != SOAP_OK)
    return SOAP_ERR;

  to = wsa_header_get_ReplyTo (soap);
  if (!to)
    {
      to = (char *) wsa_anonymousURI;
    }

  return wse_process_unsubscribe_res (soap, (char *) to);
#else
  WS4D_UNUSED_PARAM (_param_1);
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (wse__Unsubscribe);

  return err;
#endif
}

int
__wse__SubscriptionEnd (struct soap *soap, struct _wse__SubscriptionEnd *_req)
{
#ifdef DPWS_CLIENT
  return wse_process_subscriptionend (soap, _req);
#else
  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (_req);

  return SOAP_OK;
#endif
}

#ifdef DPWS_CLIENT

struct ws4d_delivery_type *
dpws_gen_delivery_push (struct soap *soap, const char *notifyTo)
{
  struct ws4d_delivery_type *result = NULL;
  struct wse__DeliveryType *data = NULL;

  result = (struct ws4d_delivery_type *)
    soap_malloc (soap, sizeof (struct ws4d_delivery_type));
  if (!result)
    return NULL;

  data = (struct wse__DeliveryType *)
    soap_malloc (soap, sizeof (struct wse__DeliveryType));
  if (!data)
    return NULL;

  soap_default_wse__DeliveryType (soap, data);

  result->data = data;
  result->DeliveryMode = (char *) dpws_deliveryMode_push;

  data->Mode = (char *) dpws_deliveryMode_push;
  data->wse__NotifyTo.Address = soap_strdup (soap, notifyTo);

  return result;
}

struct ws4d_filter_type *
dpws_gen_filter_action (struct soap *soap, const char *action_list)
{
  struct ws4d_filter_type *result = NULL;
  struct wse__FilterType *data = NULL;

  result = (struct ws4d_filter_type *)
    soap_malloc (soap, sizeof (struct ws4d_filter_type));
  if (!result)
    return NULL;

  data = (struct wse__FilterType *)
    soap_malloc (soap, sizeof (struct wse__FilterType));
  if (!data)
    return NULL;

  soap_default_wse__FilterType (soap, data);

  result->data = data;
  result->FilterType = (char *) dpws_filterType_action;

  data->Dialect = (char *) dpws_filterType_action;

  data->__sizeftype = 1;
  data->__ftype = soap_malloc (soap, sizeof (struct wse__FilterAnyType));
  data->__ftype[0].__any = soap_strdup (soap, action_list);
  data->__ftype[0].__type = SOAP_TYPE_string;

  return result;
}

const char *
dpws_subscribe (struct soap *soap, struct ws4d_epr *service,
                const char *endToAddress, ws4d_time * expires,
                struct ws4d_delivery_type *delivery,
                struct ws4d_filter_type *filter)
{
  char MsgId[DPWS_MSGID_SIZE];
  struct wsa__EndpointReferenceType *subsman;
  struct ws4d_subscription *subs;
  const char *subsid;
  struct ws4d_dur dur;

  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);

  ws4d_s_to_dur (*expires, &dur);
  subsman =
    wse_subscribe (soap, MsgId, ws4d_epr_get_Addrs (service), endToAddress,
                   delivery->data, &dur, filter->data);

  subsid = wse_subs_get_id (subsman);
  ws4d_fail ((subsid == NULL), NULL);

  subs = ws4d_subsproxy_addsubs (service, subsid);
  ws4d_dur_to_s (&dur, expires);
  *expires += ws4d_systime_s ();
  ws4d_subs_set_expires (subs, *expires);
  ws4d_subs_set_subsm (subs, subsman->Address);

  return ws4d_subs_get_id (subs);
}

int
dpws_subs_renew2 (struct soap *soap, const char *subsman, const char *id,
                  ws4d_time * expires)
{
  int err;
  char MsgId[DPWS_MSGID_SIZE];
  struct ws4d_dur dur;

  ws4d_assert (expires, WS4D_ERR);

  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);
  ws4d_s_to_dur (*expires, &dur);

  err = wse_subs_renew (soap, MsgId, subsman, id, &dur);
  if (err)
    return err;

  err = ws4d_dur_to_s (&dur, expires);

  return err;
}

int
dpws_subs_renew (struct soap *soap, struct ws4d_epr *service, const char *id,
                 ws4d_time * expires)
{
  int err;
  struct ws4d_subscription *subs;

  subs = ws4d_subsproxy_getsubs (service, id);
  if (!subs)
    return WS4D_ERR;

  err = dpws_subs_renew2 (soap, ws4d_subs_get_subsm (subs), id, expires);
  if (err)
    return err;

  return ws4d_subs_set_expires (subs, *expires);
}

int
dpws_subs_unsubscribe2 (struct soap *soap, const char *subsman,
                        const char *id)
{
  char MsgId[DPWS_MSGID_SIZE];

  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);

  return wse_subs_unsubscribe (soap, MsgId, subsman, id);
}

int
dpws_subs_unsubscribe (struct soap *soap, struct ws4d_epr *service,
                       const char *id)
{
  int err;
  struct ws4d_subscription *subs;

  subs = ws4d_subsproxy_getsubs (service, id);
  if (!subs)
    return WS4D_ERR;

  err = dpws_subs_unsubscribe2 (soap, ws4d_subs_get_subsm (subs), id);
  if (err)
    return err;

  return ws4d_subsproxy_delsubs (service, id);
}

int
dpws_subs_get_status2 (struct soap *soap, const char *subsman, const char *id,
                       ws4d_time * expires)
{
  int err;
  struct ws4d_dur *res = NULL;
  char MsgId[DPWS_MSGID_SIZE];

  dpws_header_gen_MessageId (MsgId, DPWS_MSGID_SIZE);

  res = wse_subs_get_status (soap, MsgId, subsman, id);
  ws4d_fail (res == NULL, WS4D_ERR);

  err = ws4d_dur_to_s (res, expires);

  *expires += ws4d_systime_s ();

  return err;
}

int
dpws_subs_get_status (struct soap *soap, struct ws4d_epr *service,
                      const char *id, ws4d_time * expires)
{
  int err;
  struct ws4d_subscription *subs;

  ws4d_assert (service, WS4D_ERR);

  subs = ws4d_subsproxy_getsubs (service, id);
  ws4d_fail (subs == NULL, WS4D_ERR);

  err = dpws_subs_get_status2 (soap, ws4d_subs_get_subsm (subs), id, expires);
  ws4d_fail (err != WS4D_OK, err);

  return ws4d_subs_set_expires (subs, *expires);
}
#endif

/** @} */
