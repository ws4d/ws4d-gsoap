/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "stdsoap2.h"

#include <stdarg.h>

#include "dpwsH.h"
#include "soap_misc.h"

#include "ws4d_localizedstring.h"


int
soap_header_new (struct soap *soap, size_t size)
{
  if (!soap || (size < 1))
    return SOAP_ERR;

  soap->header = (struct SOAP_ENV__Header *) soap_malloc (soap, size);

  if (soap->header)
    {
      memset (soap->header, 0, size);
      return SOAP_OK;
    }
  else
    {
      return soap->error;
    }
}

struct Namespace *
soap_extend_namespaces (struct Namespace *orig_namespaces,
                        struct Namespace *extns,
                        struct ws4d_abs_allocator *alist)
{
  register struct Namespace *entry, *ext_entry;
  struct Namespace *result;
  int old_size = 0, ext_size = 0, new_size = 0, count;

  /* test parameters */
  ws4d_assert (extns && orig_namespaces && alist, NULL);

  /* count entries in orig_namespaces */
  for (entry = orig_namespaces; entry && entry->id; entry++)
    {
      old_size++;
      new_size++;
    }

  /* count entries in extns */
  for (entry = extns; entry && entry->id; entry++)
    {
      int duplicate = 0;
      register struct Namespace *old_entry = NULL;

      /* check for duplicates */
      for (old_entry = orig_namespaces; old_entry && old_entry->id
           && old_entry->ns; old_entry++)
        {
          if (!strcmp (old_entry->ns, entry->ns) && !strcmp (old_entry->id,
                                                             entry->id))
            {
              duplicate = 1;
              break;
            }
        }

      if (!duplicate)
        {
          new_size++;
        }

      ext_size++;
    }

  /* check if extns is really an extension */
  if (new_size > old_size)
    {
      /* allocate new memory */
      result = ws4d_malloc_alist (sizeof (struct Namespace) * (new_size + 1),
                                  alist);
      ws4d_fail (result == NULL, NULL);

      result[new_size].id = NULL;
      result[new_size].ns = NULL;
    }
  else
    {
      /* no extension - return NULL */
      return NULL;
    }

  /* copy entries from orig_namespace */
  for (count = 0, entry = result; count < old_size; count++)
    {
      entry->id = ws4d_strdup (orig_namespaces[count].id, alist);
      entry->ns = ws4d_strdup (orig_namespaces[count].ns, alist);
      entry++;
    }

  /* copy entries from extns but skip duplicates */
  for (count = 0, ext_entry = extns;
       ext_entry && ext_entry->ns && ext_entry->id && (count < ext_size);
       count++, ext_entry++, entry++)
    {
      int duplicate;

      do
        {
          register struct Namespace *old_entry;

          duplicate = 0;
          for (old_entry = orig_namespaces; old_entry && old_entry->id
               && old_entry->ns; old_entry++)
            {
              if (!strcmp (old_entry->ns, ext_entry->ns)
                  && !strcmp (old_entry->id, ext_entry->id))
                {
                  duplicate = 1;
                  ext_entry++;
                  break;
                }
            }
        }
      while (ext_entry && (duplicate == 1));

      entry->id = ws4d_strdup (ext_entry->id, alist);
      entry->ns = ws4d_strdup (ext_entry->ns, alist);
    }

  return result;
}

int
soap_free_namespaces (struct Namespace *_namespaces,
                      struct ws4d_abs_allocator *alist)
{
  register struct Namespace *entry;

  /* test parameters */
  ws4d_assert (_namespaces && alist, NULL);

  for (entry = _namespaces; entry && entry->id && entry->ns; entry++)
    {
      ws4d_allocator_free(alist, (void *) entry->id);
      ws4d_allocator_free(alist, (void *) entry->ns);
    }

  ws4d_allocator_free(alist, _namespaces);

  return WS4D_OK;
}

#ifndef WITH_NOIO
void *
soap_getpeer (struct soap *soap)
{
  return &soap->peer;
}

size_t
soap_getpeerlen (struct soap * soap)
{
  return soap->peerlen;
}
#endif

char *
soap_gen_prefix (int *num, struct ws4d_abs_allocator *alist,
                 struct Namespace *prefix_namespaces)
{
  char prefix[255];
  int collision = 0;

  /* test parameters */
  ws4d_assert (alist && prefix_namespaces, NULL);

  do
    {
      struct Namespace *cur_ns = prefix_namespaces;
      collision = 0;

      *num += 1;
      SNPRINTF (prefix, 255, "n%d", *num);

      while (cur_ns->ns)
        {
          if (cur_ns->id)
            {
              if (!strcmp (cur_ns->id, prefix))
                {
                  collision = 1;
                }
            }
          cur_ns++;
        }

    }
  while (collision == 1);

  return ws4d_strdup (prefix, alist);
}

struct Namespace *
soap_qnamelist_namespaces (struct ws4d_qnamelist *head,
                           struct ws4d_abs_allocator *alist,
                           struct Namespace *qname_namespaces)
{
  register struct ws4d_qname *type, *next;
  register struct Namespace *result = NULL, *entry = NULL, *ns = NULL;
  int type_count = 0, prefix_count = 0;

  /* test parameters */
  ws4d_assert (head && alist, result);

  ws4d_qnamelist_foreach (type, next, head)
  {
    if (type->ns)
      {
        int found = 0;
        for (ns = qname_namespaces; ns && ns->id; ns++)
          {
            if (ns && ns->ns && !strcmp (type->ns, ns->ns))
              {
                found = 1;
                break;
              }
          }
        if (found == 0)
          {
            type_count++;
          }
      }
  }

  result = ws4d_malloc_alist (sizeof (struct Namespace) * (type_count + 1),
                              alist);
  ws4d_assert (result != NULL, NULL);
  memset (result, 0, sizeof (struct Namespace) * (type_count + 1));

  entry = result;
  ws4d_qnamelist_foreach (type, next, head)
  {
    if (type->ns)
      {
        int found = 0;
        for (ns = qname_namespaces; ns && ns->id; ns++)
          {
            if (ns && ns->ns && !strcmp (type->ns, ns->ns))
              {
                found = 1;
                break;
              }
          }
        if (found == 0)
          {
            if (type->prefix)
              {
                entry->id = ws4d_strdup(type->prefix, alist);
              }
            else
              {
                /* TODO: should fail somehow if no prefix can be generated */
                entry->id =
                  soap_gen_prefix (&prefix_count, alist, qname_namespaces);
              }
            entry->ns = ws4d_strdup(type->ns, alist);
            entry++;
          }
      }
  }

  return result;
}

struct Namespace *
soap_qnamelist_extend_namespaces (struct ws4d_qnamelist *qnamelist,
                                  const struct Namespace *_namespaces,
                                  struct ws4d_abs_allocator *alist)
{
  struct Namespace *type_namespaces, *result;

  /* test parameters */
  ws4d_assert (qnamelist && _namespaces && alist, NULL);

  type_namespaces = soap_qnamelist_namespaces (qnamelist, alist,
                                               (struct Namespace *) _namespaces);

  result = soap_extend_namespaces ((struct Namespace *) _namespaces,
                                   type_namespaces, alist);

  soap_free_namespaces(type_namespaces, alist);

  return result;
}

/******************************************************************************
 * gSOAP based allocator plugin                                               *
 ******************************************************************************/

#define SOAP_ALLOCATOR_ID "gSOAP allocator V 1.0"
const char *soap_allocator_id = SOAP_ALLOCATOR_ID;

#define soap_allocator_checkid(allocator) \
  ((allocator)->id && (((allocator)->id == soap_allocator_id)||(!strcmp((allocator)->id, soap_allocator_id))))

static void
soap_allocator_fdone (struct ws4d_abs_allocator *allocator)
{
  WS4D_UNUSED_PARAM (allocator);

  return;
}

static int
soap_allocator_ffreeall (struct ws4d_abs_allocator *allocator)
{
  WS4D_UNUSED_PARAM (allocator);

  return WS4D_OK;
}

static void *
soap_allocator_falloc (struct ws4d_abs_allocator *allocator, size_t size)
{
  struct soap *soap;
  void *result;

  /* test parameters */
  ws4d_assert (soap_allocator_checkid (allocator), NULL);
  soap = allocator->data;

  /* allocate memory */
  result = soap_malloc (soap, size);

  return memset(result, 0, size);
}

static int
soap_allocator_ffree (struct ws4d_abs_allocator *allocator, void *buf)
{
  struct soap *soap;

  /* test parameters */
  ws4d_assert (soap_allocator_checkid (allocator), WS4D_ERR);
  soap = allocator->data;

  /* free memory */
  soap_dealloc (soap, buf);
  return WS4D_OK;
}

static void *
soap_allocator_fmemdup (struct ws4d_abs_allocator *allocator, const void *buf,
                        size_t size)
{
  struct soap *soap;
  void *result;

  /* test parameters */
  ws4d_assert (soap_allocator_checkid (allocator) && (size > 0), NULL);
  soap = allocator->data;

  /* allocate memory */
  result = soap_malloc (soap, size);
  ws4d_fail (result == NULL, NULL);

  /* free memory */
  memcpy (result, buf, size);

  return result;
}

static struct ws4d_abs_allocator_cbs soap_allocator_cbs = {
  soap_allocator_fdone,
  soap_allocator_ffreeall,
  soap_allocator_falloc,
  soap_allocator_ffree,
  soap_allocator_fmemdup,
  soap_allocator_fmemdup,
  soap_allocator_ffree
};

int
soap_allocator_finit (struct ws4d_abs_allocator *allocator, void *arg)
{
  struct soap *soap = arg;

  /* test parameters */
  ws4d_assert (allocator && soap, WS4D_ERR);

  allocator->id = soap_allocator_id;
  allocator->callbacks = &soap_allocator_cbs;
  allocator->data = soap;

  return WS4D_OK;
}

/******************************************************************************
 * gSOAP allocator plugin                                                     *
 ******************************************************************************/

#define SOAP_ALLOCATOR_PLUGIN_ID "gSOAP allocator plugin V 1.0"
const char *soap_allocator_plugin_id = SOAP_ALLOCATOR_PLUGIN_ID;

struct soap_allocator_plugin_data
{
  struct ws4d_abs_allocator alist;
};

static void
soap_allocator_plugin_delete (struct soap *soap, struct soap_plugin *p)
{
  struct soap_allocator_plugin_data *data = p->data;

  WS4D_UNUSED_PARAM (soap);

  /* destroy plugin data */
  ws4d_allocator_done (&data->alist);

  /* free plugin data structure */
  ws4d_free (p->data);

  /* reset id */
  p->id = NULL;
}

static int
soap_allocator_plugin_init (struct soap *soap, struct soap_plugin *p, void *arg)
{
  struct soap_allocator_plugin_data *data;
  int ret;

  WS4D_UNUSED_PARAM (arg);

  /* allocate memory for plugin data */
  data = (void *) ws4d_malloc (sizeof (struct soap_allocator_plugin_data));
  ws4d_fail (data == NULL, SOAP_EOM);

  ret = SOAP_ALLOCLIST_INIT(&data->alist, soap);
  ws4d_fail(ret != WS4D_OK, ret);

  /* initialize plugin structure */
  p->id = soap_allocator_plugin_id;
  p->data = data;
  p->fdelete = soap_allocator_plugin_delete;

  return WS4D_OK;
}

struct ws4d_abs_allocator *soap_get_allocator(struct soap *soap)
{
  struct soap_allocator_plugin_data *data;
  int ret =
    soap_register_plugin_singleton (soap, soap_allocator_plugin_init,
                                    soap_allocator_plugin_id, (void **) &data);

  if ((ret == SOAP_OK) || (ret == SOAP_PLUGIN_DUPLICATE))
    {
      return &data->alist;
    }
  else
    {
      return NULL;
    }
}

/******************************************************************************
 * namepsace utilities                                                        *
 ******************************************************************************/

/* TODO: fix memory leak that may exist here */
void
soap_copy_local_namespaces (struct soap *src, struct soap *dst)
{
  if (src->local_namespaces && !dst->local_namespaces)
    {
      register const struct Namespace *ns1;
      register struct Namespace *ns2;
      register size_t n = 1;
      for (ns1 = src->local_namespaces; ns1->id; ns1++)
        n++;
      n *= sizeof (struct Namespace);
      ns2 = (struct Namespace *) SOAP_MALLOC (dst, n);
      if (ns2)
        {
          memcpy (ns2, src->local_namespaces, n);
          dst->local_namespaces = ns2;
        }
    }
}

const char *
soap_expand_QNames (struct soap *soap, const char *s)
{
  /* test parameters */
  ws4d_assert (soap && s, NULL);

  soap->labidx = 0;
  while (*s)
    {
      while ((*s > 0) && (*s <= 32))
        s++;
      if (!*s)
        {
          return "";
        }

      if (*s == '"')            /* already expanded QName */
        {
          const char *q;
          q = strchr (s, '"');
          if (!q)
            {
              /* This should never happen (invalid gSOAP escaping!) */
              return NULL;
            }
          while (*q > 32)
            {
              q++;
            }
          soap_append_lab (soap, s, q - s);
          s = q;
        }
      else                      /* expand QName */
        {
          struct Namespace *np;
          const char *ns = NULL;
          const char *q, *r = strchr (s, ':');
          size_t k = r - s;

          /* find namespace in local namespace table */
          for (np = soap->local_namespaces; np->id; np++)
            {
              if (!strncmp (np->id, s, k))
                {
                  ns = np->ns;
                  break;
                }
            }

          /* find namepsace in namespace stack */
          if (ns == NULL) {
            register struct soap_nlist *npl;

            for (npl = soap->nlist; npl->next != NULL; npl = npl->next)
            {
              if (!strncmp (npl->id, s, k))
                {
                  ns = npl->ns;
                  break;
                }
            }
          }

          soap_append_lab (soap, "\"", 1);
          soap_append_lab (soap, ns, strlen (ns));
          soap_append_lab (soap, "\":", 2);
          r++;
          q = r;
          while (*q > 32)
            {
              q++;
            }
          soap_append_lab (soap, r, q - r);
          s = q;
        }
      /* advance to next and add spacing */
      if (*s)
        {
          soap_append_lab (soap, " ", 1);
        }
    }
  soap_append_lab (soap, "\0", 1);

  return soap_strdup (soap, soap->labbuf);
}

/******************************************************************************
 * gSOAP plugin utilities                                                     *
 ******************************************************************************/

int
soap_register_plugin_singleton (struct soap *soap,
                                int (*fcreate) (struct soap *,
                                                struct soap_plugin *, void *),
                                const char *id, void **plugin)
{
  *plugin = soap_lookup_plugin (soap, id);
  if (*plugin == NULL)
    {
      int ret = soap_register_plugin (soap, fcreate);
      *plugin = soap_lookup_plugin (soap, id);
      return ret;
    }
  else
    {
      return SOAP_PLUGIN_DUPLICATE;
    }
}
