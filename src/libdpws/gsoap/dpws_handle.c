/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *  Created on: 12.05.2011
 *      Author: elmex
 */

#include "stddpws.h"

#define DPWS_SOAP_PLUGIN_ID "DPWS-Soap-Handle-PLUGIN-0.1"
const char *dpws_soap_plugin_id = DPWS_SOAP_PLUGIN_ID;

struct dpws_soap_s
{
  struct dpws_soap_s *copy_of;
  struct dpws_s *dpws;
  const char *transport;
  struct ws4d_stringlist paddrs;
  struct ws4d_abs_allocator alloc;
};
/* forward declarations */
INLINE struct dpws_soap_s *dpws_handle_get_plugindata (struct soap *soap);

int
dpws_handle_plugin_init (struct soap *soap, struct soap_plugin *p, void *arg);

static int
_dpws_handle_uri_isbound (struct dpws_s *dpws, struct soap *soap,
                          struct dpws_soap_s *data, struct ws4d_uri *uri,
                          struct ws4d_stringlist *matches);

static int
_dpws_handle_uri_dochecks (struct dpws_s *dpws, struct soap *soap,
                           struct dpws_soap_s *data, struct ws4d_uri *uri,
                           const char *transport);

/* implementation */

INLINE struct dpws_soap_s *
dpws_handle_get_plugindata (struct soap *soap)
{
  return (struct dpws_soap_s *) soap_lookup_plugin (soap,
                                                    dpws_soap_plugin_id);
}

static void
dpws_handle_plugin_delete (struct soap *soap, struct soap_plugin *p)
{
  struct dpws_soap_s *data = p->data;

  WS4D_UNUSED_PARAM (soap);

  /* destroy plugin data */
  ws4d_stringlist_done (&data->paddrs);
  ws4d_allocator_freeall(&data->alloc);
  ws4d_allocator_done (&data->alloc);

  /* free plugin data structure */
  ws4d_free (p->data);

  /* reset id */
  p->id = NULL;
}

static int
dpws_handle_plugin_copy (struct soap *soap, struct soap_plugin *dst,
                         struct soap_plugin *src)
{
  int err;
  struct dpws_soap_s *data, *src_data;

  /* test parameters */
  ws4d_assert (src && src->data, SOAP_ERR);
  src_data = src->data;

  /* initialize copy */
  err = dpws_handle_plugin_init (soap, dst, NULL);
  ws4d_fail (dst->data == NULL, SOAP_ERR);
  data = dst->data;

  /* create links to origin */
  data->copy_of = src->data;

  /* copy plugin content */
  data->dpws = src_data->dpws;
  err = ws4d_stringlist_addlist (&data->paddrs, &src_data->paddrs);
  ws4d_fail (err != SOAP_OK, SOAP_ERR);

  data->transport = src_data->transport;

  return SOAP_OK;
}

int
dpws_handle_plugin_init (struct soap *soap, struct soap_plugin *p, void *arg)
{
  struct dpws_soap_s *data;

  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (arg);

  /* allocate memory for plugin data */
  data = (void *) ws4d_malloc (sizeof (struct dpws_soap_s));
  ws4d_fail (data == NULL, WS4D_EOM);

  /* initialize plugin structure */
  p->id = dpws_soap_plugin_id;
  p->data = data;
  p->fcopy = dpws_handle_plugin_copy;
  p->fdelete = dpws_handle_plugin_delete;

  /* plugin data initialization */
  WS4D_ALLOCLIST_INIT (&data->alloc);
  ws4d_stringlist_init2 (&data->paddrs, &data->alloc);

  return SOAP_OK;
}

int
dpws_handle_init (struct dpws_s *dpws, struct soap *soap)
{
  int err;
  struct dpws_soap_s *handle_data;

  /* test parameters */
  soap_assert (soap, dpws && soap, SOAP_ERR);

  /* register soap handle as dpws serving handle */
  err =
    soap_register_plugin_singleton (soap, dpws_handle_plugin_init,
                                    dpws_soap_plugin_id,
                                    (void **) &handle_data);

  if (handle_data != NULL)
    {
      handle_data->dpws = dpws;
    }

  if ((err == SOAP_OK) || (err == SOAP_PLUGIN_DUPLICATE))
    {
      return SOAP_OK;
    }
  else
    {
      return err;
    }
}

static int
_dpws_handle_uri_dochecks (struct dpws_s *dpws, struct soap *soap,
                           struct dpws_soap_s *data, struct ws4d_uri *uri,
                           const char *transport)
{
  const char *host;
  void *it;

  WS4D_UNUSED_PARAM (soap);
  WS4D_UNUSED_PARAM (data);

  /* does the passed transport match ? */
  if (transport != NULL)
    {
      if (strncmp (transport, uri->scheme, strlen (uri->scheme)))
        {
          return 0;
        }
    }

  /* check if host place holder is used */
  if (!strcmp (ws4d_uri_get_host(uri), "host"))
    {
      return 1;
    }

  /* check uri host combinations */
  ws4d_stringlist_foreach (host, it, dpws_get_hosts(dpws))
  {
    /* check if host is the same as dpws->host */
    if (!strncmp (ws4d_uri_get_host(uri), host, strlen (host)))
      {
        return 1;
      }
  }

  return 0;
}

static int
_dpws_handle_uri_isbound (struct dpws_s *dpws, struct soap *soap,
                          struct dpws_soap_s *data, struct ws4d_uri *uri,
                          struct ws4d_stringlist *matches)
{
  const char *paddr;
  void *it;
  struct ws4d_uri paddr_uri;
  int does_match = 0;

  WS4D_UNUSED_PARAM (dpws);
  WS4D_UNUSED_PARAM (soap);

  ws4d_uri_init (&paddr_uri);

  /* try to find a match for each host */
  ws4d_stringlist_foreach (paddr, it, &data->paddrs)
  {
    int host_matches = 0, port_matches = 0;
    does_match = 0;

    ws4d_parse_uri (paddr, &paddr_uri, WITH_HOST | WITH_PORT);

    if (!strcmp (ws4d_uri_get_host(uri), "host"))
      {
        host_matches = 1;
      }
    else
      {
        if (!strcmp (ws4d_uri_get_host(uri), ws4d_uri_get_host(&paddr_uri)))
          {
            host_matches = 1;
          }
      }

    if (host_matches)
      {
        if (!strcmp (ws4d_uri_get_port(uri), "0"))
          {
            port_matches = 1;
          }
        else
          {
            if (!strcmp (ws4d_uri_get_port(uri), ws4d_uri_get_port(&paddr_uri)))
              {
                port_matches = 1;
              }
          }
      }

    if (host_matches && port_matches)
      {
        does_match = 1;

        ws4d_uri_set_scheme (&paddr_uri, ws4d_uri_get_scheme(uri));
        ws4d_uri_set_path (&paddr_uri, ws4d_uri_get_path(uri));
        ws4d_uri_set_fragment (&paddr_uri, ws4d_uri_get_fragment(uri));
        ws4d_uri_set_query (&paddr_uri, ws4d_uri_get_query(uri));

        ws4d_stringlist_adduri (matches, &paddr_uri);

      }
  }

  ws4d_uri_done (&paddr_uri);

  return does_match;
}

static int
_dpws_handle_dobind (struct dpws_s *dpws, struct soap *soap,
                     struct dpws_soap_s *data, struct ws4d_uri *uri,
                     int backlog, struct ws4d_stringlist *bindings)
{
  int result, err;
  struct sockaddr_in addr;
  SOAP_SOCKLEN_T addrlen = sizeof (struct sockaddr_in);
  const char *host;
  char port[6] = "";
  void *it;

  /* if no transport was set yet, set it */
  if (data->transport == NULL)
    {
      /* store transport */
      data->transport = ws4d_strndup (uri->scheme, strlen (uri->scheme),
                                      &data->alloc);

      if (!strncmp (uri->scheme, "soap.udp", 8))
        {
          soap_set_mode (soap, SOAP_IO_UDP);
        }
    }

  ws4d_stringlist_foreach (host, it, dpws_get_hosts(dpws))
  {
    result = soap_mbind (soap, host, (ws4d_uri_get_port(uri) ? atoi (ws4d_uri_get_port(uri)) : 0),
                         backlog);
    if (result == SOAP_INVALID_SOCKET)
      {
        soap_print_fault (soap, stderr);
        return WS4D_ERR;
      }

    /* retrieve port assigned by bind() */
    if ((ws4d_uri_get_port(uri) == NULL) || (!strcmp (ws4d_uri_get_port(uri), "0")))
      {

#ifdef WITH_IPV6
        if (ws4d_is_ipv4 (host))
          {
#endif
            /* get port number */
            err = getsockname (result, (struct sockaddr *) &addr, &addrlen);
            if (err == -1)
              {
                perror ("problem");
                return WS4D_ERR;
              }

            sprintf (port, "%d", ntohs (addr.sin_port));
#ifdef WITH_IPV6
          }
        else
          {
            struct sockaddr_in6 addr6;
            SOAP_SOCKLEN_T addr6len = sizeof (struct sockaddr_in6);

            err = getsockname (result, (struct sockaddr *) &addr6, &addr6len);
            if (err == -1)
              {
                perror ("problem");
                return WS4D_ERR;
              }

            sprintf (port, "%d", ntohs (addr6.sin6_port));
          }
#endif

        ws4d_uri_set_port (uri, port);
      }

    /* assign host */
    ws4d_uri_set_host (uri, host);

    /* add uri to bindings and to paddrs */
    ws4d_stringlist_adduri (bindings, uri);
    ws4d_stringlist_adduri (&data->paddrs, uri);
  }

  return WS4D_OK;
}


int
dpws_handle_bind (struct dpws_s *dpws, struct soap *soap, char *uri,
                  size_t size, int backlog)
{
  struct dpws_soap_s *data = dpws_handle_get_plugindata (soap);
  struct ws4d_uri uri_s;
  struct ws4d_stringlist uri_list, matches, result;
  const char *uri_item;
  int err;
  void *it;

  /* check if plugin is registered - if not do register */
  if (!data)
    {
      ws4d_fail (dpws_handle_init (dpws, soap) != SOAP_OK, WS4D_ERR);

      data = dpws_handle_get_plugindata (soap);
    }

  ws4d_stringlist_init3 (&uri_list, uri);
  ws4d_stringlist_init (&matches);
  ws4d_stringlist_init (&result);
  ws4d_uri_init (&uri_s);

  /* do checks for all uris */
  ws4d_stringlist_foreach (uri_item, it, &uri_list)
  {
    /* parse uri and do checks */
    if ((ws4d_parse_uri (uri_item, &uri_s, WITH_ALL) != WS4D_OK)
        || (!_dpws_handle_uri_dochecks (dpws, soap, data, &uri_s,
                data->transport)))
      {
        err = WS4D_ERR;
        goto error;
      }
  }

  /* do binding for all uris */
  ws4d_stringlist_foreach (uri_item, it, &uri_list)
  {

    /* parse uri */
    err = ws4d_parse_uri (uri_item, &uri_s, WITH_ALL);
    if (err != WS4D_OK)
      {
        goto error;
      }

    /* check if handle is already bound to this host or port */
    if (_dpws_handle_uri_isbound (dpws, soap, data, &uri_s, &matches) != 1)
      {
        /* do the binding */
        err =
          _dpws_handle_dobind (dpws, soap, data, &uri_s, backlog, &matches);
        if (err != WS4D_OK)
          {
            goto error;
          }
      }

    /* add matching bindings to result */
    err = ws4d_stringlist_addlist (&result, &matches);
    if (err != WS4D_OK)
      {
        goto error;
      }

    /* clear matches list */
    ws4d_stringlist_clear (&matches);
  }

  /* register ws-addressing module at handle */
  wsa_register_handle (soap);

  /* copy resulting uris to uri buffer */
  strncpy (uri, ws4d_stringlist_tostring (&result), size);

error:
  ws4d_uri_done (&uri_s);
  ws4d_stringlist_done (&uri_list);
  ws4d_stringlist_done (&matches);
  ws4d_stringlist_done (&result);

  return err;
}

const char *
dpws_handle_get_paddrs (struct soap *soap)
{
  struct dpws_soap_s *data = dpws_handle_get_plugindata (soap);

  /* test parameters */
  ws4d_assert (data, NULL);

  return ws4d_stringlist_tostring (&data->paddrs);
}

struct ws4d_abs_allocator *
dpws_handle_get_allocator (struct soap *soap)
{
  struct dpws_soap_s *data = dpws_handle_get_plugindata (soap);

  /* test parameters */
  ws4d_assert (data, NULL);

  return &data->alloc;
}

struct dpws_s *
dpws_handle_get_device (struct soap *soap)
{
  struct dpws_soap_s *data = dpws_handle_get_plugindata (soap);

  /* test parameters */
  ws4d_assert (data, NULL);

  return data->dpws;
}
