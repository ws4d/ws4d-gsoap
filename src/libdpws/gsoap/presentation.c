/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *  Created on: 16.05.2011
 *      Author: elmex
 */

#include "stddpws.h"
#include "ws4d_target.h"

struct dpws_pres_s
{
  const char *webroot;
  const char *url;
  const char *path;
  struct ws4d_abs_allocator alloc;
};

#define DPWS_PRES_PLUGIN_ID "DPWS-Presentation-PLUGIN-0.1"
const char *dpws_pres_plugin_id = DPWS_PRES_PLUGIN_ID;

/* forward declarations */

INLINE struct dpws_pres_s *dpws_pres_get_plugindata (struct soap *soap);

static int
dpws_pres_plugin_init (struct soap *soap, struct soap_plugin *p, void *arg);

static int
dpws_pres_plugin_copy (struct soap *soap, struct soap_plugin *dst,
                       struct soap_plugin *src);

static void dpws_pres_plugin_reset (struct soap *soap, struct soap_plugin *p);

static void
dpws_pres_plugin_delete (struct soap *soap, struct soap_plugin *p);

/* implementation */

INLINE struct dpws_pres_s *
dpws_pres_get_plugindata (struct soap *soap)
{
  return (struct dpws_pres_s *) soap_lookup_plugin (soap,
                                                    dpws_pres_plugin_id);
}

static int
dpws_pres_fget (struct soap *soap)
{
  struct dpws_pres_s *pdata = dpws_pres_get_plugindata (soap);
  FILE *fd = NULL;
  int len = strlen (soap->path) + strlen (pdata->webroot);
  char *path;
  size_t r;

  if (strncmp (pdata->path, soap->path, strlen (pdata->path)))
    {
      return 403;
    }

  path = soap_malloc (soap, len + 1);
  memset (path, 0, len + 1);

  strcat (path, pdata->webroot);
  strcat (path, soap->path + strlen (pdata->path));

  fd = fopen (path, "rb");      /* open file to copy */
  if (!fd)
    {
      return 404;               /* return HTTP not found */
    }

  if (!soap_tag_cmp (soap->path, "*.html"))
    {
      soap->http_content = "text/html";
    }

  if (!soap_tag_cmp (soap->path, "*.css"))
    {
      soap->http_content = "text/css";
    }

  if (!soap_tag_cmp (soap->path, "*.js"))
    {
      soap->http_content = "application/javascript";
    }

  if (!soap_tag_cmp (soap->path, "*.xml")
      || !soap_tag_cmp (soap->path, "*.xsd")
      || !soap_tag_cmp (soap->path, "*.wsdl"))
    {
      soap->http_content = "text/xml";
    }

  if (!soap_tag_cmp (soap->path, "*.jpg"))
    {
      soap->http_content = "image/jpeg";
    }

  if (!soap_tag_cmp (soap->path, "*.gif"))
    {
      soap->http_content = "image/gif";
    }

  if (!soap_tag_cmp (soap->path, "*.png"))
    {
      soap->http_content = "image/png";
    }

  if (!soap_tag_cmp (soap->path, "*.ico"))
    {
      soap->http_content = "image/ico";
    }

  if (soap_response (soap, SOAP_FILE))  /* OK HTTP response header */
    {
      soap_end_send (soap);
      fclose (fd);
      return soap->error;
    }

  for (;;)
    {
      r = fread (soap->tmpbuf, 1, sizeof (soap->tmpbuf), fd);
      if (!r)
        break;
      if (soap_send_raw (soap, soap->tmpbuf, r))
        {
          soap_end_send (soap);
          fclose (fd);
          return soap->error;
        }
    }

  fclose (fd);
  return soap_end_send (soap);
}

static int
dpws_pres_plugin_init (struct soap *soap, struct soap_plugin *p, void *arg)
{
  struct dpws_pres_s *data;

  WS4D_UNUSED_PARAM (arg);

  /* allocate memory for plugin data */
  data = (void *) ws4d_malloc (sizeof (struct dpws_pres_s));
  ws4d_fail (data == NULL, SOAP_EOM);

  /* initialize plugin structure */
  p->id = dpws_pres_plugin_id;
  p->data = data;
  p->fcopy = dpws_pres_plugin_copy;
  p->fdelete = dpws_pres_plugin_delete;

  /* set HTTP get handler */
  soap->fget = dpws_pres_fget;

  /* initialize plugin data */
  WS4D_ALLOCLIST_INIT (&data->alloc);

  return SOAP_OK;
}

static int
dpws_pres_plugin_copy (struct soap *soap, struct soap_plugin *dst,
                       struct soap_plugin *src)
{
  struct dpws_pres_s *data, *src_data;

  /* initialize and reset destination plugin */
  dpws_pres_plugin_init (soap, dst, NULL);
  dpws_pres_plugin_reset (soap, dst);

  data = dst->data;
  src_data = src->data;

  /* copy path */
  if (src_data->path)
    {
      ws4d_strdup2 (src_data->path, &data->path, &data->alloc);
    }

  /* copy url */
  if (src_data->url)
    {
      ws4d_strdup2 (src_data->url, &data->url, &data->alloc);
    }

  /* copy webroot */
  if (src_data->webroot)
    {
      ws4d_strdup2 (src_data->webroot, &data->webroot, &data->alloc);
    }

  return SOAP_OK;
}

static void
dpws_pres_plugin_reset (struct soap *soap, struct soap_plugin *p)
{
  struct dpws_pres_s *data = p->data;

  WS4D_UNUSED_PARAM (soap);

  ws4d_assert (data,);

  /* reset path */
  if (data->path)
    {
      ws4d_strdup2 (NULL, &data->path, NULL);
    }

  /* reset url */
  if (data->url)
    {
      ws4d_strdup2 (NULL, &data->url, NULL);
    }

  /* reset webroot */
  if (data->webroot)
    {
      ws4d_strdup2 (NULL, &data->webroot, NULL);
    }
}

static void
dpws_pres_plugin_delete (struct soap *soap, struct soap_plugin *p)
{
  struct dpws_pres_s *data = p->data;

  WS4D_UNUSED_PARAM (soap);

  /* destroy allocator */
  ws4d_allocator_done (&data->alloc);

  /* free plugin data structure */
  ws4d_free (p->data);

  /* reset id */
  p->id = NULL;
}

int
dpws_setup_presentation (struct dpws_s *dpws, const char *webroot,
                         const char *epoint)
{
  int err, len;
  struct dpws_pres_s *data;

  /* test parameters */
  ws4d_assert (dpws && dpws->hosting_handle && webroot && *webroot, WS4D_ERR);

  /* register soap handle as dpws serving handle */
  err =
    soap_register_plugin_singleton (dpws->hosting_handle,
                                    dpws_pres_plugin_init,
                                    dpws_pres_plugin_id, (void **) &data);

  if (data != NULL)
    {

      /* calculate addrlen */
      len = strlen (dpws_get_XAddrs (dpws)) + strlen (epoint);

      /* concat presentation url */
      data->url = ws4d_malloc_alist (len + 1, &data->alloc);
      strcat ((char *) data->url, dpws_get_XAddrs (dpws));
      strcat ((char *) data->url, (char *) epoint);

      /* calculate path len */
      len = strlen (dpws_get_UUID (dpws)) + strlen ("/");

      /* concat path */
      data->path = ws4d_malloc_alist (len + 1, &data->alloc);
      strcat ((char *) data->path, "/");
      strcat ((char *) data->path,
              dpws_get_UUID (dpws) + WS4D_UUID_SCHEMA_PREFIX_LEN);

      /* copy webroot */
      data->webroot = ws4d_strdup (webroot, &data->alloc);
    }

  return err;
}

const char *
dpws_presentation_getaddr (struct dpws_s *dpws)
{
  struct dpws_pres_s *pdata = dpws_pres_get_plugindata (dpws->hosting_handle);

  return pdata != NULL ? pdata->url : NULL;
}
