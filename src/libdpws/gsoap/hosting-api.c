/* lib hosting service - hosting service as library
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 01.06.2011
 *      Author: elmex
 */

#include "dpws.nsmap"
#include "stddpws.h"
#include "dpws_hosting.h"

static int
hs_soapinit (struct soap *soap, const struct Namespace *ns)
{
  int err;

  /* initialize soap handle */
  soap_init (soap);

  /* use standard ns table if no table was given */
  if (ns == NULL)
    {
      ns = dpws_namespaces;
    }

  /* set namespace table */
  err = soap_set_namespaces (soap, ns);
  ws4d_fail (err != SOAP_OK, WS4D_ERR);

#ifdef DEBUG
  soap_set_omode (soap, SOAP_XML_INDENT);
#endif

  return WS4D_OK;
}

int
hs_init (struct hosting_service *service, const char *uuid,
         const char *serviceID, const char *interf, int port, int backlog,
         int https, const struct Namespace *ns)
{
  return hs_init2 (service, hs_soapinit, uuid, serviceID, interf, port,
                   backlog, https, ns);
}

int
hs_init2 (struct hosting_service *service,
          hosting_service_soapinit_cb soap_init_cb, const char *uuid,
          const char *serviceID, const char *interf, int port, int backlog,
          int https, const struct Namespace *ns)
{
  int err;

  /* test parameters */
  ws4d_assert (service && soap_init_cb, WS4D_EPARAM);

  /* initialize dpws structure */
  err = dpws_init (&service->dpws, interf);
  ws4d_fail (err != WS4D_OK, err);

  /* initialize soap handle */
  err = soap_init_cb (&service->soap, ns);
  ws4d_fail (err != WS4D_OK, err);

  /* initialize hosting service */
  return dpws_add_hosting_service (&service->dpws, &service->soap, port,
                                   serviceID, uuid, backlog, https);
}

int
hs_done (struct hosting_service *service)
{
  int err;

  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  /* deactivate hosting service */
  dpws_deactivate_hosting_service (&service->dpws);

  /* destroy dpws handle */
  err = dpws_done (&service->dpws);
  ws4d_fail (err != WS4D_OK, err);

  /* destroy soap handle */
  soap_done (&service->soap);

  return WS4D_OK;
}

int
hs_addtypes (struct hosting_service *service, const char *types)
{
  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  return dpws_add_typestring (&service->dpws, types);
}

int
hs_removetypes (struct hosting_service *service, const char *types)
{
  WS4D_UNUSED_PARAM (types);

  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  /* TODO: to be implemented */
  return WS4D_ERR;
}

int
hs_addscopes (struct hosting_service *service, const char *scopes)
{
  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  return dpws_add_scope (&service->dpws, scopes);
}

int
hs_removescopes (struct hosting_service *service, const char *scopes)
{
  WS4D_UNUSED_PARAM (scopes);

  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  /* TODO: to be implemented */
  return WS4D_ERR;
}

struct ws4d_thisModel *
hs_metadata_change_thismodel (struct hosting_service *service)
{
  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  return dpws_change_thismodel (&service->dpws);
}

struct ws4d_thisDevice *
hs_metadata_change_thisdevice (struct hosting_service *service)
{
  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  return dpws_change_thisdevice (&service->dpws);
}

int
hs_metadata_update (struct hosting_service *service)
{
  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  return dpws_update_Metadata_hosting (&service->dpws);
}

int
hs_activate (struct hosting_service *service)
{
  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  return dpws_activate_hosting_service (&service->dpws);
}

int
hs_deactivate (struct hosting_service *service)
{
  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  return dpws_deactivate_hosting_service (&service->dpws);
}

int
hs_addservice (struct hosting_service *service, const char *serviceID,
               char *addrs, const char *types)
{
  int err;
  struct ws4d_epr *hosted_service = NULL;

  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  hosted_service = dpws_service_init (&service->dpws, serviceID);
  ws4d_fail (hosted_service == NULL, WS4D_ERR);

  err = dpws_service_add_typestring (hosted_service, types);
  ws4d_fail (err != WS4D_OK, err);

  return dpws_add_hosted_service (&service->dpws, hosted_service, addrs,
                                  DPWS_URI_MAX_LEN);
}

int
hs_removeservice (struct hosting_service *service, const char *serviceID)
{
  struct ws4d_epr *hosted_service = NULL;
  int err;

  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  hosted_service = dpws_get_hosted_service (&service->dpws, serviceID);
  ws4d_fail (hosted_service == NULL, WS4D_ERR);

  err = dpws_del_hosted_service (&service->dpws, hosted_service);
  ws4d_fail (err != WS4D_OK, err);

  return dpws_service_done (&service->dpws, hosted_service);
}

int
hs_dowork (struct hosting_service *service, ws4d_time timeout)
{
  struct soap *soap_set[] =
    SOAP_HANDLE_SET (service != NULL ? &service->soap : NULL);
  int (*serve_requests[]) (struct soap * soap) = SOAP_SERVE_SET (NULL);
  struct soap *handle = NULL;

  /* test parameters */
  ws4d_assert (service, WS4D_EPARAM);

  handle = dpws_maccept (&service->dpws, timeout, 1, soap_set);
  if (handle == NULL)
    {
      return WS4D_TO;
    }

  if (dpws_mserve (handle, 0, serve_requests))
    {
      soap_print_fault (handle, stderr);
    }

  /* clean up soaps internaly allocated memory */
  soap_end (handle);

  return WS4D_OK;
}
