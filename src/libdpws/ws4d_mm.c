/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *
 *  Created on: 25.05.2009
 *      Author: elmex
 */

#include "ws4d_misc.h"

/******************************************************************************
 * basic memory management                                                    *
 ******************************************************************************/

void *
ws4d_malloc (size_t length)
{
  void *p = NULL;
  p = WS4D_MALLOC (length);

  return memset (p, 0, length);
}

void
ws4d_free (void *p)
{
  if (p)
    {
      WS4D_FREE (p);
    }
}

char *
ws4d_strdup (const char *src, struct ws4d_abs_allocator *alist)
{
  char *res;
  size_t len;

  /* allow strdup(null) */
  if (src == NULL)
    {
      return NULL;
    }

  len = strlen (src);
  res = (char *) ws4d_allocator_memdup (alist, src, len + 1);
  if (res)
    {
      res[len] = '\0';
      return res;
    }

  return NULL;
}

char *
ws4d_strndup (const char *src, size_t size, struct ws4d_abs_allocator *alist)
{
  char *res;
  size_t len;

  ws4d_assert (src, NULL);

  len = strlen (src);
  if (len > size)
    {
      len = size;
    }
  res = (char *) ws4d_allocator_memdup (alist, src, len + 1);
  if (res)
    {
      res[len] = '\0';
      return res;
    }

  return NULL;
}

int
ws4d_strdup2 (const char *src, const char **dst,
              struct ws4d_abs_allocator *alist)
{
  ws4d_assert (dst, WS4D_ERR);

  ws4d_free_alist ((void *) *dst);

  if (src)
    {
      *dst = ws4d_strdup (src, alist);
      ws4d_assert (*dst, WS4D_EOM);
    }
  else
    {
      *dst = NULL;
    }

  return WS4D_OK;
}

int
ws4d_strndup2 (const char *src, size_t size, const char **dst,
               struct ws4d_abs_allocator *alist)
{
  ws4d_assert (dst, WS4D_ERR);

  if (*dst)
    {
      ws4d_free_alist ((void *) *dst);
      *dst = NULL;
    }

  if (src)
    {
      *dst = ws4d_strndup (src, size, alist);
      ws4d_assert (*dst, WS4D_EOM);
    }

  return WS4D_OK;
}

/******************************************************************************
 * abstract allocator                                                         *
 ******************************************************************************/

int
ws4d_allocator_init (struct ws4d_abs_allocator *allocator,
                     int (*finit) (struct ws4d_abs_allocator *
                                   allocator, void *arg), void *arg)
{
  int ret = 0;

  ws4d_assert (allocator && finit, WS4D_ERR);

  memset (allocator, 0, sizeof (struct ws4d_abs_allocator));
  ret = finit (allocator, arg);
  if (ret != WS4D_OK)
    return ret;

  allocator->copy = NULL;

  ws4d_assert (allocator->callbacks
               && allocator->callbacks->falloc
               && allocator->callbacks->fdone
               && allocator->callbacks->ffree
               && allocator->callbacks->ffreeall
               && allocator->callbacks->fmemdup
               && allocator->callbacks->fmemref
               && allocator->callbacks->funref, WS4D_ERR);

  return WS4D_OK;
}

void
ws4d_allocator_done (struct ws4d_abs_allocator *allocator)
{
  ws4d_assert (allocator && allocator->callbacks,);

  if (allocator->callbacks && allocator->callbacks->fdone)
    allocator->callbacks->fdone (allocator);

  if (allocator->copy != NULL)
    {
      ws4d_free (allocator->copy);
    }

  memset (allocator, 0, sizeof (struct ws4d_abs_allocator));
}

int
ws4d_allocator_freeall (struct ws4d_abs_allocator *allocator)
{
  ws4d_assert (allocator && allocator->callbacks
               && allocator->callbacks->ffreeall, WS4D_ERR);

  return allocator->callbacks->ffreeall (allocator);
}

void *
ws4d_allocator_alloc (struct ws4d_abs_allocator *allocator, size_t length)
{
  ws4d_assert (allocator && allocator->callbacks
               && allocator->callbacks->falloc, NULL);

  return allocator->callbacks->falloc (allocator, length);
}

int
ws4d_allocator_free (struct ws4d_abs_allocator *allocator, void *buf)
{
  ws4d_assert (allocator && allocator->callbacks
               && allocator->callbacks->ffree, WS4D_ERR);

  return allocator->callbacks->ffree (allocator, buf);
}

void *
ws4d_allocator_memdup (struct ws4d_abs_allocator *allocator, const void *src,
                       size_t length)
{
  ws4d_assert (allocator && allocator->callbacks
               && allocator->callbacks->fmemdup, NULL);

  return allocator->callbacks->fmemdup (allocator, src, length);
}

void *
ws4d_allocator_memref (struct ws4d_abs_allocator *allocator, const void *src,
                       size_t length)
{
  ws4d_assert (allocator && allocator->callbacks
               && allocator->callbacks->fmemref, NULL);

  return allocator->callbacks->fmemref (allocator, src, length);
}

int
ws4d_allocator_unref (struct ws4d_abs_allocator *allocator, void *src)
{
  ws4d_assert (allocator && allocator->callbacks
               && allocator->callbacks->funref, WS4D_ERR);

  return allocator->callbacks->funref (allocator, src);
}

/******************************************************************************
 * copy of allocator                                                          *
 ******************************************************************************/

#define WS4D_ALLOCATORCOPYPLUGIN_ID "WS4D allocator copy V 1.0"
const char *ws4d_allocatorcopy_plugin_id = WS4D_ALLOCATORCOPYPLUGIN_ID;

static void
ws4d_copyallocator_fdone (struct ws4d_abs_allocator *allocator)
{
  WS4D_UNUSED_PARAM (allocator);

  return;
}

static int
ws4d_copyallocator_ffreeall (struct ws4d_abs_allocator *allocator)
{
  struct ws4d_allocatorcopy_s *data =
    (struct ws4d_allocatorcopy_s *) allocator->data;

  return ws4d_allocator_freeall (data->copyof);
}

static void *
ws4d_copyallocator_falloc (struct ws4d_abs_allocator *allocator, size_t size)
{
  struct ws4d_allocatorcopy_s *data =
    (struct ws4d_allocatorcopy_s *) allocator->data;

  return ws4d_allocator_alloc (data->copyof, size);
}

static int
ws4d_copyallocator_ffree (struct ws4d_abs_allocator *allocator, void *buf)
{
  struct ws4d_allocatorcopy_s *data =
    (struct ws4d_allocatorcopy_s *) allocator->data;

  return ws4d_allocator_free (data->copyof, buf);
}

static void *
ws4d_copyallocator_fmemdup (struct ws4d_abs_allocator *allocator,
                            const void *buf, size_t size)
{
  struct ws4d_allocatorcopy_s *data =
    (struct ws4d_allocatorcopy_s *) allocator->data;

  return ws4d_allocator_memdup (data->copyof, buf, size);
}

static void *
ws4d_copyallocator_fmemref (struct ws4d_abs_allocator *allocator,
                            const void *buf, size_t size)
{
  struct ws4d_allocatorcopy_s *data =
    (struct ws4d_allocatorcopy_s *) allocator->data;

  return ws4d_allocator_memref (data->copyof, buf, size);
}

static int
ws4d_copyallocator_fmemunref (struct ws4d_abs_allocator *allocator, void *buf)
{
  struct ws4d_allocatorcopy_s *data =
    (struct ws4d_allocatorcopy_s *) allocator->data;

  return ws4d_allocator_unref (data->copyof, buf);
}

static struct ws4d_abs_allocator_cbs ws4d_copyallocator_cbs = {
  ws4d_copyallocator_fdone,
  ws4d_copyallocator_ffreeall,
  ws4d_copyallocator_falloc,
  ws4d_copyallocator_ffree,
  ws4d_copyallocator_fmemdup,
  ws4d_copyallocator_fmemref,
  ws4d_copyallocator_fmemunref
};

static int
ws4d_allocatorcopy_finit (struct ws4d_abs_allocator *dst, void *arg)
{
  struct ws4d_abs_allocator *src = arg;
  struct ws4d_allocatorcopy_s *data;

  /* test parameters */
  ws4d_assert (dst && src, WS4D_ERR);

  if (src->copy == NULL)
    {
      data = ws4d_malloc (sizeof (struct ws4d_allocatorcopy_s));
      ws4d_fail (data == NULL, WS4D_EOM);
      src->copy = data;
    }
  else
    {
      data = src->copy;
    }

  data->copyof = src;

  dst->id = ws4d_allocatorcopy_plugin_id;
  dst->data = data;
  dst->callbacks = &ws4d_copyallocator_cbs;

  return WS4D_OK;
}

int
ws4d_allocator_copy (struct ws4d_abs_allocator *src,
                     struct ws4d_abs_allocator *dst)
{
  return ws4d_allocator_init (dst, ws4d_allocatorcopy_finit, src);
}

int
ws4d_allocator_iscopy (struct ws4d_abs_allocator *allocator)
{
  return ((allocator != NULL)
          && (allocator->id == ws4d_allocatorcopy_plugin_id)) ? 1 : 0;
}

/******************************************************************************
 * list based allocator implementation                                        *
 ******************************************************************************/
#define WS4D_ALLOCATORPLUGIN_ID "WS4D list allocator V 1.0"
const char *ws4d_allocator_plugin_id = WS4D_ALLOCATORPLUGIN_ID;

typedef struct ws4d_alloc_list_item_s
{
  struct ws4d_list_node list;
  ws4d_alloc_list *alist;
  size_t size;
  void *start;
} ws4d_alloc_list_item;

static int
ws4d_alistallocator_ffreeall (struct ws4d_abs_allocator *allocator)
{
  ws4d_alloc_list *alist = allocator->data;
  register ws4d_alloc_list_item *item, *next;

  ws4d_assert (alist, WS4D_ERR);

  ws4d_mutex_lock (&alist->lock);
  ws4d_list_foreach (item, next, &alist->alist, ws4d_alloc_list_item, list)
  {
    ws4d_list_del (&item->list);
    memset (item, 0, item->size);
    ws4d_free (item);
  }
  ws4d_mutex_unlock (&alist->lock);

  return WS4D_OK;
}

static void
ws4d_alistallocator_fdone (struct ws4d_abs_allocator *allocator)
{
  ws4d_alloc_list *alist = allocator->data;
  if (alist)
    {
      ws4d_alistallocator_ffreeall (allocator);
      ws4d_mutex_destroy (&alist->lock);

      memset (alist, 0, sizeof (ws4d_alloc_list));
      ws4d_free (alist);
    }
}

static void *
ws4d_alistallocator_falloc (struct ws4d_abs_allocator *allocator, size_t size)
{
  ws4d_alloc_list *alist = allocator->data;
  register ws4d_alloc_list_item *buf = NULL;

  ws4d_assert (alist, NULL);

  ws4d_mutex_lock (&alist->lock);
  buf = (ws4d_alloc_list_item *)
    ws4d_malloc (size + offsetof (ws4d_alloc_list_item, start));

  if (buf)
    {
      buf->size = size + offsetof (ws4d_alloc_list_item, start);
      buf->alist = alist;
      ws4d_list_add_tail (&buf->list, &alist->alist);
      ws4d_mutex_unlock (&alist->lock);
      return &buf->start;
    }

  ws4d_mutex_unlock (&alist->lock);
  return NULL;
}

static int
ws4d_alistallocator_ffree (struct ws4d_abs_allocator *allocator, void *buf)
{
  ws4d_alloc_list *alist = allocator->data;
  ws4d_alloc_list_item *entry;

  ws4d_assert (buf && alist, WS4D_ERR);

  entry = ws4d_container_of (buf, ws4d_alloc_list_item, start);
  ws4d_fail (entry && ws4d_list_check(&entry->list) && entry->alist, WS4D_ERR);

  ws4d_mutex_lock (&alist->lock);
  ws4d_list_del (&entry->list);
  memset (entry, 0, entry->size);
  ws4d_free (entry);
  ws4d_mutex_unlock (&alist->lock);

  return WS4D_ERR;
}

void
ws4d_free_alist (void *buf)
{
  ws4d_alloc_list_item *entry;
#ifdef WITH_MUTEXES
  ws4d_alloc_list *alist;
#endif

  if (buf == NULL)
    {
      return;
    }

  entry = ws4d_container_of (buf, ws4d_alloc_list_item, start);
  ws4d_fail (!(entry && ws4d_list_check(&entry->list) && entry->alist),);

#ifdef WITH_MUTEXES
  alist = entry->alist;
#endif
  ws4d_mutex_lock (&alist->lock);
  ws4d_list_del (&entry->list);
  memset (entry, 0, entry->size);
  ws4d_free (entry);
  ws4d_mutex_unlock (&alist->lock);
}

static void *
ws4d_alistallocator_fmemdup (struct ws4d_abs_allocator *allocator,
                             const void *buf, size_t size)
{
  void *res = NULL;

  if (!(res = ws4d_alistallocator_falloc (allocator, size)))
    {
      return res;
    }

  return memcpy (res, buf, size);
}


static struct ws4d_abs_allocator_cbs ws4d_allocator_cbs = {
  ws4d_alistallocator_fdone,
  ws4d_alistallocator_ffreeall,
  ws4d_alistallocator_falloc,
  ws4d_alistallocator_ffree,
  ws4d_alistallocator_fmemdup,
  ws4d_alistallocator_fmemdup,
  ws4d_alistallocator_ffree
};

int
ws4d_allocator_finit (struct ws4d_abs_allocator *allocator, void *arg)
{
  struct ws4d_alloc_list_s *head = ws4d_malloc (sizeof (ws4d_alloc_list));
  ws4d_fail (head == NULL, WS4D_EOM);

  WS4D_UNUSED_PARAM (arg);

  allocator->id = ws4d_allocator_plugin_id;
  allocator->callbacks = &ws4d_allocator_cbs;
  allocator->data = head;

  WS4D_INIT_LIST (&(head)->alist);
  ws4d_mutex_init (&(head)->lock);

  return WS4D_OK;
}
