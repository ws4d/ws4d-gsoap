/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 *  Created on: 15.01.2010
 *      Author: elmex
 */

#include "ws4d_misc.h"

int
ws4d_qname_init (struct ws4d_qname *qname)
{
  /* test parameters */
  ws4d_assert (qname, WS4D_EPARAM);

  WS4D_INIT_LIST (&qname->list);
  qname->name = NULL;
  qname->ns = NULL;
  qname->prefix = NULL;

  return WS4D_OK;
}

int
ws4d_qname_done (struct ws4d_qname *qname)
{
  /* test parameters */
  ws4d_assert (qname, WS4D_EPARAM);

  ws4d_free_alist ((void *) qname->name);
  ws4d_free_alist ((void *) qname->ns);
  ws4d_free_alist ((void *) qname->prefix);

  return WS4D_OK;
}

int
ws4d_qname_parse (const char *string, struct ws4d_qname *qname,
                  struct ws4d_abs_allocator *alist)
{
  char *id, *prefix_or_ns, *end;

  /* test parameters */
  ws4d_assert (string && qname && alist, WS4D_ERR);

  /* serach for beginning of name */
  if (string[0] == '\"')
    {
      /* in case of a namespace we search for ": */
      id = strstr (string, "\":");
    }
  else
    {
      /* in case of a prefix we search of a : */
      id = strchr (string, ':');
    }

  /* fail if id was not found */
  ws4d_fail (id == NULL, WS4D_ERR);

  /* get prefix or namespace */
  prefix_or_ns = ws4d_malloc_alist (id - string + 1, alist);
  ws4d_assert (prefix_or_ns, WS4D_EOM);

  if (string[0] == '\"')
    {
      /* namespace */
      memcpy (prefix_or_ns, string + 1, id - string - 1);
      prefix_or_ns[id - string] = '\0';
      qname->ns = prefix_or_ns;
      id += 2;
    }
  else
    {
      /* prefix */
      memcpy (prefix_or_ns, string, id - string);
      prefix_or_ns[id - string] = '\0';
      qname->prefix = prefix_or_ns;
      id += 1;
    }

  /* check if this is a stringlist */
  end = memchr (id, ' ', strlen (id));
  if (end == NULL)
    {
      qname->name = ws4d_strdup (id, alist);
    }
  else
    {
      qname->name = ws4d_strndup (id, end - id, alist);
    }

  return WS4D_OK;
}

size_t
ws4d_qname_strlen (struct ws4d_qname * qname)
{
  /* test parameters */
  ws4d_assert (qname && qname->name && qname->ns, 0);

  return strlen (qname->ns) + strlen (qname->name) + 4;
}

int
ws4d_qname_match (struct ws4d_qname *q1, struct ws4d_qname *q2)
{
  /* test parameters */
  ws4d_assert (q1 && q2 && q1->ns && q2->ns && q1->name && q2->name, 0);

  return !strcmp (q1->ns, q2->ns) && !strcmp (q1->name, q2->name);
}

char *
ws4d_qname_tostring2 (struct ws4d_qname *qname, char *buffer, size_t size)
{
  register char *result = buffer;
  size_t ns_length, name_length;

  /* test parameters */
  ws4d_assert (qname && qname->name && qname->ns && buffer, NULL);

  /* check buffer size */
  ws4d_fail (ws4d_qname_strlen (qname) > size, NULL);

  /* calculate lengths */
  ns_length = strlen (qname->ns);
  name_length = strlen (qname->name);

  /* build string */
  strncpy (result, "\"", 1);
  result += 1;
  strncpy (result, qname->ns, ns_length);
  result += ns_length;
  strncpy (result, "\":", 2);
  result += 2;
  strncpy (result, qname->name, name_length);
  result += name_length;
  result[0] = '\0';

  return buffer;
}

struct ws4d_qname *
ws4d_qname_dup (const struct ws4d_qname *src,
                struct ws4d_abs_allocator *alist)
{
  struct ws4d_qname *result = NULL;

  /* test parameters */
  ws4d_assert (src, NULL);

  /* alloc qname */
  result = (struct ws4d_qname *) ws4d_malloc_alist(sizeof(struct ws4d_qname), alist);
  ws4d_fail (result == NULL, NULL);

  /* update fields */
  ws4d_qname_init(result);
  ws4d_qname_setName(result, src->name != NULL ? ws4d_strdup (src->name, alist) : NULL);
  ws4d_qname_setNS(result, src->ns != NULL ? ws4d_strdup (src->ns, alist) : NULL);
  ws4d_qname_setPrefix(result, src->prefix != NULL ? ws4d_strdup (src->prefix, alist) : NULL);

  return result;
}
