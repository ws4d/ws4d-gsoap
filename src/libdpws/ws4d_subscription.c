/* WS4D-gSOAP - Implementation of the Devices Profile for Web Services
 * (DPWS) on top of gSOAP
 * Copyright (C) 2007 University of Rostock
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "ws4d_misc.h"
#include "ws4d_subscription.h"

int
ws4d_subs_init (struct ws4d_subscription *subs, const char *id)
{
  /* test parameters */
  ws4d_assert (subs && id, WS4D_EPARAM);

  WS4D_ALLOCLIST_INIT (&subs->alist);

  /* set id */
  subs->id = ws4d_strdup (id, &subs->alist);
  ws4d_fail (subs->id == NULL, WS4D_EOM);

  return WS4D_OK;
}

int
ws4d_subs_done (struct ws4d_subscription *subs)
{
  /* test parameters */
  ws4d_assert (subs && subs->id, WS4D_EPARAM);

  /* free value */
  ws4d_free_alist ((void *) subs->id);

  if (subs->endto)
    {
      ws4d_free_alist ((void *) subs->endto);
    }

  if (subs->subsm)
    {
      ws4d_free_alist ((void *) subs->subsm);
    }

  ws4d_alloclist_done (&subs->alist);

  return WS4D_OK;
}

int
ws4d_subs_set_endto (struct ws4d_subscription *subs, const char *endto)
{
  /* test parameters */
  ws4d_assert (subs && endto, WS4D_ERR);

  /* update endto */
  return ws4d_strdup2 (endto, (const char **) &subs->endto,
                       ws4d_subs_get_alist (subs));
}

const char *
ws4d_subs_get_subsm (struct ws4d_subscription *subs)
{
  /* test parameters */
  ws4d_assert (subs, NULL);

  return subs->subsm;
}

int
ws4d_subs_set_subsm (struct ws4d_subscription *subs, const char *subsm)
{
  /* test parameters */
  ws4d_assert (subs && subsm, WS4D_EPARAM);

  /* update subsm */
  return ws4d_strdup2 (subsm, (const char **) &subs->subsm,
                       ws4d_subs_get_alist (subs));
}

int
ws4d_subs_set_expires (struct ws4d_subscription *subs, ws4d_time exptime)
{
  /* test parameters */
  ws4d_assert (subs, WS4D_EPARAM);

  subs->expiration = exptime;

  return WS4D_OK;
}

const char *
ws4d_subs_get_id (struct ws4d_subscription *subs)
{
  /* test parameters */
  ws4d_assert (subs, NULL);

  return subs->id;
}

ws4d_time
ws4d_subs_get_expires (struct ws4d_subscription * subs)
{
  /* test parameters */
  ws4d_assert (subs, WS4D_EPARAM);

  return subs->expiration;
}

int
ws4d_subs_dup (struct ws4d_subscription *src, struct ws4d_subscription *dst)
{
  int err;

  /* test parameters */
  ws4d_assert (src && src->id && dst && (src != dst), WS4D_EPARAM);

  if (dst->id)
    {
      err =
        ws4d_strdup2 (src->id, (const char **) &dst->id,
                      ws4d_subs_get_alist (dst));
      ws4d_fail (err != WS4D_OK, err);
    }
  else
    {
      err = ws4d_subs_init (dst, src->id);
      ws4d_fail (err != WS4D_OK, err);
    }

  dst->expiration = src->expiration;

  /* update endto */
  err =
    ws4d_strdup2 (src->endto, (const char **) &dst->endto,
                  ws4d_subs_get_alist (dst));
  ws4d_fail (err != WS4D_OK, err);

  /* update subsm */
  err =
    ws4d_strdup2 (src->subsm, (const char **) &dst->subsm,
                  ws4d_subs_get_alist (dst));
  ws4d_fail (err != WS4D_OK, err);

  /* update filter mode */
  if (src->filter_mode)
    {
      /* TODO: copy filter mode */
    }

  /* update delivery mode */
  if (src->delivery_mode)
    {
      /* TODO: copy filter mode */
    }

  return WS4D_OK;
}

struct ws4d_subscription *
ws4d_subslist_getsubs (struct ws4d_list_node *list, const char *id)
{
  register struct ws4d_subscription *cur = NULL, *next = NULL;

  /* test parameters */
  ws4d_assert (list && id, NULL);

  ws4d_list_foreach (cur, next, list, struct ws4d_subscription, list)
  {
    if (!strcmp (cur->id, id))
      {
        return cur;
      }
  }

  return NULL;
}

struct ws4d_subscription *
ws4d_subslist_initsubs (struct ws4d_list_node *list, const char *id,
                        struct ws4d_abs_allocator *alist)
{
  struct ws4d_subscription *new_subs;
  int err;

  /* test parameters */
  ws4d_assert (list && id && (ws4d_subslist_getsubs (list, id) == NULL)
               && alist, NULL);

  /* allocate memory */
  new_subs = ws4d_malloc_alist (sizeof (struct ws4d_subscription), alist);
  ws4d_fail (new_subs == NULL, NULL);

  /* init subs */
  err = ws4d_subs_init (new_subs, id);
  ws4d_fail (err != WS4D_OK, NULL);

  /* add subs to list */
  ws4d_list_add (&new_subs->list, list);

  return new_subs;
}

static int
_ws4d_subslist_rmsubs (struct ws4d_subscription *subs)
{
  /* test parameters */
  ws4d_assert (subs, WS4D_EPARAM);

  ws4d_subs_done (subs);

  /* remove subs from list */
  ws4d_list_del (&subs->list);

  /* free subs structure */
  ws4d_free_alist (subs);

  return WS4D_OK;
}

int
ws4d_subslist_rmsubs (struct ws4d_list_node *list, const char *id)
{
  struct ws4d_subscription *del_subs;

  /* test parameters */
  ws4d_assert (list && id, WS4D_EPARAM);

  del_subs = ws4d_subslist_getsubs (list, id);
  ws4d_fail (del_subs == NULL, WS4D_ERR);

  /* remove key */
  return _ws4d_subslist_rmsubs (del_subs);
}

int
ws4d_subslist_dup (struct ws4d_list_node *src, struct ws4d_list_node *dst,
                   struct ws4d_abs_allocator *alist)
{
  register struct ws4d_subscription *cur = NULL, *next = NULL;
  int dst_empty;

  /* test parameters */
  ws4d_assert (src && dst && alist, WS4D_EPARAM);

  /* remember if destination list is empty */
  dst_empty = ws4d_list_empty (dst);

  ws4d_list_foreach (cur, next, src, struct ws4d_subscription, list)
  {
    struct ws4d_subscription *new_subs = NULL;
    int replace = 0, err;

    /* search for existing key in destination list */
    if (!dst_empty)
      {
        new_subs = ws4d_subslist_getsubs (dst, cur->id);
        replace = 1;
      }

    /* create new key for destination list */
    if (new_subs == NULL)
      {
        new_subs =
          ws4d_malloc_alist (sizeof (struct ws4d_subscription), alist);
        ws4d_fail (new_subs == NULL, WS4D_EOM);
      }

    /* duplicate key and value */
    err = ws4d_subs_dup (cur, new_subs);
    ws4d_fail (err != WS4D_OK, err);

    /* if this is a new key, register it */
    if (!replace)
      {
        ws4d_list_add (&new_subs->list, dst);
      }
  }

  return WS4D_OK;
}

int
ws4d_subslist_clear (struct ws4d_list_node *list)
{
  register struct ws4d_subscription *cur = NULL, *next = NULL;
  int err;

  /* test parameters */
  ws4d_assert (list, WS4D_EPARAM);

  /* clear all subs */
  ws4d_list_foreach (cur, next, list, struct ws4d_subscription, list)
  {
    err = _ws4d_subslist_rmsubs (cur);
    ws4d_fail (err != WS4D_OK, err);
  }

  return WS4D_OK;
}
