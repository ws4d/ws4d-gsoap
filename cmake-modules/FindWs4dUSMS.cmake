# Copyright (C) 2007  University of Rostock
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.

INCLUDE(FindPackageHandleStandardArgs)

FIND_LIBRARY(DPWS_USMS_LIBRARY
  NAMES usms
  PATHS ${DPWS-P_LIB_DIR} ${DPWS-C_LIB_DIR}
  NO_DEFAULT_PATH
  NO_CMAKE_ENVIRONMENT_PATH
  NO_CMAKE_PATH
  NO_SYSTEM_ENVIRONMENT_PATH
  NO_CMAKE_SYSTEM_PATH)

FIND_LIBRARY(DPWS_USMSMT_LIBRARY
  NAMES usmsmt
  PATHS ${DPWS-P_LIB_DIR} ${DPWS-C_LIB_DIR}
  NO_DEFAULT_PATH
  NO_CMAKE_ENVIRONMENT_PATH
  NO_CMAKE_PATH
  NO_SYSTEM_ENVIRONMENT_PATH
  NO_CMAKE_SYSTEM_PATH)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(Ws4dUSMS "Could not find utilized services manager service libraries" DPWS_USMS_LIBRARY DPWS_USMSMT_LIBRARY)
