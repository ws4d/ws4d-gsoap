# Copyright (C) 2007  University of Rostock
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.

# A collection of little helpers.

MACRO(DPWS_METADATA_GENERATE metadata_file prefix destination)
  IF(XSLTPROC_EXECUTABLE)
    SET(dpws_metadata_generated_files
      ${destination}/${prefix}_metadata.c
      ${destination}/${prefix}_metadata.h)
    SET_SOURCE_FILES_PROPERTIES(${dpws_metadata_generated_files}
      PROPERTIES GENERATED TRUE)

    IF(${ARGC} GREATER 3)

      FOREACH(target ${ARGN})
        IF(target MATCHES "device")
          SET(DPWS_METADATATOC ${DPWS_DEVICE_METADATATOC})
          SET(DPWS_METADATATOH ${DPWS_DEVICE_METADATATOH})
        ELSEIF(target MATCHES "hosted")
          SET(DPWS_METADATATOC ${DPWS_HOSTED_METADATATOC})
          SET(DPWS_METADATATOH ${DPWS_HOSTED_METADATATOH})
        ELSE(target MATCHES "device")
          MESSAGE(SEND_ERROR "target \"${target}\" not supported in DPWS_METADATA_GENERATE")
        ENDIF(target MATCHES "device")
      ENDFOREACH(target)

    ELSE(${ARGC} GREATER 3)
      SET(DPWS_METADATATOC ${DPWS_DEVICE_METADATATOC})
      SET(DPWS_METADATATOH ${DPWS_DEVICE_METADATATOH})
    ENDIF(${ARGC} GREATER 3)

    ADD_CUSTOM_COMMAND(OUTPUT ${dpws_metadata_generated_files}
      COMMAND ${XSLTPROC_EXECUTABLE} --stringparam nsprefix ${prefix} ${DPWS_METADATATOC} ${CMAKE_CURRENT_SOURCE_DIR}/${metadata_file} > ${destination}/${prefix}_metadata.c
      COMMAND ${XSLTPROC_EXECUTABLE} --stringparam nsprefix ${prefix} ${DPWS_METADATATOH} ${CMAKE_CURRENT_SOURCE_DIR}/${metadata_file} > ${destination}/${prefix}_metadata.h
      DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${metadata_file})
  ENDIF(XSLTPROC_EXECUTABLE)
ENDMACRO(DPWS_METADATA_GENERATE)

MACRO(DPWS_EMBEDD_WSDL destination prefix)
  SET(dpws_wsdl_generated_files
    ${destination}/${prefix}_wsdl.c
    ${destination}/${prefix}_wsdl.h)
  SET_SOURCE_FILES_PROPERTIES( ${dpws_wsdl_generated_files}
    PROPERTIES GENERATED TRUE)

  IF(DPWS_EMBEDWSDL)
    ADD_CUSTOM_COMMAND(OUTPUT ${dpws_wsdl_generated_files}
      COMMAND "${DPWS_EMBEDWSDL}" -d ${destination} -p ${prefix} ${ARGN}
      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
      DEPENDS ${ARGN})
  ELSE(DPWS_EMBEDWSDL)
    ADD_CUSTOM_COMMAND(OUTPUT ${dpws_wsdl_generated_files}
      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
      DEPENDS ${ARGN})
  ENDIF(DPWS_EMBEDWSDL)
ENDMACRO(DPWS_EMBEDD_WSDL)

MACRO(DO_WSDL2H outfiles opts typemap)
  FOREACH (it ${ARGN})
    GET_FILENAME_COMPONENT(name ${it} 	NAME_WE)
    SET(outfile ${CMAKE_CURRENT_BINARY_DIR}/${name}.h)
        IF(CYGWIN)
            ADD_CUSTOM_COMMAND(OUTPUT ${outfile}
                COMMAND ${GSOAP_WSDL2H} ARGS ${opts} -t$\(shell cygpath -am ${typemap}\) -n$\(shell cygpath -am ${name}\) -o$\(shell cygpath -am ${outfile}.tmp\) $\(shell cygpath -am ${it}\)
                COMMAND ${GNU_SED_EXECUTABLE} ARGS -i -e "/^..gsoapopt/d" -e "w ${outfile}" ${outfile}.tmp
                DEPENDS ${it} ${typemap})
            ELSE(CYGWIN)
            ADD_CUSTOM_COMMAND(OUTPUT ${outfile}
                COMMAND ${GSOAP_WSDL2H} ARGS ${opts} -t${typemap} -n${name} -o${outfile}.tmp ${it}
                COMMAND ${GNU_SED_EXECUTABLE} ARGS -e "/^..gsoapopt/d" -e "w ${outfile}" ${outfile}.tmp
                DEPENDS ${it} ${typemap})
            ENDIF(CYGWIN)
    SET(${outfiles} ${${outfiles}} ${outfile})
  ENDFOREACH (it)
ENDMACRO(DO_WSDL2H)

FUNCTION(WS4D_ADD_FEATURE_INFO _name _enabled _desc)
IF (${_enabled})
    SET_PROPERTY(GLOBAL APPEND PROPERTY WS4D_ENABLED_FEATURES "${_name}")
  ELSE ()
    SET_PROPERTY(GLOBAL APPEND PROPERTY WS4D_DISABLED_FEATURES "${_name}")
  ENDIF ()

  SET_PROPERTY(GLOBAL PROPERTY _WS4D_${_name}_DESCRIPTION "${_desc}" )
ENDFUNCTION(WS4D_ADD_FEATURE_INFO)

FUNCTION(WS4D_FEATURE_SUMMARY)
  MESSAGE(STATUS "Feature Summary:")
  GET_PROPERTY(_EnabledFeatures  GLOBAL PROPERTY WS4D_ENABLED_FEATURES)
  FOREACH(_currentFeature ${_EnabledFeatures})
    GET_PROPERTY(_info  GLOBAL PROPERTY _WS4D_${_currentFeature}_DESCRIPTION)
    MESSAGE(STATUS "  ENABLED ${_currentFeature}: ${_info}")
  ENDFOREACH(_currentFeature)
  GET_PROPERTY(_DisabledFeatures  GLOBAL PROPERTY WS4D_DISABLED_FEATURES)
  FOREACH(_currentFeature ${_DisabledFeatures})
    GET_PROPERTY(_info  GLOBAL PROPERTY _WS4D_${_currentFeature}_DESCRIPTION)
    MESSAGE(STATUS "  DISABLED ${_currentFeature}: ${_info}")
  ENDFOREACH(_currentFeature)
ENDFUNCTION(WS4D_FEATURE_SUMMARY)
