# ws4d-gSOAP $Id$

INCLUDE(FindPackageHandleStandardArgs)

FIND_PROGRAM(COMP_CODE_EXECUTABLE
  NAMES compcode
  DOC "CompCode.exe. See \"The NetBurner Tools\" documentation for details."
  PATHS ${NBROOT}/pcbin
)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(CompCode DEFAULT_MSG COMP_CODE_EXECUTABLE)

