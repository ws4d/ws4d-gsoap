#!/bin/bash
############################################
## runs indent for one or more source files
############################################

INDENT_OPTIONS="-gnu -nut -sc" # -hnl

# Test for parameters
if [ "$#" -eq 0 ]
then
  echo "Usage: `basename $0` file_1 [file_2] ... [file_n]"
  exit $E_NOARGS
fi

# Find indent file
indent_file=`which indent`
if [ ! -e "${indent_file}" ]
then
  echo "Can't find indent!"
  exit 1
fi

# Loop for each parameter
for arg in "$@"
do
  # test if file exists
  if [ ! -e $arg ]
  then
    echo "File \"$arg\" doesn't exist!"
    exit 1
  fi

  convertfile="$arg"
  
  #try to indent
  ${indent_file} -npro ${INDENT_OPTIONS} ${convertfile}
  if [ $? -ne 0 ]
  then
    echo "Error calling indent!"
    exit 2
  fi
done
exit 0
