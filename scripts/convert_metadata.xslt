<?xml version="1.0" encoding="UTF-8"?>

<!--
_one line to give the program's name and a brief idea of what it does._
Copyright (C) 2007  University of Rostock

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301 USA.
 -->

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:wsm="http://schemas.xmlsoap.org/ws/2004/09/mex"
xmlns:wdp="http://schemas.xmlsoap.org/ws/2006/02/devprof"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:wsd="http://schemas.xmlsoap.org/ws/2005/04/discovery"
xmlns:wsa="http://schemas.xmlsoap.org/ws/2004/08/addressing"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:ws4ddm="http://www.ws4d.org/2012/03/device_metadata"
xmlns:ws4dcm="http://www.ws4d.org/2012/03/codegen_metadata"
exclude-result-prefixes="xsl wsm wdp xsi wsd wsa" >
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:template match="text()" />

<xsl:template match="/wsm:Metadata">
<xsl:element name="ws4ddm:Configuration">
<xsl:copy-of select="namespace::*" />
<xsl:apply-templates select="./*" />
</xsl:element>
</xsl:template>

<xsl:template match="/wsm:Metadata/wsm:MetadataSection">
<xsl:element name="ws4ddm:ConfigurationSection">
<xsl:choose>
<xsl:when test="@Dialect='http://schemas.xmlsoap.org/ws/2006/02/devprof/ThisDevice'">
<xsl:attribute name="ws4ddm:Dialect">http://www.ws4d.org/2012/03/device_metadata/ThisDevice</xsl:attribute>
</xsl:when>
<xsl:when test="@Dialect='http://schemas.xmlsoap.org/ws/2006/02/devprof/ThisModel'">
<xsl:attribute name="ws4ddm:Dialect">http://www.ws4d.org/2012/03/device_metadata/ThisModel</xsl:attribute>
</xsl:when>
<xsl:when test="@Dialect='http://schemas.xmlsoap.org/ws/2006/02/devprof/Relationship'">
<xsl:attribute name="ws4ddm:Dialect">http://www.ws4d.org/2012/03/device_metadata/Relationship</xsl:attribute>
</xsl:when>
<xsl:otherwise>
<xsl:attribute name="ws4ddm:Dialect"><xsl:value-of select="@Dialect" /></xsl:attribute>
</xsl:otherwise>
</xsl:choose>
<xsl:if test="count(@Identifier)!= 0">
<xsl:attribute name="ws4ddm:Identifier"><xsl:value-of select="@Identifier" /></xsl:attribute>
</xsl:if>
<xsl:apply-templates select="./*"/>
</xsl:element>
</xsl:template>

<xsl:template match="/wsm:Metadata/wsm:MetadataSection/wdp:ThisDevice">
<xsl:element name="ws4ddm:ThisDevice">
<xsl:for-each select="*">
<xsl:choose>
<xsl:when test="name() = 'wdp:FriendlyName'">
<xsl:element name="ws4ddm:FriendlyName">
<xsl:attribute name="lang"><xsl:value-of select="@lang" /></xsl:attribute>
<xsl:value-of select="." />
</xsl:element>
</xsl:when>
<xsl:when test="name() = 'wdp:FirmwareVersion'">
<xsl:element name="ws4ddm:FirmwareVersion">
<xsl:value-of select="." />
</xsl:element>
</xsl:when>
<xsl:when test="name() = 'wdp:SerialNumber'">
<xsl:element name="ws4ddm:SerialNumber">
<xsl:value-of select="." />
</xsl:element>
</xsl:when>
<xsl:otherwise>
<xsl:copy><xsl:value-of select="." /></xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</xsl:element>
</xsl:template>

<xsl:template match="/wsm:Metadata/wsm:MetadataSection/wdp:ThisModel">
<xsl:element name="ws4ddm:ThisModel">
<xsl:for-each select="*">
<xsl:choose>
<xsl:when test="name() = 'wdp:Manufacturer'">
<xsl:element name="ws4ddm:Manufacturer">
<xsl:attribute name="lang"><xsl:value-of select="@lang" /></xsl:attribute>
<xsl:value-of select="." />
</xsl:element>
</xsl:when>
<xsl:when test="name() = 'wdp:ManufacturerUrl'">
<xsl:element name="ws4ddm:ManufacturerUrl">
<xsl:value-of select="." />
</xsl:element>
</xsl:when>
<xsl:when test="name() = 'wdp:ModelName'">
<xsl:element name="ws4ddm:ModelName">
<xsl:attribute name="lang"><xsl:value-of select="@lang" /></xsl:attribute>
<xsl:value-of select="." />
</xsl:element>
</xsl:when>
<xsl:when test="name() = 'wdp:ModelNumber'">
<xsl:element name="ws4ddm:ModelNumber">
<xsl:value-of select="." />
</xsl:element>
</xsl:when>
<xsl:when test="name() = 'wdp:ModelUrl'">
<xsl:element name="ws4ddm:ModelUrl">
<xsl:value-of select="." />
</xsl:element>
</xsl:when>
<xsl:when test="name() = 'wdp:PresentationUrl'">
<xsl:element name="ws4ddm:PresentationUrl">
<xsl:value-of select="." />
</xsl:element>
</xsl:when>
<xsl:otherwise>
<xsl:copy><xsl:value-of select="." /></xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</xsl:element>
</xsl:template>

<xsl:template match="/wsm:Metadata/wsm:MetadataSection/wdp:Relationship">
<xsl:element name="ws4ddm:Relationship">
<xsl:for-each select="*">
<xsl:choose>
<xsl:when test="name() = 'wdp:Host'">
<xsl:apply-templates select="."/>
</xsl:when>
<xsl:when test="name() = 'wdp:Hosted'">
<xsl:apply-templates select="."/>
</xsl:when>
<xsl:otherwise>
<xsl:copy><xsl:value-of select="." /></xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</xsl:element>
</xsl:template>

<xsl:template match="/wsm:Metadata/wsm:MetadataSection/wdp:Relationship/wdp:Host">
<xsl:element name="ws4ddm:Host">
<xsl:for-each select="*">
<xsl:choose>
<xsl:when test="name() = 'wsa:EndpointReference'">
<xsl:element name="ws4ddm:DeviceId"><xsl:value-of select="." /></xsl:element>
<xsl:element name="ws4ddm:XAddrs">Add Address here</xsl:element>
</xsl:when>
<xsl:when test="name() = 'wdp:Types'">
<xsl:element name="ws4ddm:Types"><xsl:value-of select="." /></xsl:element>
</xsl:when>
<xsl:when test="name() = 'wdp:ServiceId'">
<xsl:element name="ws4ddm:ServiceId"><xsl:value-of select="."/></xsl:element>
</xsl:when>
<xsl:when test="name() = 'Name'">
<xsl:element name="ws4dcm:Name"><xsl:value-of select="."/></xsl:element>
</xsl:when>
<xsl:otherwise>
<xsl:copy><xsl:value-of select="." /></xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</xsl:element>
</xsl:template>

<xsl:template match="/wsm:Metadata/wsm:MetadataSection/wdp:Relationship/wdp:Hosted">
<xsl:element name="ws4ddm:Hosted">
<xsl:for-each select="*">
<xsl:choose>
<xsl:when test="name() = 'wsa:EndpointReference'">
<xsl:element name="ws4ddm:XAddrs"><xsl:value-of select="." /></xsl:element>
</xsl:when>
<xsl:when test="name() = 'wdp:Types'">
<xsl:element name="ws4ddm:Types"><xsl:value-of select="." /></xsl:element>
</xsl:when>
<xsl:when test="name() = 'wdp:ServiceId'">
<xsl:element name="ws4ddm:ServiceId"><xsl:value-of select="."/></xsl:element>
</xsl:when>
<xsl:when test="name() = 'Name'">
<xsl:element name="ws4dcm:Name"><xsl:value-of select="."/></xsl:element>
</xsl:when>
<xsl:otherwise>
<xsl:copy><xsl:value-of select="." /></xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</xsl:element>
</xsl:template>

</xsl:stylesheet>
