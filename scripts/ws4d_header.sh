#!/bin/bash

#############################################
# Writes the content of header_file into the
# header of every target_file
#############################################


if [ "$#" -lt 2 ]
then
  echo "Usage: `basename $0` header_file target_file_1 [target_file_2] .. [target_file_n]"
  exit $E_NOARGS
fi

idx=1

for arg in "$@"
do
  if [ "$idx" -eq 1 ]
  then
  	if [ ! -e $arg ]
  	then
  	  echo "Error: Header $arg does not exist"
  	  exit 1
  	fi
    header_file=$arg
  else
    if [ ! -e $arg ]
    then
      echo "Error: cannot paste header to $arg - file does not exists"
      exit 2
    fi
    tmp_file=`mktemp`
    cat $header_file > $tmp_file
    cat $arg >> $tmp_file
    cp $tmp_file $arg
    rm $tmp_file
  fi
  let "idx += 1"
done

exit 0