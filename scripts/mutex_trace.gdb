# Copyright (C) 2007  University of Rostock
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.

set pagination off

break ws4d_mutex_init
commands
silent
set logging off
set logging file ./mutex.trace
set logging overwrite off
set logging redirect on
set logging on
printf "<--- created mutex\n" 
bt full
set logging off
cont
end

break ws4d_mutex_destroy
commands
silent
set logging off
set logging file ./mutex.trace
set logging overwrite off
set logging redirect on
set logging on
printf "<--- destroyed mutex\n" 
bt full
set logging off
cont
end

run
