#!/usr/bin/perl -w
# Copyright (C) 2007  University of Rostock
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.

@init_trace=();
@dest_trace=();
@mutex_init=();
@mutex_dest=();
$init_index=-1;
$dest_index=-1;
$is_init = -1;
$leaked_index=-1;
@mutex_leaked=();

# read log file
# build chunks from "Hardware watchpoint" to "Hardware watchpoint"
open(TRACEFILE, "mutex.trace") or die("Could not open input mutex.trace file.");
foreach $line (<TRACEFILE>) {
    # do line-by-line processing.
    if ($line =~ m/^#0  ws4d_mutex_init \(mutex=(0x[0-9a-f]+)\)/) {
        $init_index++;
    	$init_trace[$init_index]=$line;
        $is_init = 1;
        $mutex_init[$init_index]=$1;
    } elsif ($line =~ m/^#0  ws4d_mutex_destroy \(mutex=(0x[0-9a-f]+)\)/) {
    	$dest_index++;
    	$dest_trace[$dest_index]=$line;
        $is_init = 0;
        $mutex_dest[$dest_index]=$1;
    } elsif ($line =~ m/^<--- created mutex/) {
    } elsif ($line =~ m/^<--- destroyed mutex/) {
    } else {
        if ($is_init == 1) {
	  $init_trace[$init_index]="$init_trace[$init_index]$line";
	} elsif ($is_init == 0) {
          $dest_trace[$dest_index]="$dest_trace[$dest_index]$line";
	}
    }
}
close(TRACEFILE);

# determin leaks
for($init=0;$init<=$init_index;$init++) {
  $found = 0;
  for($dest=0;$dest<=$dest_index;$dest++) {
    if ($mutex_init[$init] eq $mutex_dest[$dest]) {
      $found = 1;
      last;
    }
  }

  if ($found == 0) {
    $leaked_index++;
    $mutex_leaked[$leaked_index]=$mutex_init[$init];
  }
}

# print traces related to leaks
for($leaked=0;$leaked<=$leaked_index;$leaked++) {
  for($init=0;$init<=$init_index;$init++) {
    if ($mutex_init[$init] eq $mutex_leaked[$leaked]) {
      print "###### LEAK $leaked ######\n";
      print "Backtrace for mutex $mutex_init[$init]:\n$init_trace[$init]\n";
      print "##########################\n\n";
      last;
    }
  }
}

$init_index++;
$dest_index++;
$leaked_index++;

print "Read traces: ws4d_mutex_init():$init_index ws4d_mutex_destroy():$dest_index leaked:$leaked_index\n";
