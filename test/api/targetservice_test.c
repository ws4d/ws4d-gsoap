/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 02.12.2010
 *      Author: elmex
 */

#include "dpws.nsmap"
#include "dpws_device.h"

#include "test_support.h"

#include "ws4d_targetservice.h"
#include "ws4d_eprllist.h"

#define DPWS_STRCMP0_MATCHING_FUNC "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/strcmp0"

START_TEST (targetservice_create_destroy)
{
  struct ws4d_targetservice ts;

  fail_if (ws4d_targetservice_init(&ts, DPWS_STRCMP0_MATCHING_FUNC, NULL) != WS4D_OK, "Can't initialize target service\n");

  fail_if (ws4d_targetservice_done (&ts) != WS4D_OK, "Can't clean up target service structure");
}
END_TEST

START_TEST (targetservice_add_remove_target)
{
  struct ws4d_targetservice ts;
  struct ws4d_epr *target;

  fail_if (ws4d_targetservice_init(&ts, DPWS_STRCMP0_MATCHING_FUNC, NULL) != WS4D_OK, "Can't initialize target service\n");

  target = ws4d_targetservice_inittarget(&ts, "urn:uuid:testtarget", "http://127.0.0.1/testtarget");
  fail_if (target == NULL, "Could not add target to target service\n");

  fail_if (ws4d_targetservice_deltarget(&ts, target) != WS4D_OK, "Can't remove target from target service\n");

  fail_if (ws4d_targetservice_done (&ts) != WS4D_OK, "Can't clean up target service structure");
}
END_TEST

START_TEST (targetservice_test_matching)
{
  struct ws4d_targetservice ts;
  struct ws4d_epr *target;
  struct ws4d_abs_eprlist matches;

  fail_if (ws4d_targetservice_init(&ts, DPWS_STRCMP0_MATCHING_FUNC, NULL) != WS4D_OK, "Can't initialize target service\n");

  target = ws4d_targetservice_inittarget(&ts, "urn:uuid:testtarget", "http://127.0.0.1/testtarget");
  fail_if (target == NULL, "Could not add target to target service\n");

  ws4d_targetep_add_Scope(target, "http://www.ws4d.org/testscope");
  ws4d_targetep_add_Typestr(target, "\"http://www.ws4d.org/namespaces/testns\":testtype1 \"http://www.ws4d.org/namespaces/testns\":testtype2");

  ws4d_targetep_activate(target, 0, 0, 0);

  ws4d_eprlist_init (&matches, ws4d_eprllist_init, NULL);

  ws4d_targetservice_getmatches(&ts, NULL, NULL, NULL, &matches);
  fail_if (ws4d_eprlist_isempty(&matches), "Target does not match empty scope and empty type\n");

  ws4d_targetservice_getmatches(&ts, "", NULL, "", &matches);
  fail_if (ws4d_eprlist_isempty(&matches), "Target does not match empty scope and empty type\n");

  ws4d_targetservice_getmatches(&ts, "http://www.ws4d.org/testscope", NULL, "", &matches);
  fail_if (ws4d_eprlist_isempty(&matches), "Target does not match test testscope and empty type\n");

  ws4d_targetservice_getmatches(&ts, "http://www.ws4d.org/wrongscope", NULL, "", &matches);
  fail_if (!ws4d_eprlist_isempty(&matches), "Target should not match wrong scope and empty type\n");

  ws4d_targetservice_getmatches(&ts, "", NULL, "\"http://www.ws4d.org/namespaces/testns\":testtype1", &matches);
  fail_if (ws4d_eprlist_isempty(&matches), "Target does not match testtype1 and empty scope\n");

  ws4d_targetservice_getmatches(&ts, "", NULL, "\"http://www.ws4d.org/namespaces/testns\":testtype2", &matches);
  fail_if (ws4d_eprlist_isempty(&matches), "Target does not match testtype2 and empty scope\n");

  ws4d_targetservice_getmatches(&ts, "", NULL, "\"http://www.ws4d.org/namespaces/testns\":testtype1 \"http://www.ws4d.org/namespaces/testns\":testtype2", &matches);
  fail_if (ws4d_eprlist_isempty(&matches), "Target does not match testtype1 and testtype2 and empty scope\n");

  ws4d_targetservice_getmatches(&ts, "", NULL, "\"http://www.ws4d.org/namespaces/testns\":testtype3", &matches);
  fail_if (!ws4d_eprlist_isempty(&matches), "Target should not match testtype3 and empty scope\n");

  ws4d_targetservice_getmatches(&ts, "http://www.ws4d.org/testscope", NULL, "\"http://www.ws4d.org/namespaces/testns\":testtype1 \"http://www.ws4d.org/namespaces/testns\":testtype2", &matches);
  fail_if (ws4d_eprlist_isempty(&matches), "Target does not match testtype1 and testtype2 and testscope\n");

  ws4d_eprlist_done(&matches);

  fail_if (ws4d_targetservice_deltarget(&ts, target) != WS4D_OK, "Can't remove target from target service\n");

  fail_if (ws4d_targetservice_done (&ts) != WS4D_OK, "Can't clean up target service structure");
}
END_TEST


WS4D_TESTCASE_START ("targetservice")
WS4D_TESTCASE_ADD (targetservice_create_destroy)
WS4D_TESTCASE_ADD (targetservice_add_remove_target)
WS4D_TESTCASE_ADD (targetservice_test_matching)
WS4D_TESTCASE_END
