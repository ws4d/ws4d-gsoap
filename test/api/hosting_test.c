/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 02.12.2010
 *      Author: elmex
 */

#include "dpws.nsmap"
#include "dpws_device.h"
#include <signal.h>
#include <time.h>
#include "msiop_metadata.h"
#include "msiop_wsdl.h"
#include "iop_services.h"

#include "test_support.h"

const char *interfaces_s = "127.0.0.1";

START_TEST (hosting_create_destroy)
{
  struct ws4d_epr hosting;

  ws4d_epr_init(&hosting);

  fail_if (ws4d_hosting_init (&hosting) != WS4D_OK, "Can't initialize hosting service\n");

  fail_if (ws4d_epr_done (&hosting) != WS4D_OK, "Can't clean up hosting service structure");
}
END_TEST

START_TEST (hosting_add_and_delete_service)
{
  struct ws4d_epr hosting;
  struct ws4d_epr *service = NULL;

  ws4d_epr_init(&hosting);

  ws4d_targetep_set_XAddrs(&hosting, interfaces_s);

  fail_if (ws4d_hosting_init (&hosting) != WS4D_OK, "Can't initialize hosting service\n");

  service = ws4d_hosting_add_service (&hosting, "http://www.ws4d.org/gsoap/testservice");
  fail_if (service == NULL, "Can't add service\n");

  fail_if (ws4d_hosting_remove_service (&hosting, service) != WS4D_OK, "Can't remove service\n");

  fail_if (ws4d_epr_done (&hosting) != WS4D_OK, "Can't clean up hosting service structure");
}
END_TEST

START_TEST (hosting_activate_and_deactivate_service)
{
  struct ws4d_epr hosting;
  struct ws4d_epr *service = NULL;
  char uri[255];

  ws4d_epr_init(&hosting);

  ws4d_targetep_set_XAddrs(&hosting, interfaces_s);

  memset(uri, 0, 255);
  strncpy(uri, "http://host:0/testservice", 26);

  fail_if (ws4d_hosting_init (&hosting) != WS4D_OK, "Can't initialize hosting service\n");

  service = ws4d_hosting_add_service (&hosting, "http://www.ws4d.org/gsoap/testservice");
  fail_if (service == NULL, "Can't add service\n");

  fail_if (ws4d_hosting_activate_service(&hosting, service, uri, 255) != WS4D_OK, "Can't activate service\n");

  fail_if (ws4d_hosting_deactivate_service(&hosting, service) != WS4D_OK, "Can't deactivate service\n");

  fail_if (ws4d_hosting_remove_service (&hosting, service) != WS4D_OK, "Can't remove service\n");

  fail_if (ws4d_epr_done (&hosting) != WS4D_OK, "Can't clean up hosting service structure");
}
END_TEST

WS4D_TESTCASE_START ("hosting")
WS4D_TESTCASE_ADD (hosting_create_destroy)
WS4D_TESTCASE_ADD (hosting_add_and_delete_service)
WS4D_TESTCASE_ADD (hosting_activate_and_deactivate_service)
WS4D_TESTCASE_END
