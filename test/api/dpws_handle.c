/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 08.06.2011
 *      Author: elmex
 */

#include "dpws.nsmap"
#include "dpws_device.h"
#include <signal.h>
#include <time.h>

#include "test_support.h"

START_TEST (dpws_handle_copy)
{
  struct dpws_s dpws;
  struct soap handle, *copy;
  char uri[DPWS_URI_MAX_LEN] = "http://host:0/test";

  /* initialize dpws handle */
  fail_if (dpws_init (&dpws, "127.0.0.1"), "Can't initialize dpws handle");

  /* initialize soap handle */
  soap_init (&handle);

  /* register dpws_handle plugin */
  fail_if (dpws_handle_bind(&dpws, &handle, uri, DPWS_URI_MAX_LEN, 100), "Can't init dpws handle plugin");

  /* copy handle to test dpws_handle plugin copy function */
  copy = soap_copy(&handle);
  fail_if (copy == NULL, "Can't copy dpws handle plugin");

  /*try to get paddr from copy */
  fail_if (dpws_handle_get_paddrs(copy) == NULL, "Can't copy dpws handle plugin");

  /* clean up copy */
  soap_done(copy);
  soap_free(copy);

  /* clean up soap handle */
  soap_done (&handle);

  /* clean up of device structure */
  fail_if (dpws_done (&dpws),
           "Can't clean up device structure");
}
END_TEST

WS4D_TESTCASE_START ("dpws_handle")
WS4D_TESTCASE_ADD (dpws_handle_copy)
WS4D_TESTCASE_END
