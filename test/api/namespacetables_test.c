/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 02.12.2010
 *      Author: elmex
 */

#include "dpws.nsmap"
#include "dpws_device.h"
#include <signal.h>
#include <time.h>
#include "msiop_metadata.h"
#include "msiop_wsdl.h"
#include "iop_services.h"

#include "test_support.h"

struct Namespace table1[] =
{
  {"SOAP-ENV", "http://www.w3.org/2003/05/soap-envelope", "http://www.w3.org/*/soap-envelope", NULL},
  {"SOAP-ENC", "http://www.w3.org/2003/05/soap-encoding", "http://www.w3.org/*/soap-encoding", NULL},
  {"xsi", "http://www.w3.org/2001/XMLSchema-instance", "http://www.w3.org/*/XMLSchema-instance", NULL},
  {"xsd", "http://www.w3.org/2001/XMLSchema", "http://www.w3.org/*/XMLSchema", NULL},
  {"wsm", "http://schemas.xmlsoap.org/ws/2004/09/mex", NULL, NULL},
  {"wsa", "http://schemas.xmlsoap.org/ws/2004/08/addressing", NULL, NULL},
  {"wsdp", "http://schemas.xmlsoap.org/ws/2006/02/devprof", NULL, NULL},
  {NULL, NULL, NULL, NULL}
};

struct Namespace table2[] =
{
  {"SOAP-ENV", "http://www.w3.org/2003/05/soap-envelope", "http://www.w3.org/*/soap-envelope", NULL},
  {"SOAP-ENC", "http://www.w3.org/2003/05/soap-encoding", "http://www.w3.org/*/soap-encoding", NULL},
  {"xsi", "http://www.w3.org/2001/XMLSchema-instance", "http://www.w3.org/*/XMLSchema-instance", NULL},
  {"xsd", "http://www.w3.org/2001/XMLSchema", "http://www.w3.org/*/XMLSchema", NULL},
  {"wsa", "http://schemas.xmlsoap.org/ws/2004/08/addressing", NULL, NULL},
  {"wst", "http://schemas.xmlsoap.org/ws/2004/09/transfer", NULL, NULL},
  {"wsd", "http://schemas.xmlsoap.org/ws/2005/04/discovery", NULL, NULL},
  {NULL, NULL, NULL, NULL}
};

struct Namespace result1[] =
{
  {"SOAP-ENV", "http://www.w3.org/2003/05/soap-envelope", "http://www.w3.org/*/soap-envelope", NULL},
  {"SOAP-ENC", "http://www.w3.org/2003/05/soap-encoding", "http://www.w3.org/*/soap-encoding", NULL},
  {"xsi", "http://www.w3.org/2001/XMLSchema-instance", "http://www.w3.org/*/XMLSchema-instance", NULL},
  {"xsd", "http://www.w3.org/2001/XMLSchema", "http://www.w3.org/*/XMLSchema", NULL},
  {"wsm", "http://schemas.xmlsoap.org/ws/2004/09/mex", NULL, NULL},
  {"wsa", "http://schemas.xmlsoap.org/ws/2004/08/addressing", NULL, NULL},
  {"wsdp", "http://schemas.xmlsoap.org/ws/2006/02/devprof", NULL, NULL},
  {"wst", "http://schemas.xmlsoap.org/ws/2004/09/transfer", NULL, NULL},
  {"wsd", "http://schemas.xmlsoap.org/ws/2005/04/discovery", NULL, NULL},
  {NULL, NULL, NULL, NULL}
};


START_TEST (namespacetables_extend)
{
  struct Namespace *temp_namespaces, *res_entry, *expected;
  struct ws4d_abs_allocator alist;

  WS4D_ALLOCLIST_INIT(&alist);

  temp_namespaces =
    soap_extend_namespaces (table1, table2, &alist);
  fail_if (temp_namespaces == NULL, "Can't extend namespaces\n");

  for(expected = result1; (expected->id != NULL) && (expected->ns != NULL); expected++)
  {
    int match = 0;
    for(res_entry = temp_namespaces; (res_entry->id != NULL) && (res_entry->ns != NULL); res_entry++)
    {
      if (!strcmp(expected->id, res_entry->id) && !strcmp(expected->ns, res_entry->ns))
      {
        match = 1;
        break;
      }
    }
    fail_if(match == 0, "Incorrect extension: entry (%s):%s is missing\n", expected->ns, expected->id);
  }

  soap_free_namespaces(temp_namespaces, &alist);

  ws4d_alloclist_done(&alist);
}
END_TEST

WS4D_TESTCASE_START ("namespacetables")
WS4D_TESTCASE_ADD (namespacetables_extend)
WS4D_TESTCASE_END
