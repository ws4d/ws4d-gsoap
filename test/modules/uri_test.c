/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 02.12.2010
 *      Author: elmex
 */

#include "dpws.nsmap"
#include "dpws_device.h"
#include <signal.h>
#include <time.h>
#include "msiop_metadata.h"
#include "msiop_wsdl.h"
#include "iop_services.h"

#include "test_support.h"

START_TEST (uri_create_destroy)
{
  struct ws4d_uri uri;

  fail_if (ws4d_uri_init (&uri) != WS4D_OK, "Can't initialize uri structure\n");

  fail_if (ws4d_uri_done (&uri) != WS4D_OK, "Can't clean up uri structure\n");
}
END_TEST

START_TEST (parse_check_uris)
{
    struct ws4d_uri uri;
    const char *uri1 = "ftp://ftp.is.co.za/rfc/rfc1808.txt";
    const char *uri2 = "gopher://spinaltap.micro.umn.edu/00/Weather/California/Los%20Angeles";
    const char *uri3 = "http://www.math.uio.no/faq/compression-faq/part1.html";
    const char *uri4 = "mailto:mduerst@ifi.unizh.ch";
    const char *uri5 = "news:comp.infosystems.www.servers.unix";
    const char *uri6 = "telnet://melvyl.ucop.edu/";
    const char *uri7 = "http://duckduckgo.com/?q=word+h%C3%B6he+der+kopfzeile+%C3%A4ndern";

    fail_if (ws4d_uri_init (&uri) != WS4D_OK, "Can't initialize uri structure\n");

    fail_if (ws4d_parse_uri(uri1, &uri, WITH_ALL) != WS4D_OK, "Can't parse uri %s\n", uri1);

    fail_if (ws4d_parse_uri(uri2, &uri, WITH_ALL) != WS4D_OK, "Can't parse uri %s\n", uri2);

    fail_if (ws4d_parse_uri(uri3, &uri, WITH_ALL) != WS4D_OK, "Can't parse uri %s\n", uri3);

    fail_unless (ws4d_parse_uri(uri4, &uri, WITH_ALL) != WS4D_OK, "Can't parse uri %s\n", uri4);

    fail_unless (ws4d_parse_uri(uri5, &uri, WITH_ALL) != WS4D_OK, "Can't parse uri %s\n", uri5);

    fail_if (ws4d_parse_uri(uri6, &uri, WITH_ALL) != WS4D_OK, "Can't parse uri %s\n", uri6);

    fail_if (ws4d_parse_uri(uri7, &uri, WITH_ALL) != WS4D_OK, "Can't parse uri %s\n", uri7);

    fail_if (ws4d_uri_done (&uri) != WS4D_OK, "Can't clean up uri structure\n");
}
END_TEST

WS4D_TESTCASE_START ("uri")
WS4D_TESTCASE_ADD (uri_create_destroy)
WS4D_TESTCASE_ADD (parse_check_uris)
WS4D_TESTCASE_END
