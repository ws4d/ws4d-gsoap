/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 02.12.2010
 *      Author: elmex
 */

#include "dpws.nsmap"
#include "dpws_device.h"
#include <signal.h>
#include <time.h>
#include "msiop_metadata.h"
#include "msiop_wsdl.h"
#include "iop_services.h"

#include "test_support.h"

START_TEST (stringlist_create_destroy)
{
  struct ws4d_stringlist list;

  fail_if (ws4d_stringlist_init (&list) != WS4D_OK, "Can't initialize string list structure\n");

  fail_if (ws4d_stringlist_done (&list) != WS4D_OK, "Can't clean up string list structure\n");
}
END_TEST

START_TEST (stringlist_add_one_item)
{
  struct ws4d_stringlist list;

  fail_if (ws4d_stringlist_init (&list) != WS4D_OK, "Can't initialize string list structure\n");

  fail_if (ws4d_stringlist_add(&list, "test") != WS4D_OK, "Can't add string to list\n");

  fail_if (ws4d_stringlist_done (&list) != WS4D_OK, "Can't clean up string list structure\n");
}
END_TEST

START_TEST (stringlist_add_several_items)
{
  struct ws4d_stringlist list;

  fail_if (ws4d_stringlist_init (&list) != WS4D_OK, "Can't initialize string list structure\n");

  fail_if (ws4d_stringlist_add(&list, "test1 test2  test3    test4 ") != WS4D_OK, "Can't add string to list\n");

  fail_if (ws4d_stringlist_done (&list) != WS4D_OK, "Can't clean up string list structure\n");
}
END_TEST

START_TEST (stringlist_to_string_one)
{
  struct ws4d_stringlist list;
  const char *string = NULL;

  fail_if (ws4d_stringlist_init (&list) != WS4D_OK, "Can't initialize string list structure\n");

  fail_if (ws4d_stringlist_add(&list, "test1") != WS4D_OK, "Can't add string to list\n");

  string = ws4d_stringlist_tostring(&list);
  fail_if (string == NULL, "to string returns NULL");
  fail_if (strcmp(string, "test1"), "unexpected result of to string");

  fail_if (ws4d_stringlist_done (&list) != WS4D_OK, "Can't clean up string list structure\n");
}
END_TEST

START_TEST (stringlist_to_string_two)
{
  struct ws4d_stringlist list;
  const char *string = NULL;

  fail_if (ws4d_stringlist_init (&list) != WS4D_OK, "Can't initialize string list structure\n");

  fail_if (ws4d_stringlist_add(&list, "test1 test2 ") != WS4D_OK, "Can't add string to list\n");

  string = ws4d_stringlist_tostring(&list);
  fail_if (string == NULL, "to string returns NULL");
  fail_if (strcmp(string, "test1 test2"), "unexpected result of to string");

  fail_if (ws4d_stringlist_done (&list) != WS4D_OK, "Can't clean up string list structure\n");
}
END_TEST

WS4D_TESTCASE_START ("stringlist")
WS4D_TESTCASE_ADD (stringlist_create_destroy)
WS4D_TESTCASE_ADD (stringlist_add_one_item)
WS4D_TESTCASE_ADD (stringlist_add_several_items)
WS4D_TESTCASE_ADD (stringlist_to_string_one)
WS4D_TESTCASE_ADD (stringlist_to_string_two)
WS4D_TESTCASE_END
