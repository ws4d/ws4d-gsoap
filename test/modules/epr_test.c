/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 02.12.2010
 *      Author: elmex
 */

#include "dpws.nsmap"
#include "dpws_device.h"
#include <signal.h>
#include <time.h>
#include "msiop_metadata.h"
#include "msiop_wsdl.h"
#include "iop_services.h"

#include "test_support.h"

START_TEST (epr_create_destroy)
{
  struct ws4d_epr epr;

  fail_if (ws4d_epr_init (&epr) != WS4D_OK, "Can't initialize epr structure\n");

  fail_if (ws4d_epr_done (&epr) != WS4D_OK, "Can't clean up epr structure\n");
}
END_TEST

START_TEST (epr_check_invalid)
{
  struct ws4d_epr epr;

  fail_if (ws4d_epr_init (&epr) != WS4D_OK, "Can't initialize epr structure\n");

  fail_if (ws4d_epr_check(&epr) != 1, "epr check failed!\n");

  fail_if (ws4d_epr_done (&epr) != WS4D_OK, "Can't clean up epr structure\n");

  fail_unless (ws4d_epr_check(&epr) != 1, "epr check did not fail!\n");
}
END_TEST

WS4D_TESTCASE_START ("epr")
WS4D_TESTCASE_ADD (epr_create_destroy)
WS4D_TESTCASE_ADD (epr_check_invalid)
WS4D_TESTCASE_END
