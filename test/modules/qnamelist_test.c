/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2007  University of Rostock
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 *  Created on: 02.12.2010
 *      Author: elmex
 */

#include "dpws.nsmap"
#include "dpws_device.h"
#include <signal.h>
#include <time.h>
#include "msiop_metadata.h"
#include "msiop_wsdl.h"
#include "iop_services.h"

#include "test_support.h"

START_TEST (qnamelist_create_destroy)
{
  struct ws4d_qnamelist list;

  fail_if (ws4d_qnamelist_init (&list) != WS4D_OK, "Can't initialize qname list structure\n");

  fail_if (ws4d_qnamelist_done (&list) != WS4D_OK, "Can't clean up qname list structure\n");
}
END_TEST

START_TEST (qnamelist_add_several_qnames)
{
  struct ws4d_qnamelist list;
  struct ws4d_qname qname1, qname2;
  const char *string;

  fail_if (ws4d_qnamelist_init (&list) != WS4D_OK, "Can't initialize qname list structure\n");

  fail_if (ws4d_qnamelist_addstring(&list, "\"http://www.ws4d.org/ns1\":test1") != WS4D_OK, "Can't add qname to list\n");

  fail_if (ws4d_qnamelist_addstring(&list, "   \"http://www.ws4d.org/ns1\":test2 \"http://www.ws4d.org/ns1\":test3   ") != WS4D_OK, "Can't add qname to list\n");

  fail_if (ws4d_qname_init(&qname1) != WS4D_OK, "Can't initialize qname structure\n");

  ws4d_qname_setNS(&qname1, "http://www.ws4d.org/ns1");
  ws4d_qname_setName(&qname1, "test4");

  fail_if (ws4d_qnamelist_add(&list, &qname1) != WS4D_OK, "Can't add qname to list\n");

  fail_if (ws4d_qname_init(&qname2) != WS4D_OK, "Can't initialize qname structure\n");

  ws4d_qname_setNS(&qname2, "http://www.ws4d.org/ns1");
  ws4d_qname_setPrefix(&qname2, "ws4d");
  ws4d_qname_setName(&qname2, "test5");

  fail_if (ws4d_qnamelist_add(&list, &qname2) != WS4D_OK, "Can't add qname to list\n");

  string = ws4d_qnamelist_tostring(&list);
  printf("QNameList: %s\n", ws4d_qnamelist_tostring(&list));

  fail_if (strcmp(string, "\"http://www.ws4d.org/ns1\":test1 \"http://www.ws4d.org/ns1\":test2 \"http://www.ws4d.org/ns1\":test3 \"http://www.ws4d.org/ns1\":test4 \"http://www.ws4d.org/ns1\":test5") != 0, "Unexpected qname string representation\n");

  fail_if (ws4d_qnamelist_done (&list) != WS4D_OK, "Can't clean up qname list structure\n");
}
END_TEST

START_TEST (qnamelist_compare)
{
  struct ws4d_qnamelist list1, list2, list3, list4;

  fail_if (ws4d_qnamelist_init2(&list1, "\"http://www.ws4d.org/ns1\":test3 \"http://www.ws4d.org/ns1\":test2 \"http://www.ws4d.org/ns1\":test4 \"http://www.ws4d.org/ns1\":test5 \"http://www.ws4d.org/ns1\":test1"), "Can't initialize qname list structure\n");

  fail_if (ws4d_qnamelist_init2(&list2, "\"http://www.ws4d.org/ns1\":test2 \"http://www.ws4d.org/ns1\":test3 \"http://www.ws4d.org/ns1\":test4 \"http://www.ws4d.org/ns1\":test5"), "Can't initialize qname list structure\n");

  fail_if (ws4d_qnamelist_init(&list3), "Can't initialize qname list structure\n");

  fail_if (ws4d_qnamelist_init(&list4), "Can't initialize qname list structure\n");

  fail_if (ws4d_qnamelist_addlist(&list3, &list2), "Can't copy list\n");

  fail_if (ws4d_qnamelist_compare(&list2, &list3) != 0, "List2 and List3 should match exactly\n");

  fail_if (ws4d_qnamelist_compare(&list2, &list1) != -1, "List1 should contains List2\n");

  fail_if (ws4d_qnamelist_compare(&list1, &list2) != +1, "List1 should not match or contain List2\n");

  fail_if (ws4d_qnamelist_compare(&list1, &list4) != +1, "List1 should not match or contain emtpy List4\n");

  fail_if (ws4d_qnamelist_compare(&list4, &list1) != -1, "List4 contains emtpy List1\n");

  fail_if (ws4d_qnamelist_done (&list1) != WS4D_OK, "Can't clean up qname list structure\n");

  fail_if (ws4d_qnamelist_done (&list2) != WS4D_OK, "Can't clean up qname list structure\n");

  fail_if (ws4d_qnamelist_done (&list3) != WS4D_OK, "Can't clean up qname list structure\n");

  fail_if (ws4d_qnamelist_done (&list4) != WS4D_OK, "Can't clean up qname list structure\n");
}
END_TEST

WS4D_TESTCASE_START ("qnamelist")
WS4D_TESTCASE_ADD (qnamelist_create_destroy)
WS4D_TESTCASE_ADD (qnamelist_add_several_qnames)
WS4D_TESTCASE_ADD (qnamelist_compare)
WS4D_TESTCASE_END
